﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatVoiceItemUIController : ChatItemControllerBase
    {
        private bool m_alwaysEnableTranslate;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, ChatInfo> EventOnSoundChatItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ChatInfo> EventOnAudio2StringButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ChatInfo> EventOnTranslateStringButtonClick;
        private bool m_isPlaying;
        private const float m_minBGImageWidth = 80f;
        private const float m_maxBGImageWidth = 250f;
        private const string VoicePlay = "Animation";
        private const string VoiceNotPlay = "Stop";
        [AutoBind("./ChatDetail/SoundChat/SoundGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./PlayerName/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNameText;
        [AutoBind("./PlayerName/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommanderItemDummy;
        [AutoBind("./ChatDetail/SoundChat/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnReadTip;
        [AutoBind("./ChatDetail/SoundChat/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeNumberText;
        [AutoBind("./ChatDetail/SoundChat", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SoundButton;
        [AutoBind("./ChatDetail/SoundChat", AutoBindAttribute.InitState.NotInit, false)]
        public Image BGImage;
        [AutoBind("./ChatDetail/SoundChat/Audio2StringButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Audio2StringButton;
        [AutoBind("./ChatDetail/SoundChat/Audio2StringButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Audio2StringButtonGo;
        [AutoBind("./TranslateImage/BGImage/TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TranslateStringButton;
        [AutoBind("./TranslateImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TranslateStringRoot;
        [AutoBind("./TranslateImage/BGImage/TranslateButton/TranslateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TranslateButtonText;
        [AutoBind("./TranslateImage/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text Audio2StringText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_UnRegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_RegisterAudio2StringButtonClick;
        private static DelegateBridge __Hotfix_OnSoundButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_OnAudio2StringButtonClick;
        private static DelegateBridge __Hotfix_OnTranslateStringButtonClick;
        private static DelegateBridge __Hotfix_UpdateChatItemInfo;
        private static DelegateBridge __Hotfix_UpdateAudio2StringContent;
        private static DelegateBridge __Hotfix_SetSoundPlayAnimationState;
        private static DelegateBridge __Hotfix_GetImageWidthByChatInfo;
        private static DelegateBridge __Hotfix_ConvertToTimeStringWithCharacterUnit;
        private static DelegateBridge __Hotfix_ConvertToViewNameText;
        private static DelegateBridge __Hotfix_SetUnReadTip;
        private static DelegateBridge __Hotfix_GetChatText;
        private static DelegateBridge __Hotfix_add_EventOnSoundChatItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSoundChatItemClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAudio2StringButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAudio2StringButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTranslateStringButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTranslateStringButtonClick;
        private static DelegateBridge __Hotfix_get_IsPlaying;

        public event Action<ChatInfo> EventOnAudio2StringButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, ChatInfo> EventOnSoundChatItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ChatInfo> EventOnTranslateStringButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string ConvertToTimeStringWithCharacterUnit(int time)
        {
        }

        [MethodImpl(0x8000)]
        private string ConvertToViewNameText(string allianceName, string playerName)
        {
        }

        [MethodImpl(0x8000)]
        private string GetChatText(string srcString)
        {
        }

        [MethodImpl(0x8000)]
        private float GetImageWidthByChatInfo(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAudio2StringButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerHeadButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSoundButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTranslateStringButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterAudio2StringButtonClick(Action<ChatInfo> audio2StringButtonAction, Action<ChatInfo> translateStringButtonAction)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterButtonClickEvent(Action<UIControllerBase, ChatInfo> soundAction, Action<UIControllerBase, ChatInfo> playerHeadAction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundPlayAnimationState(bool isPlay)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUnReadTip(bool isRead)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterButtonClickEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAudio2StringContent(bool isSelf)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatItemInfo(ChatInfo chatInfo, Dictionary<string, UnityEngine.Object> m_dynamicCache, bool isSelf, bool isPlaying)
        {
        }

        public bool IsPlaying
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

