﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using Cinemachine;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Gestures;
    using TouchScript.Layers;
    using UnityEngine;

    public class ThreeDModelViewController : UIControllerBase
    {
        public float ScreenWidth;
        public float ScreenHeight;
        public GameObject ModelViewGameObject;
        public Bounds ModelViewObjectBounds;
        public float CurrScreenPortion;
        [AutoBind("./ModelViewDummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModelViewDummyRoot;
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera ModelViewCamera;
        [AutoBind("./CameraRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TouchScript.Gestures.ScreenTransformGesture ScreenTransformGesture;
        [AutoBind("./CameraRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullScreenLayer;
        public const float RotateSensitivityForY = 4.5f;
        public const float RotateSensitivityForX = 1.5f;
        public const float RotateMaxForY = 45f;
        public float ScaleSensitivity;
        public const float MaxScreenPortion = 1.3f;
        public const float DefaultScreenPortion = 1.1f;
        public const float MinScreenPortion = 0.8f;
        public static string UIModelViewLayerMask = "UIModelPreviewLayer";
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefab;
        private static DelegateBridge __Hotfix_Clear3DModel;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefabImp;
        private static DelegateBridge __Hotfix_CalcScreenSize;
        private static DelegateBridge __Hotfix_GetCameraDistance;
        private static DelegateBridge __Hotfix_GetCameraPositionFromScreenPortion;
        private static DelegateBridge __Hotfix_GetObjectBound;
        private static DelegateBridge __Hotfix_GetPixelsPerMeter;
        private static DelegateBridge __Hotfix_SetViewModelLayerMask;
        private static DelegateBridge __Hotfix_SetGameObjectLayerMask;
        private static DelegateBridge __Hotfix_ManipulationTransformedHandler;

        [MethodImpl(0x8000)]
        private void CalcScreenSize(Rect viewRect)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear3DModel()
        {
        }

        [MethodImpl(0x8000)]
        private float GetCameraDistance(float screenPortion)
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 GetCameraPositionFromScreenPortion(float screenPortion)
        {
        }

        [MethodImpl(0x8000)]
        private Bounds GetObjectBound()
        {
        }

        [MethodImpl(0x8000)]
        private float GetPixelsPerMeter()
        {
        }

        [MethodImpl(0x8000)]
        public void InitWith3DModelPrefab(GameObject modelCloneSource, Rect viewRect, Func<GameObject, Bounds> PrepareViewGameObjectFunc = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator InitWith3DModelPrefabImp(GameObject modelCloneSource, Rect viewRect, Func<GameObject, Bounds> PrepareViewGameObjectFunc)
        {
        }

        [MethodImpl(0x8000)]
        private void ManipulationTransformedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void SetGameObjectLayerMask(GameObject go, int layer)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewModelLayerMask()
        {
        }

        [CompilerGenerated]
        private sealed class <InitWith3DModelPrefabImp>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GameObject modelCloneSource;
            internal CinemachineVirtualCamera <virtualCamrea>__0;
            internal Func<GameObject, Bounds> PrepareViewGameObjectFunc;
            internal Rect viewRect;
            internal Vector3 <cameraPos>__0;
            internal ThreeDModelViewController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

