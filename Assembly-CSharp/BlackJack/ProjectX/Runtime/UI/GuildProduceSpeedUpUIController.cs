﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildProduceSpeedUpUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildProductionSpeedUpLevel> EventOnSpeedUpItemClick;
        private GuildProductionLineInfo m_currLineInfo;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./DetailPanel/SpeedUpBarImage02", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProcessBar;
        [AutoBind("./DetailPanel/TimeTitleText/PlantText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RestTimeText;
        [AutoBind("./DetailPanel/ItemGroup/Item00/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyText_1;
        [AutoBind("./DetailPanel/ItemGroup/Item01/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyText_2;
        [AutoBind("./DetailPanel/ItemGroup/Item02/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyText_3;
        [AutoBind("./DetailPanel/ItemGroup/Item00/SpeedUpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EffectDescText_1;
        [AutoBind("./DetailPanel/ItemGroup/Item01/SpeedUpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EffectDescText_2;
        [AutoBind("./DetailPanel/ItemGroup/Item02/SpeedUpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EffectDescText_3;
        [AutoBind("./DetailPanel/ItemGroup/Item00/IconButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SpeedUpButton_1;
        [AutoBind("./DetailPanel/ItemGroup/Item01/ValueText/IconButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SpeedUpButton_2;
        [AutoBind("./DetailPanel/ItemGroup/Item02/ValueText/IconButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SpeedUpButton_3;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateSpeedUpPanelInfo;
        private static DelegateBridge __Hotfix_GetSpeedUpEffectDescByLevel;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick_1;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick_2;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick_3;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildProductionSpeedUpLevel> EventOnSpeedUpItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string GetSpeedUpEffectDescByLevel(GuildProductionSpeedUpLevel level)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick_1(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick_2(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick_3(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpeedUpPanelInfo(GuildProductionLineInfo lineInfo, Dictionary<string, Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

