﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBuildingRecycleUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnRecycleItemClick;
        private GuildBuildingInfo m_guildBuildingInfo;
        private List<CommonItemIconUIController> m_recycleGainItemList;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool RecycleGainItemEasyPool;
        [AutoBind("./ContentGroup/GuildBuildingRecycleGroup/ItemGroup/GainItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform GainItemGroupRoot;
        [AutoBind("./ContentGroup/GuildBuildingRecycleGroup/RecycleTime/RecycleTimeTitleText/RecycleTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RecycleTimeText;
        [AutoBind("./ContentGroup/GuildBuildingRecycleGroup/RecycleTarget/TargetNameTitleText/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./ContentGroup/RecycleButtons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroupButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BuildingInProduceMsgBox", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_notfiyMsgBoxStateCtrl;
        [AutoBind("./BuildingInProduceMsgBox/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buildInProduceConfirmButton;
        [AutoBind("./BuildingInProduceMsgBox/BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buildInProduceBgButton;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_contentRoot;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_contentBgRoot;
        [AutoBind("./BGImages/TitleText/TipsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipWindowStateCtrl;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipWindowCloseButton;
        [AutoBind("./BuildingIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BuildingIconImage;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingRecycle;
        private static DelegateBridge __Hotfix_CreateShowMainPanelProcess;
        private static DelegateBridge __Hotfix_ShowDetailContent;
        private static DelegateBridge __Hotfix_CreateShowBuildingInProduceMsgBoxProcess;
        private static DelegateBridge __Hotfix_GetTipWindowUIProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnRecycleItemCreated;
        private static DelegateBridge __Hotfix_PrepareRecycleGainItems;
        private static DelegateBridge __Hotfix_OnRecycleItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRecycleItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRecycleItemClick;

        public event Action<int> EventOnRecycleItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateShowBuildingInProduceMsgBoxProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateShowMainPanelProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecycleItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecycleItemCreated(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareRecycleGainItems(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowDetailContent(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingRecycle(GuildBuildingInfo buildingInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

