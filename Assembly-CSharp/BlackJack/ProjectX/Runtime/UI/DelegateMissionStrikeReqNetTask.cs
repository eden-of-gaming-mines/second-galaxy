﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class DelegateMissionStrikeReqNetTask : NetWorkTransactionTask
    {
        private ulong m_signalInstanceId;
        private List<ulong> m_captainInsIdList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <StartReqResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnMissionStartAck;
        private static DelegateBridge __Hotfix_set_StartReqResult;
        private static DelegateBridge __Hotfix_get_StartReqResult;

        [MethodImpl(0x8000)]
        public DelegateMissionStrikeReqNetTask(ulong signalInstanceId, List<ulong> captainInsIdList)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMissionStartAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int StartReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

