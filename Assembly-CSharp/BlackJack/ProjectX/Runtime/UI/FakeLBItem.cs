﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class FakeLBItem : ILBItem
    {
        protected StoreItemType m_itemType;
        protected object m_configInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetItemType;
        private static DelegateBridge __Hotfix_GetConfigInfo;

        [MethodImpl(0x8000)]
        public FakeLBItem(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfigInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetItemType()
        {
        }
    }
}

