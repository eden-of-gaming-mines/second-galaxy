﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class TechUpgradeChooseCaptainUITask : UITaskBase
    {
        public const string UIMode_UpgradeTechMode = "UpgradeTech";
        public const string UIMode_ProduceMode = "Produce";
        public const string ParamKey_UpgradeTechId = "UpgradeTechId";
        public const string ParamKey_ProduceBlueprintId = "ProduceBlueprintId";
        public const string ParamKey_ProduceBlueprintCount = "ProduceBlueprintCount";
        private TechUpgradeChooseCaptainUIController m_mainCtrl;
        private int m_techId;
        private int m_produceBlueprintId;
        private int m_produceBlueprintCount;
        private List<LBStaticHiredCaptain> m_captainListCache;
        private Dictionary<ulong, Turple<float, float, float>> m_captainAssistEffectValueDict;
        private int m_currentSelectionIndex;
        private List<Turple<LBNpcCaptainFeats, bool, bool>> m_captainFeatList;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartChooseCaptainUITask;
        private static DelegateBridge __Hotfix_StartChooseCaptainUITaskInProduceUITask;
        private static DelegateBridge __Hotfix_HasCanUsedCaptain;
        private static DelegateBridge __Hotfix_GetMaxEffectCaptainForTech;
        private static DelegateBridge __Hotfix_GetMaxEffectCaptainForProduce;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_GetNextTechLevelInfo;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_ProduceMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_TechMode;
        private static DelegateBridge __Hotfix_GetSuitableCaptainInfoItemStartIndex;
        private static DelegateBridge __Hotfix_GetAssitEffectValueForProduceMode;
        private static DelegateBridge __Hotfix_GetAssitEffectValueWithCaptainAndTech;
        private static DelegateBridge __Hotfix_CaptainAssistValueComparer;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainItemClicked;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public TechUpgradeChooseCaptainUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainAssistValueComparer(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private static Turple<float, float, float> GetAssitEffectValueForProduceMode(LBStaticHiredCaptain captain, int blueprintId, int count)
        {
        }

        [MethodImpl(0x8000)]
        private static Turple<float, float, float> GetAssitEffectValueWithCaptainAndTech(LBStaticHiredCaptain captain, int techId, ConfigDataTechLevelInfo nextLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ulong GetMaxEffectCaptainForProduce(int blueprintId)
        {
        }

        [MethodImpl(0x8000)]
        public static ulong GetMaxEffectCaptainForTech(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataTechLevelInfo GetNextTechLevelInfo(int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableCaptainInfoItemStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasCanUsedCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainItemClicked(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeChooseCaptainUITask StartChooseCaptainUITask(UIIntent mainIntent, int techId)
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeChooseCaptainUITask StartChooseCaptainUITaskInProduceUITask(int blueprintId, int count, UIIntent returnIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_ProduceMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_TechMode()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsCaptainSelectionChanged
        }
    }
}

