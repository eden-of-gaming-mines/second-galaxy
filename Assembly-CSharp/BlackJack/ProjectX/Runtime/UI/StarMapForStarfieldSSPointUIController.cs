﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForStarfieldSSPointUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSolarSystemSimpleInfo> EventOnPointButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemSimpleInfo> EventOnPoint3DTouch;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_pointButton;
        [AutoBind("./ColorCircleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_colorChangedImage;
        [AutoBind("./SSNamePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_solarSystemNamePosTrans;
        [AutoBind("./LineOffsetDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lineOffsetPosTrans;
        [AutoBind("./TunnelEffectImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tunnelEffectObj;
        [AutoBind("./TunnelEffectImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tunnelEffectStat;
        [AutoBind("./TunnelEffectImage", AutoBindAttribute.InitState.NotInit, false)]
        public CanvasGroup m_tunnelCanvasGroup;
        [AutoBind("./TunnelEffectImage/Enabled", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tunnelEnabled;
        [AutoBind("./TunnelEffectImage/OutOfRange", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tunnelOutOfRange;
        [AutoBind("./TunnelEffectImage/FlagShip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tunnelFlagShip;
        [AutoBind("./TunnelEffectImage/Center", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tunnelCenter;
        private const string StrNormal = "Normal";
        private const string StrFlicker = "Flicker";
        public ThreeDTouchEventListener m_3dTouchEventTrigger;
        private static float m_defaultSize;
        private GDBSolarSystemSimpleInfo m_solarSystemInfo;
        public int m_tunnelStat;
        [CompilerGenerated]
        private static Predicate<GuildTeleportTunnelEffectInfoClient> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetSolarSystemInfo;
        private static DelegateBridge __Hotfix_SetPointButtonClickEvent;
        private static DelegateBridge __Hotfix_SetPointColor;
        private static DelegateBridge __Hotfix_SetPointPos;
        private static DelegateBridge __Hotfix_SetPointSize;
        private static DelegateBridge __Hotfix_GetLineOffset;
        private static DelegateBridge __Hotfix_GetSolarSystemNameOffset;
        private static DelegateBridge __Hotfix_SetTunnelEffect;
        private static DelegateBridge __Hotfix_SetTunnelStat;
        private static DelegateBridge __Hotfix_OnPointButtonClick;
        private static DelegateBridge __Hotfix_OnPoint3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnPointButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPointButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPoint3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnPoint3DTouch;

        public event Action<GDBSolarSystemSimpleInfo> EventOnPoint3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemSimpleInfo> EventOnPointButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public float GetLineOffset()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetSolarSystemNameOffset()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoint3DTouch()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPointButtonClickEvent(Action<GDBSolarSystemSimpleInfo> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPointColor(Color32 color)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPointPos(Transform parentTrans, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPointSize(float size)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemInfo(GDBSolarSystemSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTunnelEffect(float effect)
        {
        }

        [MethodImpl(0x8000)]
        public int SetTunnelStat(int showTunnelType, float enableRange = 0f, GDBSolarSystemSimpleInfo centerSolar = null, List<GuildTeleportTunnelEffectInfoClient> tunnelList = null)
        {
        }
    }
}

