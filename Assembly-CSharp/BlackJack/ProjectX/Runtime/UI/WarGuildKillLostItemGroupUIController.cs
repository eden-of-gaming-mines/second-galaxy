﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class WarGuildKillLostItemGroupUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, bool> EvenOnGuildIconClick;
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [AutoBind("./LeftLostItem", AutoBindAttribute.InitState.NotInit, false)]
        public WarGuildKillLostItemUIController m_leftLostItemUICtrl;
        [AutoBind("./RightLostItem", AutoBindAttribute.InitState.NotInit, false)]
        public WarGuildKillLostItemUIController m_rightLostItemUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateWarGuildKillLostItemGroupUI;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnLeftGuildIconClick;
        private static DelegateBridge __Hotfix_OnRightGuildIconClick;
        private static DelegateBridge __Hotfix_add_EvenOnGuildIconClick;
        private static DelegateBridge __Hotfix_remove_EvenOnGuildIconClick;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        public event Action<UIControllerBase, bool> EvenOnGuildIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeftGuildIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRightGuildIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarGuildKillLostItemGroupUI(GuildBattleGuildKillLostStatInfo leftLossInfo, GuildBattleGuildKillLostStatInfo rightLossInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

