﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonUIUtil
    {
        public const string FunctionUnlockActionItemName = "FunctionUnlockActionItem";
        private static readonly Dictionary<char, string> m_chineseChar2FirstSpellDict = new Dictionary<char, string>();
        protected static float m_netLoseConnectFlagTime;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Action <>f__am$cache3;
        [CompilerGenerated]
        private static Action <>f__am$cache4;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache5;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PrepareItemForDatas;
        private static DelegateBridge __Hotfix_IsDSUnSyncErrorCode;
        private static DelegateBridge __Hotfix_DownloadHttpFile;
        private static DelegateBridge __Hotfix_OpenWebUrl;
        private static DelegateBridge __Hotfix_GetLangueTypeByLangeueSimpleName;
        private static DelegateBridge __Hotfix_WaitUntil;
        private static DelegateBridge __Hotfix_SetRectTransformSize;
        private static DelegateBridge __Hotfix_GetRectTransformWorldSize;
        private static DelegateBridge __Hotfix_SetRectTransformRotationByNormalizationNum;
        private static DelegateBridge __Hotfix_CalculateTouchPos;
        private static DelegateBridge __Hotfix_GetSpriteFromCache;
        private static DelegateBridge __Hotfix_ConvertStrToColorStr;
        private static DelegateBridge __Hotfix_GetForceIconResPathByForceType;
        private static DelegateBridge __Hotfix_GetFactionNameStrByForceType;
        private static DelegateBridge __Hotfix_GetProduceProcess;
        private static DelegateBridge __Hotfix_IsFeatUsefulForTechUpgrade;
        private static DelegateBridge __Hotfix_IsFeatUsefulForProduce;
        private static DelegateBridge __Hotfix_GetMailTitleString;
        private static DelegateBridge __Hotfix_GetMailContentString;
        private static DelegateBridge __Hotfix_GetBlueprintCurrencyTotalCost;
        private static DelegateBridge __Hotfix_GetLicenseNodeIcon;
        private static DelegateBridge __Hotfix_GetFactionIconInDriving;
        private static DelegateBridge __Hotfix_CheckShipDrivingLicenseRequest;
        private static DelegateBridge __Hotfix_SetClientPlayerPrefsString;
        private static DelegateBridge __Hotfix_SetClientPlayerPrefsFloat;
        private static DelegateBridge __Hotfix_SetClientPlayerPrefsInt;
        private static DelegateBridge __Hotfix_GetClientPlayerPrefsInt;
        private static DelegateBridge __Hotfix_GetClientPlayerPrefsFloat;
        private static DelegateBridge __Hotfix_GetClientPlayerPrefsString;
        private static DelegateBridge __Hotfix_StartNewShipGetActionQueue;
        private static DelegateBridge __Hotfix_BringTipWindowAndPromptToTop;
        private static DelegateBridge __Hotfix_StartNewCaptainGetActionQueue;
        private static DelegateBridge __Hotfix_StartUserGuideActionQueue;
        private static DelegateBridge __Hotfix_CreateFunctionUnlockActionItem;
        private static DelegateBridge __Hotfix_SetSpeedValueToFcText;
        private static DelegateBridge __Hotfix_SetDistanceValueToFcText;
        private static DelegateBridge __Hotfix_IsTheSameShipTeplateInfo;
        private static DelegateBridge __Hotfix_GetTemplateName;
        private static DelegateBridge __Hotfix_CloseAllModalTask;
        private static DelegateBridge __Hotfix_IsModelTaskCanShow;
        private static DelegateBridge __Hotfix_GetCharFirstSpell;
        private static DelegateBridge __Hotfix_CompareForString;
        private static DelegateBridge __Hotfix_IsObjectVisibleByCamera;
        private static DelegateBridge __Hotfix_GetIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetEditorSceneArtResFullPath;
        private static DelegateBridge __Hotfix_GetCutSceneArtResFullPath;
        private static DelegateBridge __Hotfix_GetAudioResFullPath;
        private static DelegateBridge __Hotfix_CheckShipCustomTempleteNameValid;
        private static DelegateBridge __Hotfix_CheckGuildFleetNameValid;
        private static DelegateBridge __Hotfix_IsPlayerNameLegal;
        private static DelegateBridge __Hotfix_GetCurrencyLogo;
        private static DelegateBridge __Hotfix_GetGuildBuildGlobalSceneIdInSolarSystem;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_ScreenRecordEnabled;

        [MethodImpl(0x8000)]
        public static void BringTipWindowAndPromptToTop()
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 CalculateTouchPos(float z, SceneLayerBase sceneLayer)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckGuildFleetNameValid(string guildFleetName)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckShipCustomTempleteNameValid(string inputString)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckShipDrivingLicenseRequest(ConfigDataSpaceShipInfo shipInfo, out int drivingLicenseId)
        {
        }

        [MethodImpl(0x8000)]
        public static void CloseAllModalTask()
        {
        }

        [MethodImpl(0x8000)]
        public static int CompareForString(string a, string b)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertStrToColorStr(string str, string colorStr)
        {
        }

        [MethodImpl(0x8000)]
        public static UIManager.UIActionQueueItem CreateFunctionUnlockActionItem(Action startAction)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator DownloadHttpFile(string url, Action<bool, WWW> onReceive, Action<WWW> onUpdate = null)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAudioResFullPath(string audioResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CostInfo> GetBlueprintCurrencyTotalCost(List<CostInfo> costInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCharFirstSpell(char c)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetClientPlayerPrefsFloat(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetClientPlayerPrefsInt(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetClientPlayerPrefsString(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCurrencyLogo(CostCurrencyType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCutSceneArtResFullPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEditorSceneArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFactionIconInDriving(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFactionNameStrByForceType(GrandFaction grandFaction, bool isShort = false)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetForceIconResPathByForceType(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildBuildGlobalSceneIdInSolarSystem(int solarSystemId, ConfigDataGuildBuildingInfo buildConfig)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static LanguageType GetLangueTypeByLangeueSimpleName(string simpleName)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetLicenseNodeIcon(GrandFaction faction, ShipType shipType, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetMailContentString(LBStoredMailClient lbMail, bool showTranslate)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetMailTitleString(LBStoredMailClient lbMail, bool showTranslate)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetProduceProcess(DateTime startTime, DateTime endTime, int modifyTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2 GetRectTransformWorldSize(RectTransform trans)
        {
        }

        [MethodImpl(0x8000)]
        public static Sprite GetSpriteFromCache(Dictionary<string, UnityEngine.Object> resDict, string path, bool useErrorSprite = true)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTemplateName(ShipCustomTemplateInfo templateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsDSUnSyncErrorCode(int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFeatUsefulForProduce(LBNpcCaptainFeats featInfo, ConfigDataProduceBlueprintInfo blueprintConf)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFeatUsefulForTechUpgrade(LBNpcCaptainFeats featInfo, ConfigDataTechInfo techConf, ConfigDataTechLevelInfo techLvInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsModelTaskCanShow()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsObjectVisibleByCamera(Vector3 targetPosition, Camera cam = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPlayerNameLegal(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsTheSameShipTeplateInfo(ShipCustomTemplateInfo srcInfo, ShipCustomTemplateInfo destInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void OpenWebUrl(string url)
        {
        }

        [MethodImpl(0x8000)]
        public static void PrepareItemForDatas<T>(int count, List<T> ctrlList, RectTransform root, EasyObjectPool easyPool, string poolName) where T: UIControllerBase
        {
        }

        [MethodImpl(0x8000)]
        public static bool ScreenRecordEnabled()
        {
        }

        [MethodImpl(0x8000)]
        public static void SetClientPlayerPrefsFloat(string name, float val)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetClientPlayerPrefsInt(string name, int val)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetClientPlayerPrefsString(string name, string val)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetDistanceValueToFcText(FrequentChangeText fcText, double value)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetRectTransformRotationByNormalizationNum(RectTransform trans, float num, float selfAngle, float notShowAngle)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetRectTransformSize(RectTransform trans, Vector2 newSize)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetSpeedValueToFcText(FrequentChangeText fcText, double value)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartNewCaptainGetActionQueue(ulong captainConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartNewShipGetActionQueue(int shipConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartUserGuideActionQueue(UITaskBase targetUITask, UserGuideTriggerPoint triggerPointTyp, Func<bool> canStart, int questConfigId = 0)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator WaitUntil(Func<bool> predicate)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DownloadHttpFile>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <retryCount>__0;
            internal string url;
            internal Action<WWW> onUpdate;
            internal Action<bool, WWW> onReceive;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <DownloadHttpFile>c__AnonStorey2 $locvar0;
            private <DownloadHttpFile>c__AnonStorey3 $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                int num2;
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <DownloadHttpFile>c__AnonStorey2();
                        this.$locvar0.<>f__ref$0 = this;
                        this.$locvar0.onUpdate = this.onUpdate;
                        this.<retryCount>__0 = 5;
                        break;

                    case 1:
                        if (!string.IsNullOrEmpty(this.$locvar1.www.error))
                        {
                            Debug.LogError($"DownHttpTextFile {this.url} Error: {this.$locvar1.www.error}");
                            if (this.<retryCount>__0 > 0)
                            {
                                Debug.Log("Wait for 2 seconds and retry to download...");
                                this.$current = new WaitForSeconds(2f);
                                if (!this.$disposing)
                                {
                                    this.$PC = 2;
                                }
                                goto TR_0004;
                            }
                        }
                        else
                        {
                            if (this.onReceive != null)
                            {
                                this.onReceive(true, this.$locvar1.www);
                            }
                            goto TR_0000;
                        }
                        break;

                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                this.<retryCount>__0 = (num2 = this.<retryCount>__0) - 1;
                if (num2 > 0)
                {
                    this.$locvar1 = new <DownloadHttpFile>c__AnonStorey3();
                    this.$locvar1.<>f__ref$0 = this;
                    this.$locvar1.<>f__ref$2 = this.$locvar0;
                    this.$locvar1.www = new WWW(this.url);
                    this.$current = CommonUIUtil.WaitUntil(new Func<bool>(this.$locvar1.<>m__0));
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_0004;
                }
                else
                {
                    if (this.onReceive != null)
                    {
                        this.onReceive(false, null);
                    }
                    this.$PC = -1;
                }
            TR_0000:
                return false;
            TR_0004:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <DownloadHttpFile>c__AnonStorey2
            {
                internal Action<WWW> onUpdate;
                internal CommonUIUtil.<DownloadHttpFile>c__Iterator0 <>f__ref$0;
            }

            private sealed class <DownloadHttpFile>c__AnonStorey3
            {
                internal WWW www;
                internal CommonUIUtil.<DownloadHttpFile>c__Iterator0 <>f__ref$0;
                internal CommonUIUtil.<DownloadHttpFile>c__Iterator0.<DownloadHttpFile>c__AnonStorey2 <>f__ref$2;

                internal bool <>m__0()
                {
                    if (this.<>f__ref$2.onUpdate != null)
                    {
                        this.<>f__ref$2.onUpdate(this.www);
                    }
                    return this.www.isDone;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNewCaptainGetActionQueue>c__AnonStorey5
        {
            internal ulong captainConfigId;
            internal UIManager.UIActionQueueItem item;

            internal void <>m__0()
            {
                SpaceStationUIHelper.EnableActiveBtnEffect(false);
                NewCaptainGetUITask.StartNewCaptainGetUITask(this.captainConfigId).RegisteNewCaptainGetUITaskCloseEvent(delegate {
                    SpaceStationUIHelper.EnableActiveBtnEffect(true);
                    this.item.OnEnd();
                });
            }

            internal void <>m__1()
            {
                SpaceStationUIHelper.EnableActiveBtnEffect(true);
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <StartNewShipGetActionQueue>c__AnonStorey4
        {
            internal int shipConfigId;
            internal UIManager.UIActionQueueItem item;

            internal void <>m__0()
            {
                SpaceStationUIHelper.EnableActiveBtnEffect(false);
                NewShipGetUITask.StartNewShipGetUITask(this.shipConfigId).RegisterBGButtonEvent(delegate {
                    this.item.OnEnd();
                    SpaceStationUIHelper.EnableActiveBtnEffect(true);
                });
            }

            internal void <>m__1()
            {
                this.item.OnEnd();
                SpaceStationUIHelper.EnableActiveBtnEffect(true);
            }
        }

        [CompilerGenerated]
        private sealed class <StartUserGuideActionQueue>c__AnonStorey6
        {
            internal UITaskBase targetUITask;
            internal UserGuideTriggerPoint triggerPointTyp;
            internal int questConfigId;
            internal UIManager.UIActionQueueItem item;

            internal void <>m__0()
            {
                object[] param = new object[] { this.questConfigId };
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.targetUITask, this.triggerPointTyp, param);
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <WaitUntil>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Func<bool> predicate;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.predicate())
                        {
                            this.$PC = -1;
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public enum CostCurrencyType
        {
            None,
            PrivateBindMoney,
            PrivateRealMoney,
            PrivateTradeMoney,
            Credit,
            GuildTradeMoney,
            GuildTacticalPoint,
            GuildInfomationPoint,
            SolarSystemFlourish
        }
    }
}

