﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildFlagShipHangarShipSupplementFuelReqNetTask : NetWorkTransactionTask
    {
        private ulong m_shipHangarInstanseId;
        private List<ProGuildFlagShipHangarShipSupplementFuelInfo> m_fuelSupplementInfoList;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarShipSupplementFuelAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarShipSupplementFuelReqNetTask(ulong shipHangarInstanseId, List<ProGuildFlagShipHangarShipSupplementFuelInfo> fuelSupplementInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarShipSupplementFuelAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

