﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonNewAreaInfoUIController : UIControllerBase
    {
        public GuildLogoController m_logoCtrl;
        [AutoBind("./InfoGroup/ForceGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LogoRoot;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NewAreaReachNoticeCtrl;
        [AutoBind("./InfoGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NewReachStarFieldNameText;
        [AutoBind("./InfoGroup/StationGroup/StationInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemNameText;
        [AutoBind("./InfoGroup/ForceGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FactionNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowNewArriveSolarSystemInfo;
        private static DelegateBridge __Hotfix_ShowSpaceStationInfo;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewArriveSolarSystemInfo(int solarSystemId, Color solarSystemNameColor, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSpaceStationInfo(string stationName, int solarSystemId, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

