﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberUIController : UIControllerBase
    {
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./ToggleGroup/Member", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_memberBtn;
        [AutoBind("./ToggleGroup/Member", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_memberBtnState;
        [AutoBind("./ToggleGroup/StaffingLog", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_staffingLogBtn;
        [AutoBind("./ToggleGroup/StaffingLog", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_staffingLogBtnState;
        [AutoBind("./ToggleGroup/ApplyFor", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_applyFoBtn;
        [AutoBind("./ToggleGroup/ApplyFor", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_applyForBtnState;
        [AutoBind("./ToggleGroup/ApplyFor/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_applyRedPoint;
        [AutoBind("./ToggleGroup/Job", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_jobBtn;
        [AutoBind("./ToggleGroup/Job", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_jobBtnState;
        [AutoBind("./ToggleGroup/Member/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_memberCountText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        public Dictionary<GuildMemberUITask.SubSysType, CommonUIStateController> m_btnStates;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateMemberCount;
        private static DelegateBridge __Hotfix_UpdateToggeState;
        private static DelegateBridge __Hotfix_GetUIProcess;
        private static DelegateBridge __Hotfix_SetApplyRedPoint;

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(string showToggle, UIProcess.ProcessExecMode mode, bool show, bool immediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetApplyRedPoint(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberCount(int totalCount, int onLineCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateToggeState(GuildMemberUITask.SubSysType type)
        {
        }
    }
}

