﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BattlePassMainUIController : UIControllerBase
    {
        public BattlePassMainTabRewardUIController m_TabRewardUIController;
        public BattlePassMainTabChallangeUIController m_TabChallangeUIController;
        public BattlePassMainLevelPurchaseUIController m_LevelPurchaseUIController;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CurrencyType> EventOnAddMoneyClick;
        public TitleMoneyGroupUIController m_IRMoneyCtrl;
        public TitleMoneyGroupUIController m_SMoneyCtrl;
        public TitleMoneyGroupUIController m_CMoneyCtrl;
        [AutoBind("./TitleBGImage/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./TitleBGImage/MoneyGroup/IRMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject IRMoneyGameObj;
        [AutoBind("./TitleBGImage/MoneyGroup/SMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SMoneyGameObj;
        [AutoBind("./TitleBGImage/MoneyGroup/CMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CMoneyGameObj;
        [AutoBind("./Main/ButtonGroup/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RewardButton;
        [AutoBind("./Main/ButtonGroup/RewardButton/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RewardButtonRedImage;
        [AutoBind("./Main/ButtonGroup/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardButtonTabUIState;
        [AutoBind("./Main/ButtonGroup/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChallengeButton;
        [AutoBind("./Main/ButtonGroup/ChallengeButton/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ChallengeButtonRedImage;
        [AutoBind("./Main/ButtonGroup/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ChallengeButtonTabUIState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnAddMoneyBtnClick;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_RedPoint;
        private static DelegateBridge __Hotfix_add_EventOnAddMoneyClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddMoneyClick;

        public event Action<CurrencyType> EventOnAddMoneyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnAddMoneyBtnClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_RedPoint(bool hasAnyAnyLevelReward, bool hasAnyChallangeQuest, bool hasAnyQuestReward)
        {
        }
    }
}

