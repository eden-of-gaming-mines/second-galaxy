﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacterPropertyEffectItemUIController : UIControllerBase
    {
        private const string StateUp = "Up";
        private const string StateDowm = "Down";
        private const string StateNormal = "Normal";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Change", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_changeValueText;
        [AutoBind("./Number", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_valueText;
        private static DelegateBridge __Hotfix_SetEffctItemValue;

        [MethodImpl(0x8000)]
        public void SetEffctItemValue(float value, float newValue)
        {
        }
    }
}

