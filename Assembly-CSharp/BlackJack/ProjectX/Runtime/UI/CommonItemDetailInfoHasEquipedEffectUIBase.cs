﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class CommonItemDetailInfoHasEquipedEffectUIBase : UIControllerBase
    {
        public List<CommonItemDetailOnePropertyUICtroller> DynamicPropertyCtrlList;
        public GameObject PropertyRootGo;
        public GameObject NegativePropertyRootGo;
        public GameObject PropertyItemPrefab;
        private static DelegateBridge __Hotfix_PrepareDynamicPropertyGoList;
        private static DelegateBridge __Hotfix_UpdatePropertyList;
        private static DelegateBridge __Hotfix_UpdatePropertyCompareList;
        private static DelegateBridge __Hotfix_GetActivePropertyItemCtrlByIndex;
        private static DelegateBridge __Hotfix_HidePropertyItemCtrlByIndex;
        private static DelegateBridge __Hotfix_UpdateArrowWithChangeValue_1;
        private static DelegateBridge __Hotfix_UpdateArrowWithChangeValue_0;
        private static DelegateBridge __Hotfix_CreatePropertyItemUIController;

        [MethodImpl(0x8000)]
        private CommonItemDetailOnePropertyUICtroller CreatePropertyItemUIController()
        {
        }

        [MethodImpl(0x8000)]
        protected CommonItemDetailOnePropertyUICtroller GetActivePropertyItemCtrlByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void HidePropertyItemCtrlByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PrepareDynamicPropertyGoList(GameObject propertyRootGo, GameObject negativePropertyRootGo, int count)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateArrowWithChangeValue(GameObject downArrow, GameObject upArrow, int compareValue = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateArrowWithChangeValue(GameObject downArrow, GameObject upArrow, float changeValue = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdatePropertyCompareList(List<CommonPropertyInfo> propertyList, List<CommonPropertyInfo> propertyCompareList)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdatePropertyList(List<CommonPropertyInfo> propertyList)
        {
        }
    }
}

