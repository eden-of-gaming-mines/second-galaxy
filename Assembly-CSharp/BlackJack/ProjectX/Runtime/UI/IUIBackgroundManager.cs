﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using System;

    public interface IUIBackgroundManager
    {
        UIIntent GetManagerIntent();
        void OnLeaveBackground(string popUITaskName);
        void OnReEnterBackground(string popUITaskName);
    }
}

