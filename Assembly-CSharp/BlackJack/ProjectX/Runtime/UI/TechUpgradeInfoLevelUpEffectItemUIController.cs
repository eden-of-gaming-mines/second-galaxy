﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TechUpgradeInfoLevelUpEffectItemUIController : UIControllerBase
    {
        [AutoBind("./TechPropertyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyNameText;
        [AutoBind("./EffectUpGroup/CurrentValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrEffectValueText;
        [AutoBind("./EffectUpGroup/NextValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextEffectValueText;
        private static DelegateBridge __Hotfix_UpdateEffectItemInfo;

        [MethodImpl(0x8000)]
        public void UpdateEffectItemInfo(CommonPropertyInfo currLevelPropertyInfo, CommonPropertyInfo nextLevelPropertyInfo)
        {
        }
    }
}

