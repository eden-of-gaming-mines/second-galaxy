﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BattlePassMainTask : UITaskBase
    {
        private int m_accIndex;
        private int m_comepleteAccCount;
        private int m_totalAccCount;
        private bool m_getAccAll;
        private static int LONG_PRESS_LEVELPURCHASE_COUNT = 3;
        private static int BUY_LEVELPURCHASE_INIT_COUNT = 10;
        private readonly List<QuestRewardInfo> m_tempEnoughItemBuyLevel;
        private readonly List<QuestRewardInfo> m_tempEnoughItemBuyShortCut;
        public int m_CurLevelPurchaseTotalLevelCount;
        private int m_longPressingIntTimer;
        private float m_longPressingIntervalTimer;
        private float m_longPressing3Sec;
        private float m_longPressing5Sec;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private const int WEEK_HOURS = 0xa8;
        protected ConfigDataBattlePassChallangeQuestGroupInfo[] m_challengeQuestGroupConfList;
        protected ConfigDataBattlePassLevelInfo[] m_periodLevelRewardConfList;
        public static int CUR_PERIOD_LEVEL_MAX;
        protected ConfigDataBattlePassChallangeQuestAccRewardInfo[] m_challengeQuestAccRewardConfList;
        private int m_CurChageSelectWeekIndex;
        private BattlePassMainUIController m_MainCtrl;
        public const string ParamKeyTabRewardMode = "TabReard";
        public const string ParamKeyTabChallengeMode = "TabChallenge";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "BattlePassMainTask";
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassLevelInfo>, int> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassLevelInfo>, ConfigDataBattlePassLevelInfo> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo>, int> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo>, ConfigDataBattlePassChallangeQuestGroupInfo> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo>, int> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo>, ConfigDataBattlePassChallangeQuestAccRewardInfo> <>f__am$cache6;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitDataStartOrResum;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewTabRedPoint;
        private static DelegateBridge __Hotfix_TryGetChallengeAccState;
        private static DelegateBridge __Hotfix_GetCurChallangeQuestAccRewardInfoIndex;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_RegiserNetworkEvent;
        private static DelegateBridge __Hotfix_UnRegiserNetworkEvent;
        private static DelegateBridge __Hotfix_OnChallangeQuestGroupChange;
        private static DelegateBridge __Hotfix_ToggleTabClickReward;
        private static DelegateBridge __Hotfix_ToggleTabClickChallange;
        private static DelegateBridge __Hotfix_OnAddMoneyClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnLevelPurchaseCancelButtonClick;
        private static DelegateBridge __Hotfix_OnLevelPurchaseConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnLevelPurchaseBuyLevelChanged;
        private static DelegateBridge __Hotfix_GetLongPressingTimerVal;
        private static DelegateBridge __Hotfix_OnAddLevelPurchaseLongPressStart;
        private static DelegateBridge __Hotfix_OnAddLevelPurchaseLongPressing;
        private static DelegateBridge __Hotfix_OnAddLevelPurchaseLongPressEnd;
        private static DelegateBridge __Hotfix_OnLevelPurchaseAddButtonClick;
        private static DelegateBridge __Hotfix_OnRemoveLevelPurchaseLongPressStart;
        private static DelegateBridge __Hotfix_OnRemoveLevelPurchaseLongPressing;
        private static DelegateBridge __Hotfix_OnRemoveLevelPurchaseLongPressEnd;
        private static DelegateBridge __Hotfix_OnLevelPurchaseRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnLevelPurchasBackButtonClick;
        private static DelegateBridge __Hotfix_GetEnoughItemByServerLevelCount;
        private static DelegateBridge __Hotfix_GetEnoughItemBuLevelPurchasLevel;
        private static DelegateBridge __Hotfix_OnLevelPurchaseRewardItemClick;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnTabRewardLevelPurchaseButonClick;
        private static DelegateBridge __Hotfix_OnTabRewardShortcutButtonGetButtonClick;
        private static DelegateBridge __Hotfix_OnTabRewardDescribeToBuyButtonClick;
        private static DelegateBridge __Hotfix_GetFakeLBStoreItem;
        private static DelegateBridge __Hotfix_OnTabRewardShipDescribeButtonClick;
        private static DelegateBridge __Hotfix_OnTabRewardItemDescribeButtonClick;
        private static DelegateBridge __Hotfix_SendGetBattlePassLevReq;
        private static DelegateBridge __Hotfix_OnTabChallengeNextWeekButtonClick;
        private static DelegateBridge __Hotfix_OnTabChallengeLastWeekButtonClick;
        private static DelegateBridge __Hotfix_OnChallengeGetButtonAccClick;
        private static DelegateBridge __Hotfix_GetRemainGetRemainBattelPassDateTime;
        private static DelegateBridge __Hotfix_GetRemainBattelPassDays;
        private static DelegateBridge __Hotfix_FormatDateTimeStringForBattlePass;
        private static DelegateBridge __Hotfix_GetRemainBattlePassChallengeDayOrHourByWeekIndex;
        private static DelegateBridge __Hotfix_GetBattlePassConfDict;
        private static DelegateBridge __Hotfix_GetBattlePassChallengeInfoByWeek;
        private static DelegateBridge __Hotfix_GetServerBattlePassLevel;
        private static DelegateBridge __Hotfix_IsPlayerFullLevel;
        private static DelegateBridge __Hotfix_GetTrumpItemShip;
        private static DelegateBridge __Hotfix_GetStringTableForBattlePass;
        private static DelegateBridge __Hotfix_get_CurLevelPurchaseTotalBuyCount;
        private static DelegateBridge __Hotfix_get_CurrChallangeQuestGroupInfoMaxIndex;
        private static DelegateBridge __Hotfix_get_CurrPeriodBattlePassLevelMax;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public BattlePassMainTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private static string FormatDateTimeStringForBattlePass(TimeSpan reamainDate)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassChallangeQuestGroupInfo GetBattlePassChallengeInfoByWeek(int weekIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void GetBattlePassConfDict()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurChallangeQuestAccRewardInfoIndex()
        {
        }

        [MethodImpl(0x8000)]
        private List<QuestRewardInfo> GetEnoughItemBuLevelPurchasLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        private int GetEnoughItemByServerLevelCount()
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem GetFakeLBStoreItem(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private int GetLongPressingTimerVal(int dir)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetRemainBattelPassDays()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetRemainBattlePassChallengeDayOrHourByWeekIndex(int weekIndex)
        {
        }

        [MethodImpl(0x8000)]
        public static TimeSpan GetRemainGetRemainBattelPassDateTime()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetServerBattlePassLevel()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringTableForBattlePass(int id)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetTrumpItemShip(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void InitDataStartOrResum(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayerFullLevel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddLevelPurchaseLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddLevelPurchaseLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddLevelPurchaseLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddMoneyClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChallangeQuestGroupChange(int pId, int groupId, DateTime currPeriodStartTime)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChallengeGetButtonAccClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchasBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseBuyLevelChanged(int opCount, bool onlyLvText = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseRemoveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelPurchaseRewardItemClick(FakeLBStoreItem itemInfo, CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveLevelPurchaseLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveLevelPurchaseLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveLevelPurchaseLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabChallengeLastWeekButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabChallengeNextWeekButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabRewardDescribeToBuyButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabRewardItemDescribeButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabRewardLevelPurchaseButonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabRewardShipDescribeButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTabRewardShortcutButtonGetButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void RegiserNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGetBattlePassLevReq(int level, bool isUpgrade, Action<bool, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(FakeLBStoreItem item, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        private void ToggleTabClickChallange(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ToggleTabClickReward(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetChallengeAccState(out int targetIndex, out int curCount, out int totalCount, out bool getAll)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegiserNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewTabRedPoint()
        {
        }

        public int CurLevelPurchaseTotalBuyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected int CurrChallangeQuestGroupInfoMaxIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected int CurrPeriodBattlePassLevelMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetBattlePassConfDict>c__AnonStorey3
        {
            internal ConfigDataBattlePassPeriodInfo periodInfo;

            internal bool <>m__0(KeyValuePair<int, ConfigDataBattlePassLevelInfo> t) => 
                (t.Value.TemplateId == this.periodInfo.LevelTemplateId);

            internal bool <>m__1(KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo> t) => 
                (t.Value.TemplateId == this.periodInfo.ChallangeGroupTemplateId);

            internal bool <>m__2(KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo> t) => 
                (t.Value.TemplateId == this.periodInfo.ChallangeAccRewardTemplateId);
        }

        [CompilerGenerated]
        private sealed class <OnChallengeGetButtonAccClick>c__AnonStorey2
        {
            internal ConfigDataBattlePassChallangeQuestAccRewardInfo info;
            internal BattlePassMainTask $this;

            internal void <>m__0(Task task)
            {
                BattlePassChallangeQuestAccRewardBalanceReqTask task2 = task as BattlePassChallangeQuestAccRewardBalanceReqTask;
                if (task2.IsNetworkError)
                {
                    Debug.LogError("BattlePassChallangeQuestAccRewardBalanceReqTask  : NetworkError  ");
                }
                else if (task2.ReqResult != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.ReqResult, true, false);
                }
                else
                {
                    TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_PickDropBoxItemSuccess, new object[0]), SolarSystemUIHelper.GetDropBoxItemDescStr(StoreItemType.StoreItemType_CrackBox, !this.$this.PlayerCtx.GetLBBattlePass().IsUpgraded() ? this.info.RewardItemId : this.info.UpgradedRewardItemId, 1L)), false);
                    this.$this.EnablePipelineStateMask(BattlePassMainTask.PipeLineStateMaskType.ChallangeAccRewardGet);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnLevelPurchaseConfirmButtonClick>c__AnonStorey0
        {
            internal bool enough;
            internal BattlePassMainTask $this;

            internal void <>m__0()
            {
                if (!this.enough)
                {
                    CommonUITaskHelper.ToAddCurrency(CurrencyType.CurrencyType_RealMoney, this.$this.CurrentIntent, delegate (bool ret) {
                        if (ret)
                        {
                            this.$this.Pause();
                        }
                    });
                }
                else
                {
                    BattlePassLevelPurchaseReqTask task = new BattlePassLevelPurchaseReqTask(this.$this.m_CurLevelPurchaseTotalLevelCount);
                    task.EventOnStop += delegate (Task task) {
                        BattlePassLevelPurchaseReqTask task2 = task as BattlePassLevelPurchaseReqTask;
                        if (task2.IsNetworkError)
                        {
                            Debug.LogError(" OnLevelPurchaseConfirmButtonClick returnTask.IsNetworkError ");
                        }
                        else if (task2.ReqResult != 0)
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.ReqResult, true, false);
                        }
                        else
                        {
                            if ((this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.GetCurrentUIState() == null) || !this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.GetCurrentUIState().StateName.Equals("Close"))
                            {
                                this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.SetToUIStateExtra("Close", false, true, null);
                            }
                            this.$this.EnablePipelineStateMask(BattlePassMainTask.PipeLineStateMaskType.RewardTab);
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                    };
                    task.Start(null, null);
                }
            }

            internal void <>m__1(Task task)
            {
                BattlePassLevelPurchaseReqTask task2 = task as BattlePassLevelPurchaseReqTask;
                if (task2.IsNetworkError)
                {
                    Debug.LogError(" OnLevelPurchaseConfirmButtonClick returnTask.IsNetworkError ");
                }
                else if (task2.ReqResult != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.ReqResult, true, false);
                }
                else
                {
                    if ((this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.GetCurrentUIState() == null) || !this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.GetCurrentUIState().StateName.Equals("Close"))
                    {
                        this.$this.m_MainCtrl.m_LevelPurchaseUIController.PanelUIState.SetToUIStateExtra("Close", false, true, null);
                    }
                    this.$this.EnablePipelineStateMask(BattlePassMainTask.PipeLineStateMaskType.RewardTab);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }

            internal void <>m__2(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGetBattlePassLevReq>c__AnonStorey1
        {
            internal Action<bool, bool> onEnd;
            internal bool isUpgrade;
            internal BattlePassMainTask $this;

            internal void <>m__0(Task task)
            {
                BattlePassRewardBalanceReqTask task2 = task as BattlePassRewardBalanceReqTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false, this.isUpgrade);
                    }
                }
                else if (task2.SetReqResult != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.SetReqResult, true, false);
                }
                else
                {
                    this.$this.UpdateViewTabRedPoint();
                    this.onEnd(true, this.isUpgrade);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            RewardTab,
            ChallengeTab,
            ChallangeWeeklyChanged,
            ChallangeAccRewardGet,
            LevalPurchase,
            PeriodError
        }
    }
}

