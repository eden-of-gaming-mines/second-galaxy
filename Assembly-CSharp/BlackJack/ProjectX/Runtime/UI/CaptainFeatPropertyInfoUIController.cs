﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CaptainFeatPropertyInfoUIController : UIControllerBase
    {
        [AutoBind("./PropertyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyText;
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        private static DelegateBridge __Hotfix_SetFeatPropertyInfo;

        [MethodImpl(0x8000)]
        public void SetFeatPropertyInfo(string propertyName, string value)
        {
        }
    }
}

