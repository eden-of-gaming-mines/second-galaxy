﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class GuildBuildingHudGroupUIController : UIControllerBase
    {
        private const double DistancePowThresholdInGroup = 1E+17;
        private GuildBuildingHudItemUIController m_mainItem;
        private readonly List<GuildBuildingHudItemUIController> m_hudItemList;
        [AutoBind("./HudGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform HudGroup;
        private static DelegateBridge __Hotfix_UpdateBuildingHudItem;
        private static DelegateBridge __Hotfix_IsHudItemMergeable2Group;
        private static DelegateBridge __Hotfix_UpdateMainBuildingPosition;
        private static DelegateBridge __Hotfix_ResetHudGroupCtrl;
        private static DelegateBridge __Hotfix_get_MainHudItemInstanceId;

        [MethodImpl(0x8000)]
        public bool IsHudItemMergeable2Group(GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetHudGroupCtrl(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingHudItem(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo, EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMainBuildingPosition(Vector3 pos)
        {
        }

        public ulong MainHudItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

