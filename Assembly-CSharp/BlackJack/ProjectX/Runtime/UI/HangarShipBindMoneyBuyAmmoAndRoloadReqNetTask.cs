﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask : NetWorkTransactionTask
    {
        private int m_shipHangarIndex;
        private int m_equipSlotIndex;
        public int m_ammoAddCount;
        private int m_ackResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnHangarShipBindMoneyBuyAmmoAndRoloadAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;

        [MethodImpl(0x8000)]
        public HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask(int hangarShipIndex, int equipSlotIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHangarShipBindMoneyBuyAmmoAndRoloadAck(int result, int slotIndex, int ammoAddCount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

