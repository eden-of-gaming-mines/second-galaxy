﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class DiplomacySortFilterUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DiplomacyItemType> EventOnSortFilterChanged;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        private bool m_isSortFilterPanelShow;
        private DiplomacyItemType m_currFilter;
        private const DiplomacyItemType AllFilter = (DiplomacyItemType.Alliance | DiplomacyItemType.Guild | DiplomacyItemType.Player);
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./All", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sortAllButton;
        [AutoBind("/Player", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sortPlayerButton;
        [AutoBind("./Guild", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sortGuildButton;
        [AutoBind("./Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sortAllianceButton;
        [AutoBind("./All", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortAllButtonStateCtrl;
        [AutoBind("/Player", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortPlayerButtonStateCtrl;
        [AutoBind("./Guild", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortGuildButtonStateCtrl;
        [AutoBind("./Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortAllianceButtonStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetSortFilterPanelUIProcess;
        private static DelegateBridge __Hotfix_HasItemFilterType;
        private static DelegateBridge __Hotfix_IsSortFilterPanelShow;
        private static DelegateBridge __Hotfix_SetAllFilterButtonState;
        private static DelegateBridge __Hotfix_SetNewFilterByChangingFilter;
        private static DelegateBridge __Hotfix_GetFilter;
        private static DelegateBridge __Hotfix_OnAllButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerButtonClick;
        private static DelegateBridge __Hotfix_OnGuildButtonClick;
        private static DelegateBridge __Hotfix_OnAllianceButtonClick;
        private static DelegateBridge __Hotfix_SetSingleFilterButtonState;
        private static DelegateBridge __Hotfix_add_EventOnSortFilterChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSortFilterChanged;

        public event Action<DiplomacyItemType> EventOnSortFilterChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public DiplomacyItemType GetFilter()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSortFilterPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasItemFilterType(DiplomacyItemType sourceFilterType, DiplomacyItemType containFilterType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSortFilterPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllFilterButtonState(DiplomacyItemType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetNewFilterByChangingFilter(DiplomacyItemType filterType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSingleFilterButtonState(DiplomacyItemType filterType, bool isOn)
        {
        }
    }
}

