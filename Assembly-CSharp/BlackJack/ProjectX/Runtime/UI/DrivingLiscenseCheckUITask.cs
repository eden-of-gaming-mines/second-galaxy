﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class DrivingLiscenseCheckUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private AllowInShipGroupUIController m_allowInShipCtrl;
        private DrivingLiscenseCheckUIController m_mainCtrl;
        private DrivingAbilityItemListUIController m_drivingAbiltyCtrl;
        private LBPassiveSkill m_lbskillInfo;
        private ConfigDataQuestInfo m_questConfigInfo;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        public static string ParamKey_ID;
        public static string ParamKey_Level;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "DrivingLiscenseCheckUITask";
        private LBProcessingQuestBase m_userGuideQuestInfo;
        private GuideState m_currGuideState;
        private const string ParamKey_UserGuideQuestInfo = "UserGuideQuestInfo";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartDrivingLiscenseCheckUITask;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnCheckButtonClick;
        private static DelegateBridge __Hotfix_QuestStrike;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnSkipButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnAllowInShipTypeButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnDrivingLicenseUpgradeNtf;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartDrivingLiscenseCheckUITaskForNotForceGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;

        [MethodImpl(0x8000)]
        public DrivingLiscenseCheckUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCheckButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrivingLicenseUpgradeNtf(int skillID, int skillLevel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl, bool isRecommondItem)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void QuestStrike(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, bool showResource, ItemSimpleInfoUITask.PositionType posType = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static Task StartDrivingLiscenseCheckUITask(UIIntent intent, int id, int level, Action<bool> onEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static DrivingLiscenseCheckUITask StartDrivingLiscenseCheckUITaskForNotForceGuide(UIIntent intent, int id, int level, LBProcessingQuestBase questInfo, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private enum GuideState
        {
            None,
            Start,
            StartCheck
        }
    }
}

