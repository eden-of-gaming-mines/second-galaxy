﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ChatUITask : UITaskBase
    {
        private const int MaxSendTextLength = 300;
        private const int MaxMsgLength = 100;
        public const string ParamsKeyGameUserId = "WisperPlayerGameUserIdKey";
        private bool m_isTeamCacheChange;
        public static ChatChannel m_currChannel;
        public static ChatLanguageChannel m_currLanguageChannel;
        private bool m_isNeedTeamNoJoinTip;
        private bool m_isNeedGuildNoJoinTip;
        private bool m_isNeedAllianceNoJoinTip;
        private bool m_isTriggerChatChannelToggleEvent;
        private bool m_isToggleTriggerNeedClearBGChatCache;
        private bool m_chatListNeedRefreshNextTick;
        private bool m_isEnterAppointChannel;
        private ChatChannel m_appointChatChannel;
        private CharacterSimpleInfoUITask m_charSimpleInfoUITask;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        public ChatUIController m_mainCtrl;
        public ChatChannelToggleUIController m_chatChannelToggleCtrl;
        public ChatScrollViewWindowUIController m_scrollViewCtrl;
        public ChatBGUITask m_chatBGUITask;
        private IUIBackgroundManager m_backgroundManager;
        public static string m_paramsKeyChatChannelLangue;
        public static string m_paramsKeyIsDataInIntent;
        public static string m_paramsKeyAppointChannel;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string m_chatUIPrefabAssetPath;
        private readonly Dictionary<ChatChannel, ChatInfo> m_lastChatInfoOnCurrentPipeline;
        public const string TaskName = "ChatUITask";
        private bool m_isCancelRecord;
        private bool m_isSoundRecordButtonDown;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartChatUITaskNormal;
        private static DelegateBridge __Hotfix_StartChatUITaskToEnterAppointChatChannel;
        private static DelegateBridge __Hotfix_StartChatUITaskAndResetToNormalState;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_PlayChatVoiceAnimation;
        private static DelegateBridge __Hotfix_AddSysMessageToAppointChannel;
        private static DelegateBridge __Hotfix_GetChatLanguageChannelBySdk;
        private static DelegateBridge __Hotfix_UpdateChatListView;
        private static DelegateBridge __Hotfix_RefreshChatListViewWithCurrData;
        private static DelegateBridge __Hotfix_OnLinkChatItemClick;
        private static DelegateBridge __Hotfix_OnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_OnTranslateButtonClick;
        private static DelegateBridge __Hotfix_OnAudio2StringButtonClick;
        private static DelegateBridge __Hotfix_OnAudioItemTranslateButtonClick;
        private static DelegateBridge __Hotfix_OnMoveOutButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnNewMsgTipPanelClick;
        private static DelegateBridge __Hotfix_OnSendButtonClick;
        private static DelegateBridge __Hotfix_OnAutoPlayToggleChange;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange;
        private static DelegateBridge __Hotfix_OnInputEndEidt;
        private static DelegateBridge __Hotfix_AddNotJoinedNotice;
        private static DelegateBridge __Hotfix_OnTeamInviteConfirmAck;
        private static DelegateBridge __Hotfix_OnTeamCreateAck;
        private static DelegateBridge __Hotfix_OnTeamLeaveAck;
        private static DelegateBridge __Hotfix_OnTeamMemberAddNtf;
        private static DelegateBridge __Hotfix_OnTeamMemberLeaveNtf;
        private static DelegateBridge __Hotfix_OnMisbehaviorReportAck;
        private static DelegateBridge __Hotfix_OnWisperChatNtf;
        private static DelegateBridge __Hotfix_OnStringTranslateComplete;
        private static DelegateBridge __Hotfix_OnAudio2StringTranslateComplete;
        private static DelegateBridge __Hotfix_SendTextChatReq;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_ChatButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_PanelClose;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_EnableUIInput_0;
        private static DelegateBridge __Hotfix_EnableUIInput_1;
        private static DelegateBridge __Hotfix_AddSysMsgToPointChannel;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_RegisteTranslateEvent;
        private static DelegateBridge __Hotfix_UnRegisteTranslateEvent;
        private static DelegateBridge __Hotfix_SetInputTipText;
        private static DelegateBridge __Hotfix_HandleTextString;
        private static DelegateBridge __Hotfix_SetAutoPlayToggleAndTextState;
        private static DelegateBridge __Hotfix_GetUnReadChatMsgCnt;
        private static DelegateBridge __Hotfix_IsSwitchToTargetChannel;
        private static DelegateBridge __Hotfix_IsCacheChange;
        private static DelegateBridge __Hotfix_SetChatSeqValue;
        private static DelegateBridge __Hotfix_UpdateVoiceItemAnimation;
        private static DelegateBridge __Hotfix_UpdateUnReadToggleTip;
        private static DelegateBridge __Hotfix_GenerateShipCustomTemplateInfoFromChatItem;
        private static DelegateBridge __Hotfix_SetDefaultLangueChannel;
        private static DelegateBridge __Hotfix_get_LbChat;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StopCurrPlayVoiceAnimation;
        private static DelegateBridge __Hotfix_GetUnReadVoiceListUnder4AppointChatMsg;
        private static DelegateBridge __Hotfix_OnSoundChatItemClick;
        private static DelegateBridge __Hotfix_OnSoundButtonDown;
        private static DelegateBridge __Hotfix_OnSoundButtonUp;
        private static DelegateBridge __Hotfix_OnSoundButtonMoveUp;
        private static DelegateBridge __Hotfix_get_IsSoundRecordButtonDown;
        private static DelegateBridge __Hotfix_GetVoiceContentAndPlayVoice;
        private static DelegateBridge __Hotfix_GetVoiceContent;
        private static DelegateBridge __Hotfix_PlayPlayerVoice;
        private static DelegateBridge __Hotfix_OnPlayerVoiceStart;
        private static DelegateBridge __Hotfix_StopCurrPlayerVoice;
        private static DelegateBridge __Hotfix_StartNextPlayerVoice;

        [MethodImpl(0x8000)]
        public ChatUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool AddNotJoinedNotice(ChatChannel channel, ChatLanguageChannel languageChannel, string text, ref bool needDisplayMask)
        {
        }

        [MethodImpl(0x8000)]
        public static void AddSysMessageToAppointChannel(ChatChannel channel, string sysStr, bool isNeedTip)
        {
        }

        [MethodImpl(0x8000)]
        private void AddSysMsgToPointChannel(ChatChannel channel, string sysStr, bool isNeedTip)
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop()
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_ChatButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_PanelClose()
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable, bool isGlobalEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipCustomTemplateInfo GenerateShipCustomTemplateInfoFromChatItem(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLanguageChannel GetChatLanguageChannelBySdk()
        {
        }

        [MethodImpl(0x8000)]
        private int GetUnReadChatMsgCnt(List<ChatInfo> chatList, int unReadMsgCntWithMe)
        {
        }

        [MethodImpl(0x8000)]
        public List<ChatContentVoice> GetUnReadVoiceListUnder4AppointChatMsg(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void GetVoiceContent(ChatContentVoice chatInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void GetVoiceContentAndPlayVoice(ChatContentVoice chatInfo, Action onStartPlayVoice = null)
        {
        }

        [MethodImpl(0x8000)]
        private string HandleTextString(string str)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCacheChange(ChatChannel chatChannel, ChatLanguageChannel channelLangue)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSwitchToTargetChannel(ChatChannel targetChannel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAudio2StringButtonClick(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAudio2StringTranslateComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAudioItemTranslateButtonClick(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoPlayToggleChange(bool isAutoPlay)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange(ChatChannel chatChannel, ChatLanguageChannel chatLangueChannel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInputEndEidt(string inputContent)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLinkChatItemClick(UIControllerBase obj, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMisbehaviorReportAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMoveOutButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewMsgTipPanelClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerHeadButtonClick(UIControllerBase uCtrl, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerVoiceStart(ChatContentVoice chatInfo, ChatVoiceItemUIController voiceCtrl, bool isReaded)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonDown()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonMoveUp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonUp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundChatItemClick(UIControllerBase obj, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStringTranslateComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamCreateAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamInviteConfirmAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamLeaveAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberAddNtf(string gameUserId, string playerName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberLeaveNtf(string gameUserId, string playerName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTranslateButtonClick(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWisperChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayChatVoiceAnimation(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayPlayerVoice(ChatContentVoice chatInfo, Action onStartPlayVoice = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshChatListViewWithCurrData()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisteTranslateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendTextChatReq()
        {
        }

        [MethodImpl(0x8000)]
        private void SetAutoPlayToggleAndTextState(ChatChannel chatChannel)
        {
        }

        [MethodImpl(0x8000)]
        private void SetChatSeqValue(ChatChannel chatChannel, ChatLanguageChannel channelLangue)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDefaultLangueChannel()
        {
        }

        [MethodImpl(0x8000)]
        private void SetInputTipText()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartChatUITaskAndResetToNormalState(ChatChannel channel, CharactorObserverAck gameUserInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatUITask StartChatUITaskNormal(UIIntent returnToIntent, bool isBringToTop = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatUITask StartChatUITaskToEnterAppointChatChannel(ChatChannel channel, UIIntent returnToIntent, CharactorObserverAck gameUserInfo = null, bool isBringToTop = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartNextPlayerVoice(ChatContentVoice chatInfo, ChatVoiceItemUIController voiceCtrl, bool isReaded)
        {
        }

        [MethodImpl(0x8000)]
        private void StopCurrPlayerVoice(ChatContentVoice chatInfo, ChatVoiceItemUIController voiceCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCurrPlayVoiceAnimation()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisteTranslateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChatListView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnReadToggleTip()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVoiceItemAnimation()
        {
        }

        private LogicBlockChatClient LbChat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsSoundRecordButtonDown
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetVoiceContent>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal ChatContentVoice chatInfo;

            internal void <>m__0(Task task)
            {
                GetVoiceContentReqNetTask task2 = task as GetVoiceContentReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    bool flag = false;
                    foreach (ChatContentVoice voice in task2.VoiceContentList)
                    {
                        if (voice.m_instanceId == this.chatInfo.m_instanceId)
                        {
                            this.chatInfo.m_voice = voice.m_voice;
                            this.chatInfo.m_audioFrequency = voice.m_audioFrequency;
                            this.chatInfo.m_sampleLength = voice.m_sampleLength;
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        this.chatInfo.m_isOverdued = true;
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(flag);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetVoiceContentAndPlayVoice>c__AnonStorey3
        {
            internal ChatContentVoice chatInfo;
            internal Action onStartPlayVoice;
            internal ChatUITask $this;

            internal void <>m__0(Task task)
            {
                GetVoiceContentReqNetTask task2 = task as GetVoiceContentReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    bool flag = false;
                    foreach (ChatContentVoice voice in task2.VoiceContentList)
                    {
                        if (voice.m_instanceId == this.chatInfo.m_instanceId)
                        {
                            this.chatInfo.m_voice = voice.m_voice;
                            this.chatInfo.m_audioFrequency = voice.m_audioFrequency;
                            this.chatInfo.m_sampleLength = voice.m_sampleLength;
                            flag = true;
                            break;
                        }
                    }
                    if (flag)
                    {
                        this.$this.PlayPlayerVoice(this.chatInfo, this.onStartPlayVoice);
                    }
                    else
                    {
                        this.chatInfo.m_isOverdued = true;
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnAudio2StringButtonClick>c__AnonStorey2
        {
            internal ChatContentVoice audioChat;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    Debug.LogError("OnAudio2StringButtonClick GetVoiceContent failed");
                }
                else if (!this.audioChat.m_isAudio2String)
                {
                    if (this.audioChat.m_voice != null)
                    {
                        this.audioChat.m_isAudio2String = true;
                        this.audioChat.m_audioMd5 = Util.CreateMd5FromBytes(this.audioChat.m_voice);
                        if (!string.IsNullOrEmpty(this.audioChat.m_audioMd5))
                        {
                            TranslateManager.Instance.TranslateAudio2String(this.audioChat.m_audioMd5, this.audioChat.m_voice, this.audioChat.m_sampleLength, this.audioChat.m_languageType);
                        }
                    }
                }
                else if (this.audioChat.m_voice != null)
                {
                    if (TranslateManager.Instance.IsInAudio2String(this.audioChat.m_audioMd5))
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_Audio2StringProcessing, false, new object[0]);
                    }
                    else if (TranslateManager.Instance.IsAudio2StringFailed(this.audioChat.m_audioMd5))
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_Audio2StringFailed, false, new object[0]);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartChatUITaskAndResetToNormalState>c__AnonStorey0
        {
            internal ChatChannel channel;
            internal CharactorObserverAck gameUserInfo;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    SolarSystemUITask task = UIManager.Instance.FindUITaskWithName("SolarSystemUITask", true) as SolarSystemUITask;
                    if (task != null)
                    {
                        task.StartChatUITask(this.channel, this.gameUserInfo);
                    }
                }
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    SpaceStationUITask task = UIManager.Instance.FindUITaskWithName("SpaceStationUITask", true) as SpaceStationUITask;
                    if (task != null)
                    {
                        task.OpenChatUITask(this.channel, this.gameUserInfo);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNextPlayerVoice>c__AnonStorey5
        {
            internal ChatContentVoice chatInfo;
            internal ChatVoiceItemUIController voiceCtrl;
            internal bool isReaded;
            internal ChatUITask $this;

            internal void <>m__0()
            {
                this.$this.OnPlayerVoiceStart(this.chatInfo, this.voiceCtrl, this.isReaded);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey1
        {
            internal bool isScroll;
            internal ChatUITask $this;

            internal void <>m__0()
            {
                this.$this.m_chatChannelToggleCtrl.SwitchToAppointChannel(ChatUITask.m_currChannel, ChatUITask.m_currLanguageChannel);
                this.$this.m_isTriggerChatChannelToggleEvent = true;
            }

            internal void <>m__1()
            {
                ChatInfo lastChatInfo = !this.$this.m_lastChatInfoOnCurrentPipeline.ContainsKey(ChatUITask.m_currChannel) ? null : this.$this.m_lastChatInfoOnCurrentPipeline[ChatUITask.m_currChannel];
                this.$this.m_scrollViewCtrl.UpdateChatListUIInfo(this.$this.LbChat.GetChatInfoList(ChatUITask.m_currChannel), ChatUITask.m_currLanguageChannel, lastChatInfo, this.$this.m_dynamicResCacheDict, this.isScroll);
            }
        }

        public enum MaskType
        {
            ScrollViewReflesh = 1,
            Init = 2,
            Resume = 3,
            ChatChannelChange = 4,
            TranslateString = 5
        }
    }
}

