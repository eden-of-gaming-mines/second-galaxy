﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeChooseCaptainUIController : UIControllerBase
    {
        private int m_techId;
        private int m_blueprintId;
        private string m_currMode;
        private int m_selectedItemIndex;
        private List<LBStaticHiredCaptain> m_captainList;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private Dictionary<ulong, Turple<float, float, float>> m_captainAssistEffectValueDict;
        private string m_captainListUIItemPoolName;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCaptainInfoUIItemClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CaptainChoosePanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./CaptainChoosePanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./CaptainChoosePanel/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeTechNameText;
        [AutoBind("./CaptainChoosePanel/CaptainChooseScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect CaptainListScrollRect;
        [AutoBind("./CaptainChoosePanel/CaptainChooseScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool CaptainListItemPool;
        [AutoBind("./CaptainChoosePanel/CaptainChooseScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainListViewContent;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateAssistCaptainInfo;
        private static DelegateBridge __Hotfix_UpdateCaptainItemSelectState;
        private static DelegateBridge __Hotfix_OnCaptainInfoItemClick;
        private static DelegateBridge __Hotfix_OnCaptainInfoItemFill;
        private static DelegateBridge __Hotfix_OnNewItemCreateInPool;
        private static DelegateBridge __Hotfix_add_EventOnCaptainInfoUIItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainInfoUIItemClick;

        public event Action<int> EventOnCaptainInfoUIItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainInfoItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainInfoItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewItemCreateInPool(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAssistCaptainInfo(string mode, int id, List<LBStaticHiredCaptain> captainList, Dictionary<ulong, Turple<float, float, float>> captainAssistEffectValueDict, Dictionary<string, UnityEngine.Object> resDict, int startIdx = 0, int selIdx = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainItemSelectState(int selIdx)
        {
        }
    }
}

