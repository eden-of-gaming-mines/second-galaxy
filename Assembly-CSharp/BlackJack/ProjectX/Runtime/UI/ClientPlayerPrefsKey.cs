﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;

    public class ClientPlayerPrefsKey
    {
        public const string LoginSessionOutTimeCountName = "LoginSessionOutTimeCount";
        public const string KillRecordeRedPointName = "KillRecordeRedPointState";
        public const string BeKilledRecordeRedPointName = "BeKilledRecordeRedPointName";
        public const string GuildWarButtonRedPointName = "GuildWarButtonRedPointName";
        public const string UseCachedCameraOffsetZ = "UseCachedCameraOffsetZ";
        public const string CachedCameraOffsetZ = "CachedCameraOffsetZ";
        public const string GuildBattleSimpleReportInfoLastOneId = "GuildBattleSimpleReportInfoLastOneID";
        public const string BackgroundMusicPrefsKey = "BackgroundMusicState";
        public const string SoundEffectPrefsKey = "SoundEffectState";
        public const string UseLowFrameRateSetting = "UseLowFrameRateSetting";
        public const string GraphicQualityLevel = "GraphicQualityLevel";
        public const string KeyScreenRecordOpen = "ScreenRecordOpen";
        public const string GraphicEffectLevel = "GraphicEffectLevel";
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

