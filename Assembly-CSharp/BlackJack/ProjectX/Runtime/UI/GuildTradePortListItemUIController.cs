﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildTradePortListItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private GuildTradePortExtraInfo m_guildTradePortExtraInfo;
        [AutoBind("./PlanIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PlanIconImageUICtrl;
        [AutoBind("./OrderIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController OrderIconImageCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./TradePortNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradePortNameText;
        [AutoBind("./TradePortLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradePortLevelText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateTradePortInfo;
        private static DelegateBridge __Hotfix_SetIsSelected;
        private static DelegateBridge __Hotfix_GetPortSolarSystemId;
        private static DelegateBridge __Hotfix_SetPlanAndOrderIconImage;
        private static DelegateBridge __Hotfix_GetStateStr;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetPortSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        private string GetStateStr(bool isSelect, bool isOnGoing)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsSelected(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlanAndOrderIconImage(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradePortInfo(GuildTradePortExtraInfo guildTradePortExtraInfo)
        {
        }
    }
}

