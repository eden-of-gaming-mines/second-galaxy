﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class NpcShopMainUITask : UITaskBase
    {
        private NpcShopItemCategory m_guideItemCategory;
        private int m_guideItemType;
        private NpcShopItemCategoryToggleUIController m_guideCategoryController;
        public const string NpcShopMainUITaskMode_Buy = "NpcShopMainUITaskMode_Buy";
        public const string NpcShopMainUITaskMode_SellFromShipStore = "NpcShopMainUITaskMode_SellFromShipStore";
        public const string NpcShopMainUITaskMode_SellFromItemStore = "NpcShopMainUITaskMode_SellFromItemStore";
        private DateTime m_nextShopItemClickCanTriggerTime;
        private const float m_shopItemClickEventTriggerTimeSpan = 0.2f;
        private NpcShopMainUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private Dictionary<NpcShopItemCategory, List<int>> m_categoryToTypeDict;
        private Dictionary<NpcShopItemCategory, long> m_categoryToItemCountDict;
        private Dictionary<int, NpcShopItemCategory> m_typeToCategoryDict;
        private bool m_isNeedClearCacheOnPause;
        private NpcShopItemCategory m_selectShopCategoryForBuy;
        private NpcShopItemType m_selectShopTypeForBuy;
        private NpcShopItemCategory m_selectShopCategoryForShipStore;
        private NpcShopItemType m_selectShopTypeForShipStore;
        private NpcShopItemCategory m_selectShopCategoryForItemStore;
        private NpcShopItemType m_selectShopTypeForItemStore;
        private List<NpcShopItemUIInfo> m_currSelectedItemList;
        private List<NpcShopItemUIInfo> m_currSelectedBuyItemList;
        private List<NpcShopItemUIInfo> m_currSelectSellFromItemStoreList;
        private List<NpcShopItemUIInfo> m_currSelectSellFromShipStoreList;
        private List<NpcShopItemUIInfo> m_currShopItemList;
        private List<NpcShopItemUIInfo> m_currShopAutomaticItemList;
        private bool m_needTriggerByOrSellToggleEvent;
        private bool m_needTriggerShopTypeItemToggleEvent;
        private bool m_isShowAutomaticSellPanel;
        private bool m_isSaleTradeLegalItem;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKeyNpcShopCatagary = "NpcShopCatagary";
        public const string ParamKeyNpcShopType = "NpcShopType";
        public const string ParamKeyIsTradeSaleShop = "IsTradeSaleShop";
        public const string ParamKeySaleTradeLegalItem = "IsTradeSaleLegal";
        public const string ParamKeyIsNeedAutomaticSell = "IsNeedAutomaticSell";
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        private const int ShowBuyShipConfirmWndWithoutDrvingLisceneLevel = 20;
        private bool m_isTradeSaleShop;
        private string m_npcShopName;
        private const int m_cleanSelectedItemListMaskIndex = 1;
        private const int m_refreshBuyOrSellItemListMaskIndex = 2;
        private const int m_updateViewBuyOrSellTogglesMaskIndex = 3;
        private const int m_updateViewCategoryTogglesMaskIndex = 4;
        private const int m_updateViewSelectedItemListMaskIndex = 6;
        private const int m_refreshCurrSelectedItemTypeMaskIndex = 7;
        private const int m_ignoreAllPipeLineUpdate = 8;
        private List<NpcShopItemFilter> m_canSelectFilterList;
        private bool m_isSortListPanelOpen;
        private int m_selectFilterCount;
        private NpcShopSortType m_sortState;
        private List<NpcShopItemFilter> m_filterList;
        private bool[] m_allfilterToggleStateList;
        public const string TaskName = "NpcShopMainUITask";
        [CompilerGenerated]
        private static Predicate<NpcShopItemFilter> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<NpcShopItemFilter> <>f__am$cache1;
        [CompilerGenerated]
        private static Predicate<NpcShopItemFilter> <>f__am$cache2;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetFirstItemPathString;
        private static DelegateBridge __Hotfix_GuideStepOne;
        private static DelegateBridge __Hotfix_GuideStepTwo;
        private static DelegateBridge __Hotfix_OpenNpcShopWithoutDialog_0;
        private static DelegateBridge __Hotfix_OpenNpcShopWithoutDialog_1;
        private static DelegateBridge __Hotfix_StartNpcShopMainUITaskWithOutDialog_Defult;
        private static DelegateBridge __Hotfix_StartNpcShopMainUITaskWithOutDialog_TradeSale;
        private static DelegateBridge __Hotfix_StartNpcShopMainUITaskWithFixedType;
        private static DelegateBridge __Hotfix_StartNpcShopMainUITask_Defult;
        private static DelegateBridge __Hotfix_StartNpcShopMainUITask_TradeSale;
        private static DelegateBridge __Hotfix_GetShopNpc;
        private static DelegateBridge __Hotfix_SendInStationNpcDialogStartReq;
        private static DelegateBridge __Hotfix_IsDefultNpcShopBuyMode;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateSelectCategoryAndType;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateCategoryAndType;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateCategoryAndTypeForBuyMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateCategoryAndTypeForSellMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_BuyMode;
        private static DelegateBridge __Hotfix_GetStoreItemAlreadyHaveCount;
        private static DelegateBridge __Hotfix_GetShipItemAlreadyHaveCount;
        private static DelegateBridge __Hotfix_UpdateDataCache_SellFromShipStore;
        private static DelegateBridge __Hotfix_UpdateDataCache_SellFromItemStore;
        private static DelegateBridge __Hotfix_UpdateDataCache_TradeSellFromShipStore;
        private static DelegateBridge __Hotfix_UpdateDataCache_TradeSellFromItemStore;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateCategoryCount;
        private static DelegateBridge __Hotfix_UpdateDataCache_CleanAllSelectedItem;
        private static DelegateBridge __Hotfix_UpdateDataCache_SortAndFilter;
        private static DelegateBridge __Hotfix_UpdateDataCache_AutomaticItemSotre;
        private static DelegateBridge __Hotfix_UpdateDataCache_AutomaticShipSotre;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_CurrenyAndFaction;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_TradeItemList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_ItemList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AutomaticItemList;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_WndTitleText;
        private static DelegateBridge __Hotfix_UpdateView_CurrencyCount;
        private static DelegateBridge __Hotfix_UpdateView_BuyAndSellToggles;
        private static DelegateBridge __Hotfix_UpdateView_SellTogglesRedPoint;
        private static DelegateBridge __Hotfix_UpdateView_CategoryToggleGroup;
        private static DelegateBridge __Hotfix_UpdateView_UpdateCategoryItemCount;
        private static DelegateBridge __Hotfix_UpdateView_BuyOrSellItemList;
        private static DelegateBridge __Hotfix_UpdateView_SelectedItemList;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_UpdateView_AutomaticSell;
        private static DelegateBridge __Hotfix_OnPriceSortButtonClick;
        private static DelegateBridge __Hotfix_OnSizeSortButtonClick;
        private static DelegateBridge __Hotfix_OnSubRankSortButtonClick;
        private static DelegateBridge __Hotfix_OnSortStateChanged;
        private static DelegateBridge __Hotfix_OnSortListButtonClick;
        private static DelegateBridge __Hotfix_OnFilterToggleValueChanged;
        private static DelegateBridge __Hotfix_OnSortListBgButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemToggleSelected;
        private static DelegateBridge __Hotfix_OnItemStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_OnShipStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_OnCategoryToggle_ItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnCategoryToggle_ItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_NpcShopItemClick;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_NpcShopItemLongPressStart;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_NpcShopItemIconClick;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_NpcShopItemIcon3DTouch;
        private static DelegateBridge __Hotfix_OnNpcShipItemList_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_NpcShopItemDoubleClick;
        private static DelegateBridge __Hotfix_OnNpcShopItemList_ScrollViewClick;
        private static DelegateBridge __Hotfix_OnSelectedItemList_ItemIconClick;
        private static DelegateBridge __Hotfix_OnSelectedItemList_BuyOrSellButtonClick;
        private static DelegateBridge __Hotfix_OnSelectedItemList_ScrollViewClick;
        private static DelegateBridge __Hotfix_OnBuyItemConfirmWnd_CancelButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemConfirmWnd_ConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnNpcShopBuyOrSellWnd_ConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnAddMoneyClick;
        private static DelegateBridge __Hotfix_OnAutoSaleComfimClick;
        private static DelegateBridge __Hotfix_OnAutoSaleItemClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_SetSelectShopTypeFromIntent;
        private static DelegateBridge __Hotfix_GetNpcShopItemBindInfo;
        private static DelegateBridge __Hotfix_NpcShopItemListFilter;
        private static DelegateBridge __Hotfix_GetNpcShopItemFilterType;
        private static DelegateBridge __Hotfix_NpcShopItemListSortCompare;
        private static DelegateBridge __Hotfix_NpcShopItemSortById;
        private static DelegateBridge __Hotfix_NpcShopItemSortBySubRanktype;
        private static DelegateBridge __Hotfix_NpcShopItemSortByPickSize;
        private static DelegateBridge __Hotfix_NpcShopItemSortByPrice;
        private static DelegateBridge __Hotfix_GetDefaultSellCount;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_GetNpcShopName;
        private static DelegateBridge __Hotfix_NeedShowTechDissatisfyObject;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForInit;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForModeChanged;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForSelectedItemTypeChanged;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForSelectedItemListChanged;
        private static DelegateBridge __Hotfix_SetPipelineCategoryTogglesMaskChanged;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForBuyShopItems;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForAutoSellEnd;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForNpcShopBuyItemReq;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForShipStoreItemSaleReq;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForShipStoreItemSaleReqImp;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForStoreItemBatchSaleReq;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForStoreItemBatchSaleReqImp;
        private static DelegateBridge __Hotfix_ExceNetTaskProcessForTradeNpcShopSale;
        private static DelegateBridge __Hotfix_ExecNetTaskProcessForItemAutoSaleReq;
        private static DelegateBridge __Hotfix_GetPlayerSelectedItem;
        private static DelegateBridge __Hotfix_AddPlayerSelectedItemWithoutUpdateView;
        private static DelegateBridge __Hotfix_UpdatePlayerSelectedItemView;
        private static DelegateBridge __Hotfix_AddPlayerSelectedItem;
        private static DelegateBridge __Hotfix_RemovePlayerSelectedItem;
        private static DelegateBridge __Hotfix_GetCurrencyTypeForCurrUIState;
        private static DelegateBridge __Hotfix_GetGrandFactionTypeForCurrUIState;
        private static DelegateBridge __Hotfix_GetTotalCostMoneyForSelectedItemList;
        private static DelegateBridge __Hotfix_GetTotalCostMoneyForAutomaticSaleItemList;
        private static DelegateBridge __Hotfix_GetTotalOccupySpaceForSelectedItemList;
        private static DelegateBridge __Hotfix_SetCurrModeSelectCategory;
        private static DelegateBridge __Hotfix_GetCurrModeSelectCategory;
        private static DelegateBridge __Hotfix_SetCurrModeSelectType;
        private static DelegateBridge __Hotfix_GetCurrModeSelectType;
        private static DelegateBridge __Hotfix_ShowAddItemTipWindow;
        private static DelegateBridge __Hotfix_CheckShowBuyShipConfirmBox;
        private static DelegateBridge __Hotfix_CheckItemCanSaleInNpcShop;
        private static DelegateBridge __Hotfix_CreateNpcShopItemFromItemStore;
        private static DelegateBridge __Hotfix_CreateNpcShopItemFromShipStore;
        private static DelegateBridge __Hotfix_CreateNpcShopItemFromShipStoreWithID;
        private static DelegateBridge __Hotfix_CreateNpcShopItemFromItemStoreWithID;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBNpcShopClient;

        [MethodImpl(0x8000)]
        public NpcShopMainUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddPlayerSelectedItem(NpcShopItemUIInfo itemInfo, long itemCount)
        {
        }

        [MethodImpl(0x8000)]
        private void AddPlayerSelectedItemWithoutUpdateView(NpcShopItemUIInfo itemInfo, long itemCount)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckItemCanSaleInNpcShop(StoreItemType itemType, object configInfo, int itemId, out float itemPrice, bool checkItemShopType = false, NpcShopItemType currSelectedShopItemType = 0)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShowBuyShipConfirmBox(StoreItemType itemType, int itemId, Action onConfirm, Action onCancel, out bool needShowNoDrvingLicensePostfix)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AutomaticItemList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_CurrenyAndFaction(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_ItemList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_TradeItemList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemUIInfo CreateNpcShopItemFromItemStore(LBStoreItemClient itemInfo, float itemPrice)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemUIInfo CreateNpcShopItemFromItemStoreWithID(int itemID, float itemPrice)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemUIInfo CreateNpcShopItemFromShipStore(LBShipStoreItemBase itemInfo, float itemPrice)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemUIInfo CreateNpcShopItemFromShipStoreWithID(int itemID, float price)
        {
        }

        [MethodImpl(0x8000)]
        private void ExceNetTaskProcessForTradeNpcShopSale(Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForItemAutoSaleReq(Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForNpcShopBuyItemReq(Action onBuyItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForShipStoreItemSaleReq(Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForShipStoreItemSaleReqImp(List<NpcShopItemUIInfo> saleList, Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForStoreItemBatchSaleReq(Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void ExecNetTaskProcessForStoreItemBatchSaleReqImp(List<NpcShopItemUIInfo> saleList, Action onSellItemFinished)
        {
        }

        [MethodImpl(0x8000)]
        private CurrencyType GetCurrencyTypeForCurrUIState()
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemCategory GetCurrModeSelectCategory()
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemType GetCurrModeSelectType()
        {
        }

        [MethodImpl(0x8000)]
        private int GetDefaultSellCount(NpcShopItemUIInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public string GetFirstItemPathString()
        {
        }

        [MethodImpl(0x8000)]
        private GrandFaction GetGrandFactionTypeForCurrUIState()
        {
        }

        [MethodImpl(0x8000)]
        private bool GetNpcShopItemBindInfo(ConfigDataNpcShopItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemFilter GetNpcShopItemFilterType(NpcShopItemUIInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetNpcShopName(bool isTradeSaleShop, bool isLegal)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemUIInfo GetPlayerSelectedItem(StoreItemType itemType, int itemId, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        private long GetShipItemAlreadyHaveCount(StoreItemType itemType, int configID, bool isBind, bool isFreezing = false, bool noDifferenceIsFreezing = true)
        {
        }

        [MethodImpl(0x8000)]
        private static bool GetShopNpc(out GDBSpaceStationNpcTalkerInfo shopNpc, int npcShopFlag = 0, bool isTardeSaleShop = false, bool isLegal = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private long GetStoreItemAlreadyHaveCount(StoreItemType itemType, int configID)
        {
        }

        [MethodImpl(0x8000)]
        private ulong GetTotalCostMoneyForAutomaticSaleItemList()
        {
        }

        [MethodImpl(0x8000)]
        private ulong GetTotalCostMoneyForSelectedItemList()
        {
        }

        [MethodImpl(0x8000)]
        private ulong GetTotalOccupySpaceForSelectedItemList()
        {
        }

        [MethodImpl(0x8000)]
        public void GuideStepOne()
        {
        }

        [MethodImpl(0x8000)]
        public void GuideStepTwo(bool isInRect)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDefultNpcShopBuyMode()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool NeedShowTechDissatisfyObject(bool isTradeSaleShop)
        {
        }

        [MethodImpl(0x8000)]
        private void NpcShopItemListFilter()
        {
        }

        [MethodImpl(0x8000)]
        private int NpcShopItemListSortCompare(NpcShopItemUIInfo itemA, NpcShopItemUIInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private int NpcShopItemSortById(NpcShopItemUIInfo itemA, NpcShopItemUIInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private int NpcShopItemSortByPickSize(NpcShopItemUIInfo itemA, NpcShopItemUIInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private int NpcShopItemSortByPrice(NpcShopItemUIInfo itemA, NpcShopItemUIInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private int NpcShopItemSortBySubRanktype(NpcShopItemUIInfo itemA, NpcShopItemUIInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddMoneyClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoSaleComfimClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoSaleItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemConfirmWnd_CancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemConfirmWnd_ConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCategoryToggle_ItemCategoryButtonClick(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCategoryToggle_ItemTypeToggleSelected(int itemTypeId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFilterToggleValueChanged(int filterType, bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreSellToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShipItemList_OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopBuyOrSellWnd_ConfirmButtonClick(NpcShopBuyOrSellWndUIController wndCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_NpcShopItemClick(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_NpcShopItemDoubleClick(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_NpcShopItemIcon3DTouch(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_NpcShopItemIconClick(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_NpcShopItemLongPressStart(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemList_ScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPriceSortButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelectedItemList_BuyOrSellButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelectedItemList_ItemIconClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelectedItemList_ScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStoreSellToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSizeSortButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortListBgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortListButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortStateChanged(NpcShopSortType destType)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubRankSortButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public static void OpenNpcShopWithoutDialog(UIIntent uiIntent, StoreItemType type, int configId, Action onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void OpenNpcShopWithoutDialog(UIIntent uiIntent, string mode, NpcShopItemCategory category, NpcShopItemType type, Action onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RemovePlayerSelectedItem(NpcShopItemUIInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static void SendInStationNpcDialogStartReq(GDBSpaceStationNpcTalkerInfo shopNpc, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrModeSelectCategory(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrModeSelectType(int type)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipelineCategoryTogglesMaskChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForAutoSellEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForBuyShopItems()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForInit()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForModeChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForSelectedItemListChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMaskForSelectedItemTypeChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSelectShopTypeFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowAddItemTipWindow(string name, long itemCount, bool withNoDrvingLicensePostfix = false)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartNpcShopMainUITask_Defult(Action<bool> onEnd = null, UIIntent returnIntent = null, IUIBackgroundManager backgroundManager = null, ILBStoreItemClient selectedItem = null)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartNpcShopMainUITask_TradeSale(Action<bool> onEnd = null, bool isLegal = false, UIIntent returnIntent = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartNpcShopMainUITaskWithFixedType(UIIntent returnIntent, string mode = "NpcShopMainUITaskMode_Buy", NpcShopItemCategory shopCatagory = 1, NpcShopItemType shopType = 0, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartNpcShopMainUITaskWithOutDialog_Defult(NpcShopFlag shopFlag, UIIntent uiIntent, Action onEnd, IUIBackgroundManager backgroundManager = null, ILBStoreItemClient selectedItem = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartNpcShopMainUITaskWithOutDialog_TradeSale(bool isLegal, UIIntent uiIntent, Action onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_AutomaticItemSotre()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_AutomaticShipSotre()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_BuyMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_CleanAllSelectedItem()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SellFromItemStore()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SellFromShipStore()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SortAndFilter()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_TradeSellFromItemStore()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_TradeSellFromShipStore()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_UpdateCategoryAndType()
        {
        }

        [MethodImpl(0x8000)]
        protected Dictionary<NpcShopItemCategory, List<int>> UpdateDataCache_UpdateCategoryAndTypeForBuyMode(List<KeyValuePair<int, long>> itemList)
        {
        }

        [MethodImpl(0x8000)]
        protected Dictionary<NpcShopItemCategory, List<int>> UpdateDataCache_UpdateCategoryAndTypeForSellMode(List<NpcShopItemUIInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_UpdateCategoryCount()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_UpdateSelectCategoryAndType()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePlayerSelectedItemView(NpcShopItemUIInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_AutomaticSell()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_BuyAndSellToggles()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_BuyOrSellItemList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_CategoryToggleGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_CurrencyCount()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SelectedItemList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SellTogglesRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_UpdateCategoryItemCount()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_WndTitleText()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockNpcShopClient LBNpcShopClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AddPlayerSelectedItem>c__AnonStoreyF
        {
            internal NpcShopMainUITask.NpcShopItemUIInfo itemInfo;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);

            internal bool <>m__1(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);
        }

        [CompilerGenerated]
        private sealed class <AddPlayerSelectedItemWithoutUpdateView>c__AnonStoreyD
        {
            internal NpcShopMainUITask.NpcShopItemUIInfo itemInfo;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);

            internal bool <>m__1(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);
        }

        [CompilerGenerated]
        private sealed class <CheckItemCanSaleInNpcShop>c__AnonStorey11
        {
            internal int itemId;

            internal bool <>m__0(TradeNpcShopItemInfo item) => 
                (this.itemId == item.m_tradeItemId);
        }

        [CompilerGenerated]
        private sealed class <ExceNetTaskProcessForTradeNpcShopSale>c__AnonStoreyC
        {
            internal Action onSellItemFinished;
            internal NpcShopMainUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                TradeNpcShopSaleItemsReqNetTask task2 = task as TradeNpcShopSaleItemsReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        if (task2.Result == -1773)
                        {
                            this.$this.SetPipeLineMaskForBuyShopItems();
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        this.$this.SetPipeLineMaskForBuyShopItems();
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_Shop_ItemSale, null, 1f);
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_SaleItemSucceed, false, new object[0]);
                        if (this.onSellItemFinished != null)
                        {
                            this.onSellItemFinished();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ExecNetTaskProcessForNpcShopBuyItemReq>c__AnonStorey9
        {
            internal Action onBuyItemFinished;
            internal NpcShopMainUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                NpcShopBuyItemReqNetTask task2 = task as NpcShopBuyItemReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.Result == 0)
                    {
                        this.$this.SetPipeLineMaskForBuyShopItems();
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_BuyItemSucceed, false, new object[0]);
                        PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_BuyItemInNpcShopAudioResFullPath, null, 1f);
                        if (this.onBuyItemFinished != null)
                        {
                            this.onBuyItemFinished();
                        }
                    }
                    else if (task2.Result != -1038)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        object[] args = new object[] { StringFormatUtil.GetCurrencyTypeName(this.$this.GetCurrencyTypeForCurrUIState()) };
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_CurrencyNotEnough, false, args);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ExecNetTaskProcessForShipStoreItemSaleReqImp>c__AnonStoreyA
        {
            internal Action onSellItemFinished;
            internal NpcShopMainUITask $this;

            internal void <>m__0(Task task)
            {
                ShipStoreItemSaleReqNetTask task2 = task as ShipStoreItemSaleReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onSellItemFinished != null)
                        {
                            this.onSellItemFinished();
                        }
                    }
                    else
                    {
                        this.$this.SetPipeLineMaskForBuyShopItems();
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_Shop_ItemSale, null, 1f);
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_SaleItemSucceed, false, new object[0]);
                        if (this.onSellItemFinished != null)
                        {
                            this.onSellItemFinished();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ExecNetTaskProcessForStoreItemBatchSaleReqImp>c__AnonStoreyB
        {
            internal Action onSellItemFinished;
            internal NpcShopMainUITask $this;

            internal void <>m__0(Task task)
            {
                StoreItemBatchSaleReqNetTask task2 = task as StoreItemBatchSaleReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onSellItemFinished != null)
                        {
                            this.onSellItemFinished();
                        }
                    }
                    else
                    {
                        this.$this.SetPipeLineMaskForBuyShopItems();
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_Shop_ItemSale, null, 1f);
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_SaleItemSucceed, false, new object[0]);
                        if (this.onSellItemFinished != null)
                        {
                            this.onSellItemFinished();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <NpcShopItemListFilter>c__AnonStorey8
        {
            internal NpcShopMainUITask.NpcShopItemFilter filterType;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemFilter e) => 
                (e == this.filterType);
        }

        [CompilerGenerated]
        private sealed class <OnFilterToggleValueChanged>c__AnonStorey5
        {
            internal NpcShopMainUITask.NpcShopItemFilter type;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemFilter e) => 
                (e == this.type);

            internal bool <>m__1(NpcShopMainUITask.NpcShopItemFilter e) => 
                (e == this.type);
        }

        [CompilerGenerated]
        private sealed class <OnNpcShopBuyOrSellWnd_ConfirmButtonClick>c__AnonStorey7
        {
            internal NpcShopMainUITask.NpcShopItemUIInfo itemInfo;
            internal long itemCount;
            internal string itemName;
            internal NpcShopMainUITask $this;

            internal void <>m__0()
            {
                this.$this.AddPlayerSelectedItem(this.itemInfo, this.itemCount);
                this.$this.ShowAddItemTipWindow(this.itemName, this.itemCount, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnNpcShopItemList_NpcShopItemClick>c__AnonStorey6
        {
            internal NpcShopListItemUIController ctrl;
            internal long itemCount;
            internal string itemName;
            internal GameObject itemMovePrefab;
            internal NpcShopMainUITask $this;

            internal void <>m__0()
            {
                this.$this.AddPlayerSelectedItem(this.ctrl.m_itemUIInfo, this.itemCount);
                this.$this.ShowAddItemTipWindow(this.itemName, this.itemCount, true);
            }

            internal void <>m__1()
            {
                this.$this.m_mainCtrl.m_npcShopItemListCtrl.SetItemMoveUIToUnusedState(this.itemMovePrefab);
                this.$this.UpdatePlayerSelectedItemView(this.ctrl.m_itemUIInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OpenNpcShopWithoutDialog>c__AnonStorey0
        {
            internal UIIntent uiIntent;
            internal StoreItemType type;
            internal int configId;
            internal IUIBackgroundManager backgroundManager;
            internal Action onEnd;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    UIIntentReturnable intent = new UIIntentReturnable(this.uiIntent, "NpcShopMainUITask", "NpcShopMainUITaskMode_Buy");
                    intent.SetParam("ItemType", ClientStoreItemBaseHelper.GetNpcShopItemTypeFromItemTypeInfo(this.type, this.configId));
                    intent.SetParam("QuickJump", 1);
                    intent.SetParam("BackgroundManager", this.backgroundManager);
                    if (UIManager.Instance.StartUITask(intent, true, false, null, null) == null)
                    {
                        Debug.LogError("Start NpcShop Failed;");
                    }
                    else if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OpenNpcShopWithoutDialog>c__AnonStorey1
        {
            internal UIIntent uiIntent;
            internal string mode;
            internal NpcShopItemCategory category;
            internal NpcShopItemType type;
            internal IUIBackgroundManager backgroundManager;
            internal Action onEnd;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    if (NpcShopMainUITask.StartNpcShopMainUITaskWithFixedType(this.uiIntent, this.mode, this.category, this.type, this.backgroundManager) == null)
                    {
                        Debug.LogError("Start NpcShop Failed;");
                    }
                    else if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RemovePlayerSelectedItem>c__AnonStorey10
        {
            internal NpcShopMainUITask.NpcShopItemUIInfo itemInfo;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);

            internal bool <>m__1(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);
        }

        [CompilerGenerated]
        private sealed class <SendInStationNpcDialogStartReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                InStationNpcDialogStartReqNetTask task2 = task as InStationNpcDialogStartReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.StartNpcDialogReqResult == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.StartNpcDialogReqResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNpcShopMainUITaskWithOutDialog_Defult>c__AnonStorey2
        {
            internal UIIntent uiIntent;
            internal IUIBackgroundManager backgroundManager;
            internal ILBStoreItemClient selectedItem;
            internal Action onEnd;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    if (NpcShopMainUITask.StartNpcShopMainUITask_Defult(null, this.uiIntent, this.backgroundManager, this.selectedItem) == null)
                    {
                        Debug.LogError("Start NpcShop Failed;");
                    }
                    else if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNpcShopMainUITaskWithOutDialog_TradeSale>c__AnonStorey3
        {
            internal bool isLegal;
            internal UIIntent uiIntent;
            internal IUIBackgroundManager backgroundManager;
            internal Action onEnd;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    TradeNpcShopInfoReqNetTask task = new TradeNpcShopInfoReqNetTask();
                    task.EventOnStop += delegate (Task task) {
                        TradeNpcShopInfoReqNetTask task2 = task as TradeNpcShopInfoReqNetTask;
                        if ((task2 != null) && !task2.IsNetworkError)
                        {
                            if (task2.Result != 0)
                            {
                                TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                            }
                            else
                            {
                                NpcShopMainUITask.StartNpcShopMainUITask_TradeSale(null, this.isLegal, this.uiIntent, this.backgroundManager);
                                if (this.onEnd != null)
                                {
                                    this.onEnd();
                                }
                            }
                        }
                    };
                    task.Start(null, null);
                }
            }

            internal void <>m__1(Task task)
            {
                TradeNpcShopInfoReqNetTask task2 = task as TradeNpcShopInfoReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        NpcShopMainUITask.StartNpcShopMainUITask_TradeSale(null, this.isLegal, this.uiIntent, this.backgroundManager);
                        if (this.onEnd != null)
                        {
                            this.onEnd();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdatePlayerSelectedItemView>c__AnonStoreyE
        {
            internal NpcShopMainUITask.NpcShopItemUIInfo itemInfo;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemUIInfo item) => 
                this.itemInfo.Equals(item);
        }

        public enum NpcShopItemFilter
        {
            RankFilter_T1,
            RankFilter_T2,
            RankFilter_T3,
            RankFilter_None
        }

        public class NpcShopItemUIInfo
        {
            public ConfigDataNpcShopItemInfo m_shopItemConfigData;
            public StoreItemType m_itemType;
            public int m_itemConfigId;
            public long m_itemNum;
            public int m_itemStoreIndex;
            public bool m_itemIsBind;
            public float m_price;
            public CurrencyType m_currencyType;
            public long m_itemAlreadyHaveCount;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Equals_1;
            private static DelegateBridge __Hotfix_Equals_0;
            private static DelegateBridge __Hotfix_GetHashCode;

            [MethodImpl(0x8000)]
            public bool Equals(NpcShopMainUITask.NpcShopItemUIInfo p)
            {
            }

            [MethodImpl(0x8000)]
            public override bool Equals(object obj)
            {
            }

            [MethodImpl(0x8000)]
            public override int GetHashCode()
            {
            }
        }

        public enum NpcShopSortType
        {
            DefaultIdUp,
            SubRankTypeUp,
            SubRankTypeDown,
            PickSizeUp,
            PickSizeDown,
            PriceUp,
            PriceDown
        }
    }
}

