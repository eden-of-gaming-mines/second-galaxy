﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillTreeDesc : MonoBehaviour
    {
        public Text[] m_skillGenericNameTextList;
        public SkillTreeNodeInfo[] m_skillTreeNodeInfoList;

        [Serializable]
        public class SkillTreeNodeInfo
        {
            public GameObject m_nodeObject;
            public GameObject[] m_prevLinkLineObjects;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

