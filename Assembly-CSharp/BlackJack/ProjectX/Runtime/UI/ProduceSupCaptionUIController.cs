﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceSupCaptionUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRemoveCaptainButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProductionLine> EventOnCaptainPanelClick;
        public const string NoCaptainMode = "Empty";
        public const string CaptainMode = "Captain";
        public const string CaptainCanNotAddMode = "CanNotAdd";
        public const string CaptainCanAddMode = "CanAdd";
        public const string NotCanUsedCaptin = "NoUseCapyain";
        protected LBProductionLine m_currProducetionLine;
        protected LBStaticHiredCaptain m_workCaption;
        protected CommonCaptainIconUIController m_captainIconCtrl;
        protected ProjectXPlayerContext m_playerCtx;
        protected const string CaptainIconItemName = "CommonCaptainUIPrefab";
        [AutoBind("./CaptainGroup/RemoveCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveCaptainButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CaptainInfoPanelButton;
        [AutoBind("./CaptainGroup/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainInfoPanelStateCtrl;
        [AutoBind("./CaptainGroup/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateSupCaptionInfo;
        private static DelegateBridge __Hotfix_ShowOrHidePanel;
        private static DelegateBridge __Hotfix_SetToNormalState;
        private static DelegateBridge __Hotfix_SetReduceCostInfo;
        private static DelegateBridge __Hotfix_OnCaptainPanelClick;
        private static DelegateBridge __Hotfix_OnRemoveCaptainButtonClic;
        private static DelegateBridge __Hotfix_add_EventOnRemoveCaptainButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRemoveCaptainButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainPanelClick;

        public event Action<LBProductionLine> EventOnCaptainPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRemoveCaptainButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainPanelClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRemoveCaptainButtonClic(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetReduceCostInfo(int blueprintId, ulong captainId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNormalState()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHidePanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSupCaptionInfo(LBProductionLine produceLine, int selectBlueprintId, ulong captainId, bool hasCanUsedCaptain, int count, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

