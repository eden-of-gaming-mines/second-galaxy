﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class CharacterSimpleInfoUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProPlayerSimplestInfo> EventOnCharacterDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ProPlayerSimplestInfo> EventOnSendChatMsgButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProPlayerSimplestInfo> EventOnTeamInviteButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProPlayerSimplestInfo> EventOnSendMailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ProPlayerSimplestInfo> EventOnDiplomacyUpdateButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProPlayerSimplestInfo> EventOnExpelGuildButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProPlayerSimplestInfo> EventOnEidtJobButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string, bool, DiplomacyState> EventOnDiplomacyUpdate;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPanelClose;
        private CommonDiplomacySetWndUITask m_diplomacySetWndUITask;
        private float m_spaceSize;
        private Vector2 m_attachRectSize;
        private Vector3 m_panelPos;
        private PostionType m_showPanelType;
        private DiplomacyPostionType m_diplomacyPanelPostionType;
        private Dictionary<ButtonType, ButtonState> m_buttonsStates;
        private ChatInfo m_chatInfo;
        private CharacterSimpleInfoUIController m_mainCtrl;
        private ProPlayerSimplestInfo m_playerInfo;
        public const string ChatPanelStartMode = "StartByChatPanel";
        public const string PlayerListStartMode = "StartByPlayerList";
        public const string ParamsPanelPos = "PlayerSimpleInfoPanelPostion";
        public const string ParamsPanelType = "PlayerSimpleInfoPanelType";
        public const string ParamsDiplomacyPanelType = "PlayerSimpleInfoDiplomacyPanelType";
        public const string ParamsReturnToIntent = "PlayerSimpleInfoReturnToIntent";
        public const string ParamsPlayerInfo = "PlayerSimpleInfo";
        public const string ParamsRectSize = "RectSize";
        public const string ParamsSpaceSize = "SpaceSize";
        public const string ParamsButtonState = "ButtonState";
        public const string ParamsNeedPassEvent = "NeedPassEvent";
        public const string ParamsChatInfo = "ChatInfo";
        public bool m_needPassEvent;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CharacterSimpleInfoUITask";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        public static string m_lockState;
        public static string m_unLockState;
        private const string LockLayerName = "CharacterSimpleInfo";
        private const string LockMailName = "Mail";
        private const string LockTeamName = "Team";
        private const string LockChatName = "Chat";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowCharacterSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnCharacterDetailButtonClick_0;
        private static DelegateBridge __Hotfix_OnSendChatMsgButtonClick_0;
        private static DelegateBridge __Hotfix_OnTeamInviteButtonClick_0;
        private static DelegateBridge __Hotfix_OnSendMailButtonClick_0;
        private static DelegateBridge __Hotfix_OnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyUpdate;
        private static DelegateBridge __Hotfix_OnExpelGuildButtonClick_0;
        private static DelegateBridge __Hotfix_OnEidtJobButtonClick;
        private static DelegateBridge __Hotfix_OnReportButtonClick;
        private static DelegateBridge __Hotfix_GetVoiceMessageAsString;
        private static DelegateBridge __Hotfix_OnPassBkgClick;
        private static DelegateBridge __Hotfix_OnCharacterDetailButtonClick_1;
        private static DelegateBridge __Hotfix_OnSendChatMsgButtonClick_1;
        private static DelegateBridge __Hotfix_OnTeamInviteButtonClick_1;
        private static DelegateBridge __Hotfix_OnSendMailButtonClick_1;
        private static DelegateBridge __Hotfix_OnAddFriendButtonClick;
        private static DelegateBridge __Hotfix_OnExpelGuildButtonClick_1;
        private static DelegateBridge __Hotfix_HideCharSimpleInfoPanel;
        private static DelegateBridge __Hotfix_GetCurrPlayerUserId;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterOutSideBindUIEvent;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_ShowGameFunctionNotOpenTip;
        private static DelegateBridge __Hotfix_add_EventOnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendChatMsgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendChatMsgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendMailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendMailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnEidtJobButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnEidtJobButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyUpdate;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyUpdate;
        private static DelegateBridge __Hotfix_add_EventOnPanelClose;
        private static DelegateBridge __Hotfix_remove_EventOnPanelClose;
        private static DelegateBridge __Hotfix_get_m_playerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableMail;
        private static DelegateBridge __Hotfix_UnLockMailButton;
        private static DelegateBridge __Hotfix_EnableTeam;
        private static DelegateBridge __Hotfix_UnLockTeam;
        private static DelegateBridge __Hotfix_EnableChat;
        private static DelegateBridge __Hotfix_UnLockChat;

        public event Action<ProPlayerSimplestInfo> EventOnCharacterDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string, bool, DiplomacyState> EventOnDiplomacyUpdate
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnDiplomacyUpdateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnEidtJobButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnExpelGuildButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPanelClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnSendChatMsgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnSendMailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProPlayerSimplestInfo> EventOnTeamInviteButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CharacterSimpleInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableChat(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableMail(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableTeam(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetCurrPlayerUserId()
        {
        }

        [MethodImpl(0x8000)]
        private static string GetVoiceMessageAsString(ChatContentVoice voiceChatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void HideCharSimpleInfoPanel(Action onProcessEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAddFriendButtonClick(ProPlayerSimpleInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyUpdate(bool isPersonal, DiplomacyState changeDiplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyUpdateButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEidtJobButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnExpelGuildButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnExpelGuildButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPassBkgClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnReportButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendChatMsgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSendChatMsgButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendMailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamInviteButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPanelPostion()
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterSimpleInfoUITask ShowCharacterSimpleInfoPanel(string mode, ProPlayerSimplestInfo playerInfo, Vector3 pos, Vector2 rectSize, float spaceSize = 5f, PostionType postionType = 3, DiplomacyPostionType diplomacyPostionType = 0, UIIntent returnIntent = null, Dictionary<ButtonType, ButtonState> buttonStateDic = null, bool needPassEvent = true, ChatInfo chatInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        private bool ShowGameFunctionNotOpenTip(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockChat(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockMailButton(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockTeam(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterOutSideBindUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext m_playerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnCharacterDetailButtonClick>c__AnonStorey1
        {
            internal UIIntent prevIntent;

            internal void <>m__0(Task returnTask)
            {
                CharactorObserveReqNetTas tas = returnTask as CharactorObserveReqNetTas;
                if ((tas != null) && !tas.IsNetworkError)
                {
                    int ackResult = tas.m_ackResult;
                    if (ackResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(ackResult, true, false);
                        Debug.LogError("SpaceStation::CharSimpleTask_CharacterDetailButtonClick CharactorObserveReqNetTas Fail: result " + ackResult);
                    }
                    else
                    {
                        CharactorObserverAck charactorObserverAckInfo = tas.m_charactorObserverAckInfo;
                        UIIntentReturnable intent = new UIIntentReturnable(this.prevIntent, "CharacterInfoUITask", "CharacterUITaskMode_AnotherPlayer");
                        intent.SetParam("CharactorObserverAck", charactorObserverAckInfo);
                        UIManager.Instance.StartUITask(intent, true, false, null, null);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnExpelGuildButtonClick>c__AnonStorey2
        {
            internal ProPlayerSimplestInfo playerInfo;
            private static Action<Task> <>f__am$cache0;

            internal void <>m__0()
            {
                GuildMemberRemoveReqNetTask task = new GuildMemberRemoveReqNetTask(this.playerInfo.PlayerGameUserId);
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = delegate (Task task) {
                        GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                        if ((!task2.IsNetworkError && (task2 != null)) && (task2.Result != 0))
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                    };
                }
                task.EventOnStop += <>f__am$cache0;
                task.Start(null, null);
            }

            private static void <>m__1(Task task)
            {
                GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                if ((!task2.IsNetworkError && (task2 != null)) && (task2.Result != 0))
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnPassBkgClick>c__AnonStorey0
        {
            internal Action<PointerEventData> passAction;
            internal PointerEventData eventData;
            internal CharacterSimpleInfoUITask $this;

            internal void <>m__0()
            {
                if ((this.passAction != null) && this.$this.m_needPassEvent)
                {
                    this.passAction(this.eventData);
                }
            }
        }

        public enum ButtonState
        {
            Hide,
            Normal,
            Forbid
        }

        public enum ButtonType
        {
            WatchDetail,
            SendMsg,
            SendMail,
            FormTeam,
            AddFriend,
            Relation,
            ExperlGuild,
            EditJob,
            Report
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct ButtonTypeTypeComparer : IEqualityComparer<CharacterSimpleInfoUITask.ButtonType>
        {
            public bool Equals(CharacterSimpleInfoUITask.ButtonType x, CharacterSimpleInfoUITask.ButtonType y) => 
                (x == y);

            public int GetHashCode(CharacterSimpleInfoUITask.ButtonType obj) => 
                ((int) obj);
        }

        public enum DiplomacyPostionType
        {
            Left,
            Right,
            Auto
        }

        public enum PostionType
        {
            UseInput,
            Left,
            Right,
            Original,
            Auto
        }
    }
}

