﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapCelestialBodyInfoUIController : UIControllerBase
    {
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_uiStateCtrl;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialNameInfoGroup/CelestialTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_celestialTypeText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialNameInfoGroup/CelestialNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_celestialNameText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/PlanetTypeInfo/PlanetTypeInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetTypeInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/OrbitRadiusInfo/OrbitRadiusInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_orbitRadiusInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/OrbitalPeriodInfo/OrbitalPeriodInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_orbitalPeriodInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/EccentricityInfo/EccentricityInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_eccentricityInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/AgeInfo/AgeInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_ageInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/BrightnessInfo/BrightnessInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_brightnessInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/RadiusInfo/RadiusInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_radiusInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/MassInfo/MassInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_massInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/GravityInfo/GravityInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_gravityInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/EscapeVelocityInfo/EscapeVelocityInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_escapeVelocityInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/SpectrumInfo/SpectrumInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spectrumInfoText;
        [AutoBind("./WndRoot/WindowInfoGroup/CelestialBodyInfoGroup/TemperatureInfo/TemperatureInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_temperatureInfoText;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backgroupButton;
        private CelestialInfoType m_celestialInfoType;
        private static DelegateBridge __Hotfix_SetCelestialInfoType;
        private static DelegateBridge __Hotfix_SetCelestialTypeInfo;
        private static DelegateBridge __Hotfix_SetCelestialNameInfo;
        private static DelegateBridge __Hotfix_SetPlanetTypeInfo;
        private static DelegateBridge __Hotfix_SetOrbitRadiusInfo;
        private static DelegateBridge __Hotfix_SetOrbitalPeriodInfo;
        private static DelegateBridge __Hotfix_SetEccentricityInfo;
        private static DelegateBridge __Hotfix_SetAgeInfo;
        private static DelegateBridge __Hotfix_SetBrightnessInfo;
        private static DelegateBridge __Hotfix_SetRadiusInfo;
        private static DelegateBridge __Hotfix_SetMassInfo;
        private static DelegateBridge __Hotfix_SetGravityInfo;
        private static DelegateBridge __Hotfix_SetEscapeVelocityInfo;
        private static DelegateBridge __Hotfix_SetSpectrumInfo;
        private static DelegateBridge __Hotfix_SetTemperatureInfo;

        [MethodImpl(0x8000)]
        public void SetAgeInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBrightnessInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCelestialInfoType(CelestialInfoType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCelestialNameInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCelestialTypeInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEccentricityInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEscapeVelocityInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGravityInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMassInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOrbitalPeriodInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOrbitRadiusInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlanetTypeInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRadiusInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpectrumInfo(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTemperatureInfo(string text)
        {
        }

        public enum CelestialInfoType
        {
            StarInfo,
            PlanetInfo,
            MoonInfo
        }
    }
}

