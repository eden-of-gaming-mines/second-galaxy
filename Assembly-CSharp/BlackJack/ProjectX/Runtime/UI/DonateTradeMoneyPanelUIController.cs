﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public sealed class DonateTradeMoneyPanelUIController : UIControllerBase
    {
        private int m_donatedCount;
        private int m_maxCanDonatedTradeMoney;
        private int m_donateFactor;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./DonateCount/DonateCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DonateCountText;
        [AutoBind("./ContributionCount/ThisTimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ThisTimeNumberText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DonatePanelCommonUIStateController;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./DonateCountSlider/DonateCountSlider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider DonateCountSlider;
        [AutoBind("./DonateCountSlider/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./DonateCountSlider/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveButton;
        [AutoBind("./DonateCountSlider/DonateCountSlider/DonatedValue", AutoBindAttribute.InitState.NotInit, false)]
        public Image DonatedValue;
        [AutoBind("./MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyText;
        [AutoBind("./MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyText;
        [AutoBind("./MoneyGroup/IRMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_readMoneyText;
        private static DelegateBridge __Hotfix_GetGuildWalletUIProcess;
        private static DelegateBridge __Hotfix_UpdateDonateTradeMoneyPanelUI;
        private static DelegateBridge __Hotfix_UpdatePlayerMoneyUI;
        private static DelegateBridge __Hotfix_SetCurrDonateCount;
        private static DelegateBridge __Hotfix_GetNeedDonateCount;
        private static DelegateBridge __Hotfix_ChangeDonateCountByButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnDonateValueChaned;
        private static DelegateBridge __Hotfix_SetDonateText;

        [MethodImpl(0x8000)]
        public void ChangeDonateCountByButton(bool add)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildWalletUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public int GetNeedDonateCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateValueChaned(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrDonateCount(int donatedValue)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDonateText()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDonateTradeMoneyPanelUI(int donatedValue, int maxDonateValueToday, int maxCanDonate)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerMoneyUI(ulong bindMondey, ulong tradeMoney, ulong readMoney)
        {
        }
    }
}

