﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeSimpleInfoLevelUpEffectItemUIController : UIControllerBase
    {
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyNameText;
        [AutoBind("./LevelChange/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrEffectValueText;
        [AutoBind("./LevelChange/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextEffectValueText;
        [AutoBind("./FullLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FullEffectValueText;
        [AutoBind("./LevelChange", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LevelUpGroupRoot;
        [AutoBind("./FullLevel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FullLevelGroupRoot;
        private static DelegateBridge __Hotfix_UpdateEffectItemInfo;

        [MethodImpl(0x8000)]
        public void UpdateEffectItemInfo(CommonPropertyInfo currLevelPropertyInfo, CommonPropertyInfo nextLevelPropertyInfo)
        {
        }
    }
}

