﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask : NetWorkTransactionTask
    {
        public int? m_ackResult;
        private readonly ulong m_instanceId;
        private readonly int m_hangarShipIndex;
        private readonly int m_equipSlotIndex;
        private readonly int m_equipItemStoreIndex;
        private int m_shipStoreItemIndex;
        private int m_currEquipSlotItemIndex;
        protected bool m_isItemStoreContextChanged;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildHangarShipAddWeaponEquip2SlotAck;

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask(ulong instanceId, int hangarShipIndex, int hangarShipStoreIndex, int equipSlotIndex, int equipItemStoreIndex, int currEquipItemStoreIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildHangarShipAddWeaponEquip2SlotAck(int result, bool isItemStoreContextChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

