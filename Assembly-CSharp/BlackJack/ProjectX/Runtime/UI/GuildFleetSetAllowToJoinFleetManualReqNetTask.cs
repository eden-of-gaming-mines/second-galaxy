﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildFleetSetAllowToJoinFleetManualReqNetTask : NetWorkTransactionTask
    {
        private readonly ulong m_fleetId;
        private readonly bool m_allowToJoinFleetManual;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildFleetSetAllowToJoinFleetManualAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildFleetSetAllowToJoinFleetManualReqNetTask(ulong fleetId, bool allowToJoinFleetManual, bool blockedUI = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFleetSetAllowToJoinFleetManualAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

