﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DelegateMissionFleetItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnItemClick;
        private ulong m_delegateInstanceId;
        private CommonItemIconUIController m_fleetIconCtrl;
        private const string FleetState_Fighting = "Fighting";
        private const string FleetState_Collecting = "Collecting";
        private const string FleetState_BeInvading = "BeInvading";
        private const string FleetState_Transporting = "Transporting";
        private const string FleetState_AlreadyReturned = "AlreadyReturned";
        private const string AssetName_CommonItem = "CommonItem";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_uiStateCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject IconItemDummy;
        [AutoBind("./TimeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeRoot;
        [AutoBind("./TimeRoot/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image TimeProgressBar;
        [AutoBind("./TimeRoot/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionTime;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetDelegateMissionFleetInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<ulong> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDelegateMissionFleetInfo(LBDelegateMission mission, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

