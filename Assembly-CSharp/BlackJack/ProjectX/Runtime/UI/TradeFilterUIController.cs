﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class TradeFilterUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateUICtrl;
        [AutoBind("./Item Label", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemBaseCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_UnSelect;
        private static DelegateBridge __Hotfix_SetState;

        [MethodImpl(0x8000)]
        public void Init(bool isCareClick)
        {
        }

        [MethodImpl(0x8000)]
        private void SetState(bool isSelect, bool isUp)
        {
        }

        [MethodImpl(0x8000)]
        public void UnSelect()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(TradeUITask.TradeFilterInfo itemInfo, bool isSelect)
        {
        }
    }
}

