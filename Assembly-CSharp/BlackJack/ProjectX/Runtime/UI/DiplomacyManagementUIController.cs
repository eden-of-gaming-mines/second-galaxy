﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DiplomacyManagementUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyState, DiplomacyItemUIController> EventOnDiplomacyTypeChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DiplomacyManagementUITask.SearchType> EventOnSearchTypeChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyItemType> EventOnSortFilterChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyItemUIController> EventOnDiplomacyItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyItemUIController, bool> EventOnDiplomacyItemRenoveButtonClick;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        private bool m_isIntroducePanelShow;
        private DiplomacyItemListUIController m_searchItemListCtrl;
        private DiplomacyItemListUIController m_friendOrEnemyItemListCtrl;
        private DiplomacySortFilterUIController m_sortFilterUICtrl;
        private DiplomacySearchTypeUIController m_searchTypeUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_diplomacyStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backButton;
        [AutoBind("./ToggleGroup/FriendToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_friendButton;
        [AutoBind("./ToggleGroup/EnemyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_enemyButton;
        [AutoBind("./ToggleGroup/SearchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_searchButton;
        [AutoBind("./ToggleGroup/FriendToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_friendButtonStateCtrl;
        [AutoBind("./ToggleGroup/EnemyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_enemyButtonStateCtrl;
        [AutoBind("./ToggleGroup/SearchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_searchButtonStateCtrl;
        [AutoBind("./BGImages/Title/ExplainButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_explainButton;
        [AutoBind("./DiplomacyManagementIntroducePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_explainPanelStateCtrl;
        [AutoBind("./BackGroudButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController m_passEventCtrl;
        [AutoBind("./RelationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_friendOrEnemyGroupStateCtrl;
        [AutoBind("./RelationGroup/DiplomacyItemList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_friendOrEnemyItemListGameObject;
        [AutoBind("./RelationGroup/EmptyImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyStateCtrl;
        [AutoBind("./RelationGroup/SortGroup/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sortPanelRoot;
        [AutoBind("./RelationGroup/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortButtonStateCtrl;
        [AutoBind("./RelationGroup/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sortButton;
        [AutoBind("./SearchGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_searchGroupStateCtrl;
        [AutoBind("./SearchGroup/DiplomacyItemList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_searchItemListGameObject;
        [AutoBind("./SearchGroup/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_serchInputField;
        [AutoBind("./SearchGroup/InputField/Placeholder", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_serchInputFieldPlaceholderText;
        [AutoBind("./SearchGroup/SearchConfrimButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_searchConfrimButton;
        [AutoBind("./SearchGroup/SortGroup/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_seachTypePanelRoot;
        [AutoBind("./SearchGroup/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_searchTypeButtonStateCtrl;
        [AutoBind("./SearchGroup/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_searchSortButton;
        [AutoBind("./SearchGroup/SortGroup/SortButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchTypeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitDiplomacyItemListData;
        private static DelegateBridge __Hotfix_UpdateGuildDiplomacyItemList;
        private static DelegateBridge __Hotfix_UpdateSigleDiplomacyItem_0;
        private static DelegateBridge __Hotfix_UpdateSigleDiplomacyItem_1;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_GetSearchKey;
        private static DelegateBridge __Hotfix_ChangeSearchState;
        private static DelegateBridge __Hotfix_IsIntroducePanelShow;
        private static DelegateBridge __Hotfix_IsSortFilterPanelShow;
        private static DelegateBridge __Hotfix_IsSearchTypePanelShow;
        private static DelegateBridge __Hotfix_SetSearchType;
        private static DelegateBridge __Hotfix_SetFilterButtonState;
        private static DelegateBridge __Hotfix_ChangeFilterButtonState;
        private static DelegateBridge __Hotfix_GetFilterType;
        private static DelegateBridge __Hotfix_GetDiplomacyPanelUIProcess;
        private static DelegateBridge __Hotfix_GetIntroducePanelUIProcess;
        private static DelegateBridge __Hotfix_GetSortFilterPanelUIProcess;
        private static DelegateBridge __Hotfix_GetSearchTypePanelUIProcess;
        private static DelegateBridge __Hotfix_OnSortFilterChanged;
        private static DelegateBridge __Hotfix_OnSearchTypeChanged;
        private static DelegateBridge __Hotfix_OnDiplomacyItemRenoveButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_OnDiplomacyItemClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_add_EventOnSearchTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSearchTypeChanged;
        private static DelegateBridge __Hotfix_add_EventOnSortFilterChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSortFilterChanged;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyItemClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyItemRenoveButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyItemRenoveButtonClick;

        public event Action<DiplomacyItemUIController> EventOnDiplomacyItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DiplomacyItemUIController, bool> EventOnDiplomacyItemRenoveButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DiplomacyState, DiplomacyItemUIController> EventOnDiplomacyTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DiplomacyManagementUITask.SearchType> EventOnSearchTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DiplomacyItemType> EventOnSortFilterChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool ChangeFilterButtonState(DiplomacyItemType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public void ChangeSearchState(DiplomacyManagementUITask.SearchType itemType, string text = null)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetDiplomacyPanelUIProcess(bool isShow, bool isSerach, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyItemType GetFilterType()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetIntroducePanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public string GetSearchKey()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSearchTypePanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSortFilterPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void InitDiplomacyItemListData(uint guildId, uint allianceId, bool hasPromissionDiplomacyUpdate, bool isGuildDiplomacy)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsIntroducePanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSearchTypePanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSortFilterPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyItemRenoveButtonClick(UIControllerBase ctrl, bool hasPermission)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyTypeChanged(DiplomacyState type, UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchTypeChanged(DiplomacyManagementUITask.SearchType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortFilterChanged(DiplomacyItemType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonState(bool isFriend, bool isEnemy, bool isSearch)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFilterButtonState(DiplomacyItemType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSearchType(DiplomacyManagementUITask.SearchType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildDiplomacyItemList(bool isSearch, bool isFriend, List<DiplomacyItemInfo> diplomacyItemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSigleDiplomacyItem(bool isSerach, DiplomacyItemInfo item, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSigleDiplomacyItem(bool isSerach, DiplomacyItemUIController ctrl, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

