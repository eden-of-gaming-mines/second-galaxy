﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildDiplomacyUpdateReqNetTask : NetWorkTransactionTask
    {
        private string m_userId;
        private uint m_guildId;
        private uint m_allienceId;
        private DeplomacyOptType m_optType;
        private int m_result;
        public List<AllianceSimplestInfo> m_allianceSimplestInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyUpdateAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_get_AllianceSimplestInfoList;

        [MethodImpl(0x8000)]
        public GuildDiplomacyUpdateReqNetTask(string userId, uint guildId, uint allienceId, DeplomacyOptType optType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildDiplomacyUpdateAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<AllianceSimplestInfo> AllianceSimplestInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

