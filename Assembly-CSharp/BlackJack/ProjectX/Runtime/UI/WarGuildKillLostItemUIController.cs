﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class WarGuildKillLostItemUIController : UIControllerBase
    {
        private const string ItemAssetPath = "GuildLogoUIPrefab";
        public GuildLogoController m_guildLogoUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnGuildIconClick;
        [AutoBind("./RecordGroup/LossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_lossNumberText;
        [AutoBind("./RecordGroup/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_killNumberText;
        [AutoBind("./RecordGroup/GraphGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_numberText;
        [AutoBind("./RecordGroup/GraphGroup/KillBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_killBarImage;
        [AutoBind("./GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildNameText;
        [AutoBind("./GuildNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildNumberText;
        [AutoBind("./SignGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_signGroupButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateWarGuildLostItemUI;
        private static DelegateBridge __Hotfix_OnGuildLogoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildIconClick;

        public event Action<UIControllerBase> EventOnGuildIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLogoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarGuildLostItemUI(GuildBattleGuildKillLostStatInfo lossInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

