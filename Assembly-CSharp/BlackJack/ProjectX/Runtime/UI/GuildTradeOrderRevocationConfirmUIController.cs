﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class GuildTradeOrderRevocationConfirmUIController : UIControllerBase
    {
        private bool m_isDetailShow;
        private const string PanelOpen = "PanelOpen";
        private const string PanelClose = "PanelClose";
        private Action m_onDetailCloseClick;
        private Action m_onDetailClick;
        private Action m_onCancel;
        private Action m_onConfirm;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoPanelCtrl;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailCloseButton;
        [AutoBind("./BGImages/ButtonGroup/IconGroup/IconImage/ButtonCollider", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailInfoButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx YesButton;
        [AutoBind("./TextGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./TextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyNumberText;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetReverseIntroducePanelUIProcess;
        private static DelegateBridge __Hotfix_GetIntroducePanelUIProcess;
        private static DelegateBridge __Hotfix_GetCloseIntroducePanelUIProcess;
        private static DelegateBridge __Hotfix_OnDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnDetailCloseButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;

        [MethodImpl(0x8000)]
        public UIProcess GetCloseIntroducePanelUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetIntroducePanelUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetReverseIntroducePanelUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(int moneyNumber, Action onDetailClick, Action onDetailCloseClick, Action onConfirm, Action onCancel = null)
        {
        }
    }
}

