﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainRetireReportUIController : UIControllerBase
    {
        private List<CaptainMemoirsBookUIController> m_memoirsBookList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ItemInfo, int> EventOnReaminBookItemClick;
        private List<ItemInfo> m_reaminItemList;
        private ItemInfo m_remainFeatBookItem;
        private CommonItemIconUIController m_remainFeatBookCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./TitleGroup/SubRankText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankTextCtrl;
        [AutoBind("./ButtonBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ButtonBG;
        [AutoBind("./CaptainImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainImage;
        [AutoBind("./TitleGroup/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./TitleGroup/SubRankText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainSubRankText;
        [AutoBind("./TitleGroup/Profession/ProfessionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIconImage;
        [AutoBind("./TitleGroup/Profession/ProfessionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProfessionText;
        [AutoBind("./TitleGroup/PreLvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainLevelText;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/DetailGroup/Experience/MemoirsBookGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MemoirsBookGroup;
        [AutoBind("./Margin/DetailGroup/FeatBook/FeatGroup/FeatBookItem", AutoBindAttribute.InitState.NotInit, false)]
        public Button FeatBookItem;
        [AutoBind("./Margin/DetailGroup/FeatBook/FeatGroup/FeatBookItem/CommonItemIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FeatBookIconDummy;
        [AutoBind("./Margin/DetailGroup/FeatBook/FeatGroup/FeatBookItem/FeatBookNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatBookNameText;
        [AutoBind("./Margin/DetailGroup/FeatBook/FeatGroup/NoFeatRemains", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NoFeatRemains;
        [AutoBind("./FeatBookItemISimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FeatBookItemISimpleInfoDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetCaptainRetireReportInfo;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_GetMemoirsBookController;
        private static DelegateBridge __Hotfix_OnMemoryBookItemClick;
        private static DelegateBridge __Hotfix_OnFeatBookItemClick;
        private static DelegateBridge __Hotfix_add_EventOnReaminBookItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnReaminBookItemClick;

        public event Action<ItemInfo, int> EventOnReaminBookItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos(int index, out ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        private CaptainMemoirsBookUIController GetMemoirsBookController(int idx, int realIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFeatBookItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemoryBookItemClick(CaptainMemoirsBookUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainRetireReportInfo(LBStaticHiredCaptain retiredCaptain, List<StoreItemUpdateInfo> storeItemUpdateInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

