﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class QuestListCategoryUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<QuestListCategoryUIController> EventOnToggleValueChanged;
        private bool m_currFoldState = true;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private CategoryQuestType <ItemQuestType>k__BackingField;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx CategoryToggle;
        [AutoBind("./CategoryTitle/CornerImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController QuestCountStateCtrl;
        [AutoBind("./CategoryTitle/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestCountText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetCategoryFoldState;
        private static DelegateBridge __Hotfix_SwitchCategoryFoldState;
        private static DelegateBridge __Hotfix_SetQuestCount;
        private static DelegateBridge __Hotfix_OnToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnToggleValueChanged;
        private static DelegateBridge __Hotfix_get_FoldState;
        private static DelegateBridge __Hotfix_set_ItemQuestType;
        private static DelegateBridge __Hotfix_get_ItemQuestType;

        public event Action<QuestListCategoryUIController> EventOnToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnToggleValueChanged(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCategoryFoldState(bool fold)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestCount(int questCount, bool isAcceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchCategoryFoldState()
        {
        }

        public bool FoldState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CategoryQuestType ItemQuestType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public enum CategoryQuestType
        {
            None,
            ScienceExplore,
            Story,
            FreeSignal,
            FactionCredit,
            Branch,
            Unaccepted
        }
    }
}

