﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceBattleUIController : UIControllerBase
    {
        private const string Show = "Show";
        private const string Close = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_battleObjPool;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_battleObjScrollRect;
        [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_empty;
        private List<GuildBattleSimpleReportInfo> m_dataList;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        public Action<GuildBattleSimpleReportInfo> m_eventOnItemClick;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_EventOnBattleObjPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnRankingListItemFill;
        private static DelegateBridge __Hotfix_InitPanelInfo;

        [MethodImpl(0x8000)]
        private void EventOnBattleObjPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void InitPanelInfo(List<GuildBattleSimpleReportInfo> dataList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnRankingListItemFill(UIControllerBase ctrl)
        {
        }
    }
}

