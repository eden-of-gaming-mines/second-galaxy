﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildUITaskBase : UITaskBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_RegisterPlayerCtxEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayerCtxEvent;
        private static DelegateBridge __Hotfix_IsSelfGuildInvalid;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_BackToBasicUI;

        [MethodImpl(0x8000)]
        public GuildUITaskBase(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void BackToBasicUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsSelfGuildInvalid()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayerCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayerCtxEvent()
        {
        }
    }
}

