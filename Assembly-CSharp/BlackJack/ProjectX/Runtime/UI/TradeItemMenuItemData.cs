﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;

    public class TradeItemMenuItemData
    {
        public int m_typeId;
        public int m_itemId;
        public string m_str;
        public long m_ownAmount;
        public int m_factionCreditLevel;
        public bool m_isPersonal;
        public TradeUITaskTabType m_curTab;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

