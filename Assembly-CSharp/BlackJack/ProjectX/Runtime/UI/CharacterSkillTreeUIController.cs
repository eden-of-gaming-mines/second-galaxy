﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillTreeUIController : UIControllerBase
    {
        [AutoBind("./ShowEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterSkillTreeDesc m_skillTreeDesc;
        [AutoBind("./SkillLevelCountInfo/SkillTreeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_skillTreeNameText;
        [AutoBind("./SkillLevelCountInfo/CurrSkillLevelCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currSkillLevelCountText;
        [AutoBind("./SkillLevelCountInfo/MaxSkillLevelCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_maxSkillLevelCountText;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        private ConfigDataPassiveSkillTypeInfo m_skillTreeTypeInfo;
        private static DelegateBridge __Hotfix_ShowSKillTreePanel;
        private static DelegateBridge __Hotfix_SetSkillTreeName;
        private static DelegateBridge __Hotfix_SetSkillTreeIcon;
        private static DelegateBridge __Hotfix_SetSkillLevelCountInfo;
        private static DelegateBridge __Hotfix_SetSkillTreeTypeInfo;
        private static DelegateBridge __Hotfix_SetSkillLearningInfo;
        private static DelegateBridge __Hotfix_RegEventOnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_SetSkillGenericName;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_get_SkillTreeTypeInfo;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnSkillTreeNodeClick(Action<ConfigDataPassiveSkillInfo> onClick)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillGenericName(List<SkillLearningInfoForSkillTree> skillLearnInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillLearningInfo(List<SkillLearningInfoForSkillTree> skillLearningInfoList, List<ConfigDataPassiveSkillInfo> canLearnSkillList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillLevelCountInfo(int currCount, int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillTreeIcon(Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillTreeName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillTreeTypeInfo(ConfigDataPassiveSkillTypeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSKillTreePanel()
        {
        }

        public ConfigDataPassiveSkillTypeInfo SkillTreeTypeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetSkillLearningInfo>c__AnonStorey0
        {
            internal CharacterSkillTreeUIController.SkillLearningInfoForSkillTree skillInfo;

            internal bool <>m__0(ConfigDataPassiveSkillInfo item) => 
                (item.ID == this.skillInfo.m_skillInfo.ID);
        }

        public class SkillLearningInfoForSkillTree
        {
            public ConfigDataPassiveSkillInfo m_skillInfo;
            public Sprite m_skillIcon;
            public int m_currSkillLevel;
            public bool m_isLocked;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

