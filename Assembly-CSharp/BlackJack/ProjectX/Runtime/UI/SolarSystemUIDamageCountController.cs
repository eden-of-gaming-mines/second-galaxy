﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemUIDamageCountController : PrefabControllerBase
    {
        [AutoBind("./DamageMissingCountRoot", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_damageMissCountStateCtrl;
        [AutoBind("./DamageCountRoot", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_damageCountStateCtrl;
        [AutoBind("./DamageCountRoot/Text", AutoBindAttribute.InitState.NotInit, false)]
        protected Text m_damageCountText;
        protected bool m_isPopDamageCount;
        protected LinkedList<DamageCountInfo> m_damageCountInfoList;
        protected DateTime m_popDamageCountOutTime;
        protected float m_currAvailableTimeout;
        protected DateTime m_lastDamageInfoClearTime;
        protected bool m_isVisible;
        public uint m_targetObjId;
        protected const float m_popDamageTime = 0.1f;
        private const float m_availableTimeout = 10f;
        private const float m_damageInfoClearTimeout = 2f;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint> EventOnAvailableTimeEnd;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_SetAvailable;
        private static DelegateBridge __Hotfix_SetTargetObjId;
        private static DelegateBridge __Hotfix_AddDamageCount;
        private static DelegateBridge __Hotfix_PopOneDamageInfo;
        private static DelegateBridge __Hotfix_PopDamageHitInfo;
        private static DelegateBridge __Hotfix_PopDamageMissInfo;
        private static DelegateBridge __Hotfix_ClearDamageValueInfo;
        private static DelegateBridge __Hotfix_add_EventOnAvailableTimeEnd;
        private static DelegateBridge __Hotfix_remove_EventOnAvailableTimeEnd;

        public event Action<uint> EventOnAvailableTimeEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddDamageCount(DamageCountInfo damageCountInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearDamageValueInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void PopDamageHitInfo(DamageCountInfo hitInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void PopDamageMissInfo(DamageCountInfo missInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void PopOneDamageInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAvailable(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetObjId(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        public class DamageCountInfo
        {
            public string m_text;
            public SolarSystemUIDamageCountController.DamagetCountType m_type;
            private static DelegateBridge _c__Hotfix_ctor;

            public DamageCountInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum DamagetCountType
        {
            Normal,
            Crit,
            Miss
        }
    }
}

