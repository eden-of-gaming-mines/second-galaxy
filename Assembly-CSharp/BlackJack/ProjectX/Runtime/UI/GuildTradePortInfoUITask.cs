﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class GuildTradePortInfoUITask : UITaskBase
    {
        private Action m_postUpdateViewDelayEvent;
        private float m_refreshPortInfoInterval;
        private DateTime m_nextRefreshPortInfoTime;
        private bool? m_isSuccessRequestGuildPortInfo;
        private bool? m_isSuccessRequestGuildOccupiedInfo;
        private readonly List<ConfigDataNormalItemInfo> m_guildTradeItemConfigList;
        private int m_curGuildPortIndex;
        private readonly Dictionary<int, GuildTradePortExtraInfo> m_cacheGuildTradePortExtraInfoDict;
        private ushort m_cacheTradePortInfoVersion;
        private GuildTradePurchaseInfo m_guildTradePurchaseInfo;
        private GuildTradeTransportInfo m_guildTradeTransportInfo;
        private GuildTradeOrderEditUITask m_guildTradeOrderEditTask;
        private GuildTradeOrderEditUITask.GuildTradeOrderInfo m_orderInfo;
        private const string ParamKeyRefresh = "Refresh";
        private const string ParamKeyOrder = "Order";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildTradeTransportInfo> EventOpenStarmapToTradeCenter;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGuildTradePortNotFound;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildTradeOrderEditUITask.GuildTradeOrderInfo> EventOnAddGuildTradeMoneyBtnClick;
        private GuildTradePortInfoUIController m_guildTradePortInfoUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildTradePortInfoUITask";
        public const string CurSelectTradePortID = "TradeCurSelectTradePortKey";
        [CompilerGenerated]
        private static Comparison<GuildTradePortExtraInfo> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildTradePortInfoUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_PrepareForGuildTradePortExtraInfoList;
        private static DelegateBridge __Hotfix_PrepareGuildOccupyiedInfo;
        private static DelegateBridge __Hotfix_PrepareGuildTradePortInfo;
        private static DelegateBridge __Hotfix_OnGuildPrepareEnd;
        private static DelegateBridge __Hotfix_IsAllInfoPrepareEnd;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_RecordInnerData;
        private static DelegateBridge __Hotfix_TryRecoverContextFromRecord;
        private static DelegateBridge __Hotfix_UpdateAllData;
        private static DelegateBridge __Hotfix_RemoveAllEvent;
        private static DelegateBridge __Hotfix_CloseGuildTradePortInfoPanel;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnIssuePurchaseOrderButtonClick;
        private static DelegateBridge __Hotfix_OnLocateTransportShipBtnClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrderButtonClick;
        private static DelegateBridge __Hotfix_OnGuildTradePortListItemClick;
        private static DelegateBridge __Hotfix_OnAddTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_OpenCreateOrEditOrder;
        private static DelegateBridge __Hotfix_UpdateSelectedTradePortInfo;
        private static DelegateBridge __Hotfix_GetGuildTradePortTradeInfo;
        private static DelegateBridge __Hotfix_GetFirstOrPreviousGuildPortId;
        private static DelegateBridge __Hotfix_UpdateTradePortExtraPortInfoList;
        private static DelegateBridge __Hotfix_IsPortIndexValid;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_IsNeedGetTradePortTradeInfo;
        private static DelegateBridge __Hotfix_get_GuildTradeItemConfigList;
        private static DelegateBridge __Hotfix_add_EventOpenStarmapToTradeCenter;
        private static DelegateBridge __Hotfix_remove_EventOpenStarmapToTradeCenter;
        private static DelegateBridge __Hotfix_add_EventOnGuildTradePortNotFound;
        private static DelegateBridge __Hotfix_remove_EventOnGuildTradePortNotFound;
        private static DelegateBridge __Hotfix_add_EventOnAddGuildTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddGuildTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetFristTradePortPath;

        public event Action<GuildTradeOrderEditUITask.GuildTradeOrderInfo> EventOnAddGuildTradeMoneyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildTradePortNotFound
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildTradeTransportInfo> EventOpenStarmapToTradeCenter
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildTradePortInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void CloseGuildTradePortInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(TradeGuildPlanUITaskUpdateMask state)
        {
        }

        [MethodImpl(0x8000)]
        private int GetFirstOrPreviousGuildPortId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetFristTradePortPath()
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildTradePortTradeInfo(int solarSystemId, ushort purchaseVersion, ushort transportVersion, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllInfoPrepareEnd()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedGetTradePortTradeInfo(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(TradeGuildPlanUITaskUpdateMask state)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPortIndexValid()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddTradeMoneyBtnClick(GuildTradeOrderEditUITask.GuildTradeOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrderButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildPrepareEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradePortListItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssuePurchaseOrderButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocateTransportShipBtnClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OpenCreateOrEditOrder()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareForGuildTradePortExtraInfoList(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareGuildOccupyiedInfo(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareGuildTradePortInfo(int solarSystemId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RecordInnerData()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildTradePortInfoUITask(GuildTradeOrderEditUITask.GuildTradeOrderInfo orderInfo, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void TryRecoverContextFromRecord()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAllData(UIIntent intent, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectedTradePortInfo(int index, Action<bool> onUpdateEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradePortExtraPortInfoList()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private List<ConfigDataNormalItemInfo> GuildTradeItemConfigList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildTradePortTradeInfo>c__AnonStorey7
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GetGuildTradePortTradeInfoNetTask task2 = task as GetGuildTradePortTradeInfoNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    int result = task2.Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildTradePortListItemClick>c__AnonStorey5
        {
            internal int index;
            internal GuildTradePortInfoUITask $this;

            internal void <>m__0(bool result)
            {
                if (!result)
                {
                    Debug.LogError("GuildTradePortInfoUITask.OnGuildTradePortListItemClick Error To Update Selected Trade Port Info");
                }
                else
                {
                    this.$this.m_curGuildPortIndex = this.index;
                    this.$this.EnablePipelineStateMask(GuildTradePortInfoUITask.TradeGuildPlanUITaskUpdateMask.UpdateTradePortInfo);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnResume>c__AnonStorey3
        {
            internal UIIntent intent;
            internal Action<bool> onPipelineEnd;
            internal GuildTradePortInfoUITask $this;

            internal void <>m__0(bool result)
            {
                this.$this.<OnResume>__BaseCallProxy0(this.intent, this.onPipelineEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForGuildTradePortExtraInfoList>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GetGuildTradePortExtraInfoListNetTask task2 = task as GetGuildTradePortExtraInfoListNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    int result = task2.Result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareGuildOccupyiedInfo>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task2 = task as GuildOccupiedSolarSystemInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareGuildTradePortInfo>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0(bool result)
            {
                this.onEnd(result);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateAllData>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal GuildTradePortInfoUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    this.onEnd(false);
                }
                else
                {
                    this.$this.UpdateTradePortExtraPortInfoList();
                    if ((this.$this.m_cacheGuildTradePortExtraInfoDict == null) || (this.$this.m_cacheGuildTradePortExtraInfoDict.Count == 0))
                    {
                        this.onEnd(true);
                        if (this.$this.EventOnGuildTradePortNotFound != null)
                        {
                            this.$this.EventOnGuildTradePortNotFound();
                        }
                    }
                    else
                    {
                        this.$this.TryRecoverContextFromRecord();
                        bool? nullable = null;
                        this.$this.m_isSuccessRequestGuildOccupiedInfo = nullable;
                        nullable = null;
                        this.$this.m_isSuccessRequestGuildPortInfo = nullable;
                        this.$this.PrepareGuildOccupyiedInfo(delegate (bool occupyResult) {
                            this.$this.m_isSuccessRequestGuildOccupiedInfo = new bool?(occupyResult);
                            this.$this.OnGuildPrepareEnd(this.onEnd);
                        });
                        this.$this.PrepareGuildTradePortInfo(this.$this.m_curGuildPortIndex, delegate (bool portInfoResult) {
                            this.$this.m_isSuccessRequestGuildPortInfo = new bool?(portInfoResult);
                            this.$this.OnGuildPrepareEnd(this.onEnd);
                        });
                    }
                }
            }

            internal void <>m__1(bool occupyResult)
            {
                this.$this.m_isSuccessRequestGuildOccupiedInfo = new bool?(occupyResult);
                this.$this.OnGuildPrepareEnd(this.onEnd);
            }

            internal void <>m__2(bool portInfoResult)
            {
                this.$this.m_isSuccessRequestGuildPortInfo = new bool?(portInfoResult);
                this.$this.OnGuildPrepareEnd(this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateSelectedTradePortInfo>c__AnonStorey6
        {
            internal Action<bool> onUpdateEnd;

            internal void <>m__0(bool result)
            {
                this.onUpdateEnd(result);
            }
        }

        private enum TradeGuildPlanUITaskUpdateMask
        {
            UpdateTradePortExtraInfoList,
            UpdateTradePortInfo,
            OpenOrder
        }
    }
}

