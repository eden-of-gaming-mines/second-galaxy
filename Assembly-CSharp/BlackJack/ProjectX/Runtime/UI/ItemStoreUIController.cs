﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemStoreUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CurrencyType> EventOnAddMoneyClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoTechButtonClick;
        public ItemStoreListUIController m_itemStoreListUICtrl;
        public ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        public TitleMoneyGroupUIController m_bindMoneyCtrl;
        public TitleMoneyGroupUIController m_tradeMoneyCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./ItemStoreListPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreListPanelDummy;
        [AutoBind("./ItemSimpleInfoUIPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoUIPanelDummy;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./CTitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BindMoneyObj;
        [AutoBind("./STitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TradeMoneyObj;
        [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EmptyPanelStateCtrl;
        private static DelegateBridge __Hotfix_GetShowItemStorePanelEffectInfo;
        private static DelegateBridge __Hotfix_GetShowItemStorePanelProcess;
        private static DelegateBridge __Hotfix_GetEmptyPanelUIProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnAddMoneyBtnClick;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddMoneyClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddMoneyClick;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;

        public event Action<CurrencyType> EventOnAddMoneyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetEmptyPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetShowItemStorePanelEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetShowItemStorePanelProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddMoneyBtnClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

