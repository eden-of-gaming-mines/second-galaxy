﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainMemoirsBookUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <BookIndex>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CaptainMemoirsBookUIController> EventOnMemoirsBookClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressingStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressing;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressingEnd;
        private static string CommonItemAssetPath = "CommonUIItem";
        private CommonItemIconUIController m_itemCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MemoirsBookButton;
        [AutoBind("./CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CommonItemUIDummy;
        [AutoBind("./BookNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BookNameText;
        [AutoBind("./Experience/BookExpNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BookExpNumberText;
        [AutoBind("./Experience", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExperienceGameObject;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetMemoirBookInfo;
        private static DelegateBridge __Hotfix_UpdateMemoirBookCount;
        private static DelegateBridge __Hotfix_GetIconImageTransform;
        private static DelegateBridge __Hotfix_OnMemoirsBookClick;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressingEnd;
        private static DelegateBridge __Hotfix_get_BookIndex;
        private static DelegateBridge __Hotfix_set_BookIndex;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookClick;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressingEnd;

        public event Action<CaptainMemoirsBookUIController> EventOnMemoirsBookClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressing
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CaptainMemoirsBookUIController> EventOnMemoirsBookLongPressingStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetIconImageTransform()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressing()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressingEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressingStart()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMemoirBookInfo(ConfigDataNormalItemInfo bookInfo, long bookCount, bool isBind, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemoirBookCount(long bookCount)
        {
        }

        public int BookIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

