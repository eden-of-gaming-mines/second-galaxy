﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CreateOrSearchGuildItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private ProGuildSimplestInfo m_guildInfo;
        private Action<uint> m_eventOnGotoGuildButtonClick;
        private Action<uint, Action<bool>> m_eventOnItemClick;
        public ScrollItemBaseUIController m_scrollCtrl;
        public GuildLogoController m_iconCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_gotoGuildButton;
        [AutoBind("./ArmyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_armyNameText;
        [AutoBind("./PeopleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_peopleText;
        [AutoBind("./CounryText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_counryText;
        [AutoBind("./CounryImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_counryImage;
        [AutoBind("./AllianceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceText;
        [AutoBind("./OfficeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_officeText;
        [AutoBind("./JoinButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_joinButton;
        [AutoBind("./ApplyForButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_applyForButton;
        [AutoBind("./SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject m_iconObj;
        private static DelegateBridge __Hotfix_UpdateGuildItemInfo;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnGotoGuildButtonClick;
        private static DelegateBridge __Hotfix_OnApplyButtonClick;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplyButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoGuildButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildItemInfo(ProGuildSimplestInfo guildInfo, bool isApply, int guildMaxCount, Dictionary<string, UnityEngine.Object> dynamicResDict, Action<uint> eventOnGotoGuildButtonClick, Action<uint, Action<bool>> eventOnItemClick, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

