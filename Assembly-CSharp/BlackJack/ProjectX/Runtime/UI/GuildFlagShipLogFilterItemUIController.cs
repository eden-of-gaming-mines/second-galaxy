﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildFlagShipLogFilterItemUIController : UIControllerBase
    {
        private const string MarkStateCheck = "Check";
        private const string MarkStateNoCheck = "NOCheck";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [AutoBind("./Item Label", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemText;
        [AutoBind("./CheckGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_checkMarkStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateViewOfLogTypeFilter;
        private static DelegateBridge __Hotfix_UpdateViewOfSolarSystemFilter;

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOfLogTypeFilter(GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOfSolarSystemFilter(GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo info)
        {
        }
    }
}

