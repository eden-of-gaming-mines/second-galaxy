﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildInfoEditorController : UIControllerBase
    {
        private const string PanelStateOpen = "Show";
        private const string PanelStateClose = "Close";
        private const string StateDisabled = "Disabled";
        private const string StateEnabled = "Normal";
        private static readonly List<int> m_errText;
        private static readonly Dictionary<int, ErrorType> m_errPlace;
        private static readonly Color m_normalColor;
        private static readonly Color m_errorColor;
        private static readonly Dictionary<GuildInfoEditAction, string> m_statusDict;
        private static readonly Dictionary<NameCodeException, StringTableId> m_exceptionWords;
        public GuildInfoEditAction m_action;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backButton;
        [AutoBind("./TipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipGroup;
        [AutoBind("./NameButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_nameBtn;
        [AutoBind("./RegionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_regionBtn;
        [AutoBind("./AnnouncementButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_announcementBtn;
        [AutoBind("./ManifestoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_manifestoBtn;
        [AutoBind("./QuitButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_quitBtn;
        [AutoBind("./DismissButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_dismissBtn;
        [AutoBind("./UndismissButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_undismissBtn;
        [AutoBind("./SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_signBtn;
        [AutoBind("./WarButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_warBtn;
        [AutoBind("./WarButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_warRedPoint;
        [AutoBind("./MiningButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_miningBut;
        [AutoBind("./MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_mailBtn;
        [AutoBind("./ToggleGroup/Switch", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_joinTypeSwitchButton;
        [AutoBind("./ToggleGroup/FreedomToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_joinTypeToggle0;
        [AutoBind("./ToggleGroup/AuditToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_joinTypeToggle1;
        [AutoBind("./RefundToggleGroup/FreedomToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_freeCompensationToggle;
        [AutoBind("./RefundToggleGroup/AuditToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_auditCompensationToggle;
        [AutoBind("./RefundToggleGroup/Switch", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_compensationBtn;
        [AutoBind("./TipGroup/ContentGroup/Buttons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_yesBtn;
        [AutoBind("./TipGroup/ContentGroup/Buttons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_noBtn;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_yesBtn2;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_noBtn2;
        [AutoBind("./TipGroup/ContentGroup/", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_inputController;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/CodeInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_codeInput;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/NameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_nameInput;
        [AutoBind("./TipGroup/ContentGroup/ModificationRegion/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_regionInput;
        [AutoBind("./TipGroup/ContentGroup/ModificationAnnouncement/TextGroup/TextInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_announcementInput;
        [AutoBind("./TipGroup/ContentGroup/ModificationManifesto/TextGroup/TextInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_manifestoInput;
        [AutoBind("./NameButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_nameBtnCtrl;
        [AutoBind("./RegionButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_regionBtnCtrl;
        [AutoBind("./AnnouncementButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_announcementBtnCtrl;
        [AutoBind("./ManifestoButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_manifestoBtnCtrl;
        [AutoBind("./QuitButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_quitBtnCtrl;
        [AutoBind("./DismissButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_dismissBtnCtrl;
        [AutoBind("./TipGroup/ContentGroup/ModificationRegion/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public GuildRegionController m_languageTypeList;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/NameInputField/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_nameFrame;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/NameInputField/NameExistsErr", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_nameErr;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/NameInputField/NameExistsErr/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameErrText;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/CodeInputField/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_codeFrame;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/CodeInputField/CodeExistsErr", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_codeErr;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/CodeInputField/CodeExistsErr/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_codeErrText;
        [AutoBind("./TipGroup/ContentGroup/ModificationAnnouncement/TextGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_announcementErrText;
        [AutoBind("./TipGroup/ContentGroup/ModificationManifesto/TextGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_manifestoErrText;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/YesButton/Cost/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_changeNameCost;
        [AutoBind("./UndismissButton/TextTime/CancelTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_dismissInTimeText;
        [AutoBind("./TipGroup/ContentGroup/Dismiss/TextGroup/RedHintText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_dismissHint;
        [AutoBind("./MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mail;
        [AutoBind("./WarButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_warSettings;
        private bool m_guildNameLegitimate;
        private bool m_guildCodeNumberLegitimate;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_DisableUnUsedFunctions;
        private static DelegateBridge __Hotfix_SetWhetherButtonEnabled;
        private static DelegateBridge __Hotfix_SetAction;
        private static DelegateBridge __Hotfix_OpenTipGroup;
        private static DelegateBridge __Hotfix_SetNameAndCode;
        private static DelegateBridge __Hotfix_GetModifiedName;
        private static DelegateBridge __Hotfix_GetModifiedCode;
        private static DelegateBridge __Hotfix_SetAnnouncement;
        private static DelegateBridge __Hotfix_GetModifiedAnnouncement;
        private static DelegateBridge __Hotfix_SetLanguageType;
        private static DelegateBridge __Hotfix_GetModifiedLanguageType;
        private static DelegateBridge __Hotfix_SetManifesto;
        private static DelegateBridge __Hotfix_GetModifiedManifesto;
        private static DelegateBridge __Hotfix_SetJoinPolicy;
        private static DelegateBridge __Hotfix_SetGuildCompensationAuto;
        private static DelegateBridge __Hotfix_SetWarRedPoint;
        private static DelegateBridge __Hotfix_CheckNameInput;
        private static DelegateBridge __Hotfix_CheckCodeNumberInput;
        private static DelegateBridge __Hotfix_CheckAnnouncementInput;
        private static DelegateBridge __Hotfix_CheckManifestoInput;
        private static DelegateBridge __Hotfix_CheckCreateGuildParameter;
        private static DelegateBridge __Hotfix_GetErrorString;
        private static DelegateBridge __Hotfix_PostProcessAnnouncement;
        private static DelegateBridge __Hotfix_ClearError;
        private static DelegateBridge __Hotfix_DisplayError;

        [MethodImpl(0x8000)]
        private void CheckAnnouncementInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckCodeNumberInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCreateGuildParameter()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckManifestoInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckNameInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearError(bool clearAnnouncementErr = true, bool clearManifestoErr = true)
        {
        }

        [MethodImpl(0x8000)]
        public void DisableUnUsedFunctions()
        {
        }

        [MethodImpl(0x8000)]
        public bool DisplayError(int err)
        {
        }

        [MethodImpl(0x8000)]
        private string GetErrorString(NameCodeException exc)
        {
        }

        [MethodImpl(0x8000)]
        public string GetModifiedAnnouncement()
        {
        }

        [MethodImpl(0x8000)]
        public string GetModifiedCode()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType GetModifiedLanguageType()
        {
        }

        [MethodImpl(0x8000)]
        public string GetModifiedManifesto()
        {
        }

        [MethodImpl(0x8000)]
        public string GetModifiedName()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess OpenTipGroup(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void PostProcessAnnouncement(GuildInfoEditAction guildInfoEditAction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAction(GuildInfoEditAction action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncement(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildCompensationAuto(bool auto)
        {
        }

        [MethodImpl(0x8000)]
        public void SetJoinPolicy(bool isFree)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLanguageType(GuildAllianceLanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetManifesto(string manifesto)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNameAndCode(string guildName, string guildCode, int cost, bool moneyEnough = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWarRedPoint(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWhetherButtonEnabled(CommonUIStateController ctrl, bool isEnabled)
        {
        }

        [CompilerGenerated]
        private sealed class <CheckNameInput>c__AnonStorey0
        {
            internal string str;

            internal bool <>m__0(string s) => 
                (this.str[0] != ' ');
        }

        private enum ErrorType
        {
            Other,
            NameErr,
            CodeErr
        }

        public enum GuildInfoEditAction
        {
            ChangeName,
            ChangeRegion,
            ChangeAnnouncement,
            ChangeManifesto,
            Dismiss
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct GuildInfoEditActionComparer : IEqualityComparer<GuildInfoEditorController.GuildInfoEditAction>
        {
            public bool Equals(GuildInfoEditorController.GuildInfoEditAction x, GuildInfoEditorController.GuildInfoEditAction y) => 
                (x == y);

            public int GetHashCode(GuildInfoEditorController.GuildInfoEditAction obj) => 
                ((int) obj);
        }

        private enum NameCodeException
        {
            NameRule,
            NameEmpty,
            NameStart,
            NameSesitiveWord,
            CodeRule,
            CodeStart,
            CodeEmpty,
            CodeSensitiveWord
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        private struct NameCodeExceptionComparer : IEqualityComparer<GuildInfoEditorController.NameCodeException>
        {
            public bool Equals(GuildInfoEditorController.NameCodeException x, GuildInfoEditorController.NameCodeException y) => 
                (x == y);

            public int GetHashCode(GuildInfoEditorController.NameCodeException obj) => 
                ((int) obj);
        }
    }
}

