﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class GuildBuildingHudUIController : UIControllerBase
    {
        private readonly List<GuildBuildingHudGroupUIController> m_hudItemGroupUICtrlList;
        private Canvas m_canvas;
        [AutoBind("./UnusedObjectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool HudEasyPool;
        [AutoBind("./Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemContentRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform GuildBuildingHudRoot;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingHud;
        private static DelegateBridge __Hotfix_OnTickUpdateGuildBuildingPos;
        private static DelegateBridge __Hotfix_SetCanvas;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnGuildBuildingHudCreate;
        private static DelegateBridge __Hotfix_ResetHudUIController;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBuildingHudCreate(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTickUpdateGuildBuildingPos(Dictionary<ulong, Vector2> buildScreenPosDic)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetHudUIController()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCanvas(Canvas canvas)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingHud(List<GuildBuildingInfo> buildingInfos, GuildSolarSystemInfoClient guildSolarSystemInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

