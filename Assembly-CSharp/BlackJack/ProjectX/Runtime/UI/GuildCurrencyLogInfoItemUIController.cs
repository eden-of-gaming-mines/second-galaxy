﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public sealed class GuildCurrencyLogInfoItemUIController : UIControllerBase
    {
        public ScrollItemBaseUIController m_scrollCtrl;
        [AutoBind("./AlterationText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LogTextCommonUIStateController;
        [AutoBind("./SurplusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SurplusText;
        [AutoBind("./AlterationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AlterationText;
        [AutoBind("./ProjectText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProjectText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildCurrencyLogInfoItemUIPrefabCommonUIStateController;
        private static DelegateBridge __Hotfix_UpdateLogItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool careButtonClick = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLogItem(GuildCurrencyLogInfo logInfo)
        {
        }
    }
}

