﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemTacticalEquipInfoUIController : UIControllerBase
    {
        protected bool m_isPanelOpen;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Scroll View/Viewport/Content/NameTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./Scroll View/Viewport/Content/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText;
        private static DelegateBridge __Hotfix_UpdateTacticalEquipInfo;
        private static DelegateBridge __Hotfix_GetShowOrHidePaneUIProcess;
        private static DelegateBridge __Hotfix_get_IsPanelOpen;
        private static DelegateBridge __Hotfix_set_IsPanelOpen;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetShowOrHidePaneUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEquipInfo(LBTacticalEquipGroupBase tacticalEquip, Dictionary<string, Object> resDict, ILBSpaceTarget target)
        {
        }

        public bool IsPanelOpen
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

