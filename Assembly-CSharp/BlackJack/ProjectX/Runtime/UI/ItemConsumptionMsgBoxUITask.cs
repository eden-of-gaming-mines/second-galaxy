﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ItemConsumptionMsgBoxUITask : UITaskBase
    {
        private const string TitleMsg = "TitleMsg";
        private const string Items = "Items";
        private const string OnLackOfItemTips = "OnLackOfItemTips";
        private const string OnMsgBoxConfirm = "OnMsgBoxConfirm";
        private const string OnMsgBoxCancelClick = "onMsgBoxCancelClick";
        private ItemConsumptionMsgBoxUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ItemConsumptionMsgBoxUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowMsgBox;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateMsgBoxInfo;
        private static DelegateBridge __Hotfix_CheckItemsIsEnough;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ItemConsumptionMsgBoxUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckItemsIsEnough()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMsgBox(string title, ICollection<KeyValuePair<ILBItem, int>> items, Action onMsgBoxConfirm = null, Action onMsgBoxCancelClick = null, Action onLackOfItemTips = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMsgBoxInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnCancelButtonClick>c__AnonStorey1
        {
            internal Action onMsgBoxCancelClick;
            internal ItemConsumptionMsgBoxUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.SetIconClickAction(null);
                this.$this.Pause();
                if (this.onMsgBoxCancelClick != null)
                {
                    this.onMsgBoxCancelClick();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey0
        {
            internal Action onMsgBoxConfirm;
            internal ItemConsumptionMsgBoxUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

