﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class WarShipLostItemUIController : UIControllerBase
    {
        public string m_shipUnitString;
        private const string ItemAssetPath = "CommonItemUIPrefab";
        public CommonItemIconUIController m_shipIconUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnShipIconClick;
        [AutoBind("./CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_commonItemUIDummy;
        [AutoBind("./LossDesc/EstimateNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_estimateNumberText;
        [AutoBind("./LossDesc/LossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_lossNumberText;
        [AutoBind("./BarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_barImage;
        [AutoBind("./ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateWarGuildLostItemUI;
        private static DelegateBridge __Hotfix_OnGuildLogoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnShipIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipIconClick;

        public event Action<UIControllerBase> EventOnShipIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLogoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarGuildLostItemUI(GuildBattleShipLostStatInfo lossInfo, int maxLossCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

