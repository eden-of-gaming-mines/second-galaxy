﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DevelopmentSelectionUIController : UIControllerBase
    {
        public const string DevelopmentApplicationItem = "DevelopmentApplicationItem";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DevelopmentNodeType> EventOnProjectButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnProjectChangeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ConfigDataDevelopmentNodeDescInfo> EventOnDetailPanelNodeClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ConfigDataDevelopmentProjectInfo> EventOnApplicationButtonClick;
        private const string GameObjectStateShow = "Show";
        private const string GameObjectStateClose = "Close";
        private const int MaxSecondaryNodeCount = 5;
        private const int MaxTertiaryNodeCount = 6;
        private List<ConfigDataDevelopmentProjectInfo> m_applicationItemInfos;
        private List<bool> m_applicationItemAvaliableList;
        private Dictionary<string, UnityEngine.Object> m_dynamicResDic;
        private readonly List<DevelopmentSecondaryNodeUIController> m_secondaryNodeUICtrls;
        private readonly List<DevelopmentTertiaryNodeUIController> m_tertiaryNodeUICtrls;
        [AutoBind("./ProjectPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_projectPanelStateCtrl;
        [AutoBind("./DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailPanelStateCtrl;
        [AutoBind("./ProjectPanel/ProjectGroup/ShipProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shipProjectButton;
        [AutoBind("./ProjectPanel/ProjectGroup/WeaponsProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_weaponsProjectButton;
        [AutoBind("./ProjectPanel/ProjectGroup/DeviceProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_deviceProjectButton;
        [AutoBind("./ProjectPanel/ProjectGroup/AmmoProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ammoProjectButton;
        [AutoBind("./ProjectPanel/ProjectGroup/ComponentProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_componentProjectButton;
        [AutoBind("./ProjectPanel/ProjectGroup/ChipProjectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_chipProjectButton;
        [AutoBind("./DetailPanel/LeftGroup/ProjectGroup/BgIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_projectBgIconImage;
        [AutoBind("./DetailPanel/LeftGroup/ProjectGroup/PatternText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_projectNameText;
        [AutoBind("./DetailPanel/LeftGroup/ProjectGroup/PatternText/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_projectIconImage;
        [AutoBind("./DetailPanel/LeftGroup/ProjectGroup/CutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_projectChangeButton;
        [AutoBind("./DetailPanel/LeftGroup/SecondaryGroup/PatternTitleImage/PatternTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_projectDescText;
        [AutoBind("./DetailPanel/LeftGroup/SecondaryGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_secondaryNodeGroupTf;
        [AutoBind("./DetailPanel/LeftGroup/SecondaryGroup/Group/SecondaryNode", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_secondaryNodeGo;
        [AutoBind("./DetailPanel/LeftGroup/SecondaryGroup/NodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_secondaryNodeNameText;
        [AutoBind("./DetailPanel/LeftGroup/TertiaryGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tertiaryNodeGroupTf;
        [AutoBind("./DetailPanel/LeftGroup/TertiaryGroup/Group/QualityPlane", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tertiaryNodeGo;
        [AutoBind("./DetailPanel/LeftGroup/TertiaryGroup/NodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tertiaryNodeNameText;
        [AutoBind("./DetailPanel/RightGroup/FunctionGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopScrollRect;
        [AutoBind("./DetailPanel/RightGroup/FunctionGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./DetailPanel/RightGroup/FunctionGroup/Scroll View/Viewport/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_applicationNodeRootTf;
        private static DelegateBridge __Hotfix_CreateProjectPanelProcess;
        private static DelegateBridge __Hotfix_CreateDetailPanelProcess;
        private static DelegateBridge __Hotfix_UpdateProjectArea;
        private static DelegateBridge __Hotfix_UpdateSecondaryArea;
        private static DelegateBridge __Hotfix_UpdateTertiaryArea;
        private static DelegateBridge __Hotfix_UpdateQuaternaryArea;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnShipProjectButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponsProjectButtonClick;
        private static DelegateBridge __Hotfix_OnDeviceProjectButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoProjectButtonClick;
        private static DelegateBridge __Hotfix_OnComponentProjectButtonClick;
        private static DelegateBridge __Hotfix_OnChipProjectButtonClick;
        private static DelegateBridge __Hotfix_OnProjectChangeButtonClick;
        private static DelegateBridge __Hotfix_OnApplicationItemCreated;
        private static DelegateBridge __Hotfix_OnApplicationItemFill;
        private static DelegateBridge __Hotfix_add_EventOnProjectButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnProjectButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnProjectChangeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnProjectChangeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailPanelNodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailPanelNodeClick;
        private static DelegateBridge __Hotfix_add_EventOnApplicationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnApplicationButtonClick;

        public event Action<ConfigDataDevelopmentProjectInfo> EventOnApplicationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataDevelopmentNodeDescInfo> EventOnDetailPanelNodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DevelopmentNodeType> EventOnProjectButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnProjectChangeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateDetailPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateProjectPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnChipProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnComponentProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeviceProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProjectChangeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponsProjectButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProjectArea(ConfigDataDevelopmentNodeDescInfo projectNodeInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuaternaryArea(List<ConfigDataDevelopmentProjectInfo> quaternaryNodeInfos, List<bool> nodeAvaliableList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSecondaryArea(List<ConfigDataDevelopmentNodeDescInfo> secondaryNodeInfos, ConfigDataDevelopmentNodeDescInfo selectedNodeInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTertiaryArea(List<ConfigDataDevelopmentNodeDescInfo> tertiaryNodeInfos, ConfigDataDevelopmentNodeDescInfo selectedNodeInfo)
        {
        }

        [CompilerGenerated]
        private sealed class <OnApplicationItemCreated>c__AnonStorey2
        {
            internal DevelopmentApplicationNodeUIController itemCtrl;
            internal DevelopmentSelectionUIController $this;

            internal void <>m__0()
            {
                if (this.$this.EventOnApplicationButtonClick != null)
                {
                    this.$this.EventOnApplicationButtonClick(this.itemCtrl.NodeInfo);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnBindFiledsCompleted>c__AnonStorey0
        {
            internal DevelopmentSecondaryNodeUIController ctrl;
            internal DevelopmentSelectionUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnBindFiledsCompleted>c__AnonStorey1
        {
            internal DevelopmentTertiaryNodeUIController ctrl;
            internal DevelopmentSelectionUIController $this;

            internal void <>m__0()
            {
                if (this.$this.EventOnDetailPanelNodeClick != null)
                {
                    this.$this.EventOnDetailPanelNodeClick(this.ctrl.NodeInfo);
                }
            }
        }
    }
}

