﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PuzzleGame3DSceneDesc : MonoBehaviour
    {
        [Header("星体投影的影子大小")]
        public float m_shadowScale;
        [Header("星体投影的影子偏移")]
        public Vector2 m_shadowOffset;
        [Header("最大音量对应的旋转角度(一帧)")]
        public float m_rotateDegreesForMaxVolume;
        [Header("最小音量对应的旋转角度(一帧)")]
        public float m_rotateDegreesForMinVolume;
        [Header("旋转音效最大音量")]
        public float m_maxVolume;
        [Header("旋转音效最小音量")]
        public float m_minVolume;
        [Header("一段时间未旋转,静音旋转音效(s)")]
        public float m_muteMoveInterval;
        [Header("星体的分布范围")]
        public float m_starRange;
        [Header("星体的初始匹配度最小值(0-1)")]
        public float m_defultMatchingRateMin;
        [Header("星体的初始匹配度最大值(0-1)")]
        public float m_defultMatchingRateMax;
        [Header("星体的完成匹配度(0-1)")]
        public float m_completeMatchingRate;
        [Header("星体的完成时的插值次数(大于0)")]
        public int m_completeLerpCount;
        [Header("星体的完成时的插值每几帧做一次插值")]
        public int m_lerpFrame;
        [Header("星体的旋转参数水平方向")]
        public float m_rotateParamX;
        [Header("星体的旋转参数垂直方向")]
        public float m_rotateParamY;
        [Header("星体的旋转参数黄道面方向")]
        public float m_rotateParamZ;
        [Header("太阳系线的分段节点(0-1)")]
        public List<float> m_soalrSystemLinePoints;
        [Header("阴影线的分段节点(0-1)")]
        public List<float> m_shadowLinePoints;
        [Header("答案系线的分段节点(0-1)")]
        public List<float> m_answerLinePoints;
        [Header("投射线的分段节点(0-1)")]
        public List<float> m_castLinePoints;
    }
}

