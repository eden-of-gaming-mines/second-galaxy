﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class UserGuideUITask : UserGuideUITaskBase
    {
        private static bool m_enableUIInputByUserGuide;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static Action EventOnGroupEnd;
        protected CanvasGroup m_canvasGroup;
        protected bool m_isCurrStepDragEnd;
        protected UserGuideUIController m_mainCtrl;
        protected CustomUserGuideUIController m_customCtrl;
        protected Vector2 m_dragBeginStartPosition;
        protected Action<UITaskBase, UserGuideStepInfo> m_onEndAction;
        protected DateTime m_autoStopOutTime;
        public const string UIMode_Prepare = "Prepare";
        public const string UIMode_ExecuteStep = "ExecuteStep";
        public static string ParamKey_UserGuideStepInfo;
        public static string ParamKey_UserGuideStepId;
        public static string ParamKey_OnEndAction;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "UserGuideUITask";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CurrUserGuideGroupId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CurrUserGuideStepId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ConfigDataUserGuideStepInfo <CurrUserGuideStepConfigInfo>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ConfigDataUserGuideGroupInfo <CurrUserGuideGroupConfigInfo>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <CurrWaitForNotifyTaskName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <CurrWaitForNotifyDesc>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private UserGuideStepInfo <CurrWaitForNotifyStepInfo>k__BackingField;
        protected Dictionary<string, UserGuideStepFuncInfo> m_userGuideStepFuncDict;
        private const int m_baseRedeployQuest = 0x4e67;
        private const float DefaultClickRadious = 100f;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache0;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache1;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache2;
        [CompilerGenerated]
        private static Action<UITaskBase, UserGuideStepInfo> <>f__mg$cache3;
        [CompilerGenerated]
        private static Action<UITaskBase, UserGuideStepInfo> <>f__mg$cache4;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache5;
        [CompilerGenerated]
        private static Action<UITaskBase, UserGuideStepInfo> <>f__mg$cache6;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<UITaskBase, UserGuideStepInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache7;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache8;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cache9;
        [CompilerGenerated]
        private static Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> <>f__mg$cacheA;
        [CompilerGenerated]
        private static CustomUIProcess.UIProcessExecutor <>f__am$cache2;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache3;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache4;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache5;
        [CompilerGenerated]
        private static CustomUIProcess.UIProcessExecutor <>f__am$cache6;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache7;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache8;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cache9;
        [CompilerGenerated]
        private static CustomUIProcess.UIProcessExecutor <>f__am$cacheA;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cacheB;
        [CompilerGenerated]
        private static CustomUIProcess.UIProcessExecutor <>f__am$cacheC;
        [CompilerGenerated]
        private static UIProcess.DebugOnEndFunc <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cacheE;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickForAutoStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_OnStepStart;
        private static DelegateBridge __Hotfix_OnStepEnd;
        private static DelegateBridge __Hotfix_SaveLogicStateOnStepEnd;
        private static DelegateBridge __Hotfix_OnStepEndInternal;
        private static DelegateBridge __Hotfix_GetUIController;
        private static DelegateBridge __Hotfix_StartUserGuideTaskForPrepare;
        private static DelegateBridge __Hotfix_StartUserGuideTaskForExecute;
        private static DelegateBridge __Hotfix_ShowOrHideUserGuide;
        private static DelegateBridge __Hotfix_PrintfMaskPanelState;
        private static DelegateBridge __Hotfix_ShowUserGuide;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_EnableGlobalUIInputByUserGuide;
        private static DelegateBridge __Hotfix_AutoStopStep;
        private static DelegateBridge __Hotfix_OnNpcChatEnd;
        private static DelegateBridge __Hotfix_OnCustomLayerButtonClick;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_HasCustomLayer;
        private static DelegateBridge __Hotfix_GetCustomLayerResPath;
        private static DelegateBridge __Hotfix_add_EventOnGroupEnd;
        private static DelegateBridge __Hotfix_remove_EventOnGroupEnd;
        private static DelegateBridge __Hotfix_get_LbUserGuide;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Activity;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Activity_Step2;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Auction;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_OpenAuction_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_OpenAuction_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_OpenAuction_Step2;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_AuctionBuyStaticUI;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4BranchStory;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_BranchStory_Quest20132_Step2;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_BranchStory_Quest20132_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_BranchStory_Quest20132_Step4;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4CaptainManage;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_EnterCaptainUI_Step2;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_CaptainUISetWingMan_Step1;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_UnLockCaptainShip_CaptainShip_step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_UnLockCaptainShip_CaptainShip_step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Staion_CaptainLeraning_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Staion_CaptainLeraning_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Staion_CaptainLeraning_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Staion_CaptainLeraning_Step4;
        private static DelegateBridge __Hotfix_DefaultPrepare4StepForCaptainUI;
        private static DelegateBridge __Hotfix_DefaultOnStepEndForUnLockCaptain3DUI;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_CaptainWingManButtonClick;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_CaptainTrainButtonClick;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_CaptainShipButtonClick;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4CharacterInfo;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_CharacterSkillGuide_Step1;
        private static DelegateBridge __Hotfix_OnStepEnd_Station_CharacterSkillGuide_Step1;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_CharacterSkillGuide_Step2;
        private static DelegateBridge __Hotfix_OnStepEnd_Station_CharacterSkillGuide_Step2;
        private static DelegateBridge __Hotfix_GetCharacterSkillTypeIdForStep22102;
        private static DelegateBridge __Hotfix_GetCharacterSkillTypeIndexForStep22102;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_CharacterSkillGuide_Step3;
        private static DelegateBridge __Hotfix_OnStepEnd_Station_CharacterSkillGuide_Step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DestroyerCheckGuide_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_DestroyerCheckGuide_Step4;
        private static DelegateBridge __Hotfix_StationDestroyerCheckGuideUIProcess;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickDrivingLicenceDestroyer;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_ChipSlot_Step1;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_ChipSlot_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_ChipSlot_Step1;
        private static DelegateBridge __Hotfix_CheckConfigConditionListAnd;
        private static DelegateBridge __Hotfix_CheckExtraConfigConditionListAnd;
        private static DelegateBridge __Hotfix_CheckConfigConditionListOr;
        private static DelegateBridge __Hotfix_CheckSingleCondition;
        private static DelegateBridge __Hotfix_CheckExtraSingleCondition;
        private static DelegateBridge __Hotfix_CheckLevel;
        private static DelegateBridge __Hotfix_CheckQuestAcceptOrComplete;
        private static DelegateBridge __Hotfix_CheckUserGuideStepFinished;
        private static DelegateBridge __Hotfix_CheckIsQuestUITaskStop;
        private static DelegateBridge __Hotfix_CheckIsQuestWaitForComplete;
        private static DelegateBridge __Hotfix_CheckQuestFinishedOrNot;
        private static DelegateBridge __Hotfix_CheckIsWithMotherShip;
        private static DelegateBridge __Hotfix_CheckIsInSpce;
        private static DelegateBridge __Hotfix_CheckIsTechFinishedOrNot;
        private static DelegateBridge __Hotfix_CheckIsLicenseFinishedOrNot;
        private static DelegateBridge __Hotfix_CheckIsSkillFinishedOrNot;
        private static DelegateBridge __Hotfix_CheckItemStoreHasItem;
        private static DelegateBridge __Hotfix_CheckInScene;
        private static DelegateBridge __Hotfix_CheckAccpetQuest;
        private static DelegateBridge __Hotfix_CheckUnaccpetedQuest;
        private static DelegateBridge __Hotfix_CheckNpcChatDialogEnd;
        private static DelegateBridge __Hotfix_CheckTextNoticeDialogId;
        private static DelegateBridge __Hotfix_CheckDrivingLicenseUpgradeState;
        private static DelegateBridge __Hotfix_CheckIsSolarSystemUIAvaliable;
        private static DelegateBridge __Hotfix_CheckIsFunctionStateOpened;
        private static DelegateBridge __Hotfix_CheckIsEqualFunctionState;
        private static DelegateBridge __Hotfix_CheckFixedQuestFinished;
        private static DelegateBridge __Hotfix_CheckIsSpaceStationUITaskShowEnd;
        private static DelegateBridge __Hotfix_CheckStoreHasAnyItem;
        private static DelegateBridge __Hotfix_CheckQuestWaitForCompleteOrComplete;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Crack;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_CrackBox_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_CrackBox_Step1;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Development;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Exploit_Quest2825_Step2;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Exploit_Quest2825_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Exploit_Quest2825_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Exploit_Quest2825_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Exploit_Quest2825_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Exploit_Quest2825_Step6;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Exploit_Main_Static1;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Explore;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_TreasureMap_Step3;
        private static DelegateBridge __Hotfix_StationItemStoreScroll;
        private static DelegateBridge __Hotfix_Prepare4StepExec_GetFirstTargetFromCurrTargetList;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_TreasureMap_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenStarMapAtSolar;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenStarMapAtStation;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ItemStoreOther;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_SelectItem;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_StartRecover;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Assign;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClosePuzzleGame;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4FactionCredit;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_FactionCredit_Quest20072_Step2;
        private static DelegateBridge __Hotfix_Prepare4StepExec_FactionCredit_Quest20072_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_FactionCredit_Quest20072_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_FactionCredit_Quest20072_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_FactionCredit_Faction_Static1_Step1;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Guild;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_Guild_Step2;
        private static DelegateBridge __Hotfix_TryStartUserGuide_Station_Guild_Step2;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4HeadQuarter;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_ChapterGuide4_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_ChapterGuide4_Step1;
        private static DelegateBridge __Hotfix_SkipStep;
        private static DelegateBridge __Hotfix_IsAnyUserGuideStepCanStart;
        private static DelegateBridge __Hotfix_ExecuteUserGuideTriggerPoint;
        private static DelegateBridge __Hotfix_TryLaunchUserGuideStep;
        private static DelegateBridge __Hotfix_IsInUserGuide;
        private static DelegateBridge __Hotfix_ResetUserGuideUITask;
        private static DelegateBridge __Hotfix_PrepareStartUserGuideTask;
        private static DelegateBridge __Hotfix_OnPrepareStartUserGuideTaskEnd;
        private static DelegateBridge __Hotfix_StartUserGuideStep;
        private static DelegateBridge __Hotfix_StartUserGuideStepInternal;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict;
        private static DelegateBridge __Hotfix_Prepare4Step;
        private static DelegateBridge __Hotfix_DefaultPrepare4Step;
        private static DelegateBridge __Hotfix_IsDefaultUserGuidePrepareSuccess;
        private static DelegateBridge __Hotfix_CheckUserGuideStep;
        private static DelegateBridge __Hotfix_CheckUserGuideStepCanStart;
        private static DelegateBridge __Hotfix_CheckIsStepGroupEqualCurrGroup;
        private static DelegateBridge __Hotfix_CheckIsUserGuideStepNeedSkip;
        private static DelegateBridge __Hotfix_DefaultPrepare4StepExec;
        private static DelegateBridge __Hotfix_DefaultOnUserGuideStepEnd;
        private static DelegateBridge __Hotfix_DefaultOnUITaskNofity;
        private static DelegateBridge __Hotfix_QuitCurrUserGuideStep;
        private static DelegateBridge __Hotfix_QuitCurrUserGuideGroup;
        private static DelegateBridge __Hotfix_SkipCurrStep;
        private static DelegateBridge __Hotfix_SkipCurrGroup;
        private static DelegateBridge __Hotfix_BreakCurrStep;
        private static DelegateBridge __Hotfix_RegisterUITaskNofityForPrepare;
        private static DelegateBridge __Hotfix_OnUITaskNotifyForPrepare;
        private static DelegateBridge __Hotfix_RegisterUITaskNofityForStepEnd;
        private static DelegateBridge __Hotfix_OnUITaskNotifyForStepEnd;
        private static DelegateBridge __Hotfix_GetUserGuideUITask;
        private static DelegateBridge __Hotfix_GetCurrUserGuideStepId;
        private static DelegateBridge __Hotfix_GetPrepare4StepFuncByStepName;
        private static DelegateBridge __Hotfix_GetPrepare4StepExecFuncByStepName;
        private static DelegateBridge __Hotfix_GetEndFuncByStepName;
        private static DelegateBridge __Hotfix_GetUserGuideStepFuncInfo;
        private static DelegateBridge __Hotfix_CreateUserGuideStepInfoFromStepId;
        private static DelegateBridge __Hotfix_GetSavePointUserGuideStepByGroupId;
        private static DelegateBridge __Hotfix_ClearCurrUserGuideStepInfo;
        private static DelegateBridge __Hotfix_IsLastStepInGroup;
        private static DelegateBridge __Hotfix_get_CurrUserGuideGroupId;
        private static DelegateBridge __Hotfix_set_CurrUserGuideGroupId;
        private static DelegateBridge __Hotfix_get_CurrUserGuideStepId;
        private static DelegateBridge __Hotfix_set_CurrUserGuideStepId;
        private static DelegateBridge __Hotfix_get_CurrUserGuideStepConfigInfo;
        private static DelegateBridge __Hotfix_set_CurrUserGuideStepConfigInfo;
        private static DelegateBridge __Hotfix_get_CurrUserGuideGroupConfigInfo;
        private static DelegateBridge __Hotfix_set_CurrUserGuideGroupConfigInfo;
        private static DelegateBridge __Hotfix_get_CurrWaitForNotifyTaskName;
        private static DelegateBridge __Hotfix_set_CurrWaitForNotifyTaskName;
        private static DelegateBridge __Hotfix_get_CurrWaitForNotifyDesc;
        private static DelegateBridge __Hotfix_set_CurrWaitForNotifyDesc;
        private static DelegateBridge __Hotfix_get_CurrWaitForNotifyStepInfo;
        private static DelegateBridge __Hotfix_set_CurrWaitForNotifyStepInfo;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Produce;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step2;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Produce_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Produce_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step5;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Produce_Step6;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step6;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step7;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step8;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Produce_Step9;
        private static DelegateBridge __Hotfix_GetGuideBluePrintItem;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Quest;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Ranking;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Scene;
        private static DelegateBridge __Hotfix_Prepare4Step_Solar_Weapon_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_SolarSystem_InteractWithWormholeGate_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_SolarSystem_InteractWithWormholeGate_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Solar_Navigation_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_SolarSystem_Wormhole_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_SolarSystem_InteractWithWormholeTunle_Step1;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SolarSystem_Wormhole_Step2;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SolarSystem_Wormhole_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_DrivingLicense_Destoyer_LV3_Step1;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_DrivingLicense_BattleCruiser_LV2_Step1;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4ShipHangar;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_ShipAssign_C2_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_ShipAssign_C2_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_ShipAssign_C2_Step3;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_ShipAssign_C2_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_ShipAssign_C2_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_EquipWeapon_Step2;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_EquipWeapon_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_EquipWeapon_Step4;
        private static DelegateBridge __Hotfix_GetItemConfigIdForStep8004;
        private static DelegateBridge __Hotfix_Prepare4StepExec_ShipHangar;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Shop;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_NpcShopStaticUI;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_NpcShopStaticUI;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4SolarSystemUI;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4StarMap;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_FreeQuest_Step5;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_FreeQuest_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_FreeQuest_Step5;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_FreeQuest_Step6;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_FreeQuest_Step6;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenStarField;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_EnterSolar;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_BaseRedeploy_RedeployButtonClick;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_BaseRedeploy_RedeployConfirm;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_BaseRedeploy_RedeployButton;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_BaseRedeploy_SelectStation;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_BaseRedeploy_ClickStation;
        private static DelegateBridge __Hotfix_Prepare4Step_Space_UnLockEmergencySingal_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_FreeQuest_Scientist_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_WormHole_Step5;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_WormHole_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_WormHole_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_WormHole_Step6;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_WormHole_Step9;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_WormHole_Step9;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_WormHole_Step10;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_WormHole_Step10;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateMineral_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateMineral_Step2;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateMineral_Step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateMineral_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_DelegateMineral_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_DelegateMineral_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_DelegateMineral_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_DelegateMineral_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateFight_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateFight_Step2;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateFight_Step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_DelegateFight_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_DelegateFight_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_DelegateFight_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_DelegateFight_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_DelegateFight_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_PVPSignal_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_PVPSignal_Step2;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_PVPSignal_Step2;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_PVPSignal_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_PVPSignal_Step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_PVPSignal_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_PVPSignal_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_PVPSignal_Step4;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_PVPSignal_Step5;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_RandomQuest_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_RandomQuest_Step3;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_RandomQuest_Step5;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_RandomQuest_Step5;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_RandomQuest_Step6;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_StrikeForDelagateMission;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Station;
        private static DelegateBridge __Hotfix_Prepare4Step_Activity_Quest1002_Step1;
        private static DelegateBridge __Hotfix_Prepare4Step_Station_AfterShipTemplate_Step1;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Tech;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_OpenTech;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Tech_ClickTechCategory;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Tech_ClickTechCategory;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Tech_ClickTechType;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Tech_ClickTechType;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Tech_Step3;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Tech_Step4;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_Station_Tech_GuideAfterQuest_Step7;
        private static DelegateBridge __Hotfix_GetTecchUITypeByGrandFaction;
        private static DelegateBridge __Hotfix_InitUserGuideStepFuncDict4Trade;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenTradeAtStation;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenPersonalTrade;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Trade_PersonalUI;
        private static DelegateBridge __Hotfix_Prepare4StepExec_Station_Trade_GuildPortUI;
        private static DelegateBridge __Hotfix_UserGuideDebug;
        private static DelegateBridge __Hotfix_Prepare4Step_SwitchToNoFriendlyTargetList;
        private static DelegateBridge __Hotfix_Prepare4Step_SwitchToBuildingTargetListSortWithNameUp;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SelectFirstTargetInTargetList;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickFirstTargetInTargetList;
        private static DelegateBridge __Hotfix_Prepare4Step_RestSolarSysmteUIToNormalState;
        private static DelegateBridge __Hotfix_Prepare4Step_SolarSystemCloseMenu;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickSolarNavigationPanel;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickSolarActivityButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_OpenSolarMenu;
        private static DelegateBridge __Hotfix_Prepare4Step_ResetToStationUINormalState;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationFunMenuButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipHangarButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickItemStoreButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickCrewDormitoryButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationMenuPanelActivityButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationActivityButton;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SelectHeadQuarter;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickHeadQuarter;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SelectShop;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShop;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickNpcList_StartNPCShop;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationStarMapButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickGuildButton;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SelectRanking;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickRank;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationStrikeButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationCharacterInfoPanel;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStationDrivingLicence;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickCrackButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickHighSlotButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipWeaponEquipReplaceButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickWeaponEquipSetFirstItem;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickWeaponEquipSetConfirmButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipTemplateButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipTemplateConfirmButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipTemplateCloseButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickShipAssignConfirmButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickStarMapForSolarSystemBackToStarfieldButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickCenterSolarSystem;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickSSNodePopMenu_EnterStarMapForSSButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickOpenCeletialListButton;
        private static DelegateBridge __Hotfix_Prepare4Step_ClickMannualSignalButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickMannualSignalButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickDelegateSignalButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickOpenDelegateSingnalListButton;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SetFristQuestSignalMenuItemArea;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickFristQuestSignalMenuItem;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickFristQuestSignalMenuItem4Soldier;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SetOrientedSolarSystemRect;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickOrientedSolarSystem;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickQuestAcceptDlgFirstText;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_ClickCharacterSkillButton;
        private static DelegateBridge __Hotfix_OnUserGuideStepEnd_UseSoleItem;
        private static DelegateBridge __Hotfix_Prepare4StepExec_DisablePassEvent;
        private static DelegateBridge __Hotfix_Prepare4StepExec_DisablePassEvent_CancelWithOutsideClick;
        private static DelegateBridge __Hotfix_Prepare4StepExec_CancelWithOutsideClick;
        private static DelegateBridge __Hotfix_Prepare4StepExec_SelectCenterPoint;
        private static DelegateBridge __Hotfix_GetSpaceStationUITask;
        private static DelegateBridge __Hotfix_GetNpcTalkerDialogUITask;
        private static DelegateBridge __Hotfix_GetNpcListDialogUITask;
        private static DelegateBridge __Hotfix_GetSolarSystemUITask;
        private static DelegateBridge __Hotfix_CheckUserGuideStepEndResult;
        private static DelegateBridge __Hotfix_GetGameObjectPath;
        private static DelegateBridge __Hotfix_PlayUserGuideProcessForPrepare;
        private static DelegateBridge __Hotfix_SwitchSolarSystemUITaskTargetList_0;
        private static DelegateBridge __Hotfix_SwitchSolarSystemUITaskTargetList_1;
        private static DelegateBridge __Hotfix_ResetToSolarSystemUINormalState;
        private static DelegateBridge __Hotfix_SolarSystemCloseMenu;
        private static DelegateBridge __Hotfix_CreateDelayTimeUIProcess;
        private static DelegateBridge __Hotfix_CreateDelayTickUIProcess;
        private static DelegateBridge __Hotfix_ResetToStationUINormalState;
        private static DelegateBridge __Hotfix_FocusOnNearestWormholeSolarSystem;
        private static DelegateBridge __Hotfix_FocusOnNearestnEmergencySolarSystem;
        private static DelegateBridge __Hotfix_CreateUIActionQueueItem;

        public static  event Action EventOnGroupEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void AutoStopStep()
        {
        }

        [MethodImpl(0x8000)]
        protected void BreakCurrStep()
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckAccpetQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckConfigConditionListAnd(UITaskBase task, List<UserGuideConditionInfo> conditionList, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckConfigConditionListOr(UITaskBase task, List<UserGuideConditionInfo> conditionList, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckDrivingLicenseUpgradeState(int value)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckExtraConfigConditionListAnd(UITaskBase task, List<ExtraUserGuideConditionInfo> conditionList, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckExtraSingleCondition(UITaskBase task, ExtraUserGuideConditionInfo condition, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckFixedQuestFinished(int questId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckInScene(int sceneId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsEqualFunctionState(int value, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsFunctionStateOpened(int value)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsInSpce(int isInSpace)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsLicenseFinishedOrNot(int lincenseId, bool isFinished)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsQuestUITaskStop(int stop)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsQuestWaitForComplete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsSkillFinishedOrNot(int skillId, bool isFinished)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsSolarSystemUIAvaliable()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsSpaceStationUITaskShowEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsStepGroupEqualCurrGroup(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsTechFinishedOrNot(int techId, bool isFinished)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType CheckIsUserGuideStepNeedSkip(UITaskBase targetUITask, int stepId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckIsWithMotherShip(int isWithMotherShip)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckItemStoreHasItem(int itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckLevel(int level, bool isGreater)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckNpcChatDialogEnd(int dialogId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckQuestAcceptOrComplete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckQuestFinishedOrNot(int questId, bool isFinished)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckQuestWaitForCompleteOrComplete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckSingleCondition(UITaskBase task, UserGuideConditionInfo condition, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckStoreHasAnyItem(string conditionParam)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckTextNoticeDialogId(int dialogId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckUnaccpetedQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType CheckUserGuideStep(UITaskBase targetUITask, int stepId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType CheckUserGuideStepCanStart(UITaskBase targetUITask, int stepId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected void CheckUserGuideStepEndResult(bool res, UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool CheckUserGuideStepFinished(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearCurrUserGuideStepInfo(bool isClearGroupInfo = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess CreateDelayTickUIProcess(uint delayTick)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess CreateDelayTimeUIProcess(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        public static UIManager.UIActionQueueItem CreateUIActionQueueItem(UITaskBase targetTask, UserGuideTriggerPoint triggerPoint, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuideStepInfo CreateUserGuideStepInfoFromStepId(UITaskBase targetUITask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static void DefaultOnStepEndForUnLockCaptain3DUI(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool DefaultOnUITaskNofity(int stepId, UITaskBase targetUITask, string notifyType, string notifyDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected static void DefaultOnUserGuideStepEnd(UITaskBase targetUITask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType DefaultPrepare4Step(UITaskBase targetUITask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static void DefaultPrepare4StepExec(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType DefaultPrepare4StepForCaptainUI(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public static void EnableGlobalUIInputByUserGuide(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public static bool ExecuteUserGuideTriggerPoint(UITaskBase targetUITask, UserGuideTriggerPoint triggerPointType, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess FocusOnNearestnEmergencySolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess FocusOnNearestWormholeSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetCharacterSkillTypeIdForStep22102()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetCharacterSkillTypeIndexForStep22102()
        {
        }

        [MethodImpl(0x8000)]
        protected static int GetCurrUserGuideStepId()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCustomLayerResPath(string configPath)
        {
        }

        [MethodImpl(0x8000)]
        protected Action<UITaskBase, UserGuideStepInfo> GetEndFuncByStepName(string stepName)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGameObjectPath(GameObject obj, GameObject rootObj, bool includeRootObj = true)
        {
        }

        [MethodImpl(0x8000)]
        private int GetGuideBluePrintItem()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetItemConfigIdForStep8004()
        {
        }

        [MethodImpl(0x8000)]
        protected NpcListDialogUITask GetNpcListDialogUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected NpcTalkerDialogUITask GetNpcTalkerDialogUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> GetPrepare4StepExecFuncByStepName(string stepName)
        {
        }

        [MethodImpl(0x8000)]
        protected Func<UITaskBase, int, UserGuidePrepare4StepResultType> GetPrepare4StepFuncByStepName(string stepName)
        {
        }

        [MethodImpl(0x8000)]
        protected static ConfigDataUserGuideStepInfo GetSavePointUserGuideStepByGroupId(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemUITask GetSolarSystemUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected SpaceStationUITask GetSpaceStationUITask()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTecchUITypeByGrandFaction(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected override UserGuideUIControllerBase GetUIController()
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuideStepFuncInfo GetUserGuideStepFuncInfo(string stepName)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuideUITask GetUserGuideUITask()
        {
        }

        [MethodImpl(0x8000)]
        private bool HasCustomLayer()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool InitDataFromUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Activity()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Auction()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4BranchStory()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4CaptainManage()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4CharacterInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Crack()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Development()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Explore()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4FactionCredit()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Guild()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4HeadQuarter()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Produce()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Quest()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Ranking()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Scene()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4ShipHangar()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Shop()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4SolarSystemUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4StarMap()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Station()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Tech()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitUserGuideStepFuncDict4Trade()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsAnyUserGuideStepCanStart(UITaskBase targetUITask, UserGuideTriggerPoint triggerPointType, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsDefaultUserGuidePrepareSuccess(UITaskBase targetUITask, int stepId, out UserGuidePrepare4StepResultType result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsInUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsLastStepInGroup(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBeginDrag(Vector2 position)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCustomLayerButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrag(Vector2 position)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEndDrag()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNpcChatEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnPrepareStartUserGuideTaskEnd(bool isSuccess, int stepId, UITaskBase targetUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStepEnd(bool isClickOperationArea)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStepEnd_Station_CharacterSkillGuide_Step1(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStepEnd_Station_CharacterSkillGuide_Step2(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStepEnd_Station_CharacterSkillGuide_Step3(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnStepEndInternal(UserGuideStepInfo stepInfo, Action<UITaskBase, UserGuideStepInfo> onEnd, bool isGroupEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStepStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUITaskNotifyForPrepare(UITaskBase target, string notifyType, string notifyDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUITaskNotifyForStepEnd(UITaskBase target, string notifyType, string notifyDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Assign(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_BranchStory_Quest20132_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_BranchStory_Quest20132_Step3(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_BranchStory_Quest20132_Step4(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_CaptainShipButtonClick(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_CaptainTrainButtonClick(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_CaptainWingManButtonClick(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickCenterSolarSystem(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickCharacterSkillButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickCrackButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickCrewDormitoryButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickDelegateSignalButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickDrivingLicenceDestroyer(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickFirstTargetInTargetList(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickFristQuestSignalMenuItem(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickFristQuestSignalMenuItem4Soldier(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickGuildButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickHeadQuarter(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickHighSlotButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickItemStoreButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickMannualSignalButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickNpcList_StartNPCShop(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickOpenCeletialListButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickOpenDelegateSingnalListButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickOrientedSolarSystem(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickQuestAcceptDlgFirstText(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickRank(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipAssignConfirmButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipHangarButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipTemplateButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipTemplateCloseButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipTemplateConfirmButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShipWeaponEquipReplaceButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickShop(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickSolarActivityButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickSolarNavigationPanel(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickSSNodePopMenu_EnterStarMapForSSButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStarMapForSolarSystemBackToStarfieldButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationActivityButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationCharacterInfoPanel(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationDrivingLicence(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationFunMenuButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationMenuPanelActivityButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationStarMapButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickStationStrikeButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickWeaponEquipSetConfirmButton(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClickWeaponEquipSetFirstItem(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ClosePuzzleGame(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_DrivingLicense_BattleCruiser_LV2_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_DrivingLicense_Destoyer_LV3_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_EnterSolar(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Exploit_Quest2825_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Exploit_Quest2825_Step3(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Exploit_Quest2825_Step4(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Exploit_Quest2825_Step5(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Exploit_Quest2825_Step6(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_FactionCredit_Quest20072_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_FactionCredit_Quest20072_Step3(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_ItemStoreOther(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenPersonalTrade(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenSolarMenu(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenStarField(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenStarMapAtSolar(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenStarMapAtStation(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_OpenTradeAtStation(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_SelectItem(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_SolarSystem_InteractWithWormholeGate_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_SolarSystem_InteractWithWormholeTunle_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Staion_CaptainLeraning_Step3(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Staion_CaptainLeraning_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_StartRecover(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Activity_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_BaseRedeploy_ClickStation(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_BaseRedeploy_RedeployButtonClick(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_BaseRedeploy_RedeployConfirm(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnUserGuideStepEnd_Station_ChapterGuide4_Step1(UITaskBase targetUITask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_ChipSlot_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_CrackBox_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_DelegateFight_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_DelegateFight_Step5(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_DelegateMineral_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_DelegateMineral_Step5(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_EquipWeapon_Step4(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_FreeQuest_Step5(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_FreeQuest_Step6(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_OpenAuction_Step1(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_OpenAuction_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_OpenTech(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step2(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step3(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step5(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step6(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step7(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step8(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Produce_Step9(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_PVPSignal_Step2(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_PVPSignal_Step3(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_PVPSignal_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_RandomQuest_Step5(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_ShipAssign_C2_Step3(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_ShipAssign_C2_Step4(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Tech_ClickTechCategory(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Tech_ClickTechType(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Tech_GuideAfterQuest_Step7(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Tech_Step3(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_Tech_Step4(UITaskBase targetTask, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_WormHole_Step10(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_WormHole_Step5(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_Station_WormHole_Step9(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUserGuideStepEnd_UseSoleItem(UITaskBase targetTask, UserGuideStepInfo stepConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayUserGuideProcessForPrepare(UIProcess process, UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step(UITaskBase targetUITask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Activity_Quest1002_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_ClickMannualSignalButton(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_ResetToStationUINormalState(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_RestSolarSysmteUIToNormalState(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Solar_Navigation_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Solar_Weapon_Step5(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_SolarSystem_InteractWithWormholeGate_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_SolarSystem_Wormhole_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_SolarSystemCloseMenu(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Space_UnLockEmergencySingal_Step5(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Staion_CaptainLeraning_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_AfterShipTemplate_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Station_CaptainUISetWingMan_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Station_ChipSlot_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Station_CrackBox_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateFight_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateFight_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateFight_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateFight_Step4(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateMineral_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateMineral_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateMineral_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DelegateMineral_Step4(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_DestroyerCheckGuide_Step4(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Station_EnterCaptainUI_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_FreeQuest_Scientist_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_FreeQuest_Step5(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_Guild_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_NpcShopStaticUI(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_PVPSignal_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_PVPSignal_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_PVPSignal_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_PVPSignal_Step4(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_RandomQuest_Step1(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_RandomQuest_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_RandomQuest_Step5(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_ShipAssign_C2_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_StrikeForDelagateMission(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_TreasureMap_Step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected static UserGuidePrepare4StepResultType Prepare4Step_Station_UnLockCaptainShip_CaptainShip_step3(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_WormHole_Step5(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_Station_WormHole_Step6(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_SwitchToBuildingTargetListSortWithNameUp(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected UserGuidePrepare4StepResultType Prepare4Step_SwitchToNoFriendlyTargetList(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_CancelWithOutsideClick(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_DisablePassEvent(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_DisablePassEvent_CancelWithOutsideClick(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Exploit_Main_Static1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Exploit_Quest2825_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_FactionCredit_Faction_Static1_Step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_FactionCredit_Quest20072_Step3(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_FactionCredit_Quest20072_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_GetFirstTargetFromCurrTargetList(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SelectCenterPoint(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SelectFirstTargetInTargetList(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SelectHeadQuarter(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SelectRanking(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SelectShop(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SetFristQuestSignalMenuItemArea(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SetOrientedSolarSystemRect(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_ShipHangar(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SolarSystem_Wormhole_Step2(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_SolarSystem_Wormhole_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Staion_CaptainLeraning_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_AuctionBuyStaticUI(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_BaseRedeploy_RedeployButton(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_BaseRedeploy_SelectStation(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_ChapterGuide4_Step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_CharacterSkillGuide_Step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_CharacterSkillGuide_Step2(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_CharacterSkillGuide_Step3(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_ChipSlot_Step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_DelegateFight_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_DelegateFight_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_DelegateMineral_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_DelegateMineral_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_DestroyerCheckGuide_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_EquipWeapon_Step2(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_EquipWeapon_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_FreeQuest_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_FreeQuest_Step6(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_NpcShopStaticUI(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_OpenAuction_Step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Produce_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Produce_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Produce_Step6(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_PVPSignal_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_PVPSignal_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_RandomQuest_Step6(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_ShipAssign_C2_Step3(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_ShipAssign_C2_Step4(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Tech_ClickTechCategory(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Tech_ClickTechType(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Trade_GuildPortUI(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_Trade_PersonalUI(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_TreasureMap_Step4(UITaskBase targetTask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Prepare4StepExec_Station_UnLockCaptainShip_CaptainShip_step1(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_WormHole_Step10(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_WormHole_Step5(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void Prepare4StepExec_Station_WormHole_Step9(UITaskBase targetUITask, ConfigDataUserGuideStepInfo stepConfigInfo, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void PrepareStartUserGuideTask(int stepId, UITaskBase targetUITask)
        {
        }

        [MethodImpl(0x8000)]
        public void PrintfMaskPanelState()
        {
        }

        [MethodImpl(0x8000)]
        protected void QuitCurrUserGuideGroup(UITaskBase targetTask)
        {
        }

        [MethodImpl(0x8000)]
        protected void QuitCurrUserGuideStep(UITaskBase targetTask)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUITaskNofityForPrepare(string taskName, string taskNofityDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUITaskNofityForStepEnd(string taskName, string taskNofityDesc, UserGuideStepInfo stepInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess ResetToSolarSystemUINormalState()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess ResetToStationUINormalState()
        {
        }

        [MethodImpl(0x8000)]
        public static void ResetUserGuideUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected static void SaveLogicStateOnStepEnd(int stepId, bool isSkip, bool isSkipGroup, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowOrHideUserGuide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUserGuide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected void SkipCurrGroup(UITaskBase targetUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected void SkipCurrStep(bool isSkipGroup, UITaskBase targetUITask)
        {
        }

        [MethodImpl(0x8000)]
        public static void SkipStep(int stepId, bool isSkipGroup, UITaskBase targetUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess SolarSystemCloseMenu()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartUserGuideStep(int stepId, UITaskBase targetTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartUserGuideStepInternal(ConfigDataUserGuideStepInfo stepConfigInfo, UITaskBase targetTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void StartUserGuideTaskForExecute(UserGuideStepInfo stepInfo, Action<UITaskBase, UserGuideStepInfo> endAction)
        {
        }

        [MethodImpl(0x8000)]
        public static UserGuideUITask StartUserGuideTaskForPrepare(int stepId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess StationDestroyerCheckGuideUIProcess(UITaskBase targetTask)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess StationItemStoreScroll(UITaskBase targetTask)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess SwitchSolarSystemUITaskTargetList(SolarSystemUITask.TargetListTableState tableState)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess SwitchSolarSystemUITaskTargetList(SolarSystemUITask.TargetListTableState tableState, SolarSystemUITask.TargetListSortState sortState)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForAutoStop()
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryLaunchUserGuideStep(UITaskBase targetUITask, int stepId, params object[] param)
        {
        }

        [MethodImpl(0x8000)]
        protected void TryStartUserGuide_Station_Guild_Step2(UITaskBase targetTask, int stepId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public static void UserGuideDebug(string msg)
        {
        }

        protected LogicBlockUserGuideClient LbUserGuide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int CurrUserGuideGroupId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int CurrUserGuideStepId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public ConfigDataUserGuideStepInfo CurrUserGuideStepConfigInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public ConfigDataUserGuideGroupInfo CurrUserGuideGroupConfigInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public string CurrWaitForNotifyTaskName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public string CurrWaitForNotifyDesc
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public UserGuideStepInfo CurrWaitForNotifyStepInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateDelayTickUIProcess>c__AnonStorey71
        {
            internal uint delayTick;
            internal UserGuideUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                <CreateDelayTickUIProcess>c__AnonStorey72 storey = new <CreateDelayTickUIProcess>c__AnonStorey72 {
                    <>f__ref$113 = this,
                    onEnd = onEnd
                };
                this.$this.PostDelayTicksExecuteAction(new Action(storey.<>m__0), (ulong) this.delayTick);
            }

            private sealed class <CreateDelayTickUIProcess>c__AnonStorey72
            {
                internal Action<bool> onEnd;
                internal UserGuideUITask.<CreateDelayTickUIProcess>c__AnonStorey71 <>f__ref$113;

                internal void <>m__0()
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateDelayTimeUIProcess>c__AnonStorey6F
        {
            internal float delayTime;
            internal UserGuideUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                <CreateDelayTimeUIProcess>c__AnonStorey70 storey = new <CreateDelayTimeUIProcess>c__AnonStorey70 {
                    <>f__ref$111 = this,
                    onEnd = onEnd
                };
                this.$this.PostDelayTimeExecuteAction(new Action(storey.<>m__0), this.delayTime);
            }

            private sealed class <CreateDelayTimeUIProcess>c__AnonStorey70
            {
                internal Action<bool> onEnd;
                internal UserGuideUITask.<CreateDelayTimeUIProcess>c__AnonStorey6F <>f__ref$111;

                internal void <>m__0()
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateUIActionQueueItem>c__AnonStorey73
        {
            internal UITaskBase targetTask;
            internal UserGuideTriggerPoint triggerPoint;
            internal object[] param;
            internal UserGuideUITask.UIActionQueueItem4UserGuide actionItem;

            internal void <>m__0()
            {
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.targetTask, this.triggerPoint, this.param);
                this.actionItem.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <OnStepEnd_Station_CharacterSkillGuide_Step1>c__AnonStoreyB
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnStepEnd_Station_CharacterSkillGuide_Step2>c__AnonStoreyC
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnStepEnd_Station_CharacterSkillGuide_Step3>c__AnonStoreyD
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Assign>c__AnonStorey1D
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_BranchStory_Quest20132_Step2>c__AnonStorey3
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ActivityInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_BranchStory_Quest20132_Step3>c__AnonStorey4
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("BranchStoryUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_BranchStory_Quest20132_Step4>c__AnonStorey5
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("BranchStoryUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_CaptainShipButtonClick>c__AnonStoreyA
        {
            internal CaptainUITask captainUITask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.captainUITask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_CaptainTrainButtonClick>c__AnonStorey9
        {
            internal CaptainUITask captainUITask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.captainUITask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_CaptainWingManButtonClick>c__AnonStorey8
        {
            internal CaptainUITask captainUITask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.captainUITask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickCenterSolarSystem>c__AnonStorey62
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickCharacterSkillButton>c__AnonStorey6A
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("CharacterSkillUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickCrackButton>c__AnonStorey5A
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("CrackManagerUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickCrewDormitoryButton>c__AnonStorey4E
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("CaptainUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickDelegateSignalButton>c__AnonStorey65
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickDrivingLicenceDestroyer>c__AnonStoreyF
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("DrivingLiscenseCheckUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickFirstTargetInTargetList>c__AnonStorey48
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickFristQuestSignalMenuItem>c__AnonStorey67
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickFristQuestSignalMenuItem4Soldier>c__AnonStorey68
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickGuildButton>c__AnonStorey55
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("GuildInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickHeadQuarter>c__AnonStorey51
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UITaskBase targetTask;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("NpcListDialogUITask", true);
                if (targetTask != null)
                {
                    this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
                }
                else
                {
                    this.$this.CheckUserGuideStepEndResult(false, this.targetTask, this.stepConfigInfo);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickHighSlotButton>c__AnonStorey5B
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickItemStoreButton>c__AnonStorey4D
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ItemStoreUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickMannualSignalButton>c__AnonStorey64
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickNpcList_StartNPCShop>c__AnonStorey53
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, UIManager.Instance.FindUITaskWithName("NpcShopMainUITask", true), this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickOpenCeletialListButton>c__AnonStorey63
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.CheckUserGuideStepEndResult(true, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickOpenDelegateSingnalListButton>c__AnonStorey66
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.CheckUserGuideStepEndResult(true, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickOrientedSolarSystem>c__AnonStorey69
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickRank>c__AnonStorey56
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShipHangarButton>c__AnonStorey4C
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ShipHangarUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShipTemplateButton>c__AnonStorey5F
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ShipCustomTemplateUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShipTemplateCloseButton>c__AnonStorey61
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShipTemplateConfirmButton>c__AnonStorey60
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShipWeaponEquipReplaceButton>c__AnonStorey5C
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                WeaponEquipSetUITask targetTask = UIManager.Instance.FindUITaskWithName("WeaponEquipSetUITask", true) as WeaponEquipSetUITask;
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickShop>c__AnonStorey52
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickSolarActivityButton>c__AnonStorey49
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationActivityButton>c__AnonStorey50
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ActionPlanManagerUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationCharacterInfoPanel>c__AnonStorey58
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("CharacterInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationDrivingLicence>c__AnonStorey59
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.m_currIntent, "DrivingLicenseInfoUITask", null);
                UIManager.Instance.StartUITask(intent, true, false, null, delegate (bool res) {
                    UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("DrivingLicenseInfoUITask", true);
                    this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
                });
            }

            internal void <>m__1(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("DrivingLicenseInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationFunMenuButton>c__AnonStorey4B
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationMenuPanelActivityButton>c__AnonStorey4F
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ActionPlanManagerUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationStarMapButton>c__AnonStorey54
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("StarMapForSolarSystemUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickStationStrikeButton>c__AnonStorey57
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("StarMapForSolarSystemUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickWeaponEquipSetConfirmButton>c__AnonStorey5E
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ShipHangarUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClickWeaponEquipSetFirstItem>c__AnonStorey5D
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ClosePuzzleGame>c__AnonStorey1E
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_DrivingLicense_BattleCruiser_LV2_Step1>c__AnonStorey2D
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_EnterSolar>c__AnonStorey32
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                StarMapForSolarSystemUITask targetTask = UIManager.Instance.FindUITaskWithName("StarMapForSolarSystemUITask", true) as StarMapForSolarSystemUITask;
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Exploit_Quest2825_Step2>c__AnonStorey12
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("MotherShipUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Exploit_Quest2825_Step3>c__AnonStorey13
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("MotherShipUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Exploit_Quest2825_Step4>c__AnonStorey14
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("MotherShipUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Exploit_Quest2825_Step5>c__AnonStorey15
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("MotherShipUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Exploit_Quest2825_Step6>c__AnonStorey16
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("MotherShipUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_FactionCredit_Quest20072_Step2>c__AnonStorey1F
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ActivityInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_FactionCredit_Quest20072_Step3>c__AnonStorey20
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("FactionCreditDetailUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_ItemStoreOther>c__AnonStorey1A
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_OpenPersonalTrade>c__AnonStorey47
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("TradeUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_OpenSolarMenu>c__AnonStorey4A
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_OpenStarMapAtSolar>c__AnonStorey18
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_OpenStarMapAtStation>c__AnonStorey19
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                StarMapForSolarSystemUITask targetTask = UIManager.Instance.FindUITaskWithName("StarMapForSolarSystemUITask", true) as StarMapForSolarSystemUITask;
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_OpenTradeAtStation>c__AnonStorey46
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("TradeEntryPanelUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_SelectItem>c__AnonStorey1B
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Staion_CaptainLeraning_Step3>c__AnonStorey6
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Staion_CaptainLeraning_Step4>c__AnonStorey7
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_StartRecover>c__AnonStorey1C
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("PuzzleGameUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Activity_Step2>c__AnonStorey0
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ActivityInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_BaseRedeploy_ClickStation>c__AnonStorey35
        {
            internal StarMapForSolarSystemUITask starMapForSolarSystemUITask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.starMapForSolarSystemUITask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_BaseRedeploy_RedeployButtonClick>c__AnonStorey33
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                BaseRedeployUITask targetTask = UIManager.Instance.FindUITaskWithName("BaseRedeployUITask", true) as BaseRedeployUITask;
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_BaseRedeploy_RedeployConfirm>c__AnonStorey34
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_ChipSlot_Step1>c__AnonStorey10
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_CrackBox_Step1>c__AnonStorey11
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_DelegateFight_Step4>c__AnonStorey3A
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_DelegateFight_Step5>c__AnonStorey3B
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("SignalStrikeUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_DelegateMineral_Step4>c__AnonStorey38
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_DelegateMineral_Step5>c__AnonStorey39
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("SignalStrikeUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_EquipWeapon_Step4>c__AnonStorey30
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_FreeQuest_Step5>c__AnonStorey31
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_OpenAuction_Step1>c__AnonStorey1
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UITaskBase targetTask;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("NpcListDialogUITask", true);
                if (targetTask != null)
                {
                    this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
                }
                else
                {
                    this.$this.CheckUserGuideStepEndResult(false, this.targetTask, this.stepConfigInfo);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_OpenAuction_Step2>c__AnonStorey2
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("AuctionUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_OpenTech>c__AnonStorey40
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("TechUpgradeManageUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step2>c__AnonStorey26
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ProduceUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step3>c__AnonStorey27
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ProduceItemStoreUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step4>c__AnonStorey28
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step5>c__AnonStorey29
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step6>c__AnonStorey2A
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step7>c__AnonStorey2B
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ProduceUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Produce_Step8>c__AnonStorey2C
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_PVPSignal_Step2>c__AnonStorey3C
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_PVPSignal_Step3>c__AnonStorey3D
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.CheckUserGuideStepEndResult(true, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_PVPSignal_Step4>c__AnonStorey3E
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_RandomQuest_Step5>c__AnonStorey3F
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.CheckUserGuideStepEndResult(true, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_ShipAssign_C2_Step3>c__AnonStorey2E
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("ShipHangarAssignUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_ShipAssign_C2_Step4>c__AnonStorey2F
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Tech_ClickTechCategory>c__AnonStorey41
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Tech_ClickTechType>c__AnonStorey42
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Tech_GuideAfterQuest_Step7>c__AnonStorey45
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("TechUpgradeManageUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Tech_Step3>c__AnonStorey43
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_Tech_Step4>c__AnonStorey44
        {
            internal UserGuideStepInfo stepInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("TechUpgradeInfoUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_WormHole_Step5>c__AnonStorey36
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_Station_WormHole_Step9>c__AnonStorey37
        {
            internal UITaskBase targetTask;
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.CheckUserGuideStepEndResult(res, this.targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUserGuideStepEnd_UseSoleItem>c__AnonStorey6B
        {
            internal UserGuideStepInfo stepConfigInfo;
            internal UserGuideUITask $this;

            internal void <>m__0(bool res)
            {
                UITaskBase targetTask = UIManager.Instance.FindUITaskWithName("QuestListUITask", true);
                this.$this.CheckUserGuideStepEndResult(res, targetTask, this.stepConfigInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <PlayUserGuideProcessForPrepare>c__AnonStorey6C
        {
            internal int stepId;
            internal UITaskBase targetTask;
            internal UserGuideUITask $this;

            internal void <>m__0(UIProcess p, bool res)
            {
                UserGuideUITask.UserGuideDebug($"step:{this.stepId} PlayUserGuideProcess end");
                if (res)
                {
                    this.$this.StartUserGuideStep(this.stepId, this.targetTask);
                }
                else
                {
                    this.$this.QuitCurrUserGuideStep(this.targetTask);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Prepare4Step_Station_Guild_Step2>c__AnonStorey21
        {
            internal UITaskBase targetTask;
            internal int stepId;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.TryStartUserGuide_Station_Guild_Step2(this.targetTask, this.stepId);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartUserGuideTask>c__AnonStorey24
        {
            internal int stepId;
            internal UITaskBase targetUITask;

            internal void <>m__0(bool res)
            {
                UserGuideUITask.UserGuideDebug($"PrepareStartUserGuideTask end stepId = {this.stepId}");
                UserGuideUITask.OnPrepareStartUserGuideTaskEnd(res, this.stepId, this.targetUITask);
            }
        }

        [CompilerGenerated]
        private sealed class <SkipStep>c__AnonStorey23
        {
            internal bool isSkipGroup;
            internal ConfigDataUserGuideStepInfo skipStep;
            internal UITaskBase targetUITask;

            internal void <>m__0()
            {
                if (!this.isSkipGroup)
                {
                    bool isGroupEnd = UserGuideUITask.IsLastStepInGroup(this.skipStep.ID);
                    UserGuideStepInfo stepInfo = new UserGuideStepInfo {
                        m_stepId = this.skipStep.ID,
                        m_targetTask = this.targetUITask
                    };
                    if (UserGuideUITask.<>f__mg$cache4 == null)
                    {
                        UserGuideUITask.<>f__mg$cache4 = new Action<UITaskBase, UserGuideStepInfo>(UserGuideUITask.DefaultOnUserGuideStepEnd);
                    }
                    UserGuideUITask.OnStepEndInternal(stepInfo, UserGuideUITask.<>f__mg$cache4, isGroupEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartUserGuideStep>c__AnonStorey25
        {
            internal ConfigDataUserGuideStepInfo stepConfigInfo;
            internal UITaskBase targetTarget;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.StartUserGuideStepInternal(this.stepConfigInfo, this.targetTarget);
            }
        }

        [CompilerGenerated]
        private sealed class <StationDestroyerCheckGuideUIProcess>c__AnonStoreyE
        {
            internal UITaskBase targetTask;

            internal void <>m__0(Action<bool> onEnd)
            {
                DrivingLicenseInfoUITask targetTask = this.targetTask as DrivingLicenseInfoUITask;
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                if ((targetTask == null) || (playerContext == null))
                {
                    onEnd(false);
                }
                else
                {
                    targetTask.SetFactionToggle(playerContext.GetLBCharacter().GetMotherLand());
                    targetTask.UpdatePanel(playerContext.GetLBCharacter().GetMotherLand(), onEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StationItemStoreScroll>c__AnonStorey17
        {
            internal UITaskBase targetTask;

            internal void <>m__0(Action<bool> onEnd)
            {
                ItemStoreUITask targetTask = this.targetTask as ItemStoreUITask;
                if (targetTask != null)
                {
                    targetTask.ScrollToEnd(onEnd);
                }
                else
                {
                    onEnd(false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SwitchSolarSystemUITaskTargetList>c__AnonStorey6D
        {
            internal SolarSystemUITask solarSystemUITask;
            internal SolarSystemUITask.TargetListTableState tableState;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.solarSystemUITask.SetCurrTargetListTableState(this.tableState, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <SwitchSolarSystemUITaskTargetList>c__AnonStorey6E
        {
            internal SolarSystemUITask solarSystemUITask;
            internal SolarSystemUITask.TargetListTableState tableState;
            internal SolarSystemUITask.TargetListSortState sortState;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.solarSystemUITask.SetCurrTargetListTableStateWithSortRule(this.tableState, this.sortState, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <TryStartUserGuide_Station_Guild_Step2>c__AnonStorey22
        {
            internal UITaskBase targetTask;
            internal int stepId;
            internal UserGuideUITask $this;

            internal void <>m__0()
            {
                this.$this.TryStartUserGuide_Station_Guild_Step2(this.targetTask, this.stepId);
            }
        }

        public class CachedUserGuideTriggerPoint
        {
            public UserGuideTriggerPoint m_triggerPointType;
            public UITaskBase m_targetUITask;
            private static DelegateBridge _c__Hotfix_ctor;

            public CachedUserGuideTriggerPoint()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum DragFlagType
        {
            NoneDrag = 0,
            DragLeft = 1,
            DragRight = 2,
            DragUp = 4,
            DragDown = 8
        }

        public class PageInfo : UserGuideStepInfo.IPageInfo
        {
            public bool m_isShowNPCDialog;
            public string m_npcIconPath;
            public string m_npcName;
            public string m_dialogStr;
            public bool m_isRemoteNPC;
            public string m_dialogChooseAnswerStr;
            public string m_dialogAudioPath;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsShowNPCDialog;
            private static DelegateBridge __Hotfix_GetNPCIconPath;
            private static DelegateBridge __Hotfix_GetNPCName;
            private static DelegateBridge __Hotfix_GetDialogStr;
            private static DelegateBridge __Hotfix_IsRemoteNPC;
            private static DelegateBridge __Hotfix_GetDialogChooseAnswerStr;
            private static DelegateBridge __Hotfix_GetDialogAudioPath;

            public PageInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public string GetDialogAudioPath()
            {
                DelegateBridge bridge = __Hotfix_GetDialogAudioPath;
                return ((bridge == null) ? this.m_dialogAudioPath : bridge.__Gen_Delegate_Imp33(this));
            }

            public string GetDialogChooseAnswerStr()
            {
                DelegateBridge bridge = __Hotfix_GetDialogChooseAnswerStr;
                return ((bridge == null) ? this.m_dialogChooseAnswerStr : bridge.__Gen_Delegate_Imp33(this));
            }

            public string GetDialogStr()
            {
                DelegateBridge bridge = __Hotfix_GetDialogStr;
                return ((bridge == null) ? this.m_dialogStr : bridge.__Gen_Delegate_Imp33(this));
            }

            public string GetNPCIconPath()
            {
                DelegateBridge bridge = __Hotfix_GetNPCIconPath;
                return ((bridge == null) ? this.m_npcIconPath : bridge.__Gen_Delegate_Imp33(this));
            }

            public string GetNPCName()
            {
                DelegateBridge bridge = __Hotfix_GetNPCName;
                return ((bridge == null) ? this.m_npcName : bridge.__Gen_Delegate_Imp33(this));
            }

            public bool IsRemoteNPC()
            {
                DelegateBridge bridge = __Hotfix_IsRemoteNPC;
                return ((bridge == null) ? this.m_isRemoteNPC : bridge.__Gen_Delegate_Imp9(this));
            }

            public bool IsShowNPCDialog()
            {
                DelegateBridge bridge = __Hotfix_IsShowNPCDialog;
                return ((bridge == null) ? this.m_isShowNPCDialog : bridge.__Gen_Delegate_Imp9(this));
            }
        }

        public class UIActionQueueItem4UserGuide : UIManager.UIActionQueueItem
        {
            public UserGuideTriggerPoint m_userGuideTriggerPoint;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnUserGuideGroupEnd;

            public UIActionQueueItem4UserGuide()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void OnUserGuideGroupEnd()
            {
                DelegateBridge bridge = __Hotfix_OnUserGuideGroupEnd;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    UserGuideUITask.EventOnGroupEnd -= new Action(this.OnUserGuideGroupEnd);
                    base.OnEnd();
                }
            }
        }

        public class UserGuidePrepare4StepResult
        {
            public UserGuideUITask.UserGuidePrepare4StepResultType m_resultType = UserGuideUITask.UserGuidePrepare4StepResultType.Execute;
            public float m_delayTime;
            private static DelegateBridge _c__Hotfix_ctor;

            public UserGuidePrepare4StepResult()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum UserGuidePrepare4StepResultType
        {
            None,
            CannotExecute,
            Execute,
            ExecuteOnEvent,
            SkipStep,
            SkipGroup
        }

        public class UserGuideStepFuncInfo
        {
            public Func<UITaskBase, int, UserGuideUITask.UserGuidePrepare4StepResultType> Prepare4Step;
            public Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> Prepare4StepExec;
            public Action<UITaskBase, UserGuideStepInfo> OnUserGuideStepEnd;
            private static DelegateBridge _c__Hotfix_ctor;

            public UserGuideStepFuncInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

