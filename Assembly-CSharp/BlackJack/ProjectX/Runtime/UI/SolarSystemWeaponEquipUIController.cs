﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemWeaponEquipUIController : UIControllerBase
    {
        public List<SolarSystemWeaponEquipItemUIController> HighSlotUICtrlList;
        public List<SolarSystemWeaponEquipItemUIController> MiddleSlotUICtrlList;
        public SolarSystemWeaponEquipItemUIController DefaultBoosterEquipInfoUICtrl;
        public SolarSystemWeaponEquipItemUIController DefaultRepairerEquipInfoUICtrl;
        public SolarSystemWeaponEquipItemUIController FlagShipTeleportInfoUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnFlagShipTeleportButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnWeaponEquipGroupButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnWeaponEquipButtonLongPressStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnWeaponEquipButtonLongPressEnd;
        private List<Button> m_weaponButtonList;
        private List<Button> m_activeEquipButtonList;
        [AutoBind("./WeaponPanelUnlockEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeaponPanelUnlockEffectStateCtrl;
        [AutoBind("./HighSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HighSlotRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/HighSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HighSlotRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/HighSlotGroup/HighSlotButton1", AutoBindAttribute.InitState.NotInit, false)]
        public Button HighSlotButton1;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/HighSlotGroup/HighSlotButton2", AutoBindAttribute.InitState.NotInit, false)]
        public Button HighSlotButton2;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/HighSlotGroup/HighSlotButton3", AutoBindAttribute.InitState.NotInit, false)]
        public Button HighSlotButton3;
        [AutoBind("./MiddleSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MiddleSlotRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/MiddleSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MiddleSlotRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/MiddleSlotGroup/MiddleSlotButton3", AutoBindAttribute.InitState.NotInit, false)]
        public Button MiddleSlotButton1;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/MiddleSlotGroup/MiddleSlotButton2", AutoBindAttribute.InitState.NotInit, false)]
        public Button MiddleSlotButton2;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/MiddleSlotGroup/MiddleSlotButton1", AutoBindAttribute.InitState.NotInit, false)]
        public Button MiddleSlotButton3;
        [AutoBind("./SuperWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject SuperWeaponRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/SuperWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject SuperWeaponRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/SuperWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SuperWeaponStateCtrl;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/SuperWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SuperWeaponButton;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/SuperWeaponButton/ChargeProgressBar1", AutoBindAttribute.InitState.NotInit, false)]
        public Image SuperWeaponChargeProgress1;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/SuperWeaponButton/ChargeProgressBar2", AutoBindAttribute.InitState.NotInit, false)]
        public Image SuperWeaponChargeProgress2;
        [AutoBind("./SuperWeaponButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SuperWeaponIconImage;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/NormalAttackWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NormalAttackWeaponRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/NormalAttackWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button NormalAttackWeaponButton;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/NormalAttackWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NormalAttackWeaponButtonStateCtrl;
        [AutoBind("./BoosterButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefaultBoosterEquipRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/BoosterButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefaultBoosterEquipRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/BoosterButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DefaultBoosterEquipButton;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/BoosterButton/BoosterUnlockEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DefaultBoosterUnlockEffectStateCtrl;
        [AutoBind("./RepairerButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefaultRepairerEquipRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/RepairerButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefaultRepairerEquipRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/RepairerButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DefaultRepairerEquipButton;
        [AutoBind("./FlagShipTeleportButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FlagShipTeleportRoot;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/FlagShipTeleportButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FlagShipTeleportRootVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/FlagShipTeleportButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button FlagShipTeleportButton;
        [AutoBind("./HoldFireButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button HoldFireButton;
        [AutoBind("./HoldFireButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HoldFireCtrl;
        [AutoBind("./FireCtrlDelegateToAIButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireCtrlDelegateToAIRoot;
        [AutoBind("./FireCtrlDelegateToAIButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FireCtrlDelegateToAIRootButton;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/FireCtrlDelegateToAIButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireCtrlDelegateToAIRootVolatile;
        [AutoBind("./HUDButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HUDRoot;
        [AutoBind("./HUDButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button HUDButton;
        [AutoBind("./GuildFleetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildFleetButton;
        [AutoBind("./SlotSimpleInfoDummyGroup/HighSlotSimpleInfoDummy1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_highSlotSimpleInfoTrans1;
        [AutoBind("./SlotSimpleInfoDummyGroup/HighSlotSimpleInfoDummy2", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_highSlotSimpleInfoTrans2;
        [AutoBind("./SlotSimpleInfoDummyGroup/HighSlotSimpleInfoDummy3", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_highSlotSimpleInfoTrans3;
        [AutoBind("./SlotSimpleInfoDummyGroup/MiddleSlotSimpleInfoDummy1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_middleSlotSimpleInfoTrans1;
        [AutoBind("./SlotSimpleInfoDummyGroup/MiddleSlotSimpleInfoDummy2", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_middleSlotSimpleInfoTrans2;
        [AutoBind("./SlotSimpleInfoDummyGroup/MiddleSlotSimpleInfoDummy3", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_middleSlotSimpleInfoTrans3;
        [AutoBind("./SlotSimpleInfoDummyGroup/DefaultBoosterSlotSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defaultBoosterSlotSimpleInfoTrans;
        [AutoBind("./SlotSimpleInfoDummyGroup/DefaultRepairerSlotSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defaultRepairerSlotSimpleInfoTrans;
        [AutoBind("./SlotSimpleInfoDummyGroup/SuperWeaponSlotSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_superWeaponSlotSimpleInfoTrans;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponEquipUIPanelVolatile;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/WeaponEffectPannel/UIFX_SuperWeaponReady_Pfb", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SuperWeaponReadyOkEffect;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/WeaponEffectPannel/UIFX_SuperWeaponActivated_Pfb", AutoBindAttribute.InitState.NotInit, false)]
        public EffectRectUVController SuperWeaponIsActiveEffect;
        [AutoBindWithParent("../CanvasVolatile/WeaponEquipUIPanel/WeaponEffectPannel/UIFX_WeaponButtomDown_Pfb", AutoBindAttribute.InitState.NotInit, false)]
        public EffectRectUVController NormalAttackWeaponClickEffect;
        private string m_superWeaponEquipStateCharge;
        private string m_superWeaponEquipStateChargeFinished;
        private string m_superWeaponEquipStateEmpty;
        private bool m_isPointerDownForNormalAttackButton;
        private float m_pointerDownForNormalAttackButtonKeepingTime;
        private bool m_isPointerIntoNormalAttackButtonRange;
        private bool m_isPointerIntoHoldFireButtonRange;
        private const float m_holdFireButtonActivateInterval = 0.5f;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_InitAllWeaponEquipGroupUI;
        private static DelegateBridge __Hotfix_PlayWeaponEquipLaunchableEffect;
        private static DelegateBridge __Hotfix_GetShipSlotSimpleInfoDummy;
        private static DelegateBridge __Hotfix_EnableWeaponEquipUIPanel;
        private static DelegateBridge __Hotfix_EnableWeaponUIPanel;
        private static DelegateBridge __Hotfix_CreateWeaponPanelUnlockEffectProcess;
        private static DelegateBridge __Hotfix_ShowWeaponUIPanelUnlockEffect;
        private static DelegateBridge __Hotfix_EnableAutoFirePanel;
        private static DelegateBridge __Hotfix_SetNormalAttackWeaponButtonState;
        private static DelegateBridge __Hotfix_EnableDefalutBooster;
        private static DelegateBridge __Hotfix_CreateDefaultBoosterUnlockEffectProcess;
        private static DelegateBridge __Hotfix_EnableDefaultRepair;
        private static DelegateBridge __Hotfix_EnableFlagShipTelport;
        private static DelegateBridge __Hotfix_EnableEquipUIPanel;
        private static DelegateBridge __Hotfix_EnableHUDPanel;
        private static DelegateBridge __Hotfix_ShowGuildFleetButton;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipGroupList;
        private static DelegateBridge __Hotfix_UpdateSuperWeaponEquipGroupInfo;
        private static DelegateBridge __Hotfix_SetSuperWeaponEquipState;
        private static DelegateBridge __Hotfix_ShowHideSuperWeaponEnergyFullEffect;
        private static DelegateBridge __Hotfix_PlayNormalAttackButtonClickEffect;
        private static DelegateBridge __Hotfix_SwitchFireCtrlDelegateToAIState;
        private static DelegateBridge __Hotfix_PlayAutoAmmoReloadEffect;
        private static DelegateBridge __Hotfix_OnFlagShipTeleportButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponGroupButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipGroupLongPressStart;
        private static DelegateBridge __Hotfix_OnWeaponEquipGroupLongPressEnd;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipGroupButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipGroupButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipButtonLongPressStart;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipButtonLongPressStart;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipButtonLongPressEnd;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipButtonLongPressEnd;
        private static DelegateBridge __Hotfix_get_WeaponButtonList;
        private static DelegateBridge __Hotfix_get_ActiveEquipButtonList;
        private static DelegateBridge __Hotfix_RegisteHoldFireOperationUIEvent;
        private static DelegateBridge __Hotfix_OnPointerDown_NormalAttackButton;
        private static DelegateBridge __Hotfix_OnPointerUp_NormalAttackButton;
        private static DelegateBridge __Hotfix_OnPointerLeave_NormalAttackButton;
        private static DelegateBridge __Hotfix_OnPointerEnter_HoldFireButton;
        private static DelegateBridge __Hotfix_OnPointerLeave_HoldFireButton;
        private static DelegateBridge __Hotfix_UpdateForHoldFireButtonActivate;

        public event Action<UIControllerBase> EventOnFlagShipTeleportButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponEquipButtonLongPressEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponEquipButtonLongPressStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponEquipGroupButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateDefaultBoosterUnlockEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateWeaponPanelUnlockEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableAutoFirePanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableDefalutBooster(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableDefaultRepair(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableEquipUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableFlagShipTelport(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableHUDPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableWeaponEquipUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableWeaponUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetShipSlotSimpleInfoDummy(GameObject slotGameObject)
        {
        }

        [MethodImpl(0x8000)]
        public void InitAllWeaponEquipGroupUI(List<SolarSystemUITask.WeaponEquipGroupItemUIInfo> weaponEquipInfoList, LBSuperWeaponEquipGroupBase superWeaponEquipGroup, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFlagShipTeleportButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointerDown_NormalAttackButton(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointerEnter_HoldFireButton(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointerLeave_HoldFireButton(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointerLeave_NormalAttackButton(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPointerUp_NormalAttackButton(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipGroupLongPressEnd(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipGroupLongPressStart(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponGroupButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayAutoAmmoReloadEffect(int index, ItemInfo ammoReloadInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayNormalAttackButtonClickEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayWeaponEquipLaunchableEffect(SolarSystemUITask.WeaponEquipGroupItemUIInfo weaponEquipInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisteHoldFireOperationUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNormalAttackWeaponButtonState(bool isInFighting)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSuperWeaponEquipState(string state)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowGuildFleetButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowHideSuperWeaponEnergyFullEffect(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWeaponUIPanelUnlockEffect(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchFireCtrlDelegateToAIState(bool isClickToAutoAttack)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateForHoldFireButtonActivate()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSuperWeaponEquipGroupInfo(LBSuperWeaponEquipGroupBase supGroup, bool hasTargetOnFireRange, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponEquipGroupList(List<SolarSystemUITask.WeaponEquipGroupItemUIInfo> weaponEquipInfoList)
        {
        }

        public List<Button> WeaponButtonList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<Button> ActiveEquipButtonList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

