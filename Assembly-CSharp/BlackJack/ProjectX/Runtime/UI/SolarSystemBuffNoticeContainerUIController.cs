﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemBuffNoticeContainerUIController : UIControllerBase
    {
        [AutoBind("./UsedBuffNoticeRoot/BuffNoticeDummy1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_buffNoticeDummy1;
        [AutoBind("./UsedBuffNoticeRoot/BuffNoticeDummy2", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_buffNoticeDummy2;
        [AutoBind("./UsedBuffNoticeRoot/BuffNoticeDummy3", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_buffNoticeDummy3;
        [AutoBind("./HidingBuffNoticeRoot/AttackBuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_attackBuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/ElectricBuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_electricBuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/DefenseBuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defenseBuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/MoveBuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_moveBuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/AttackDebuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_attackDebuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/ElectricDebuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_electricDebuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/DefenseDebuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defenseDebuffHidePosDummy;
        [AutoBind("./HidingBuffNoticeRoot/MoveDebuffHidePosDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_moveDebuffHidePosDummy;
        [AutoBind("./UnusedBuffNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_unusedBuffNoticeRoot;
        private Transform[] m_buffNoticeDummyList;
        private SolarSystemBuffNoticeItemUIController[] m_currBuffNoticeList;
        private float[] m_buffNoticeCreatingTime;
        private LinkedList<SolarSystemBuffNoticeItemUIController.BuffTypeUIInfo> m_waitingBufUIInfoList;
        private LinkedList<SolarSystemBuffNoticeItemUIController> m_freeBuffUIItemList;
        private const float m_minBuffInfoHoldTimeLength = 1f;
        private const float m_maxBuffInfoHoldTimeLength = 2f;
        private static DelegateBridge __Hotfix_AddBuffUIItemFromBuffInfo;
        private static DelegateBridge __Hotfix_SetBuffNoticeTimeout;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetFreeBuffNoticeItem;
        private static DelegateBridge __Hotfix_CreateNewBuffNoticeItem;
        private static DelegateBridge __Hotfix_GetBuffInfoHidingPosDummy;
        private static DelegateBridge __Hotfix_SetBuffInfoItemToUnused;
        private static DelegateBridge __Hotfix_GetValidBuffNoticeDummyIndex;
        private static DelegateBridge __Hotfix_get_BuffNoticeDummyList;

        [MethodImpl(0x8000)]
        public void AddBuffUIItemFromBuffInfo(SolarSystemBuffNoticeItemUIController.BuffTypeUIInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateNewBuffNoticeItem()
        {
        }

        [MethodImpl(0x8000)]
        private Transform GetBuffInfoHidingPosDummy(ShipFightBufType bufType, bool isEnhance)
        {
        }

        [MethodImpl(0x8000)]
        private SolarSystemBuffNoticeItemUIController GetFreeBuffNoticeItem()
        {
        }

        [MethodImpl(0x8000)]
        private int GetValidBuffNoticeDummyIndex()
        {
        }

        [MethodImpl(0x8000)]
        private void SetBuffInfoItemToUnused(SolarSystemBuffNoticeItemUIController uiCtrl, bool isSucceed)
        {
        }

        [MethodImpl(0x8000)]
        private void SetBuffNoticeTimeout(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private Transform[] BuffNoticeDummyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetBuffNoticeTimeout>c__AnonStorey0
        {
            internal SolarSystemBuffNoticeItemUIController buffInfoUIItem;
            internal SolarSystemBuffNoticeContainerUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

