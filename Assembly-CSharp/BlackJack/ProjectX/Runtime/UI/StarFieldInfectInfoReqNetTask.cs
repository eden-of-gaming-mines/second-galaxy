﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarFieldInfectInfoReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_starFieldId;
        private readonly uint m_dataVersion;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnStarFieldInfectInfoAck;

        [MethodImpl(0x8000)]
        public StarFieldInfectInfoReqNetTask(int starFieldId, uint dataVersion, bool needBlockGlobalUI = true)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarFieldInfectInfoAck(StarFieldInfectInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

