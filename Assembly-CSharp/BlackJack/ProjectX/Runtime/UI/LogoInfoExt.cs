﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class LogoInfoExt
    {
        private static DelegateBridge __Hotfix_GetBgId_2;
        private static DelegateBridge __Hotfix_GetPaintingId_2;
        private static DelegateBridge __Hotfix_GetBgColor_2;
        private static DelegateBridge __Hotfix_GetPaintingColor_2;
        private static DelegateBridge __Hotfix_GetBgId_0;
        private static DelegateBridge __Hotfix_GetPaintingId_0;
        private static DelegateBridge __Hotfix_GetBgColor_0;
        private static DelegateBridge __Hotfix_GetPaintingColor_0;
        private static DelegateBridge __Hotfix_GetBgId_1;
        private static DelegateBridge __Hotfix_GetPaintingId_1;
        private static DelegateBridge __Hotfix_GetBgColor_1;
        private static DelegateBridge __Hotfix_GetPaintingColor_1;

        [MethodImpl(0x8000)]
        public static int GetBgColor(this AllianceLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetBgColor(this GuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetBgColor(this ProGuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetBgId(this AllianceLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetBgId(this GuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetBgId(this ProGuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingColor(this AllianceLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingColor(this GuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingColor(this ProGuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingId(this AllianceLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingId(this GuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPaintingId(this ProGuildLogoInfo logoInfo)
        {
        }
    }
}

