﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    [Serializable]
    public class EmojiParseDesc : MonoBehaviour
    {
        [Header("表情的大小(正方形，x,y大小一致):")]
        public Vector2 m_emojiSize;
        [Header("Emoji所占位置大小(和表情自身大小无关):")]
        public int m_emSpace;
        [Header("表情坐标在X轴的偏移:")]
        public float m_offsetX;
        [Header("表情坐标在Y轴的偏移:")]
        public float m_offsetY;
    }
}

