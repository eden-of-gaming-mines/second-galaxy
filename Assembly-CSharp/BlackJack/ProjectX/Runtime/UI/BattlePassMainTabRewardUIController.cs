﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BattlePassMainTabRewardUIController : UIControllerBase
    {
        private int m_tempRemainTrumpCount;
        private int m_tempShortCutCount;
        private const int CONST_PREVIEW_ITEM_BASE_LEVEL = 10;
        public int m_SelectItemIndex;
        public int m_SelecteItemChildIndex;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, bool, Action<bool, bool>> OnEventSendGetBattlePassLevReq;
        private ConfigDataBattlePassLevelInfo[] m_PassLevelInfoList;
        private Dictionary<string, UnityEngine.Object> m_resData;
        protected const string TabRewardItemName = "BattlePassRewardItemUIPrefab";
        protected const string CommonItemName = "CommonItemUIPrefab";
        private GameObject m_TabRewardItemTemplateGo;
        private CommonItemIconUIController m_RightCommonItemIconCtrl;
        public CommonItemIconUIController m_ShipCommonItemCtrlA;
        public CommonItemIconUIController m_ShipCommonItemCtrlB;
        private BattlePassRewardItemUIController m_FullLevelReardItemUICtrl;
        private const int POOL_SIZE = 9;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateRoot;
        [AutoBind("./MedalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LevelTitleUIState;
        [AutoBind("./MedalGroup/MedalNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurLevelText;
        [AutoBind("./MedalGroup/EXText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurExpText;
        [AutoBind("./MedalGroup/BarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ExpBarImage;
        [AutoBind("./MedalGroup/ResidueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RemainDays;
        [AutoBind("./MedalGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./MedalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MedalGroupUIState;
        [AutoBind("./MedalGroup/NameTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NameTextGroupUIState;
        [AutoBind("./ContentGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect LoopScrollView;
        [AutoBind("./ContentGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SelfEasyObjectPool;
        [AutoBind("./ContentGroup/TrumpImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TrumpUIState;
        [AutoBind("./UpgradeAndShortctGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeAndShortctUIState;
        [AutoBind("./UpgradeAndShortctGroup/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./UpgradeAndShortctGroup/UpgradeButton/UpgradeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeText;
        [AutoBind("./UpgradeAndShortctGroup/ShortcutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShortcutButton;
        [AutoBind("./UpgradeAndShortctGroup/ShortcutButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShortcutButtonUIState;
        [AutoBind("./AwardGroup/PublicityGroup/DescribeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DescribeButton;
        [AutoBind("./AwardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AwardGroupUIState;
        [AutoBind("./AwardGroup/PublicityGroup/AwardText/ItemDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy01;
        [AutoBind("./AwardGroup/PublicityGroup/AwardText/ItemDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy02;
        [AutoBind("./AwardGroup/PublicityGroup/DescribeButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PublicityDescribeButton;
        [AutoBind("./AwardGroup/DescribeGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy;
        [AutoBind("./AwardGroup/DescribeGroup/DescribeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemDescribeText;
        [AutoBind("./AwardGroup/DescribeGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./AwardGroup/DescribeGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./AwardGroup/DescribeGroup/DescribeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemDescribeButton;
        [AutoBind("./ContentGroup/FullLevelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform FullLevelDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetLevelUIStateByGrade;
        private static DelegateBridge __Hotfix_UpdateTitle;
        private static DelegateBridge __Hotfix_UpdateShortCurtCount;
        private static DelegateBridge __Hotfix_UpdateRewardItemList;
        private static DelegateBridge __Hotfix_GetFakeLBStoreItem;
        private static DelegateBridge __Hotfix_GetFullLevlIndex;
        private static DelegateBridge __Hotfix_UpdateShip;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_CreateTemplateItemEx;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_ClearChooseFlag;
        private static DelegateBridge __Hotfix_OnTablRewardItemClick;
        private static DelegateBridge __Hotfix_InitShowRightItemDetailByUpgrade;
        private static DelegateBridge __Hotfix_ShowRightItemDetail;
        private static DelegateBridge __Hotfix_OnTablRewardItemFill;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_OnEventSendGetBattlePassLevReq;
        private static DelegateBridge __Hotfix_remove_OnEventSendGetBattlePassLevReq;

        public event Action<int, bool, Action<bool, bool>> OnEventSendGetBattlePassLevReq
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearChooseFlag()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject CreateTemplateItemEx(string name, Transform parent, bool active)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem GetFakeLBStoreItem(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private int GetFullLevlIndex(int maxLv)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        public void InitShowRightItemDetailByUpgrade(FakeLBStoreItem item, int level, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTablRewardItemClick(BattlePassRewardItemUIController tabItemCtrl, CommonItemIconUIController iconCtrl, int itemChildIndex, bool isTrump, QuestRewardInfo reward)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTablRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLevelUIStateByGrade(bool isUpgrade, bool isFull)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowRightItemDetail(BattlePassRewardItemUIController tabItemCtrl, CommonItemIconUIController iconCtrl, int itemChildIndex, bool isTrump)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardItemList(ConfigDataBattlePassLevelInfo[] levelInfoList, int maxLv, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShip(CommonItemIconUIController shipCtrl, int shipConfId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShortCurtCount(int remainTrumpCount, int shortCutCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTitle(int curExp, int constExp, int maxLv)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnTablRewardItemClick>c__AnonStorey0
        {
            internal QuestRewardInfo reward;
            internal BattlePassRewardItemUIController tabItemCtrl;
            internal bool isTrump;
            internal BattlePassMainTabRewardUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool end, bool upgrade)
            {
            }
        }
    }
}

