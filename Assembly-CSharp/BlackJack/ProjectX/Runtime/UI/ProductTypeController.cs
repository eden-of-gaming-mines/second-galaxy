﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ProductTypeController : UIControllerBase
    {
        public int m_typeId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> OnValueChanged;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_toggle;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_name;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_SetListener;
        private static DelegateBridge __Hotfix_add_OnValueChanged;
        private static DelegateBridge __Hotfix_remove_OnValueChanged;

        private event Action<int> OnValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public void SetListener(Action<int> listener)
        {
        }
    }
}

