﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class FactionCreditNpcInfoMenuItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./FlagBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FlagBgImage;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItem3DtouchEvent;
        private static DelegateBridge __Hotfix_UnregisterItem3DTouchEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareItemClick)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItem3DtouchEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItem3DTouchEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ConfigDataFactionCreditNpcInfo npcInfo, Dictionary<string, Object> assetDic)
        {
        }
    }
}

