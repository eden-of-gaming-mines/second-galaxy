﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AuctionItemOnShelveReqNetworkTask : NetWorkTransactionTask
    {
        private int m_result;
        private ulong AuctionItemInsId;
        private int AuctionItemId;
        private long ItemCount;
        private long Price;
        private int Period;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAuctionItemOnShelveAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public AuctionItemOnShelveReqNetworkTask(ulong AuctionItemInsId, int AuctionItemId, long ItemCount, long Price, int Period)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAuctionItemOnShelveAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

