﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CrackQueueRemoveBoxReqNetTask : NetWorkTransactionTask
    {
        private int m_boxItemIdx;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CrackQueueRemoveBoxResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCrackQueueRemoveBoxAck;
        private static DelegateBridge __Hotfix_set_CrackQueueRemoveBoxResult;
        private static DelegateBridge __Hotfix_get_CrackQueueRemoveBoxResult;

        [MethodImpl(0x8000)]
        public CrackQueueRemoveBoxReqNetTask(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCrackQueueRemoveBoxAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CrackQueueRemoveBoxResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

