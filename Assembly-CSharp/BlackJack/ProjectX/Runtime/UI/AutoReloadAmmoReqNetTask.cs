﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AutoReloadAmmoReqNetTask : NetWorkTransactionTask
    {
        private int m_hangarShipIndex;
        private bool m_autoChangeAmmo;
        public List<int> m_ammoAddList;
        private int m_ackResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAutoReloadAmmoAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;

        [MethodImpl(0x8000)]
        public AutoReloadAmmoReqNetTask(int hangarShipIndex, bool autoChangeAmmo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAutoReloadAmmoAck(int result, List<int> ammoAddList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

