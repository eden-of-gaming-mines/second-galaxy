﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipHangarAssignUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGoShopButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGoTradingPlaceButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnTypeDropDownValueChanged;
        public ItemSimpleInfoUIController m_itemSimpleInfoCtrl;
        public ItemStoreListUIController m_itemStoreListUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AuctionBuyItemFilterType> EventOnFilterChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnFilterPanelBackGroundButtonClick;
        public ItemFilterUIController m_itemFilterUICtrl;
        public GuildFlagShipUnpackConfirmUIController m_GuildUnpackComfirmUICtrl;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_stateCtrl;
        [AutoBind("./Margin/NoItemPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NoItemRoot;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./Margin/FilterMask", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FilterMaskImage;
        [AutoBind("./Margin/ItemStorePanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FilterButton;
        [AutoBind("./Margin/ItemStorePanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FilterButtonState;
        [AutoBind("./Margin/ItemStorePanel/FilterButton/TextShowAll", AutoBindAttribute.InitState.NotInit, false)]
        public Text FilterButtonText;
        [AutoBind("./Margin/FilterPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_filterPanelDummy;
        [AutoBind("./Margin/GuildUnpackComfirmDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_GuildUnpackComfirmDumy;
        [AutoBind("./Margin/ItemStorePanel/TypeFilterDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_typeFilterDropDown;
        [AutoBind("./Margin/ItemStorePanel/ItemStoreListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreListDummy;
        [AutoBind("./Margin/ItemSimpleInfoUIPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoUIPanelDummy;
        [AutoBind("./Margin/ItemSimpleInfoUIPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemSimpleInfoBGStateCtrl;
        [AutoBind("./Margin/Buttons/ChooseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChooseButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_InitTypeFilterDropDownList;
        private static DelegateBridge __Hotfix_ResetSetTypeFilterDropDownValue;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_SetItemStoreFilterName;
        private static DelegateBridge __Hotfix_GetFilterPanelUIProcess;
        private static DelegateBridge __Hotfix_SetSortButtonState;
        private static DelegateBridge __Hotfix_SetAllFilterButtonState;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_OnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_SetNoItemPanelState;
        private static DelegateBridge __Hotfix_GetFirstItemRect;
        private static DelegateBridge __Hotfix_GetItemListPos;
        private static DelegateBridge __Hotfix_OnGoShopButtonClick;
        private static DelegateBridge __Hotfix_OnGoTradingPlaceButtonClick;
        private static DelegateBridge __Hotfix_OnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnGoShopButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoShopButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGoTradingPlaceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoTradingPlaceButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnFilterChange;
        private static DelegateBridge __Hotfix_remove_EventOnFilterChange;
        private static DelegateBridge __Hotfix_add_EventOnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFilterPanelBackGroundButtonClick;

        public event Action<AuctionBuyItemFilterType> EventOnFilterChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFilterPanelBackGroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGoShopButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGoTradingPlaceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnTypeDropDownValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetFilterPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemListPos()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos()
        {
        }

        [MethodImpl(0x8000)]
        public void InitTypeFilterDropDownList(List<string> menuList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterPanelBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoShopButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoTradingPlaceButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTypeDropDownValueChanged(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetSetTypeFilterDropDownValue()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllFilterButtonState(AuctionBuyItemFilterType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreFilterName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNoItemPanelState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortButtonState(bool isOpen, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIMode(bool isGuildHangar)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowItemSimpleInfoPanel(bool isshow)
        {
        }
    }
}

