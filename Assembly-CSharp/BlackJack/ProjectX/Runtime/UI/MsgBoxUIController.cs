﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class MsgBoxUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./TextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_textGroupCtrl;
        [AutoBind("./TextGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_text;
        [AutoBind("./TextGroup/Text1", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_firstText;
        [AutoBind("./TextGroup/Text2", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_secondText;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        [AutoBind("./Buttons/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_confirmButtonText;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        [AutoBind("./Buttons/CancelButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_cancelButtonText;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsAutoInitLocalized;
        private static DelegateBridge __Hotfix_OnShowMsgBoxStart;
        private static DelegateBridge __Hotfix_CreateShowBothOrConfirmProcess;
        private static DelegateBridge __Hotfix_CreatCloseBothOrConfirmProcess;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreatCloseBothOrConfirmProcess(bool isCloseBoth)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateShowBothOrConfirmProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsAutoInitLocalized()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnBindFiledsCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.AutoInitLocalizedString = this.IsAutoInitLocalized();
                base.OnBindFiledsCompleted();
            }
        }

        [MethodImpl(0x8000)]
        public void OnShowMsgBoxStart(string normalInfo, string extraInfo, string titleInfo)
        {
        }
    }
}

