﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class StarMapForGalaxyUITask : UITaskBase
    {
        public static string StarMapForGalaxyMode_NormalMode = "StarMapForGalaxyMode_NormalMode";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackToMainUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBStarfieldsInfo, int> EventOnBacktoStarfieldUITask;
        private StarMapForGalaxyUIController m_mainCtrl;
        private StarMapOrientedTargetSelectWndUIController m_orientedTargetSelectWndUICtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "StarMapForGalaxyUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartStarMapForGalaxyUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateAllStarfieldNodeOnStarMap;
        private static DelegateBridge __Hotfix_OnOrientationButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnSignalSearchButtonClick;
        private static DelegateBridge __Hotfix_OnStarfieldNodeIconClick;
        private static DelegateBridge __Hotfix_OnWndBackgroupClick;
        private static DelegateBridge __Hotfix_OnMainButtonClick;
        private static DelegateBridge __Hotfix_DoOrientationBySolarSystemId;
        private static DelegateBridge __Hotfix_DoEnterStarfieldStarMap;
        private static DelegateBridge __Hotfix_add_EventOnBackToMainUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToMainUITask;
        private static DelegateBridge __Hotfix_add_EventOnBacktoStarfieldUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBacktoStarfieldUITask;
        private static DelegateBridge __Hotfix_get_m_playerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnBackToMainUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBStarfieldsInfo, int> EventOnBacktoStarfieldUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public StarMapForGalaxyUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void DoEnterStarfieldStarMap(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        private void DoOrientationBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMainButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CommanderButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_MotherShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSignalSearchButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarfieldNodeIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWndBackgroupClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static StarMapForGalaxyUITask StartStarMapForGalaxyUITask(string mode)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateAllStarfieldNodeOnStarMap()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext m_playerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnStarfieldNodeIconClick>c__AnonStorey0
        {
            internal StarMapStarfieldNodeUIController starfieldNodeCtrl;
            internal StarMapForGalaxyUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess p, bool r)
            {
            }
        }
    }
}

