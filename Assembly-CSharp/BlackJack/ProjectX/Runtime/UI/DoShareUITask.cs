﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime.SDK;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class DoShareUITask : UITaskBase
    {
        private DoShareUIController m_doShareUIController;
        private Action m_onEndAction;
        private Texture2D m_screenTexture;
        public static string ParamKey_OnEndAciton = "OnEndAciton";
        public const string ModeScreenShoot = "ScreenShoot";
        public const string ModePreview = "PreView";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartDoShareUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView4ModePreview;
        private static DelegateBridge __Hotfix_UpdateView4ModeScreenShoot;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnDoShare4WeiBoButtonClick;
        private static DelegateBridge __Hotfix_OnDoShare4QQButtonClick;
        private static DelegateBridge __Hotfix_OnDoShare4WeChatFriendButtonClick;
        private static DelegateBridge __Hotfix_OnDoShare4FaceBookButtonClick;
        private static DelegateBridge __Hotfix_OnDoShare4TwitterButtonClick;
        private static DelegateBridge __Hotfix_OnDoShare4InsButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_RegisterSdkEvent;
        private static DelegateBridge __Hotfix_UnRegisterSdkEvent;
        private static DelegateBridge __Hotfix_ClosePanel;
        private static DelegateBridge __Hotfix_DoScreenShoot;
        private static DelegateBridge __Hotfix_DoShare;
        private static DelegateBridge __Hotfix_OnDoShareCallBack;
        private static DelegateBridge __Hotfix_OnDoShareCancel;
        private static DelegateBridge __Hotfix_get_ShareImagePath;
        private static DelegateBridge __Hotfix_get_ShareThumbImagePath;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public DoShareUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClosePanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator DoScreenShoot(Action onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator DoShare(SDKHelper.SharePlatform platform)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4FaceBookButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4InsButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4QQButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4TwitterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4WeChatFriendButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShare4WeiBoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShareCallBack(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDoShareCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterSdkEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartDoShareUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterSdkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView4ModePreview()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView4ModeScreenShoot()
        {
        }

        public static string ShareImagePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string ShareThumbImagePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DoScreenShoot>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action onEnd;
            internal DoShareUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DoShare>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal SDKHelper.SharePlatform platform;
            internal DoShareUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            internal void <>m__0(bool ret)
            {
                Debug.Log("DoShareEnd ret:" + ret);
                if (ret)
                {
                    this.$this.PostDelayTimeExecuteAction(() => this.$this.EnableUIInput(true, true), 5f);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_DoShareFailed, false, new object[0]);
                    this.$this.EnableUIInput(true, true);
                }
            }

            internal void <>m__1()
            {
                this.$this.EnableUIInput(true, true);
            }

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Debug.Log("DoShare to platform:" + this.platform);
                        if (SDKHelper.IsUseSdk())
                        {
                            this.$this.EnableUIInput(false, false);
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                        }
                        else
                        {
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        this.$current = SDKHelper.DoShare(this.platform, this.$this.m_screenTexture, new Action<bool>(this.<>m__0));
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        break;

                    case 2:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 3;
                        }
                        break;

                    case 3:
                        this.$PC = -1;
                        goto TR_0000;

                    default:
                        goto TR_0000;
                }
                return true;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

