﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DevelopmentItemDisplayLineUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnItemClick;
        private DevelopmentSingleItemUIController[] m_itemCtrls;
        [AutoBind("./IconTitle/TitleImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemGroupDescText;
        [AutoBind("./IconTitle/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankBgStateCtrl;
        [AutoBind("./IconTitle/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankImage;
        [AutoBind("./IconTitle/TIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_itemTechImage;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateViewOnItemLine;
        private static DelegateBridge __Hotfix_UpdateItemLineChooseState;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(GameObject itemTemplateGo, int lineIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemLineChooseState(DevelopmentTransformationUITask.ItemDisplayInfo[] itemInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnItemLine(string descStr, DevelopmentCostItemList lineInfo, DevelopmentTransformationUITask.ItemDisplayInfo[] itemInfos, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

