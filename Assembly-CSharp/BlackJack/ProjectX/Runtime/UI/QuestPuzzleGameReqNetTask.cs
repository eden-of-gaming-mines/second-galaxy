﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class QuestPuzzleGameReqNetTask : NetWorkTransactionTask
    {
        protected int m_questInstanceId;
        protected List<int> m_currencyList;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnQuestPuzzleGameComplete;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public QuestPuzzleGameReqNetTask(int questInstanceId, List<int> currencyList)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestPuzzleGameComplete(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

