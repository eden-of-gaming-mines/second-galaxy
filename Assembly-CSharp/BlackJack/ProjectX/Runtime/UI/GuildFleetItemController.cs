﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildFleetItemController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_button;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamNameStateCtrl;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_teamNameText;
        [AutoBind("./TextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_nameStateCtrl;
        [AutoBind("./TextGroup/TeamText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./PeopleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_peopleCountText;
        [AutoBind("./NumberBgText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nullStateNumberText;
        [AutoBind("./CaptainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_captainPhotoStateCtrl;
        [AutoBind("./CaptainGroup/CommonCaptainImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_captainPhoto;
        private static DelegateBridge __Hotfix_SetItemInfo;
        private static DelegateBridge __Hotfix_SetItemNullState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_get_lbGuild;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemInfo(GuildFleetSimpleInfo data, Dictionary<string, Object> m_dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemNullState(int index)
        {
        }

        private LogicBlockGuildClient lbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

