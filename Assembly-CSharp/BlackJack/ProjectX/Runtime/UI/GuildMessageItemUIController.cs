﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMessageItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnMessageIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnPlayerNameClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnDeleteButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, string> EventOnDeleteTextButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLoadMoreButtonClick;
        private const string StateLoadMore = "ToLoad";
        private const string StateAllMessage = "Empty";
        private const string StateNormal = "Normal";
        private const string StateDelete = "Deleted";
        private const string StateLast = "Last";
        private const string StateGuild = "Guild";
        private const string StatePlayer = "Player";
        private const string AssetPathCaptainIcon = "CaptainIconUIPrefab";
        private const string AssetPathGuildIcon = "GuildIconUIPrefab";
        private DateTime m_nextTimeRefreshTime;
        private DateTime m_messageSendTime;
        public ScrollItemBaseUIController m_scrollCtrl;
        public CommonCaptainIconUIController m_captainIconUICtrl;
        public GuildLogoController m_guildIconUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./MessageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_messageStateCtrl;
        [AutoBind("./MessageGroup/MessageImage/DummyGroup/GuildDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_guildIconDummy;
        [AutoBind("./MessageGroup/MessageImage/DummyGroup/GuildDummy", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildIconButton;
        [AutoBind("./MessageGroup/MessageImage/DummyGroup/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_playerIconDummy;
        [AutoBind("./MessageGroup/MessageImage/FixedMessageImage/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerNameText;
        [AutoBind("./MessageGroup/MessageImage/FixedMessageImage/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_playerNameButton;
        [AutoBind("./MessageGroup/MessageImage/FixedMessageImage/TimeImage/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeText;
        [AutoBind("./MessageGroup/MessageImage/LeaveWordText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_messageText;
        [AutoBind("./MessageGroup/MessageImage/Button/TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_translateButton;
        [AutoBind("./MessageGroup/MessageImage/Button/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_deleteButton;
        [AutoBind("./DeletedInfromationGroup/DeletedInfromationImage/DeletedInfromationText", AutoBindAttribute.InitState.NotInit, false)]
        public TextExpand m_deletedMessageText;
        [AutoBind("./ToLoadGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lastItemStateCtrl;
        [AutoBind("./ToLoadGroup/ToLoadImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_loadMoreMessageButton;
        [AutoBind("./PlayerSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_playerSimpleInfoPanelDummy;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateMessageInfo;
        private static DelegateBridge __Hotfix_GetSimplePlayerPos;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnGuildIconClick;
        private static DelegateBridge __Hotfix_OnMessageIconClick;
        private static DelegateBridge __Hotfix_OnPlayerNameClick;
        private static DelegateBridge __Hotfix_OnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_SetToLastMode;
        private static DelegateBridge __Hotfix_SetToNormalMode;
        private static DelegateBridge __Hotfix_UpdateMessageTime;
        private static DelegateBridge __Hotfix_SetToDeleteMode;
        private static DelegateBridge __Hotfix_UpdateNextTimeRefreshTime;
        private static DelegateBridge __Hotfix_add_EventOnMessageIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnMessageIconClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerNameClick;
        private static DelegateBridge __Hotfix_add_EventOnDeleteButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDeleteButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLoadMoreButtonClick;

        public event Action<UIControllerBase> EventOnDeleteButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, string> EventOnDeleteTextButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLoadMoreButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnMessageIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnPlayerNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimplePlayerPos()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteTextButtonClick(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildIconClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadMoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMessageIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetToDeleteMode(GuildMessageInfo messageInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetToLastMode(bool showAll)
        {
        }

        [MethodImpl(0x8000)]
        private void SetToNormalMode(GuildMessageInfo messageInfo, bool showDeleteButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMessageInfo(GuildMessageInfo messageInfo, bool showAll, bool hasDeletePermission, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMessageTime()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateNextTimeRefreshTime(TimeSpan time)
        {
        }
    }
}

