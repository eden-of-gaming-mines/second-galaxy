﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class MonthCardSubscriptionTask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBRechargeMonthlyCard> EventOnBuyBtnClick;
        private MonthCardSubscriptionUIController m_monthCardSubscriptionUICtrl;
        private LBRechargeMonthlyCard m_monthCardItem;
        private const string ParamKeyMonthCardItem = "MonthCardItem";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartMonthCardSubscriptionTask;
        private static DelegateBridge __Hotfix_UnRegistAllEvent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBuyBtnClick;
        private static DelegateBridge __Hotfix_OnAgreementUrlBtnClick;
        private static DelegateBridge __Hotfix_OnPrivicyUrlBtnClick;
        private static DelegateBridge __Hotfix_OnCloseBtnClick;
        private static DelegateBridge __Hotfix_OpenUrl;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_GetSubscriptionPanelUIProcess;
        private static DelegateBridge __Hotfix_add_EventOnBuyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuyBtnClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<LBRechargeMonthlyCard> EventOnBuyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public MonthCardSubscriptionTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetSubscriptionPanelUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAgreementUrlBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBuyBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPrivicyUrlBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenUrl(string url)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartMonthCardSubscriptionTask(UIIntent preIntent, LBRechargeMonthlyCard monthCard)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

