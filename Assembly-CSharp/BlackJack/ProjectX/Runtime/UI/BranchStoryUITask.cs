﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BranchStoryUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStorySubQuestInfo> EventOnGoImmediateButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryItemInfo> EventOnOpenButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action, bool, bool> EventOnRequestBranchStoryUIClose;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        private IUIBackgroundManager m_backgroundManager;
        private bool m_isRepeatableUserGuide;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private bool m_isPlayAnim;
        private bool m_isTaskInit;
        private BranchStoryItemInfo m_currSelectBranchStory;
        private Dictionary<int, BranchStorySubQuestInfo> m_subQuestDict;
        private Dictionary<int, BranchStoryItemInfo> m_branchId2BranchQuestDict;
        private Dictionary<int, BranchStoryItemInfo> m_subQuestId2BranchQuestDict;
        private Dictionary<int, List<BranchStoryItemInfo>> m_branchStoryDict;
        private BranchStoryUIController m_mainCtrl;
        private BranchStoryListUIController m_branchStoryListCtrl;
        private BranchStoryUnOpenDetailUIController m_unOpenDetailCtrl;
        private BranchStoryPuzzleDetailUIController m_puzzleBranchStoryCtrl;
        private BranchStoryRealDetailUIController m_realBranchStoryCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKey_BranchStoryConfId = "SelectBranchStoryConfId";
        public const string ParamKey_PlayAnim = "PlayerPanelAnimation";
        public const string TaskName = "BranchStoryUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartBranchQuestUITask;
        private static DelegateBridge __Hotfix_ShowBranchStoryPanel;
        private static DelegateBridge __Hotfix_HideBranchStoryPanel;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_GetNeedTipBranchStoryCount;
        private static DelegateBridge __Hotfix_SetBackGroundManager;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_InitBranchStoryDict;
        private static DelegateBridge __Hotfix_UpdateDataCache_InitParam;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView4DetailPanel;
        private static DelegateBridge __Hotfix_GetDetailPanelOpenProcess;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnQuestComplete;
        private static DelegateBridge __Hotfix_OnQuestAccept;
        private static DelegateBridge __Hotfix_OnBranchStoryItemClick;
        private static DelegateBridge __Hotfix_OnOpenButtonClick;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_OnPuzzleQuestItemShowTipButtonClick;
        private static DelegateBridge __Hotfix_OnPuzzleQuestTipBgButtonClick;
        private static DelegateBridge __Hotfix_SaveClienData;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_AddUpdateMask;
        private static DelegateBridge __Hotfix_CheckUpdateMask;
        private static DelegateBridge __Hotfix_GetBranchStoryUIInfoByConfId;
        private static DelegateBridge __Hotfix_UpdateBranchStoryUIInfo;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_GetCategoryName;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_CloseActionPlanUIPanel;
        private static DelegateBridge __Hotfix_GetCustomDataKey;
        private static DelegateBridge __Hotfix_LeaveCurrBackground;
        private static DelegateBridge __Hotfix_GetBranchOpenAnimShowStateKey;
        private static DelegateBridge __Hotfix_GetRealQuestOpenAnimShowStateKey;
        private static DelegateBridge __Hotfix_GetSubQuestOpenAnimShowStateKey;
        private static DelegateBridge __Hotfix_add_EventOnGoImmediateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoImmediateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOpenButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOpenButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRequestBranchStoryUIClose;
        private static DelegateBridge __Hotfix_remove_EventOnRequestBranchStoryUIClose;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_get_LBBranchStory;
        private static DelegateBridge __Hotfix_get_LBQuest;
        private static DelegateBridge __Hotfix_get_LBRedPoint;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnBranchStoryFirstCategoryClick_UserGuide;
        private static DelegateBridge __Hotfix_OnBranchStoryFirstCategoryFirstItemClick_UserGuide;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;

        public event Action<BranchStorySubQuestInfo> EventOnGoImmediateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<BranchStoryItemInfo> EventOnOpenButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool, bool> EventOnRequestBranchStoryUIClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public BranchStoryUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddUpdateMask(PipeLineMaskType type)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckUpdateMask(PipeLineMaskType type)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseActionPlanUIPanel(Action eventAfterPause = null, bool isIgnoreAnim = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private string GetBranchOpenAnimShowStateKey(int branchStoryId)
        {
        }

        [MethodImpl(0x8000)]
        private BranchStoryItemInfo GetBranchStoryUIInfoByConfId(ConfigDataBranchStoryQuestListInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetCategoryName(int category)
        {
        }

        [MethodImpl(0x8000)]
        private string GetCustomDataKey(int branchStoryId)
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetDetailPanelOpenProcess()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNeedTipBranchStoryCount()
        {
        }

        [MethodImpl(0x8000)]
        private string GetRealQuestOpenAnimShowStateKey(int branchStoryId)
        {
        }

        [MethodImpl(0x8000)]
        private string GetSubQuestOpenAnimShowStateKey(int branchStoryId, int subQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void HideBranchStoryPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackground()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBranchStoryFirstCategoryClick_UserGuide(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBranchStoryFirstCategoryFirstItemClick_UserGuide(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBranchStoryItemClick(BranchStoryItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenButtonClick(BranchStoryItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPuzzleQuestItemShowTipButtonClick(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPuzzleQuestTipBgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestAccept(int result, int instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestComplete(int result, int questConfigId, List<QuestRewardInfo> rewardList, bool disableReturnNotice)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestGalaxyButtonClick(BranchStorySubQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(int index, QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButtonClick(BranchStorySubQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void SaveClienData()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundManager(IUIBackgroundManager backgroundManager)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPostion(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBranchStoryPanel(bool isImmediate = false, int branchStoryConfId = 0, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        public static BranchStoryUITask StartBranchQuestUITask(bool isPlayAnim = true, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBranchStoryUIInfo(BranchStoryItemInfo uiInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_InitBranchStoryDict()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_InitParam()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView4DetailPanel()
        {
        }

        private LogicBlockBranchStoryClient LBBranchStory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockQuestClient LBQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockRedPointClient LBRedPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideBranchStoryPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal BranchStoryUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestGalaxyButtonClick>c__AnonStorey1
        {
            internal int solarSystemId;
            internal BranchStoryUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                if (this.$this.PlayerCtx.CurrSolarSystemId == this.solarSystemId)
                {
                    StarMapManagerUITask.StartStarMapForSolarSystemUITask("StarMapForSolarSystemMode_NormalMode", this.solarSystemId, returnIntent, null, null, null, null);
                }
                else
                {
                    StarMapManagerUITask.StartStarMapForStarfieldUITask(returnIntent, this.solarSystemId, this.$this.PlayerCtx.CurrSolarSystemId, this.solarSystemId, GEBrushType.GEBrushType_SecurityLevel, null, 0, false, null, false, 0, 0UL, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey2
        {
            internal FakeLBStoreItem item;
            internal Vector3 pos;
            internal string mode;
            internal ItemSimpleInfoUITask.PositionType type;
            internal BranchStoryUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, returnIntent, true, this.pos, this.mode, this.type, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        public class BranchStoryItemInfo
        {
            public BranchStoryUITask.BranchStoryState m_state;
            public string m_name;
            public int m_category;
            public int m_currProcessQuestId;
            public int m_allRealQuestCount;
            public int m_completedRealQuestCount;
            public string m_bigBgPath;
            public List<BranchStoryUITask.BranchStorySubQuestInfo> m_subQuestList;
            public string m_desc;
            public int m_branchStoryConfId;
            public bool m_redPointTip;
            private bool? m_havePuzzleSubQuest;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_HavePuzzleSubQuest;

            public BranchStoryItemInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public bool HavePuzzleSubQuest
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_HavePuzzleSubQuest;
                    if (bridge != null)
                    {
                        return bridge.__Gen_Delegate_Imp9(this);
                    }
                    if (this.m_havePuzzleSubQuest == null)
                    {
                        if (this.m_subQuestList == null)
                        {
                            this.m_havePuzzleSubQuest = false;
                        }
                        else
                        {
                            foreach (BranchStoryUITask.BranchStorySubQuestInfo info in this.m_subQuestList)
                            {
                                if (info.m_isPuzzle)
                                {
                                    this.m_havePuzzleSubQuest = true;
                                    break;
                                }
                            }
                            if (this.m_havePuzzleSubQuest == null)
                            {
                                this.m_havePuzzleSubQuest = false;
                            }
                        }
                    }
                    return this.m_havePuzzleSubQuest.Value;
                }
            }
        }

        public enum BranchStoryState
        {
            UnOpen,
            CanOpen,
            PuzzleQuestProcessing,
            RealQuestProcessing,
            Complete
        }

        public class BranchStorySubQuestInfo
        {
            public int m_index;
            public int m_confId;
            public bool m_isPuzzle;
            public string m_name;
            public string m_questDesc;
            public bool m_isComplete;
            public string m_completeDesc;
            public string m_bgResPath;
            public bool m_isAnimPlayed;
            private static DelegateBridge _c__Hotfix_ctor;

            public BranchStorySubQuestInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum PipeLineMaskType
        {
            QuestComplete,
            QuestAccept,
            ToggleChange,
            OnlyUpdateList
        }
    }
}

