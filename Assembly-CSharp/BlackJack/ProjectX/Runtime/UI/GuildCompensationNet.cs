﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildCompensationNet : NetWorkTransactionTask
    {
        private readonly ulong m_instanceId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildCompensationAck <Ack>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnAck;
        private static DelegateBridge __Hotfix_set_Ack;
        private static DelegateBridge __Hotfix_get_Ack;

        [MethodImpl(0x8000)]
        public GuildCompensationNet(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAck(GuildCompensationAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildCompensationAck Ack
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

