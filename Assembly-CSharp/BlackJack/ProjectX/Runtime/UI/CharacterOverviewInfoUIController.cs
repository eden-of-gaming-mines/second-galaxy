﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterOverviewInfoUIController : UIControllerBase
    {
        private CommonUIStateController m_weaponPropertyEffectCtrl;
        private CommonUIStateController m_defensePropertyEffectCtrl;
        private CommonUIStateController m_drivePropertyEffectCtrl;
        private CommonUIStateController m_electronicPropertyEffectCtrl;
        private GuildLogoController m_guildLogoUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAddPropertyPointButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAchievementButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnSkillButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnDrivingLicenseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnChatButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDiplomacyButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTeamButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnMailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWeaponContrlButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDefenseOperateButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnShipDriveButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnElectronicWarButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnKillRecordButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWantedInfoClick;
        private const float m_normalBoundNum = 0f;
        private const float m_suspectBoundNum = 5f;
        private const float m_criminalBoundNum = 10f;
        private const string m_normalMode = "Nomral";
        private const string m_suspectMode = "Suspect";
        private const string m_criminalMode = "Criminal";
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty", AutoBindAttribute.InitState.NotInit, false)]
        public Button WeaponContrlButton;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty", AutoBindAttribute.InitState.NotInit, false)]
        public Button DefenseContrlButton;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty", AutoBindAttribute.InitState.NotInit, false)]
        public Button DriveContrlButton;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty", AutoBindAttribute.InitState.NotInit, false)]
        public Button ElectronicContrlButton;
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty/PropertyInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform WeaponContrlTipDummy;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty/PropertyInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DefenseContrlTipDummy;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty/PropertyInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DriveContrlTipDummy;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty/PropertyInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ElectronicContrlTipDummy;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_displayModeCtrl;
        [AutoBind("./WantedLevelInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_wantedStateCtrl;
        [AutoBind("./WantedLevelInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_wantedButton;
        [AutoBind("./WantedLevelInfoRoot/KillReportButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_killRecordButton;
        [AutoBind("./WantedLevelInfoRoot/KillReportButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_killRecordRedPoint;
        [AutoBind("./GuildInfoRoot/GuildLogoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_guildLogoDummy;
        [AutoBind("./GuildInfoRoot/GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildNameText;
        [AutoBind("./GuildInfoRoot/GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildNameBtn;
        [AutoBind("./GuildInfoRoot/GuildDutyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildDutyNameText;
        [AutoBind("./WantedLevelInfoRoot/WantedLevelNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_wantedLevelText;
        [AutoBind("./AddPropertyPointRoot/PropertyPointCountInfoRoot/PropertyPointCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyPointCountText;
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty/PropertyPointInfo/BasicPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_basicPointTextForWeaponProperty;
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty/PropertyPointInfo/ExtraPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_extraPointTextForWeaponProperty;
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty/EffectDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_weaponPropertyEffectTrans;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty/PropertyPointInfo/BasicPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_basicPointTextForDefenseProperty;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty/PropertyPointInfo/ExtraPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_extraPointTextForDefenseProperty;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty/EffectDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defensePropertyEffectTrans;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty/PropertyPointInfo/BasicPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_basicPointTextForDriveProperty;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty/PropertyPointInfo/ExtraPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_extraPointTextForDriveProperty;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty/EffectDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_drivePropertyEffectTrans;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty/PropertyPointInfo/BasicPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_basicPointTextForElectronicProperty;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty/PropertyPointInfo/ExtraPointText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_extraPointTextForElectronicProperty;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty/EffectDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_electronicPropertyEffectTrans;
        [AutoBind("./AddPropertyPointRoot/AddPropertyPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addPropertyPointButton;
        [AutoBind("./AddPropertyPointRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_addPropertyCtrl;
        [AutoBind("./AddPropertyPointRoot/AddPropertyPointButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_propertyPointRedPoint;
        [AutoBind("./ButtonsRoot/DrivingLicenseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_drivingLicenseButton;
        [AutoBind("./ButtonsRoot/DrivingLicenseButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_drivingLicenseRedPoint;
        [AutoBind("./ButtonsRoot/DrivingLicenseButton/RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_drivingLicenseButtonStateCtrl;
        [AutoBind("./ButtonsRoot/DrivingLicenseButton/RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_drivingLicenseLockedButton;
        [AutoBind("./ButtonsRoot/CharacterAchievementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_characterAchievementButton;
        [AutoBind("./ButtonsRoot/CharacterAchievementButton/AchievementCountInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_characterUnconfirmedAchievementInfoObj;
        [AutoBind("./ButtonsRoot/CharacterAchievementButton/AchievementCountInfo/AchievementCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterUnconfirmedAchievementCountText;
        [AutoBind("./ButtonsRoot/CharacterSkillButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_characterSkillButton;
        [AutoBind("./ButtonsRoot/CharacterSkillButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_characterSkillRedPoint;
        [AutoBind("./ButtonsRoot/CharacterSkillButton/RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_characterSkillButtonStateCtrl;
        [AutoBind("./ButtonsRoot/CharacterSkillButton/RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_characterSkillLockedButton;
        [AutoBind("./ButtonsRoot/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_chatButton;
        [AutoBind("./ButtonsRoot/DiplomacyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_diplomacyButton;
        [AutoBind("./ButtonsRoot/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_teamButton;
        [AutoBind("./ButtonsRoot/MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_mailButton;
        [AutoBind("./WantedLevelInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_crimeInfoStateCtrl;
        [AutoBind("./WantedLevelInfoRoot/WantedLevelNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_crimeLevelText;
        [AutoBind("./ShowAndCloseState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./BasicPropertiesInfoRoot/WeaponBasicProperty/PropertyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyNameTextC1;
        [AutoBind("./BasicPropertiesInfoRoot/DefenseBasicProperty/PropertyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyNameTextC2;
        [AutoBind("./BasicPropertiesInfoRoot/ElectronicBasicProperty/PropertyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyNameTextC3;
        [AutoBind("./BasicPropertiesInfoRoot/DriveBasicProperty/PropertyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyNameTextC4;
        [AutoBind("./SetDiplomacyPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_setDiplomacyPanelDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetOverviewInfoMode;
        private static DelegateBridge __Hotfix_SetGuildInfo;
        private static DelegateBridge __Hotfix_SetKillRecordButtonActive;
        private static DelegateBridge __Hotfix_SetBasicPropertyInfo;
        private static DelegateBridge __Hotfix_SetPropertyPoint;
        private static DelegateBridge __Hotfix_SetCurrUnconfirmedAchievementCount;
        private static DelegateBridge __Hotfix_SetCrimeLevelInfo;
        private static DelegateBridge __Hotfix_ShowRedPoint;
        private static DelegateBridge __Hotfix_ShowKillRecordRedPoint;
        private static DelegateBridge __Hotfix_GetSetDiplomacyPanelDummy;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnAddPropertyPointButtonClick;
        private static DelegateBridge __Hotfix_OnAchievementButtonClick;
        private static DelegateBridge __Hotfix_OnSkillButtonClick;
        private static DelegateBridge __Hotfix_OnSkillLockedButtonClick;
        private static DelegateBridge __Hotfix_OnDrivingLicenseButtonClick;
        private static DelegateBridge __Hotfix_OnDrivingLicenseLockedButtonClick;
        private static DelegateBridge __Hotfix_OnChatButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyButtonClick;
        private static DelegateBridge __Hotfix_OnTeamButtonClick;
        private static DelegateBridge __Hotfix_OnMailButtonClick;
        private static DelegateBridge __Hotfix_OnKillRecordButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponContrlButtonClick;
        private static DelegateBridge __Hotfix_OnDefenseContrlButtonClick;
        private static DelegateBridge __Hotfix_OnDriveContrlButtonClick;
        private static DelegateBridge __Hotfix_OnElectronicContrlButtonClick;
        private static DelegateBridge __Hotfix_OnWantedButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddPropertyPointButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddPropertyPointButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAchievementButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAchievementButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSkillButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSkillButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDrivingLicenseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDrivingLicenseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnChatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeaponContrlButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponContrlButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDefenseOperateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDefenseOperateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnShipDriveButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipDriveButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnElectronicWarButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnElectronicWarButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnKillRecordButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnKillRecordButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWantedInfoClick;
        private static DelegateBridge __Hotfix_remove_EventOnWantedInfoClick;

        public event Action EventOnAchievementButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnAddPropertyPointButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnChatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDefenseOperateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDiplomacyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnDrivingLicenseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnElectronicWarButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnKillRecordButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnShipDriveButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSkillButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTeamButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWantedInfoClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWeaponContrlButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSetDiplomacyPanelDummy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAchievementButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddPropertyPointButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDefenseContrlButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDriveContrlButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrivingLicenseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrivingLicenseLockedButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnElectronicContrlButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRecordButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillLockedButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWantedButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponContrlButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBasicPropertyInfo(int basicWeaponProperty, int basicDefenseProperty, int basicDriveProperty, int basicElectronicProperty, int extraWeaponProperty, int extraDefenseProperty, int extraDriveProperty, int extraElectronicProperty, bool needPlayPropertyChangingEffect = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCrimeLevelInfo(float crimeLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrUnconfirmedAchievementCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildInfo(string guildName, GuildLogoInfo logo, Dictionary<string, UnityEngine.Object> resDict, string jobName = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetKillRecordButtonActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOverviewInfoMode(bool isSelfInfoMode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyPoint(int pointCount)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowKillRecordRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRedPoint(bool isShow)
        {
        }
    }
}

