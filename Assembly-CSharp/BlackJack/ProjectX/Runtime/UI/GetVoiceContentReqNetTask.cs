﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GetVoiceContentReqNetTask : NetWorkTransactionTask
    {
        private readonly List<ulong> m_instanceIdList;
        private readonly ChatChannel m_channel;
        private List<ChatContentVoice> m_voiceContentList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGetVoiceContentAck;
        private static DelegateBridge __Hotfix_get_VoiceContentList;

        [MethodImpl(0x8000)]
        public GetVoiceContentReqNetTask(ChatChannel channel, List<ulong> instanceIdList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetVoiceContentAck(List<ChatContentVoice> voiceList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public List<ChatContentVoice> VoiceContentList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

