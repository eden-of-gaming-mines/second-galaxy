﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ChatUIHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_GetAnnouncementString;
        private static DelegateBridge __Hotfix_AddMsgToChat;
        private static DelegateBridge __Hotfix_ConvertContentToView;
        private static DelegateBridge __Hotfix_GetStoreItemName;
        private static DelegateBridge __Hotfix_GetChatMsgNameText;
        private static DelegateBridge __Hotfix_GetBannedStr;
        private static DelegateBridge __Hotfix_GetLastChatUIInfo;

        [MethodImpl(0x8000)]
        public static void AddMsgToChat(string msg, ChatChannel channel)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertContentToView(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAnnouncementString(int systemMessageId, string content, List<FormatStringParamInfo> formatStringParamInfos)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBannedStr()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetChatMsgNameText(ChatChannel chatChannel, string name = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatUIInfo GetLastChatUIInfo()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStoreItemName(int configId, StoreItemType itemType)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

