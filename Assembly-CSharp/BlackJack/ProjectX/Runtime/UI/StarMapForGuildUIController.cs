﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class StarMapForGuildUIController : UIControllerBase
    {
        public CommonNewAreaInfoUIController m_newAreaInfoUICtrl;
        [AutoBind("./FunctionButtonGroup/SwitchSolarSystemButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SwitchSolarSystemButtonCountText;
        [AutoBind("./ShieldInfoPanel/ShieldProcessImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldProcessImage;
        [AutoBind("./ShieldInfoPanel/ArmorProcessImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorProcessImage;
        [AutoBind("./ShieldInfoPanel/ShieldValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldValueText;
        [AutoBind("./ShieldInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShieldInfoPanelRoot;
        [AutoBind("./BuildingOperateErrorConfirmPanel/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuildingOperateErrorConfirmButton;
        [AutoBind("./BuildingOperateErrorConfirmPanel/ContentGroup/HintText02", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingOperateErrorConfirmHintText02;
        [AutoBind("./BuildingOperateErrorConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BuildingOperateErrorConfirmPanelGo;
        [AutoBind("./StartBuildingBattleConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StartBuildingBattleConfirmPanelGo;
        [AutoBind("./StartBuildingBattleConfirmPanel/ContentGroup/Buttons/ConfirmButton/CostMoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StartBuildingBattleCostMoneyText;
        [AutoBind("./StartBuildingBattleConfirmPanel/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StartBuildingBattleConfirmButton;
        [AutoBind("./StartBuildingBattleConfirmPanel/BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StarBuildingBattleConfirmPanelBGButton;
        [AutoBind("./FunctionButtonGroup/SwitchSolarSystemButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SwitchSolarSystemButton;
        [AutoBind("./FunctionButtonGroup/StarfieldButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StarfieldButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StarMapForGuildStateCtrl;
        [AutoBind("./NewAreaArriveDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform NewAreaArriveDummyRoot;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./FunctionButtonGroup/GuildActionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildActionButton;
        [AutoBind("./FunctionButtonGroup/GuildActionButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildActionButtonCountText;
        [AutoBind("./FunctionButtonGroup/GuildActionButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildActionState;
        [AutoBind("./FunctionButtonGroup/OccupyWarButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OccupyWarButton;
        [AutoBind("./FunctionButtonGroup/OccupyWarButton/ItemNumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_occupyWarButtonSubscriptObj;
        [AutoBind("./FunctionButtonGroup/OccupyWarButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_occupyWarButtonSubscriptText;
        [AutoBind("./StartBuildingBattleConfirmPanel/ContentGroup/HintText02", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_startBuildingBattleNotfiyText;
        [AutoBind("./StartBuildingBattleConfirmPanel/ContentGroup/HintText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_startBuildingBattleTimeNotfiyText;
        [AutoBind("./StartBuildingBattleConfirmPanel/MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradeMoneyValueText;
        [AutoBind("./StartBuildingBattleConfirmPanel/MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildTradeMoneyAddButton;
        private static DelegateBridge __Hotfix_SetOccupyWarButtonSubscriptText;
        private static DelegateBridge __Hotfix_CreateShowPanelProcess;
        private static DelegateBridge __Hotfix_ShowBuildingOperateErrorConfirmPanel;
        private static DelegateBridge __Hotfix_ShowStartBuildingBattleConfirmPanel;
        private static DelegateBridge __Hotfix_UpdateStartBuildingBattleConfirmPanel;
        private static DelegateBridge __Hotfix_ShowBuildingShieldInfo;
        private static DelegateBridge __Hotfix_UpdateStarMapForGuild;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess CreateShowPanelProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetOccupyWarButtonSubscriptText(SovereignBattleSimpleInfo sovereignBattleSimpleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBuildingOperateErrorConfirmPanel(bool isShow, string msg)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBuildingShieldInfo(bool isShow, float currShieldValue = 0f, float maxShieldValue = 1f, float currArmorValue = 0f, float maxArmorValue = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowStartBuildingBattleConfirmPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarMapForGuild(int occpiedSolarSystemCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStartBuildingBattleConfirmPanel(long costMoney, ulong currMoney, GDBSolarSystemInfo solarSystemInfo, string guildName, string guildCodeName, GuildBuildingType buildingType)
        {
        }
    }
}

