﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MailUIController : UIControllerBase
    {
        public List<MailItemUIController> m_mailItemUICtrlList;
        public MailItemDetailUICtrl m_mailItemDetailUICtrl;
        public MailSendUIController m_mailSendUIController;
        private List<LBStoredMail> m_mailListCache;
        private Dictionary<string, UnityEngine.Object> m_resDic;
        private bool m_isEditorMode;
        private string m_plyaeritemPoolName;
        private string m_playerItemAssetName;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, LBStoredMail, bool> EventOnMailSelectedChange;
        private HashSet<ulong> m_selectedItemHashSet;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainCtrl;
        [AutoBind("./MailListPanel/MailScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./MailListPanel/MailScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_objPool;
        [AutoBind("./EmptyText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_emptyText;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroundButton;
        [AutoBind("./ToggleScroll View/Viewport/Content/AllToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AllToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/AllToggle/CountImage/UnReadMailCountText_All", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextAll;
        [AutoBind("./ToggleScroll View/Viewport/Content/PlayerToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx PlayerToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/PlayerToggle/CountImage/UnReadMailCountText_Player", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextPlayer;
        [AutoBind("./ToggleScroll View/Viewport/Content/SystemToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SystemToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/SystemToggle/CountImage/UnReadMailCountText_System", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextSystem;
        [AutoBind("./ToggleScroll View/Viewport/Content/GuildToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx GuildToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/GuildToggle/CountImage/UnReadMailCountText_Guild", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextGuild;
        [AutoBind("./ToggleScroll View/Viewport/Content/AllianceToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AllianceToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/AllianceToggle/CountImage/UnReadMailCountText_Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextAlliance;
        [AutoBind("./ToggleScroll View/Viewport/Content/AuctionToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AuctionToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/AuctionToggle/CountImage/UnReadMailCountText_Auction", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unReadMailCountTextAuction;
        [AutoBind("./MailListPanel/ButtonGroup/AllPostHolenButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GetAllButton;
        [AutoBind("./MailListPanel/ButtonGroup/AllDeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeleteAllButton;
        [AutoBind("./MailListPanel/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mailListButtonCtrl;
        [AutoBind("./MailListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_maillListPanel;
        [AutoBind("./MailDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_mailDetailPanel;
        [AutoBind("./SendMailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sendMailPanel;
        [AutoBind("./ItemSimpleInfoPanelLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSimpleInfoPanelLeftDummy;
        [AutoBind("./ItemSimpleInfoPanelRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSimpleInfoPanelRightDummy;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBStoredMail> EventOnMailItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<MailUITask.ModeCategory> EventOnMailTableTypeChange;
        [CompilerGenerated]
        private static Predicate<LBStoredMail> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<LBStoredMail> <>f__am$cache1;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateMailStoreInfo;
        private static DelegateBridge __Hotfix_UpdateSendUIInfo_1;
        private static DelegateBridge __Hotfix_UpdateSendUIInfo_0;
        private static DelegateBridge __Hotfix_UpdateMailListUIInfo;
        private static DelegateBridge __Hotfix_UpdateMailInfoDetail;
        private static DelegateBridge __Hotfix_ClearUIMode;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelPos;
        private static DelegateBridge __Hotfix_OnMailListItemFill;
        private static DelegateBridge __Hotfix_OnMailListItemClick;
        private static DelegateBridge __Hotfix_OnMailTableChange;
        private static DelegateBridge __Hotfix_OnMailSelectedChange;
        private static DelegateBridge __Hotfix_OnMailTableChange_Guild;
        private static DelegateBridge __Hotfix_OnMailTableChange_Alliance;
        private static DelegateBridge __Hotfix_OnMailTableChange_System;
        private static DelegateBridge __Hotfix_OnMailTableChange_Player;
        private static DelegateBridge __Hotfix_OnMailTableChange_Auction;
        private static DelegateBridge __Hotfix_OnMailTableChange_All;
        private static DelegateBridge __Hotfix_GetUIProcess;
        private static DelegateBridge __Hotfix_GetMailListButtonState;
        private static DelegateBridge __Hotfix_SetMailListButtonState;
        private static DelegateBridge __Hotfix_add_EventOnMailSelectedChange;
        private static DelegateBridge __Hotfix_remove_EventOnMailSelectedChange;
        private static DelegateBridge __Hotfix_add_EventOnMailItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailItemClick;
        private static DelegateBridge __Hotfix_add_EventOnMailTableTypeChange;
        private static DelegateBridge __Hotfix_remove_EventOnMailTableTypeChange;

        public event Action<LBStoredMail> EventOnMailItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, LBStoredMail, bool> EventOnMailSelectedChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<MailUITask.ModeCategory> EventOnMailTableTypeChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearUIMode()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelPos(float process)
        {
        }

        [MethodImpl(0x8000)]
        private MailListButtonState GetMailListButtonState(List<LBStoredMail> mailList, MailUITask.ModeCategory currCategory)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailListItemClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailListItemFill(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailSelectedChange(UIControllerBase ctrl, LBStoredMail selectedMail, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange(MailUITask.ModeCategory currentCategory)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_All(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_Alliance(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_Auction(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_Guild(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_Player(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableChange_System(UIControllerBase uiCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMailListButtonState(MailListButtonState state)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateMailInfoDetail(LBStoredMail currentDetailMailInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool showTranslate)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateMailListUIInfo(MailUITask.ModeCategory currCategory, List<LBStoredMail> mailList, bool isEditorMode, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, HashSet<ulong> selectedItemHashSet)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateMailStoreInfo(int allCount, int playerCount, int systemCount, int guildCount, int allianceCount, int auctionCount)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateSendUIInfo(string sendFromGameUserName)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateSendUIInfo(int sendToGameUserAvatarId, ProfessionType profession, string sendToGameUserName, string sendToGameUserId, string sendFromGameUserName, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public enum MailListButtonState
        {
            None,
            Get,
            Delete
        }
    }
}

