﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class ScreenFadeInOutUITask : UITaskBase
    {
        private ScreenFadeInOutUIController m_mainCtrl;
        protected List<CachedEndActionItem> m_cachedEndActionItemList;
        protected List<CachedEndActionItem> m_removeEndActionItemList;
        private bool m_isFadeIn;
        private bool m_isNeedExecuteFade;
        private bool m_isImmediate;
        private bool m_isAutoFadeIn;
        private float m_autoFadeInTime;
        private Color m_fadeColor;
        private Action m_onEnd;
        private DateTime m_autoFadeInOutTime;
        private float m_manualFadeTime;
        private bool m_isShowLoadingImage;
        private bool m_isDuringFadeInOut;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKeyFadeIn = "FadeIn";
        public const string ParamKeyIsImmediate = "IsImmediate";
        public const string ParamKeyAutoFadeIn = "AutoFadeIn";
        public const string ParamKeyAutoFadeInTime = "AutoFadeInTime";
        public const string ParamKeyOnEndFunc = "OnEndFunc";
        public const string ParamKeyBlackColor = "BlackColor";
        public const string ParamKeyManualFadeTime = "ManualFadeTime";
        public const string ParamKeyIsShowLoading = "IsShowLoading";
        public const string TaskName = "ScreenFadeInOutUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickAutoFadeIn;
        private static DelegateBridge __Hotfix_TickCachedEndActionItem;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_StartFadeIn;
        private static DelegateBridge __Hotfix_StartFadeOut;
        private static DelegateBridge __Hotfix_StartFadeInOut;
        private static DelegateBridge __Hotfix_StartManualFadeOut;
        private static DelegateBridge __Hotfix_StartManualFadeIn;
        private static DelegateBridge __Hotfix_StartManualFadeInOut;
        private static DelegateBridge __Hotfix_StartFadeOutAndAutoFadeIn;
        private static DelegateBridge __Hotfix_UpdateProcessValue;
        private static DelegateBridge __Hotfix_UpdateLoadProcessValue;
        private static DelegateBridge __Hotfix_OnFadeInOutEnd;
        private static DelegateBridge __Hotfix_GetParamFromUIIntent;
        private static DelegateBridge __Hotfix_GetFadeInOutEndTime;
        private static DelegateBridge __Hotfix_CalcAutoFadeInDelayTime;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ScreenFadeInOutUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAutoFadeInDelayTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetFadeInOutEndTime()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetParamFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFadeInOutEnd()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public static ScreenFadeInOutUITask StartFadeIn(bool isImmediate = false, Action onEnd = null, bool blackColor = true, bool isShowLoading = false)
        {
        }

        [MethodImpl(0x8000)]
        protected static ScreenFadeInOutUITask StartFadeInOut(bool isFadeIn, bool isImmediate = false, Action onEnd = null, bool blackColor = true, bool isShowLoading = false)
        {
        }

        [MethodImpl(0x8000)]
        public static ScreenFadeInOutUITask StartFadeOut(bool isImmediate = false, Action onEnd = null, bool blackColor = true, bool isShowLoading = false)
        {
        }

        [MethodImpl(0x8000)]
        public static ScreenFadeInOutUITask StartFadeOutAndAutoFadeIn(bool isImmediateFadeOut, float autoFadeInTime = 0f, Action onEnd = null, bool blackColor = true, bool isShowLoading = false)
        {
        }

        [MethodImpl(0x8000)]
        public static ScreenFadeInOutUITask StartManualFadeIn(float fadeTime, Action onEnd = null, bool blackColor = true)
        {
        }

        [MethodImpl(0x8000)]
        protected static ScreenFadeInOutUITask StartManualFadeInOut(bool isFadeIn, float fadeTime, Action onEnd = null, bool blackColor = true)
        {
        }

        [MethodImpl(0x8000)]
        public static ScreenFadeInOutUITask StartManualFadeOut(float fadeTime, Action onEnd = null, bool blackColor = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickAutoFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickCachedEndActionItem()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLoadProcessValue(float val)
        {
        }

        [MethodImpl(0x8000)]
        public static void UpdateProcessValue(float val)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class CachedEndActionItem
        {
            public Action m_onEnd;
            public DateTime m_outdateTime;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

