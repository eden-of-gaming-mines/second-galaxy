﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipWeaponEquipDetailInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDetailInfoUITabChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnGotoTechButtonClick;
        private CommonItemDetailInfoUIController m_itemDetailInfoUICtrl;
        private CommonItemIconUIController m_itemIconUICtrl;
        private CommonItemDetailInfoUIController.DetailInfoTabType m_weaponTab;
        private CommonItemDetailInfoUIController.DetailInfoTabType m_ammoTab;
        private CommonItemDetailInfoUIController.DetailInfoTabType m_equipTab;
        private bool m_isEquipDetail;
        private bool m_isWeaponDetail;
        private bool m_isAmmoDetail;
        public FakeLBStoreItem m_SelectedLBItemFake;
        private string m_itemDetailAssetName;
        private string m_itemAssetName;
        private const string UIStateName_Close = "PanelClose";
        private const string UIStateName_Open = "PanelOpen";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Quality/QualityBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankStateCtrl;
        [AutoBind("./ItemDetailInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_itemDetailInfoDummy;
        [AutoBind("./ItemPropertyRect", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_itemPropertyRect;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_itemDummy;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./Tech/TechImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TechImage;
        [AutoBind("./Quality/QualityImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image QualityImage;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowWeaponEquipDetailInfo;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfo_0;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfo_1;
        private static DelegateBridge __Hotfix_SetWeaponEquipSimpleInfo;
        private static DelegateBridge __Hotfix_OnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;

        public event Action EventOnDetailInfoUITabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailInfoUITabChanged(CommonItemDetailInfoUIController.DetailInfoTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetWeaponEquipSimpleInfo(LBStaticWeaponEquipSlotGroup slotGroup, FakeLBStoreItem item, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowWeaponEquipDetailInfo(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponEquipInfo(LBStaticWeaponEquipSlotGroup slotGroup, bool isAmmo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponEquipInfo(LBStaticWeaponEquipSlotGroup slotGroup, StoreItemType itemType, object confInfo, bool isBind, bool isFreezing, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isCompared = true)
        {
        }
    }
}

