﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeManageUIController : UIControllerBase
    {
        private TechUpgradeOverviewUIController m_overviewPanelCtrl;
        private List<TechCategoryToggleUIController> m_techCategoryToggleCtrlList;
        private Dictionary<int, TechTypeToggleUIController> m_techUITypeIdToToggleCtrlDict;
        private TechUpgradeTreeUIController m_techTreeCtrl;
        private SystemDescriptionController m_descriptionCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TechCategoryToggleUIController> EventOnTechCategoryToggleValueChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TechType> EventOnModelButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnTechTypeToggleValueChanged;
        private const float TotalProgerssFillStartValue = 0.07f;
        private const float TotalProgerssFillEndValue = 0.926f;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./OverviewPanel/TotalValueProcessBarBG", AutoBindAttribute.InitState.NotInit, false)]
        public Image TotalProcessbar;
        [AutoBind("./OverviewPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OverviewPanel;
        [AutoBind("./OverviewPanel/TotalValueGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OverviewValueText;
        [AutoBind("./OverviewPanel/ModuleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OverviewModuleGroup;
        [AutoBind("./OverviewPanel/ModuleGroup/Ship", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/Weapon", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeaponModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/ShipProject", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProjectModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/ElectronicEngineering", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ElectricModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/Manufacture", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProduceModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/Management", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CommandModelStateCtrl;
        [AutoBind("./OverviewPanel/ModuleGroup/Weapon", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WeaponModelButton;
        [AutoBind("./OverviewPanel/ModuleGroup/Ship", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipModelButton;
        [AutoBind("./OverviewPanel/ModuleGroup/ShipProject", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ProjectModelButton;
        [AutoBind("./OverviewPanel/ModuleGroup/ElectronicEngineering", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ElectricModelButton;
        [AutoBind("./OverviewPanel/ModuleGroup/Manufacture", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ProduceModelButton;
        [AutoBind("./OverviewPanel/ModuleGroup/Management", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CommandModelButton;
        [AutoBind("./TogglesPanel/ToggleScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechCategoryGroup;
        [AutoBind("./TogglesPanel/ToggleScrollView/Viewport/Content/TechCategoryTogglePrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechCategoryTogglePrefab;
        [AutoBind("./TogglesPanel/ToggleScrollView/Viewport/Content/TechCategoryTogglePrefab/TechTypeItemRoot/TechItemToggle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechTypeItemPrefab;
        [AutoBind("./TechTreePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechTreePanel;
        [AutoBind("./TechTreePanel/PanelColorChange", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TechTreePanelStateUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_InitTechUpgradeManageUI;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_CreateAllTechToggles;
        private static DelegateBridge __Hotfix_InitOverviewInfo;
        private static DelegateBridge __Hotfix_UpdateTechCategoryAndTypeToggle;
        private static DelegateBridge __Hotfix_IsNeedPlayTechTreeHideAnim;
        private static DelegateBridge __Hotfix_ShowModelButtonClickAnim;
        private static DelegateBridge __Hotfix_ShowToggleSelectEffect;
        private static DelegateBridge __Hotfix_SetTechOverViewPanelData;
        private static DelegateBridge __Hotfix_UpdateTechTreeInfo;
        private static DelegateBridge __Hotfix_ShowOrHideSystemDescriptionButton;
        private static DelegateBridge __Hotfix_GetCategoryToggleCtrl;
        private static DelegateBridge __Hotfix_GetCatagoryToggleTransByType;
        private static DelegateBridge __Hotfix_GetTechTransInTechType;
        private static DelegateBridge __Hotfix_OnWeaponModelButtonClick;
        private static DelegateBridge __Hotfix_OnShipModelButtonClick;
        private static DelegateBridge __Hotfix_OnProduceModelButtonClick;
        private static DelegateBridge __Hotfix_OnCommandModelButtonClick;
        private static DelegateBridge __Hotfix_OnProjectModelButtonClick;
        private static DelegateBridge __Hotfix_OnElectricModelButtonClick;
        private static DelegateBridge __Hotfix_OnTechCategoryToggleValueChanged;
        private static DelegateBridge __Hotfix_OnTechTypeButtonClick;
        private static DelegateBridge __Hotfix_get_OverviewPanelCtrl;
        private static DelegateBridge __Hotfix_get_TechTreeCtrl;
        private static DelegateBridge __Hotfix_add_EventOnTechCategoryToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTechCategoryToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnModelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnModelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTechTypeToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTechTypeToggleValueChanged;

        public event Action<TechType> EventOnModelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TechCategoryToggleUIController> EventOnTechCategoryToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnTechTypeToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateAllTechToggles(Dictionary<TechType, List<ConfigDataTechUITypeInfo>> techCategoryToTechTypeListInfoDict, Action<TechCategoryToggleUIController> onCategoryToggleSelected, Action<int> onTechTypeToggleSeleced, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCatagoryToggleTransByType(TechType type)
        {
        }

        [MethodImpl(0x8000)]
        private TechCategoryToggleUIController GetCategoryToggleCtrl(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetMainUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetTechTransInTechType(TechType type, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void InitOverviewInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void InitTechUpgradeManageUI(Dictionary<TechType, List<ConfigDataTechUITypeInfo>> techCategoryToTechTypeListInfoDict, Action<TechCategoryToggleUIController> onCategoryToggleSelected, Action<int> onTechTypeToggleSeleced, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedPlayTechTreeHideAnim()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommandModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnElectricModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProduceModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProjectModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechCategoryToggleValueChanged(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponModelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTechOverViewPanelData(Dictionary<TechType, int> category2LearntTechLevelCountDict, Dictionary<TechType, int> category2TotalTechLevelCountDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowModelButtonClickAnim(int category)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSystemDescriptionButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowToggleSelectEffect(int category)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechCategoryAndTypeToggle(int selCategory, int selTypeId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechTreeInfo(TechType selectCategory, List<ConfigDataTechInfo> techInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public TechUpgradeOverviewUIController OverviewPanelCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TechUpgradeTreeUIController TechTreeCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

