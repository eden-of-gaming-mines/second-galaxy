﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LossWarningWindowUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private float m_spaceSize;
        private Vector2 m_attachRectSize;
        private Vector3 m_panelPos;
        private LossWarningWindowPos m_showPanelType;
        private bool m_showAutoMove;
        private bool m_showSafety;
        private bool m_showDanger;
        private LossWarningWindowUIController m_mainCtrl;
        public const string ParamKeyPanelPos = "Postion";
        public const string ParamKeyPanelPosType = "PosType";
        public const string ParamKeyRectSize = "RectSize";
        public const string ParamKeySpaceSize = "SpaceSize";
        public const string ParamKeyShowAutoMove = "ShowAutoMove";
        public const string ParamKeyShowSafety = "ShowSafety";
        public const string ParamKeyShowDanger = "ShowDanger";
        public const string TaskName = "LossWarningWindowUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowLossWarningWindowUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBGImageButtonClick;
        private static DelegateBridge __Hotfix_HideCharSimpleInfoPanel;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public LossWarningWindowUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void HideCharSimpleInfoPanel(Action onProcessEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGImageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPanelPostion()
        {
        }

        [MethodImpl(0x8000)]
        public static LossWarningWindowUITask ShowLossWarningWindowUITask(bool showAutoMove, bool showSafety, bool showDanger, Vector3 pos, Vector2 rectSize, float spaceSize = 0f, LossWarningWindowPos postionType = 0, UIIntent returnIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideCharSimpleInfoPanel>c__AnonStorey0
        {
            internal Action onProcessEnd;
            internal LossWarningWindowUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                if (this.onProcessEnd != null)
                {
                    this.onProcessEnd();
                }
            }
        }

        public enum LossWarningWindowPos
        {
            UseInput,
            Left,
            Right
        }
    }
}

