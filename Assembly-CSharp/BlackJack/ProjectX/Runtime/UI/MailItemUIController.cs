﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class MailItemUIController : UIControllerBase
    {
        public LBStoredMailClient m_lbStoredMailClientCache;
        public DateTime m_currentTime;
        private const string HasPlayerState = "FromPlayer";
        private const string NotHasPlayerState = "FromSystem";
        private float m_lastTickTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, LBStoredMail, bool> EventOnMailSelectedChange;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./MailSendTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MailSendTimeText;
        [AutoBind("./TimeOutRoot/TimeOutText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeOutText;
        [AutoBind("./SelectedToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SelectedToggle;
        [AutoBind("./PlayerInfo/PlayerIcon/ProfessionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionImage;
        [AutoBind("./PlayerInfo/PlayerIcon/CharacterIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CharacterIconImage;
        [AutoBind("./PlayerInfo/CharacterNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterNameText;
        [AutoBind("./MailState/UnReadImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UnReadImgage;
        [AutoBind("./MailState/ReadImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ReadImage;
        [AutoBind("./MailState/AttachmentTagImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image AttachmentTagImage;
        private static DelegateBridge __Hotfix_RegToggleValueChangeEvent;
        private static DelegateBridge __Hotfix_OnSelectedToggleValueChange;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateMailItemUIInfo;
        private static DelegateBridge __Hotfix_ShowMailItemSelected;
        private static DelegateBridge __Hotfix_SetMailItemSelectState;
        private static DelegateBridge __Hotfix_SetToggleState;
        private static DelegateBridge __Hotfix_UpdateMailSendInfo;
        private static DelegateBridge __Hotfix_UpdateMailState;
        private static DelegateBridge __Hotfix_UpateTimeOutText;
        private static DelegateBridge __Hotfix_IsMailWillTimeOut;
        private static DelegateBridge __Hotfix_get_m_lbPlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnMailSelectedChange;
        private static DelegateBridge __Hotfix_remove_EventOnMailSelectedChange;

        public event Action<UIControllerBase, LBStoredMail, bool> EventOnMailSelectedChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private bool IsMailWillTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelectedToggleValueChange(bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        public void RegToggleValueChangeEvent(Action<UIControllerBase, LBStoredMail, bool> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMailItemSelectState(bool isSeleceted)
        {
        }

        [MethodImpl(0x8000)]
        private void SetToggleState(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMailItemSelected(bool isShowSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void UpateTimeOutText()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMailItemUIInfo(LBStoredMail storedMailClient, bool isEditor, Dictionary<string, Object> resDic, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMailSendInfo(Dictionary<string, Object> resDic, MailInfo mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMailState(bool isReaded, bool hasAttachment)
        {
        }

        private ILBPlayerContext m_lbPlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

