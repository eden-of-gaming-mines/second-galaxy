﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class SwitchGuildSolarSystemPanelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSwitchSolarSystemItemClick;
        private List<SwitchGuildSolarSystemPanelItemUIController> m_itemCtrlList;
        [AutoBind("./BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SwitchSolarSystemBgButton;
        [AutoBind("./SwitchSolarSystemListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SwitchSolarSystemPanelButton;
        [AutoBind("./SwitchSolarSystemListPanel/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform SwitchItemListRoot;
        [AutoBind("./UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SwitchPanelItemEasyPool;
        [AutoBind("./UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform UnusedItemRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SwitchPanelStateCtrl;
        private static DelegateBridge __Hotfix_ShowPanel;
        private static DelegateBridge __Hotfix_SwitchPanelVisibleState;
        private static DelegateBridge __Hotfix_UpdateSwitchGuildSolarSystemPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnSwitchPanelItemCreate;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_PrepareSwitchItems;
        private static DelegateBridge __Hotfix_IsGuildSolarSystemInWar;
        private static DelegateBridge __Hotfix_IsGuildBase;
        private static DelegateBridge __Hotfix_add_EventOnSwitchSolarSystemItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSwitchSolarSystemItemClick;

        public event Action<int> EventOnSwitchSolarSystemItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private bool IsGuildBase(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGuildSolarSystemInWar(GuildSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSwitchPanelItemCreate(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareSwitchItems(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPanel(bool isShow, bool isImmediately = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchPanelVisibleState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSwitchGuildSolarSystemPanel(int currSolarSystemId, List<StarMapForGuildUITask.GuildOccupiedSolarSystemInfo> guildSolarSystemInfos, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

