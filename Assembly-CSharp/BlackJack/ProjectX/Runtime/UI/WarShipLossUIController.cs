﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class WarShipLossUIController : UIControllerBase
    {
        private const string ShipLostAssetPath = "LostItemUIPrefab";
        private const string ShipLostString = "{0}<size=18>{1}</size>";
        private string m_shipUnitString;
        private const string StateNormal = "Normal";
        private const string StateEmpty = "Empty";
        private WarBothSidesInfoUIController m_warBothSidesInfoUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, bool, Vector3> EvenOnShipIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EvenOnItemFilled;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/LeftSovereigntyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DefendGuildButton;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/RightSovereigntyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AttackGuildButton;
        [AutoBind("./ProgressOfABattleGroup/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EmptyGroupStateCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnGuildNameClick;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TitleGroupGameObject;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildShipLossUIPrefabCommonUIStateController;
        [AutoBind("./ProgressOfABattleGroup/RightSimpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform RightSimpleDummy;
        [AutoBind("./ProgressOfABattleGroup/LeftSimpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LeftSimpleDummy;
        [AutoBind("./ProgressOfABattleGroup/LossGroup/LossItemGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRoot;
        [AutoBind("./ProgressOfABattleGroup/LossGroup/LossItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool LossItemGroupEasyObjectPool;
        [AutoBind("./ProgressOfABattleGroup/LossGroup/LossItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect LossItemGroupLoopVerticalScrollRect;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/KillGroup/KillRightNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillRightNumberText;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/KillGroup/KillLeftNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillLeftNumberText;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/KillGroup/Slider", AutoBindAttribute.InitState.NotInit, false)]
        public UnityEngine.UI.Slider Slider;
        private static DelegateBridge __Hotfix_UpdateWarShipLossUI;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolObjectCreat;
        private static DelegateBridge __Hotfix_OnShipIconClick;
        private static DelegateBridge __Hotfix_OnItemFileed;
        private static DelegateBridge __Hotfix_OnDefendGuildNameClick;
        private static DelegateBridge __Hotfix_OnAttackGuildNameClick;
        private static DelegateBridge __Hotfix_add_EvenOnShipIconClick;
        private static DelegateBridge __Hotfix_remove_EvenOnShipIconClick;
        private static DelegateBridge __Hotfix_add_EvenOnItemFilled;
        private static DelegateBridge __Hotfix_remove_EvenOnItemFilled;
        private static DelegateBridge __Hotfix_add_EventOnGuildNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildNameClick;

        public event Action<UIControllerBase> EvenOnItemFilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, bool, Vector3> EvenOnShipIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnGuildNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAttackGuildNameClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDefendGuildNameClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFileed(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreat(string poolName, GameObject poolObject)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipIconClick(UIControllerBase ctrl, bool isLeft)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarShipLossUI(GuildBattleReportInfo reportInfo, int scollItemTotalCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

