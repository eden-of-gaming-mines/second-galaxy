﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonWeaponEquipSimpleInfoUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainCtrl;
        [AutoBind("./ButtonGroupStateCtrlRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonGroupStateCtrl;
        [AutoBind("./WithButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonWeaponEquipSimpleInfoItemUIController m_withButtonSimpleInfoItemCtrl;
        [AutoBind("./NoButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonWeaponEquipSimpleInfoItemUIController m_noButtonSimpleInfoItemCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipStaticSlotGroupSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipInSpaceEquipSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipInSpaceWeaponSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipSuperWeaponSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipSuperEquipSlotSimpleInfo;
        private static DelegateBridge __Hotfix_CreateUIShowOrHideProcess;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess CreateUIShowOrHideProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInSpaceEquipSlotSimpleInfo(LBEquipGroupBase group, bool isFlagShip, Dictionary<string, Object> resDict, bool needButtonGroup = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInSpaceWeaponSlotSimpleInfo(LBWeaponGroupBase group, bool isFlagShip, Dictionary<string, Object> resDict, bool needButtonGroup = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipStaticSlotGroupSimpleInfo(LBStaticWeaponEquipSlotGroup group, bool isFlagShip, Dictionary<string, Object> resDict, bool needButtonGroup = false, bool needRecommend = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipSuperEquipSlotSimpleInfo(ConfigDataShipSuperEquipInfo superEquipInfo, ConfigDataSpaceShipInfo shipConfInfo, Dictionary<string, Object> resDict, bool needButtonGroup = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipSuperWeaponSlotSimpleInfo(ConfigDataSuperWeaponInfo superWeaponInfo, ConfigDataSpaceShipInfo shipConfInfo, Dictionary<string, Object> resDict, bool needButtonGroup = false)
        {
        }
    }
}

