﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildTradeOrderEditUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<long> EventOnCreatePurchaseOrder_AmountInput;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<long> EventOnCreatePurchaseOrder_PriceInput;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCreatePurchaseOrder_DropDownClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<long> EventOnEditPurchaseOrder_AmountInput;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<long> EventOnEditPurchaseOrder_PriceInput;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCreateTransportOrder_DropDownClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<long> EventOnCreateTransportOrer_AmountInput;
        private GuildTradeOrderEditUITask.GuildTradeTransportInfoData m_guildTradeTransportInfoData;
        private GuildTradeOrderEditUITask.GuildTradeCreateInfoData m_guildTradeCreateInfoData;
        private GuildTradeOrderEditUITask.GuildTradeEditInfoData m_guildTradeEditInfoData;
        private List<GuildTradePurchaseOrderTransportLogInfo> m_guildTradePurchaseOrderTransportLogInfoList;
        public GuildTradeOrderRevocationConfirmUIController m_guildTradeOrderRevocationConfirmUIController;
        [AutoBind("./GuildTradeOrderRevocationConfirmDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildTradeOrderRevocationConfirmDummy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGImages/ButtonGroup/IconGroup/IconImage/ClickArea", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TitleDetailInfoButton;
        [AutoBind("./TitleDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TitleDetailInfoPanelCtrl;
        [AutoBind("./TitleDetailInfoPanel/FrameImage/BGImage/DetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TitleDetailInfoDescCtrl;
        [AutoBind("./TitleDetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TitleDetailInfoCloseButton;
        [AutoBind("./LeftGroup/WaybillGroup/TaxButton/ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TaxDetailInfoButton;
        [AutoBind("./TaxDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TaxDetailInfoPanelCtrl;
        [AutoBind("./TaxDetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TaxDetailInfoCloseButton;
        [AutoBind("./YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx YesButton;
        [AutoBind("./RevocationButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button RevocationButton;
        [AutoBind("./LeftGroup/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        [AutoBind("./LeftGroup/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image SubRank;
        [AutoBind("./LeftGroup/CommonItemDummy/ItemIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemIcon;
        [AutoBind("./BGImages/CommonItemDummy/ItemIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image EditItemIcon;
        [AutoBind("./BGImages/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image EditSubRank;
        [AutoBind("./LeftGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./LeftGroup/ExistingText/ExistingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ExistingNumberText;
        [AutoBind("./LeftGroup/ItemDetailedText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemDetailedText;
        [AutoBind("./LeftGroup/WaybillGroup/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./LeftGroup/WaybillGroup/WaybillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OrderSumPriceText;
        [AutoBind("./LeftGroup/WaybillGroup/AgioImage/AgioText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DeltaSumPriceText;
        [AutoBind("./GoodsGroup/ItemGroup/RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown ChooseItemDropdownButton;
        [AutoBind("./GoodsGroup/ItemGroup/RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ChooseItemDropdownArrowUIStateCtrl;
        [AutoBind("./GoodsGroup/StandardPriceImage/StandardPriceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StandardPriceStateCtrl;
        [AutoBind("./GoodsGroup/PurchaseNumGroup/PurchaseNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public InputField PurchaseNumberInputText;
        [AutoBind("./GoodsGroup/PurchaseNumGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchaseNumberAddButton;
        [AutoBind("./GoodsGroup/PurchaseNumGroup/SubtractButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchaseNumberSubtractButton;
        [AutoBind("./GoodsGroup/PurchasePriceGroup/PurchasePriceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchasePriceValueText;
        [AutoBind("./GoodsGroup/PurchasePriceGroup/PurchasePriceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public InputField PurchasePriceValueInputText;
        [AutoBind("./GoodsGroup/PurchasePriceGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchasePriceAddButton;
        [AutoBind("./GoodsGroup/PurchasePriceGroup/SubtractButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchasePriceSubtractButton;
        [AutoBind("./GoodsGroup/StandardPriceImage/StandardPriceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StandardPriceValueText;
        [AutoBind("./ModificationGroup/LeftGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGItemNameLeft;
        [AutoBind("./ModificationGroup/LeftGroup/ExistingText/ExistingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGItemExistingNumText;
        [AutoBind("./ModificationGroup/LeftGroup/WaybillGroup/WaybillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGOrderSumPriceText;
        [AutoBind("./ModificationGroup/LeftGroup/WaybillGroup/AgioImage/AgioText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGOrderDeltaSumPriceText;
        [AutoBind("./ModificationGroup/LeftGroup/WaybillGroup/AgioImage/AgioText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MGDeltaUIStateCtrl;
        [AutoBind("./ModificationGroup/ModificationGroup/ItemGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGItemNameRight;
        [AutoBind("./ModificationGroup/ModificationGroup/StandardPriceGroup/StandardPriceText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MGStandardPriceStateCtrl;
        [AutoBind("./ModificationGroup/ModificationGroup/NumberGroup/PurchaseNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public InputField MGPurchaseNumberInputText;
        [AutoBind("./ModificationGroup/ModificationGroup/NumberGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MGPurchaseNumberAddButton;
        [AutoBind("./ModificationGroup/ModificationGroup/NumberGroup/SubtractButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MGPurchaseNumberSubtractButton;
        [AutoBind("./ModificationGroup/ModificationGroup/PriceGroup/PurchasePriceText", AutoBindAttribute.InitState.NotInit, false)]
        public InputField MGPurchasePriceInputText;
        [AutoBind("./ModificationGroup/ModificationGroup/PriceGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MGPurchasePriceAddButton;
        [AutoBind("./ModificationGroup/ModificationGroup/PriceGroup/SubtractButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MGPurchasePriceSubtractButton;
        [AutoBind("./ModificationGroup/ModificationGroup/StandardPriceGroup/StandardPriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MGStandardPriceText;
        [AutoBind("./ModificationGroup/LogGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect PurchaseOrderTransportLogScrollRect;
        [AutoBind("./ModificationGroup/LogGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool PurchaseOrderTransportLogTextObjPool;
        [AutoBind("./TransportGroup/PortGroup/RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown TransportPortDropdownButton;
        [AutoBind("./TransportGroup/PortGroup/RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TransportPortDropdownArrowUIStateCtrl;
        [AutoBind("./TransportGroup/NumberGroup/TransportNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public InputField TransportNumberInputText;
        [AutoBind("./TransportGroup/NumberGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TransportNumberAddButton;
        [AutoBind("./TransportGroup/NumberGroup/SubtractButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TransportNumberSubtractButton;
        [AutoBind("./TransportGroup/PriceGroup/TotalPriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportOrderSinglePrice;
        [AutoBind("./TransportGroup/CapacityGroup/CapacityValuText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportCapacityValueText;
        [AutoBind("./TransportGroup/TargetGroup/TargetText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportTargetText;
        [AutoBind("./MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyNumberText;
        [AutoBind("./MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddTradeMoneyBtn;
        private const string LogTextItemPoolName = "LogText";
        private static DelegateBridge __Hotfix_CreatePurchaseOrder;
        private static DelegateBridge __Hotfix_EditPurchaseOrder;
        private static DelegateBridge __Hotfix_CreateTransportOrder;
        private static DelegateBridge __Hotfix_GetTitleDetailInfoPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetTaxDetailInfoPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetTitleDetailInfoDescProcess;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_UpdateGoodsGroup;
        private static DelegateBridge __Hotfix_UpdateModificationGroup;
        private static DelegateBridge __Hotfix_UpdateTranportGroup;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateLogTextListPool;
        private static DelegateBridge __Hotfix_UpdatePricePercentageColor;
        private static DelegateBridge __Hotfix_UpdateStandardPriceColor;
        private static DelegateBridge __Hotfix_BuildTransportDropDown;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_OnCreatePurchaeOrder_ChooseItemDropdownClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_OnCreateTransportOrder_TransportDropdownClick;
        private static DelegateBridge __Hotfix_OnCreateTransportOrer_AmountInput;
        private static DelegateBridge __Hotfix_OnLogTextItemFill;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnCreatePurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_remove_EventOnCreatePurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_add_EventOnCreatePurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_remove_EventOnCreatePurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_add_EventOnCreatePurchaseOrder_DropDownClick;
        private static DelegateBridge __Hotfix_remove_EventOnCreatePurchaseOrder_DropDownClick;
        private static DelegateBridge __Hotfix_add_EventOnEditPurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_remove_EventOnEditPurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_add_EventOnEditPurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_remove_EventOnEditPurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_add_EventOnCreateTransportOrder_DropDownClick;
        private static DelegateBridge __Hotfix_remove_EventOnCreateTransportOrder_DropDownClick;
        private static DelegateBridge __Hotfix_add_EventOnCreateTransportOrer_AmountInput;
        private static DelegateBridge __Hotfix_remove_EventOnCreateTransportOrer_AmountInput;

        public event Action<long> EventOnCreatePurchaseOrder_AmountInput
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCreatePurchaseOrder_DropDownClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<long> EventOnCreatePurchaseOrder_PriceInput
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCreateTransportOrder_DropDownClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<long> EventOnCreateTransportOrer_AmountInput
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<long> EventOnEditPurchaseOrder_AmountInput
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<long> EventOnEditPurchaseOrder_PriceInput
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void BuildTransportDropDown(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, int curDropIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateLogTextListPool()
        {
        }

        [MethodImpl(0x8000)]
        public void CreatePurchaseOrder(GuildTradeOrderEditUITask.GuildTradeCreateInfoData guildTradeCreateInfoData, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateTransportOrder(GuildTradeOrderEditUITask.GuildTradeTransportInfoData guildTradeTransportInfoData, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void EditPurchaseOrder(GuildTradeOrderEditUITask.GuildTradeEditInfoData guildTradeEditInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTaxDetailInfoPanelShowOrHideProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTitleDetailInfoDescProcess(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTitleDetailInfoPanelShowOrHideProcess(string mode, bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaeOrder_ChooseItemDropdownClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_AmountInput(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PriceInput(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrder_TransportDropdownClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrer_AmountInput(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_AmountInput(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PriceInput(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogTextItemFill(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string name, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGoodsGroup(GuildTradeOrderEditUITask.GuildTradeCreateInfoData guildTradeCreateInfoData, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(ConfigDataNormalItemInfo itemConfig, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateModificationGroup(GuildTradeOrderEditUITask.GuildTradeEditInfoData guildTradeEditInfoData, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePricePercentageColor(CommonUIStateController ctrl, int percent)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStandardPriceColor(CommonUIStateController ctrl, int price)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTranportGroup(GuildTradeOrderEditUITask.GuildTradeTransportInfoData guildTradeTransportInfoData, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

