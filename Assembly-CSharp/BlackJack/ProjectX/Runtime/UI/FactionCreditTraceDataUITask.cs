﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class FactionCreditTraceDataUITask : UITaskBase
    {
        private bool m_CheckResult;
        private long m_CheckTickDelay;
        private DateTime WaveImageOutTime;
        private const int MinRandomTimeOut = 0x5dc;
        private const int MaxRandomTimeOut = 0x9c4;
        private FactionCreditTraceDataUIController m_MainCtrl;
        private int m_QuestConfigId;
        private int m_QuestInstanceId;
        private bool m_IsStarScanQuest;
        private bool m_IsPlanetScanQuest;
        private bool m_IsSolarSystemScanQuest;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamsKeyQuestInstanceId = "ParamsKeyQuestInstanceId";
        public const string ParamsKeyQuestConfigId = "ParamsKeyQuestConfigId";
        public const string TaskName = "FactionCreditTraceDataUITask";
        private bool m_isRepeatableUserGuideForFail;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_StartFactionCreditTraceDataUITask;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnDeatilInfoOpenClick;
        private static DelegateBridge __Hotfix_OnDeatilInfoCloseClick;
        private static DelegateBridge __Hotfix_OnGoStartMapButtonClick;
        private static DelegateBridge __Hotfix_OnYesButtonClick;
        private static DelegateBridge __Hotfix_OnRecordButtonClick;
        private static DelegateBridge __Hotfix_QuestChecking;
        private static DelegateBridge __Hotfix_SendQuestCheckingSucessReq;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_get_RangeTime;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_OnRepeatableClickEndStarMap;
        private static DelegateBridge __Hotfix_GotoStarMapForRepeatableUserGuide;

        [MethodImpl(0x8000)]
        public FactionCreditTraceDataUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMapForRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeatilInfoCloseClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeatilInfoOpenClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoStartMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecordButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRepeatableClickEndStarMap(bool isInRect)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnYesButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void QuestChecking(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendQuestCheckingSucessReq()
        {
        }

        [MethodImpl(0x8000)]
        public static bool StartFactionCreditTraceDataUITask(UIIntent returnToIntent, int questConfigId, int instanceId, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private int RangeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

