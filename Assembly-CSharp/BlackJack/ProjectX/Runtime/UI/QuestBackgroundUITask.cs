﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class QuestBackgroundUITask : UITaskBase
    {
        protected List<int> m_dontCareUnaccpetQuestList;
        protected Func<QuestWaitForAcceptInfo, bool> m_isQuestWaitForAutoAcceptFunc;
        private bool m_isQuestBackgroundUITaskEnable;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public static string m_questBackgroundUITaskDefault = "QuestBackgroundUITask_Default";
        public const string UIActionNameForQuestAcceptDialogSpaceStationUI = "QuestAcceptDialogForSpaceStationUI";
        public const string UIActionNameForQuestAcceptDialogInSpaceUI = "QuestAcceptDialogForInSpaceUI";
        public const string UIActionNameForQuestAcceptDialogBackgroundUI = "QuestAcceptDialogForBackgroundUI";
        public const string UIActionNameForSailReportUI = "SailReportUI";
        private const string RebirthUITaskName = "RebirthUITask";
        public const string TaskName = "QuestBackgroundUITask";
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        [CompilerGenerated]
        private static Action <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetFirstAutoAcceptQuestForQuestAcceptDialog;
        private static DelegateBridge __Hotfix_SetQuestBackgroundUITaskEnable;
        private static DelegateBridge __Hotfix_SetQuestBackgroundUITaskEnableImp;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickForQuestAcceptDialog;
        private static DelegateBridge __Hotfix_CheckQuestAcceptDialogTickable;
        private static DelegateBridge __Hotfix_QuestAcceptDialogHandleIter;
        private static DelegateBridge __Hotfix_CreateQuestAcceptDialogUIActionForSpaceStationUI;
        private static DelegateBridge __Hotfix_CreateQuestAcceptDialogUIActionForSolarSystemUI;
        private static DelegateBridge __Hotfix_CreateQuestAcceptDialogUIActionForSelf;
        private static DelegateBridge __Hotfix_RegisterEvent;
        private static DelegateBridge __Hotfix_UnregisterEvent;
        private static DelegateBridge __Hotfix_SetDontCareUnacceptQuest;
        private static DelegateBridge __Hotfix_RemoveQuestFromDontCareUnacceptQuest;
        private static DelegateBridge __Hotfix_IsDontCareUnacceptQuest;
        private static DelegateBridge __Hotfix_IsSpaceStationUISuitableToDealWithQuestAcceptDialog;
        private static DelegateBridge __Hotfix_IsInSpaceUISuitableToDealWithQuestAcceptDialog;
        private static DelegateBridge __Hotfix_IsBackgroundUISuitableToDealWithQuestAcceptDialog;
        private static DelegateBridge __Hotfix_IsQuestWaitForAutoAccept;
        private static DelegateBridge __Hotfix_IsQuestCanMultyAccept;
        private static DelegateBridge __Hotfix_OnQuestAcceptAck;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbQuest;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public QuestBackgroundUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckQuestAcceptDialogTickable()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateQuestAcceptDialogUIActionForSelf(QuestWaitForAcceptInfo waitForAcceptInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateQuestAcceptDialogUIActionForSolarSystemUI(QuestWaitForAcceptInfo waitAcceptQuestInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateQuestAcceptDialogUIActionForSpaceStationUI(QuestWaitForAcceptInfo waitAcceptQuestInfo)
        {
        }

        [MethodImpl(0x8000)]
        public QuestWaitForAcceptInfo GetFirstAutoAcceptQuestForQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private int IsBackgroundUISuitableToDealWithQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsDontCareUnacceptQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private int IsInSpaceUISuitableToDealWithQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsQuestCanMultyAccept(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsQuestWaitForAutoAccept(QuestWaitForAcceptInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        private int IsSpaceStationUISuitableToDealWithQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAcceptAck(int result, int instanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void QuestAcceptDialogHandleIter()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveQuestFromDontCareUnacceptQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDontCareUnacceptQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetQuestBackgroundUITaskEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void SetQuestBackgroundUITaskEnableImp(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void TickForQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockQuestClient LbQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static QuestBackgroundUITask Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateQuestAcceptDialogUIActionForSelf>c__AnonStorey2
        {
            internal UIManager.UIActionQueueItem item;
            internal UnAcceptedQuestUIInfo questUIInfo;
            internal QuestBackgroundUITask $this;

            internal bool <>m__0() => 
                (this.$this.State == Task.TaskState.Running);

            internal void <>m__1()
            {
                Debug.LogWarning("[弹窗]-BGWatchingUITask-Tick中产生的任务接受对话开始, Ticks: " + Timer.m_currTick);
                QuestUtilHelper.StartQuestAccpetDilaog(this.item, this.questUIInfo, null, null, null, delegate (bool res) {
                    if (!res)
                    {
                        this.$this.SetDontCareUnacceptQuest(this.questUIInfo.ConfigId);
                    }
                });
            }

            internal void <>m__2(bool res)
            {
                if (!res)
                {
                    this.$this.SetDontCareUnacceptQuest(this.questUIInfo.ConfigId);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateQuestAcceptDialogUIActionForSolarSystemUI>c__AnonStorey1
        {
            internal SolarSystemNoticeAndNPCChatUITask solarSystemNoticeAndNpcChatUITask;
            internal UIManager.UIActionQueueItem item;
            internal UnAcceptedQuestUIInfo questUIInfo;
            internal QuestBackgroundUITask $this;

            internal bool <>m__0() => 
                (this.solarSystemNoticeAndNpcChatUITask.State == Task.TaskState.Running);

            internal void <>m__1()
            {
                Debug.LogWarning("[弹窗]-太空-Tick中产生的任务接受对话开始, Ticks: " + Timer.m_currTick);
                QuestUtilHelper.StartQuestAccpetDilaog(this.item, this.questUIInfo, null, delegate (int rst, int qid) {
                    if (rst == 0)
                    {
                        UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_AcceptQuest, new object[0]);
                        this.solarSystemNoticeAndNpcChatUITask.ShowRelativeQuestMsg();
                    }
                }, null, delegate (bool res) {
                    if (!res)
                    {
                        this.$this.SetDontCareUnacceptQuest(this.questUIInfo.ConfigId);
                    }
                });
            }

            internal void <>m__2(int rst, int qid)
            {
                if (rst == 0)
                {
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_AcceptQuest, new object[0]);
                    this.solarSystemNoticeAndNpcChatUITask.ShowRelativeQuestMsg();
                }
            }

            internal void <>m__3(bool res)
            {
                if (!res)
                {
                    this.$this.SetDontCareUnacceptQuest(this.questUIInfo.ConfigId);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateQuestAcceptDialogUIActionForSpaceStationUI>c__AnonStorey0
        {
            internal SpaceStationUITask spaceStationUITask;
            internal UIManager.UIActionQueueItem item;
            internal UnAcceptedQuestUIInfo questUIInfo;
            internal QuestBackgroundUITask $this;

            internal bool <>m__0() => 
                (this.spaceStationUITask.State == Task.TaskState.Running);

            [MethodImpl(0x8000)]
            internal void <>m__1()
            {
            }

            internal void <>m__2()
            {
                this.spaceStationUITask.Pause();
            }

            internal void <>m__3(int rst, int qid)
            {
                this.spaceStationUITask.Resume(null, null);
            }

            internal void <>m__4()
            {
                this.spaceStationUITask.Resume(null, null);
            }

            [MethodImpl(0x8000)]
            internal void <>m__5(bool res)
            {
            }
        }
    }
}

