﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CharacterUITask : UITaskBase
    {
        protected string m_userId;
        protected LoginBGTask m_bgTask;
        protected bool m_isLoginBGTaskReadyToShow;
        protected bool m_isLoginBGTaskNeedLodeResource;
        protected CharacterUIController m_mainCtrl;
        protected Action<bool> m_onCreateCharacterEndAction;
        public static string UIMODE_CREATECHARACTER;
        public static string ParamKey_ActionOnCreateCharacterEnd;
        protected const int m_createCharacter_SwitchFactionSound = 1;
        protected const int m_createCharacter_SwitchFactionTxt = 2;
        protected const int m_createCharacter_ChangeFaction = 3;
        protected const int m_createCharacter_ChangeProfession = 4;
        protected const int m_createCharacter_ChangeAvatar = 6;
        protected const int m_createCharacter_RandName = 7;
        protected const int m_createCharacter_FirstFactionProfessionAvatar = 8;
        protected const int m_createCharacter_OpenConfirmWin = 9;
        protected GrandFaction m_faction;
        protected ProfessionType m_profession;
        protected GenderType m_gender;
        protected string m_characterName;
        protected int m_resId;
        protected int m_candidateResId;
        protected bool m_characterNameIsRandomName;
        protected bool m_changeName;
        protected CharacterUITaskState m_taskState;
        private bool m_isBoardingProgressFinished;
        protected Dictionary<int, List<int>> m_factionProf2ResIdDict;
        private readonly Dictionary<int, List<string>> m_faction2GenderFirstName;
        private readonly Dictionary<int, List<string>> m_faction2LastName;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartItImp;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AvatarList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AvatarHalfList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AvatarBigPic;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateShowCharacterView;
        private static DelegateBridge __Hotfix_UpdateCreateCharacterView;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateCharacterView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnEnterButtonClick;
        private static DelegateBridge __Hotfix_OnFaceChoose;
        private static DelegateBridge __Hotfix_OnFaceFilled;
        private static DelegateBridge __Hotfix_OnFactionIntroSoundSwitchButtonClick;
        private static DelegateBridge __Hotfix_OnFactionIntroSwitchButtonClick;
        private static DelegateBridge __Hotfix_OnFactionButtonClick;
        private static DelegateBridge __Hotfix_OnCloseFactionIntroButtonClick;
        private static DelegateBridge __Hotfix_OnProfesionButtonClick;
        private static DelegateBridge __Hotfix_OnRandNameButtonClick;
        private static DelegateBridge __Hotfix_OnEditCharacterNameEnd;
        private static DelegateBridge __Hotfix_OnOpenConfirmWin;
        private static DelegateBridge __Hotfix_OnCancelConfirmWin;
        private static DelegateBridge __Hotfix_StartLoginBGTask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInLoginBGTask;
        private static DelegateBridge __Hotfix_SetPipeLineMaskForCreateCharacterInit;
        private static DelegateBridge __Hotfix_UpdateDataCache_CreateCharacterMode;
        private static DelegateBridge __Hotfix_StartCreateCharacterReq;
        private static DelegateBridge __Hotfix_OnReturnToLoginConfirmed;
        private static DelegateBridge __Hotfix_ValidateInputCharacterName;
        private static DelegateBridge __Hotfix_GetFactionProf2ResIdDict;
        private static DelegateBridge __Hotfix_GetFactionProfKey;
        private static DelegateBridge __Hotfix_GetFactionKey;
        private static DelegateBridge __Hotfix_GetFactionGenderKey;
        private static DelegateBridge __Hotfix_InitResAndNameDict;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CharacterUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AvatarBigPic(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AvatarHalfList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AvatarList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private int GetFactionGenderKey(GrandFaction faction, GenderType gender)
        {
        }

        [MethodImpl(0x8000)]
        private int GetFactionKey(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static Dictionary<int, List<int>> GetFactionProf2ResIdDict()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFactionProfKey(GrandFaction faction, ProfessionType profession)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitlizeBeforeManagerStartItImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitResAndNameDict()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelConfirmWin(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseFactionIntroButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditCharacterNameEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFaceChoose(FaceListItemUIController faceCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFaceFilled()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionButtonClick(UIControllerBase controllerBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionIntroSoundSwitchButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionIntroSwitchButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadAllResCompletedInLoginBGTask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenConfirmWin(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProfesionButtonClick(UIControllerBase controllerBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRandNameButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReturnToLoginConfirmed(bool obj, bool switchAccount)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineMaskForCreateCharacterInit(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void StartCreateCharacterReq()
        {
        }

        [MethodImpl(0x8000)]
        protected bool StartLoginBGTask()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCharacterView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCreateCharacterView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_CreateCharacterMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShowCharacterView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected bool ValidateInputCharacterName(string inputVal)
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum CharacterUITaskState
        {
            None,
            Start,
            StartCreateCharacter,
            CharacterCreated,
            StartInitPlayerInfo,
            PlayerInfoInitEnd,
            StartWorldEnterReq
        }
    }
}

