﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildCurrencyLogInfoReqNetTask : NetWorkTransactionTask
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Result>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <StartIndex>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TotalPages>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private List<GuildCurrencyLogInfo> <CurrencyLogInfo>k__BackingField;
        private readonly int m_startIndex;
        private readonly ulong m_updateMask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildCurrencyLogInfoAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_StartIndex;
        private static DelegateBridge __Hotfix_set_StartIndex;
        private static DelegateBridge __Hotfix_get_TotalPages;
        private static DelegateBridge __Hotfix_set_TotalPages;
        private static DelegateBridge __Hotfix_get_CurrencyLogInfo;
        private static DelegateBridge __Hotfix_set_CurrencyLogInfo;

        [MethodImpl(0x8000)]
        public GuildCurrencyLogInfoReqNetTask(int startIndex, ulong updateMask)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCurrencyLogInfoAck(GuildCurrencyLogInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int StartIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int TotalPages
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public List<GuildCurrencyLogInfo> CurrencyLogInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

