﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationDonateHintUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LostItem> EventOnConfirmDonateClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCancelDonateClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnCommonIconClick;
        public CommonItemIconUIController m_itemIconCtrl;
        public LostItem m_lostItemInfo;
        public int m_itemIndex;
        private int m_guildCompensationDonateGuildGalaFactor;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./OwnTextGroup/OwnNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OwnNumberText;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RootState;
        [AutoBind("./AvailableBonusGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DonateGuildGalaText;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnCommonItemIconClick;
        private static DelegateBridge __Hotfix_OnConfirmDonateClick;
        private static DelegateBridge __Hotfix_OnCancelDonateClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmDonateClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmDonateClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelDonateClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelDonateClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;

        public event Action EventOnCancelDonateClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LostItem> EventOnConfirmDonateClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(GameObject itemIconObj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelDonateClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmDonateClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show, bool immediate, UIProcess.OnEnd onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(LostItem itemInfo, Dictionary<string, UnityEngine.Object> dict)
        {
        }
    }
}

