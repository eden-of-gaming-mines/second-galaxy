﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipCustomTemplateUITask : UITaskBase
    {
        private const string StateOpen = "Select";
        private const string StateClose = "UnSelect";
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private ShipEquipSlotType m_lastSelectedItemSlotType;
        private int m_lastSelectedItemIndex;
        private ShipCustomTemplateUIController m_mainCtrl;
        private ShipTemplateMenuTreeRootUIController m_selfMenuTreeCtrl;
        private ShipTemplateMenuTreeRootUIController m_guildMenuTreeCtrl;
        private ShipTemplateAllSlotGroupUIController m_allSlotGroupCtrl;
        private ShipTemplateInfoBuyLackItemPanelUIController m_buyLackItemPanelCtrl;
        private ShipCustomTemplatePopupWndUIController m_popWndCtrl;
        private List<ShipCustomTemplateInfo> m_customTemplateInfoList;
        private List<ShipCustomTemplateInfo> m_guildTemplateInfoList;
        private Dictionary<int, List<ShipCustomTemplateInfo>> m_preTemplateInfoDict;
        private List<ShipCustomTemplateInfo> m_currShipTypeTemplateInfoList;
        private List<ShipCustomTemplateInfo> m_currShipTypeGuildTemplateInfoList;
        private List<ShipCustomTemplateInfo> m_currShipTypePreTemplateInfoList;
        private ShipCustomTemplateInfo m_temporaryShipCustomTemplateInfo;
        private List<ItemInfo> m_lackBindMoneyBuyableItemList;
        private double m_buyLackItemNeedMoney;
        private ulong m_shipInstanceId;
        private LBFakeStaticPlayerShipClient m_fakeStaticShip;
        private ShipCustomTemplateInfo m_currSelectMenuItemShipCustomTemplateInfo;
        private bool m_isShowAllTemplateInfoGroup;
        private bool m_isDuringUpdateView;
        private bool m_isSelectTemporary;
        private bool m_isShowTemporary;
        private bool m_isShowUnloadItemWarningPanel;
        private bool m_isShowBuyLackItemPanel;
        private bool m_isShowRenamePanel;
        private bool m_isChangeNameWhenSaveTemplate;
        private bool m_isChangedNameBeforeSaveTemplate;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKeyTemporaryShipCustomTemplateInfo = "TemporaryShipCustomTemplateInfo";
        public const string ParamKeyIsShowAllTemplateInfoGroups = "IsShowAllTemplateInfoGroups";
        public const string ParamKeyIsRefreshAll = "IsRefreshAll";
        public const string ParamKeyIsUpdateSelectedShip = "IsUpdateSelectedShip";
        public const string ParamKeyShipInstanceId = "ShipInstanceId";
        public const string ParamKeyFakeShip = "FakeShip";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        public const string UIModeNormal = "Normal";
        public const string UIModeBuyLackItem = "BuyLackItem";
        public const string UIModeUnLoadItemMsgBox = "UnloadItemMsgBox";
        public const string UIModeRename = "Rename";
        private const string Normal = "Normal";
        private const string Choose = "Choose";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ShipCustomTemplateUITask";
        [CompilerGenerated]
        private static Func<string, string, string> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartShipCustomTemplateUITask;
        private static DelegateBridge __Hotfix_StartShipCustomTemplateUITaskFromChat;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllResourceFromShipCustomTemplateList_1;
        private static DelegateBridge __Hotfix_CollectAllResourceFromShipCustomTemplateList_0;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_OnGuildTemplateUpdated;
        private static DelegateBridge __Hotfix_ShowSelfMenu;
        private static DelegateBridge __Hotfix_ShowGuildMenu;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClickImp;
        private static DelegateBridge __Hotfix_OnShowAllTemplateGroupToggleValueChange;
        private static DelegateBridge __Hotfix_OnSaveButtonClick;
        private static DelegateBridge __Hotfix_OnSaveGuildButtonClick;
        private static DelegateBridge __Hotfix_OnPurchaseButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteButtonClick;
        private static DelegateBridge __Hotfix_OnApplyButtonClick;
        private static DelegateBridge __Hotfix_OnApplyButtonClickImp;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnRenameButtonClick;
        private static DelegateBridge __Hotfix_OnShipTemplateInfoMenuItemClick;
        private static DelegateBridge __Hotfix_OnMenuItemShipLevelExpand;
        private static DelegateBridge __Hotfix_OnTemporaryShipCustomInfoItemClick;
        private static DelegateBridge __Hotfix_OnRenamePanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnRenamePanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnRenamePanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBuyLackItemBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnBuyLackItemCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBuyLackItemConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadItemWarningBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadItemWarningCancelButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadItemWarningConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemBuyClick;
        private static DelegateBridge __Hotfix_OnShipIconClick;
        private static DelegateBridge __Hotfix_OnBuyItemClick;
        private static DelegateBridge __Hotfix_CheckIsTemplateInfoAllSlotEmpty;
        private static DelegateBridge __Hotfix_CheckIsTemplateInfoInPreTemplateInfoDict;
        private static DelegateBridge __Hotfix_CheckIsTemplateInfoInTemplateInfoList;
        private static DelegateBridge __Hotfix_CheckIsCostEnoughWhenApply;
        private static DelegateBridge __Hotfix_GetConfHighSlotInfoBySlotIndex;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemIdBySlotIndex;
        private static DelegateBridge __Hotfix_GetLowSlotItemIdBySlotIndex;
        private static DelegateBridge __Hotfix_IsTheSameTypeShipTemplateInfo;
        private static DelegateBridge __Hotfix_GetCurrShipTypeCustomShipTemplateInfoList;
        private static DelegateBridge __Hotfix_GetDefaultShipTemplateInfoByCurrInfo;
        private static DelegateBridge __Hotfix_GetDefaultShipTemplateInfoByCurrInfoFromList;
        private static DelegateBridge __Hotfix_SendApplyShipCustomTemplate;
        private static DelegateBridge __Hotfix_SendSaveTemporaryShipCustomTemplate;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_UpdateShipEquipedItem;
        private static DelegateBridge __Hotfix_UpdateNeedItemForTemplete;
        private static DelegateBridge __Hotfix_AddSlotItemToList;
        private static DelegateBridge __Hotfix_AddItemToList;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitPipeLineStateFromUIIntent;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetCurrShipId;
        private static DelegateBridge __Hotfix_GetLayerCamrea;
        private static DelegateBridge __Hotfix_GetBackTransform;
        private static DelegateBridge __Hotfix_ClickConfirmButton;
        private static DelegateBridge __Hotfix_ClickCloseButton;

        [MethodImpl(0x8000)]
        public ShipCustomTemplateUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddItemToList(StoreItemType type, int itemid, List<FakeLBStoreItem> itemList, int addcount = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddSlotItemToList(LBStaticWeaponEquipSlotGroup slotGroup, List<FakeLBStoreItem> itemList)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsCostEnoughWhenApply(ShipCustomTemplateInfo info, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsTemplateInfoAllSlotEmpty(ShipCustomTemplateInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsTemplateInfoInPreTemplateInfoDict(ShipCustomTemplateInfo info, out ShipCustomTemplateInfo preInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsTemplateInfoInTemplateInfoList(List<ShipCustomTemplateInfo> infoList, ShipCustomTemplateInfo info, out ShipCustomTemplateInfo resultInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickCloseButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickConfirmButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllResourceFromShipCustomTemplateList(ShipCustomTemplateInfo info, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllResourceFromShipCustomTemplateList(List<ShipCustomTemplateInfo> infoList, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetBackTransform()
        {
        }

        [MethodImpl(0x8000)]
        protected ConfHighSlotInfo GetConfHighSlotInfoBySlotIndex(ShipCustomTemplateInfo info, int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrShipId()
        {
        }

        [MethodImpl(0x8000)]
        protected List<ShipCustomTemplateInfo> GetCurrShipTypeCustomShipTemplateInfoList(ShipCustomTemplateInfo currInfo, List<ShipCustomTemplateInfo> totalInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipCustomTemplateInfo GetDefaultShipTemplateInfoByCurrInfo(ShipCustomTemplateInfo currInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipCustomTemplateInfo GetDefaultShipTemplateInfoByCurrInfoFromList(ShipCustomTemplateInfo currInfo, List<ShipCustomTemplateInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamrea()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetLowSlotItemIdBySlotIndex(ShipCustomTemplateInfo info, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetMiddleSlotItemIdBySlotIndex(ShipCustomTemplateInfo info, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitPipeLineStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsTheSameTypeShipTemplateInfo(ShipCustomTemplateInfo srcInfo, ShipCustomTemplateInfo destInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnApplyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnApplyButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyLackItemBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyLackItemCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyLackItemConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDeleteButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildTemplateUpdated(bool hasUpdate)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemBuyClick(UIControllerBase ctrl, NpcShopItemType shopType, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl, ShipEquipSlotType slotType, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemShipLevelExpand(CommonMenuItemUIController menuItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPurchaseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRenameButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRenamePanelBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRenamePanelCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRenamePanelConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSaveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSaveGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipTemplateInfoMenuItemClick(ShipCustomTemplateInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShowAllTemplateGroupToggleValueChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTemporaryShipCustomInfoItemClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnloadItemWarningBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnloadItemWarningCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnloadItemWarningConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendApplyShipCustomTemplate(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendSaveTemporaryShipCustomTemplate()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowGuildMenu(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowSelfMenu(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipCustomTemplateUITask StartShipCustomTemplateUITask(ILBStaticPlayerShip ship, UIIntent returnToUIIntent = null, Action<bool> onEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipCustomTemplateUITask StartShipCustomTemplateUITaskFromChat(ShipCustomTemplateInfo info, UIIntent returnToUIIntent = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateNeedItemForTemplete(ShipCustomTemplateInfo templeteInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShipEquipedItem()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AddItemToList>c__AnonStorey4
        {
            internal StoreItemType type;
            internal int itemid;

            internal bool <>m__0(FakeLBStoreItem item) => 
                ((item.GetItemType() == this.type) && (ClientStoreItemBaseHelper.GetItemConfigId(item.GetItemType(), item.GetConfigInfo<object>()) == this.itemid));
        }

        [CompilerGenerated]
        private sealed class <OnApplyButtonClickImp>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal ShipCustomTemplateUITask $this;

            internal void <>m__0()
            {
                this.$this.m_isShowBuyLackItemPanel = false;
                this.$this.m_isShowUnloadItemWarningPanel = false;
                this.$this.SendApplyShipCustomTemplate(this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnBackButtonClickImp>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal ShipCustomTemplateUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (currIntent == null)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (currIntent.PrevTaskIntent.TargetTaskName == "ShipHangarUITask")
                    {
                        ((UIIntentCustom) currIntent.PrevTaskIntent).SetParam(ShipHangarUITask.ParamKey_ShipListRefreshMode, ShipHangarUITask.ShipListRefreshMode_RefreshSingle);
                        ((UIIntentCustom) currIntent.PrevTaskIntent).SetParam(ShipHangarUITask.ParamKey_IsShipDataChanged, true);
                    }
                    UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, this.onEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnItemBuyClick>c__AnonStorey2
        {
            internal NpcShopItemType shopType;
            internal ShipCustomTemplateUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                ((UIIntentCustom) this.$this.CurrentIntent).SetParam("IsUpdateSelectedShip", true);
                this.$this.Pause();
                NpcShopItemType shopType = this.shopType;
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                AuctionUITask.StartAuctionUITaskWithPrepare(this.$this.m_currIntent, "Mode_Buy", shopType, -1, delegate (bool res) {
                    if (!res)
                    {
                        this.$this.Resume(null, null);
                    }
                }, null, backgroundManager);
            }

            internal void <>m__1(bool res)
            {
                if (!res)
                {
                    this.$this.Resume(null, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendApplyShipCustomTemplate>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal ShipCustomTemplateUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                ShipCustomTemplateApplyReqNetTask task2 = task as ShipCustomTemplateApplyReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_ShipCustomTemplateInfoApplySuccess, new object[0]), false);
                        this.$this.m_isShowBuyLackItemPanel = false;
                        this.$this.m_isShowUnloadItemWarningPanel = false;
                        this.$this.m_temporaryShipCustomTemplateInfo = this.$this.m_currSelectMenuItemShipCustomTemplateInfo;
                        this.$this.EnablePipelineStateMask(ShipCustomTemplateUITask.PipeLineStateMaskType.IsUpdateSelect);
                        this.$this.EnablePipelineStateMask(ShipCustomTemplateUITask.PipeLineStateMaskType.IsNeedUpdateDataCache);
                        this.$this.EnablePipelineStateMask(ShipCustomTemplateUITask.PipeLineStateMaskType.IsUpdateMenuTree);
                        this.$this.EnablePipelineStateMask(ShipCustomTemplateUITask.PipeLineStateMaskType.IsExpandMenuTreeToSelect);
                        this.$this.EnablePipelineStateMask(ShipCustomTemplateUITask.PipeLineStateMaskType.IsUpdateEquipItemInShip);
                        Action<bool> onEnd = this.onEnd;
                        this.$this.StartUpdatePipeLine(null, false, false, true, onEnd);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsRefreshAll,
            IsNeedUpdateDataCache,
            IsNeedLoadDynamicRes,
            IsOnDeleteShipTemplate,
            IsUpdateSelect,
            IsUpdateMenuTree,
            IsExpandMenuTreeToSelect,
            IsExpandMenuTreeToTemporaryShipLevel,
            IsUpdateEquipItemInShip
        }

        public class ShipHangarUITaskPipeLineCtx : UITaskPipeLineCtx
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public ShipHangarUITaskPipeLineCtx()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

