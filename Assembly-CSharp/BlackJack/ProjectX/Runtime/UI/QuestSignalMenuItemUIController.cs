﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestSignalMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/Info/QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questNameText;
        [AutoBind("./TargetInfo/Info/TextGroup/NpcNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_npcNameText;
        [AutoBind("./TargetInfo/Info/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_signalNpcIcon;
        [AutoBind("./TargetInfo/Info/ShipTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_shipTypeIcon;
        [AutoBind("./TargetInfo/Info/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_levelText;
        [AutoBind("./TargetInfo/Info/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_levelTextStateCtrl;
        [AutoBind("./TargetInfo/Info/DifficultyImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_emergencyImage;
        private ConfigDataQuestInfo m_questConfInfo;
        private bool m_isEmergencyQuestSignal;
        private static DelegateBridge __Hotfix_SetManualSignalnfo;
        private static DelegateBridge __Hotfix_IsEmergencyQuestSignal;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetQuestConfInfo;

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo GetQuestConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEmergencyQuestSignal()
        {
        }

        [MethodImpl(0x8000)]
        public void SetManualSignalnfo(LBSignalManualQuest manualSigInfo, Dictionary<string, UnityEngine.Object> resDict, bool isEmergencySignal)
        {
        }
    }
}

