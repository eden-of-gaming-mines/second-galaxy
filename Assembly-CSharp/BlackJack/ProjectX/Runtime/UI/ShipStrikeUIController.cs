﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipStrikeUIController : UIControllerBase
    {
        protected List<ILBStaticPlayerShip> m_shipHangarShipInfoList;
        protected List<ShipStrikeUITask.CachedHireCaptain> m_hireCaptainList;
        protected List<ShipType> m_allowInShipTypeList;
        protected ShipStrikeUITask.CachedHireCaptain m_currSelectShipCaptainInfo;
        protected Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private bool m_canFastLoadTradeItem;
        private const string m_shipListUIItemPoolName = "ShipInfoItem";
        private const string m_hireCaptainListUIItemPoolName = "StrikeHireCaptainItem";
        private const string m_captainShipSelectListItemUIItemPoolName = "HangarShipListItem";
        private int m_selectedShipItemIndex;
        protected Dictionary<ShipType, CommonUIStateController> m_allowInShipTypeDict;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ShipInfoItemUIController, int> EventOnShipInfoItemFill;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnShipInfoUIItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnShipAssembleButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnAutoReloadAmmoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnTradeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDisposeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllowInShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<StrikeHireCaptainItemUIController> EventOnCaptainItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<StrikeHireCaptainItemUIController> EventOnHireCaptainItemFill;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCaptainSelectShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCaptainUpButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCaptainDownButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ShipStrikeUITask.CachedHireCaptain> EventOnCaptainIconButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCaptainSelectShipItemClick;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        public AllowInShipGroupUIController m_allowInShipGroupUIController;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./StrikeShipsListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StrikeShipListPanelRoot;
        [AutoBind("./StrikeShipsListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ShipListScrollRect;
        [AutoBind("./StrikeShipsListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ShipListItemPool;
        [AutoBind("./StrikeShipsListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipListViewContent;
        [AutoBind("./NoShipImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NoShipImage;
        [AutoBind("./HireCaptainListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HireCaptainListPanelRoot;
        [AutoBind("./Buttons/StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipStrikeButton;
        [AutoBind("./Buttons/StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipStrikeButtonUIStateCtrl;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelButton;
        [AutoBind("./Buttons/SwitchShipPanelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SwitchShipPanelButton;
        [AutoBind("./Buttons/SwitchShipPanelButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SwitchShipPanelButtonStateCtrl;
        [AutoBind("./Buttons/SwitchHireCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SwitchHireCaptainButton;
        [AutoBind("./Buttons/SwitchHireCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SwitchHireCaptainButtonStateCtrl;
        [AutoBind("./AllowIn/AllowInButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllowInShipButton;
        [AutoBind("./AllowIn/AllowInShipGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AllowInShipGroupDummy;
        [AutoBind("./AllowIn/FlagImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_countyIconImage;
        [AutoBind("./AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeFrigate;
        [AutoBind("./AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeDestroyer;
        [AutoBind("./AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeCruiser;
        [AutoBind("./AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleCruiser;
        [AutoBind("./AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleShip;
        [AutoBind("./AllowIn/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeIndustryShip;
        [AutoBind("./HireCaptainListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect HireCaptainListScrollRect;
        [AutoBind("./HireCaptainListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool HireCaptainItemPool;
        [AutoBind("./HireCaptainShipSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HireCaptainShipSelectPanelStateCtrl;
        [AutoBind("./HireCaptainShipSelectPanel/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect HireCaptainShipSelectPanelScrollRect;
        [AutoBind("./HireCaptainShipSelectPanel/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool HireCaptainShipSelectItemPool;
        [AutoBind("./HireCaptainShipSelectPanel/BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button HireCaptainShipSelectPanelBgButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitShipListItems;
        private static DelegateBridge __Hotfix_InitHireCaptainsListItems;
        private static DelegateBridge __Hotfix_InitHireCaptainShipSelectListItems;
        private static DelegateBridge __Hotfix_UpdateStrikeShipInfoList;
        private static DelegateBridge __Hotfix_UpdateStrikeButtonState;
        private static DelegateBridge __Hotfix_UpdateAllowGrandFaction;
        private static DelegateBridge __Hotfix_UpdateHireCaptainInfoList;
        private static DelegateBridge __Hotfix_UpdateAllowInShipTypeList;
        private static DelegateBridge __Hotfix_UpdateSingleShipHangar;
        private static DelegateBridge __Hotfix_GetShipInfoItemByIndex;
        private static DelegateBridge __Hotfix_UpdateShipListSelectState;
        private static DelegateBridge __Hotfix_UpdateCaptainSelectShipPanel;
        private static DelegateBridge __Hotfix_SetSwitchShipPanelButtonState;
        private static DelegateBridge __Hotfix_SetSwitchHireCaptainButtonState;
        private static DelegateBridge __Hotfix_CreateCaptainSelectShipPanelShowProcess;
        private static DelegateBridge __Hotfix_CreateShipStrikePanelShowProcess;
        private static DelegateBridge __Hotfix_SetShipSelectedIndex;
        private static DelegateBridge __Hotfix_OnShipInfoUIItemClick;
        private static DelegateBridge __Hotfix_OnShipAssembleClick;
        private static DelegateBridge __Hotfix_OnAutoReloadAmmoButtonClick;
        private static DelegateBridge __Hotfix_OnTradeButtonClick;
        private static DelegateBridge __Hotfix_OnDisposeButtonClick;
        private static DelegateBridge __Hotfix_OnAllowInShipGroupButtonClick;
        private static DelegateBridge __Hotfix_OnShipUIItemFill;
        private static DelegateBridge __Hotfix_OnHireCaptainItemClick;
        private static DelegateBridge __Hotfix_OnHireCaptainItemFill;
        private static DelegateBridge __Hotfix_OnCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainSelectShipItemFill;
        private static DelegateBridge __Hotfix_OnCaptainSelectShipItemClick;
        private static DelegateBridge __Hotfix_OnCaptainUpButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainDownButtonClick;
        private static DelegateBridge __Hotfix_SwitchShipPanelAndHireCaptainPanel;
        private static DelegateBridge __Hotfix_GetFirstStrikeIndex;
        private static DelegateBridge __Hotfix_add_EventOnShipInfoItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnShipInfoItemFill;
        private static DelegateBridge __Hotfix_add_EventOnShipInfoUIItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipInfoUIItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipAssembleButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipAssembleButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAutoReloadAmmoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAutoReloadAmmoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTradeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTradeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDisposeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDisposeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainItemClick;
        private static DelegateBridge __Hotfix_add_EventOnHireCaptainItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnHireCaptainItemFill;
        private static DelegateBridge __Hotfix_add_EventOnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainUpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainUpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainDownButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainDownButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainSelectShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainSelectShipItemClick;

        public event Action EventOnAllowInShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnAutoReloadAmmoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainDownButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ShipStrikeUITask.CachedHireCaptain> EventOnCaptainIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<StrikeHireCaptainItemUIController> EventOnCaptainItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainSelectShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainSelectShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainUpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDisposeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<StrikeHireCaptainItemUIController> EventOnHireCaptainItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnShipAssembleButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ShipInfoItemUIController, int> EventOnShipInfoItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnShipInfoUIItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnTradeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateCaptainSelectShipPanelShowProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateShipStrikePanelShowProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetFirstStrikeIndex()
        {
        }

        [MethodImpl(0x8000)]
        public ShipInfoItemUIController GetShipInfoItemByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitHireCaptainShipSelectListItems()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitHireCaptainsListItems()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitShipListItems()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAllowInShipGroupButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAutoReloadAmmoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainDownButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainIconButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainSelectShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainSelectShipItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainSelectShipItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainUpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDisposeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHireCaptainItemClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHireCaptainItemFill(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipAssembleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipInfoUIItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipUIItemFill(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTradeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipSelectedIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSwitchHireCaptainButtonState(bool isDisable, bool isFlicker, bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSwitchShipPanelButtonState(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        protected void SwitchShipPanelAndHireCaptainPanel(bool showShipPanel)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllowGrandFaction(List<GrandFaction> m_validGrandFaction, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllowInShipTypeList(List<ShipType> shipTypeList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainSelectShipPanel(ShipStrikeUITask.CachedHireCaptain captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateHireCaptainInfoList(List<ShipStrikeUITask.CachedHireCaptain> hiredCaptains, List<ShipType> allowInShipTypes, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int selectedShipIdx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipListSelectState(int selIdx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleShipHangar(int updateSlotIndex, ILBStaticPlayerShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStrikeButtonState(bool isGray)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStrikeShipInfoList(List<ILBStaticPlayerShip> shipHangarList, List<ShipType> allowInShipTypes, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int selectedShipIdx, int startIndex = 0, bool canFastLoadTradeItem = false)
        {
        }
    }
}

