﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class FactionCreditItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <FactionID>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> OnEventItemClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PanelUIButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtr;
        [AutoBind("./BarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BarGroup;
        [AutoBind("./NationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationGroupUIStateCtr;
        [AutoBind("./NationGroup/RSNationIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NationIconImage;
        [AutoBind("./BarGroup/BarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BarProgressImage;
        [AutoBind("./BarGroup/BarNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BarNumberText;
        [AutoBind("./BarGroup/BarNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BarNameText;
        [AutoBind("./BarGroup/HonorNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HonorNumberText;
        [AutoBind("./BarGroup/HonorText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HonorText;
        [AutoBind("./CornerImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CornerImage;
        [AutoBind("./CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestCountText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateViewByID;
        private static DelegateBridge __Hotfix_get_FactionID;
        private static DelegateBridge __Hotfix_set_FactionID;
        private static DelegateBridge __Hotfix_add_OnEventItemClick;
        private static DelegateBridge __Hotfix_remove_OnEventItemClick;

        public event Action<int> OnEventItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewByID(int factionId, ConfigDataFactionInfo configInfo, float val, int maxVal, long money, string lvNameStrKey, Dictionary<string, UnityEngine.Object> resList)
        {
        }

        public int FactionID
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

