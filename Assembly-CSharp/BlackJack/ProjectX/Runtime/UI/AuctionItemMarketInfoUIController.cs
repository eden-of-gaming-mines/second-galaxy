﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionItemMarketInfoUIController : UIControllerBase
    {
        private List<Text> m_priceList;
        private List<Text> m_countList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_mainStateCtrl;
        [AutoBind("./BazaarGroup", AutoBindAttribute.InitState.NotInit, false)]
        private Transform m_BazaarGroupParent;
        [AutoBind("./SumGroup/SumNumberText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_TotalNumberOfProductsText;
        private static DelegateBridge __Hotfix_SetSelectEmptyState;
        private static DelegateBridge __Hotfix_SetProductInformationList;
        private static DelegateBridge __Hotfix_SetTotalNumberOfProducts;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetProductInformationList(List<ProAuctionOrderBriefInfo> proAuctionOrderBriefInfoList, bool isEmpty, int totalCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectEmptyState(bool isEmpty)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTotalNumberOfProducts(int count)
        {
        }
    }
}

