﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class FunctionOpenAnimationEndReqNetTask : NetWorkTransactionTask
    {
        private SystemFuncType m_funcType;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnFunctionOpenAnimationEndAck;

        [MethodImpl(0x8000)]
        public FunctionOpenAnimationEndReqNetTask(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFunctionOpenAnimationEndAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

