﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class DragEventHandler : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector2> EventOnBeginDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector2> EventOnDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEndDrag;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_OnDrag;
        private static DelegateBridge __Hotfix_add_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_remove_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_add_EventOnDrag;
        private static DelegateBridge __Hotfix_remove_EventOnDrag;
        private static DelegateBridge __Hotfix_add_EventOnEndDrag;
        private static DelegateBridge __Hotfix_remove_EventOnEndDrag;

        public event Action<Vector2> EventOnBeginDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Vector2> EventOnDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEndDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEndDrag(PointerEventData eventData)
        {
        }
    }
}

