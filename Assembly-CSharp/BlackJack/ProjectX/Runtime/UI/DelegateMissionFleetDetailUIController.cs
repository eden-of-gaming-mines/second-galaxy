﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DelegateMissionFleetDetailUIController : UIControllerBase
    {
        private DelegateMissionInfo m_delegateMission;
        private const string AssetName_CaptainItemPrefab = "CaptainItemPrefab";
        private const string AssetName_CommonItemIconPrefab = "CommonItemIconPrefab";
        private const string AssetName_ItemSimpleInfoUIPrefab = "ItemSimpleInfoUIPrefab";
        private const string AssetName_InvadeInfoUIPrefab = "DelegateMissionInvadeInfoPrefab";
        private const string MissionTimeState_Finish = "Finish";
        private const string MissionTimeState_Remaining = "Remaining";
        private const string MissionResultState_BeInvade = "BeInvade";
        private const string MissionResultState_Recall = "Recall";
        private const string MissionResultState_Complete = "Complete";
        private const string MissionResultState_Failed = "Failed";
        private const string ButtonGroupState_Recall = "Recall";
        private const string ButtonGroupState_Rescue = "Rescue";
        private const string ButtonGroupState_Confirm = "Confirm";
        private const string ButtonGroupState_ConfirmAndWatchReward = "ConfirmAndWatchReward";
        private const string ButtonGroupState_ConfirmAndWatchJournal = "ConfirmAndWatchJournal";
        private const string ButtonGroupState_WatchRewardAndClose = "WatchRewardAndClose";
        private const string ButtonGroupState_WatchLogAndClose = "WatchLogAndClose";
        private const string ChangeButton_Ship = "Ship";
        private const string ChangeButton_Captatin = "Captain";
        private bool m_isHistory;
        private List<CommonCaptainIconUIController> m_captainIconList;
        private List<CommonItemIconUIController> m_shipIconList;
        private List<CommonUIStateController> m_captainLevelUpEffectList;
        private List<CommonItemIconUIController> m_fleetRewardIconList;
        private List<CommonItemIconUIController> m_missionRewardIconList;
        private List<GameObject> m_fleetRewardIconEffectList;
        private List<GameObject> m_missionRewardIconEffectList;
        private List<DelegateMissionInvadeInfoUIController> m_invadeInfoUIList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PlayerSimpleInfo> EventOnInvadeInfoPlayerNameClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnMissionRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnChangeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWatchRewardButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWatchJornalButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx BackGroundButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./CaptainOrShipGroup/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChangeButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/RecallButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecallButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SubCloseButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/RescueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RescueButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/CallForRescueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CallForRescueButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/WatchRewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WatchRewardButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup/WatchJournalButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WatchJournalButton;
        [AutoBind("./MissionDetailPanel/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupStateCtrl;
        [AutoBind("./CaptainOrShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainOrShipGroupStateCtrl;
        [AutoBind("./CaptainOrShipGroup/CaptainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainGroup;
        [AutoBind("./CaptainOrShipGroup/ShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipGroup;
        [AutoBind("./MissonInfoPanel/MissionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionNameText;
        [AutoBind("./MissonInfoPanel/MissionDetailGroup/MissionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionText;
        [AutoBind("./MissonInfoPanel/FinishOrRemainTimeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FinishTimeStateCtrl;
        [AutoBind("./MissonInfoPanel/FinishOrRemainTimeGroup/FinishTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FinishTimeText;
        [AutoBind("./MissonInfoPanel/FinishOrRemainTimeGroup/RemainingTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RemainingTimeText;
        [AutoBind("./MissonInfoPanel/FinishOrRemainTimeGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image TimeProgressBar;
        [AutoBind("./MissonInfoPanel/ResultImageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MissionResultStateCtrl;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionJournalRoot;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionStartMsg", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionStartMsg;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionStartMsg/StartMsgText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StartMsgText;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionStartMsg/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionStartTime;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionStartMsg/DateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionStartDate;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionEndMsg", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionEndMsg;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionEndMsg/EndMsgText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EndMsgText;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionEndMsg/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionEndTime;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/JournalGroup/MissionEndMsg/DateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionEndDate;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionRewardRoot;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/Loss", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionLoss;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/Loss/LosssValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MissionLossValueText;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/FleetReward/FleetRewardGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionFleetRewardGroup;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/FleetReward/FleetRewardGroup/LossRewardText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FleetRewardLoss;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/FleetReward/FleetRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FleetRewardGroupCtrl;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/MissionReward/MissionRewardGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionRewardGroup;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/MissionReward/MissionRewardGroup/LossRewardText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MissionRewardLoss;
        [AutoBind("./MissionDetailPanel/DetailGroupScrollView/Viewport/DetailGroup/RewardRoot/MissionReward/MissionRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MissionRewardGroupCtrl;
        [AutoBind("./RecallPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RecallPanel;
        [AutoBind("./RecallPanel/DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecallCancelButton;
        [AutoBind("./RecallPanel/DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecallConfirmButton;
        [AutoBind("./ItemSimpleInfoPanelLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoPanelLeftDummy;
        [AutoBind("./ItemSimpleInfoPanelRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoPanelRightDummy;
        [AutoBind("./PlayerSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PlayerSimpleInfoPanelDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreatCaptainItem;
        private static DelegateBridge __Hotfix_CreateShipItem;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetDelegateMissionInfo;
        private static DelegateBridge __Hotfix_ShowRecallFleetPanel;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelPos;
        private static DelegateBridge __Hotfix_GetRewardItemCount;
        private static DelegateBridge __Hotfix_CreatMainWindowUIProcess;
        private static DelegateBridge __Hotfix_CreateRewardItem;
        private static DelegateBridge __Hotfix_ShowCaptainOrShipList;
        private static DelegateBridge __Hotfix_ShowMissionBasicInfo;
        private static DelegateBridge __Hotfix_ShowWorkingMissionInfo;
        private static DelegateBridge __Hotfix_ShowMissionRewardInfo;
        private static DelegateBridge __Hotfix_ShowMissionJournalInfo;
        private static DelegateBridge __Hotfix_ShowRewardOrJournal;
        private static DelegateBridge __Hotfix_IsCaptainDestroyedInMission;
        private static DelegateBridge __Hotfix_OnInvadeInfoPlayerNameClick;
        private static DelegateBridge __Hotfix_OnChangeButtonClick;
        private static DelegateBridge __Hotfix_OnWatchJornalButtonClick;
        private static DelegateBridge __Hotfix_OnWatchRewardButtonClick;
        private static DelegateBridge __Hotfix_OnMissionRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnInvadeInfoPlayerNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnInvadeInfoPlayerNameClick;
        private static DelegateBridge __Hotfix_add_EventOnMissionRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMissionRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnChangeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChangeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWatchRewardButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWatchRewardButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWatchJornalButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWatchJornalButtonClick;

        public event Action EventOnChangeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PlayerSimpleInfo> EventOnInvadeInfoPlayerNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnMissionRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWatchJornalButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWatchRewardButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private CommonCaptainIconUIController CreatCaptainItem(GameObject itemPrefab, Transform rootTransform)
        {
        }

        [MethodImpl(0x8000)]
        private CommonItemIconUIController CreateRewardItem(Transform rootTrans, int idx, Action<UIControllerBase> onClickAction)
        {
        }

        [MethodImpl(0x8000)]
        private CommonItemIconUIController CreateShipItem(GameObject itemPrefab, Transform rootTransform)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatMainWindowUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelPos(float process)
        {
        }

        [MethodImpl(0x8000)]
        public int GetRewardItemCount()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCaptainDestroyedInMission(DelegateMissionInfo mission, NpcCaptainInfo captain)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInvadeInfoPlayerNameClick(PlayerSimpleInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMissionRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWatchJornalButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWatchRewardButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDelegateMissionInfo(DelegateMissionInfo delMission, bool isHistory, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainOrShipList(bool showCaptainOrShip)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMissionBasicInfo(DelegateMissionInfo delMission)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMissionJournalInfo(DelegateMissionInfo delMission)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMissionRewardInfo(DelegateMissionInfo delMission, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRecallFleetPanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRewardOrJournal(bool showReward)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowWorkingMissionInfo(DelegateMissionInfo delMission)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }
    }
}

