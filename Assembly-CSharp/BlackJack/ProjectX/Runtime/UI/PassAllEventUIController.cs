﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.EventSystems;

    public class PassAllEventUIController : UIBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnPointClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnPointDown;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnPointUp;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PointerEventData, Action<PointerEventData>> EventOnBeginDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnEndDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnPointEventRaise;
        private static DelegateBridge __Hotfix_OnPointerClick;
        private static DelegateBridge __Hotfix_PassPointerClick;
        private static DelegateBridge __Hotfix_OnPointerDown;
        private static DelegateBridge __Hotfix_PassPointerDown;
        private static DelegateBridge __Hotfix_OnPointerUp;
        private static DelegateBridge __Hotfix_PassPointerUp;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_PassBeginDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_PassEndDrag;
        private static DelegateBridge __Hotfix_OnDrag;
        private static DelegateBridge __Hotfix_PassOnDrag;
        private static DelegateBridge __Hotfix_PassEvent;
        private static DelegateBridge __Hotfix_add_EventOnPointClick;
        private static DelegateBridge __Hotfix_remove_EventOnPointClick;
        private static DelegateBridge __Hotfix_add_EventOnPointDown;
        private static DelegateBridge __Hotfix_remove_EventOnPointDown;
        private static DelegateBridge __Hotfix_add_EventOnPointUp;
        private static DelegateBridge __Hotfix_remove_EventOnPointUp;
        private static DelegateBridge __Hotfix_add_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_remove_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_add_EventOnEndDrag;
        private static DelegateBridge __Hotfix_remove_EventOnEndDrag;
        private static DelegateBridge __Hotfix_add_EventOnDrag;
        private static DelegateBridge __Hotfix_remove_EventOnDrag;
        private static DelegateBridge __Hotfix_add_EventOnPointEventRaise;
        private static DelegateBridge __Hotfix_remove_EventOnPointEventRaise;

        public event Action<PointerEventData, Action<PointerEventData>> EventOnBeginDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnEndDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnPointClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnPointDown
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnPointEventRaise
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnPointUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerUp(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void PassBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void PassEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        protected void PassEvent<T>(PointerEventData data, ExecuteEvents.EventFunction<T> function) where T: IEventSystemHandler
        {
        }

        [MethodImpl(0x8000)]
        public void PassOnDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void PassPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void PassPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void PassPointerUp(PointerEventData eventData)
        {
        }
    }
}

