﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    public class StarMapForSolarSystemCameraFocusDesc : MonoBehaviour
    {
        [Header("摄像机视线水平偏移坐标值")]
        public float m_cameraOffsetX;
        [Header("默认的，当选中对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVerticalDefault;
        [Header("默认的，当选中对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTargetDefault;
        [Header("默认的，当选中恒星对象时，摄像头定位时的水平面偏转角度")]
        public float m_focusTargetCameraOffsetAngleOnHorizontalDefault;
        [Header("当选中恒星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_Sun;
        [Header("当选中恒星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_Sun;
        [Header("当选中恒星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_Sun;
        [Header("当选中熔岩行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_LavaPlanet;
        [Header("当选中熔岩行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_LavaPlanet;
        [Header("当选中熔岩行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_LavaPlanet;
        [Header("当选中风暴行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_StormPlanet;
        [Header("当选中风暴行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_StormPlanet;
        [Header("当选中风暴行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_StormPlanet;
        [Header("当选中宜居行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_EarthPlanet;
        [Header("当选中宜居行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_EarthPlanet;
        [Header("当选中宜居行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_EarthPlanet;
        [Header("当选中沙漠行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_DesertPlanet;
        [Header("当选中沙漠行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_DesertPlanet;
        [Header("当选中沙漠行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_DesertPlanet;
        [Header("当选中海洋行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_OceanPlanet;
        [Header("当选中海洋行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_OceanPlanet;
        [Header("当选中海洋行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_OceanPlanet;
        [Header("当选中气体行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_GasPlanet;
        [Header("当选中气体行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_GasPlanet;
        [Header("当选中气体行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_GasPlanet;
        [Header("当选中冰体行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_IcePlanet;
        [Header("当选中冰体行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_IcePlanet;
        [Header("当选中冰体行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_IcePlanet;
        [Header("当选中岩石行星对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_RockPlanet;
        [Header("当选中岩石行星对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_RockPlanet;
        [Header("当选中岩石行星对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_RockPlanet;
        [Header("当选中军团建筑对象时，摄像头拉近显示时，相对于目标的俯仰角度值")]
        public float m_focusTargetCameraOffsetAngleOnVertical_GuildBuilding;
        [Header("当选中军团建筑对象时，摄像头拉近显示时，相对于目标的距离")]
        public float m_focusTargetDistanceToTarget_GuildBuilding;
        [Header("当选中军团建筑对象时，摄像头定位时的水平面偏转角度，即摄像机-目标连线与太阳-目标连线的夹角")]
        public float m_focusTargetCameraOffsetAngleOnHorizontal_GuildBuilding;
    }
}

