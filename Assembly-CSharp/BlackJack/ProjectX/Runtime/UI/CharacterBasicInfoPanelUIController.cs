﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterBasicInfoPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSendToChatButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPlayerIDButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCharChipSchemeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPreSchemeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNextSchemeButtonClick;
        private CharacterChipSlotItemUIController[] m_chipSlotItemList;
        private CommonUIStateController[] m_chipSchemeStateList;
        private static string ButtonStateNormal;
        private static string ButtonStateGray;
        public static string ChipSchemeStateNormal;
        public static string ChipSchemeStateCurr;
        public static string ChipSchemeStateLocked;
        private static string ImplantPanelStateSelf;
        private static string ImplantPanelStateOther;
        private string m_chipSchemeString1;
        private string m_chipSchemeString2;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./SchemeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_chipPageGroup;
        [AutoBind("./ImplanttButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_chipDetailRoot;
        [AutoBind("./SendToChatButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sendToChatButton;
        [AutoBind("./IDButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_idButton;
        [AutoBind("./BasicInfoRoot/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterNameText;
        [AutoBind("./BasicInfoRoot/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterLevelText;
        [AutoBind("./BasicInfoRoot/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterExpValueText;
        [AutoBind("./BasicInfoRoot/CharacterExpProgressInfo/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_characterExpProgressBar;
        [AutoBind("./BasicInfoRoot/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_characterIconImage;
        [AutoBind("./BasicInfoRoot/GradeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterGradeText;
        [AutoBind("./BasicInfoRoot/GradeText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_characterGradeTextStateCtrl;
        [AutoBind("./ChipSlotRoot/ChipSlot1", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot1;
        [AutoBind("./ChipSlotRoot/ChipSlot2", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot2;
        [AutoBind("./ChipSlotRoot/ChipSlot3", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot3;
        [AutoBind("./ChipSlotRoot/ChipSlot4", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot4;
        [AutoBind("./ChipSlotRoot/ChipSlot5", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot5;
        [AutoBind("./ChipSlotRoot/ChipSlot6", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot6;
        [AutoBind("./ChipSlotRoot/ChipSlot7", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot7;
        [AutoBind("./ChipSlotRoot/ChipSlot8", AutoBindAttribute.InitState.NotInit, false)]
        public CharacterChipSlotItemUIController m_chipSlot8;
        [AutoBind("./ImplanttButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_implanttButton;
        [AutoBind("./ImplantGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_implantGroup;
        [AutoBind("./ImplantGroup/Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_implantStateCtrl;
        [AutoBind("./ImplantGroup/Detail/MessageTitle/TitleSelf", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_implantTitleTextForSelf;
        [AutoBind("./ImplantGroup/Detail/MessageTitle/LButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_preSchemeButton;
        [AutoBind("./ImplantGroup/Detail/MessageTitle/LButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_preSchemeButtonStateCtrl;
        [AutoBind("./ImplantGroup/Detail/MessageTitle/RButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_nextSchemeButton;
        [AutoBind("./ImplantGroup/Detail/MessageTitle/RButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_nextSchemeButtonStateCtrl;
        [AutoBind("./ImplantGroup/Detail/MessageGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_propItemRoot;
        [AutoBind("./ImplantGroup/Detail/MessageGroup/Scroll View/Viewport/Content/MessageText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_propItem;
        [AutoBind("./ImplantGroup/BgBackGroudImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_implantBgBack;
        [AutoBind("./ImplantGroup/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyPanelState;
        private readonly List<PropItem> m_propItemList;
        [AutoBind("./SchemeGroup/SchemeOne", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_schemeOneStatectrl;
        [AutoBind("./SchemeGroup/SchemeTwo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_schemeTwoStatectrl;
        [AutoBind("./SchemeGroup/SchemeThree", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_schemeThreeStatectrl;
        [AutoBind("./SchemeGroup/SchemeOne", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_schemeOneButton;
        [AutoBind("./SchemeGroup/SchemeTwo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_schemeTwoButton;
        [AutoBind("./SchemeGroup/SchemeThree", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_schemeThreeButton;
        [AutoBind("./SchemeGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currSchemeNameText;
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitAllChipSlot;
        private static DelegateBridge __Hotfix_SetIsPlayerSelf;
        private static DelegateBridge __Hotfix_SetChipScheme;
        private static DelegateBridge __Hotfix_UpdateProperties;
        private static DelegateBridge __Hotfix_ShowChipSetEffect;
        private static DelegateBridge __Hotfix_SetCharName;
        private static DelegateBridge __Hotfix_SetCharIcon;
        private static DelegateBridge __Hotfix_SetCharLevel;
        private static DelegateBridge __Hotfix_SetCharExpInfo;
        private static DelegateBridge __Hotfix_SetCharGradeText;
        private static DelegateBridge __Hotfix_EnableChipTipAndChipPageBtn;
        private static DelegateBridge __Hotfix_RegEventOnChipSlotItemClick;
        private static DelegateBridge __Hotfix_GetChipSlotTrans;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreateImplantGroupProcess;
        private static DelegateBridge __Hotfix_OnSendToChatButtonClick;
        private static DelegateBridge __Hotfix_OnIDButtonClick;
        private static DelegateBridge __Hotfix_OnReturn2LoginClick;
        private static DelegateBridge __Hotfix_OnChipSchemeOneButtonClick;
        private static DelegateBridge __Hotfix_OnChipSchemeTwoButtonClick;
        private static DelegateBridge __Hotfix_OnChipSchemeThreeButtonClick;
        private static DelegateBridge __Hotfix_OnPreSchemeButtonClick;
        private static DelegateBridge __Hotfix_OnNextSchemeButtonClick;
        private static DelegateBridge __Hotfix_CreatePropertyAtIndex;
        private static DelegateBridge __Hotfix_EnableSendToChatButton;
        private static DelegateBridge __Hotfix_GetChipSchemeString2;
        private static DelegateBridge __Hotfix_GetChipSchemeString1;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_add_EventOnSendToChatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendToChatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerIDButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerIDButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCharChipSchemeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCharChipSchemeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPreSchemeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPreSchemeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNextSchemeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnNextSchemeButtonClick;
        private static DelegateBridge __Hotfix_get_ChipSlotItemList;
        private static DelegateBridge __Hotfix_get_ChipSchemeStateList;

        public event Action<int> EventOnCharChipSchemeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNextSchemeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPlayerIDButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPreSchemeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSendToChatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateImplantGroupProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreatePropertyAtIndex(string key, string value, int index)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableChipTipAndChipPageBtn(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableSendToChatButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private string GetChipSchemeString1(int schemeIndex)
        {
        }

        [MethodImpl(0x8000)]
        private string GetChipSchemeString2(int schemeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetChipSlotTrans(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void InitAllChipSlot(List<ConfigDataCharChipInfo> chipSlotList, List<bool> chipCanEuipedList, int playerLevel, Dictionary<string, UnityEngine.Object> resDict, bool ignoreFunctionState = false, bool showFunctionOpenAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChipSchemeOneButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChipSchemeThreeButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChipSchemeTwoButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnIDButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNextSchemeButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPreSchemeButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReturn2LoginClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSendToChatButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnChipSlotItemClick(Action<int> onClick)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharExpInfo(uint currExp, uint maxExp)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharGradeText(int value, bool showChangeAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharIcon(Sprite icon)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharName(string characterName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipScheme(List<string> schemeStateList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPlayerSelf(bool isPlayerSelf)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowChipSetEffect(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProperties(Dictionary<int, float> propList, int schemeIndex, List<string> stateList)
        {
        }

        private CharacterChipSlotItemUIController[] ChipSlotItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private CommonUIStateController[] ChipSchemeStateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

