﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarListUIController : UIControllerBase
    {
        private bool m_isShow;
        private const string ShipHangarListAssetName = "ShipHangarListItem";
        private const string StateShow = "Show";
        private const string StateClose = "Close";
        private ulong m_selectHangarInsId;
        private List<IGuildFlagShipHangarDataContainer> m_hangarListCache;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildFlagShipHangarListItemUIController> EventOnGuildFlagHangarShipListItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_hangarListEasyObjectPool;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_hangarListScrollRect;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_itemRoot;
        [AutoBind("./HangarDestoryedPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hangarDestoryedPanelStateCtrl;
        private static DelegateBridge __Hotfix_ShowOrHideShipHangrListPanel;
        private static DelegateBridge __Hotfix_UpdateViewGuildFlagShipHangarListUI;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnGuildFlagHangarShipListItemClick;
        private static DelegateBridge __Hotfix_OnGuildFlagHangarShipListItemFill;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagHangarShipListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagHangarShipListItemClick;

        public event Action<GuildFlagShipHangarListItemUIController> EventOnGuildFlagHangarShipListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagHangarShipListItemClick(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagHangarShipListItemFill(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideShipHangrListPanel(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewGuildFlagShipHangarListUI(List<IGuildFlagShipHangarDataContainer> hangarList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, ulong selectHangarInsId, int startIndex = 0)
        {
        }
    }
}

