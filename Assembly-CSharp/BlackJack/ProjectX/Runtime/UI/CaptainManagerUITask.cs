﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CaptainManagerUITask : UITaskBase
    {
        private bool m_isInCaptainLevelUpRepeatGuide;
        private bool m_isInCaptainUnlockShipRepeatGuide;
        public const string ParamKey_IsCaptainLevelUp = "IsCaptainLevelUp";
        public const string ParamKey_IsCaptainUnlockShip = "IsCaptainUnlockShip";
        public static string ParamKey_SelectedCaptain;
        public static string ParamKey_ResumeMode;
        private CaptainUITask m_captainUITask;
        private ShipHangarBGUITask m_shipModelBGTask;
        private CaptainListUITask m_captainListTask;
        private CaptainTrainingUITask m_captainTrainingTask;
        private CaptainRetireUITask m_captainRetireTask;
        private LBStaticHiredCaptain m_selCaptain;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private bool m_isCaptainUITaskResourceLoadComplete;
        private bool m_isCaptainListUITaskResourceLoadComplete;
        private bool m_isCaptainShipBGTaskResouceLoadComplete;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CaptainManagerUITask";
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetRepeatGuideParamFromIntent;
        private static DelegateBridge __Hotfix_RepeatGuideStepOne;
        private static DelegateBridge __Hotfix_RepeatGuideStepTwo;
        private static DelegateBridge __Hotfix_StopRepeatGuide;
        private static DelegateBridge __Hotfix_StartReturnableCaptainManagerUITask;
        private static DelegateBridge __Hotfix_StartCaptainManagerUITaskFromShipManageUI;
        private static DelegateBridge __Hotfix_BlockBGInput;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_RegisterUIEvents;
        private static DelegateBridge __Hotfix_UnregisterUIEvents;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_StartCaptainUITask;
        private static DelegateBridge __Hotfix_RegisterCaptainUIEvents;
        private static DelegateBridge __Hotfix_UnregisterCaptainUIEvents;
        private static DelegateBridge __Hotfix_StartCaptainListUITask;
        private static DelegateBridge __Hotfix_RegisterCaptainListUIEvents;
        private static DelegateBridge __Hotfix_UnRegisterCaptainListUIEvents;
        private static DelegateBridge __Hotfix_StartShipModelBGTask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCaptainListUITask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCaptainUITask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCaptainShipBGTask;
        private static DelegateBridge __Hotfix_OnCaptainSelectionChanged;
        private static DelegateBridge __Hotfix_OnCaptainShipButtonClick_0;
        private static DelegateBridge __Hotfix_OnCaptainShipButtonClick_1;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainTrainingButtonClick_0;
        private static DelegateBridge __Hotfix_OnCaptainTrainingButtonClick_1;
        private static DelegateBridge __Hotfix_OnCaptainTrainStart;
        private static DelegateBridge __Hotfix_OnCaptainTrainEnd;
        private static DelegateBridge __Hotfix_OnCaptainRetireButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainInterveneButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainLearnFeatButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_BackToPreUI;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_OnWingManShipItemClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfo;
        private static DelegateBridge __Hotfix_OnShowWndAnimEnd;
        private static DelegateBridge __Hotfix_GetCurrCaptainFromUIIntent;
        private static DelegateBridge __Hotfix_GetResumeUIModeFromUIIntent;
        private static DelegateBridge __Hotfix_PauseCaptainManagerTask;
        private static DelegateBridge __Hotfix_ReturnToPrevUI;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CaptainManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void BackToPreUI()
        {
        }

        [MethodImpl(0x8000)]
        public void BlockBGInput(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticHiredCaptain GetCurrCaptainFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public void GetRepeatGuideParamFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetResumeUIModeFromUIIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainDetailInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainFeatItemClick(LBNpcCaptainFeats feat)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainInterveneButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainLearnFeatButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainRetireButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainSelectionChanged(LBStaticHiredCaptain selCaptain)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainShipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainShipButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainTrainEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainTrainingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainTrainingButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainTrainStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCaptainListUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCaptainShipBGTask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCaptainUITask()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShowWndAnimEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnUpgradeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnWingManShipItemClick(ILBStoreItemClient ship, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void PauseCaptainManagerTask(Action endAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCaptainListUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCaptainUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStepOne()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStepTwo(bool isInRect)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToPrevUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfo(ILBStoreItemClient item, Vector3 pos, string mode, ItemSimpleInfoUITask.PositionType posType, bool isShowItemObtainSource)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartCaptainListUITask(Action redirectAction = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartCaptainManagerUITaskFromShipManageUI(UIIntent retIntent, LBStaticHiredCaptain selCaptain)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartCaptainUITask(Action redirectAction = null, string mode = "InitMode")
        {
        }

        [MethodImpl(0x8000)]
        public static CaptainManagerUITask StartReturnableCaptainManagerUITask(UIIntent retIntent, LBStaticHiredCaptain selCaptain = null, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartShipModelBGTask(Action redirectAction = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatGuide(bool stop)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterCaptainListUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterCaptainUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnCaptainShipButtonClick>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal CaptainManagerUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PauseCaptainManagerTask>c__AnonStorey1
        {
            internal Action endAction;
            internal CaptainManagerUITask $this;

            internal void <>m__0()
            {
                this.$this.m_captainListTask.Pause();
                this.$this.m_shipModelBGTask.Pause();
                this.$this.Pause();
                if (this.endAction != null)
                {
                    this.endAction();
                }
            }
        }
    }
}

