﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildMemberJobCtrlUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, GuildMemberInfo> EventOnMemberClick;
        private TabType m_curTab;
        private GuildMemberJobCtrlUIController m_jobCtrl;
        private GuildMemberUITask m_parentTask;
        private List<GuildMemberInfo> m_members;
        private List<JobInfo> m_jobInfos;
        private bool m_resetHorizontalScroll;
        private bool m_isMemberVersionChanged;
        private uint m_memberInfoVersion;
        private bool m_pauseTaskOnPostUpdateView;
        private UITaskBase.LayerDesc[] layer;
        private UITaskBase.UIControllerDesc[] ctrl;
        public const string TaskName = "GuildMemberJobCtrlUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask_1;
        private static DelegateBridge __Hotfix_StartTask_0;
        private static DelegateBridge __Hotfix_PlayShowProcess;
        private static DelegateBridge __Hotfix_ExpelGuildSucess;
        private static DelegateBridge __Hotfix_EditJob;
        private static DelegateBridge __Hotfix_GetFirstItemIndex;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_RegisterPlayerEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayerEvent;
        private static DelegateBridge __Hotfix_OnTitleItemClick;
        private static DelegateBridge __Hotfix_FindMemberCountForJob;
        private static DelegateBridge __Hotfix_OnTabModeChange;
        private static DelegateBridge __Hotfix_OnMemberJobEdit;
        private static DelegateBridge __Hotfix_OnConfirmChangeJob;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateAck;
        private static DelegateBridge __Hotfix_OnGuildLeaderTransferAbortAck;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateNtf;
        private static DelegateBridge __Hotfix_OnMemberListItemClick;
        private static DelegateBridge __Hotfix_set_MainJobType;
        private static DelegateBridge __Hotfix_get_MainJobType;
        private static DelegateBridge __Hotfix_set_MainArrowState;
        private static DelegateBridge __Hotfix_get_MainArrowState;
        private static DelegateBridge __Hotfix_add_EventOnMemberClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemberClick;
        private static DelegateBridge __Hotfix_set_EditPlayerUserId;
        private static DelegateBridge __Hotfix_get_EditPlayerUserId;
        private static DelegateBridge __Hotfix_set_GuildMemberFirstActiveIndex;
        private static DelegateBridge __Hotfix_get_GuildMemberFirstActiveIndex;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<UIControllerBase, GuildMemberInfo> EventOnMemberClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildMemberJobCtrlUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public void EditJob()
        {
        }

        [MethodImpl(0x8000)]
        public void ExpelGuildSucess()
        {
        }

        [MethodImpl(0x8000)]
        private int FindMemberCountForJob(GuildJobType job)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFirstItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmChangeJob(GuildMemberInfo memberInfo, GuildJobType job, bool isAppoint)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaderTransferAbortAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberJobUpdateAck(GuildMemberJobUpdateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberJobUpdateNtf(GuildMemberJobUpdateNtf msg)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberJobEdit(int index, GuildJobType job)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberListItemClick(GuildJobCtrlMemberNameItemController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabModeChange(TabType tab)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayShowProcess(bool show, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayerEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberJobCtrlUITask StartTask(Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberJobCtrlUITask StartTask(Action onLoadResComplete)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayerEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public GuildJobType MainJobType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public LogicBlockGuildClient.MemberTitleArrowState MainArrowState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private string EditPlayerUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private int GuildMemberFirstActiveIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnMemberJobEdit>c__AnonStorey1
        {
            internal GuildMemberInfo member;
            internal GuildJobType job;
            internal GuildMemberJobCtrlUITask $this;
        }

        [CompilerGenerated]
        private sealed class <OnMemberJobEdit>c__AnonStorey2
        {
            internal ProjectXPlayerContext playCtx;
            internal GuildMemberJobCtrlUITask.<OnMemberJobEdit>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(Task task)
            {
                GuildMemberJobCtrlUITask.GuildLeaderTransferAbortNetTask task2 = task as GuildMemberJobCtrlUITask.GuildLeaderTransferAbortNetTask;
                if (task2 != null)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        this.<>f__ref$1.$this.OnGuildLeaderTransferAbortAck(task2.Result);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnMemberJobEdit>c__AnonStorey3
        {
            internal int changeLeaderSeconds;
            internal GuildMemberJobCtrlUITask.<OnMemberJobEdit>c__AnonStorey1 <>f__ref$1;
            internal GuildMemberJobCtrlUITask.<OnMemberJobEdit>c__AnonStorey2 <>f__ref$2;

            internal void <>m__0()
            {
                GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask task = new GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask(this.<>f__ref$1.member.m_basicInfo.m_gameUserId, this.<>f__ref$1.job, true);
                task.EventOnStop += delegate (Task task) {
                    GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask task2 = task as GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask;
                    if (task2 != null)
                    {
                        if (task2.UpdateAck.Result != 0)
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.UpdateAck.Result, true, false);
                        }
                        else
                        {
                            LogicBlockGuildClient lBGuild = this.<>f__ref$2.playCtx.GetLBGuild() as LogicBlockGuildClient;
                            if (lBGuild != null)
                            {
                                GuildMemberInfo memberInfo = lBGuild.GetMemberInfo(task2.UpdateAck.PlayerGameUserId);
                                string timeSpanMaxUnitString = StringFormatUtil.GetTimeSpanMaxUnitString(TimeSpan.FromSeconds((double) this.changeLeaderSeconds));
                                object[] args = new object[] { memberInfo.m_runtimeInfo.m_name, timeSpanMaxUnitString };
                                TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_StartTransferLeader, false, args);
                            }
                            this.<>f__ref$1.$this.OnGuildMemberJobUpdateAck(task2.UpdateAck);
                        }
                    }
                };
                task.Start(null, null);
            }

            internal void <>m__1(Task task)
            {
                GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask task2 = task as GuildMemberJobCtrlUITask.GuildMemberJobUpdateNetTask;
                if (task2 != null)
                {
                    if (task2.UpdateAck.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.UpdateAck.Result, true, false);
                    }
                    else
                    {
                        LogicBlockGuildClient lBGuild = this.<>f__ref$2.playCtx.GetLBGuild() as LogicBlockGuildClient;
                        if (lBGuild != null)
                        {
                            GuildMemberInfo memberInfo = lBGuild.GetMemberInfo(task2.UpdateAck.PlayerGameUserId);
                            string timeSpanMaxUnitString = StringFormatUtil.GetTimeSpanMaxUnitString(TimeSpan.FromSeconds((double) this.changeLeaderSeconds));
                            object[] args = new object[] { memberInfo.m_runtimeInfo.m_name, timeSpanMaxUnitString };
                            TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_StartTransferLeader, false, args);
                        }
                        this.<>f__ref$1.$this.OnGuildMemberJobUpdateAck(task2.UpdateAck);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PlayShowProcess>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess p, bool ret)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(ret);
                }
            }
        }

        public class GuildLeaderTransferAbortNetTask : NetWorkTransactionTask
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Result>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_OnGuildLeaderTransferAbortAck;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_Result;

            public GuildLeaderTransferAbortNetTask() : base(10f, true, false, false)
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected void OnGuildLeaderTransferAbortAck(int result)
            {
                DelegateBridge bridge = __Hotfix_OnGuildLeaderTransferAbortAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp31(this, result);
                }
                else
                {
                    this.Result = result;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildLeaderTransferAbortAck += new Action<int>(this.OnGuildLeaderTransferAbortAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildLeaderTransferAbortReq());
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildLeaderTransferAbortAck -= new Action<int>(this.OnGuildLeaderTransferAbortAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }
        }

        public class GuildMemberJobUpdateNetTask : NetWorkTransactionTask
        {
            public GuildMemberJobUpdateAck UpdateAck;
            private string m_userId;
            private GuildJobType m_job;
            private bool m_isAppoint;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateAck;

            public GuildMemberJobUpdateNetTask(string userId, GuildJobType job, bool isAppoint) : base(10f, true, false, false)
            {
                this.m_userId = userId;
                this.m_job = job;
                this.m_isAppoint = isAppoint;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3602(this, userId, job, isAppoint);
                }
            }

            protected void OnGuildMemberJobUpdateAck(GuildMemberJobUpdateAck ack)
            {
                DelegateBridge bridge = __Hotfix_OnGuildMemberJobUpdateAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, ack);
                }
                else
                {
                    this.UpdateAck = ack;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMemberJobUpdateAck += new Action<GuildMemberJobUpdateAck>(this.OnGuildMemberJobUpdateAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildMemberJobUpdateReq(this.m_userId, this.m_job, this.m_isAppoint));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMemberJobUpdateAck -= new Action<GuildMemberJobUpdateAck>(this.OnGuildMemberJobUpdateAck);
                    }
                }
            }
        }

        public enum JobArrowState
        {
            Down = -1,
            Hide = 0,
            Up = 1
        }

        public class JobInfo
        {
            public ConfigDataGuildJobInfo cfgInfo;
            public int count;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public enum PipeLineMaskType
        {
            RemoveMember,
            Sort,
            ComeFromList,
            JobChange
        }

        public enum TabType
        {
            Member,
            Jurisdiction
        }
    }
}

