﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipHangarItemStoreUITask : UITaskBase
    {
        protected DateTime m_addButtonLongPressStartTime;
        protected DateTime m_removeButtonLongPressStartTime;
        protected ItemStoreUIPanelDataCache m_motherShipItemStoreUIData;
        protected ItemStoreUIPanelDataCache m_hangarShipItemStoreUIData;
        protected LBStaticPlayerShipClient m_currStaticShip;
        protected int m_currentHangarShipIndex;
        protected ShipHangarItemStoreUIController m_mainCtrl;
        protected ItemStoreListUIController m_motherShipItemStoreUICtrl;
        protected ItemStoreListUIController m_hangarShipItemStoreUICtrl;
        protected ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        protected bool m_isOpenInSpace;
        private DateTime m_lastItemStoreUpdateTime;
        private bool m_isDuringUpdateView;
        public static string ModeNoSelected;
        public static string ModeHangarShipSelectedItem;
        public static string ModeMotherShipSelectedItem;
        public static string ParamKey_HangarShipIndex;
        public static string ParamKey_IsRefreshAll;
        public static string HangarShipItemUIDataStateKey;
        public static string MotherShipItemUIDataStateKey;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string ShipHangarItemStoreUIPrefabAssetPath;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache0;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_CreatePipeLineCtx;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitMainUICtrl;
        private static DelegateBridge __Hotfix_InitMotherShipItemStoreUICtrl;
        private static DelegateBridge __Hotfix_InitSpaceShipItemStoreUICtrl;
        private static DelegateBridge __Hotfix_InitSelectItemInfoUICtrl;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_UpdateViewMotherShipItemStoreListUI;
        private static DelegateBridge __Hotfix_UpdateViewHangarShipItemStoreListUI;
        private static DelegateBridge __Hotfix_UpdateSelectItemSimpleInfoUI;
        private static DelegateBridge __Hotfix_SetItemInfoUICtrlToMoveToHangarShip;
        private static DelegateBridge __Hotfix_SetItemInfoUICtrlToMoveToMotherShip;
        private static DelegateBridge __Hotfix_UpdateItemListUITabChanged;
        private static DelegateBridge __Hotfix_OnMotherShipItemListClick;
        private static DelegateBridge __Hotfix_OnMotherShipItemListDoubleClick;
        private static DelegateBridge __Hotfix_OnHangarShipItemListClick;
        private static DelegateBridge __Hotfix_OnHangarShipItemListDoubleClick;
        private static DelegateBridge __Hotfix_OnMotherShipItemListUITabChanged;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnItemInfoRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnItemInfoRemoveButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnItemInfoRemoveButtonLongPressing;
        private static DelegateBridge __Hotfix_OnItemInfoAddButtonClick;
        private static DelegateBridge __Hotfix_OnItemInfoAddButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnItemInfoAddButtonLongPressing;
        private static DelegateBridge __Hotfix_MotherShipItemDestroyButtonButtonClick;
        private static DelegateBridge __Hotfix_OnLoadToHangarShipButtonClick;
        private static DelegateBridge __Hotfix_OnUnLoadToMotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnSelectItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadAllButtonClick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_GetShipCompItemStoreClient;
        private static DelegateBridge __Hotfix_UpdateShipInfoDataCache;
        private static DelegateBridge __Hotfix_GetCurrStaticShip;
        private static DelegateBridge __Hotfix_GetCurrSelectItem;
        private static DelegateBridge __Hotfix_ResetUIState;
        private static DelegateBridge __Hotfix_IsStayWithMotherShip;
        private static DelegateBridge __Hotfix_CheckIsOperatorNeedReturnBase;
        private static DelegateBridge __Hotfix_CheckMoveItemToItemStore;
        private static DelegateBridge __Hotfix_SendMoveItemToHangarShipItemStore;
        private static DelegateBridge __Hotfix_SendMoveItemToMotherShipItemStore;
        private static DelegateBridge __Hotfix_get_CurrPipeLineCtx;
        private static DelegateBridge __Hotfix_set_CurrPipeLineCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ShipHangarItemStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckIsOperatorNeedReturnBase()
        {
        }

        [MethodImpl(0x8000)]
        protected int CheckMoveItemToItemStore(ILBStoreItemClient item, int moveCount, float remainVolume)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override UITaskPipeLineCtx CreatePipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected ILBStoreItemClient GetCurrSelectItem()
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticPlayerShipClient GetCurrStaticShip()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LogicBlockShipCompItemStoreClient GetShipCompItemStoreClient()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitMainUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitMotherShipItemStoreUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitSelectItemInfoUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitSpaceShipItemStoreUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        protected void MotherShipItemDestroyButtonButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHangarShipItemListClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHangarShipItemListDoubleClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoAddButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoAddButtonLongPressStart(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoRemoveButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoRemoveButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemInfoRemoveButtonLongPressStart(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadToHangarShipButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMotherShipItemListClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMotherShipItemListDoubleClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMotherShipItemListUITabChanged(ItemStoreListUIController.ItemStoreTabType itemTabType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelectItemDetailButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnloadAllButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnLoadToMotherShipButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetUIState()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendMoveItemToHangarShipItemStore(ILBStoreItemClient item, long count)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendMoveItemToMotherShipItemStore(ILBStoreItemClient item, long count)
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemInfoUICtrlToMoveToHangarShip()
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemInfoUICtrlToMoveToMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemListUITabChanged(ItemStoreUIPanelDataCache itemStoreState, ItemStoreListUIController.ItemStoreTabType itemTabType)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectItemSimpleInfoUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateShipInfoDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewHangarShipItemStoreListUI()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewMotherShipItemStoreListUI()
        {
        }

        protected ShipHangarItemStoreUITaskPipeLineCtx CurrPipeLineCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnUnloadAllButtonClick>c__AnonStorey0
        {
            internal HangarShipTransformAllItemToMSStoreReqNetTask netTask;
            internal ShipHangarItemStoreUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipTransformAllItemToMSStoreReqNetTask netTask = this.netTask;
                if (!netTask.IsNetworkError)
                {
                    if (netTask.AckResult != 0)
                    {
                        Debug.LogError(string.Format("OnUnloadAllButtonClick: HangarShipTransformAllItemToMSStoreReqNetTask AckResult = ", netTask.AckResult));
                        TipWindowUITask.ShowTipWindowWithErrorCode(netTask.AckResult, true, false);
                    }
                    else
                    {
                        PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_ShipHangarItemStore_TransferedItem, null, 1f);
                        this.$this.EnablePipelineStateMask(ShipHangarItemStoreUITask.PipeLineStateMaskType.IsHangarShipRefreshAllItem);
                        this.$this.EnablePipelineStateMask(ShipHangarItemStoreUITask.PipeLineStateMaskType.IsMotherShipRefreshAllItem);
                        this.$this.EnablePipelineStateMask(ShipHangarItemStoreUITask.PipeLineStateMaskType.IsUpdateSelectItemSimpleInfo);
                        this.$this.m_motherShipItemStoreUIData.m_selectedItemIndex = -1;
                        this.$this.m_hangarShipItemStoreUIData.m_selectedItemIndex = -1;
                        this.$this.m_currIntent.TargetMode = ShipHangarItemStoreUITask.ModeNoSelected;
                        this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                    }
                }
            }
        }

        protected class ItemStoreUIPanelDataCache
        {
            public ItemStoreDefaultUIFilter m_itemStoreFilter = new ItemStoreDefaultUIFilter();
            public List<ILBStoreItemClient> m_cachedStoreItemList = new List<ILBStoreItemClient>();
            public int m_selectedItemIndex = -1;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Reset;
            private static DelegateBridge __Hotfix_GetCurrSelectItem;
            private static DelegateBridge __Hotfix_GetLBItem;

            public ItemStoreUIPanelDataCache()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public ILBStoreItemClient GetCurrSelectItem()
            {
                DelegateBridge bridge = __Hotfix_GetCurrSelectItem;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp3212(this);
                }
                if ((this.m_selectedItemIndex < 0) || (this.m_selectedItemIndex >= this.m_cachedStoreItemList.Count))
                {
                    return null;
                }
                return this.m_cachedStoreItemList[this.m_selectedItemIndex];
            }

            public ILBStoreItemClient GetLBItem(int index)
            {
                DelegateBridge bridge = __Hotfix_GetLBItem;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5165(this, index);
                }
                if ((index < 0) || (index >= this.m_cachedStoreItemList.Count))
                {
                    return null;
                }
                return this.m_cachedStoreItemList[index];
            }

            public void Reset()
            {
                DelegateBridge bridge = __Hotfix_Reset;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_itemStoreFilter.CurrItemTabType = ItemStoreListUIController.ItemStoreTabType.All;
                    this.m_itemStoreFilter.CurrDropdownValue = 0;
                    this.m_selectedItemIndex = -1;
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsMotherShipRefreshAllItem,
            IsMotherShipResetStartIndex,
            IsMotherShipRefreshSingleItem,
            IsMotherShipDataCacheChanged,
            IsMotherShipItemFilterChanged,
            IsHangarShipRefreshAllItem,
            IsHangarShipItemFilterChanged,
            IsHangarShipResetStartIndex,
            IsHangarShipRefreshSingleItem,
            IsHangarShipDataCacheChanged,
            IsUpdateSelectItemSimpleInfo
        }
    }
}

