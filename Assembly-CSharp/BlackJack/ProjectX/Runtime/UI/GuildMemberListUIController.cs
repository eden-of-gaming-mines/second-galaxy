﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnMemberItemClick;
        private List<GuildMemberInfo> m_members;
        private Dictionary<string, UnityEngine.Object> m_resData;
        private GuildMemberInfo[] m_mostThreeContribute;
        private Dictionary<LogicBlockGuildClient.MemberTitleType, CommonUIStateController> m_sortBtnStateDic;
        private CommonUIStateController[] m_sortButtonArray;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./TitleGroup/StationGalaxy", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExGalaxy;
        [AutoBind("./TitleGroup/StationGalaxy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlGalaxy;
        [AutoBind("./TitleGroup/Grade", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExGrade;
        [AutoBind("./TitleGroup/Grade", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlGrade;
        [AutoBind("./TitleGroup/MilitaryRank", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExMilitaryRank;
        [AutoBind("./TitleGroup/MilitaryRank", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlMilitaryRank;
        [AutoBind("./TitleGroup/Contribution", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExContribution;
        [AutoBind("./TitleGroup/Contribution", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlContribution;
        [AutoBind("./TitleGroup/OnLine", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExOnLine;
        [AutoBind("./TitleGroup/OnLine", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlOnLine;
        [AutoBind("./TitleGroup/Job", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buttonExJob;
        [AutoBind("./TitleGroup/Job", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ctrlJob;
        [AutoBind("./PeopleGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_memberItemRoot;
        [AutoBind("./PeopleGroup/ItemRoot/GuildMemberItemUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_memberItemObj;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopScrollView;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        public const string MemberItemUIPrefab = "GuildMemberItemUIPrefab";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateSortButtonState;
        private static DelegateBridge __Hotfix_OnMemberItemClick;
        private static DelegateBridge __Hotfix_OnMemberItemFill;
        private static DelegateBridge __Hotfix_add_EventOnMemberItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemberItemClick;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_GetFirstItemIndex;
        private static DelegateBridge __Hotfix_IsPlayerSelf;

        public event Action<UIControllerBase> EventOnMemberItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetFirstItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool isImmediateComplete)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPlayerSelf(GuildMemberInfo member)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSortButtonState(LogicBlockGuildClient.MemberTitleType type, LogicBlockGuildClient.MemberTitleArrowState states)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<GuildMemberInfo> members, GuildMemberInfo[] mostThreeContribute, Dictionary<string, UnityEngine.Object> resData, int firstIndex)
        {
        }
    }
}

