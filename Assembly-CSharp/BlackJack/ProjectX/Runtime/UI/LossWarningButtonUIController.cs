﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LossWarningButtonUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        private int m_pvpMinLevel;
        private bool m_showAutoMove;
        private bool m_showSafety;
        private bool m_showDanger;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_lossWarningButton;
        [AutoBind("./MoveButton", AutoBindAttribute.InitState.NotInit, true)]
        public GameObject m_autoMoveGo;
        [AutoBind("./SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_safetyGo;
        [AutoBind("./DangerButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_dangerGo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateLossWarningButtonState_1;
        private static DelegateBridge __Hotfix_UpdateLossWarningButtonState_2;
        private static DelegateBridge __Hotfix_UpdateLossWarningButtonState_3;
        private static DelegateBridge __Hotfix_UpdateLossWarningButtonState_0;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_SetLossWarningState;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetLossWarningState(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLossWarningButtonState(bool isDanger)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLossWarningButtonState(ConfigDataQuestInfo conf, bool isQuestFailed = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLossWarningButtonState(ConfigDataSignalInfo conf, bool isQuestFailed = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLossWarningButtonState(ConfigDataSpaceSignalInfo conf, bool isQuestFailed = false)
        {
        }
    }
}

