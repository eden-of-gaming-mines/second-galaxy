﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildMomentsListItemUIController : UIControllerBase
    {
        private ScrollItemBaseUIController m_scrollItemBaseUICtrl;
        [AutoBind("./ContentGroup/FixedGroup/LabelImage/LabelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_typeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonStateUICtrl;
        [AutoBind("./ToLoadGroup/ToLoadImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_loadMoreButton;
        [AutoBind("./ContentGroup/FixedGroup/Time", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeText;
        [AutoBind("./ContentGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
        public TextExpand m_contentText;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateListItem;
        private static DelegateBridge __Hotfix_UpdateGuildMomentsState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMomentsState(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateListItem(GuildMomentsInfo info)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

