﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForSolarSystemRightPopupMenuUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_viewModeCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler m_menuScrollViewEventHandler;
        [AutoBind("./Tab/CelestialTab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_celestialTab;
        [AutoBind("./Tab/CelestialTab/PointImage/MissionCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spaceSignalCountText;
        [AutoBind("./Tab/QuestTab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_questTab;
        [AutoBind("./Tab/QuestTab/PointImage/MissionCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questCountText;
        [AutoBind("./Tab/QuestSignalTab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_questSignalTab;
        [AutoBind("./Tab/QuestSignalTab/Image", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_questSignalTabClickImage;
        [AutoBind("./Tab/QuestSignalTab/PointImage/MissionCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questSignalCountText;
        [AutoBind("./Tab/DelegateSignalTab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_delegateSignalTab;
        [AutoBind("./Tab/DelegateSignalTab/Image", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_delegateSignalTabClickImage;
        [AutoBind("./Tab/DelegateSignalTab/PointImage/MissionCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_delegateSignalCountText;
        [AutoBind("./Tab/PvpSignalTab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_pvpSignalTab;
        [AutoBind("./Tab/PvpSignalTab/Image", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_pvpSignalTabClickImage;
        [AutoBind("./Tab/PvpSignalTab/PointImage/MissionCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pvpSignalCountText;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_scrollViewTrans;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_viewItemListRootTrans;
        [AutoBind("./NoSignalStateRoot/NoSignalStateText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_noSignalStateObj;
        [AutoBind("./ItemSimpleInfoPanelLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSimpleInfoPanelLeftDummy;
        [AutoBind("./PoolParent", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<MenuItemUIControllerBase> EventOnMenuItemIconButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnQuestAssignButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<VirtualBuffDesc>> EventOnQuestRewardBonusButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnDelegateSignalAssignButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DelegateSignalMenuItemUIController> EventOnDelegateSignalRewardBonusButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PvpSignalMenuItemUIController> EventOnPVPSignalAssignButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PvpSignalMenuItemUIController> EventOnPVPSignalOperationButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBPlanetInfo> EventOnJumpToPlanetButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBStarInfo> EventOnJumpToStarButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSpaceStationInfo> EventOnJumpToSpaceStationButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSpaceStationInfo> EventOnBaseDeployButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<WormholeMenuItemUIController> EventOnJumpToWormholeStargateButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnJumpToUnknownWormholeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBStargateInfo> EventOnJumpToStargateButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SignalInfo> EventOnJumpToSpaceSignalButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnMenuItemRewardClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnMenuItemReward3DTouch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnMenuScrollViewClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnInfectFinalBattleStrikeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllowInShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBPVPInvadeRescue> EventOnRescueStrikeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<RescueMenuItemUIController> EventOnRescueCaptainShowingButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBPVPInvadeRescue, Vector3> EventOnRescueSignalOperationButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildSentryInterestScene> EventOnGuildSentrySignalStrikeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector3> EventOnSpaceSignalPrecisenessDetailButtonClick;
        public RightPopupMenuItemProviderController m_menuItemProviderCtrl;
        public StarMapRightPopupWndUIController m_scrollViewUICtrl;
        private readonly List<MenuItemUIControllerBase> m_scrollViewItemList;
        private ButtonEx[] m_menuTabButtonList;
        private const float PlayItemTweensInterval = 0.2f;
        public const string TABMODE_QUESTSIGNAL = "QuestSignalTab";
        public const string TABMODE_DELEGATESIGNAL = "DelegateSignalTab";
        public const string TABMODE_PVPSIGNAL = "PvpSignalTab";
        public const string TABMODE_QUEST = "QuestTab";
        public const string TABMODE_CELESTIAL = "CelestialTab";
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<MenuItemUIControllerBase, bool> <>f__am$cache8;
        private static DelegateBridge __Hotfix_CheckScrollViewShowStateFinished;
        private static DelegateBridge __Hotfix_CheckScrollViewItemShowStateFinished;
        private static DelegateBridge __Hotfix_CheckScrollViewItemDetailInfoShowStateFinished;
        private static DelegateBridge __Hotfix_GetFirstFreeQuestMenuItem;
        private static DelegateBridge __Hotfix_GetFirstQuestSignalMenuItem;
        private static DelegateBridge __Hotfix_GetDelegateSignalMenuItem;
        private static DelegateBridge __Hotfix_GetPVPSignalMenuItem;
        private static DelegateBridge __Hotfix_GetGuildSentryItemCtrlByInstanceId;
        private static DelegateBridge __Hotfix_GetFirstEmergencySignalMenuItem;
        private static DelegateBridge __Hotfix_GetFirstSpaceSignalMenuItem;
        private static DelegateBridge __Hotfix_GetQuestMenuItemStrikeButtonTrans;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPopupMenuViewMode;
        private static DelegateBridge __Hotfix_SwitchPopupMenuViewMode;
        private static DelegateBridge __Hotfix_SetViewScrollToBottom;
        private static DelegateBridge __Hotfix_SetViewScrollToMakeItemOnBottom;
        private static DelegateBridge __Hotfix_SetScrollViewVisible;
        private static DelegateBridge __Hotfix_SetMenuTabAvailable;
        private static DelegateBridge __Hotfix_SetMenuTabSelected;
        private static DelegateBridge __Hotfix_SetMenuTabUnselected;
        private static DelegateBridge __Hotfix_SetAllMenuTabUnselected;
        private static DelegateBridge __Hotfix_SwitchDetailInfoVisibleState;
        private static DelegateBridge __Hotfix_GetScrollViewItemCtrlByIndex;
        private static DelegateBridge __Hotfix_GetFirstScrollViewItemByType;
        private static DelegateBridge __Hotfix_GetMenuItemCtrlByInstanceId;
        private static DelegateBridge __Hotfix_UpdateMenuItemBySignals;
        private static DelegateBridge __Hotfix_RefreshScrollViewListByCelestials;
        private static DelegateBridge __Hotfix_RefreshScrollViewListByQuests;
        private static DelegateBridge __Hotfix_RefreshScrollViewListByQuestSignals;
        private static DelegateBridge __Hotfix_RefreshScrollViewListByDelegateSignals;
        private static DelegateBridge __Hotfix_RefreshScrollViewListByCurrSolarSystemPvpSignals;
        private static DelegateBridge __Hotfix_AddScrollViewListByOtherSolarSystemPvpSignals;
        private static DelegateBridge __Hotfix_AddScrollViewListByGuildSentrySingals;
        private static DelegateBridge __Hotfix_SetPvpSignalButtonEventAndAddMenu;
        private static DelegateBridge __Hotfix_AddScrollViewListByRescuePvpSignals;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelDummyPos;
        private static DelegateBridge __Hotfix_AddInfectInfoItemToScrollView;
        private static DelegateBridge __Hotfix_SetScrollViewItemCountByTabMode;
        private static DelegateBridge __Hotfix_PlayAllScrollViewItemShowingEffect;
        private static DelegateBridge __Hotfix_ShowAllScrollViewItemImmediately;
        private static DelegateBridge __Hotfix_OnMenuItemIconButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateSignalRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_OnPVPSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_OnPVPSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_OnQuestAssignButtonClick;
        private static DelegateBridge __Hotfix_OnRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_OnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnRescuePlayerOperationButtonClick;
        private static DelegateBridge __Hotfix_OnRescueCaptainShowingButtonClick;
        private static DelegateBridge __Hotfix_OnAllowInShipTypeButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToStarButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToPlanetButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_OnBaseDeployButtonClick;
        private static DelegateBridge __Hotfix_OnJumptoStargateButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToWormholeStargateButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToGuildSentrySignalButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToUnknownWormholeButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceSignalPrecisenessDetailButtonClick;
        private static DelegateBridge __Hotfix_OnMenuItemRewardItemClick;
        private static DelegateBridge __Hotfix_OnMenuItemRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnMenuScrollViewClick;
        private static DelegateBridge __Hotfix_OnInfectFinalBattleStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_ClearScrollViewItems;
        private static DelegateBridge __Hotfix_AddMenuItemToScrollView;
        private static DelegateBridge __Hotfix_GetFirstMenuItem;
        private static DelegateBridge __Hotfix_GetSpaceStationItemRect;
        private static DelegateBridge __Hotfix_GetSpaceStation;
        private static DelegateBridge __Hotfix_EnableScrollView;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployButton;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployRect;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployButtonArea;
        private static DelegateBridge __Hotfix_GetWormholeMenuPanelController;
        private static DelegateBridge __Hotfix_UnLockBaseDeployButton;
        private static DelegateBridge __Hotfix_SetAssignButtonForFristDelegateSignalItemInView;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestAssignButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestAssignButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateSignalRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateSignalRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVPSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVPSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVPSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVPSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToPlanetButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToPlanetButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToStarButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToStarButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBaseDeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseDeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToWormholeStargateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToWormholeStargateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToUnknownWormholeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToUnknownWormholeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToStargateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToStargateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToSpaceSignalButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToSpaceSignalButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemRewardClick;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemRewardClick;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemReward3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemReward3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnMenuScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnMenuScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnInfectFinalBattleStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnInfectFinalBattleStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRescueCaptainShowingButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueCaptainShowingButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRescueSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpaceSignalPrecisenessDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpaceSignalPrecisenessDetailButtonClick;
        private static DelegateBridge __Hotfix_get_MenuTabButtonList;

        public event Action EventOnAllowInShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSpaceStationInfo> EventOnBaseDeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnDelegateSignalAssignButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DelegateSignalMenuItemUIController> EventOnDelegateSignalRewardBonusButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildSentryInterestScene> EventOnGuildSentrySignalStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnInfectFinalBattleStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBPlanetInfo> EventOnJumpToPlanetButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SignalInfo> EventOnJumpToSpaceSignalButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSpaceStationInfo> EventOnJumpToSpaceStationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBStarInfo> EventOnJumpToStarButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBStargateInfo> EventOnJumpToStargateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnJumpToUnknownWormholeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<WormholeMenuItemUIController> EventOnJumpToWormholeStargateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<MenuItemUIControllerBase> EventOnMenuItemIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnMenuItemReward3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnMenuItemRewardClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMenuScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PvpSignalMenuItemUIController> EventOnPVPSignalAssignButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PvpSignalMenuItemUIController> EventOnPVPSignalOperationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnQuestAssignButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<VirtualBuffDesc>> EventOnQuestRewardBonusButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<RescueMenuItemUIController> EventOnRescueCaptainShowingButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBPVPInvadeRescue, Vector3> EventOnRescueSignalOperationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBPVPInvadeRescue> EventOnRescueStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Vector3> EventOnSpaceSignalPrecisenessDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddInfectInfoItemToScrollView(float infectProgress, DateTime selfHealingTime, ConfigDataItemDropInfo infectRewardInfo, bool isFinalBattle, Dictionary<string, UnityEngine.Object> resDict, Action eventOnTimeout)
        {
        }

        [MethodImpl(0x8000)]
        private void AddMenuItemToScrollView(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void AddScrollViewListByGuildSentrySingals(int ssId, List<GuildSentryInterestScene> guildSentryList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void AddScrollViewListByOtherSolarSystemPvpSignals(int ssId, List<LBSignalBase> otherPvpSignalList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void AddScrollViewListByRescuePvpSignals(List<LBPVPInvadeRescue> rescueInfoList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScrollViewItemDetailInfoShowStateFinished(int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScrollViewItemShowStateFinished(int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScrollViewShowStateFinished()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearScrollViewItems()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableScrollView(bool isEnable = true)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateSignalMenuItemUIController GetDelegateSignalMenuItem(SignalType signalType, out int index)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController GetFirstEmergencySignalMenuItem(out int index)
        {
        }

        [MethodImpl(0x8000)]
        public QuestMenuItemUIController GetFirstFreeQuestMenuItem(QuestType questType, out int index)
        {
        }

        [MethodImpl(0x8000)]
        private MenuItemUIControllerBase GetFirstMenuItem(Func<MenuItemUIControllerBase, bool> condition)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController GetFirstQuestSignalMenuItem(QuestType questType, out int index)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase GetFirstScrollViewItemByType(MenuItemType menuItemType)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSignalMenuItemUIController GetFirstSpaceSignalMenuItem(out int index)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentrySignalMenuItemUIController GetGuildSentryItemCtrlByInstanceId(ulong instanceId, out int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase GetMenuItemCtrlByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemUIController GetPVPSignalMenuItem(out int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetQuestMenuItemStrikeButtonTrans(int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase GetScrollViewItemCtrlByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationMenuItemUIController GetSpaceStation()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetSpaceStationBaseDeployButton()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationBaseDeployButtonArea()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationBaseDeployRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase GetWormholeMenuPanelController()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseDeployButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalAssignButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalRewardBonusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInfectFinalBattleStrikeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToGuildSentrySignalButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToPlanetButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceStationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToStarButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumptoStargateButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToUnknownWormholeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToWormholeStargateButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger, MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemIconButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemRewardItem3DTouch(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPSignalAssignButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPSignalOperationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAssignButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueCaptainShowingButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescuePlayerOperationButtonClick(LBPVPInvadeRescue signalInfo, Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueStrikeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardBonusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceSignalPrecisenessDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayAllScrollViewItemShowingEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshScrollViewListByCelestials(GDBSolarSystemInfo solarSystemInfo, List<SignalInfo> spaceSignalInfoList, LinkedList<GlobalSceneInfo> globalSceneList, ConfigDataWormholeStarGroupInfo extraWormhole, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshScrollViewListByCurrSolarSystemPvpSignals(int ssId, List<LBSignalBase> signalList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshScrollViewListByDelegateSignals(int ssId, List<LBSignalDelegateBase> signalList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshScrollViewListByQuests(List<LBProcessingQuestBase> questInfoList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshScrollViewListByQuestSignals(int ssId, List<LBSignalManualBase> signalList, List<LBSignalManualBase> emergencySignalList, Dictionary<string, UnityEngine.Object> resDict, bool needPlayTweens = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllMenuTabUnselected()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAssignButtonForFristDelegateSignalItemInView()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuTabAvailable(string mode, bool isAvailable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuTabSelected(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuTabUnselected(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPopupMenuViewMode(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPvpSignalButtonEventAndAddMenu(PvpSignalMenuItemUIController pvpSignalItem, LBPVPSignal signalInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScrollViewItemCountByTabMode(string tabMode, int count)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess SetScrollViewVisible(bool isVisible, bool isImmediateHideOrShow = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetViewScrollToBottom()
        {
        }

        [MethodImpl(0x8000)]
        public void SetViewScrollToMakeItemOnBottom(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowAllScrollViewItemImmediately()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchDetailInfoVisibleState(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchPopupMenuViewMode(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UnLockBaseDeployButton()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuItemBySignals(DateTime currTime, Dictionary<ulong, LBSignalBase> signalDict, List<LBSignalManualBase> questList, List<LBSignalManualBase> emergencySignalList)
        {
        }

        private ButtonEx[] MenuTabButtonList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetMenuItemCtrlByInstanceId>c__AnonStorey0
        {
            internal ulong insId;

            internal bool <>m__0(MenuItemUIControllerBase elem) => 
                (elem.GetInstanceId() == this.insId);
        }

        [CompilerGenerated]
        private sealed class <OnJumpToWormholeStargateButtonClick>c__AnonStorey1
        {
            internal UIControllerBase ctrl;
            internal StarMapForSolarSystemRightPopupMenuUIController $this;

            internal void <>m__0()
            {
                if (this.$this.EventOnJumpToWormholeStargateButtonClick != null)
                {
                    this.$this.EventOnJumpToWormholeStargateButtonClick(this.ctrl as WormholeMenuItemUIController);
                }
            }
        }
    }
}

