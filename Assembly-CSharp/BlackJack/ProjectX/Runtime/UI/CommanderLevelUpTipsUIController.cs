﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CommanderLevelUpTipsUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAnimationEnd;
        [AutoBind("./LevelGroup/PreGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreviousLevel;
        [AutoBind("./LevelGroup/AftGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrentLevel;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_ShowLevelUpTips;
        private static DelegateBridge __Hotfix_EndAnimationInTime;
        private static DelegateBridge __Hotfix_add_EventOnAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnAnimationEnd;

        public event Action EventOnAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator EndAnimationInTime()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLevelUpTips(int preLevel, int currLevel)
        {
        }

        [CompilerGenerated]
        private sealed class <EndAnimationInTime>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal CommanderLevelUpTipsUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

