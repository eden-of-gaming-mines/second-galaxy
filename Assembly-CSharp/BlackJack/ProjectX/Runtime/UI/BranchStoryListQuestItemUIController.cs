﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryListQuestItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick;
        private BranchStoryUITask.BranchStoryItemInfo m_currItemInfo;
        private bool m_isInit;
        private bool m_isTriggerEvent = true;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ItemToggle;
        [AutoBind("./QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./JumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemProcessText;
        [AutoBind("./RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RedPoint;
        [AutoBind("./EmptyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EmptyText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UpdateQuestItem;
        private static DelegateBridge __Hotfix_UpdateRedPoint;
        private static DelegateBridge __Hotfix_UpdateQuestItemSelectState;
        private static DelegateBridge __Hotfix_OnToggleValueChange;
        private static DelegateBridge __Hotfix_add_EventOnQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestItemClick;
        private static DelegateBridge __Hotfix_get_ItemInfo;

        public event Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleValueChange(bool select)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterUIEvent(Action<BranchStoryUITask.BranchStoryItemInfo> onItemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestItem(BranchStoryUITask.BranchStoryItemInfo itemInfo, bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestItemSelectState(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRedPoint(BranchStoryUITask.BranchStoryItemInfo itemInfo, bool isSelect)
        {
        }

        public BranchStoryUITask.BranchStoryItemInfo ItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

