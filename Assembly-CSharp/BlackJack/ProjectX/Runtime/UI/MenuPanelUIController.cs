﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(CanvasGroup))]
    public class MenuPanelUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MenuStateCtrl;
        protected int m_actionPlanNoticeCount;
        public const string MenuState_PanelOpen = "PanelOpen";
        public const string MenuState_PanelClose = "PanelClose";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CanvasMask/FunctionButtonScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_functionButtonViewTransform;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button StarMapButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button MailButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/MailButton/FlashEffectImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MailFlashEffectCtrl;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button TeamButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/ExploreButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ActivityButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/CommanderPacketButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CommanderPacketButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/GuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/SettingButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button SettingButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/TradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button TradeButton;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/RelationButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button RelationButton;
        [AutoBind("./CharacterInfo", AutoBindAttribute.InitState.NotInit, false)]
        public Button CharacterButton;
        [AutoBind("./CharacterInfo/CharacterImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CharacterImage;
        [AutoBind("./CharacterInfo/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelValue;
        [AutoBind("./CharacterInfo/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CharacterRedPoint;
        [AutoBind("./CharacterInfo/CharacterNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterName;
        [AutoBind("./CharacterInfo/C-Group/C-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindMoneyValue;
        [AutoBind("./CharacterInfo/S-Group/S-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeMoneyValue;
        [AutoBind("./CharacterInfo/IR-Group/IR-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text RealMoneyValue;
        [AutoBind("./CharacterInfo/JobImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image JobImage;
        [AutoBind("./CharacterInfo/JobText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JobText;
        [AutoBind("./CharacterInfo/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ExperienceText;
        [AutoBind("./CharacterInfo/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ExperienceProgressImage;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/MailButton/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MailNotice;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/MailButton/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text MailNoticeCount;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/ExploreButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActionPlanStateCtrl;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/ExploreButton/PointGroup/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActionPlanNoticeCount;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/TeamButton/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TeamNotice;
        [AutoBind("./CanvasMask/FunctionButtonScrollView/Viewport/Content/GuildButton/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_guildNoticeImage;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdatePlayerInfo;
        private static DelegateBridge __Hotfix_UpdateMailNoticeInfo;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_ShowMenuPanel;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_UpdateActionPlanInfo;
        private static DelegateBridge __Hotfix_SetTeamRedPoint;
        private static DelegateBridge __Hotfix_SetCharacterPoint;
        private static DelegateBridge __Hotfix_SetGuildRedPoint;
        private static DelegateBridge __Hotfix_SetIsInStationMode;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacterPoint(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildRedPoint(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsInStationMode(bool isInStation)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTeamRedPoint(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIMode(MenuPanelUIMode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMenuPanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActionPlanInfo(bool isRedPoint, bool isRedCount = false, int count = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMailNoticeInfo(int mailCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerInfo(ProjectXPlayerContext ctx, Dictionary<string, UnityEngine.Object> spriteDict)
        {
        }

        public enum MenuPanelUIMode
        {
            WithMotherShip,
            WithoutMotherShip
        }
    }
}

