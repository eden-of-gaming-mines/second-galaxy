﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipListUIController : UIControllerBase
    {
        public int m_shipHangarNextUnLocedLevel;
        public int m_shipSelectIndex;
        public List<ILBStaticPlayerShip> m_shipHangarShipInfoList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private string m_shipListUIItemPoolName;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnShipListItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnShipListItemFill;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ShipListScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ShipListUIItemPool;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipHangarInfoList;
        private static DelegateBridge __Hotfix_GetFirstEmptyShipHangarRect;
        private static DelegateBridge __Hotfix_GetShipItemByIndex;
        private static DelegateBridge __Hotfix_SetShipItemScrollToViewBottom;
        private static DelegateBridge __Hotfix_UpdateSingleShipHangerSelected;
        private static DelegateBridge __Hotfix_UpdateSingleShipHangar;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnShipUIItemClick;
        private static DelegateBridge __Hotfix_OnShipUIItemFill;
        private static DelegateBridge __Hotfix_add_EventOnShipListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipListItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipListItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnShipListItemFill;

        public event Action<UIControllerBase> EventOnShipListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnShipListItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstEmptyShipHangarRect()
        {
        }

        [MethodImpl(0x8000)]
        public HangarShipListItemUIController GetShipItemByIndex(int shipIndex, out int totalViewCount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipUIItemClick(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipUIItemFill(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipItemScrollToViewBottom(HangarShipListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipHangarInfoList(List<ILBStaticPlayerShip> shipHangarList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int nextUnLockLevel, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleShipHangar(List<ILBStaticPlayerShip> shipHangarList, int updateSlotIndex, ILBStaticPlayerShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleShipHangerSelected(int selectIndex)
        {
        }
    }
}

