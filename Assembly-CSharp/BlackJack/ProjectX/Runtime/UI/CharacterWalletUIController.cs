﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacterWalletUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<WalletCurrencyType> EventOnCurrencySelect;
        private const string CurrencySelectStateNormal = "Normal";
        private const string CurrencySelectStateChoose = "Choose";
        private const string MainPanelStateShow = "Show";
        private const string MainPanelStateClose = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bindMoneyStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bindMoneyButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup01/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyText;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tradeMoneyStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tradeMoneyButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup02/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyText;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panGalaStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup03", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_panGalaButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup03/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_panGalaText;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildGalaStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup04", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildGalaButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup04/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildGalaText;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup05", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_realMoneyStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup05", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_realMoneyButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup05/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_realMoneyText;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup06", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_creditCurrencyStateCtrl;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup06", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_creditCurrencyButton;
        [AutoBind("./WalletGroup/Scroll View/Viewport/Content/CurrencyGroup06/CurrencyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_creditCurrencyText;
        [AutoBind("./ExplainGroup/Scroll View/Viewport/Content/PublisherTextGroup/PublisherText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_issuerDescText;
        [AutoBind("./ExplainGroup/Scroll View/Viewport/Content/UseTextGroup/UseText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_monetaryUsageDescText;
        [AutoBind("./ExplainGroup/Scroll View/Viewport/Content/AcquisitionTextGroup/AcquisitionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_acquisitionPathDescText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetPanelShowOrCloseProcess;
        private static DelegateBridge __Hotfix_UpdateViewOfWallet;
        private static DelegateBridge __Hotfix_SetSeclectCurrency;
        private static DelegateBridge __Hotfix_OnBindMoneyClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyClick;
        private static DelegateBridge __Hotfix_OnPanGalaClick;
        private static DelegateBridge __Hotfix_OnGuildGalaClick;
        private static DelegateBridge __Hotfix_OnReadMoneyClick;
        private static DelegateBridge __Hotfix_OnCreditCurrencyClick;
        private static DelegateBridge __Hotfix_OnCurrencySelect;
        private static DelegateBridge __Hotfix_add_EventOnCurrencySelect;
        private static DelegateBridge __Hotfix_remove_EventOnCurrencySelect;

        public event Action<WalletCurrencyType> EventOnCurrencySelect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetPanelShowOrCloseProcess(bool isShow, bool immediateComplete, bool allowToRefresh)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindMoneyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreditCurrencyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCurrencySelect(WalletCurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildGalaClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPanGalaClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReadMoneyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeMoneyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSeclectCurrency(WalletCurrencyType type, ConfigDataCurrencyInfo selectedCurrencyInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOfWallet(ulong bindMoney, ulong tradeMoney, ulong panGala, ulong guildGala, ulong realMoney, ulong creditCurrency, WalletCurrencyType type, ConfigDataCurrencyInfo selectedCurrency)
        {
        }

        public enum WalletCurrencyType
        {
            BindMoney,
            TradeMoney,
            PanGala,
            GuildGala,
            RealMoney,
            CreditCurrency
        }
    }
}

