﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ActionPlanManagerUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private int m_selectAcceptQuestInstanceId;
        private object m_selectUnAcceptQuest;
        private int m_selectBranchStoryId;
        private bool m_isRepeatbleUserGuide;
        private bool m_isQuestPanelResLoadEnd;
        private bool m_isActivityResLoadEnd;
        private bool m_isStoryResLoadEnd;
        private bool m_isFactionCreditResLoadEnd;
        private bool m_isBranchStoryResLoadEnd;
        public ActionPlanTabType m_currTabType;
        public QuestListUITask m_questListUITask;
        public ActivityInfoUITask m_exploreUITask;
        public StoryUITask m_storyUITask;
        public BranchStoryUITask m_branchStoryUITask;
        public FactionCreditUITask m_factionCreditUITask;
        public ActionPlanManagerUIController m_mainCtrl;
        public const int m_isToggleClickMask = 0;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamsKeySelectType = "SelectType";
        public const string ParamsKeyQuestInstanceId = "QuestInstanceId";
        public const string ParamsKeyUnaccptQuest = "UnaccptQuest";
        public const string ParamKeyReapeatbleUserGuideForScienceExplore = "ScienceExploreReapeatbleUserGuide";
        public const string ParamsKeyBranchStoryId = "BranchStoryId";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private IUIBackgroundManager m_backgroundManager;
        public const string TaskName = "ActionPlanManagerUITask";
        private const string LockLayerName = "ActionPlan";
        private const string ExploreName = "Explore";
        private const string FactionCreditName = "FactionCredit";
        private const string BranchStoryName = "BranchStory";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        private bool m_neeRresumeWhenStrikeFailed;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartActionPlanUITask;
        private static DelegateBridge __Hotfix_CheckCanOpen;
        private static DelegateBridge __Hotfix_StartActionPlanUITaskWithQuestInstanceId;
        private static DelegateBridge __Hotfix_StartActionPlanUITaskWithUnaccptQuest;
        private static DelegateBridge __Hotfix_SwitchTabTypeForActionPlanManager;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_GetHideAllSubUIImmediateUIProcess;
        private static DelegateBridge __Hotfix_GetQuestPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetExplorePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetStoryPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetBranchStoryPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetFactionCreditPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_PauseActionPlanUITask;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_RequestSwitchTab;
        private static DelegateBridge __Hotfix_RequestSwitchTabImp;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_OnQuestPanelResLoadEnd;
        private static DelegateBridge __Hotfix_OnExploreResLoadEnd;
        private static DelegateBridge __Hotfix_OnStoryResLoadEnd;
        private static DelegateBridge __Hotfix_OnBranchStoryResLoadEnd;
        private static DelegateBridge __Hotfix_OnFactionCreditResLoadEnd;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_SetPipeLineMask;
        private static DelegateBridge __Hotfix_CheckPipeLineMask;
        private static DelegateBridge __Hotfix_IsRandomQuest;
        private static DelegateBridge __Hotfix_OnQuestAcceptEnd;
        private static DelegateBridge __Hotfix_GetHideActionPlanUIProcess;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetworkEvent;
        private static DelegateBridge __Hotfix_IsSameSubPanelStateSwtich;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnActionPlanTabButtonClick;
        private static DelegateBridge __Hotfix_OnUnacceptQuestTab_ImmediateCommunicationButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerActivityInfoNtf;
        private static DelegateBridge __Hotfix_OnActivityVitalityGetRewardAck;
        private static DelegateBridge __Hotfix_OnQuestCancelAck;
        private static DelegateBridge __Hotfix_OnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_OnQuestCompleteAck;
        private static DelegateBridge __Hotfix_OnQuestWaitForCompleteNtf;
        private static DelegateBridge __Hotfix_get_m_lbQuest;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnGoImmediateButtonClick_BranchStory;
        private static DelegateBridge __Hotfix_OnOpenButtonClick_BranchStory;
        private static DelegateBridge __Hotfix_OnBranchStorySubQuestAcceptEnd;
        private static DelegateBridge __Hotfix_OnGoToTask;
        private static DelegateBridge __Hotfix_GotoStarMap_DelegateMineral;
        private static DelegateBridge __Hotfix_GotoStarMap_DelegateFight;
        private static DelegateBridge __Hotfix_GotoStarMap_FreeQuest;
        private static DelegateBridge __Hotfix_GotoStarMap_Wormhole;
        private static DelegateBridge __Hotfix_GotoStarMap_Infect;
        private static DelegateBridge __Hotfix_GotoStarMap_Invade;
        private static DelegateBridge __Hotfix_GotoNpcShop_Purchase;
        private static DelegateBridge __Hotfix_GotoCharacter_Level;
        private static DelegateBridge __Hotfix_GotoCharacter_ChipSet;
        private static DelegateBridge __Hotfix_GotoCharacter_PropertiesPointAdd;
        private static DelegateBridge __Hotfix_GotoCharacter_Driving;
        private static DelegateBridge __Hotfix_GotoCharacter_Skill;
        private static DelegateBridge __Hotfix_GotoCharacter;
        private static DelegateBridge __Hotfix_GotoShipHangar_UnpackShip;
        private static DelegateBridge __Hotfix_GotoShipHangar_ShipAmmoSet;
        private static DelegateBridge __Hotfix_GotoShipHangar_ShipWeaponEquipSlotSet;
        private static DelegateBridge __Hotfix_GotoShipHangar;
        private static DelegateBridge __Hotfix_GotoCaptain_Training;
        private static DelegateBridge __Hotfix_GotoCaptain_Learning;
        private static DelegateBridge __Hotfix_GotoCaptain_CaptainShipUnlockShip;
        private static DelegateBridge __Hotfix_GotoTech_TechUpgrade;
        private static DelegateBridge __Hotfix_GotoCrack_CrackComplete;
        private static DelegateBridge __Hotfix_GoToProduce_ProduceComplete;
        private static DelegateBridge __Hotfix_GotoProduce_ProduceItem;
        private static DelegateBridge __Hotfix_GotoScienceExplore;
        private static DelegateBridge __Hotfix_OnUseItemTaskClose;
        private static DelegateBridge __Hotfix_GoToPersonalTrade;
        private static DelegateBridge __Hotfix_GotoSpaceSignal;
        private static DelegateBridge __Hotfix_GoToFactionQuest;
        private static DelegateBridge __Hotfix_ShowSystemFunctionNotOpenTipWindow;
        private static DelegateBridge __Hotfix_LeaveCurrBackground;
        private static DelegateBridge __Hotfix_EnterUITaskDefult;
        private static DelegateBridge __Hotfix_GetReturnUIIntentWhenEnterOtherTask;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_UnLockExploreUI;
        private static DelegateBridge __Hotfix_UnLockFactionCreditUI;
        private static DelegateBridge __Hotfix_UnLockBranchStoryUI;
        private static DelegateBridge __Hotfix_OnStoryTab_GoImmediateButtonClick;
        private static DelegateBridge __Hotfix_OnStoryTab_LeaveStation;
        private static DelegateBridge __Hotfix_AcceptQuest;
        private static DelegateBridge __Hotfix_OnStoryQuestAcceptEnd;
        private static DelegateBridge __Hotfix_StrikeForAcceptedQuest;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_SelectActionPlanTab;

        [MethodImpl(0x8000)]
        public ActionPlanManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AcceptQuest(UnAcceptedQuestUIInfo unAcceptQuestInfo, Action<bool, int> QuestAcceptEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private static bool CheckCanOpen(ActionPlanTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckPipeLineMask(PipeLineMaskType maskType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void EnterUITaskDefult(System.Type T, string mode = null, bool leaveCurrBackGround = false, string backgroundManagerParamKey = null, bool return2PreTask = false)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetBranchStoryPanelShowOrHideProcess(bool isShow, bool immediate, int branchStoryConfId = 0)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetExplorePanelShowOrHideProcess(bool isShow, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetFactionCreditPanelShowOrHideProcess(bool isShow, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetHideActionPlanUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetHideAllSubUIImmediateUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetQuestPanelShowOrHideProcess(bool isShow, bool immediate, int questInsId = 0, object questInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        private UIIntent GetReturnUIIntentWhenEnterOtherTask(bool return2PreTask = false)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetStoryPanelShowOrHideProcess(bool isShow, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCaptain_CaptainShipUnlockShip(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCaptain_Learning(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCaptain_Training(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter(LBProcessingQuestBase questInfo, bool showEnterBase = false, bool return2PreTask = false)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter_ChipSet(LBProcessingQuestBase questInfo, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter_Driving(LBProcessingQuestBase questInfo, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter_Level()
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter_PropertiesPointAdd(LBProcessingQuestBase questInfo, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCharacter_Skill(LBProcessingQuestBase questInfo, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoCrack_CrackComplete(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToFactionQuest()
        {
        }

        [MethodImpl(0x8000)]
        private void GotoNpcShop_Purchase(LBProcessingQuestBase questInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void GoToPersonalTrade(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToProduce_ProduceComplete(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoProduce_ProduceItem(QuestCompleteCondInfo cond, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoScienceExplore(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoShipHangar(ShipHangarRepeatableUserGuideType guideType, QuestCompleteCondInfo cond, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoShipHangar_ShipAmmoSet(QuestCompleteCondInfo cond, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoShipHangar_ShipWeaponEquipSlotSet(QuestCompleteCondInfo cond, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoShipHangar_UnpackShip(QuestCompleteCondInfo cond, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        protected void GotoSpaceSignal(SceneType sceneType, bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_DelegateFight(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_DelegateMineral(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_FreeQuest(QuestType questType = 0, bool return2PreTask = false)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_Infect(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_Invade(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoStarMap_Wormhole(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoTech_TechUpgrade(bool return2PreTask)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsRandomQuest(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSameSubPanelStateSwtich()
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnActionPlanTabButtonClick(ActionPlanTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnActivityVitalityGetRewardAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBranchStoryResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBranchStorySubQuestAcceptEnd(bool ret, int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnExploreResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionCreditResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoImmediateButtonClick_BranchStory(BranchStoryUITask.BranchStorySubQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoToTask(GotoType type, bool return2PreTask, object param)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenButtonClick_BranchStory(BranchStoryUITask.BranchStoryItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerActivityInfoNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAcceptEnd(bool ret, int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCancelAck(int result, int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompleteAck(int result, int questId, List<QuestRewardInfo> rewardList, bool diableReturnStationNotice)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestPanelResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestWaitForAcceptListAdd(int questId, int questFactionId, int questLevel, int questSolarSystemId, bool fromQuestCancel, LBQuestEnv questEnv)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestWaitForCompleteNtf(int questInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoryQuestAcceptEnd(bool ret, int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoryResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoryTab_GoImmediateButtonClick(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoryTab_LeaveStation(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnacceptQuestTab_ImmediateCommunicationButtonClick(UnAcceptedQuestUIInfo unAcceptedQuestUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUseItemTaskClose(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void PauseActionPlanUITask(Action eventAfterPause = null, bool isIgnoreHideAnimation = false, bool isReapeatableUserGuide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTab(ActionPlanTabType tabType, bool isReapeatableUserGuide, int param, object param2)
        {
        }

        [MethodImpl(0x8000)]
        private void RequestSwitchTabImp(ActionPlanTabType tabType, bool isReapeatableUserGuide, int param, object param2)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequstEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectActionPlanTab(ActionPlanTabType tabType, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineMask(PipeLineMaskType maskType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        protected bool ShowSystemFunctionNotOpenTipWindow(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        public static ActionPlanManagerUITask StartActionPlanUITask(UIIntent returnToIntent, ActionPlanTabType tabType = 0, Action<bool> onEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ActionPlanManagerUITask StartActionPlanUITaskWithQuestInstanceId(UIIntent returnToIntent, int questInstanceId, Action<bool> onEnd, bool repeatableUserGuide = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ActionPlanManagerUITask StartActionPlanUITaskWithUnaccptQuest(UIIntent returnToIntent, object quest, Action<bool> onEnd, bool repeatableUserGuide = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StrikeForAcceptedQuest(LBProcessingQuestBase storyQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static bool SwitchTabTypeForActionPlanManager(ActionPlanTabType tabType, bool isReapeatableUserGuide = false, int param = 0, object param2 = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockBranchStoryUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockExploreUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnLockFactionCreditUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockQuestClient m_lbQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AcceptQuest>c__AnonStorey19
        {
            internal NpcDNId npcDNId;
            internal Action<bool, int> QuestAcceptEnd;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(true);
            }

            internal void <>m__1()
            {
                CommonStrikeUtil.StrikeForCelestial(this.$this, NavigationNodeType.EnterSpaceStation, this.npcDNId.SpaceStationId, this.npcDNId.SolarSystemId, 0, this.$this.m_backgroundManager);
            }

            internal void <>m__2(int result, int questInsId)
            {
                if (result != 0)
                {
                    this.$this.Resume(null, null);
                }
                else
                {
                    bool flag = QuestUtilHelper.TryStartAutoCompleteQuestDialogInStation(questInsId, isDialogShow => this.$this.Resume(null, null));
                    this.QuestAcceptEnd(flag, questInsId);
                }
            }

            internal void <>m__3()
            {
                this.$this.Resume(null, null);
            }

            internal void <>m__4(bool isDialogShow)
            {
                this.$this.Resume(null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeEnterSpaceStationUITask>c__AnonStorey1B
        {
            internal Action onEnd;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
                this.$this.m_neeRresumeWhenStrikeFailed = false;
            }
        }

        [CompilerGenerated]
        private sealed class <GetBranchStoryPanelShowOrHideProcess>c__AnonStorey3
        {
            internal bool immediate;
            internal int branchStoryConfId;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_branchStoryUITask.ShowBranchStoryPanel(this.immediate, this.branchStoryConfId, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_branchStoryUITask.HideBranchStoryPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetExplorePanelShowOrHideProcess>c__AnonStorey1
        {
            internal bool immediate;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_exploreUITask.ShowAcvityPanel(this.immediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_exploreUITask.HideActivityPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetFactionCreditPanelShowOrHideProcess>c__AnonStorey4
        {
            internal bool immediate;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_factionCreditUITask.ShowFactionCreditPanel(this.immediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_factionCreditUITask.HideFactionCreditPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetQuestPanelShowOrHideProcess>c__AnonStorey0
        {
            internal bool immediate;
            internal int questInsId;
            internal object questInfo;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_questListUITask.ShowQuestPanel(this.immediate, this.questInsId, this.questInfo, onEnd, this.$this.m_isRepeatbleUserGuide);
                this.$this.m_isRepeatbleUserGuide = false;
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_questListUITask.HideQuestPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetStoryPanelShowOrHideProcess>c__AnonStorey2
        {
            internal bool immediate;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_storyUITask.ShowStoryPanel(this.immediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_storyUITask.HideStoryPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoCaptain_CaptainShipUnlockShip>c__AnonStorey11
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), "CaptainManagerUITask", null);
                intent.SetParam("IsCaptainUnlockShip", true);
                UIManager.Instance.StartUITask(intent, true, false, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoCaptain_Learning>c__AnonStorey10
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), "CaptainManagerUITask", null);
                UIManager.Instance.StartUITask(intent, true, false, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoCaptain_Training>c__AnonStoreyF
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), "CaptainManagerUITask", null);
                intent.SetParam("IsCaptainLevelUp", true);
                if (UIManager.Instance.StartUITask(intent, true, false, null, null) == null)
                {
                    Debug.LogError("CaptainManagerUITask start fail");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GotoCharacter>c__AnonStoreyD
        {
            internal bool return2PreTask;
            internal LBProcessingQuestBase questInfo;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                CharacterInfoUITask.StartSelfCharInfoUITaskForNotForceGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), this.questInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoCrack_CrackComplete>c__AnonStorey13
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                CrackManagerUITask.StartCrackUITaskWithRepteableGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask));
            }
        }

        [CompilerGenerated]
        private sealed class <GotoNpcShop_Purchase>c__AnonStoreyC
        {
            internal LBProcessingQuestBase questInfo;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                int num = this.questInfo.GetConfInfo().CompleteCondList[0].P1;
                UIIntentCustom intent = new UIIntentCustom("SpaceStationUITask", null);
                intent.SetParam("QuickJump", 1);
                intent.SetParam("ItemType", num);
                SpaceStationUITask task = UIManager.Instance.StartUITask(intent, true, true, null, null) as SpaceStationUITask;
                if (task != null)
                {
                    task.HideMenuPanel();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GoToPersonalTrade>c__AnonStorey17
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                TradeEntryPanelUITask.StartTradeEntryPanelUITask(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), delegate (bool res) {
                    if (!res)
                    {
                        UIManager.Instance.ReturnUITask(this.$this.m_currIntent, null);
                    }
                }, null, true, this.$this.m_backgroundManager);
            }

            internal void <>m__1(bool res)
            {
                if (!res)
                {
                    UIManager.Instance.ReturnUITask(this.$this.m_currIntent, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GotoProduce_ProduceItem>c__AnonStorey14
        {
            internal bool return2PreTask;
            internal QuestCompleteCondInfo cond;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                ProduceUITask.StartProduceUITaskForNotForceGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), (StoreItemType) this.cond.P1, this.cond.P2);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoScienceExplore>c__AnonStorey15
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;
        }

        [CompilerGenerated]
        private sealed class <GotoScienceExplore>c__AnonStorey16
        {
            internal ILBStoreItemClient exploreTicket;
            internal ActionPlanManagerUITask.<GotoScienceExplore>c__AnonStorey15 <>f__ref$21;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.<>f__ref$21.$this.Pause();
                ItemStoreUITask.StartItemStoreUITaskWithItem(this.<>f__ref$21.$this.GetReturnUIIntentWhenEnterOtherTask(this.<>f__ref$21.return2PreTask), this.exploreTicket, null);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoShipHangar>c__AnonStoreyE
        {
            internal bool return2PreTask;
            internal ShipHangarRepeatableUserGuideType guideType;
            internal QuestCompleteCondInfo cond;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), "ShipHangarUITask", ShipHangarUITask.ShipListOverviewMode);
                intent.SetParam(ShipHangarUITask.ParamKey_ShipListRefreshMode, ShipHangarUITask.ShipListRefreshMode_RefreshAll);
                intent.SetParam(ShipHangarUITask.ParameKey_IsInRepeatUserGuide, true);
                intent.SetParam(ShipHangarUITask.ParamKey_RepeatUserGuideType, this.guideType);
                if (this.guideType == ShipHangarRepeatableUserGuideType.AddShip)
                {
                    intent.SetParam(ShipHangarUITask.ParamKey_WantedShipType, (ShipType) this.cond.P1);
                }
                intent.SetParam(ShipHangarUITask.ParamKey_SelectHangarShipIndex, -1);
                if (UIManager.Instance.StartUITask(intent, true, false, null, null) == null)
                {
                    Debug.LogError("ShipHangarUITask start fail");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GotoSpaceSignal>c__AnonStorey18
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                NpcShopMainUITask.OpenNpcShopWithoutDialog(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), StoreItemType.StoreItemType_Equip, 0x5c22, null, this.$this.m_backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_DelegateFight>c__AnonStorey7
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartStarMapUITaskWithDelegateFightGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask));
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_DelegateMineral>c__AnonStorey6
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartDelegateMineralGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask));
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_FreeQuest>c__AnonStorey8
        {
            internal bool return2PreTask;
            internal QuestType questType;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartFreeSingnalQuestGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), this.questType);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_Infect>c__AnonStoreyA
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartInfectMissonRepeatableGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask));
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_Invade>c__AnonStoreyB
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartInvadeSingnalGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask));
            }
        }

        [CompilerGenerated]
        private sealed class <GotoStarMap_Wormhole>c__AnonStorey9
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.LeaveCurrBackground();
                StarMapManagerUITask.StartWormHoleRepeatableGuide(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), true);
            }
        }

        [CompilerGenerated]
        private sealed class <GotoTech_TechUpgrade>c__AnonStorey12
        {
            internal bool return2PreTask;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                TechUpgradeManageUITask.StartTechUpgradeManageUITask(this.$this.GetReturnUIIntentWhenEnterOtherTask(this.return2PreTask), 0, null, backgroundManager, "NormalMode");
            }
        }

        [CompilerGenerated]
        private sealed class <OnStrikeEnd>c__AnonStorey1A
        {
            internal ProjectXPlayerContext ctx;

            internal void <>m__0()
            {
                if (this.ctx.GetLBCharacter().IsInSpace())
                {
                    CommonStrikeUtil.ReturnToSolarSystemUITask();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PauseActionPlanUITask>c__AnonStorey5
        {
            internal Action eventAfterPause;
            internal ActionPlanManagerUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                if (this.eventAfterPause != null)
                {
                    this.eventAfterPause();
                }
            }
        }

        public enum PipeLineMaskType
        {
            TabChanged
        }
    }
}

