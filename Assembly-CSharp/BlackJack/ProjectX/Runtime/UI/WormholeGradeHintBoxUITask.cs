﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class WormholeGradeHintBoxUITask : UITaskBase
    {
        private const string OnGoOnClick = "OnGoOnClick";
        private const string WormholeGradeHintBoxMode = "WormholeGradeHintBox_NormalMode";
        private const string OnReturnClick = "onReturnClick";
        private const string GradeLimit = "GradeLimit";
        private WormholeGradeHintBoxUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "WormholeGradeHintBoxUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateWormholeGrade;
        private static DelegateBridge __Hotfix_GoOnButtonClick;
        private static DelegateBridge __Hotfix_ReturnButtonClick;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public WormholeGradeHintBoxUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void GoOnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isShow = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void Show(string normalInfo, Action onGoOnClick = null, Action onReturnClick = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWormholeGrade()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey0
        {
            internal Action action;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
            }
        }
    }
}

