﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LoginBGTask : UITaskBase
    {
        public const string ParamKeyGrandFacton = "GrandFaction";
        public const string SavedGrandFaction = "SavedGrandFaction";
        private GrandFaction m_grandFaction;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartLoginBgTask;
        private static DelegateBridge __Hotfix_SetGrandFaction;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ShowLoginBgWithGrand;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public LoginBGTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void BringLayerToTop(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGrandFaction(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowLoginBgWithGrand(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static LoginBGTask StartLoginBgTask(Action redirectPipLineOnLoadAllResCompleted, GrandFaction grandFaction = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

