﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DevelopmentApplicationNodeUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ConfigDataDevelopmentProjectInfo <NodeInfo>k__BackingField;
        private ScrollItemBaseUIController m_scrollItemBaseUICtrl;
        [AutoBind("./ExploitDescText/InfoGroup/QualityImage02", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_upperSubRankGo;
        [AutoBind("./ExploitDescText/InfoGroup/TechImage02", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_upperRankGo;
        [AutoBind("./ExploitDescText/InfoGroup/LineImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lineGo;
        [AutoBind("./RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_redPointGo;
        [AutoBind("./ExploitDescText/InfoGroup/TechImage02", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_upperRankImage;
        [AutoBind("./ExploitDescText/InfoGroup/QualityImage02/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_upperSubRankImage;
        [AutoBind("./ExploitDescText/InfoGroup/QualityImage02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_upperSubRankStateCtrl;
        [AutoBind("./ExploitDescText/InfoGroup/TechImage01", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_lowerRankImage;
        [AutoBind("./ExploitDescText/InfoGroup/QualityImage01/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_lowerSubRankImage;
        [AutoBind("./ExploitDescText/InfoGroup/QualityImage01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lowerSubRankStateCtrl;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./PreviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_applyButton;
        [AutoBind("./FunctionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateViewOnNode;
        private static DelegateBridge __Hotfix_get_NodeInfo;
        private static DelegateBridge __Hotfix_set_NodeInfo;
        private static DelegateBridge __Hotfix_get_ItemIndex;

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnNode(ConfigDataDevelopmentProjectInfo nodeInfo, bool nodeAvaliable, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public ConfigDataDevelopmentProjectInfo NodeInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

