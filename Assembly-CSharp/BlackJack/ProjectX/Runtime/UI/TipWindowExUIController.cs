﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class TipWindowExUIController : UIControllerBase
    {
        private List<KeyValuePair<Text, CommonUIStateController>> m_promptList;
        private List<Transform> m_dummyList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateWindow;
        private static DelegateBridge __Hotfix_CloseAlTip;
        private static DelegateBridge __Hotfix_GetPromptItem;
        private static DelegateBridge __Hotfix_HasEmptyItem;

        [MethodImpl(0x8000)]
        public void CloseAlTip()
        {
        }

        [MethodImpl(0x8000)]
        private KeyValuePair<Text, CommonUIStateController> GetPromptItem()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEmptyItem()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWindow(string info, TipWindowExUITask.PromptType promptType)
        {
        }
    }
}

