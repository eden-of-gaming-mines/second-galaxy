﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CrackBGTask : UITaskBase
    {
        public static string ParamKey_CrackBoxResPath;
        public static string ParamKey_IsShowPlatAnimation;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPlatformMoveEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStartCrackEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEmptyToWaitEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCrackBoxOpenAnimationEnd;
        public const string BGTaskMode_NoBox = "NoCrackBox";
        public const string BGTaskMode_Cracking = "Cracking";
        public const string BGTaskMode_Cracked = "Cracked";
        public const string BGTaskMode_WaitForCrack = "WaitForCracked";
        private CrackBGController m_mainCtrl;
        protected string m_currCrackBoxResPath;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateCrackBG;
        private static DelegateBridge __Hotfix_StartOpenBoxAnimation;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_OnStartCrackEnd;
        private static DelegateBridge __Hotfix_OnEmptyToWaitEnd;
        private static DelegateBridge __Hotfix_OnCrackBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_remove_EventOnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_add_EventOnStartCrackEnd;
        private static DelegateBridge __Hotfix_remove_EventOnStartCrackEnd;
        private static DelegateBridge __Hotfix_add_EventOnEmptyToWaitEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEmptyToWaitEnd;
        private static DelegateBridge __Hotfix_add_EventOnCrackBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnCrackBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnCrackBoxOpenAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEmptyToWaitEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPlatformMoveEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartCrackEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CrackBGTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCrackBoxOpenAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEmptyToWaitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlatformMoveEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStartCrackEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void StartOpenBoxAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrackBG(string modelResPath, string taskMode, bool isNew = false, bool isFristCracked = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            BoxModelChanged,
            IsShowAnimation,
            PlatAnimationInit
        }
    }
}

