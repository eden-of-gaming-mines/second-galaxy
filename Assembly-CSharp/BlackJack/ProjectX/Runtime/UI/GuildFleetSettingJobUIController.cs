﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class GuildFleetSettingJobUIController : UIControllerBase
    {
        private const string AssetPath = "JobItemUIPrefab";
        private GuildFleetJobSetItemUIController m_fireControllerItemCtrl;
        private GuildFleetJobSetItemUIController m_navigatorItemCtrl;
        private GuildFleetJobSetItemUIController m_commanderItemCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FleetPosition, bool> EventOnSetInternalJob;
        [AutoBind("./ContentGroup/Commander", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CommanderRectTransform;
        [AutoBind("./ContentGroup/Navigator", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform NavigatorRectTransform;
        [AutoBind("./ContentGroup/FireController", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FireControllerRectTransform;
        [AutoBind("./DetailInfoPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildFleetPozitionDescBackGroundButton;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoPanelCommonUIStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseSetPositionPanelButton;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SetPositionPanelBackGoundButton;
        [AutoBind("./BGImages/TitleText/ParticularButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ParticularButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildFleetSettingJobUIPrefabCommonUIStateController;
        private static DelegateBridge __Hotfix_CreateFleetSettingJobUIProcess;
        private static DelegateBridge __Hotfix_CreateFleetJobDescUIProcess;
        private static DelegateBridge __Hotfix_UpdateSettingJobInfo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnSetToFleetPosition;
        private static DelegateBridge __Hotfix_add_EventOnSetInternalJob;
        private static DelegateBridge __Hotfix_remove_EventOnSetInternalJob;

        public event Action<FleetPosition, bool> EventOnSetInternalJob
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateFleetJobDescUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateFleetSettingJobUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetToFleetPosition(FleetPosition position, bool set)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSettingJobInfo(GuildFleetDetailsUITask.GuildFleetMemberDetailInfo commander, GuildFleetDetailsUITask.GuildFleetMemberDetailInfo navigator, GuildFleetDetailsUITask.GuildFleetMemberDetailInfo fireController, bool isFleetManager, GuildFleetDetailsUITask.GuildFleetMemberDetailInfo selfInfo, GuildFleetDetailsUITask.GuildFleetMemberDetailInfo selectedInfo, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

