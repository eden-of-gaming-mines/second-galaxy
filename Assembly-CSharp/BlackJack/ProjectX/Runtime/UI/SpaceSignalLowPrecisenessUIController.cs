﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public sealed class SpaceSignalLowPrecisenessUIController : UIControllerBase
    {
        private const string StateOpen = "Show";
        private const string StateClose = "Close";
        private SignalInfo m_spaceSignal;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ContentGroup/Dismiss/TextGroup/HintText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_detailText;
        [AutoBind("./ContentGroup/Buttons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        [AutoBind("./ContentGroup/Buttons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        private static DelegateBridge __Hotfix_GetSpaceSignalLowPrecisenessPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdateSpaceSignalLowPrecisenessUI;
        private static DelegateBridge __Hotfix_GetSpaceSignal;

        [MethodImpl(0x8000)]
        public SignalInfo GetSpaceSignal()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSpaceSignalLowPrecisenessPanelUIProcess(bool isShow, bool immediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpaceSignalLowPrecisenessUI(SignalInfo signal)
        {
        }
    }
}

