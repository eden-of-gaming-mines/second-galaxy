﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class UnknownWormholeMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowIn/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        [AutoBind("./Detail/AllowIn/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        private ConfigDataWormholeStarGroupInfo m_unKnowWormholeInfo;
        private CommonUIStateController[] m_allowInShipIconList;
        private static DelegateBridge __Hotfix_SetWormholeStargateSceneInfo;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetUnKnowWormholeStargroupInfo;
        private static DelegateBridge __Hotfix_SetCaptainShipInfoUIItemList;
        private static DelegateBridge __Hotfix_get_AllowInShipIconList;

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeStarGroupInfo GetUnKnowWormholeStargroupInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainShipInfoUIItemList(ConfigDataWormholeStarGroupInfo wormholeStargroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWormholeStargateSceneInfo(ConfigDataWormholeStarGroupInfo wormholeInfo)
        {
        }

        private CommonUIStateController[] AllowInShipIconList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetCaptainShipInfoUIItemList>c__AnonStorey0
        {
            internal int index;

            internal bool <>m__0(ShipType e) => 
                (e == (this.index + 1));
        }
    }
}

