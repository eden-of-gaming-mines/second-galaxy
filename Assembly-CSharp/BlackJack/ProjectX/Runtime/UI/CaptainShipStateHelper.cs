﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CaptainShipStateHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetAllShipStateListWithCaptain;
        private static DelegateBridge __Hotfix_GetShipState;
        private static DelegateBridge __Hotfix_CheckShipState;

        [MethodImpl(0x8000)]
        public static bool CheckShipState(int state, CaptainShipState checkState)
        {
        }

        [MethodImpl(0x8000)]
        public static List<int> GetAllShipStateListWithCaptain(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetShipState(LBStaticHiredCaptain captain, LBStaticHiredCaptain.ShipInfo shipInfo)
        {
        }

        public enum CaptainShipState
        {
            None,
            Locked,
            Malfunction,
            Driving,
            ReadyForActivate,
            CaptainLvNotEnough
        }
    }
}

