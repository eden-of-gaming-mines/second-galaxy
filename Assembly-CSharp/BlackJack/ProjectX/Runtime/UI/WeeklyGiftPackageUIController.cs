﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class WeeklyGiftPackageUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnWeeklyGiftPackageBuyButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestRewardInfo, bool, float> EventOnWeeklyGiftPacakgeRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnNextWeekRefresh;
        private GameObject m_giftPackageTemplateGo;
        private List<LBRechargeGiftPackage> m_giftPackageList;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        [AutoBind("./TemplateItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateItemRoot;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect WeeklyGiftPackageItemScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MainUIStateCtrl;
        [AutoBind("./LeftSimpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftSimpleInfoDummy;
        [AutoBind("./RightSimpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightSimpleInfoDummy;
        private static DelegateBridge __Hotfix_UpdateWeelyGiftPackageList;
        private static DelegateBridge __Hotfix_UpdateWeelyGiftPackageListWithKeepLocation;
        private static DelegateBridge __Hotfix_GetMainPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetGiftListStartIndex;
        private static DelegateBridge __Hotfix_GetItemSimpleDummyLocation;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitGiftPackagePool;
        private static DelegateBridge __Hotfix_OnGiftPackageItemCreated;
        private static DelegateBridge __Hotfix_OnGiftPackageItemFill;
        private static DelegateBridge __Hotfix_OnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_OnGiftPackageItemRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnWeeklyGiftPackageBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWeeklyGiftPackageBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeeklyGiftPacakgeRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnWeeklyGiftPacakgeRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnNextWeekRefresh;
        private static DelegateBridge __Hotfix_remove_EventOnNextWeekRefresh;

        public event Action<int> EventOnNextWeekRefresh
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<QuestRewardInfo, bool, float> EventOnWeeklyGiftPacakgeRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnWeeklyGiftPackageBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetGiftListStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleDummyLocation(bool onLeft)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainPanelShowOrHideProcess(bool isShow, bool immedite = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void InitGiftPackagePool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemBuyButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemRewardItemClick(QuestRewardInfo rewardInfo, int index, float x)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeelyGiftPackageList(List<LBRechargeGiftPackage> giftPackageList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeelyGiftPackageListWithKeepLocation(List<LBRechargeGiftPackage> giftPackageList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

