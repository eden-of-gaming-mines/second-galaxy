﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonItemIconUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private bool m_isBatchMode;
        private bool m_isInHangarUI;
        private bool m_isSelected;
        private CommonItemIconDataType m_itemDataType;
        private object m_itemData;
        private Image[] m_images;
        private static string UIState_UnSelected;
        private static string UIState_ShipUnSelected;
        private static string UIState_BatchUnSelected;
        private static string UIState_Selected;
        private static string UIState_ShipSelected;
        private static string UIState_BatchSelected;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./BlueprintImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BlueprintImage;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CommonItemIconImage;
        [AutoBind("./TopRightGroup/SizeTypeBGImage/SizeTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SizeTypeImage;
        [AutoBind("./TopRightGroup/SlotTypeBGImage/SlotTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SlotTypeImage;
        [AutoBind("./RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RankImage;
        [AutoBind("./SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SubRankImage;
        [AutoBind("./TopRightGroup/Bind", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BindImageGo;
        [AutoBind("./TopRightGroup/Time", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FreezingImageGo;
        [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CountText;
        [AutoBind("./SelectedImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SelectedImage;
        [AutoBind("./Unavailable", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnUnavailable;
        [AutoBind("./SmashState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SmashState;
        [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelText;
        [AutoBind("./ProbabilityImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Probability;
        [AutoBind("./NoDrivingLicenseGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NoDrivingLicense;
        [AutoBind("./LockGrayImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LockGrayImage;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButtonClick;
        [AutoBind("./BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommeondUIStateCtr;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItem3DtouchEvent;
        private static DelegateBridge __Hotfix_UnregisterItem3DTouchEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetCountText;
        private static DelegateBridge __Hotfix_SetCommonItemIconInfo_0;
        private static DelegateBridge __Hotfix_SetCommonItemIconInfoWithZeroCount;
        private static DelegateBridge __Hotfix_SetCommonItemIconInfo_1;
        private static DelegateBridge __Hotfix_SetIconIsSelected;
        private static DelegateBridge __Hotfix_SetData;
        private static DelegateBridge __Hotfix_SetToGrayMode;
        private static DelegateBridge __Hotfix_SetToDefaultMode;
        private static DelegateBridge __Hotfix_SetToBluePrintMode;
        private static DelegateBridge __Hotfix_SetToFeatsBooksMode;
        private static DelegateBridge __Hotfix_SetItemIsAvailable;
        private static DelegateBridge __Hotfix_SetShipDestroyedState;
        private static DelegateBridge __Hotfix_SetLowProbabilityState;
        private static DelegateBridge __Hotfix_SetNoDrvingLisceneState;
        private static DelegateBridge __Hotfix_SetRecommendItemFlag;
        private static DelegateBridge __Hotfix_GetCurUIState;
        private static DelegateBridge __Hotfix_GetIconUnSelected;
        private static DelegateBridge __Hotfix_GetIconSelected;
        private static DelegateBridge __Hotfix_GetIconInHangarUI;
        private static DelegateBridge __Hotfix_SetIconSprite;
        private static DelegateBridge __Hotfix_get_ItemDataType;
        private static DelegateBridge __Hotfix_set_ItemDataType;
        private static DelegateBridge __Hotfix_get_ItemData;
        private static DelegateBridge __Hotfix_set_ItemData;

        [MethodImpl(0x8000)]
        private string GetCurUIState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetIconInHangarUI()
        {
        }

        [MethodImpl(0x8000)]
        private string GetIconSelected()
        {
        }

        [MethodImpl(0x8000)]
        private string GetIconUnSelected()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareItemClick)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItem3DtouchEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommonItemIconInfo(Sprite icon, bool isBind, bool isFreezing, int count, Sprite sizeType = null, Sprite rank = null, Sprite subRankType = null, Sprite slot = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommonItemIconInfo(Sprite icon, bool isBind, bool isFreezing, string countStr = "", Sprite sizeType = null, Sprite rank = null, Sprite subRank = null, Sprite slot = null, bool showLowProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommonItemIconInfoWithZeroCount(Sprite icon, bool isBind, bool isFreezing, long count, Sprite sizeType = null, Sprite rank = null, Sprite subRankType = null, Sprite slot = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCountText(string countStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetData(CommonItemIconDataType dataType, object data)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconIsSelected(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void SetIconSprite(Image image, Sprite sprite, bool isNeedLogError = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIsAvailable(bool isAvailable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLowProbabilityState(bool isLowProbability)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNoDrvingLisceneState(bool showNoDrivingLisceneState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecommendItemFlag(bool val)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipDestroyedState(bool isFailed)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToBluePrintMode(bool locked = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToDefaultMode()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToFeatsBooksMode(ConfigDataNpcCaptainFeatsBookInfo configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToGrayMode()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItem3DTouchEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        public CommonItemIconDataType ItemDataType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public object ItemData
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public enum CommonItemIconDataType
        {
            ItemInfo
        }
    }
}

