﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CharacterSkillDetailInfoUITask : UITaskBase
    {
        public const string CharacterSkillDetailInfoUITaskMode_SkillLevelUp = "CharacterSkillDetailInfoUITaskMode_SkillLevelUp";
        public const string CharacterSkillDetailInfoUITaskMode_SkillFullLevel = "CharacterSkillDetailInfoUITaskMode_SkillFullLevel";
        public const string CharacterSkillDetailInfoUITaskMode_SkillNotReadyForLearning = "CharacterSkillDetailInfoUITaskMode_SkillNotReadyForLearning";
        private CharacterSkillDetailInfoUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private bool m_isTaskInit;
        private bool m_isPlayAnim;
        private ConfigDataPassiveSkillInfo m_characterPassiveSkill;
        private int m_characterPassiveSkillLevel;
        private int m_skillModifyPropertyId;
        private float m_currSkillLevelPropertyModify;
        private float m_nextSkillLevelPropertyModify;
        private List<ConfIdLevelInfo> m_prevSkillInfoList;
        private PropertyCategory m_needPropertyCategory;
        private int m_needPropertyPoint;
        private int m_currCharacterPropertyPoint;
        private ConfigDataPassiveSkillLevelInfo m_nextLevelSkillLevelInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLearningSkillChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBindMoneyButtonClick;
        private const string ParamKey_PlayAnim = "PlayAnim";
        public const string TaskName = "CharacterSkillDetailInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCharacterSkillDetailInfoUITask;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_ShowSkillDetailPanel;
        private static DelegateBridge __Hotfix_HideSkillDetailPanel;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_ClearBeforeUpdate;
        private static DelegateBridge __Hotfix_UpdateDataCache_SkillNotFullLevel;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_SkillInfoForNotFull;
        private static DelegateBridge __Hotfix_UpdateView_SkillInfoForFullLevel;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnTipsButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnPreSkillTipGoButtonClick;
        private static DelegateBridge __Hotfix_OnPropertyConditionGoButtonClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnEffectDetailPanelBgClick;
        private static DelegateBridge __Hotfix_OnBindMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnLearnButtonClick;
        private static DelegateBridge __Hotfix_GetDestModeAndSkill;
        private static DelegateBridge __Hotfix_get_IsHaveEnoughMoney;
        private static DelegateBridge __Hotfix_get_IsHaveEnoughLevel;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_OnCharacterAddPointPanelClose;
        private static DelegateBridge __Hotfix_GetNextSkillDetailInfoModeAfterSkillLearning;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbCharBasicProperties;
        private static DelegateBridge __Hotfix_get_LBSkillClient;
        private static DelegateBridge __Hotfix_add_EventOnLearningSkillChanged;
        private static DelegateBridge __Hotfix_remove_EventOnLearningSkillChanged;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBindMoneyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBindMoneyButtonClick;

        public event Action EventOnBindMoneyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLearningSkillChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CharacterSkillDetailInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private static string GetDestModeAndSkill(ref ConfigDataPassiveSkillInfo skillInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetNextSkillDetailInfoModeAfterSkillLearning()
        {
        }

        [MethodImpl(0x8000)]
        public void HideSkillDetailPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterAddPointPanelClose(CharacterPropertyAddUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEffectDetailPanelBgClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLearnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreSkillTipGoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPropertyConditionGoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipsButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSkillDetailPanel(ConfigDataPassiveSkillInfo skillInfo = null, bool isImmediate = false, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterSkillDetailInfoUITask StartCharacterSkillDetailInfoUITask(ConfigDataPassiveSkillInfo skillInfo, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_ClearBeforeUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SkillNotFullLevel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SkillInfoForFullLevel(Sprite iconSprite, int skillGrade)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SkillInfoForNotFull(Sprite iconSprite, bool isCanUpgrade, int skillGrade)
        {
        }

        private bool IsHaveEnoughMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private bool IsHaveEnoughLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterBasicPropertiesBase LbCharBasicProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterSkillClient LBSkillClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideSkillDetailPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal CharacterSkillDetailInfoUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess process, bool result)
            {
            }
        }
    }
}

