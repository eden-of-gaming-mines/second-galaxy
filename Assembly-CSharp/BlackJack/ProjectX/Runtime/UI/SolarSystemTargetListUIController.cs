﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemTargetListUIController : UIControllerBase
    {
        private List<SolarSystemUITask.TargetItemUIInfo> m_targetItemInfoListCache;
        private List<GameObject> m_inContentGameObjects;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private int m_currNoFriendTargetListItemCount;
        private int m_currFriendTargetListItemCount;
        private int m_currBuildingAndDropBoxTargetCount;
        private string m_listItemAssetName;
        private string m_targetListItemPoolName;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnTargetItemUIClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SolarSystemTargetItemUIController> EventOnTargetItemThreeDTouch;
        private const string SortButtonState_Up = "SortUp";
        private const string SortButtonState_Down = "SortDown";
        private const int ScrollViewShowCount = 4;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetListUIState;
        [AutoBind("./TargetListTogglePanel/Toggles/NoFriendlyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle NoFriendlyTabToggle;
        [AutoBind("./TargetListTogglePanel/Toggles/FriendlyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle FriendlyTabToggle;
        [AutoBind("./TargetListTogglePanel/Toggles/CelestialAndBuildingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle BuildingAndDropBoxTabToggle;
        [AutoBind("./TargetListTogglePanel/Toggles/NoFriendlyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NoFriendlyTabToggleEffectCtrl;
        [AutoBind("./TargetListTogglePanel/Toggles/FriendlyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FriendlyTabToggleEffectCtrl;
        [AutoBind("./TargetListTogglePanel/Toggles/CelestialAndBuildingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuildingAndDropBoxTabToggleEffectCtrl;
        [AutoBind("./TargetListTogglePanel/Toggles/NoFriendlyToggle/NumberImage/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NoFriendlyTargetNumText;
        [AutoBind("./TargetListTogglePanel/Toggles/FriendlyToggle/NumberImage/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FriendlyTargetNumText;
        [AutoBind("./TargetListTogglePanel/Toggles/CelestialAndBuildingToggle/NumberImage/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingAndDropBoxTargetNumText;
        [AutoBindWithParent("../CanvasMask/TargetListPanel/TargetListBlockImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TargetListBlockObject;
        [AutoBindWithParent("../CanvasMask/TargetListPanel/TargetListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect TargetListScrollView;
        [AutoBindWithParent("../CanvasMask/TargetListPanel/TargetListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TargetListScrollViewContentRectRct;
        [AutoBindWithParent("../CanvasMask/TargetListPanel/TargetListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool TargetListItemPool;
        [AutoBindWithParent("../CanvasMask/TargetListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TargetListMaskObject;
        [AutoBindWithParent("../SortList/SortListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortListButton;
        [AutoBindWithParent("../SortList/SortListButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortListButtonStateCtrl;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortListButtonMenuCtrl;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortOrFilterPanelSwitchCtrl;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortButton;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortButtonStateCtrl;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FilterButton;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FilterButtonStateCtrl;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/SortGroup/DistanceGate", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SortByDistanceToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/SortGroup/NameGate", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SortByNameToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/SortGroup/HpGate", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SortByRestHpToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/AllianceMember", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowAllianceMemberToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/NeutralListFriend", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowNeutralList_FriendToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/NeutralListNeutral", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowNeutralList_NeutralToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/StargateAndPortal", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowStargateAndPortalToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/NoFriendlyWingShip", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowNoFriendlyWingShipToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/FleetMember", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowFleetMemberToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/NoFleetMember", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowNoFleetMemberToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/FriendlyWingShip", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowFriendlyWingShipToggle;
        [AutoBindWithParent("../SortList/SortListButton/SortPanel/BGImage/SortButtonPanel/FilterGroup/Celestial", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ShowCelestialToggle;
        [AutoBindWithParent("./LocationImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_locationGo;
        [AutoBindWithParent("./LocationImage/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_locationText;
        [AutoBindWithParent("./LocationImage/PeopleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_peopelText;
        [AutoBindWithParent("./LocationImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_locationButton;
        [AutoBindWithParent("./LocationImage/GoHomeIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_returnButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_EnableTargetListUIPanel;
        private static DelegateBridge __Hotfix_ShowTargetListUnlockEffect;
        private static DelegateBridge __Hotfix_CreateTargetListUnlockEffectProcess;
        private static DelegateBridge __Hotfix_SwitchSortListMenuState;
        private static DelegateBridge __Hotfix_SwitchSortOrFilterState;
        private static DelegateBridge __Hotfix_OpenSortListMenu;
        private static DelegateBridge __Hotfix_CloseSortListMenu;
        private static DelegateBridge __Hotfix_UpdateTargetItemListUI;
        private static DelegateBridge __Hotfix_UpdateTargetCountInfoToUI;
        private static DelegateBridge __Hotfix_UpdateTargetListTableState;
        private static DelegateBridge __Hotfix_InitSortToggleSelectedState;
        private static DelegateBridge __Hotfix_SetNoFriendlyToggleShinkState;
        private static DelegateBridge __Hotfix_SetFriendlyToggleShinkState;
        private static DelegateBridge __Hotfix_SetBuildingsAndDropBoxToggleShinkState;
        private static DelegateBridge __Hotfix_GetTargetListScrollVelocity;
        private static DelegateBridge __Hotfix_GetFirstTargetUICtrlWithTargetNotice;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetFirstTargetFromCurrTargetList;
        private static DelegateBridge __Hotfix_UpdateSolarSystemInformation;
        private static DelegateBridge __Hotfix_UpdateSolarSystemPlayerCount;
        private static DelegateBridge __Hotfix_GetWormholeBackPoinTransform;
        private static DelegateBridge __Hotfix_GetWormholeTunnelTransform;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnTargetItemUIClick;
        private static DelegateBridge __Hotfix_OnTargetItemThreeDTouch;
        private static DelegateBridge __Hotfix_OnTargetItemUIFill;
        private static DelegateBridge __Hotfix_add_EventOnTargetItemUIClick;
        private static DelegateBridge __Hotfix_remove_EventOnTargetItemUIClick;
        private static DelegateBridge __Hotfix_add_EventOnTargetItemThreeDTouch;
        private static DelegateBridge __Hotfix_remove_EventOnTargetItemThreeDTouch;

        public event Action<SolarSystemTargetItemUIController> EventOnTargetItemThreeDTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnTargetItemUIClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseSortListMenu()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateTargetListUnlockEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableTargetListUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstTargetFromCurrTargetList()
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTargetItemUIController GetFirstTargetUICtrlWithTargetNotice()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetTargetListScrollVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetWormholeBackPoinTransform()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetWormholeTunnelTransform()
        {
        }

        [MethodImpl(0x8000)]
        public void InitSortToggleSelectedState(SolarSystemUITask.TargetListSortState sortState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject targetItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetItemThreeDTouch(SolarSystemTargetItemUIController tgtCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetItemUIClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetItemUIFill(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenSortListMenu()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuildingsAndDropBoxToggleShinkState(bool isShink)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFriendlyToggleShinkState(bool isShink)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNoFriendlyToggleShinkState(bool isShink)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowTargetListUnlockEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchSortListMenuState()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchSortOrFilterState(bool isSort)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemInformation(bool active, string solarSystemName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemPlayerCount(int playerCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetCountInfoToUI(int noFriendlyTargetCount, int friendlyTargetCount, int buildingAndDropBoxTargetCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetItemListUI(List<SolarSystemUITask.TargetItemUIInfo> itemInfoList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isNeedReFillToStart, bool isNeedSort)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetListTableState(SolarSystemUITask.TargetListTableState currentTargetListTableState)
        {
        }
    }
}

