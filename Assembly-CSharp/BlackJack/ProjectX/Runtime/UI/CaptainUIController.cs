﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainUIController : UIControllerBase
    {
        private const string CaptainFeatItemName = "CaptainFeatItem";
        private const string CaptainShipIconItemName = "CaptainShipItem";
        private bool m_isLearn;
        private CommonItemIconUIController m_captainShipIconCtrl;
        private List<CaptainFeatInfoItemUIController> m_initFeatUICtrlList;
        private List<CaptainFeatInfoItemUIController> m_additionFeatUICtrlList;
        private SystemDescriptionController m_descriptionCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCaptainFeatItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnFeatBookStoreItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnCaptainShipButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, CommonItemIconUIController> EventOnFeatBookStoreItemFilled;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, WingManItemButtonState> EventOnWingManItemClick;
        public CaptainFeatInfoPanelUIController m_featInfoPanelCtrl;
        public CaptainFeatInfoPanelUIController m_preFeatInfoPanelCtrl;
        public ItemStoreListUIController m_featsBookListUICtrl;
        public List<CaptainWingManInfoUIController> m_wingManItemCtrlList;
        public CommonItemIconUIController m_featBookInfoBookItemCtrl;
        public const string ButtonState_Enable = "Normal";
        public const string ButtonState_Disable = "Gray";
        public const string ShipStateNormal = "Close";
        public const string ShipStateUpgrade = "Upgrade";
        public const string ShipStateMarCurr = "CurrShipMar";
        public const string ShipStateMarNotCurr = "NotCurrShipMar";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CaptainIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainIcon;
        [AutoBind("./CaptainIcon", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainIconStateCtrl;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./CaptainShowPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainShowPanel;
        [AutoBind("./CaptainShowPanel/ShipShowButton/ShipInfo/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform CaptainShipIconDummy;
        [AutoBind("./CaptainShowPanel/ShipShowButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipShowButton;
        [AutoBind("./CaptainShowPanel/ShipShowButton/ShipFactionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipFactionIcon;
        [AutoBind("./CaptainShowPanel/ShipShowButton/ShipInfo/ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipNameText;
        [AutoBind("./CaptainShowPanel/ShipShowButton/ShipInfo/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipTypeText;
        [AutoBind("./CaptainShowPanel/CaptainSkill/TalentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainInitFeatGroup;
        [AutoBind("./CaptainShowPanel/CaptainSkill/SpecialityGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainAdditionFeatGroup;
        [AutoBind("./FeatTipsPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatTipsPanelGroupCtrl;
        [AutoBind("./FeatTipsPanelGroup/FeatTipsPanelDummy1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainFeatInfoPanelDummy;
        [AutoBind("./FeatTipsPanelGroup/FeatTipsPanelDummy2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainPreFeatInfoPanelDummy;
        [AutoBind("./Buttons", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ButtonPanel;
        [AutoBind("./Buttons/TrainingButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TrainingButton;
        [AutoBind("./Buttons/WingManButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WingManButton;
        [AutoBind("./Buttons/WingManButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingManButtonCtrl;
        [AutoBind("./Buttons/RetireButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RetireButton;
        [AutoBind("./Buttons/LearnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LearnButton;
        [AutoBind("./Buttons/LearnButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LearnButtonCtrl;
        [AutoBind("./Buttons/ShipStateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipStateButton;
        [AutoBind("./Buttons/ShipStateButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipStateButtonStateCtrl;
        [AutoBind("./BasicStatePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BasicStatePanel;
        [AutoBind("./BasicStatePanel/CaptainInfo/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./BasicStatePanel/CaptainInfo/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainLvNumberText;
        [AutoBind("./BasicStatePanel/CaptainInfo/EvaluateNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EvaluateValueText;
        [AutoBind("./BasicStatePanel/CaptainInfo/ProfessorGroup/ProfessorIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainProfessionIcon;
        [AutoBind("./BasicStatePanel/CaptainInfo/ProfessorGroup/ProfeserText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainProfessionText;
        [AutoBind("./BasicStatePanel/CaptainInfo/QualityText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainQualityText;
        [AutoBind("./BasicStatePanel/CaptainInfo/StateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainStateText;
        [AutoBind("./BasicStatePanel/CaptainInfo/WingState/IndexText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainWingIndexText;
        [AutoBind("./BasicStatePanel/BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle CaptainDetailInfoPanelToggle;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/FactionGroup/FactionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainFactionIcon;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/FactionGroup/FactionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainFactionText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/GenderText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainGenderText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/AgeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainAgeText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/BirthdayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainBirthdayText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/ConstellationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainConstellationText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/WeaponValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainWeaponValueText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/DriveValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainDriveValueText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/DefenseValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainDefenseValueText;
        [AutoBind("./BasicStatePanel/CaptainDetailPanel/Details/ElectronicValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainElectronicValueText;
        [AutoBind("./LearnFeatsPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LearnFeatsPanel;
        [AutoBind("./LearnFeatsPanel/FeatsBookStore/ItemStoreListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatsBookStoreListDummy;
        [AutoBind("./LearnFeatsPanel/FeatsBookStore/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button FeatsBookStoreCloseButton;
        [AutoBind("./LearnFeatsPanel/FeatsBookStore", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatsBookStorePanelState;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatsBookInfoStateCtrl;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/BookItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FeatBookDummy;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/NumTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatsBookCountText;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatsBookInfoNameText;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/LvTextGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatsBookInfoLevelText;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/FeatPropretyGroup/BuffForPlayer", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BuffForPlayerGameObject;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/FeatPropretyGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatsBookPropertyItemGroup;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/CanLearnOrNot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatsBookLearnStateCtrl;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/CanLearnOrNot/LearnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FeatsBookLearnButton;
        [AutoBind("./LearnFeatsPanel/FeatsBookInfo/CanLearnOrNot/LearnButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatsBookLearnBtnStateCtrl;
        [AutoBind("./SetWingManPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingManPanelCtrl;
        [AutoBind("./SetWingManPanel/WingmanGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WingManGroup;
        private const string StateNormal = "Normal";
        private const string StateEmpty = "EmptyPanel";
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetSecondWingManRect;
        private static DelegateBridge __Hotfix_SetCaptainInfo;
        private static DelegateBridge __Hotfix_SetToInitState;
        private static DelegateBridge __Hotfix_SetToTrainingState;
        private static DelegateBridge __Hotfix_SetToLearnFeatState;
        private static DelegateBridge __Hotfix_UpdateItemStoreList;
        private static DelegateBridge __Hotfix_ShowCaptainFeatInfo;
        private static DelegateBridge __Hotfix_ShowCaptainLearnFeatsResult;
        private static DelegateBridge __Hotfix_HideCaptainFeatInfoPanel;
        private static DelegateBridge __Hotfix_EnableLearnFeatButton;
        private static DelegateBridge __Hotfix_ShowFeatsBookInfo;
        private static DelegateBridge __Hotfix_SetFeatsBookPropertyInfo;
        private static DelegateBridge __Hotfix_HideFeatBookInfo;
        private static DelegateBridge __Hotfix_ShowCatpainDetailInfo;
        private static DelegateBridge __Hotfix_ShowCaptainBasicInfo;
        private static DelegateBridge __Hotfix_SetToWingManSettingState;
        private static DelegateBridge __Hotfix_UpdateWingManPanelInfo;
        private static DelegateBridge __Hotfix_EnableWingManSetButton;
        private static DelegateBridge __Hotfix_SetCurrSystemFuncDescType;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreateLearnFeatWindowProcess;
        private static DelegateBridge __Hotfix_InitCaptainFeatSelectionState;
        private static DelegateBridge __Hotfix_OnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_CreateFeatInfoPanel;
        private static DelegateBridge __Hotfix_OnFeatsBookItemClick;
        private static DelegateBridge __Hotfix_OnFeatsBookItemFilled;
        private static DelegateBridge __Hotfix_OnWingManItemButtonClick;
        private static DelegateBridge __Hotfix_OnShipButtonClick;
        private static DelegateBridge __Hotfix_get_IsLearn;
        private static DelegateBridge __Hotfix_add_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_add_EventOnFeatBookStoreItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnFeatBookStoreItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnFeatBookStoreItemFilled;
        private static DelegateBridge __Hotfix_remove_EventOnFeatBookStoreItemFilled;
        private static DelegateBridge __Hotfix_add_EventOnWingManItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnWingManItemClick;
        private static DelegateBridge __Hotfix_get_FeatInfoPanelCtrl;
        private static DelegateBridge __Hotfix_get_PreFeatInfoPanelCtrl;

        public event Action<int> EventOnCaptainFeatItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCaptainShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnFeatBookStoreItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, CommonItemIconUIController> EventOnFeatBookStoreItemFilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, WingManItemButtonState> EventOnWingManItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private CaptainFeatInfoPanelUIController CreateFeatInfoPanel(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateLearnFeatWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableLearnFeatButton(bool enabled)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableWingManSetButton(bool enabled)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSecondWingManRect()
        {
        }

        [MethodImpl(0x8000)]
        public void HideCaptainFeatInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void HideFeatBookInfo(Action endAction = null)
        {
        }

        [MethodImpl(0x8000)]
        private void InitCaptainFeatSelectionState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainFeatItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFeatsBookItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFeatsBookItemFilled(ILBStoreItemClient item, CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWingManItemButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainInfo(LBStaticHiredCaptain lbCaptain, Dictionary<string, UnityEngine.Object> resDict, bool selCapChanged = true, int wingmanIdx = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrSystemFuncDescType(SystemFuncDescType funcDescType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFeatsBookPropertyInfo(GameObject propertyItem, string propName, string propValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToInitState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToLearnFeatState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToTrainingState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToWingManSettingState()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainBasicInfo(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainFeatInfo(LBNpcCaptainFeats featInfo, bool isInitFeat, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainLearnFeatsResult(LBStaticHiredCaptain lbCaptain, LBNpcCaptainFeats newFeat, LBNpcCaptainFeats preFeat, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCatpainDetailInfo(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowFeatsBookInfo(LBStaticHiredCaptain captain, ILBStoreItemClient featsBookItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWingManPanelInfo(List<LBStaticHiredCaptain> wingManList, LBStaticHiredCaptain currCaptain, Dictionary<string, UnityEngine.Object> resDict, int selIndex = -1)
        {
        }

        public bool IsLearn
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CaptainFeatInfoPanelUIController FeatInfoPanelCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CaptainFeatInfoPanelUIController PreFeatInfoPanelCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

