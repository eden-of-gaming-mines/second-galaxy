﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildCompensationUITask : GuildUITaskBase
    {
        private ShipType m_curShipType;
        private List<LossCompensation> m_lossCompensationList;
        private KillRecordInfo m_killRecordInfo;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GuildCompensationUIController m_guildCompensationUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildCompensationUITask";
        public const string CompensationList = "CompensationList";
        public const string CompensationDetail = "CompensationDetail";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RequestCompensationList;
        private static DelegateBridge __Hotfix_OnAllShipBtnClick;
        private static DelegateBridge __Hotfix_OnBattleButtonClick;
        private static DelegateBridge __Hotfix_OnBattleCruiserButtonClick;
        private static DelegateBridge __Hotfix_OnCruiserButtonClick;
        private static DelegateBridge __Hotfix_OnDestroyerButtonClick;
        private static DelegateBridge __Hotfix_OnFrigateButtonClick;
        private static DelegateBridge __Hotfix_OnShipFilterChange;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCompensationItemClick;
        private static DelegateBridge __Hotfix_OnCurrencyAddBtnClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShipItemCompare;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildCompensationUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllShipBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleCruiserButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(CommonItemIconUIController commonItemIconCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompensationItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCruiserButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCurrencyAddBtnClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroyerButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFrigateButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipFilterChange(ShipType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void RequestCompensationList(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private int ShipItemCompare(LossCompensation lhs, LossCompensation rhs)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(FakeLBStoreItem itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent preIntent, Action<bool> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnCompensationItemClick>c__AnonStorey2
        {
            internal UIControllerBase ctrl;
            internal GuildCompensationUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (r)
                {
                    int itemIndex = this.ctrl.GetComponent<GuildCompensationShipItemUIController>().GetItemIndex();
                    this.$this.Pause();
                    GuildCompensationDetailUITask.StartTask(this.$this.m_lossCompensationList[itemIndex].m_InstanceId, this.$this.CurrentIntent);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnCurrencyAddBtnClick>c__AnonStorey3
        {
            internal CurrencyType type;
            internal GuildCompensationUITask $this;

            internal void <>m__0(UIProcess pre, bool result)
            {
                CommonUITaskHelper.ToAddCurrency(this.type, this.$this.CurrentIntent, delegate (bool ret) {
                    if (ret)
                    {
                        this.$this.Pause();
                    }
                });
            }

            internal void <>m__1(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal bool occupiedInfoReqFinished;
            internal bool compensationListReqFinished;

            internal void <>m__0(Task task)
            {
                GuildCompensationListNetTask task2 = task as GuildCompensationListNetTask;
                if (task2 == null)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else if (task2.Ack.Result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Ack.Result, true, false);
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else if (this.onPrepareEnd != null)
                {
                    if (this.occupiedInfoReqFinished)
                    {
                        this.onPrepareEnd(true);
                    }
                    else
                    {
                        this.compensationListReqFinished = true;
                    }
                }
            }

            internal void <>m__1(Task occupiedTask)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task = occupiedTask as GuildOccupiedSolarSystemInfoReqNetTask;
                if (task == null)
                {
                    Debug.LogError("occuppiedRetTask == null");
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else if (task.m_result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task.m_result, true, false);
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else if (this.onPrepareEnd != null)
                {
                    if (this.compensationListReqFinished)
                    {
                        this.onPrepareEnd(true);
                    }
                    else
                    {
                        this.occupiedInfoReqFinished = true;
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RequestCompensationList>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildCompensationListNetTask task2 = task as GuildCompensationListNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Ack.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Ack.Result, true, false);
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
            }
        }

        public enum PipelineMask
        {
            Resume,
            ChangeShipFilter
        }
    }
}

