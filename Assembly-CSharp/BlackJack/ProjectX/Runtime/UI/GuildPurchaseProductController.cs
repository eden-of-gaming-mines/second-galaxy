﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildPurchaseProductController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ProdId>k__BackingField;
        private const string CheckedYes = "YES";
        private const string CheckedNo = "NO";
        private const string Normal = "Normal";
        private const string Unpublished = "Unpublished";
        private const string Check = "Check";
        private long m_has;
        private long m_need;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemIconClick;
        public ScrollItemBaseUIController m_scrollCtrl;
        public CommonItemIconUIController m_itemIconUICtrl;
        [AutoBind("./CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummy;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_name;
        [AutoBind("./GuildHasTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_guildHasTitle;
        [AutoBind("./GuildHasNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_guildHasCount;
        [AutoBind("./OwnTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_hasTitle;
        [AutoBind("./OwnNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_hasCount;
        [AutoBind("./ResidueNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_needCount;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_statCtrl;
        [AutoBind("./CheckGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_statCheckCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_UpdateWithProductInfo;
        private static DelegateBridge __Hotfix_SetDisplay;
        private static DelegateBridge __Hotfix_get_ProdId;
        private static DelegateBridge __Hotfix_set_ProdId;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_SetCount;
        private static DelegateBridge __Hotfix_SetChecked;
        private static DelegateBridge __Hotfix_UnsetChecked;
        private static DelegateBridge __Hotfix_add_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemIconClick;

        public event Action<UIControllerBase> EventOnItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChecked(bool isChecked)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCount(long has, long need, bool guildHas)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDisplay(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnsetChecked()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWithProductInfo(ConfigDataAuctionItemInfo prodInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public int ProdId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

