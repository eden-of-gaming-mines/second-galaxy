﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class GuildFlagShipHangarUITask : GuildUITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private GuildFlagShipHangarShipSupplementFuelUIContoller m_guildFlagShipHangarShipSupplementFuelUIContoller;
        private HangarShipWeaponEquipDetailInfoPanelUIController m_weaponEquipDetailInfoUICtrl;
        private GuildFlagShipHangarBGUITask m_bgTask;
        private CommonWeaponEquipSimpleInfoUITask m_weaponEquipSimpleInfoTask;
        private GuildFlagShipHangarUIController m_guildFlagShipHangarUIController;
        private GuildFlagShipDetailPanelUIController m_guildFlagShipDetailPanelUIController;
        private GuildFlagShipHangarShipListUIController m_guildFlagShipHangarShipListUIController;
        private HangarShipWeaponEquipSlotPanelUIController m_weaponEquipSlotPanelUICtrl;
        private HangarShipInfoPanelUIController m_shipInfoPanelUICtrl;
        private HangarShipLowSlotGroupAssemblyUIController m_lowSlotGroupAssemblyUICtrl;
        private SceneLayerBase m_weaponEquipDetailInfoLayer;
        private SceneLayerBase m_supplementFuelLayer;
        private SceneLayerBase m_shipHangarListLayer;
        private IGuildFlagShipHangarDataContainer m_currFlagShipHangar;
        private List<ILBStaticFlagShip> m_currFlagShipHangarShipList;
        private List<IStaticFlagShipDataContainer> m_currFlagShipDestoryedShipListIncludeDetroyed;
        private int m_currSelectFlagShipListIndex;
        private int m_flagShipFuelId;
        private bool m_isGuildFlagShipHangarBGUITaskResourceLoadComplete;
        private TipWndType m_currTipWndType;
        private ShipEquipSlotType m_currSlotEditType;
        private int m_currSelectSlotIndex;
        private bool m_isShowLowSlotGroupPanel;
        private string m_lastMode;
        private bool m_isLongPressReleased;
        public const string ParamKeySelectHangarShipIndex = "SelectHangarShipIndex";
        public const string ParamKeyShipListRefreshMode = "ShipListRefreshMode";
        public const string ParamKeyIsShipDataChanged = "IsShipDataChanged";
        public const string ParamKeySolarSystemId = "SolarSystemId";
        public const string ParamKeyNeedRequsetShipHangarInfo = "NeedRequsetShipHangarInfo";
        public const string ParamKeyNeedRequsetGuildStoreInfo = "NeedRequsetGuildStoreInfo";
        public const string ShipListRefreshModeRefreshAll = "RefreshAll";
        public const string ShipListRefreshModeRefreshSingle = "RefreshSingle";
        public const string ShipListOverviewMode = "ShipListOverviewMode";
        public const string SupplementFuelMode = "SupplementFuelMode";
        public const string WeaponEquipDetailInfoMode = "WeaponEquipDetailInfoMode";
        public const string HangarListMode = "HangarListMode";
        public const string TaskName = "GuildFlagShipHangarUITask";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private GuildFlagShipHangarListUIController m_guildFlagShipHangarListUIController;
        private List<IGuildFlagShipHangarDataContainer> m_flagShipHangarList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitSupplementFuelUICtrl;
        private static DelegateBridge __Hotfix_UpdateViewSupplementFuel;
        private static DelegateBridge __Hotfix_OnSupplementFuelCloseButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarDataSyncReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarPackShipReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarShipRemoveModuleEquip2SlotReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarShipSupplementFuelReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipLockReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarShipCaptainRegisterReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarShipCaptainUnregisterReq;
        private static DelegateBridge __Hotfix_SendGuildFlagShipRenameReq;
        private static DelegateBridge __Hotfix_OnGuildFlagShipOperateErrorWithAck;
        private static DelegateBridge __Hotfix_InitWeaponEquipDetailUIController;
        private static DelegateBridge __Hotfix_UpdateViewWeaponEquipDetailInfoMode;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelReplaceButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelReplaceButtonClickImp;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoToggleChanged;
        private static DelegateBridge __Hotfix_StartWeaponEquipSetUITask;
        private static DelegateBridge __Hotfix_StartWeaponEquipSetUITaskImp;
        private static DelegateBridge __Hotfix_StartGuildFlagShipHangarUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadFromShipList;
        private static DelegateBridge __Hotfix_CollectDynmaicResForSlotItemBackGroundSprite;
        private static DelegateBridge __Hotfix_CollectDynmaicResForForceIcon;
        private static DelegateBridge __Hotfix_CollectDynamicResForShipRank;
        private static DelegateBridge __Hotfix_CollectDynmaicResForShipBGImage;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateTipWndState;
        private static DelegateBridge __Hotfix_UpdateViewShipListOverviewMode;
        private static DelegateBridge __Hotfix_UpdateViewShipListOverviewModeImp;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnUnregisterOtherButtonClick;
        private static DelegateBridge __Hotfix_OnUnresgisterSelfButtonClick;
        private static DelegateBridge __Hotfix_UnregistGuildFlagShipHangarShipCaptain;
        private static DelegateBridge __Hotfix_OnRegisterButtonClick;
        private static DelegateBridge __Hotfix_OnGoToStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnStikeToGuildFlagShipHangar;
        private static DelegateBridge __Hotfix_OnPackCancelButtonClick;
        private static DelegateBridge __Hotfix_OnPackConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnFlagShipEnergyButtonClick;
        private static DelegateBridge __Hotfix_OnFlagShipPackButtonClick;
        private static DelegateBridge __Hotfix_OnLockButtonClick;
        private static DelegateBridge __Hotfix_OnHangarListButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarShipItemClick;
        private static DelegateBridge __Hotfix_OnBackFromFlagShipDestroyedPanel;
        private static DelegateBridge __Hotfix_OnGuildFlagShipUnpackEnd;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelDetailButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelReplaceButtonClick;
        private static DelegateBridge __Hotfix_OnShipSuperWeaponButtonClick;
        private static DelegateBridge __Hotfix_OnShipTacticalEquipButtonClick;
        private static DelegateBridge __Hotfix_OnShipRenameButtonClick;
        private static DelegateBridge __Hotfix_OnShipRenameConfirmClick;
        private static DelegateBridge __Hotfix_OnShipRenameCancelClick;
        private static DelegateBridge __Hotfix_OnHighSlotItemClick;
        private static DelegateBridge __Hotfix_OnMiddleSlotItemClick;
        private static DelegateBridge __Hotfix_OnLowSlotGroupButtonClick;
        private static DelegateBridge __Hotfix_OnLowSlotItemClick;
        private static DelegateBridge __Hotfix_OnEditSlotGroup;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnLongPressed;
        private static DelegateBridge __Hotfix_OnLongPressReleased;
        private static DelegateBridge __Hotfix_OnLongPressReleasedImpl;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SetPipeLineStateToRefreshAll;
        private static DelegateBridge __Hotfix_SetPipeLineStateToRefreshSingle;
        private static DelegateBridge __Hotfix_SetPipeLineStateToSelectOtherShip;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_InitPipeLineCtxStateFromUIIntent;
        private static DelegateBridge __Hotfix_AddHideLowSlotProcessToMainProcess;
        private static DelegateBridge __Hotfix_ShowLowSlotAssemblyPanel;
        private static DelegateBridge __Hotfix_HideWeaponEquipInfoPanel;
        private static DelegateBridge __Hotfix_CreateShowWeaponEquipInfoPanelProcess;
        private static DelegateBridge __Hotfix_ShowWeaponEquipInfoPanel;
        private static DelegateBridge __Hotfix_GetEditSlotGroup;
        private static DelegateBridge __Hotfix_UpdateHangarBGToCurrShip;
        private static DelegateBridge __Hotfix_UpdateHangarLockState;
        private static DelegateBridge __Hotfix_IsGuildFlagShipHangarShipAvaliable;
        private static DelegateBridge __Hotfix_IsGuildFlagShipHangarAvaliable;
        private static DelegateBridge __Hotfix_CheckFlagShipName;
        private static DelegateBridge __Hotfix_CreateGuildFlagShipHangarBGUIBackgroundManager;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbGuild;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdateDataCache_GuildFlagShipHangar;
        private static DelegateBridge __Hotfix_CollectDynamicResForGuildFlagShipHangarList;
        private static DelegateBridge __Hotfix_InitGuildFlagShipHangarListCtrl;
        private static DelegateBridge __Hotfix_UpdateViewHangarList;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarListCloseButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarItemClick;
        private static DelegateBridge __Hotfix_OnBackFromFlagShipHangarDestroyedPanel;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddHideLowSlotProcessToMainProcess(UIProcess mainProcess, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckFlagShipName(string flagShipName)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForGuildFlagShipHangarList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectDynamicResForLoadFromShipList()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForShipRank(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForForceIcon(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForShipBGImage(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForSlotItemBackGroundSprite(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private GuildFlagShipHangarBGUIBackgroundManager CreateGuildFlagShipHangarBGUIBackgroundManager()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess CreateShowWeaponEquipInfoPanelProcess()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private LBStaticWeaponEquipSlotGroup GetEditSlotGroup(ShipEquipSlotType editType, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void HideWeaponEquipInfoPanel(bool isImmediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitGuildFlagShipHangarListCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSupplementFuelUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        private void InitWeaponEquipDetailUIController()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGuildFlagShipHangarAvaliable(IGuildFlagShipHangarDataContainer flagShipHangar)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGuildFlagShipHangarShipAvaliable(ILBStaticFlagShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackFromFlagShipDestroyedPanel(GuildLostReportUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackFromFlagShipHangarDestroyedPanel(GuildLostReportUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditSlotGroup(bool isTheSameSlot)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFlagShipEnergyButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFlagShipPackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoToStarMapButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarItemClick(GuildFlagShipHangarListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarListCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarShipItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipOperateErrorWithAck(int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipUnpackEnd(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarListButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHighSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLockButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressed()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressReleased()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLongPressReleasedImpl()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotGroupButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMiddleSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPackCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPackConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRegisterButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameCancelClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameConfirmClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipSuperWeaponButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipTacticalEquipButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStikeToGuildFlagShipHangar(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSupplementFuelCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnregisterOtherButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnresgisterSelfButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelRemoveButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelReplaceButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelReplaceButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoToggleChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelRemoveButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelReplaceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarDataSyncReq(bool getAllHangarInfo, bool getGuildStoreInfo, ulong hangarInstanceId = 0UL, bool showErrorTips = true, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarPackShipReq(ulong instanceId, int slotIndex, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarShipCaptainRegisterReq(ulong shipHangarInstanseId, int shipSlotIndex, List<int> drivingLicenseIdList, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarShipCaptainUnregisterReq(ulong shipHangarInstanseId, int shipSlotIndex, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarShipRemoveModuleEquip2SlotReq(ulong shipHangarInstanseId, int shipSlotIndex, int equipSlotIndex, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarShipSupplementFuelReq(ulong shipHangarInstanseId, List<ProGuildFlagShipHangarShipSupplementFuelInfo> fuelChangeCountList, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipLockReq(bool isLocked)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipRenameReq(ulong shipHangarInstanseId, int shipSlotIndex, string newName, Action<bool> onEnd, Action<int> onAckError)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineStateToRefreshAll(bool resetSelectIndex = true)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineStateToRefreshSingle(bool dataChanged)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPipeLineStateToSelectOtherShip()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess ShowLowSlotAssemblyPanel(bool immediateComplete, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowWeaponEquipInfoPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildFlagShipHangarUITask(UIIntent returnIntent, int solarSystemId = 0, Action<bool> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartWeaponEquipSetUITask(LBStaticWeaponEquipSlotGroup slotGroup, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartWeaponEquipSetUITaskImp(LBStaticWeaponEquipSlotGroup slotGroup, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregistGuildFlagShipHangarShipCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_GuildFlagShipHangar()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHangarBGToCurrShip()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHangarLockState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTipWndState(ILBStaticFlagShip currShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewHangarList()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewShipListOverviewMode(ILBStaticFlagShip currShip)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewShipListOverviewModeImp(ILBStaticFlagShip currShip)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewSupplementFuel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewWeaponEquipDetailInfoMode()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildFlagShipHangarItemClick>c__AnonStoreyD
        {
            internal IGuildFlagShipHangarDataContainer shipHangar;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.CurrentIntent.TargetMode = "ShipListOverviewMode";
                this.$this.StartUpdatePipeLine(this.$this.CurrentIntent, false, false, true, null);
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    this.$this.Pause();
                    this.$this.m_bgTask.SetCurrSelectHangarShip(null, this.shipHangar.GetReadOnlyBasicInfo().m_solarSystemId, false);
                    GuildLostReportUITask task = UIManager.Instance.FindUITaskWithName("GuildLostReportUITask", false) as GuildLostReportUITask;
                    if (task != null)
                    {
                        task.EventOnBeforeReturnPreTask += new Action<GuildLostReportUITask>(this.$this.OnBackFromFlagShipHangarDestroyedPanel);
                    }
                }
            }

            internal void <>m__2(UIProcess p, bool r)
            {
                ((UIIntentCustom) this.$this.CurrentIntent).SetParam("SolarSystemId", this.shipHangar.GetReadOnlyBasicInfo().m_solarSystemId);
                this.$this.SetPipeLineStateToRefreshAll(true);
                this.$this.CurrentIntent.TargetMode = "ShipListOverviewMode";
                this.$this.StartUpdatePipeLine(this.$this.CurrentIntent, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildFlagShipHangarShipItemClick>c__AnonStoreyC
        {
            internal int index;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.Pause();
                    this.$this.m_bgTask.SetCurrSelectHangarShip(null, this.$this.m_currFlagShipHangar.GetReadOnlyBasicInfo().m_solarSystemId, false);
                    RankType hangarSlotRankType = this.$this.LbGuild.GetHangarSlotRankType(this.$this.m_currFlagShipHangar, this.index);
                    ulong buildingInstanceId = this.$this.m_currFlagShipHangar.GetReadOnlyBasicInfo().m_buildingInstanceId;
                    ShipHangarAssignUITask.StartShipHangarAssignGuildUITask(this.$this.m_currIntent, buildingInstanceId, this.index, false, hangarSlotRankType, this.$this.CreateGuildFlagShipHangarBGUIBackgroundManager());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnLockButtonClick>c__AnonStoreyB
        {
            internal bool tryLock;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0()
            {
                this.$this.SendGuildFlagShipLockReq(this.tryLock);
            }
        }

        [CompilerGenerated]
        private sealed class <OnResume>c__AnonStorey9
        {
            internal int solarSystemId;
            internal GuildFlagShipHangarUITask.<OnResume>c__AnonStoreyA <>f__ref$10;

            internal void <>m__0(bool result)
            {
                if (!result)
                {
                    CommonUITaskHelper.ReturnToBasicUITask(null, true);
                }
                else
                {
                    this.<>f__ref$10.$this.m_currFlagShipHangar = this.<>f__ref$10.$this.LbGuild.GetGuildFlagShipHangarBySolarSystemId(this.solarSystemId);
                    if (!this.<>f__ref$10.$this.IsGuildFlagShipHangarAvaliable(this.<>f__ref$10.$this.m_currFlagShipHangar))
                    {
                        CommonUITaskHelper.ReturnToBasicUITask(null, true);
                    }
                    else
                    {
                        this.<>f__ref$10.$this.InitPipeLineCtxStateFromUIIntent(this.<>f__ref$10.intent);
                        if (this.<>f__ref$10.$this.m_bgTask.State == Task.TaskState.Paused)
                        {
                            this.<>f__ref$10.$this.m_bgTask.Resume(null, null);
                            this.<>f__ref$10.$this.m_bgTask.EventOnLongPressd += new Action(this.<>f__ref$10.$this.OnLongPressed);
                            this.<>f__ref$10.$this.m_bgTask.EventOnLongPressReleased += new Action(this.<>f__ref$10.$this.OnLongPressReleased);
                        }
                        this.<>f__ref$10.$this.InitLayerStateOnResume();
                        this.<>f__ref$10.$this.<OnResume>__BaseCallProxy0(this.<>f__ref$10.intent, this.<>f__ref$10.onPipelineEnd);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnResume>c__AnonStoreyA
        {
            internal UIIntent intent;
            internal Action<bool> onPipelineEnd;
            internal GuildFlagShipHangarUITask $this;
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey8
        {
            internal Action<bool> onPrepareEnd;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    this.onPrepareEnd(false);
                }
                else
                {
                    bool flag = false;
                    foreach (IGuildFlagShipHangarDataContainer container in this.$this.LbGuild.GetAllFlagShipHangarList())
                    {
                        if (this.$this.IsGuildFlagShipHangarAvaliable(container))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildFlagShipHangarListIsEmpty, false, new object[0]);
                    }
                    this.onPrepareEnd(flag);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarDataSyncReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal bool showErrorTips;

            internal void <>m__0(Task task)
            {
                GuildFlagShipHangarDataSyncNetTask task2 = task as GuildFlagShipHangarDataSyncNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                    if (this.showErrorTips)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarPackShipReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal ulong instanceId;
            internal int slotIndex;
            internal Action<int> onAckError;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    GuildFlagShipHangarPackShipReqNetTask task = new GuildFlagShipHangarPackShipReqNetTask(this.instanceId, this.slotIndex, ((IStaticFlagShipDataContainer) this.$this.m_currFlagShipHangarShipList[this.slotIndex].GetShipDataContainer()).GetShipStoreItemIndex());
                    task.EventOnStop += delegate (Task task) {
                        GuildFlagShipHangarPackShipReqNetTask task2 = task as GuildFlagShipHangarPackShipReqNetTask;
                        if ((task2 == null) || task2.IsNetworkError)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        else if (task2.Result == 0)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                        }
                        else
                        {
                            if (this.onAckError != null)
                            {
                                this.onAckError(task2.Result);
                            }
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                    };
                    task.Start(null, null);
                }
                else if (this.onEnd != null)
                {
                    this.onEnd(false);
                }
            }

            internal void <>m__1(Task task)
            {
                GuildFlagShipHangarPackShipReqNetTask task2 = task as GuildFlagShipHangarPackShipReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarShipCaptainRegisterReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal Action<int> onAckError;

            internal void <>m__0(Task task)
            {
                GuildFlagShipHangarShipCaptainRegisterReqNetTask task2 = task as GuildFlagShipHangarShipCaptainRegisterReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarShipCaptainUnregisterReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal Action<int> onAckError;

            internal void <>m__0(Task task)
            {
                GuildFlagShipHangarShipCaptainUnregisterReqNetTask task2 = task as GuildFlagShipHangarShipCaptainUnregisterReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    if (task2.Result == -3011)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(-3303, true, false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarShipRemoveModuleEquip2SlotReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal int shipSlotIndex;
            internal ulong shipHangarInstanseId;
            internal int equipSlotIndex;
            internal Action<int> onAckError;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    IStaticFlagShipDataContainer shipDataContainer = (IStaticFlagShipDataContainer) this.$this.m_currFlagShipHangarShipList[this.shipSlotIndex].GetShipDataContainer();
                    GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask task = new GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask(this.shipHangarInstanseId, this.shipSlotIndex, shipDataContainer.GetShipStoreItemIndex(), this.equipSlotIndex, shipDataContainer.GetReadOnlyShipInstanceInfo().m_moduleSlotList[this.equipSlotIndex]);
                    task.EventOnStop += delegate (Task task) {
                        GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask task2 = task as GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask;
                        if ((task2 == null) || task2.IsNetworkError)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        else if (task2.Result == 0)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                        }
                        else
                        {
                            if (this.onAckError != null)
                            {
                                this.onAckError(task2.Result);
                            }
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                    };
                    task.Start(null, null);
                }
            }

            internal void <>m__1(Task task)
            {
                GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask task2 = task as GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarShipSupplementFuelReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal ulong shipHangarInstanseId;
            internal List<ProGuildFlagShipHangarShipSupplementFuelInfo> fuelChangeCountList;
            internal Action<int> onAckError;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    GuildFlagShipHangarShipSupplementFuelReqNetTask task = new GuildFlagShipHangarShipSupplementFuelReqNetTask(this.shipHangarInstanseId, this.fuelChangeCountList);
                    task.EventOnStop += delegate (Task task) {
                        GuildFlagShipHangarShipSupplementFuelReqNetTask task2 = task as GuildFlagShipHangarShipSupplementFuelReqNetTask;
                        if ((task2 == null) || task2.IsNetworkError)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        else if (task2.Result == 0)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                        }
                        else
                        {
                            if (this.onAckError != null)
                            {
                                this.onAckError(task2.Result);
                            }
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                    };
                    task.Start(null, null);
                }
                else if (this.onEnd != null)
                {
                    this.onEnd(false);
                }
            }

            internal void <>m__1(Task task)
            {
                GuildFlagShipHangarShipSupplementFuelReqNetTask task2 = task as GuildFlagShipHangarShipSupplementFuelReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipRenameReq>c__AnonStorey6
        {
            internal Action<bool> onEnd;
            internal Action<int> onAckError;

            internal void <>m__0(Task task)
            {
                GuildFlagShipRenameReqNetTask task2 = task as GuildFlagShipRenameReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onAckError != null)
                    {
                        this.onAckError(task2.Result);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartWeaponEquipSetUITask>c__AnonStorey7
        {
            internal LBStaticWeaponEquipSlotGroup slotGroup;
            internal Action<bool> onEnd;
            internal GuildFlagShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if ((this.$this.m_weaponEquipDetailInfoUICtrl != null) && this.$this.m_weaponEquipDetailInfoUICtrl.IsShow())
                {
                    this.$this.PlayUIProcess(this.$this.m_weaponEquipDetailInfoUICtrl.ShowWeaponEquipDetailInfo(false, false, true), true, (p, r) => this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd), false);
                }
                else
                {
                    this.$this.HideWeaponEquipInfoPanel(false);
                    this.$this.m_guildFlagShipHangarUIController.EnableBackGroundButton(false);
                    UIProcess mainProcess = this.$this.m_guildFlagShipHangarUIController.SetUIMode(GuildFlagShipHangarUIController.UIMode.HideAll, false, false, false);
                    this.$this.AddHideLowSlotProcessToMainProcess(mainProcess, false, false, false, UIProcess.ProcessExecMode.Parallel);
                    this.$this.PlayUIProcess(mainProcess, true, (p, r) => this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd), false);
                }
            }

            internal void <>m__1(UIProcess p, bool r)
            {
                this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd);
            }

            internal void <>m__2(UIProcess p, bool r)
            {
                this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd);
            }
        }

        private enum PipeLineStateMaskType
        {
            IsNeedLoadDynamicRes,
            IsOnlyShowTipWnd,
            IsEditTheSameSlot,
            IsRefreshShipHangar,
            IsRefreshAllShipHangar,
            IsRefreshSingleShip,
            IsUpdateShipListItemSelected,
            IsResetSelectHangarShipIndex,
            IsUpdateShipInfoPanel,
            IsUpdateShipDetailInfoPanel,
            IsUpdateWeaponEquipSlotPanel,
            ChangeLowSlotAssemblyPanelState,
            IsShowShipRenamePanel,
            IsShowShipPackPanel
        }

        private enum TipWndType
        {
            NoTipWnd,
            SuperWeaponTipWnd,
            TacticalEquipTipWnd,
            WeaponEquipInfoTipWnd
        }
    }
}

