﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class MsgBoxForEnterStationUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private int m_solarSystemId;
        private int m_spaceStationId;
        private bool m_isReturnBaseForFunc;
        private bool m_isInSpace;
        private Action m_onMsgBoxCancelClick;
        private MsgBoxForEnterStationUIController m_mainCtrl;
        public const string TaskModeEnterBase = "EnterBase";
        public const string TaskModeEnterStation = "EnterStation";
        public const string ParamKeyEnterSpaceCancelAction = "OnEnterSpaceCancel";
        public const string ParamKeyIsReturnBaseForFunc = "IsReturnBaseForFunc";
        public const string TaskName = "MsgBoxForEnterStationUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowMsgBoxForEnterStation;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateMsgBoxInfo;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_EnterStation;
        private static DelegateBridge __Hotfix_GetNearstSpaceStationId;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public MsgBoxForEnterStationUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void EnterStation()
        {
        }

        [MethodImpl(0x8000)]
        private int GetNearstSpaceStationId(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isShow = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool ShowMsgBoxForEnterStation(bool isReturnBase, bool returnBaseForFunc = true, Action onMsgBoxCancel = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMsgBoxInfo(bool isJumpToBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey0
        {
            internal Action action;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
            }
        }
    }
}

