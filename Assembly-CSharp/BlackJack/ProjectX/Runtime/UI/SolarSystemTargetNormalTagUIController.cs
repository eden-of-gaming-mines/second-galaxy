﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System.Runtime.CompilerServices;

    public class SolarSystemTargetNormalTagUIController : SolarSystemTargetTagBaseUIController
    {
        [AutoBind("./TargetSpecialIconRoot/TargetNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetNoticeCtrl;
        [AutoBind("./TargetSpecialIconRoot/TargetPVPRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetPVPCtrl;
        private static DelegateBridge __Hotfix_GetTargetPvpFlagStateCtrl;
        private static DelegateBridge __Hotfix_GetTargetNoticeStateCtrl;

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetNoticeStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetPvpFlagStateCtrl()
        {
        }
    }
}

