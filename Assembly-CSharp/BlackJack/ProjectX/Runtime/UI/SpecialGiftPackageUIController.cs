﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SpecialGiftPackageUIController : UIControllerBase
    {
        private int m_curShowTabIndex;
        private List<CommonUIStateController> m_tabItemList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, ILBStoreItemClient, Vector3> EventOnGiftContentItemIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnGiftPackageItemBuyButtonClick;
        protected List<LBRechargeGiftPackage> m_giftPackageList;
        protected Dictionary<string, UnityEngine.Object> m_resDict;
        [AutoBind("./LastButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LeftArrowUIStateCtrl;
        [AutoBind("./NextButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RightArrowUIStateCtrl;
        [AutoBind("./LastButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeftArrowBtn;
        [AutoBind("./NextButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RightArrowBtn;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ContentRectTransform;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect PackageItemScollView;
        [AutoBind("./TabItemGroup", AutoBindAttribute.InitState.NotInit, true)]
        public RectTransform TabItemGroupRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SpecialGiftPackageStateCtrl;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollSnapCenter4LoopScroll m_scrollSnap;
        private static DelegateBridge __Hotfix_GetMainPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateGiftPackageList;
        private static DelegateBridge __Hotfix_UpdateSpecialGiftPackageListByIndex;
        private static DelegateBridge __Hotfix_GetCurrGiftPackageIndex;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_CreateTabItemList;
        private static DelegateBridge __Hotfix_UpdateTabItemList;
        private static DelegateBridge __Hotfix_UpdateLeftRightArrow;
        private static DelegateBridge __Hotfix_ClearAllTabItemList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_InitEasyPoolInfo;
        private static DelegateBridge __Hotfix_OnCenterItemChanged;
        private static DelegateBridge __Hotfix_OnAutoSnapNext;
        private static DelegateBridge __Hotfix_OnAutoSnapLast;
        private static DelegateBridge __Hotfix_OnEasyPoolObjectCreate;
        private static DelegateBridge __Hotfix_OnPoolObjectReturnPool;
        private static DelegateBridge __Hotfix_OnItemRootCtrlNeedFill;
        private static DelegateBridge __Hotfix_OnGiftPackageItemBuyButtonClicked;
        private static DelegateBridge __Hotfix_OnCommonItemIconClick;
        private static DelegateBridge __Hotfix_GetGiftPackageIndex;
        private static DelegateBridge __Hotfix_add_EventOnGiftContentItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnGiftContentItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGiftPackageItemBuyButtonClick;

        public event Action<int, ILBStoreItemClient, Vector3> EventOnGiftContentItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGiftPackageItemBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearAllTabItemList()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateTabItemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrGiftPackageIndex()
        {
        }

        [MethodImpl(0x8000)]
        private int GetGiftPackageIndex(LBRechargeGiftPackage giftPackage)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainPanelShowOrHideProcess(bool isShow, bool immedite = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPoolInfo(string assetName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoSnapLast()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoSnapNext()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCenterItemChanged(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemIconClick(LBRechargeGiftPackage giftPackage, ILBStoreItemClient rewardInfo, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolObjectCreate(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemBuyButtonClicked(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemRootCtrlNeedFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectReturnPool(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGiftPackageList(List<LBRechargeGiftPackage> giftPackageList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLeftRightArrow()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpecialGiftPackageListByIndex(List<LBRechargeGiftPackage> giftPackageList, Dictionary<string, UnityEngine.Object> resDict, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTabItemList()
        {
        }
    }
}

