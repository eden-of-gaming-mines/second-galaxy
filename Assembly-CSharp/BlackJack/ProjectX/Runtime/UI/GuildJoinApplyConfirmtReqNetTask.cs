﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildJoinApplyConfirmtReqNetTask : NetWorkTransactionTask
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildJoinApplyListAck <ApplyListAck>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <Result>k__BackingField;
        private string m_userId;
        private bool m_agree;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildMemberJoinApply;
        private static DelegateBridge __Hotfix_get_ApplyListAck;
        private static DelegateBridge __Hotfix_set_ApplyListAck;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildJoinApplyConfirmtReqNetTask(string userId, bool agree)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildMemberJoinApply(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildJoinApplyListAck ApplyListAck
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int Result
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

