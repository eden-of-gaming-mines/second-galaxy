﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class UserGuideTriggerPointDesc : MonoBehaviour
    {
        [Header("Enable时触发点类型")]
        public int m_enableUserGuideTriggerPointType;
        [Header("Disable时触发点类型")]
        public int m_distableUserGuideTriggerPointType;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<int> EventOnEnableTriggerPoint;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static Action<int> EventOnDisableTriggerPoint;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_add_EventOnEnableTriggerPoint;
        private static DelegateBridge __Hotfix_remove_EventOnEnableTriggerPoint;
        private static DelegateBridge __Hotfix_add_EventOnDisableTriggerPoint;
        private static DelegateBridge __Hotfix_remove_EventOnDisableTriggerPoint;

        public static  event Action<int> EventOnDisableTriggerPoint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public static  event Action<int> EventOnEnableTriggerPoint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }
    }
}

