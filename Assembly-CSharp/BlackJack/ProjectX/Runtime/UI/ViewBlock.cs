﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ViewBlock
    {
        private const string NameStrFormat = "Scl{0}_X{1}_Y{2}_Ver{3}{4}";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private RawImage <Image>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Texture2D <Texture>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private UnityEngine.GameObject <GameObject>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private UnityEngine.Transform <Transform>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <X>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <Y>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ScaleLevel>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsRequestingData>k__BackingField;
        public int m_loadedDataVersion;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Draw;
        private static DelegateBridge __Hotfix_DrawPic;
        private static DelegateBridge __Hotfix_SetPicName;
        private static DelegateBridge __Hotfix_Hide;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_get_Image;
        private static DelegateBridge __Hotfix_set_Image;
        private static DelegateBridge __Hotfix_get_Texture;
        private static DelegateBridge __Hotfix_set_Texture;
        private static DelegateBridge __Hotfix_get_GameObject;
        private static DelegateBridge __Hotfix_set_GameObject;
        private static DelegateBridge __Hotfix_get_Transform;
        private static DelegateBridge __Hotfix_set_Transform;
        private static DelegateBridge __Hotfix_get_X;
        private static DelegateBridge __Hotfix_set_X;
        private static DelegateBridge __Hotfix_get_Y;
        private static DelegateBridge __Hotfix_set_Y;
        private static DelegateBridge __Hotfix_get_ScaleLevel;
        private static DelegateBridge __Hotfix_set_ScaleLevel;
        private static DelegateBridge __Hotfix_get_IsRequestingData;
        private static DelegateBridge __Hotfix_set_IsRequestingData;

        [MethodImpl(0x8000)]
        public ViewBlock(RawImage image)
        {
        }

        [MethodImpl(0x8000)]
        public bool Draw(Action<ColorByteArrayContext> asyncRequestListenerRegister, Func<int, int, int, int, bool> checkIfInProgress)
        {
        }

        [MethodImpl(0x8000)]
        private bool DrawPic(Action<ColorByteArrayContext> asyncRequestListenerRegister, int dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void Hide()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPicName(int ver, string extMark = "")
        {
        }

        [MethodImpl(0x8000)]
        public void Show()
        {
        }

        public RawImage Image
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public Texture2D Texture
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public UnityEngine.GameObject GameObject
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public UnityEngine.Transform Transform
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public int X
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public int Y
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public int ScaleLevel
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsRequestingData
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DrawPic>c__AnonStorey0
        {
            internal int dataVersion;
            internal ViewBlock $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(byte[] backData)
            {
            }
        }
    }
}

