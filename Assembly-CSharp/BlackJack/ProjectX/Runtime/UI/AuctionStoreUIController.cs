﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionStoreUIController : UIControllerBase
    {
        private long m_sellMaxCount = 1L;
        private long _SellCount;
        private long _SellPrice;
        private long m_minePrice;
        private long m_maxPrice;
        private int _Period;
        private TimeSpan m_remainFreezingTime;
        private const string m_redColor = "BE3E37FF";
        private const string m_whiteColor = "FFFFFFFF";
        private const string m_greenColor = "7CB544FF";
        private int m_maxOnSheveCount;
        private AuctionItemInfoAck m_info;
        public ILBStoreItemClient CurrentSelectItem;
        public CommonItemIconUIController m_ItemIconCtrl;
        public ItemStoreListUIController m_itemStoreListUIController;
        public AuctionItemMarketInfoUIController m_auctionItemMarketInfoUIController;
        public AuctionStoreTipUIController m_auctionStoreTipUIController;
        private long bindMoney;
        private long tradeMoney;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_mainStateCtrl;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_CMoneyNumberText;
        [AutoBind("./CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_CMoneyNumberAddButton;
        [AutoBind("./SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_SMoneyNumberText;
        [AutoBind("./SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_SMoneyNumberAddButton;
        [AutoBind("./ItemStoreNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_itemStoreNameText;
        [AutoBind("./TradeInformationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tradeStateController;
        [AutoBind("./TradeInformationGroup/TitleGroup01/TradeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_TradeNameText;
        [AutoBind("./TradeInformationGroup/TitleGroup01/Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_QualityImage;
        [AutoBind("./TradeInformationGroup/TitleGroup01/Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_QualityBgImage;
        [AutoBind("./TradeInformationGroup/TitleGroup01/Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_TechImage;
        [AutoBind("./TradeInformationGroup/TitleGroup01/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_ItemDummy;
        [AutoBind("./TradeInformationGroup/SellGroup/SellNumberInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_SellText;
        [AutoBind("./TradeInformationGroup/SellGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_SellAddButton;
        [AutoBind("./TradeInformationGroup/SellGroup/CutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_SellCutButton;
        [AutoBind("./TradeInformationGroup/UnitGroup/UnitNumberInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_PriceSellText;
        [AutoBind("./TradeInformationGroup/UnitGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_PriceAddButton;
        [AutoBind("./TradeInformationGroup/UnitGroup/CutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_PriceCutButton;
        [AutoBind("./TradeInformationGroup/UpperLimitGroup/UpperLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_UpperLimitText;
        [AutoBind("./TradeInformationGroup/UpperLimitGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_UpperLimitAddButton;
        [AutoBind("./TradeInformationGroup/UpperLimitGroup/CutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_UpperLimitCutButton;
        [AutoBind("./TradeInformationGroup/BazaarImage/BazaarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_marketPriceText;
        [AutoBind("./TradeInformationGroup/CustodyImage/CustodyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_CustodyText;
        [AutoBind("./TradeInformationGroup/TodayImage/TodayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_TodayText;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/SellNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_freezingRemainTimeText;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_freezingClockImage;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/TradeinformationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_TradeinformationButton;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/TradeinformationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tradeinformationButtonStateCtrl;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/TradeinformationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tradeinformationStateController;
        [AutoBind("./TradeInformationGroup/UnitGroup/UnitMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemPriceHintButton;
        [AutoBind("./TradeInformationGroup/UpperLimitGroup/MoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemPeriodHintButton;
        [AutoBind("./TradeInformationGroup/CustodyImage/CustodyNameText/CustodyMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemCustodyHintButton;
        [AutoBind("./TradeInformationGroup/TodayImage/TodayNameText/TodayMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemTodayHintButton;
        [AutoBind("./TradeInformationGroup/DeblockingGroup/FreezeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_freezingButton;
        [AutoBind("./TradeInformationGroup/TotalPricesImage/TotalPricesText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_totalPricesText;
        [AutoBind("./ItemStoreListUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemStoreListUIDummy;
        [AutoBind("./ItemStoreListUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public ItemStoreListSizeDesc m_itemStoreListDesc;
        [AutoBind("./BazzarGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_BazzarGroupDummy;
        [AutoBind("./HintGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_HintGroupDummy;
        [AutoBind("./ItemSimpleInfoRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoDummyPos;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_SetSelectEmptyState;
        private static DelegateBridge __Hotfix_SetPlayerMoney;
        private static DelegateBridge __Hotfix_UpdateTradeInformation;
        private static DelegateBridge __Hotfix_UpdateMarketInfo;
        private static DelegateBridge __Hotfix_IsInMotherShip;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetFreezingState;
        private static DelegateBridge __Hotfix_SetFreezingRemainTime;
        private static DelegateBridge __Hotfix_GetFreezTimeString;
        private static DelegateBridge __Hotfix_IsPlayerHaveEnoughMoney;
        private static DelegateBridge __Hotfix_CalcAuctionFee;
        private static DelegateBridge __Hotfix_CalcTotalCost;
        private static DelegateBridge __Hotfix_SetMaxSellCount;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_get_SellCount;
        private static DelegateBridge __Hotfix_set_SellCount;
        private static DelegateBridge __Hotfix_get_SellPrice;
        private static DelegateBridge __Hotfix_set_SellPrice;
        private static DelegateBridge __Hotfix_CalCompareToMaketPrice;
        private static DelegateBridge __Hotfix_SetRangeOfPrice;
        private static DelegateBridge __Hotfix_get_Period;
        private static DelegateBridge __Hotfix_set_Period;

        [MethodImpl(0x8000)]
        public void CalcAuctionFee()
        {
        }

        [MethodImpl(0x8000)]
        private string CalCompareToMaketPrice(long maketPrice, long price)
        {
        }

        [MethodImpl(0x8000)]
        private void CalcTotalCost()
        {
        }

        [MethodImpl(0x8000)]
        private string GetFreezTimeString(TimeSpan remainTime)
        {
        }

        [MethodImpl(0x8000)]
        private long GetItemCount(long value, long maxValue = 0x7fffffffL, long min = 1L)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void IsInMotherShip(bool isInStation)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayerHaveEnoughMoney()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFreezingRemainTime()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFreezingState(bool isFreezing)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMaxSellCount(long maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlayerMoney(long bindMoney, long tradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRangeOfPrice(long sellPrice)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectEmptyState(bool isTradeEmpty, bool isProductInformationEmpty)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMarketInfo(AuctionItemInfoAck info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradeInformation(ILBStoreItemClient itemClient, Dictionary<string, UnityEngine.Object> resDict, DateTime freezingTime)
        {
        }

        public long SellCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public long SellPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int Period
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

