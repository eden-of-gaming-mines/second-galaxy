﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;

    public class TargetNoticeState
    {
        public const string None = "None";
        public const string EnemyNormal = "EnemyNormal";
        public const string FriendlyNormal = "FriendlyNormal";
        public const string TalkTarget = "TalkTarget";
        public const string InvestigateTarget = "InvestigateTarget";
        public const string GuildFleetFireFocusTarget = "EnemyNormal";
        public const string GuildFleetProtectTarget = "FriendlyNormal";
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

