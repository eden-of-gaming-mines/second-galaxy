﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainFeatInfoItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CaptainFeatId>k__BackingField;
        private FeatItemState m_itemState = FeatItemState.Unselect;
        private const string ItemState_Select = "Select";
        private const string ItemState_Unselect = "Unselect";
        private const string ItemState_Disable = "Disable";
        private const string ItemState_New = "New";
        private const string ItemState_Upgrade = "Upgrade";
        private const string IconState_Init = "Init";
        private const string IconState_Addition = "Addition";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IconStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FeatIcon;
        [AutoBind("./ArtEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ArtEffectCtrl;
        [AutoBind("./UsefulEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UsefulEffect;
        private static DelegateBridge __Hotfix_SetCaptainFeatInfo;
        private static DelegateBridge __Hotfix_SetSelectState;
        private static DelegateBridge __Hotfix_SetFeatItemState;
        private static DelegateBridge __Hotfix_SetIsUseful;
        private static DelegateBridge __Hotfix_ShowNewFeatEffect;
        private static DelegateBridge __Hotfix_SetIconState;
        private static DelegateBridge __Hotfix_ShowUpgradeEffect;
        private static DelegateBridge __Hotfix_set_CaptainFeatId;
        private static DelegateBridge __Hotfix_get_CaptainFeatId;
        private static DelegateBridge __Hotfix_get_CurrFeatItemState;
        private static DelegateBridge __Hotfix_get_IsSelected;

        [MethodImpl(0x8000)]
        public void SetCaptainFeatInfo(LBNpcCaptainFeats featInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFeatItemState(FeatItemState itemState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconState(bool isInit)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsUseful(bool isUseful)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewFeatEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUpgradeEffect()
        {
        }

        public int CaptainFeatId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public FeatItemState CurrFeatItemState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsSelected
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum FeatItemState
        {
            Select,
            Unselect,
            Disable
        }
    }
}

