﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class BattlePassPurchaseTask : UITaskBase
    {
        private BattlePassPurchaseUIController m_MainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "BattlePassPurchaseTask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBindConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_SendBattlePassRmbUpgradeReq;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public BattlePassPurchaseTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendBattlePassRmbUpgradeReq(bool isPackage, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnBindConfirmButtonClick>c__AnonStorey0
        {
            internal bool enough;
            internal BattlePassPurchaseTask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(bool end)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__2(bool ret)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey1
        {
            internal bool enough;
            internal BattlePassPurchaseTask $this;

            internal void <>m__0()
            {
                if (this.enough)
                {
                    this.$this.SendBattlePassRmbUpgradeReq(false, delegate (bool end) {
                        this.$this.Pause();
                        UIIntentReturnable currentIntent = this.$this.CurrentIntent as UIIntentReturnable;
                        currentIntent.PrevTaskIntent.TargetMode = this.$this.CurrentIntent.TargetMode;
                        UIIntentReturnable intent = new UIIntentReturnable(currentIntent.PrevTaskIntent, "BattlePassEntranceTask", null);
                        if (UIManager.Instance.StartUITask(intent, false, false, null, null) != null)
                        {
                            CommonUIUtil.SetClientPlayerPrefsInt(this.$this.PlayerCtx.m_gameUserId + "BattlePassOpenUpgradeFlag", 1);
                        }
                    });
                }
                else
                {
                    CommonUITaskHelper.ToAddCurrency(CurrencyType.CurrencyType_RealMoney, this.$this.CurrentIntent, delegate (bool ret) {
                        if (ret)
                        {
                            this.$this.Pause();
                        }
                    });
                }
            }

            internal void <>m__1(bool end)
            {
                this.$this.Pause();
                UIIntentReturnable currentIntent = this.$this.CurrentIntent as UIIntentReturnable;
                currentIntent.PrevTaskIntent.TargetMode = this.$this.CurrentIntent.TargetMode;
                UIIntentReturnable intent = new UIIntentReturnable(currentIntent.PrevTaskIntent, "BattlePassEntranceTask", null);
                if (UIManager.Instance.StartUITask(intent, false, false, null, null) != null)
                {
                    CommonUIUtil.SetClientPlayerPrefsInt(this.$this.PlayerCtx.m_gameUserId + "BattlePassOpenUpgradeFlag", 1);
                }
            }

            internal void <>m__2(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendBattlePassRmbUpgradeReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal bool isPackage;

            internal void <>m__0(Task task)
            {
                BattlePassRmbUpgradeReqTask task2 = task as BattlePassRmbUpgradeReqTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                    Debug.LogError("BattlePassRmbUpgradeReqTask  " + this.isPackage);
                }
                else if (task2.ReqResult == 0)
                {
                    this.onEnd(true);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.ReqResult, true, false);
                }
            }
        }
    }
}

