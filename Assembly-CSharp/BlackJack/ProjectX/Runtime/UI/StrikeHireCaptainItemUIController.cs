﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StrikeHireCaptainItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        protected ShipStrikeUITask.CachedHireCaptain m_cachedHireCaptainInfo;
        public ScrollItemBaseUIController m_scrollCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnCaptainSelectShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCaptainIconButtonClick;
        protected const string ShipItemName = "HangarShipListItem";
        protected const string CaptainItemName = "CommonCaptainUIPrefab";
        protected CommonCaptainIconUIController m_captainIconCtrl;
        protected HangarShipListItemUIController m_shipIconCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./HireCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform HireCaptainDummyRoot;
        [AutoBind("./ShipIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ShipIconDummyRoot;
        [AutoBind("./TitleGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./DPS/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DPSValueText;
        [AutoBind("./HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HPValueText;
        [AutoBind("./Speed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SpeedValueText;
        [AutoBind("./Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeValueText;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupStateCtrl;
        [AutoBind("./ButtonGroup/UPButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpButton;
        [AutoBind("./ButtonGroup/DownButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DownButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateHireCaptainItem;
        private static DelegateBridge __Hotfix_UpdateHireCaptainItemToEmpty;
        private static DelegateBridge __Hotfix_UpdateHireCaptainItemWithCaptain;
        private static DelegateBridge __Hotfix_IsCaptainShipAbleToStrike;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnShipItemClick;
        private static DelegateBridge __Hotfix_OnCaptainIconClick;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_get_CachedHireCaptainInfo;
        private static DelegateBridge __Hotfix_add_EventOnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainIconButtonClick;

        public event Action<UIControllerBase> EventOnCaptainIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCaptainSelectShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        protected event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCaptainShipAbleToStrike(List<ShipType> allowInShipTypes, ShipStrikeUITask.CachedHireCaptain captainInfo, LBStaticHiredCaptain.ShipInfo shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateHireCaptainItem(ShipStrikeUITask.CachedHireCaptain captainInfo, ShipStrikeUITask.CachedHireCaptain nextCaptainInfo, List<ShipType> allowInShipTypes, int firstStrikeIndex, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateHireCaptainItemToEmpty()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateHireCaptainItemWithCaptain(List<ShipType> allowInShipTypes, int firstStrikeIndex, ShipStrikeUITask.CachedHireCaptain nextCaptainInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public ShipStrikeUITask.CachedHireCaptain CachedHireCaptainInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <IsCaptainShipAbleToStrike>c__AnonStorey0
        {
            internal ShipType shipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.shipType);
        }
    }
}

