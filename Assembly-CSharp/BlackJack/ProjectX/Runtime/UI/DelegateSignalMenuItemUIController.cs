﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DelegateSignalMenuItemUIController : MenuItemUIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnSignalRewardItemClick;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        private CommonRewardMoneyItemsUIController m_signalRewardCtrl;
        private CommonRewardMoneyItemsUIController m_extraRewardCtrl;
        private CommonUIStateController[] m_shipLimitCtrlList;
        private LBSignalDelegateBase m_delegateSignal;
        private Dictionary<string, UnityEngine.Object> m_IconSpriteDic;
        private bool m_hasSignalLiftTime;
        private int m_rewardCount;
        private Sprite m_bindCurrencyIcon;
        [AutoBind("./TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalNameText;
        [AutoBind("./TargetInfo/Info/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        [AutoBind("./TargetInfo/Info/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_rewardIconImage;
        [AutoBind("./TargetInfo/Info/IconBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_iconBGImageStateCtrl;
        [AutoBind("/TargetInfo/Info/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_rewardCountText;
        [AutoBind("/TargetInfo/Info/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rewardCountStateCtrl;
        [AutoBind("./TargetInfo/Info/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalTimeText;
        [AutoBind("./TargetInfo/Info/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_requireLevelText;
        [AutoBind("./Detail/MissionDetail/MissionText/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalTargetDetail01;
        [AutoBind("./Detail/MissionDetail/MissionText2/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalTargetDetail02;
        [AutoBind("./Detail/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        [AutoBind("./Detail/AssignButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_assignButton;
        [AutoBind("./Detail/AssignButton/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_assignRectButton;
        [AutoBind("./Detail/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardObj;
        [AutoBind("./Detail/ExtraReward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraRewardObj;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_rewardBonusButton;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rewardBonusStateCtrl;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus/BonusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_rewardBonusText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_GetLBSignal;
        private static DelegateBridge __Hotfix_SetDelegateSignalItemInfo;
        private static DelegateBridge __Hotfix_SetDelegateSignalInfo;
        private static DelegateBridge __Hotfix_SetRewardBouus;
        private static DelegateBridge __Hotfix_SetRemainingTimeInfo;
        private static DelegateBridge __Hotfix_DisableRemainingTimeInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnSignalRewardItemClick;
        private static DelegateBridge __Hotfix_SetDelegateMineralBGState;
        private static DelegateBridge __Hotfix_add_EventOnSignalRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSignalRewardItemClick;
        private static DelegateBridge __Hotfix_get_ShipLimitCtrlList;

        public event Action<CommonItemIconUIController> EventOnSignalRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public override void DisableRemainingTimeInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalDelegateBase GetLBSignal()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSignalRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetDelegateMineralBGState(ConfigDataMineralTypeInfo mineralTypeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDelegateSignalInfo(int currSSId, LBSignalDelegateBase delSignal, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDelegateSignalItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetRemainingTimeInfo(int days, int hours, int minutes, int seconds)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRewardBouus(List<VirtualBuffDesc> buffList = null)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private CommonUIStateController[] ShipLimitCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

