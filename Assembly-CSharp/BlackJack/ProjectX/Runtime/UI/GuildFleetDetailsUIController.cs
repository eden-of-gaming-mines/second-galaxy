﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildFleetDetailsUIController : UIControllerBase
    {
        private const string PanelStateOpen = "Show";
        private const string PanelStateClose = "Close";
        private const string MemberInfoAssetPath = "MemberInfoUIPrefab";
        private const string StateShow = "Show";
        private const string StateClose = "Close";
        private const string SortStateDown = "Down";
        private const string SortStateUp = "Up";
        private const string SortStateNone = "Empty";
        private const string StateChoose = "Choose";
        private const string StateNormal = "Normal";
        private GuildFleetDetailSortType m_currSortType;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildFleetMemberInfoUIController> EventOnMemberItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildFleetMemberInfoUIController> EventOnMemberItemFilled;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildFleetDetailSortType> EventOnSortTypeChanged;
        [AutoBind("./PromptPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PromptPanelCommonUIStateController;
        [AutoBind("./PromptPanel/PromptPanelBackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PromptPanelBackGroundButton;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDescButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FormationDescButton;
        [AutoBind("./PromptPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PromptPanelGameObject;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./DealGroup/DonateGroup/JobTitle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx JobTitleButton;
        [AutoBind("./DealGroup/DonateGroup/JobTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController JobTitleCommonUIStateController;
        [AutoBind("./DealGroup/DonateGroup/LocationTitle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LocationTitleButton;
        [AutoBind("./DealGroup/DonateGroup/LocationTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LocationTitleCommonUIStateController;
        [AutoBind("./DealGroup/DonateGroup/ShipTitle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipTitleButton;
        [AutoBind("./DealGroup/DonateGroup/ShipTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipTitleCommonUIStateController;
        [AutoBind("./DealGroup/DonateGroup/MemberTitle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MemberTitleButton;
        [AutoBind("./DealGroup/DonateGroup/MemberTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MemberTitleCommonUIStateController;
        [AutoBind("./DealGroup/StatisticsGroup/AllowToJoinFleetManualToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AllowToJoinFleetManualToggle;
        [AutoBind("./DealGroup/StatisticsGroup/FormationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown FormationGroupDropdown;
        [AutoBind("./DealGroup/StatisticsGroup/OnLineText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OnLineText;
        [AutoBind("./DealGroup/StatisticsGroup/NearbyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NearbyText;
        [AutoBind("./DealGroup/StatisticsGroup/AllMemberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllMemberText;
        [AutoBind("./DealGroup/DonateGroup/GuildFleetMemberList/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRootRectTransform;
        [AutoBind("./DealGroup/DonateGroup/GuildFleetMemberList", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool GuildFleetMemberListEasyObjectPool;
        [AutoBind("./DealGroup/DonateGroup/GuildFleetMemberList", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect GuildFleetMemberListLoopVerticalScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipTroopsUIPrefabCommonUIStateController;
        [AutoBind("./BGImages/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ReturnButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/RejectButton01", AutoBindAttribute.InitState.NotInit, false)]
        public Button RejectJumpButton;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/RejectButton02", AutoBindAttribute.InitState.NotInit, false)]
        public Button RejectFormationButton;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/RejectButton01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RejectJumpButtonStateCtrl;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/RejectButton02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RejectFormationButtonStateCtrl;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/LackTextGroup/LackText01", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LackOfNavigator;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/LackTextGroup/LackText02", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LackOfFireController;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/LackTextGroup/LackText03", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LackOfCommander;
        [AutoBind("./DealGroup/StatisticsGroup/FormationDetail/StartUsingButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FormationActiveState;
        [AutoBind("./DealGroup/DonateGroup/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MemberListStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreatePromptWindowProcess;
        private static DelegateBridge __Hotfix_UpdateSortState;
        private static DelegateBridge __Hotfix_UpdateFleetMemberList;
        private static DelegateBridge __Hotfix_UpdateFleetBasic;
        private static DelegateBridge __Hotfix_UpdateFleetName;
        private static DelegateBridge __Hotfix_UpdateFleetState;
        private static DelegateBridge __Hotfix_SetGuildFleetPersonalSetting;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnMemberItemFilled;
        private static DelegateBridge __Hotfix_OnMemberItemClick;
        private static DelegateBridge __Hotfix_OnMemberTitleButtonClick;
        private static DelegateBridge __Hotfix_OnShipTitleButtonClick;
        private static DelegateBridge __Hotfix_OnLocationTitleButtonClick;
        private static DelegateBridge __Hotfix_OnPositionTitleButtonClick;
        private static DelegateBridge __Hotfix_SetSortTitleState;
        private static DelegateBridge __Hotfix_add_EventOnMemberItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemberItemClick;
        private static DelegateBridge __Hotfix_add_EventOnMemberItemFilled;
        private static DelegateBridge __Hotfix_remove_EventOnMemberItemFilled;
        private static DelegateBridge __Hotfix_add_EventOnSortTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSortTypeChanged;

        public event Action<GuildFleetMemberInfoUIController> EventOnMemberItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildFleetMemberInfoUIController> EventOnMemberItemFilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildFleetDetailSortType> EventOnSortTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatePromptWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocationTitleButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemFilled(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberTitleButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPositionTitleButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipTitleButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildFleetPersonalSetting(bool isSelfFleet, GuildFleetPersonalSetting settingFlag, bool isFlagShip)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSortTitleState(string memberSortState = "Empty", string shipSortState = "Empty", string locationSortState = "Empty", string positionSortState = "Empty")
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFleetBasic(int memberCount, int onLineCount, int nearBy, bool allowToJoinFleetManual, int formation)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFleetMemberList(int totalCount, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFleetName(string fleetName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFleetState(bool hasNavigater, bool hasCommander, bool hasFireController, bool isSelfFleet, GuildFleetPersonalSetting settingFlag, bool isFlagShip)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSortState(GuildFleetDetailSortType sortType)
        {
        }

        public enum GuildFleetDetailSortType
        {
            NameDown,
            NameUp,
            ShipNameDown,
            ShipNameUp,
            LocationDown,
            LocationUp,
            FleetPositionDown,
            FleetPositionUp
        }
    }
}

