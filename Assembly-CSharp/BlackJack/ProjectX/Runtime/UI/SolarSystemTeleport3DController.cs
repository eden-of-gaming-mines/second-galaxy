﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemTeleport3DController : UIControllerBase
    {
        [AutoBind("./AnimControl", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TeleportPrefabObject;
        [AutoBind("./AnimControl/Teleport_Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera TeleportCamera;
        private Animator m_TeleportAnimator;
        private const float PeriodTime = 3f;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_PlayFadeIn;
        private static DelegateBridge __Hotfix_StopFadeIn;
        private static DelegateBridge __Hotfix_PlayFadeOut;
        private static DelegateBridge __Hotfix_StopFadeOut;
        private static DelegateBridge __Hotfix_PlayTeleportAnim;
        private static DelegateBridge __Hotfix_SetPrefabPosition;
        private static DelegateBridge __Hotfix_StopTelportAnim;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTeleportAnim()
        {
        }

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPrefabPosition()
        {
        }

        [MethodImpl(0x8000)]
        public void StopFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        public void StopFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void StopTelportAnim()
        {
        }
    }
}

