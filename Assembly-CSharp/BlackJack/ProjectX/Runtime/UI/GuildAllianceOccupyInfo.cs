﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildAllianceOccupyInfo
    {
        private int m_count;
        public uint m_ownerId;
        private readonly Dictionary<int, List<int>> m_starFieldOccupyDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetOccupySolarSystemCount;
        private static DelegateBridge __Hotfix_UpdateStarfieldOccupyInfo;
        private static DelegateBridge __Hotfix_SelectSolarSystemIdList;

        [MethodImpl(0x8000)]
        public GuildAllianceOccupyInfo(uint ownerId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetOccupySolarSystemCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> SelectSolarSystemIdList(Func<int, bool> selector)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarfieldOccupyInfo(int starfieldId, List<int> solarSystemIdList)
        {
        }
    }
}

