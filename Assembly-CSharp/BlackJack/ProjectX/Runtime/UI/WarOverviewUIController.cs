﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WarOverviewUIController : UIControllerBase
    {
        public Action EventOnBattleStatusEnd;
        public Action<GuildBattleStatus> EventOnGuildBattleStatusItemClick;
        public Action EventOnWarSolarSystemNameButtonClick;
        public Action EventOnTipBGButtonClick;
        public Action EventOnShareButtonClick;
        public Action<bool> EventOnGuildNameClick;
        private const string SovergignSideWin = "LeftWin";
        private const string AttackSideWin = "RightWin";
        private WarReportBattleStatusUIController m_battleStateCtrl;
        private WarBothSidesInfoUIController m_bothSidesInfoUICtrl;
        private WarStatusTipUIControoler m_tipCtrl;
        private const string SovergignBattleState = "Stage3";
        private const string BuildingBattleState = "Stage1";
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/LeftSovereigntyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeftGuildButton;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/RightSovereigntyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RightGuildButton;
        [AutoBind("./ProgressOfABattleGroup/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DetailInfoRoot;
        [AutoBind("./ProgressOfABattleGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShareButton;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TitleGroup;
        [AutoBind("./ProgressOfABattleGroup/TitleGroup/WinAndFailingGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_winStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/GalaxyGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WarSolarSystemNameText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/GalaxyGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WarSolarSystemNameButton;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/OffensiveGroup/OffensiveNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DestInfoText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/TimeGroup/WarStartTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WarStartTimeText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/NextGroup/NextFightingText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextFightingTimeText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/EndTimeGroup/EndTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EndTimeText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/DestroyGroup/DestroyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DestroyShipNumText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/LossGroup/LossValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LossValueText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/CrashGroup/KillKingNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillingKingNameText;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NextFightingTimeStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/DestroyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LossShipStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/LossGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LossValueStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/PandectGroup/LeftRecordItemGroup/CrashGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillingKingStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/StageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BattleStatusGroup;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LeftBattleTypeBuildingStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RightBattleTypeBuildingStateCtrl;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupA/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingAArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupA/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingAShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupB/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingBArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupB/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingBShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupC/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingCArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroupC/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingCShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupA/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingAArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupA/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingAShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupB/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingBArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupB/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingBShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupC/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingCArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroupC/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingCShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroup/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformSortGroup/LeftPlatformGroup/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LeftBuildingShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroup/PlatformBarMiddleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingArmorProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformSortGroup/RightPlatformGroup/PlatformBarTopImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RightBuildingShieldProgressBar;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/LeftPlatformNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LeftPlatformNameText;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/RightPlatformNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RightPlatformNameText;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/LeftRecordItemGroup/GraphGroup/RedBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image KillValueProgressBar;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/LeftRecordItemGroup/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SovereignSideKillValueText;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/LeftRecordItemGroup/AttackKillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackSideKillValueText;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/RightRecordItemGroup/GraphGroup/RedBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LossShipProgressBar;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/RightRecordItemGroup/LossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SovereignSideLossShipText;
        [AutoBind("./ProgressOfABattleGroup/EndGroup/RightRecordItemGroup/AttackLossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackSideLossShipText;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InBattleStateRoot;
        [AutoBind("./ProgressOfABattleGroup/EndGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BattleEndStateRoot;
        [AutoBind("./ProgressOfABattleGroup/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NpcBattleStateRoot;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/DetailInfoPanelDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StatusDummy1;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/DetailInfoPanelDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StatusDummy2;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/DetailInfoPanelDummy03", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StatusDummy3;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/DetailInfoPanelDummy04", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StatusDummy4;
        [AutoBind("./ProgressOfABattleGroup/ChartGroup/DetailInfoPanelDummy05", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StatusDummy5;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateWarOverviewPanelInfo;
        private static DelegateBridge __Hotfix_ShowGuildBattleTipPanel;
        private static DelegateBridge __Hotfix_HideGuildBattleTipPanel;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_UpdateWarResult;
        private static DelegateBridge __Hotfix_UpdateWarSituationStatisticsInfo;
        private static DelegateBridge __Hotfix_UpdateWarProcessInfo;
        private static DelegateBridge __Hotfix_UpdateWarProcessInfoState;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnTipBGButtonClick;
        private static DelegateBridge __Hotfix_OnWarSolarSystemNameButtonClick;
        private static DelegateBridge __Hotfix_OnBattleStatusEnd;
        private static DelegateBridge __Hotfix_OnGuildBattleStatusItemClick;
        private static DelegateBridge __Hotfix_OnLeftGuildNameClick;
        private static DelegateBridge __Hotfix_OnRightGuildNameClick;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        public void HideGuildBattleTipPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleStatusEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBattleStatusItemClick(GuildBattleStatus status)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeftGuildNameClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRightGuildNameClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarSolarSystemNameButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowGuildBattleTipPanel(GuildBattleStatus clickStatus, GuildBattleStatus currStatus, DateTime startTime, DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarOverviewPanelInfo(GuildBattleReportInfo reportInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWarProcessInfo(GuildBattleReportInfo reportInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWarProcessInfoState(GuildBattleReportInfo reportInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWarResult(GuildBattleReportInfo reportInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWarSituationStatisticsInfo(GuildBattleReportInfo reportInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

