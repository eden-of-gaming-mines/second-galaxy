﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcListDialogUIController : UIControllerBase
    {
        private List<NpcListItemUIController> m_npcItemList;
        private const string AssetName_NpcListItem = "NpcListItem";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnNpcItemToggleChanged;
        private float m_showThresholdValue;
        private string m_curStateName;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup toggleGroup;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./SlideHintGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hintStateCtrl;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_scrollView;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform NPCListContent;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetNpcListInfo;
        private static DelegateBridge __Hotfix_SetMenuItemScrollToViewBottom;
        private static DelegateBridge __Hotfix_GetPanelShorOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetNpcListItem;
        private static DelegateBridge __Hotfix_OnListItemToggleChanged;
        private static DelegateBridge __Hotfix_ResetContentPos;
        private static DelegateBridge __Hotfix_add_EventOnNpcItemToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnNpcItemToggleChanged;

        public event Action<int> EventOnNpcItemToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private NpcListItemUIController GetNpcListItem(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShorOrHideUIProcess(bool isShow, bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnListItemToggleChanged(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetContentPos(RectTransform selectItem)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemScrollToViewBottom(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNpcListInfo(int selectIndex, List<LBNpcTalkerBase> npcTalkerList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

