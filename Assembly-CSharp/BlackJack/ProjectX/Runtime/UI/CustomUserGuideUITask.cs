﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CustomUserGuideUITask : UITaskBase
    {
        private string m_layerResPath;
        private List<string> m_customLayerCopyPathList;
        private Action m_onLayerButtonClick;
        private CustomUserGuideUIController m_mainCtrl;
        private const string ParamKeySystemFuncDescType = "LayerResPath";
        private const string ParamKeyLayerButtonClick = "LayerButtonClick";
        private const string ParamKeyLayerDynamicCopyPathList = "LayerDynamicCopyPathList";
        public const string TaskName = "CustomUserGuideUITask";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCustomUserGuideUITask;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetCustomLayerResPath;
        private static DelegateBridge __Hotfix_OnCustomLayerButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CustomUserGuideUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCustomLayerResPath(string configPath)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCustomLayerButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartCustomUserGuideUITask(SystemFuncDescType funcType, List<string> dynamicCopyPathList = null, Action onButtonClick = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

