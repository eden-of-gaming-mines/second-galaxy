﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MenuPanelUITask : UITaskBase
    {
        private CanvasGroup m_menuPanelCanvasGroup;
        private bool m_showCharacterRedPoint;
        private int m_mailCount;
        private bool m_actionPlanRedPoint;
        protected Vector3 m_cachedPosition;
        protected bool m_isAllResLoadCompleted;
        public const string Mode_WithMotherShip = "WithMotherShip";
        public const string Mode_WithoutMotherShip = "WithoutMotherShip";
        public const string ParamKey_IsImmediate = "ParamKey_IsImmediate";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string MenuPanelUIPrefabAssetPath = "Assets/GameProject/RuntimeAssets/UI/Prefabs/Common_ABS/MenuPanelUIPrefab.prefab";
        protected MenuPanelUIController m_mainCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnChatButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCommanderBagButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWareHouseButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStarMapButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCharacterButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnMailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTeamButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnActivityButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGuildButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSettingButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnTradeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRelationButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRedPointStateChange;
        private bool m_isPauseOnPostUpdateView;
        public const string TaskName = "MenuPanelUITask";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        private const string LayerName = "MenuPanel";
        private const string ActicityFunName = "Acticity";
        private const string StarMapFunName = "StarMap";
        private const string TeamFunName = "Team";
        private const string MailFunName = "Mail";
        private const string CommaderFunName = "Commader";
        private const string GuildFunName = "Guild";
        private const string TradeFunName = "Trade";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_StartByParent;
        private static DelegateBridge __Hotfix_StartShowOrHideMenuPanel;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_UpdateActionPlanButtonUI_1;
        private static DelegateBridge __Hotfix_UpdateCharacterInfo;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_OnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RegisterEvents;
        private static DelegateBridge __Hotfix_UnRegisterEvents;
        private static DelegateBridge __Hotfix_OnNewMailArricedNtf;
        private static DelegateBridge __Hotfix_OnTeamMemberChange;
        private static DelegateBridge __Hotfix_ShowMenuPanel;
        private static DelegateBridge __Hotfix_HideMenuPanel;
        private static DelegateBridge __Hotfix_IsWithMotherShipInSameStation;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_GetUnReadMailCount;
        private static DelegateBridge __Hotfix_UpdateUIDetailState;
        private static DelegateBridge __Hotfix_UpdateActionPlanButtonUI_0;
        private static DelegateBridge __Hotfix_UpdateMailButtonUI;
        private static DelegateBridge __Hotfix_UpdateTeamUI;
        private static DelegateBridge __Hotfix_ShowMenuPanelImpl;
        private static DelegateBridge __Hotfix_HideMenuPanelImpl;
        private static DelegateBridge __Hotfix_OnCurrencyInfoNtf;
        private static DelegateBridge __Hotfix_OnChatButtonClick;
        private static DelegateBridge __Hotfix_OnCommanderBagButtonClick;
        private static DelegateBridge __Hotfix_OnWareHouseButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnTeamButtonClick;
        private static DelegateBridge __Hotfix_OnActivityButtonClick;
        private static DelegateBridge __Hotfix_OnGuildButtonClick;
        private static DelegateBridge __Hotfix_OnSettingButtonClick;
        private static DelegateBridge __Hotfix_OnTradeButtonClick;
        private static DelegateBridge __Hotfix_OnRelationButtonClick;
        private static DelegateBridge __Hotfix_OnCharacterButtonClick;
        private static DelegateBridge __Hotfix_OnMailButtonClick;
        private static DelegateBridge __Hotfix_SetIsInStationMode;
        private static DelegateBridge __Hotfix_OnGameFunctionOpen;
        private static DelegateBridge __Hotfix_ResetMailCount;
        private static DelegateBridge __Hotfix_SetActionPlanRedPoint;
        private static DelegateBridge __Hotfix_ResetCharacterRedPoint;
        private static DelegateBridge __Hotfix_UpdateGuildRedPoint;
        private static DelegateBridge __Hotfix_IsMenuPanelShowRedPoint;
        private static DelegateBridge __Hotfix_IsMenuPanelShowRedPointWithOutCharacterPoint;
        private static DelegateBridge __Hotfix_IsGuildButtonRedPointShown;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_IsAllResLoadCompleted;
        private static DelegateBridge __Hotfix_get_CurrMenuIsShowState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_EventOnChatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCommanderBagButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommanderBagButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWareHouseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWareHouseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCharacterButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCharacterButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnActivityButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnActivityButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSettingButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSettingButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTradeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTradeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRelationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRelationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRedPointStateChange;
        private static DelegateBridge __Hotfix_remove_EventOnRedPointStateChange;
        private static DelegateBridge __Hotfix_PlayFuctionOpenState;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_Activity;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_Activity;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_CommanderPackage;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_CommanderPackage;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_StarMap;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_StarMap;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_Mail;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_Mail;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_Team;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_Team;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_Guild;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_Guild;
        private static DelegateBridge __Hotfix_EnableSystemFuncType_Trade;
        private static DelegateBridge __Hotfix_UnLockSystemFuncType_Trade;

        public event Action EventOnActivityButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCharacterButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnChatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCommanderBagButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRedPointStateChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRelationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSettingButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStarMapButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTeamButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTradeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWareHouseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public MenuPanelUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_Activity(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_CommanderPackage(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_Guild(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_Mail(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_StarMap(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_Team(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSystemFuncType_Trade(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private int GetUnReadMailCount(LogicBlockMailBase mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static void HideMenuPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void HideMenuPanelImpl(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGuildButtonRedPointShown()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsMenuPanelShowRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsMenuPanelShowRedPointWithOutCharacterPoint()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsWithMotherShipInSameStation()
        {
        }

        [MethodImpl(0x8000)]
        private void OnActivityButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommanderBagButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCurrencyInfoNtf(PlayerCurrencyInfoNtf currency)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGameFunctionOpen(SystemFuncType systemFun)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewMailArricedNtf(MailNewArrivalNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRelationButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSettingButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberChange(string gameUserId, string userNam)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWareHouseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayFuctionOpenState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetCharacterRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetMailCount()
        {
        }

        [MethodImpl(0x8000)]
        private void SetActionPlanRedPoint(bool showRedPoint)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsInStationMode(bool isInStation)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private static void ShowMenuPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowMenuPanelImpl(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static MenuPanelUITask StartByParent(string mode, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartShowOrHideMenuPanel(Action<bool> onShowMenuPanelEnd, bool isShowMenuState, bool isImmediately = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_Activity(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_CommanderPackage(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_Guild(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_Mail(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_StarMap(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_Team(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSystemFuncType_Trade(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateActionPlanButtonUI()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActionPlanButtonUI(bool showRedPoint, bool showRedCount, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCharacterInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview(bool forceInit = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMailButtonUI()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTeamUI()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUIDetailState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsAllResLoadCompleted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool CurrMenuIsShowState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideMenuPanelImpl>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowMenuPanel>c__AnonStorey0
        {
            internal bool isImmediate;
            internal Action<bool> onEnd;

            internal void <>m__0(bool res)
            {
                (UIManager.Instance.FindUITaskWithName("MenuPanelUITask", true) as MenuPanelUITask).ShowMenuPanelImpl(this.isImmediate, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_Activity>c__AnonStorey2
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Acticity" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.ActivityButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.ActivityButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_CommanderPackage>c__AnonStorey3
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Commader" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.CommanderPacketButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.CommanderPacketButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_Guild>c__AnonStorey7
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Guild" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.GuildButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.GuildButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_Mail>c__AnonStorey5
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Mail" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.MailButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.MailButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_StarMap>c__AnonStorey4
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "StarMap" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.StarMapButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.StarMapButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_Team>c__AnonStorey6
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Team" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.TeamButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.TeamButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UnLockSystemFuncType_Trade>c__AnonStorey8
        {
            internal Action onEnd;
            internal MenuPanelUITask $this;

            internal void <>m__0()
            {
                List<string> functionNameList = new List<string> { "Trade" };
                List<RectTransform> showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.TradeButton.transform as RectTransform
                };
                showAreaRectTransformList = new List<RectTransform> {
                    this.$this.m_mainCtrl.TradeButton.transform as RectTransform
                };
                GameFunctionLockAndUnlockUITask.Instance.SetLockStateForGameFunction(GameFunctionLockEffectType.Rect, this.$this, "MenuPanel", functionNameList, showAreaRectTransformList, showAreaRectTransformList, false, this.onEnd, null);
            }
        }

        public enum PipeLineStateMaskType
        {
            IsImmediate
        }
    }
}

