﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class CommonEnumComparerUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;

        [Serializable, StructLayout(LayoutKind.Sequential, Size=1)]
        public struct ActivityTypeComparer : IEqualityComparer<ActivityType>
        {
            public bool Equals(ActivityType x, ActivityType y) => 
                (x == y);

            public int GetHashCode(ActivityType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct BlueprintCategoryComparer : IEqualityComparer<BlueprintCategory>
        {
            public bool Equals(BlueprintCategory x, BlueprintCategory y) => 
                (x == y);

            public int GetHashCode(BlueprintCategory obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct BlueprintTypeComparer : IEqualityComparer<BlueprintType>
        {
            public bool Equals(BlueprintType x, BlueprintType y) => 
                (x == y);

            public int GetHashCode(BlueprintType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct CurrencyTypeComparer : IEqualityComparer<CurrencyType>
        {
            public bool Equals(CurrencyType x, CurrencyType y) => 
                (x == y);

            public int GetHashCode(CurrencyType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct NpcShopItemCategoryComparer : IEqualityComparer<NpcShopItemCategory>
        {
            public bool Equals(NpcShopItemCategory x, NpcShopItemCategory y) => 
                (x == y);

            public int GetHashCode(NpcShopItemCategory obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct PlayerLanguageTypeComparer : IEqualityComparer<GuildAllianceLanguageType>
        {
            public bool Equals(GuildAllianceLanguageType x, GuildAllianceLanguageType y) => 
                (x == y);

            public int GetHashCode(GuildAllianceLanguageType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct SceneScreenEffectTypeComparer : IEqualityComparer<SceneScreenEffectType>
        {
            public bool Equals(SceneScreenEffectType x, SceneScreenEffectType y) => 
                (x == y);

            public int GetHashCode(SceneScreenEffectType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct ShipNamedEffectTypeComparer : IEqualityComparer<ShipNamedEffectType>
        {
            public bool Equals(ShipNamedEffectType x, ShipNamedEffectType y) => 
                (x == y);

            public int GetHashCode(ShipNamedEffectType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct ShipTypeComparer : IEqualityComparer<ShipType>
        {
            public bool Equals(ShipType x, ShipType y) => 
                (x == y);

            public int GetHashCode(ShipType obj) => 
                ((int) obj);
        }

        [Serializable, StructLayout(LayoutKind.Sequential, Size=1)]
        public struct StoreItemTypeComparer : IEqualityComparer<StoreItemType>
        {
            public bool Equals(StoreItemType x, StoreItemType y) => 
                (x == y);

            public int GetHashCode(StoreItemType obj) => 
                ((int) obj);
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct TechTypeComparer : IEqualityComparer<TechType>
        {
            public bool Equals(TechType x, TechType y) => 
                (x == y);

            public int GetHashCode(TechType obj) => 
                ((int) obj);
        }
    }
}

