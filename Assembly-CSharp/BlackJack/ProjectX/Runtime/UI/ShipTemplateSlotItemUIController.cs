﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipTemplateSlotItemUIController : CommonMenuItemUIController
    {
        private ILBStoreItemClient m_lbItemInfo;
        private CommonItemIconUIController m_itemCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, NpcShopItemType> EventOnItemBuyClick;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        private static DelegateBridge __Hotfix_UpdateShipTemplateSlotItem;
        private static DelegateBridge __Hotfix_GetItemIconCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemBuyClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemBuyClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemBuyClick;

        public event Action<UIControllerBase, NpcShopItemType> EventOnItemBuyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController GetItemIconCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemBuyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipTemplateSlotItem(ILBStoreItemClient item, int needCount, int totalNeedCount, bool isBuyFromMall, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

