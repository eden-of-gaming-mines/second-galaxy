﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActivityVitalityPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ConfigDataVitalityRewardInfo, int, Vector3> EventOnRewardNodeButtonClick;
        protected List<ActivityVitalityItemNodeUIController> m_rewardNodeCtrlList;
        protected const int RewardNodeCount = 5;
        [AutoBind("./VitalityRewardPanel1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeRoot1;
        [AutoBind("./VitalityRewardPanel2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeRoot2;
        [AutoBind("./VitalityRewardPanel3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeRoot3;
        [AutoBind("./VitalityRewardPanel4", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeRoot4;
        [AutoBind("./VitalityRewardPanel5", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeRoot5;
        [AutoBind("./VitalityProgressBar/VitalityProgressImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image VitalityProgressBar;
        [AutoBind("./NowVitality/VitalityNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VitalityNumberText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRewardPanelInfo;
        private static DelegateBridge __Hotfix_OnRewardNodeButtonClick;
        private static DelegateBridge __Hotfix_InitRewardNodeInfo;
        private static DelegateBridge __Hotfix_UpdateVitalityProgressBar;
        private static DelegateBridge __Hotfix_add_EventOnRewardNodeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardNodeButtonClick;

        public event Action<ConfigDataVitalityRewardInfo, int, Vector3> EventOnRewardNodeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void InitRewardNodeInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRewardNodeButtonClick(ConfigDataVitalityRewardInfo rewardInfo, int rewardIndex, Vector3 simpleInfoPos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardPanelInfo(List<ConfigDataVitalityRewardInfo> rewardInfoList, int playerVitalityCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateVitalityProgressBar(int currCnt)
        {
        }
    }
}

