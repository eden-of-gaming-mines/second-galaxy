﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class TipWindowExItemUIController : MonoBehaviour
    {
        private float m_durationTime;
        private float m_startTime;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

