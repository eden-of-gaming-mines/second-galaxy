﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class ReturnRewardItemUIController : UIControllerBase
    {
        private DailyLoginRewardInfo m_rewardData;
        private ConfigDataDailyLoginInfo m_rewardCfgData;
        private bool m_isNextAvailable;
        private DateTime m_nextAvailableTickTime;
        private DateTime m_nextDailyRefreshTime;
        private static readonly Dictionary<int, StringTableId> DayLongNumStringDic;
        [AutoBind("./CountDownText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_countDownText;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_dayText;
        [AutoBind("./NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_countText;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_IconImage;
        [AutoBind("./UIClick", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GetBtn;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateUICtrl;
        private static DelegateBridge __Hotfix_GetRewardCfgData;
        private static DelegateBridge __Hotfix_UpdateRewardView;
        private static DelegateBridge __Hotfix_UpdateItemState;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Update4CountDown;

        [MethodImpl(0x8000)]
        public ConfigDataDailyLoginInfo GetRewardCfgData()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void Update4CountDown()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardView(ConfigDataDailyLoginInfo cfgData, DailyLoginRewardInfo readonlyInfo, bool isNextAvailable, Dictionary<string, Object> resDict)
        {
        }
    }
}

