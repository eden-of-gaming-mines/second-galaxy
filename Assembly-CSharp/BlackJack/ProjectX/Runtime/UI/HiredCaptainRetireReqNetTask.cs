﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class HiredCaptainRetireReqNetTask : NetWorkTransactionTask
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ulong <CaptainInstanceId>k__BackingField;
        public int m_selFeatId;
        public int m_featBookCount;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CaptainRetireResult>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private LBStaticHiredCaptain <RetiredCaptain>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private List<StoreItemUpdateInfo> <StoreItemUpdateInfoList>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCaptainRetireAck;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainRetireResult;
        private static DelegateBridge __Hotfix_get_CaptainRetireResult;
        private static DelegateBridge __Hotfix_set_RetiredCaptain;
        private static DelegateBridge __Hotfix_get_RetiredCaptain;
        private static DelegateBridge __Hotfix_set_StoreItemUpdateInfoList;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateInfoList;

        [MethodImpl(0x8000)]
        public HiredCaptainRetireReqNetTask(ulong captainInstanceId, int featId, int featBookCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainRetireAck(int result, LBStaticHiredCaptain retiredCaptain, List<StoreItemUpdateInfo> itemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int CaptainRetireResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public LBStaticHiredCaptain RetiredCaptain
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public List<StoreItemUpdateInfo> StoreItemUpdateInfoList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

