﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class WeeklyGiftPackageRewardItemUIController : UIControllerBase
    {
        private const int MaxShowNameLength = 10;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText;
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ValueText;
        [AutoBind("./ItemIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemIcon;
        [AutoBind("./SubRank", AutoBindAttribute.InitState.NotInit, false)]
        public Image SubRank;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnRegistItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_UpdateRewardItem;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> onItemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> onItemFill)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> onItemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegistItemNeedFillEvent(Action<UIControllerBase> onItemFill)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardItem(QuestRewardInfo rewardInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

