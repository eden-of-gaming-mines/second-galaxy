﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapSimpleNavigationConfig
    {
        [Header("星域导航线的线条颜色")]
        public Color m_navigationLineColor;
        [Header("星域导航线的宽度")]
        public float m_navigationLineWidth;
        [Header("星域导航线完全消失时的缩放比")]
        public float m_scaleValueForNavigationLineAlphaMin;
        [Header("星域导航线完全不透明时的缩放比")]
        public float m_scaleValueForNavigationLineAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

