﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemSelectedTargetUIController : UIControllerBase
    {
        private SpaceObjectType m_currSelectedSpaceObjectType;
        public CommonCaptainIconUIController m_captainIconUICtrl;
        [AutoBind("./ActiveButtons", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_buttonsTrans;
        [AutoBind("./ActiveButtons/WatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button WatchToTargetButton;
        [AutoBind("./ActiveButtons/WatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WatchToTargetButtonStateCtrl;
        [AutoBind("./ActiveButtons/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button JumpToTargetButton;
        [AutoBind("./ActiveButtons/EnterStationButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button EnterStationButton;
        [AutoBind("./ActiveButtons/TeleportButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button TeleportButton;
        [AutoBind("./ActiveButtons/CloseToTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CloseToTargetButton;
        [AutoBind("./ActiveButtons/LeaveFromTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button LeaveFromTargetButton;
        [AutoBind("./ActiveButtons/AroundToTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button AroundToTargetButton;
        [AutoBind("./ActiveButtons/GuildFleetMemberJumpToTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildFleetMemberJumpToTargetButton;
        [AutoBind("./ActiveButtons/GuildFleetMemberTeleportButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildFleetMemberTeleportButton;
        [AutoBind("./ActiveButtons/GuildFleetFireFocusTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildFleetFireFocusTargetButton;
        [AutoBind("./ActiveButtons/GuildFleetProtectTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button GuildFleetProtectTargetButton;
        [AutoBind("./ActiveButtons/TalkToTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button TalkToTargetButton;
        [AutoBind("./ActiveButtons/TalkToTargetButton/Valid", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TalkToTargetButtonStateCtrl;
        [AutoBind("./ActiveButtons/InverstigateTargetButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button InverstigateTargetButton;
        [AutoBind("./ActiveButtons/InverstigateTargetButton/Valid", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController InverstigateTargetButtonStateCtrl;
        [AutoBind("./TargetInfo/NameAndBuffGroup/TargetNameInfo/TargetNameGroup/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Buffs/AttackBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackBuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Buffs/DefenseBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefenseBuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Buffs/ElectricBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ElectricBuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Buffs/MoveBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoveBuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Debuffs/AttackDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackDebuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Debuffs/DefenseDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefenseDebuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Debuffs/ElectricDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ElectricDebuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/Debuffs/MoveDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoveDebuffObj;
        [AutoBind("./TargetInfo/NameAndBuffGroup/BuffAndDebuff/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button BuffInfoButton;
        [AutoBind("./SolarSystemShipBuffDetailInfoUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_shipBuffDetailInfoTrans;
        [AutoBind("./TargetInfo/NameAndBuffGroup/TargetNameInfo/TargetNameGroup/PVPFlag", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipPVPFlagCtrl;
        [AutoBind("./TargetInfo/NameAndBuffGroup/TargetNameInfo/TargetNameGroup/PVPFlag/ClickButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button PVPFlagInfoButton;
        [AutoBind("./TargetPVPFlagInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform PVPFlagInfoPanelDummy;
        [AutoBind("./TargetInfo/TargetItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PlayerTypeUIStateCtrl;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetGroupStateCtrl;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetImageStateCtrl;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_targetImage;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup/TeamImage-", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageSubtractionSignCtrl;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup/ShipImage=", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageequalSignCtrl;
        [AutoBind("./TargetInfo/TargetItem/TargetGroup/CaptainImage+", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageAddSignCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowOrHideSelectedUI;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedTargetUI;
        private static DelegateBridge __Hotfix_PlayButtonClickEffect;
        private static DelegateBridge __Hotfix_SetWatchButtonState;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedShipBuffUIInfo;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedShipCrimeFlag;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedTargetIcon;
        private static DelegateBridge __Hotfix_SetHudIcon;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedObjTypeState;
        private static DelegateBridge __Hotfix_SetUIStateForShipTargetAndDistance;
        private static DelegateBridge __Hotfix_SetUIStateForJumpTargetTypeAndDistance;
        private static DelegateBridge __Hotfix_SetGuildFleetMemberUseStarGateButtonState;
        private static DelegateBridge __Hotfix_SetGuildFleetMemberJumpToTargetButtonState;
        private static DelegateBridge __Hotfix_UpdateGuildFleetFireFocusTargetButtonState;
        private static DelegateBridge __Hotfix_UpdateGuildFleetProtectTargetButtonState;
        private static DelegateBridge __Hotfix_ShowOtherCelestialCloseMode;
        private static DelegateBridge __Hotfix_ShowOtherCelestialFarMode;
        private static DelegateBridge __Hotfix_ShowSpaceStationCloseMode;
        private static DelegateBridge __Hotfix_ShowSpaceStationFarMode;
        private static DelegateBridge __Hotfix_ShowPlanetLikeBuildingFarMode;
        private static DelegateBridge __Hotfix_ShowPlanetLikeBuildingCloseMode;
        private static DelegateBridge __Hotfix_ShowSceneFarMode;
        private static DelegateBridge __Hotfix_ShowSceneCloseMode;
        private static DelegateBridge __Hotfix_ShowStartgateCloseMode;
        private static DelegateBridge __Hotfix_ShowStartgateFarMode;
        private static DelegateBridge __Hotfix_ShowDropBoxCloseMode;
        private static DelegateBridge __Hotfix_ShowDropBoxFarMode;
        private static DelegateBridge __Hotfix_ShowShpTargetButtonModeFromTargetInfo;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBGuild;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayButtonClickEffect(Button button)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildFleetMemberJumpToTargetButtonState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildFleetMemberUseStarGateButtonState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHudIcon(Sprite sprite, string HudSubscriptState, string HudColorState)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUIStateForJumpTargetTypeAndDistance(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUIStateForShipTargetAndDistance(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWatchButtonState(bool watched)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDropBoxCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDropBoxFarMode()
        {
        }

        [MethodImpl(0x8000)]
        internal void ShowOrHideSelectedUI(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOtherCelestialCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOtherCelestialFarMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowPlanetLikeBuildingCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowPlanetLikeBuildingFarMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSceneCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSceneFarMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShpTargetButtonModeFromTargetInfo(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSpaceStationCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSpaceStationFarMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowStartgateCloseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowStartgateFarMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCurrentSelectedObjTypeState(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCurrentSelectedShipBuffUIInfo(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCurrentSelectedShipCrimeFlag(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCurrentSelectedTargetIcon(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrentSelectedTargetUI(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildFleetFireFocusTargetButtonState(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildFleetProtectTargetButtonState(SolarSystemUITask.TargetItemUIInfo targetItemUIInfo)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LBGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

