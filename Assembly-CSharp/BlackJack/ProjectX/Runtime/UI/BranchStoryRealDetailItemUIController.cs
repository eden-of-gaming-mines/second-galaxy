﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class BranchStoryRealDetailItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnQuestGalaxyButtonClick;
        private bool m_isInit;
        private bool m_isExpand;
        private BranchStoryUITask.BranchStorySubQuestInfo m_currRealQuestInfo;
        [AutoBind("./PlotTitleGroup/NameAndJumpText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CompleteGalaxyButton;
        [AutoBind("./PlotTitleGroup/NameAndJumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CompleteGalaxyText;
        [AutoBind("./StoryText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CompleteDescText;
        [AutoBind("./PlotTitleGroup/TypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TypeText;
        [AutoBind("./PlotTitleGroup/PlotNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestNameText;
        [AutoBind("./PlotTitleGroup/PlotNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PartNumText;
        [AutoBind("./PlotTitleGroup/PlotText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestDescText;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./PlotTitleGroup/PutAndDownGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ExpandStateCtrl;
        [AutoBind("./PlotTitleGroup/PutAndDownGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShowDetailButton;
        [AutoBind("./PlotTitleGroup/PlotBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateRealQuestItemDetailInfo;
        private static DelegateBridge __Hotfix_GetItemUIProcess;
        private static DelegateBridge __Hotfix_OnShowDetialButtonClick;
        private static DelegateBridge __Hotfix_OnCompleteGalaxyButtonClick;
        private static DelegateBridge __Hotfix_ExpandRealQuestDetail4Item;
        private static DelegateBridge __Hotfix_add_EventOnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_get_CurrItemInfo;

        public event Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnQuestGalaxyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ExpandRealQuestDetail4Item(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetItemUIProcess(bool isComplete, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompleteGalaxyButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnShowDetialButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRealQuestItemDetailInfo(string categoryStr, BranchStoryUITask.BranchStorySubQuestInfo questInfo, Dictionary<string, Object> resDict, bool resetExpandState = false)
        {
        }

        public BranchStoryUITask.BranchStorySubQuestInfo CurrItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

