﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildTradeOrderEditUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildTradeOrderInfo> EventOnAddTradeMoneyBtnClick;
        private List<GuildTradePortExtraInfo> m_cacheGuildTradePortExtraInfoList;
        private GuildTradeOrderInfo m_preOrderInfo;
        private List<GuildTradePurchaseOrderTransportLogInfo> m_guildTradePurchaseOrderTransportLogInfoList;
        private GuildTradeCreateInfoData m_createPurchaseInfoData;
        private GuildTradeEditInfoData m_editPurchaseInfoData;
        private GuildTradeTransportInfoData m_transportInfoData;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        public GuildTradeOrderRevocationConfirmUIController m_guildTradeOrderRevocationConfirmUIController;
        private GuildTradeOrderEditUIController m_guildTradeOrderEditUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKey_GuildTradeOrderEditResult = "GuildTradeOrderEditResult";
        public const string ParamKey_GuildTradeItemConfigList = "GuildTradeItemConfigList";
        public const string ParamKey_GuildTradePurchaseInfo = "GuildTradePurchaseInfo";
        public const string ParamKey_GuildTradePortInfoList = "GuildTradePortInfoList";
        public const string ParamKey_GuildTradePurchaseOrderTransportLogInfoList = "GuildTradePurchaseOrderTransportLogInfoList";
        public const string ParamKey_GuildTradePortLocateSolarSystemId = "GuildTradePortLocateSolarSystemId";
        public const string ParamKey_GuildTradePreOrderInfo = "PreOrderInfo";
        public const string GuildTradeOrderEditUITaskMode_CreatePurchaseOrder = "CreatePurchaseOrder";
        public const string GuildTradeOrderEditUITaskMode_ModifyPurchaseOrder = "ModifyPurchaseOrder";
        public const string GuildTradeOrderEditUITaskMode_CreateTransportOrder = "CreateTransportOrder";
        public const string TaskName = "GuildTradeOrderEditUITask";
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildTradeOrderEditUITaskForCreatePurchaseOrder;
        private static DelegateBridge __Hotfix_StartGuildTradeOrderEditUITaskForEditPurchaseOrder;
        private static DelegateBridge __Hotfix_StartGuildTradeOrderEditUITaskForCreateTransportOrder;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchasePriceAddButtonClick;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchasePriceAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchasePriceSubtractButtonLongPressing;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchasePriceSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchaseNumberAddButtonClick;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchaseNumberAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchaseNumberSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PurchaseNumberSubtractButtonLongPressing;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_DropdownClick;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_OnCreatePurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PurchasePriceAddButtonClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PurchasePriceSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PurchaseNumberAddButtonClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PurchaseNumberSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_AmountInput;
        private static DelegateBridge __Hotfix_OnEditPurchaseOrder_PriceInput;
        private static DelegateBridge __Hotfix_OnCreateTransportOrder_TransportNumberAddButtonClick;
        private static DelegateBridge __Hotfix_OnCreateTransportOrder_TransportNumberSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnCreateTransportOrder_DropDownClick;
        private static DelegateBridge __Hotfix_OnCreateTransportOrer_AmountInput;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnYesButtonClick;
        private static DelegateBridge __Hotfix_OnDeletePurchaseOrderYesButtonClick;
        private static DelegateBridge __Hotfix_DeletePurchaseOrder;
        private static DelegateBridge __Hotfix_OnTaxDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTaxDetailInfoCloseButtonClick;
        private static DelegateBridge __Hotfix_OnTitleDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTitleDetailCloseButtonClick;
        private static DelegateBridge __Hotfix_CheckOrderAmount;
        private static DelegateBridge __Hotfix_GetOrderMaxAmount;
        private static DelegateBridge __Hotfix_CheckOrderPrice;
        private static DelegateBridge __Hotfix_GetOrderMaxPrice;
        private static DelegateBridge __Hotfix_GetOrderMinPrice;
        private static DelegateBridge __Hotfix_CheckTransportCount;
        private static DelegateBridge __Hotfix_PauseAndReturn;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_OnGuildTradeMoneyAddButton;
        private static DelegateBridge __Hotfix_SendGuildCurrencyInfoReq;
        private static DelegateBridge __Hotfix_SendGuildTradePurchaseOrderDelete;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_add_EventOnAddTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<GuildTradeOrderInfo> EventOnAddTradeMoneyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildTradeOrderEditUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private int CheckOrderAmount(long amount)
        {
        }

        [MethodImpl(0x8000)]
        private int CheckOrderPrice(long price)
        {
        }

        [MethodImpl(0x8000)]
        private int CheckTransportCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void DeletePurchaseOrder(GuildTradePurchaseInfo purchaseOrderInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private long GetOrderMaxAmount()
        {
        }

        [MethodImpl(0x8000)]
        private long GetOrderMaxPrice()
        {
        }

        [MethodImpl(0x8000)]
        private long GetOrderMinPrice()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_AmountInput(long amount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_DropdownClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PriceInput(long price)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchaseNumberAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchaseNumberAddButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchaseNumberSubtractButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchaseNumberSubtractButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchasePriceAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchasePriceAddButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchasePriceSubtractButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreatePurchaseOrder_PurchasePriceSubtractButtonLongPressing(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrder_DropDownClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrder_TransportNumberAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrder_TransportNumberSubtractButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateTransportOrer_AmountInput(long amount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeletePurchaseOrderYesButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_AmountInput(long amount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PriceInput(long price)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PurchaseNumberAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PurchaseNumberSubtractButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PurchasePriceAddButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditPurchaseOrder_PurchasePriceSubtractButtonClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradeMoneyAddButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTaxDetailInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTaxDetailInfoCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleDetailCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleDetailInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnYesButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PauseAndReturn(GuildTradeOrderEditResult editResult)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildCurrencyInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildTradePurchaseOrderDelete(ulong instanceId, int solarSystemId, ushort version, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradeOrderEditUITask StartGuildTradeOrderEditUITaskForCreatePurchaseOrder(UIIntent returnToIntent, List<ConfigDataNormalItemInfo> itemConfigList, int solarSystemId, GuildTradeOrderInfo preOrderInfo, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildTradeOrderEditUITaskForCreateTransportOrder(UIIntent returnToIntent, GuildTradePurchaseInfo guildTradePurchaseInfo, List<GuildTradePortExtraInfo> tradePortInfoList, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradeOrderEditUITask StartGuildTradeOrderEditUITaskForEditPurchaseOrder(UIIntent returnToIntent, GuildTradePurchaseInfo guildTradePurchaseInfo, List<GuildTradePurchaseOrderTransportLogInfo> logInfoList, GuildTradeOrderInfo preOrderInfo, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DeletePurchaseOrder>c__AnonStorey1
        {
            internal GuildTradePurchaseInfo purchaseOrderInfo;
            internal GuildTradeOrderEditUITask $this;

            internal void <>m__0()
            {
                this.$this.PlayUIProcess(this.$this.m_guildTradeOrderRevocationConfirmUIController.GetReverseIntroducePanelUIProcess(), true, null, false);
            }

            internal void <>m__1()
            {
                this.$this.PlayUIProcess(this.$this.m_guildTradeOrderRevocationConfirmUIController.GetCloseIntroducePanelUIProcess(), true, null, false);
            }

            internal void <>m__2()
            {
                this.$this.SendGuildTradePurchaseOrderDelete(this.purchaseOrderInfo.m_instanceId, this.purchaseOrderInfo.m_solarSystemId, this.purchaseOrderInfo.m_version, delegate (bool res) {
                    if (res)
                    {
                        this.$this.PauseAndReturn(GuildTradeOrderEditUITask.GuildTradeOrderEditResult.DeletePurchaseOrderSuccess);
                    }
                });
            }

            internal void <>m__3(bool res)
            {
                if (res)
                {
                    this.$this.PauseAndReturn(GuildTradeOrderEditUITask.GuildTradeOrderEditResult.DeletePurchaseOrderSuccess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PauseAndReturn>c__AnonStorey2
        {
            internal GuildTradeOrderEditUITask.GuildTradeOrderEditResult editResult;
            internal GuildTradeOrderEditUITask $this;
            private static Action<bool> <>f__am$cache0;

            internal void <>m__0(bool ret)
            {
                this.$this.Pause();
                UIIntentCustom prevTaskIntent = (this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent as UIIntentCustom;
                prevTaskIntent.SetParam("GuildTradeOrderEditResult", this.editResult);
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = delegate (bool res) {
                    };
                }
                UIManager.Instance.ReturnUITaskWithPrepare(prevTaskIntent, <>f__am$cache0);
            }

            private static void <>m__1(bool res)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(Task task)
            {
                GetGuildTradePortExtraInfoListNetTask task2 = task as GetGuildTradePortExtraInfoListNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    int result = task2.Result;
                    if (result == 0)
                    {
                        this.onPrepareEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onPrepareEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildCurrencyInfoReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildCurrencyInfoReqNetTask task2 = task as GuildCurrencyInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        this.onEnd(false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildTradePurchaseOrderDelete>c__AnonStorey4
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SendGuildTradePurchaseOrderDeleteNetTask task2 = task as SendGuildTradePurchaseOrderDeleteNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    int result = task2.Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(result == 0);
                    }
                }
            }
        }

        public class GuildTradeCreateInfoData
        {
            public int m_curSelectedItemIndex;
            public List<ConfigDataNormalItemInfo> m_itemInfoConfigList;
            public int m_guildTradePortLocateSolarSystemId = -1;
            public ulong m_purchaseCount;
            public ulong m_purchasePrice;
            private static DelegateBridge _c__Hotfix_ctor;

            public GuildTradeCreateInfoData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class GuildTradeEditInfoData
        {
            public GuildTradePurchaseInfo m_guildTradePurchaseInfo;
            public List<GuildTradePurchaseOrderTransportLogInfo> m_guildTradePurchaseOrderTransportLogInfoList;
            public ulong m_purchaseCount;
            public ulong m_purchasePrice;
            private static DelegateBridge _c__Hotfix_ctor;

            public GuildTradeEditInfoData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum GuildTradeOrderEditResult
        {
            None,
            Cancel,
            Error,
            CreatePurchaseOrderSuccess,
            EditPurchaseOrderSuccess,
            DeletePurchaseOrderSuccess,
            CreateTransportOrderSuccess
        }

        public class GuildTradeOrderInfo
        {
            public string m_editMode;
            public ulong m_amount;
            public int m_curPortIndex;
            public ulong m_price;
            public ulong m_totalMoney;
            public int m_curDropdownIndex;
            private static DelegateBridge _c__Hotfix_ctor;

            public GuildTradeOrderInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class GuildTradeTransportInfoData
        {
            public GuildTradePurchaseInfo m_guildTradePurchaseInfo;
            public List<GuildTradePortExtraInfo> m_cacheGuildTradePortExtraInfoList;
            public int m_curSelectedPortIndex;
            public ulong m_transportCount;
            public GuildTradeOrderEditUITask.TransportMaxCountData m_transportMaxData;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_RefreshTransportSpaceInfo;

            public GuildTradeTransportInfoData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void RefreshTransportSpaceInfo()
            {
                DelegateBridge bridge = __Hotfix_RefreshTransportSpaceInfo;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    if (this.m_transportMaxData == null)
                    {
                        this.m_transportMaxData = new GuildTradeOrderEditUITask.TransportMaxCountData();
                    }
                    this.m_transportMaxData.MaxShipSpace = TradeUIHelper.GetGuildTradePortShipCapacity(this.m_cacheGuildTradePortExtraInfoList[this.m_curSelectedPortIndex].m_solarSystemId);
                    float singleItemOccupySize = TradeUIHelper.GetSingleItemOccupySize(this.m_guildTradePurchaseInfo.m_configId, this.m_guildTradePurchaseInfo.m_itemType);
                    this.m_transportMaxData.MaxOwnCount = TradeUIHelper.GetGuildTradeItemOwnNum(StoreItemType.StoreItemType_Normal, this.m_guildTradePurchaseInfo.m_configId);
                    this.m_transportMaxData.MaxPurchaseCount = this.m_guildTradePurchaseInfo.m_count;
                    this.m_transportMaxData.MaxStorageCount = TradeUIHelper.GetMaxTransportNumBySpace(singleItemOccupySize, this.m_transportMaxData.MaxShipSpace);
                    float[] values = new float[] { this.m_transportMaxData.MaxOwnCount, this.m_transportMaxData.MaxPurchaseCount, this.m_transportMaxData.MaxStorageCount };
                    this.m_transportMaxData.MaxCount = (long) Mathf.Min(values);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            RefreshAll,
            RefreshCreateOrder,
            RefreshEditOrder,
            RefreshTransportOrder,
            RefreshDropdown
        }

        public class TransportMaxCountData
        {
            public int MaxStorageCount;
            public int MaxPurchaseCount;
            public long MaxOwnCount;
            public long MaxCount;
            public float MaxShipSpace;
            private static DelegateBridge _c__Hotfix_ctor;

            public TransportMaxCountData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

