﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CelestialStarMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starNameText;
        [AutoBind("./Detail/SunAgeItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starAgeText;
        [AutoBind("./Detail/BrightnessItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_brightnessText;
        [AutoBind("./Detail/SunRadiusItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starRadiusText;
        [AutoBind("./Detail/SunSpectrumItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starSpectrumText;
        [AutoBind("./Detail/TemperatureItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_temperatureText;
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        private GDBStarInfo m_gdbStarInfo;
        private static DelegateBridge __Hotfix_SetStarInfoByGDBData;
        private static DelegateBridge __Hotfix_GetGDBStarInfo;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetMenuItemName;

        [MethodImpl(0x8000)]
        public GDBStarInfo GetGDBStarInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarInfoByGDBData(GDBSolarSystemInfo solarSystemInfo, GDBStarInfo starInfo)
        {
        }
    }
}

