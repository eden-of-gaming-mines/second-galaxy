﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class LongPressDesc : MonoBehaviour
    {
        [Header("长按时数值变化速度的阶段描述:")]
        public List<LongPressSingleDesc> m_descList;
        [Header("长按触发时间(S/次):")]
        public float m_longPressTriggerTime;

        [Serializable]
        public class LongPressSingleDesc
        {
            [Header("开始时间(S,例：1为长按1s后刷新，速度变为设定值):")]
            public int m_startTime;
            [Header("每次长按触发变化量(V/次):")]
            public int m_longPressChangeSpeed;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

