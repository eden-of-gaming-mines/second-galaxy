﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SpaceStationUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FakeLBStoreItem> EventOnRecommendItemClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FakeLBStoreItem> EventOnRecommendItemClose;
        public Action<bool> EventOnApplicationPause;
        private const string m_buildingTagAssetName = "SpaceStationBuildingTag";
        private GameObject m_buildingTagInstance;
        private Dictionary<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType, SpaceStationBuildingTagController> m_buildingTagDic;
        public CommonNewAreaInfoUIController m_newAreaInfoUICtrl;
        private CommonQuestInfoUIController m_questInfoUICtrl;
        private CommonItemIconUIController[] m_CommonItemIconCtr;
        private List<RecommendWeaponOrChipItemInfoUIController> m_recommedInfoUICtrlList;
        private RectTransform m_canvasRecTrans;
        public SpaceStationPlayerListUIController m_playerListUICtrl;
        public RescueSignalButtonUIControllerBase m_rescueSignalButtonCtrl;
        private const string EmojiInfoTxtName = "EmojiInfo";
        private const string EmojiShowAssetName = "EmojiShowImage";
        private EmojiParseController m_emojiHelper;
        [AutoBind("./TalkFrame/Mask/MsgTextA/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameTextA;
        [AutoBind("./TalkFrame/Mask/MsgTextA/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_chatTextA;
        [AutoBind("./TalkFrame/Mask/MsgTextA", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_textA;
        [AutoBind("./TalkFrame/Mask/MsgTextB/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameTextB;
        [AutoBind("./TalkFrame/Mask/MsgTextB/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_chatTextB;
        [AutoBind("./TalkFrame/Mask/MsgTextB", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_textB;
        [AutoBind("./FunctionButtons/ChatMessageImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ChatUnReadTip;
        [AutoBind("./FunctionButtons/ChatMessageImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChatUnReadCountText;
        [AutoBind("./FunctionButtons/MenuFunctionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MenuFunctionButton;
        [AutoBind("./FunctionButtons/MenuFunctionButton", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController MenuFunctionButtonStateCtrl;
        [AutoBind("./FunctionButtons/MenuFunctionButton/MailNumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MenuFunctionButtonRedPoint;
        [AutoBind("./FunctionButtons/GuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildButton;
        [AutoBind("./FunctionButtons/GuildButton", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_guildButtonStateCtrl;
        [AutoBind("./FunctionButtons/GuildButton/MailNumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildButtonRedPoint;
        [AutoBind("./FunctionButtons/PlayerListFunctionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PlayerListFunctionButton;
        [AutoBind("./FunctionButtons/PlayerListFunctionButton", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController PlayerListFunctionStateCtrl;
        [AutoBind("./FunctionButtons/PlayerListFunctionButton/ChatMessageImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text PlayerListCountText;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/PlayerListPanel", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController PlayerListDummyStateCtrl;
        [AutoBind("./Margin/PlayerListPanel/SortTypePanel", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController SortTypeGroupPanel;
        [AutoBind("./StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StrikeButton;
        [AutoBind("./Margin/MenuPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject MenuPanelDummy;
        [AutoBind("./BackgroundImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SpaceStaionBGButton;
        [AutoBind("./Margin/QuestButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ActionButton;
        [AutoBind("./Margin/QuestButton/NumberBG", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActionButtonCountStateCtrl;
        [AutoBind("./Margin/QuestButton/NumberBG/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActionButtonCountText;
        [AutoBind("./Margin/SOSButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RescueButtonGo;
        [AutoBind("./Margin/ConsultButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button UserInfoButton;
        [AutoBind("./Margin/ConsultButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UserInfoRedPoint;
        [AutoBind("./MotherShipPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject MotherShipPanelDummy;
        [AutoBind("./BuildingTagDummy", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject BuildingTagDummy;
        [AutoBind("./StationArriveInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject StationArriveInfoDummy;
        [AutoBind("./QuestInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject QuestInfoDummy;
        [AutoBind("./RecommenItemInfoDummy01", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject RecommenItemInfoDummy01;
        [AutoBind("./RecommenItemInfoDummy02", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject RecommenItemInfoDummy02;
        [AutoBind("./FunctionButtons/ChatMsgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_chatMsgBtn;
        public float m_chatScrollTime;
        public float m_chatStayTime;
        private float m_chatStainTime;
        private float m_chatMoveTime;
        public bool m_newMessage;
        private const float m_chatLineHight = 48f;
        private Transform m_nextChatTrans;
        private Transform m_currentChatTrans;
        private bool m_nextIsA;
        private const string ButtonState_UnSelected = "UnSelected";
        private const string ButtonState_Selected = "Selected";
        private const string ButtonState_Gray = "Gray";
        private const string PlayerListState_PanelOpen = "PanelOpen";
        private const string PlayerListState_PanelClose = "PanelClose";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnApplicationPause;
        private static DelegateBridge __Hotfix_SetSpaceStationNPCList;
        private static DelegateBridge __Hotfix_UpdateBuildingTagInfo;
        private static DelegateBridge __Hotfix_UpdateMenuFunctionButtonState;
        private static DelegateBridge __Hotfix_UpdateGuildButtonState;
        private static DelegateBridge __Hotfix_UpdatePlayerListFunctionButtonState;
        private static DelegateBridge __Hotfix_UpdateCommonButtonsState;
        private static DelegateBridge __Hotfix_GetPlayerListOpenOrHideProcess;
        private static DelegateBridge __Hotfix_SetPlayerListCount;
        private static DelegateBridge __Hotfix_GetMenuPanelDummyPosition;
        private static DelegateBridge __Hotfix_GetMotherShipPanelDummyPosition;
        private static DelegateBridge __Hotfix_SetChatUnReadTipState;
        private static DelegateBridge __Hotfix_SetMenuFunctionButtonRedPoint;
        private static DelegateBridge __Hotfix_SetGuildButtonRedPoint;
        private static DelegateBridge __Hotfix_EnableSpaceStationBGImage;
        private static DelegateBridge __Hotfix_SetEmojiAndText4ChatItem;
        private static DelegateBridge __Hotfix_ShowSpaceStationInfo;
        private static DelegateBridge __Hotfix_ShowSpaceStationInfoImpl;
        private static DelegateBridge __Hotfix_ShowQuestMsg;
        private static DelegateBridge __Hotfix_HideQuestMsg;
        private static DelegateBridge __Hotfix_ShowQuestCompleteInfo;
        private static DelegateBridge __Hotfix_UpdateActionButtonInfo;
        private static DelegateBridge __Hotfix_UpdateUserInfoButtonState;
        private static DelegateBridge __Hotfix_GetDisplayNameFromBuildingType;
        private static DelegateBridge __Hotfix_DoChatScroll;
        private static DelegateBridge __Hotfix_SetNewMessage;
        private static DelegateBridge __Hotfix_FixChatMessagePosition;
        private static DelegateBridge __Hotfix_InitRecommendWeaponOrChipItemInfoUIController;
        private static DelegateBridge __Hotfix_HidRecommendItem;
        private static DelegateBridge __Hotfix_HidRecommendItemAll;
        private static DelegateBridge __Hotfix_OnRecommendItemClick;
        private static DelegateBridge __Hotfix_OnRecommendItemCloseClick;
        private static DelegateBridge __Hotfix_UpdateRecommendItem;
        private static DelegateBridge __Hotfix_add_EventOnRecommendItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnRecommendItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnRecommendItemClose;
        private static DelegateBridge __Hotfix_remove_EventOnRecommendItemClose;
        private static DelegateBridge __Hotfix_get_CanvasRect;

        public event Action<FakeLBStoreItem> EventOnRecommendItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<FakeLBStoreItem> EventOnRecommendItemClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool DoChatScroll()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableSpaceStationBGImage(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void FixChatMessagePosition()
        {
        }

        [MethodImpl(0x8000)]
        private string GetDisplayNameFromBuildingType(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetMenuPanelDummyPosition()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetMotherShipPanelDummyPosition()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPlayerListOpenOrHideProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        public void HideQuestMsg()
        {
        }

        [MethodImpl(0x8000)]
        public void HidRecommendItem(FakeLBStoreItem storeItem)
        {
        }

        [MethodImpl(0x8000)]
        public void HidRecommendItemAll()
        {
        }

        [MethodImpl(0x8000)]
        private void InitRecommendWeaponOrChipItemInfoUIController(GameObject dummy)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationPause(bool isPause)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecommendItemClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecommendItemCloseClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChatUnReadTipState(bool active, int unReadCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEmojiAndText4ChatItem(Text text, string inputStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildButtonRedPoint(bool showRedPoint)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuFunctionButtonRedPoint(bool showRedPoint)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNewMessage(string pname, string content)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlayerListCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceStationNPCList()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestCompleteInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestMsg(string nameStr, string detailStr)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSpaceStationInfo(string stationName, int solarSystemId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator ShowSpaceStationInfoImpl(string stationName, int solarSystemId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActionButtonInfo(bool isRedPoint, bool isRedCount, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingTagInfo(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType buildingType, Vector3 goViewportPos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCommonButtonsState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildButtonState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuFunctionButtonState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerListFunctionButtonState(bool isshow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRecommendItem(List<FakeLBStoreItem> itemList, bool weaponClose, bool chipClose, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUserInfoButtonState(bool hasRedPoint)
        {
        }

        private Rect CanvasRect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HidRecommendItem>c__AnonStorey1
        {
            internal FakeLBStoreItem storeItem;

            internal bool <>m__0(RecommendWeaponOrChipItemInfoUIController item) => 
                ReferenceEquals(item.m_FakeLBStoreItem, this.storeItem);
        }

        [CompilerGenerated]
        private sealed class <ShowSpaceStationInfoImpl>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string stationName;
            internal int solarSystemId;
            internal Dictionary<string, UnityEngine.Object> resDict;
            internal SpaceStationUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

