﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonPropertyUtil
    {
        protected static List<string> m_propertyValueStrList = new List<string>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNegativeProperty;
        private static DelegateBridge __Hotfix_GetPropertyNameById;
        private static DelegateBridge __Hotfix_GetPropertyValueFormatStrByIdAndBaseValue;
        private static DelegateBridge __Hotfix_GetPropertyValueFormatStrById_0;
        private static DelegateBridge __Hotfix_GetPropertyStrListByProperyInfoList;
        private static DelegateBridge __Hotfix_GetPropertyValueFormatStrById_1;
        private static DelegateBridge __Hotfix_GetEquipDescDynamicPropertyString_0;
        private static DelegateBridge __Hotfix_GetEquipDescDynamicPropertyString_1;
        private static DelegateBridge __Hotfix_GetDestEffectEquipDescDynamicPropertyString_0;
        private static DelegateBridge __Hotfix_GetDestEffectEquipDescDynamicPropertyString_1;
        private static DelegateBridge __Hotfix_GetSelfEffectEquipDescDynamicPropertyString_0;
        private static DelegateBridge __Hotfix_GetSelfEffectEquipDescDynamicPropertyString_1;
        private static DelegateBridge __Hotfix_GetEquipDecsBasePropretyString;
        private static DelegateBridge __Hotfix_CalcDestEffectEquipDescBasePropertyString;
        private static DelegateBridge __Hotfix_CalcSelfEffectEquipDescBasePropertyString;
        private static DelegateBridge __Hotfix_CalcPreviewPropertiesWithAlgorithm;
        private static DelegateBridge __Hotfix_GetBuffPropertyDescByBuffInfo;
        private static DelegateBridge __Hotfix_GetBuffInstanceParamEffectValueFormatStrByBufType;
        private static DelegateBridge __Hotfix_GetPropertyValueFromBuffConfig;
        private static DelegateBridge __Hotfix_GetTacticalEquipDescDynamicPropertyString;
        private static DelegateBridge __Hotfix_GetTacticalEquipDecsStaticPropretyString;
        private static DelegateBridge __Hotfix_GetTacticalEquipEffectDescDynamicPropertyString;
        private static DelegateBridge __Hotfix_GetTacticalEquipDescDynamicPropertyStrForExtraParam;
        private static DelegateBridge __Hotfix_CalcTacticalEquipEffectDescStaticPropertyString;
        private static DelegateBridge __Hotfix_GetGuildPropertyValueFormatStrById;
        private static DelegateBridge __Hotfix_GetCharacterBasePropertyIconPathByPropCategory;
        private static DelegateBridge __Hotfix_GetPropertyCategorySimpleDescStr;

        [MethodImpl(0x8000)]
        protected static string CalcDestEffectEquipDescBasePropertyString(ConfigDataShipEquipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcPreviewPropertiesWithAlgorithm(PropertiesId propertiesId, float currValue)
        {
        }

        [MethodImpl(0x8000)]
        protected static string CalcSelfEffectEquipDescBasePropertyString(ConfigDataShipEquipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string CalcTacticalEquipEffectDescStaticPropertyString(ConfigDataShipTacticalEquipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetBuffInstanceParamEffectValueFormatStrByBufType(BufType bufType, PropertiesId pid, float value)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBuffPropertyDescByBuffInfo(LBShipFightBuf buff)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCharacterBasePropertyIconPathByPropCategory(PropertyCategory categoty)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetDestEffectEquipDescDynamicPropertyString(LBEquipGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetDestEffectEquipDescDynamicPropertyString(LBFakeInspaceWeaponEquipGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipDecsBasePropretyString(ConfigDataShipEquipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipDescDynamicPropertyString(LBEquipGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipDescDynamicPropertyString(LBFakeInspaceWeaponEquipGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildPropertyValueFormatStrById(int guildPid, float internalValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPropertyCategorySimpleDescStr(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPropertyNameById(int pid)
        {
        }

        [MethodImpl(0x8000)]
        public static List<PropertyValueStringPair> GetPropertyStrListByProperyInfoList(List<CommonPropertyInfo> propetyInfoList, int factor = 1)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPropertyValueFormatStrById(int pid, float internalValue)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetPropertyValueFormatStrById(int pid, float pvalue, out string valueStr, out string unitStr)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPropertyValueFormatStrByIdAndBaseValue(int pid, float baseValue)
        {
        }

        [MethodImpl(0x8000)]
        protected static float GetPropertyValueFromBuffConfig(ConfigDataBufInfo bufConf, PropertiesId pid)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetSelfEffectEquipDescDynamicPropertyString(LBEquipGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetSelfEffectEquipDescDynamicPropertyString(LBFakeInspaceWeaponEquipGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTacticalEquipDecsStaticPropretyString(ConfigDataShipTacticalEquipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTacticalEquipDescDynamicPropertyStrForExtraParam(LBTacticalEquipGroupBase group, string desc, List<ParamInfo> paramInfo, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTacticalEquipDescDynamicPropertyString(LBTacticalEquipGroupBase group, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetTacticalEquipEffectDescDynamicPropertyString(LBTacticalEquipGroupBase group, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsNegativeProperty(int pid)
        {
        }
    }
}

