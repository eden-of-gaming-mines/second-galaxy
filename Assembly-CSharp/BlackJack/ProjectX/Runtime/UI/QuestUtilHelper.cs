﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class QuestUtilHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TryStartAutoCompleteQuestDialogInStation;
        private static DelegateBridge __Hotfix_IsQuestCanPopDlgCompleteInCurrStation;
        private static DelegateBridge __Hotfix_StartQuestTalkDialogTask;
        private static DelegateBridge __Hotfix_SendQuestAcceptNetReq_0;
        private static DelegateBridge __Hotfix_SendQuestAcceptNetReq_1;
        private static DelegateBridge __Hotfix_SendFactionCreditQuestAcceptReq;
        private static DelegateBridge __Hotfix_IsCanAcceptQuestByNpc;
        private static DelegateBridge __Hotfix_StartQuestAccpetDilaog;
        private static DelegateBridge __Hotfix_StartRandomQuestAcceptDialog;
        private static DelegateBridge __Hotfix_GetRandomQuestLBInfo;
        private static DelegateBridge __Hotfix_SendRandomQuestAcceptNetReq;
        private static DelegateBridge __Hotfix_ConfirmProcessingWaitConfirmQuests;

        [MethodImpl(0x8000)]
        public static void ConfirmProcessingWaitConfirmQuests(UITaskBase blockUITask = null)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSignalManualQuest GetRandomQuestLBInfo(ConfigDataQuestInfo questConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsCanAcceptQuestByNpc(int questId, out NpcDNId npc)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsQuestCanPopDlgCompleteInCurrStation(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendFactionCreditQuestAcceptReq(UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, Action<int, int> onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendQuestAcceptNetReq(UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, Action<bool> onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendQuestAcceptNetReq(UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, Action<int, int> onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendRandomQuestAcceptNetReq(int questId, Action<bool, int> onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartQuestAccpetDilaog(UIManager.UIActionQueueItem uiActionItem, UnAcceptedQuestUIInfo unAcceptQuestInfo, Action onStart, Action<int, int> onAcceptAction, Action onRefuseAction, Action<bool> onProcessNormalEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartQuestTalkDialogTask(LBProcessingQuestBase proQuest, Action<bool> onDlgEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartRandomQuestAcceptDialog(int questId, Action onDialogStart = null, Action<bool, int> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryStartAutoCompleteQuestDialogInStation(int questInstanceId, Action<bool> onDlgEndAction = null)
        {
        }

        [CompilerGenerated]
        private sealed class <GetRandomQuestLBInfo>c__AnonStorey8
        {
            internal ConfigDataQuestInfo questConfInfo;

            internal bool <>m__0(LBSignalManualQuest emergencySingal) => 
                (emergencySingal.GetQuestInfo().ID == this.questConfInfo.ID);
        }

        [CompilerGenerated]
        private sealed class <SendFactionCreditQuestAcceptReq>c__AnonStorey3
        {
            internal Action<int, int> onEndAction;

            internal void <>m__0(Task task)
            {
                FactionCreditQuestAcceptReqNetTask task2 = task as FactionCreditQuestAcceptReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AcceptQuestResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AcceptQuestResult, true, false);
                    }
                    if (this.onEndAction != null)
                    {
                        this.onEndAction(task2.AcceptQuestResult, task2.QuestInstanceId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendQuestAcceptNetReq>c__AnonStorey1
        {
            internal Action<bool> onEndAction;

            internal void <>m__0(Task task)
            {
                QuestAcceptReqNetTask task2 = task as QuestAcceptReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AcceptQuestResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AcceptQuestResult, true, false);
                    }
                    if (this.onEndAction != null)
                    {
                        this.onEndAction(task2.AcceptQuestResult == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendQuestAcceptNetReq>c__AnonStorey2
        {
            internal Action<int, int> onEndAction;

            internal void <>m__0(Task task)
            {
                QuestAcceptReqNetTask task2 = task as QuestAcceptReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AcceptQuestResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AcceptQuestResult, true, false);
                    }
                    if (this.onEndAction != null)
                    {
                        this.onEndAction(task2.AcceptQuestResult, task2.QuestInstanceId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendRandomQuestAcceptNetReq>c__AnonStorey9
        {
            internal Action<bool, int> onEndAction;

            internal void <>m__0(Task task)
            {
                RandomQuestAcceptReqNetTask task2 = task as RandomQuestAcceptReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AcceptQuestResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AcceptQuestResult, true, false);
                    }
                    if (this.onEndAction != null)
                    {
                        this.onEndAction(task2.AcceptQuestResult == 0, task2.ProcessingQuestInstanceId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartQuestAccpetDilaog>c__AnonStorey4
        {
            internal Action<bool> onProcessNormalEnd;
            internal UIManager.UIActionQueueItem uiActionItem;
            internal Action<int, int> onAcceptAction;
            internal Action onRefuseAction;
            internal Action onStart;
            internal UnAcceptedQuestUIInfo unAcceptQuestInfo;

            internal void <>m__0(bool result)
            {
                if (this.onProcessNormalEnd != null)
                {
                    this.onProcessNormalEnd(result);
                }
                if (this.uiActionItem != null)
                {
                    this.uiActionItem.OnEnd();
                }
            }

            internal void <>m__1(int rst, int questInsId)
            {
                if (this.onProcessNormalEnd != null)
                {
                    this.onProcessNormalEnd(rst == 0);
                }
                if (this.onAcceptAction != null)
                {
                    this.onAcceptAction(rst, questInsId);
                }
                if (this.uiActionItem != null)
                {
                    this.uiActionItem.OnEnd();
                }
            }

            internal void <>m__2()
            {
                if (this.onProcessNormalEnd != null)
                {
                    this.onProcessNormalEnd(false);
                }
                if (this.onRefuseAction != null)
                {
                    this.onRefuseAction();
                }
                if (this.uiActionItem != null)
                {
                    this.uiActionItem.OnEnd();
                }
            }

            internal void <>m__3(bool result)
            {
                <StartQuestAccpetDilaog>c__AnonStorey5 storey = new <StartQuestAccpetDilaog>c__AnonStorey5 {
                    <>f__ref$4 = this,
                    result = result
                };
                if (this.onStart != null)
                {
                    this.onStart();
                }
                QuestAcceptDialogUITask.ShowNormalQuestAcceptDialog(null, this.unAcceptQuestInfo, new Action<int, int>(storey.<>m__0), null, true);
            }

            private sealed class <StartQuestAccpetDilaog>c__AnonStorey5
            {
                internal bool result;
                internal QuestUtilHelper.<StartQuestAccpetDilaog>c__AnonStorey4 <>f__ref$4;

                internal void <>m__0(int rst, int questInsId)
                {
                    if (this.<>f__ref$4.onProcessNormalEnd != null)
                    {
                        this.<>f__ref$4.onProcessNormalEnd(this.result);
                    }
                    if (this.<>f__ref$4.onAcceptAction != null)
                    {
                        this.<>f__ref$4.onAcceptAction(rst, questInsId);
                    }
                    if (this.<>f__ref$4.uiActionItem != null)
                    {
                        this.<>f__ref$4.uiActionItem.OnEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartQuestTalkDialogTask>c__AnonStorey0
        {
            internal int questInstanceId;
            internal Action<bool> onDlgEndAction;
            internal int questConfId;
            internal int npcId;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(int result)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartRandomQuestAcceptDialog>c__AnonStorey6
        {
            internal Action onDialogStart;
            internal Action<bool, int> onEnd;
        }

        [CompilerGenerated]
        private sealed class <StartRandomQuestAcceptDialog>c__AnonStorey7
        {
            internal LBSignalManualQuest lbQuestSignal;
            internal QuestUtilHelper.<StartRandomQuestAcceptDialog>c__AnonStorey6 <>f__ref$6;

            internal void <>m__0(bool result, int questInsId)
            {
                if (!result)
                {
                    this.<>f__ref$6.onEnd(false, 0);
                }
                else
                {
                    if (this.<>f__ref$6.onDialogStart != null)
                    {
                        this.<>f__ref$6.onDialogStart();
                    }
                    QuestAcceptDialogUITask.ShowRandomQuestAcceptDialog(null, this.lbQuestSignal, (rst, insId) => this.<>f__ref$6.onEnd(true, insId), () => this.<>f__ref$6.onEnd(false, 0), true);
                }
            }

            internal void <>m__1(int rst, int insId)
            {
                this.<>f__ref$6.onEnd(true, insId);
            }

            internal void <>m__2()
            {
                this.<>f__ref$6.onEnd(false, 0);
            }

            internal void <>m__3(int rst, int insId)
            {
                this.<>f__ref$6.onEnd(true, insId);
            }

            internal void <>m__4()
            {
                this.<>f__ref$6.onEnd(false, 0);
            }
        }
    }
}

