﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class VirtualBuffEffectPropertyUIController : UIControllerBase
    {
        private DateTime m_expriedTime;
        private DateTime m_nextRefreshTime;
        [AutoBind("./BuffEffectNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./BuffEffectlNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_numberText;
        [AutoBind("./BuffEffectChanedValueText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_frequentChangeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetPropertyName;
        private static DelegateBridge __Hotfix_SetPropertyNumber_1;
        private static DelegateBridge __Hotfix_SetPropertyNumber_0;
        private static DelegateBridge __Hotfix_UpdateDateTime;
        private static DelegateBridge __Hotfix_SetPropertyState;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public void SetPropertyName(string propertyName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyNumber(DateTime propertyNumber)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyNumber(string propertyNumber)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyState(bool isUp)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDateTime()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

