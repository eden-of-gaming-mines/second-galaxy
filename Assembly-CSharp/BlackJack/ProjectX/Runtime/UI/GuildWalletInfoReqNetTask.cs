﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildWalletInfoReqNetTask : NetWorkTransactionTask
    {
        public List<ProGuildDonateRankingItemInfo> m_guildDonateRankingList;
        public List<GuildCurrencyLogInfo> m_guildDonateLogList;
        public string m_walletAnnouncement;
        public int m_solarSystemCount;
        public ulong m_informationPointAddValue;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildWalletInfoAck;

        [MethodImpl(0x8000)]
        private void OnGuildWalletInfoAck(GuildWalletInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

