﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FaceListItemUIController> EventOnFaceItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFaceFilled;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEditNameEnd;
        private string m_currMode;
        public const string UIMODE_CREATECHARACTER = "CreateCharacter";
        public const string UIMODE_CHARACTEREXIST = "CharacterExist";
        public const string UIMODE_BOARDING = "Boarding";
        public const string UIMODE_DISABLEENTERGAMEBUTTON = "DisableEnterButton";
        private const string CharacterUIState_ShowFirst = "ShowFirst";
        private const string CharacterUIState_ShowSecond = "ShowSecond";
        private List<int> m_faceItemList;
        private Dictionary<string, UnityEngine.Object> m_faceItemResDict;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject Root;
        [AutoBind("./CommanderImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image AvatarImage;
        [AutoBind("./EntryGameButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx EnterGameButton;
        [AutoBind("./EntryGameButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text EnterGameButtonText;
        [AutoBind("./InputNameGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject CharacterNameRoot;
        [AutoBind("./ForceChoosePanel", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject FactionRoot;
        [AutoBind("./InputNameGroup/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField CharacterNameInput;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/NEF", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx NEFToggle;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/USD", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx USDToggle;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/OE", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx OEToggle;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/RR", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx RRToggle;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/TR", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx TRToggle;
        [AutoBind("./ForceChoosePanel/Detail/NameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text FactionNameText;
        [AutoBind("./ForceChoosePanel/Detail/VoiceButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx FactionVoiceSwitchButton;
        [AutoBind("./ForceChoosePanel/Detail/InfoButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx FactionTextSwitchButton;
        [AutoBind("./ForceChoosePanel/Detail/DescButton", AutoBindAttribute.InitState.NotInit, false)]
        private Button FactionWin;
        [AutoBind("./ForceChoosePanel/Detail/DescButton/Scroll View/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text FactionText;
        [AutoBind("./InputNameGroup/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx RandNameButton;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Scientist", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx ScientistProf;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Engineer", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx EngineerProf;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Soldier", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx SoldierProf;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Explorer", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleEx ExplorerProf;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/Detail/Scroll View/Viewport/Content/InfoPanel/JobTextGroup/JobBackgroundText/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text JobNameText;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/Detail/Scroll View/Viewport/Content/InfoPanel/JobDescText", AutoBindAttribute.InitState.NotInit, false)]
        private Text JobDescText;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/Detail/Scroll View/Viewport/Content/InfoPanel/JobSpecialtyDescText", AutoBindAttribute.InitState.NotInit, false)]
        private Text JobSpecialtyDescText;
        [AutoBind("./ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject ConfirmWin;
        [AutoBind("./ConfirmPanel/Detail/NameTitleText/NameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text ConfirmNameText;
        [AutoBind("./ConfirmPanel/Detail/CountryTitleText/ForceText", AutoBindAttribute.InitState.NotInit, false)]
        private Text ConfirmFactionText;
        [AutoBind("./ConfirmPanel/Detail/CountryTitleText/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image ConfirmFactionImg;
        [AutoBind("./ConfirmPanel/Detail/JobTitleText/JobText", AutoBindAttribute.InitState.NotInit, false)]
        private Text ConfirmProfessionText;
        [AutoBind("./ConfirmPanel/Detail/JobTitleText/JobIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image ConfirmProfessionImg;
        [AutoBind("./ConfirmPanel/Detail/FaceMask/CommanderImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image ConfirmAvatarImg;
        [AutoBind("./ConfirmPanel/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx ConfirmCancelButton;
        [AutoBind("./ConfirmPanel/EntryGameButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx ConfirmOKButton;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/NEF/FrameImage/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image NEFImg;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/USD/FrameImage/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image USDImg;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/OE/FrameImage/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image OEImg;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/RR/FrameImage/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image RRImg;
        [AutoBind("./ForceChoosePanel/Detail/ForceToggles/TR/FrameImage/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image TRImg;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Scientist/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image ScientistImg;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Engineer/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image EngineerImg;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Soldier/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image SoldierImg;
        [AutoBind("./CommanderJobInfoAndFacePanel/CommanderInfoPanel/JobToggles/Explorer/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image ExplorerImg;
        [AutoBind("./CommanderJobInfoAndFacePanel/ChooseFacePanel/FaceGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject FaceChooseRoot;
        [AutoBind("./CommanderJobInfoAndFacePanel/ChooseFacePanel/FaceGroup", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./CommanderJobInfoAndFacePanel/ChooseFacePanel/FaceGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemObjPool;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SwitchMode;
        private static DelegateBridge __Hotfix_SetCharacterName;
        private static DelegateBridge __Hotfix_GetCharacterName;
        private static DelegateBridge __Hotfix_OnEditCharacterNameEnd;
        private static DelegateBridge __Hotfix_GetSoundEnabled;
        private static DelegateBridge __Hotfix_SetEnableSound;
        private static DelegateBridge __Hotfix_SetFactionText;
        private static DelegateBridge __Hotfix_SetFactionIcon;
        private static DelegateBridge __Hotfix_OpenFactionTextWin;
        private static DelegateBridge __Hotfix_CloseFactionTextWin;
        private static DelegateBridge __Hotfix_SwitchFactionTextWin;
        private static DelegateBridge __Hotfix_GetCurrFactionOnId;
        private static DelegateBridge __Hotfix_GetCurrProfessionOnId;
        private static DelegateBridge __Hotfix_OpenConfirmWin;
        private static DelegateBridge __Hotfix_CloseConfirmWin;
        private static DelegateBridge __Hotfix_SetShowCharacterMode;
        private static DelegateBridge __Hotfix_SetCreateCharacterMode;
        private static DelegateBridge __Hotfix_SetAvatarRes;
        private static DelegateBridge __Hotfix_SetProfessionToggle;
        private static DelegateBridge __Hotfix_EnableEnterGameButton;
        private static DelegateBridge __Hotfix_SetEnterGameButtonText;
        private static DelegateBridge __Hotfix_OnFaceListItemClick;
        private static DelegateBridge __Hotfix_OnFaceItemFill;
        private static DelegateBridge __Hotfix_UpdateFaceItemList;
        private static DelegateBridge __Hotfix_ClearFaceItemSelectState;
        private static DelegateBridge __Hotfix_SetFaceItemSelectState;
        private static DelegateBridge __Hotfix_add_EventOnFaceItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnFaceItemClick;
        private static DelegateBridge __Hotfix_add_EventOnFaceFilled;
        private static DelegateBridge __Hotfix_remove_EventOnFaceFilled;
        private static DelegateBridge __Hotfix_add_EventOnEditNameEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEditNameEnd;

        public event Action EventOnEditNameEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFaceFilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<FaceListItemUIController> EventOnFaceItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearFaceItemSelectState()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseConfirmWin()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseFactionTextWin()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableEnterGameButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetCharacterName()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetCurrFactionOnId()
        {
        }

        [MethodImpl(0x8000)]
        public ProfessionType GetCurrProfessionOnId()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetSoundEnabled()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditCharacterNameEnd(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFaceItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFaceListItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenConfirmWin(string name, GrandFaction faction, ProfessionType profession, int resId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenFactionTextWin()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAvatarRes(int resId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacterName(string playerName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCreateCharacterMode()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnableSound(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnterGameButtonText(string btTxt)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFaceItemSelectState(int resId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFactionIcon(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFactionText(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProfessionToggle(ProfessionType profession)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShowCharacterMode()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchFactionTextWin()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFaceItemList(List<int> faceList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

