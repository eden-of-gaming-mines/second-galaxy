﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SignalStrikeCaptainItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ulong <CaptainInstanceId>k__BackingField;
        public ScrollItemBaseUIController m_scrollCtrl;
        public HangarShipListItemUIController m_shipIconCtrl;
        public CommonCaptainIconUIController m_captainIconCtrl;
        private const string CaptainIconItemAssetName = "CaptainIconItem";
        private const string ShipIconItemAssetName = "ShipIconItem";
        private const string CaptainShipState_Normal = "Normal";
        private const string CaptainShipState_Broken = "CanNotStrike";
        private const string CaptainShipState_Striking = "Striking";
        private const string SelectionState_Select = "Select";
        private const string SelectionState_Unselect = "UnSelect";
        private const string ShipAbilityState_Normal = "Normal";
        private const string ShipAbilityState_HighLight = "HighLight";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ShipIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipIconDummy;
        [AutoBind("./CharacterInfo/CaptainIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SelectionButton;
        [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelectionStateCtrl;
        [AutoBind("./CharacterInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Attack", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipAttack;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Attack/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipAttackValue;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Collect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipCollect;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Collect/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipCollectValue;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Defense", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipDefense;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Defense/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipDefenseValue;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Storage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipStorage;
        [AutoBind("./ShipAbilityInfo/AbilityGroup/Storage/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipStorageValue;
        [AutoBind("./CaptainFeatInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainFeatInfo;
        [AutoBind("./CaptainFeatInfo/TimeReduction", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeReduction;
        [AutoBind("./CaptainFeatInfo/TimeReduction/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeReductionValueText;
        [AutoBind("./CaptainFeatInfo/AwardAddition", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AwardAddition;
        [AutoBind("./CaptainFeatInfo/AwardAddition/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AwardAdditionValueText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_SetCaptainInfo;
        private static DelegateBridge __Hotfix_SetCaptianSelectState;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainInfo(LBSignalDelegateBase lbSignal, LBStaticHiredCaptain captain, bool isSelected, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptianSelectState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

