﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class GuildBuildingItemButtonGroupUIController : UIControllerBase
    {
        [AutoBind("./RecycleButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecycleButton;
        [AutoBind("./UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./JumpToButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx JumpToButton;
        [AutoBind("./DetailInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailInfoButton;
        [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AttackButton;
        [AutoBind("./ActivateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ActivateButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildBuildingItemButtonGroupStateCtrl;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingItemButtonGroup;
        private static DelegateBridge __Hotfix_CanOperateActivate;
        private static DelegateBridge __Hotfix_UpdateButtonGroupForGuildBuilding;
        private static DelegateBridge __Hotfix_UpdateButtonGroupForControlHubBuilding;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        private bool CanOperateActivate(uint guildId, GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateButtonGroupForControlHubBuilding(uint guildId, GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateButtonGroupForGuildBuilding(uint guildId, GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingItemButtonGroup(uint guildId, GuildBuildingInfo buildingInfo)
        {
        }
    }
}

