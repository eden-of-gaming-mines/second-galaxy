﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class CommonItemDetailInfoAmmoUIController : CommonItemDetailInfoHasEquipedEffectUIBase
    {
        public string m_propertyUIPrefabAssetName;
        [AutoBind("./Viewport/Content/EquipedEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropertyItemRoot;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/LightingDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectricalDamageResistNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/LightingDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElectricalDamageResistImage;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KineticDamageResistNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image KineticDamageResistImage;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HeatDamageResistNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageCompose/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image HeatDamageResistImage;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageBasic", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageBasicParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageBasic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DamageBasicNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageBasic/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageBasicArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageBasic/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageBasicArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageModify/", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageMultiParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageModify/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DamageMultiNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageModify/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageMultiArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/DamageModify/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DamageMultiArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/AttackCD", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackCDParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/AttackCD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackCDNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/AttackCD/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackCDArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/AttackCD/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackCDArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeMax", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMaxParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeMax/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeMaxNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeMax/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMaxArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeMax/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMaxArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeModify", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireRangeMaxMultiParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeModify/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FireRangeMaxMultiNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeModify/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireRangeMaxMultiArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeModify/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireRangeMaxMultiArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneSpeed", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneSpeedParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneSpeed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DroneSpeedNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneSpeed/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneSpeedArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneSpeed/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneSpeedArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/FightTime", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FightTimeParent;
        [AutoBind("./Viewport/Content/CapabilityPanel/FightTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FightTimeNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/FightTime/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FightTimeArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/FightTime/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FightTimeArrowUp;
        [AutoBind("./Viewport/Content/BasedataPanel/PackedSize/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackedSizeNumber;
        [AutoBind("./Viewport/Content/BasedataPanel/Tech/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechText;
        [AutoBind("./Viewport/Content/BasedataPanel/Quality/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QualityText;
        [AutoBind("./Viewport/Content/BasedataPanel/SizeType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SizeTypeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateAmmonInfoUI;
        private static DelegateBridge __Hotfix_UpdateMissileInfoUI;
        private static DelegateBridge __Hotfix_UpdateDroneInfoUI;
        private static DelegateBridge __Hotfix_HideAllPropertyUI;
        private static DelegateBridge __Hotfix_UpdateDamageComposeUIAmmon;
        private static DelegateBridge __Hotfix_UpdateDamageComposeUIMissile;
        private static DelegateBridge __Hotfix_UpdateDamageComposeUIDrone;
        private static DelegateBridge __Hotfix_UpdatePackedSizeAmmon;
        private static DelegateBridge __Hotfix_UpdatePackedSizeMissile;
        private static DelegateBridge __Hotfix_UpdatePackedSizeDrone;
        private static DelegateBridge __Hotfix_UpdateRankAndSubRank;
        private static DelegateBridge __Hotfix_UpdateDamageComposeUI;
        private static DelegateBridge __Hotfix_UpdateDamageBasicMissile;
        private static DelegateBridge __Hotfix_UpdateRangeMaxMissile;
        private static DelegateBridge __Hotfix_UpdateSpeedMissile;
        private static DelegateBridge __Hotfix_UpdateDamageBasicDrone;
        private static DelegateBridge __Hotfix_UpdateAttackCDDrone;
        private static DelegateBridge __Hotfix_UpdateRangMaxDrone;
        private static DelegateBridge __Hotfix_UpdateSpeedDrone;
        private static DelegateBridge __Hotfix_UpdateFightTimeDrone;
        private static DelegateBridge __Hotfix_UpdateDamageMulti;
        private static DelegateBridge __Hotfix_UpdateFireRangeMaxMulti;

        [MethodImpl(0x8000)]
        private void HideAllPropertyUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAmmonInfoUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAttackCDDrone(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageBasicDrone(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageBasicMissile(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageComposeUI(float kineticDamage, float heatDamage, float electricDamage)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageComposeUIAmmon(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageComposeUIDrone(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageComposeUIMissile(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageMulti(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDroneInfoUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFightTimeDrone(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFireRangeMaxMulti(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMissileInfoUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePackedSizeAmmon(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePackedSizeDrone(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePackedSizeMissile(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRangeMaxMissile(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRangMaxDrone(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRankAndSubRank(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSpeedDrone(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSpeedMissile(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }
    }
}

