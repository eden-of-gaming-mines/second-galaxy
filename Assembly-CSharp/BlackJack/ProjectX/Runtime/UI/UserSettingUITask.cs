﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UserSettingUITask : UITaskBase
    {
        private UserSettingUIController m_mainCtrl;
        private LanguageType m_languageType;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private int m_uploadLogClickTimes;
        private const int UploadLogClickTimes = 5;
        private DateTime m_uploadLogClickInvaildTime;
        private float m_uploadLogClickIntervalTime;
        public const string TaskName = "UserSettingUITask";
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitPipeLineCtx;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_PushAllNeedUILayer;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnGMClick;
        private static DelegateBridge __Hotfix_OnQuitButtonClick;
        private static DelegateBridge __Hotfix_OnTroopInviteOpenToggleClick;
        private static DelegateBridge __Hotfix_OnTroopInviteOffToggleClick;
        private static DelegateBridge __Hotfix_OnMusicOpenToggleClick;
        private static DelegateBridge __Hotfix_OnMusicOffToggleClick;
        private static DelegateBridge __Hotfix_OnSoundEffectOpenToggleClick;
        private static DelegateBridge __Hotfix_OnSoundEffectOffToggleClick;
        private static DelegateBridge __Hotfix_OnIllegaAttackOpenToggleClick;
        private static DelegateBridge __Hotfix_OnIllegaAttackOffToggleClick;
        private static DelegateBridge __Hotfix_OnLowFrameRateOpenToggleClick;
        private static DelegateBridge __Hotfix_OnLowFrameRateOffToggleClick;
        private static DelegateBridge __Hotfix_OnShipStateOpenToggleClick;
        private static DelegateBridge __Hotfix_OnShipStateOffToggleClick;
        private static DelegateBridge __Hotfix_OnSpecialEffectsOpenToggleClick;
        private static DelegateBridge __Hotfix_OnSpecialEffectsOffToggleClick;
        private static DelegateBridge __Hotfix_OnShadowOpenToggleClick;
        private static DelegateBridge __Hotfix_OnShadowOffToggleClick;
        private static DelegateBridge __Hotfix_OnDamageNumberOpenToggleClick;
        private static DelegateBridge __Hotfix_OnDamageNumberOffToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicEffectLevelLowToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicEffectLevelMediumToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicEffectLevelHighToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicQualityLevelLowToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicQualityLevelMediumToggleClick;
        private static DelegateBridge __Hotfix_OnGraphicQualityLevelHighToggleClick;
        private static DelegateBridge __Hotfix_OnRecordOn;
        private static DelegateBridge __Hotfix_OnRecordOff;
        private static DelegateBridge __Hotfix_OnExplainButtonClick;
        private static DelegateBridge __Hotfix_OnHintGroupCloseButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageApplyButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageGroupConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageGroupCloseButtonClick;
        private static DelegateBridge __Hotfix_OnGiftOpenButtonClick;
        private static DelegateBridge __Hotfix_OnUseGiftCodeBtnClick;
        private static DelegateBridge __Hotfix_OnLanguageSwitchOpenButtonClick;
        private static DelegateBridge __Hotfix_OnTitleButtonClick;
        private static DelegateBridge __Hotfix_OnLogButtonClick;
        private static DelegateBridge __Hotfix_OnUploadLogPanelCloseButtonClick;
        private static DelegateBridge __Hotfix_OnUploadLogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnHelpButtonClick;
        private static DelegateBridge __Hotfix_OnAdministrationButonClick;
        private static DelegateBridge __Hotfix_OnServerButtonClick;
        private static DelegateBridge __Hotfix_OnPrivacyButtonClick;
        private static DelegateBridge __Hotfix_OnLock4SurroundOpenToggleClick;
        private static DelegateBridge __Hotfix_OnLock4SurroundCloseToggleClick;
        private static DelegateBridge __Hotfix_StartUserSettingUITask;
        private static DelegateBridge __Hotfix_InitPanelFromCurrentSetting;
        private static DelegateBridge __Hotfix_FlushSetting2File;
        private static DelegateBridge __Hotfix_get_UserSettingCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public UserSettingUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void FlushSetting2File()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPanelFromCurrentSetting()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAdministrationButonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDamageNumberOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDamageNumberOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnExplainButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftOpenButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGMClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicEffectLevelHighToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicEffectLevelLowToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicEffectLevelMediumToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicQualityLevelHighToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicQualityLevelLowToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGraphicQualityLevelMediumToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHelpButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHintGroupCloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIllegaAttackOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIllegaAttackOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageApplyButtonClick(LanguageType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupCloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupConfirmButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageSwitchOpenButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLock4SurroundCloseToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLock4SurroundOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowFrameRateOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowFrameRateOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMusicOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMusicOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrivacyButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuitButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecordOff(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecordOn(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShadowOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShadowOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStateOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStateOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundEffectOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundEffectOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpecialEffectsOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpecialEffectsOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTroopInviteOffToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTroopInviteOpenToggleClick(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUploadLogConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUploadLogPanelCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUseGiftCodeBtnClick(string cdKey)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void PushAllNeedUILayer()
        {
        }

        [MethodImpl(0x8000)]
        public static UserSettingUITask StartUserSettingUITask(UIIntent returnToIntent, bool isShow = true, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockUserSettingClient UserSettingCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

