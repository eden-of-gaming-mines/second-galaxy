﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BlackMarketResourceShopUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestBlackMarketManagerUIPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRequestRefreshMoneyInfo;
        private List<ConfigDataBlackMarketShopItemInfo> m_blackMarketShopItemList;
        private ResourceShopItemCategory m_resourceShopCategory;
        private int m_selectItemIndex;
        private int m_selectItemNeedBuyGroupCount;
        private List<ResourceShopItemCategory> m_shopItemCategorieList;
        public const string ModeResourceShop = "ResourceShop";
        public const string ModeIrShop = "IrShop";
        protected static string ParamKeyShopCategory;
        protected static string ParamKeyRefreshAll;
        protected static string ParamKeyIsShow;
        protected static string ParamKeyIsHide;
        protected static string ParamKeyIsPlayAniImmedite;
        protected static string ParamKeyDefaultSelectItem;
        protected static string ParamKeyDefaultSelectItemType;
        private readonly List<ResourceShopItemCategory> m_shopItemCategories4IrShopMode;
        private readonly List<ResourceShopItemCategory> m_shopItemCategories4ResourceShopMode;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4PersonalShop;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4GuildShop;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4CatalystItem;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4AccelerationItem;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4CaptainItem;
        private List<ConfigDataBlackMarketShopItemInfo> m_itemList4OtherItem;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private BlackMarketResourcesShopUIController m_blackMarketResourcesShopUIController;
        private BlackMarketResourcesShopItemInfoUIController m_blackMarketResourcesShopItemInfoUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTaskWithPrepare;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_PrepareData4IrShop;
        private static DelegateBridge __Hotfix_PrepareData4ResourceShop;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache4ResourceShop;
        private static DelegateBridge __Hotfix_UpdateDataCache4IrShop;
        private static DelegateBridge __Hotfix_UpdateShopItemList4Category;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_OnShopCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnResourceShopItemFill;
        private static DelegateBridge __Hotfix_OnResourceShopItemClick;
        private static DelegateBridge __Hotfix_OnResourceShopItemIconClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_ShowBlackMarkResourceShop;
        private static DelegateBridge __Hotfix_HideBlackMarkResourceShop;
        private static DelegateBridge __Hotfix_GetCanBuyCount;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SetDefaultSelectItem;
        private static DelegateBridge __Hotfix_GetShopItemListByCategory;
        private static DelegateBridge __Hotfix_PrepareShopItemListByCategory;
        private static DelegateBridge __Hotfix_IsGuildResourceShopVaild;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_PauseGuildInfoPanel;
        private static DelegateBridge __Hotfix_RequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestBlackMarketManagerUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestBlackMarketManagerUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_remove_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_get_ResourceShopCategory;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbGuild;
        private static DelegateBridge __Hotfix_get_GuildCurrency;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<Action, bool> EventOnRequestBlackMarketManagerUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestRefreshMoneyInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public BlackMarketResourceShopUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private int GetCanBuyCount(ConfigDataBlackMarketShopItemInfo selectShopItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private List<ConfigDataBlackMarketShopItemInfo> GetShopItemListByCategory(ResourceShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void HideBlackMarkResourceShop(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsGuildResourceShopVaild()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnResourceShopItemClick(UIControllerBase itemCtrlBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResourceShopItemFill(UIControllerBase itemCtrlBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResourceShopItemIconClick(UIControllerBase ctrlBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopCategoryButtonClick(ResourceShopItemCategory shopCategory)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void PauseGuildInfoPanel(Action evntAfterPause, bool isIgnoreAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareData4IrShop(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareData4ResourceShop(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private List<ConfigDataBlackMarketShopItemInfo> PrepareShopItemListByCategory(ref List<ConfigDataBlackMarketShopItemInfo> itemList, ResourceShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestRefreshMoneyInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDefaultSelectItem()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBlackMarkResourceShop(bool immedite, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTaskWithPrepare(ResourceShopItemCategory category, Action<bool> onPrepareEnd, Action onResLoadEnd = null, Action<bool> onPipelineEnd = null, ILBItem defaultItem = null, BlackMarketShopItemType defaultItemType = 7)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache4IrShop()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache4ResourceShop()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShopItemList4Category()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ResourceShopItemCategory ResourceShopCategory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private static LogicBlockGuildClient LbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private GuildCurrencyInfo GuildCurrency
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideBlackMarkResourceShop>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal BlackMarketResourceShopUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey1
        {
            internal BlackMarketBuyItemReqNetTask netTask;
            internal BlackMarketResourceShopUITask $this;

            internal void <>m__0(Task task)
            {
                if (!((BlackMarketBuyItemReqNetTask) task).IsNetworkError)
                {
                    if (this.netTask.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(this.netTask.Result, true, false);
                        if (this.$this.m_resourceShopCategory == ResourceShopItemCategory.ResourceShopItemCategory_GuildShop)
                        {
                            GuildStoreUITask.SendGuildMoneyInfoReq(delegate (bool result) {
                                this.$this.RequestRefreshMoneyInfo();
                                this.$this.EnablePipelineStateMask(BlackMarketResourceShopUITask.PipeLineStateMaskType.SelectShopItem);
                                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                            }, true, true);
                        }
                    }
                    else if (this.$this.m_resourceShopCategory == ResourceShopItemCategory.ResourceShopItemCategory_GuildShop)
                    {
                        GuildStoreUITask.SendGuildMoneyInfoReq(delegate (bool result) {
                            TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_BuyItemSucceed, false, new object[0]);
                            this.$this.RequestRefreshMoneyInfo();
                            this.$this.EnablePipelineStateMask(BlackMarketResourceShopUITask.PipeLineStateMaskType.SelectShopItem);
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }, true, true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_BuyItemSucceed, false, new object[0]);
                        this.$this.RequestRefreshMoneyInfo();
                        this.$this.EnablePipelineStateMask(BlackMarketResourceShopUITask.PipeLineStateMaskType.SelectShopItem);
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    }
                }
            }

            internal void <>m__1(bool result)
            {
                this.$this.RequestRefreshMoneyInfo();
                this.$this.EnablePipelineStateMask(BlackMarketResourceShopUITask.PipeLineStateMaskType.SelectShopItem);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }

            internal void <>m__2(bool result)
            {
                TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_BuyItemSucceed, false, new object[0]);
                this.$this.RequestRefreshMoneyInfo();
                this.$this.EnablePipelineStateMask(BlackMarketResourceShopUITask.PipeLineStateMaskType.SelectShopItem);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnResourceShopItemIconClick>c__AnonStorey0
        {
            internal FakeLBStoreItem fakeItem;
            internal Vector3 simpleItemInfoPanelPos;
            internal string mode;
            internal BlackMarketResourceShopUITask $this;

            internal void <>m__0(UIIntent intent)
            {
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.fakeItem, intent, true, this.simpleItemInfoPanelPos, this.mode, ItemSimpleInfoUITask.PositionType.UseInput, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            SwitchShopCategory,
            SelectShopItem,
            IsShow,
            IsHide,
            IsPlayAniImmedite,
            NeedUpdateDynamicRes,
            CanNotUpdate
        }
    }
}

