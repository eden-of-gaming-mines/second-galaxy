﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ChatLocationInfoUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        public const string ChatInfoKey = "LocationChatInfo";
        private ChatContentLocation m_chatInfo;
        private UIIntent m_returnUIIntent;
        private ChatLocationInfoUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_ShowSSDetailInfoWnd;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_HideSSDetailInfoWnd;
        private static DelegateBridge __Hotfix_get_m_playerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ChatLocationInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideSSDetailInfoWnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_EnterStarMapForSSButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_JumpButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowSSDetailInfoWnd(GDBStarfieldsInfo starfieldInfo, GDBStargroupInfo stargroupInfo, GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext m_playerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSDetailInfoWnd_JumpButtonClick>c__AnonStorey0
        {
            internal GDBSolarSystemInfo solarSystemInfo;
            internal ChatLocationInfoUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess p, bool r)
            {
            }
        }
    }
}

