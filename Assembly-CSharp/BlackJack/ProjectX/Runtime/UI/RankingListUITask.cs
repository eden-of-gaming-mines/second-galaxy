﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class RankingListUITask : UITaskBase
    {
        private RankingListType m_selectRankingListType;
        private RankingListUIController m_mainCtrl;
        private RankingTargetInfo m_firstPlayerForKillRecordCountType;
        private RankingTargetInfo m_firstPlayerForKillRecordBindMoneyValueType;
        private RankingTargetInfo m_firstPlayerForContinueKillCountType;
        private RankingTargetInfo m_firstPlayerForCharacterGradeType;
        private RankingTargetInfo m_firstGuildForGuildEvaluateScoreType;
        private RankingTargetInfo m_firstAllianceForAllianceEvaluateScoreType;
        private DateTime m_lastPersonalTotalRankingListInfoReqTime;
        private DateTime m_lastGuildTotalRankingListInfoReqTime;
        private DateTime m_lastAllianceTotalRankingListInfoReqTime;
        private const float TotalRankingListInfoReqIntervalMin = 5f;
        private IUIBackgroundManager m_backgroundManager;
        private RankingListInfo m_rankingListInfoForCurrType;
        public const string TaskModePersonTotalRankingList = "PersonTotalRankingList";
        public const string TaskModePersonSingleRankingList = "PersonSingleRankingList";
        public const string TaskModeAllianceTotalRankingList = "AllianceTotalRankingList";
        public const string TaskModeAllianceSingleRankingList = "AllianceSingleRankingList";
        public const string TaskModeGuildTotalRankingList = "GuildTotalRankingList";
        public const string TaskModeGuildSingleRankingList = "GuildSingleRankingList";
        public const string IntentParamKeyRankingListType = "RankingListType";
        public const string IntentParamKeyBackgroundManager = "BackgroundManager";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "RankingListUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartRankingListUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_PersonTotalRankingList;
        private static DelegateBridge __Hotfix_UpdateDataCache_SingleRankingList;
        private static DelegateBridge __Hotfix_UpdateDataCache_AllianceTotalRankingList;
        private static DelegateBridge __Hotfix_UpdateDataCache_GuildTotalRankingList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_PersonTotalRankingList;
        private static DelegateBridge __Hotfix_CollectedDynamicResForLode_RankingTarget;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_PersonSingleRankingList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AllianceTotalRankingList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AllianceSingleRankingList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_GuildTotalRankingList;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_GuildSingleRankingList;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_PersonTotalRankingList;
        private static DelegateBridge __Hotfix_UpdateView_SingleRankingList;
        private static DelegateBridge __Hotfix_UpdateView_AllianceTotalRankingList;
        private static DelegateBridge __Hotfix_UpdateView_GuildTotalRankingList;
        private static DelegateBridge __Hotfix_PlayUIProcessForTotalRankingList;
        private static DelegateBridge __Hotfix_OnPersonRankingButtonClick;
        private static DelegateBridge __Hotfix_OnAllianceRankingButtonClick;
        private static DelegateBridge __Hotfix_OnGuildRankingButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnEnterSingleRankingListButtonClick;
        private static DelegateBridge __Hotfix_OnRankingItemDetailButtonClick;
        private static DelegateBridge __Hotfix_RegUIEvent;
        private static DelegateBridge __Hotfix_UnRegUIEvent;
        private static DelegateBridge __Hotfix_SetToPersonalTotalRankMode;
        private static DelegateBridge __Hotfix_SetToAllianceTotalRankMode;
        private static DelegateBridge __Hotfix_SetToGuildTotalRankMode;
        private static DelegateBridge __Hotfix_StartPersonalTotalRankingInfoReq;
        private static DelegateBridge __Hotfix_StartGuildTotalRankingInfoReq;
        private static DelegateBridge __Hotfix_StartAllianceTotalRankingInfoReq;
        private static DelegateBridge __Hotfix_StartSingleRankingInfoReq;
        private static DelegateBridge __Hotfix_StartCharactorObserverReq;
        private static DelegateBridge __Hotfix_SetToUIMode;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreateSingleRankingUIProcess;
        private static DelegateBridge __Hotfix_CreateTotalRankingUIProcess;
        private static DelegateBridge __Hotfix_get_LbRankingListClient;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public RankingListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AllianceSingleRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AllianceTotalRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_GuildSingleRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_GuildTotalRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_PersonSingleRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_PersonTotalRankingList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectedDynamicResForLode_RankingTarget(RankingListType type, RankingTargetInfo target, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private UIPlayingEffectInfo CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private UIPlayingEffectInfo CreateSingleRankingUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private UIPlayingEffectInfo CreateTotalRankingUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceRankingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterSingleRankingListButtonClick(RankingListType type)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildRankingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPersonRankingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRankingItemDetailButtonClick(RankingListType type, RankingTargetInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void PlayUIProcessForTotalRankingList()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetToAllianceTotalRankMode()
        {
        }

        [MethodImpl(0x8000)]
        private void SetToGuildTotalRankMode()
        {
        }

        [MethodImpl(0x8000)]
        private void SetToPersonalTotalRankMode()
        {
        }

        [MethodImpl(0x8000)]
        private void SetToUIMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void StartAllianceTotalRankingInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartCharactorObserverReq(string playerGameUserId, Action<CharactorObserverAck> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartGuildTotalRankingInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartPersonalTotalRankingInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartRankingListUITask(UIIntent returnToIntent, string mode, RankingListType rankingListType = 6, Action<bool> onPreparedEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartSingleRankingInfoReq(RankingListType rankingListType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_AllianceTotalRankingList(UIIntentReturnable intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_GuildTotalRankingList(UIIntentReturnable intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_PersonTotalRankingList(UIIntentReturnable intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SingleRankingList(UIIntentReturnable intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_AllianceTotalRankingList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_GuildTotalRankingList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_PersonTotalRankingList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SingleRankingList()
        {
        }

        private LogicBlockRankingListClient LbRankingListClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnEnterSingleRankingListButtonClick>c__AnonStorey0
        {
            internal RankingListType type;
            internal RankingListUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool result)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartAllianceTotalRankingInfoReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                RankingFirstAllianceRankingInfoNetworkTask task2 = task as RankingFirstAllianceRankingInfoNetworkTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartCharactorObserverReq>c__AnonStorey5
        {
            internal Action<CharactorObserverAck> onEnd;

            internal void <>m__0(Task returnTask)
            {
                CharactorObserveReqNetTas tas = returnTask as CharactorObserveReqNetTas;
                if ((tas != null) && !tas.IsNetworkError)
                {
                    int ackResult = tas.m_ackResult;
                    if (ackResult == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(tas.m_charactorObserverAckInfo);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(ackResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(null);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartGuildTotalRankingInfoReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                RankingFirstGuildRankingInfoNetworkTask task2 = task as RankingFirstGuildRankingInfoNetworkTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartPersonalTotalRankingInfoReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                RankingFirstPlayerRankingInfoNetworkTask task2 = task as RankingFirstPlayerRankingInfoNetworkTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartSingleRankingInfoReq>c__AnonStorey4
        {
            internal RankingListType rankingListType;
            internal Action<bool> onEnd;
            internal RankingListUITask $this;

            internal void <>m__0(Task task)
            {
                RankingListInfoGetNetworkTask task2 = task as RankingListInfoGetNetworkTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else if (this.rankingListType != RankingListType.AllianceEvaluateScore)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        LogicBlockGuildClient lBGuild = (LogicBlockGuildClient) this.$this.PlayerCtx.GetLBGuild();
                        if ((lBGuild.GetAllianceId() != 0) && (lBGuild.GetAllianceBasicInfo() == null))
                        {
                            AllianceInfoReqNetworkTask.StartTask(lBGuild.GetAllianceId(), delegate (AllianceInfo allianceInfo) {
                                if (this.onEnd != null)
                                {
                                    this.onEnd(true);
                                }
                            });
                        }
                        else if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                }
            }

            internal void <>m__1(AllianceInfo allianceInfo)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }
    }
}

