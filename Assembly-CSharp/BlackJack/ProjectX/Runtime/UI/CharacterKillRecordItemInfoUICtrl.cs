﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterKillRecordItemInfoUICtrl : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo> EventOnShipIconButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo> EventOnItemDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnSendLinkButtonClick;
        protected KillRecordInfo m_currKillRecord;
        public CommonItemIconUIController m_commonItemIconCtrl;
        public ScrollItemBaseUIController ScrollItemBaseUICtrl;
        public const string KillMode = "Kill";
        public const string BeKillMode = "BeKilled";
        private const string GuildCodeFormat = "[{0}]";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemDetailButton;
        [AutoBind("./ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SendLinkButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemUIStateCtrl;
        [AutoBind("./UnionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnionNameText;
        [AutoBind("./ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipTypeText;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./NameTextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./CornerMarkGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CornerState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_UpdateItem4KillMode;
        private static DelegateBridge __Hotfix_UpdateItem4BeKillMode;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_OnShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnShipIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendLinkButtonClick;

        public event Action<KillRecordInfo> EventOnItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnSendLinkButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnShipIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSendLinkButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateItem4BeKillMode(KillRecordInfo killRecordInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateItem4KillMode(KillRecordInfo killRecordInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(string mode, KillRecordInfo killRecordInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

