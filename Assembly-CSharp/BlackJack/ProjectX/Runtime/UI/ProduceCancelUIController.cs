﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceCancelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCancelButtonClick;
        protected LBProductionLine m_currProduceLine4Self;
        protected GuildProductionLineInfo m_currProduceLine4Guild;
        protected bool m_isProducing;
        protected bool m_isInitComplete;
        protected DateTime m_nextUpdateTime;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipTextStateCtrl;
        [AutoBind("./DetailPanel/MoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MoneyIconStateCtrl;
        [AutoBind("./DetailPanel/TimeVauleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./DetailPanel/MoneyVauleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindCurrencyText;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./DetailPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemGroup;
        [AutoBind("./DetailPanel/ItemGroup/ResourceItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemPrefab;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateViewInfo4SelfProduceMode;
        private static DelegateBridge __Hotfix_UpdateViewInfo4GuildProduceMode;
        private static DelegateBridge __Hotfix_UpdateTimeInfo;
        private static DelegateBridge __Hotfix_UpdateReturnBindCurreyInfo;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateReturnBindCurreyInfo(ulong returnCurrency, bool is4Guild)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTimeInfo(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewInfo4GuildProduceMode(GuildProductionLineInfo produceLine, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewInfo4SelfProduceMode(LBProductionLine produceLine, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

