﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterChipSetUIController : UIControllerBase
    {
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/ChipComparisonInfo/SelectedChipItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selectedChipInfoRoot;
        [AutoBind("./Margin/ChipComparisonInfo/CurrChipItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_currChipInfoRoot;
        [AutoBind("./Margin/ChipStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chipStorePanelStateController;
        [AutoBind("./Margin/ChipStorePanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemStoreListRoot;
        [AutoBind("./Margin/ChipStorePanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./Margin/ArrowLeft", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_arrowLeftObj;
        [AutoBind("./Margin/EmptyText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_emptyText;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backGroundButton;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        public ItemStoreListUIController m_itemStoreListUICtrl;
        public CharacterChipInfoItemUIController m_selectedChipInfoCtrl;
        public CharacterChipInfoItemUIController m_currChipInfoCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetArrowActive;
        private static DelegateBridge __Hotfix_SetEmptyChipTipState;
        private static DelegateBridge __Hotfix_SetBackGroundButtonActive;
        private static DelegateBridge __Hotfix_SetChipItemStoreTitle;
        private static DelegateBridge __Hotfix_CreatStorePanelAnimtionProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatStorePanelAnimtionProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArrowActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundButtonActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipItemStoreTitle(string titleStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEmptyChipTipState(bool isShow)
        {
        }
    }
}

