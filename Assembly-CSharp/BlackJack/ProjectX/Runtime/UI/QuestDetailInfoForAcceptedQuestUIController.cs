﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestDetailInfoForAcceptedQuestUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<QuestRewardInfo> EventOnQuestRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestRewardInfo> EventOnQuestRewardItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ItemDropInfoUIShowItemList> EventOnQuestRandomRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ItemDropInfoUIShowItemList> EventOnQuestRandomRewardItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnQuestDetailScrollViewClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnQuestCommitItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnQuestCommitItem3DTouch;
        private CommonUIStateController[] m_allowInShipTypeCtrlList;
        public QuestCompleteCondUIController m_questCompleteCondUICtrl;
        public CommonRewardMoneyItemsUIController m_questRewardCtrl;
        public CommonRewardMoneyItemsUIController m_questRandomRewardCtrl;
        public CommonRewardMoneyItemsUIController m_questCommitItemCtrl;
        public List<QuestRewardInfo> m_rewardItemList;
        public List<ItemDropInfoUIShowItemList> m_randomRewardItemList;
        private List<VirtualBuffDesc> m_virtualBuffDescs;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AcceptedQuestDetail;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AcceptedQuestPanelCtrl;
        [AutoBind("./DetailScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler QuestDetailScrollViewEventHandler;
        [AutoBind("./QuestTitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image QuestTitleImage;
        [AutoBind("./QuestNameText/QuestLevelGroup/QuestLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestLevelText;
        [AutoBind("./QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestNameText;
        [AutoBind("./DistanceGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestGalaxyNameText;
        [AutoBind("./DistanceGroup/DistanceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DistanceValueText;
        [AutoBind("./DetailScrollView/Viewport/Content/TargetGroup/CompleteCondTitleGroup/QuestTime", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestTimer;
        [AutoBind("./DetailScrollView/Viewport/Content/TargetGroup/CompleteCondTitleGroup/QuestTime/TimeProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image QuestTimeProgressBar;
        [AutoBind("./DetailScrollView/Viewport/Content/TargetGroup/CompleteCondTitleGroup/QuestTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestTimeValue;
        [AutoBind("./DetailScrollView/Viewport/Content/TargetGroup/CompleteCondTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestCompleteCondRoot;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTextGroup/DescriptionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestDescriptionText;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeFrigate;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeDestroyer;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeCruiser;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleCruiser;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleShip;
        [AutoBind("./DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeIndustryShip;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestReward;
        [AutoBind("./DetailScrollView/Viewport/Content/ProbabilityRewardGroup/TextTitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RandomQuestRewardTitle;
        [AutoBind("./DetailScrollView/Viewport/Content/ProbabilityRewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RandomQuestReward;
        [AutoBind("./DetailScrollView/Viewport/Content/NeedGoodsGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NeedGoodsGroup;
        [AutoBind("./DetailScrollView/Viewport/Content/NeedGoodsGroup/TextTitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NeedsTextTitleGroup;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardBounsUIState;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public Button RewardBonusButton;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardBonusStateCtrl;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup/RewardBonus/Image/BonusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardBonusText;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup/NationRewardBonus/NationBgImage/NationIcon/InfoImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FactionBounsIcon;
        [AutoBind("./DetailScrollView/Viewport/Content/RewardGroup/TextTitleGroup/RewardTitleGroup/NationRewardBonus/NationBgImage/BonusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FactionBounsName;
        [AutoBind("./DistanceGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StarMapButton;
        [AutoBind("./DistanceGroup/StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StarMapButtonImage;
        [AutoBind("./ButtonGroup/AbortButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AbortButton;
        [AutoBind("./ButtonGroup/StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StrikeButton;
        [AutoBind("./ButtonGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseQuestButton;
        [AutoBind("./ButtonGroup/RecoverButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecoverButton;
        [AutoBind("./ButtonGroup/TautologyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RetryButton;
        [AutoBind("./ButtonGroup/TraceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TraceButton;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonStateCtrl;
        [AutoBind("./DefeatedBgImage01", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestFailTip;
        [AutoBind("./LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningDummy;
        [AutoBind("./PoolParent", AutoBindAttribute.InitState.NotInit, false)]
        private EasyObjectPool m_easyPool;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetAcceptQuestDetailPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetSelectedAcceptedQuestInfo;
        private static DelegateBridge __Hotfix_SetRewardBouus;
        private static DelegateBridge __Hotfix_GetQuestVirtualBuff;
        private static DelegateBridge __Hotfix_UpdateQuestTimer;
        private static DelegateBridge __Hotfix_SetStarMapButtonState;
        private static DelegateBridge __Hotfix_GetLossWarningWindowDummyPos;
        private static DelegateBridge __Hotfix_OnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestRandomRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestCommitItemClick;
        private static DelegateBridge __Hotfix_OnQuestCommitItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestRandomRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestDetailScrollViewClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnQuestRandomRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRandomRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRandomRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRandomRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnQuestDetailScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestDetailScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestCommitItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCommitItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestCommitItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCommitItem3DTouch;
        private static DelegateBridge __Hotfix_get_AllowInShipTypeCtrlList;

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestCommitItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestCommitItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnQuestDetailScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ItemDropInfoUIShowItemList> EventOnQuestRandomRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ItemDropInfoUIShowItemList> EventOnQuestRandomRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<QuestRewardInfo> EventOnQuestRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<QuestRewardInfo> EventOnQuestRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetAcceptQuestDetailPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLossWarningWindowDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetQuestVirtualBuff()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCommitItem3DTouch(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCommitItemClick(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestDetailScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRandomRewardItem3DTouch(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRandomRewardItemClick(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRewardItem3DTouch(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRewardItemClick(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRewardBouus()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedAcceptedQuestInfo(LBProcessingQuestBase quest, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarMapButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestTimer(LBProcessingQuestBase quest)
        {
        }

        private CommonUIStateController[] AllowInShipTypeCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

