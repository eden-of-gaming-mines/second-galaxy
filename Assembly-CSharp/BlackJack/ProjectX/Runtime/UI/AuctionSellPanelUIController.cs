﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionSellPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSellItemClick;
        public List<string> m_sellItemStateList;
        public List<AuctionItemDetailInfo> m_sellItemInfoList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private const string AuctionSellItemPoolName = "AuctionSellItem";
        [AutoBind("./SellItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_itemListScrollRect;
        [AutoBind("./SellItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemListUIItemPool;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./ShellNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sellNumberText;
        [CompilerGenerated]
        private static Predicate<AuctionItemDetailInfo> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateSellItem;
        private static DelegateBridge __Hotfix_SetSellItemSelected;
        private static DelegateBridge __Hotfix_GetItemIconPos;
        private static DelegateBridge __Hotfix_GetItemIconSize;
        private static DelegateBridge __Hotfix_OnBuyItemClick;
        private static DelegateBridge __Hotfix_OnBuyItemFill;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_add_EventOnSellItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSellItemClick;

        public event Action<int> EventOnSellItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemIconPos(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetItemIconSize(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSellItemSelected(int selextedIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSellItem(List<string> sellItemStateList, List<AuctionItemDetailInfo> sellItemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

