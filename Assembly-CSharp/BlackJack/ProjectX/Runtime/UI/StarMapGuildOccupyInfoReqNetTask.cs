﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarMapGuildOccupyInfoReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_starFieldId;
        private int m_result;
        private bool m_isDataChange;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_get_IsDataChange;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnStarMapGuildOccupyInfoAck;

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyInfoReqNetTask(int starFieldId, bool needBlockGlobalUI = true)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapGuildOccupyInfoAck(int result, int starFieldId, bool isDataChange)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsDataChange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

