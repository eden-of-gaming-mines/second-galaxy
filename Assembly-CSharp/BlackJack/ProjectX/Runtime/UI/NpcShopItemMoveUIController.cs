﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class NpcShopItemMoveUIController : UIControllerBase
    {
        public GameObject m_commonIconItem;
        [AutoBind("./CommonItemUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject IconDummyPos;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public TweenPos PosTween;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public TweenCGAlpha CGAlphaTween;
        private static DelegateBridge __Hotfix_ReStartPosTween;
        private static DelegateBridge __Hotfix_UpdateCommonIconItemInfo;

        [MethodImpl(0x8000)]
        public void ReStartPosTween(Vector3 startPos, Vector3 endPos, UnityAction onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject UpdateCommonIconItemInfo(NpcShopListItemUIController listItemCtrl, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

