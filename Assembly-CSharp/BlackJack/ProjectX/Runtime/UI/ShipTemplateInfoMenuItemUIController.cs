﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ShipTemplateInfoMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_shipTemplateMenuItemToggle;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipTemplateMenuItemNameText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnToggleClick;
        private static DelegateBridge __Hotfix_OnSelectImp;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_SwitchMenuItemExpandImp;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;

        [MethodImpl(0x8000)]
        protected override void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSelectImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnToggleClick(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SwitchMenuItemExpandImp()
        {
        }
    }
}

