﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemTargetItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SolarSystemTargetItemUIController> EventOnItemThreeDTouch;
        public ScrollItemBaseUIController ScrollCtrl;
        public SolarSystemUITask.TargetItemUIInfo CurrentItemUIInfo;
        private string m_currItemName;
        [AutoBind("./AttackedStateImageEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackedStateImageEffect;
        private TweenMain[] m_targetNoticeTalkTargetEffectList;
        private TweenMain[] m_targetNoticeInvestigateEffectList;
        private TweenMain[] m_targetNoticeFriendlyTargetEffectList;
        private TweenMain[] m_targetNoticeEnemyTargetEffectList;
        [AutoBind("./TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AttackStateCtrl;
        [AutoBind("./TargetSpecialIconRoot/TargetNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetNoticeCtrl;
        [AutoBind("./TargetSpecialIconRoot/TargetPVPRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetPVPCtrl;
        [AutoBind("./TargetTypeIconImg", AutoBindAttribute.InitState.NotInit, false)]
        public Image TargetTypeIconImg;
        [AutoBind("./TargetTypeIconImg", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetTypeIconCtrl;
        [AutoBind("./ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProgressBarObj;
        [AutoBind("./ProgressBarImage/ArmorProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorProgressBarImg;
        [AutoBind("./ProgressBarImage/ShieldProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldProgressBarImg;
        [AutoBind("./ShipConfNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipConfNameText;
        [AutoBind("./DistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText DistanceNumberText;
        [AutoBind("./TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./SelectAndUnselect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetItemSelectedStateCtrl;
        [AutoBind("./TargetSpecialIconRoot/TargetSpecialIcons/TalkTarget/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_targetNoticeTalkTargetEffectRoot;
        [AutoBind("./TargetSpecialIconRoot/TargetSpecialIcons/InvestigateTarget/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_targetNoticeInvestigateEffectRoot;
        [AutoBind("./TargetSpecialIconRoot/TargetSpecialIcons/FriendlyTarget/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_targetNoticeFriendlyTargetEffectRoot;
        [AutoBind("./TargetSpecialIconRoot/TargetSpecialIcons/EnemyTarget/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_targetNoticeEnemyTargetEffectRoot;
        [AutoBind("./TargetGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetGroupStateCtrl;
        [AutoBind("./TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetImageStateCtrl;
        [AutoBind("./TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_targetImage;
        [AutoBind("./TargetGroup/TeamImage-", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageSubtractionSignCtrl;
        [AutoBind("./TargetGroup/ShipImage=", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageequalSignCtrl;
        [AutoBind("./TargetGroup/CaptainImage+", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageAddSignCtrl;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_InitTargetInfoToUI;
        private static DelegateBridge __Hotfix_UpdateTargetInfoToUI;
        private static DelegateBridge __Hotfix_DisplayName;
        private static DelegateBridge __Hotfix_UpdateTargetInfoForShip;
        private static DelegateBridge __Hotfix_UpdateTargetInfoForBuildingAndDropBox;
        private static DelegateBridge __Hotfix_UpdateUISelectedState;
        private static DelegateBridge __Hotfix_SetTargetName;
        private static DelegateBridge __Hotfix_SetShipConfigName;
        private static DelegateBridge __Hotfix_SetTargetIconSubRank;
        private static DelegateBridge __Hotfix_SetTargeArmor;
        private static DelegateBridge __Hotfix_SetTargetShield;
        private static DelegateBridge __Hotfix_SetTargetDistance;
        private static DelegateBridge __Hotfix_SetTargetNoticeType;
        private static DelegateBridge __Hotfix_PlayTargetNoticeEffect;
        private static DelegateBridge __Hotfix_SetTargetCrimeState;
        private static DelegateBridge __Hotfix_SetShipMode;
        private static DelegateBridge __Hotfix_SetHudIcon;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_SetItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemThreeDTouch;
        private static DelegateBridge __Hotfix_remove_EventOnItemThreeDTouch;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemThreeDTouch;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_TargetNoticeTalkTargetEffectList;
        private static DelegateBridge __Hotfix_get_TargetNoticeInvestigateEffectList;
        private static DelegateBridge __Hotfix_get_TargetNoticeFriendlyTargetEffectList;
        private static DelegateBridge __Hotfix_get_TargetNoticeEnemyTargetEffectList;

        protected event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SolarSystemTargetItemUIController> EventOnItemThreeDTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private string DisplayName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void InitTargetInfoToUI(SolarSystemUITask.TargetItemUIInfo itemInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemThreeDTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayTargetNoticeEffect(string targetNoticeState)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHudIcon(bool isVisible, string HudSubscriptState, string HudColorState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipConfigName(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipMode(bool isShipMode)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTargeArmor(float currentArmor, float maxArmor)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetCrimeState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTargetDistance(double distance)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTargetIconSubRank(SubRankType subRank)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTargetName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetNoticeType(string targetNoticeState)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTargetShield(float currentShield, float maxShield)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTargetInfoForBuildingAndDropBox()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTargetInfoForShip()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetInfoToUI(SolarSystemUITask.TargetItemUIInfo targetItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUISelectedState(string stateNmae)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private TweenMain[] TargetNoticeTalkTargetEffectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private TweenMain[] TargetNoticeInvestigateEffectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private TweenMain[] TargetNoticeFriendlyTargetEffectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private TweenMain[] TargetNoticeEnemyTargetEffectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

