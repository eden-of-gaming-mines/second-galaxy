﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFleetMemberInfoUIController : UIControllerBase, IScrollItem
    {
        public ScrollItemBaseUIController m_scrollCtrl;
        public string m_playerUserId;
        private const string StateNormal = "Normal";
        private const string StateEmpty = "Empty";
        [AutoBind("./NameText/CaptainImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildFleetCommanderStateCtrl;
        [AutoBind("./ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipNameText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildFleetMemberInfoUIPrefabCommonUIStateController;
        [AutoBind("./PositionGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PositionStateCtrl;
        [AutoBind("./PositionGroup/PositionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PositionText;
        [AutoBind("./LocationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LocationStateCtrl;
        [AutoBind("./LocationGroup/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LocationText;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateGuildFleetMemberInfo;
        private static DelegateBridge __Hotfix_GetMemberInfoRectTansform;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnRegisterItemClickEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetMemberLocationString;
        private static DelegateBridge __Hotfix_GetMemberShipString;
        private static DelegateBridge __Hotfix_GetMemberPositionString;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetMemberInfoRectTansform()
        {
        }

        [MethodImpl(0x8000)]
        private string GetMemberLocationString(int location, bool nearBy)
        {
        }

        [MethodImpl(0x8000)]
        private string GetMemberPositionString(List<FleetPosition> positions)
        {
        }

        [MethodImpl(0x8000)]
        private string GetMemberShipString(int shipConfigId, bool isInSpace)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool careClick)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetMemberInfo(GuildFleetDetailsUITask.GuildFleetMemberDetailInfo member, string selfUserId)
        {
        }
    }
}

