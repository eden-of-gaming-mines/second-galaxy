﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class HiredCaptainAddExpByItemReqNetTask : NetWorkTransactionTask
    {
        public ulong m_captainInstanceId;
        public int m_itemId;
        public long m_itemCount;
        public long m_expAdded;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CaptainAddExpResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAddExpByItemAck;
        private static DelegateBridge __Hotfix_set_CaptainAddExpResult;
        private static DelegateBridge __Hotfix_get_CaptainAddExpResult;

        [MethodImpl(0x8000)]
        public HiredCaptainAddExpByItemReqNetTask(ulong captainInstanceId, int itemId, long itemCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddExpByItemAck(int result, ulong captainInstanceId, long expAdded, int upLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CaptainAddExpResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

