﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcTalkerDialogUIController : UIControllerBase
    {
        private Material m_remoteNpcMat;
        private Material m_defaultNpcMat;
        private NpcDialogInfo m_dialogInfo;
        private List<NpcDialogOptionUIController> m_optionList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnNpcDialogOptionClick;
        public static int MaxOptionCountInOnePage;
        private static string SpaceStr;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroundButton;
        [AutoBind("./DialogDetail/NpcImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NpcImage;
        [AutoBind("./DialogDetail/NpcNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NpcName;
        [AutoBind("./DialogDetail/TimeImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_timeImage;
        [AutoBind("./DialogDetail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeText;
        [AutoBind("./DialogDetail/DialogText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DialogText;
        [AutoBind("./DialogDetail/OptionButtons/Option1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Option1;
        [AutoBind("./DialogDetail/OptionButtons/Option1", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Option1Button;
        [AutoBind("./DialogDetail/OptionButtons/Option2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Option2;
        [AutoBind("./DialogDetail/OptionButtons/Option2", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Option2Button;
        [AutoBind("./DialogDetail/OptionButtons/Option3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Option3;
        [AutoBind("./DialogDetail/OptionButtons/Option3", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Option3Button;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateNpcDialogContent_1;
        private static DelegateBridge __Hotfix_UpdateNpcDialogContent_0;
        private static DelegateBridge __Hotfix_GetPanelShorOrHideUIProcess;
        private static DelegateBridge __Hotfix_EventCallBackSetCountdownState;
        private static DelegateBridge __Hotfix_EventCallBackCountdownInProgress;
        private static DelegateBridge __Hotfix_EventCallBackOnDialogAutoClosed;
        private static DelegateBridge __Hotfix_SetRemoteNpcMode;
        private static DelegateBridge __Hotfix_SetDefaultNpcMode;
        private static DelegateBridge __Hotfix_SetCountdownState;
        private static DelegateBridge __Hotfix_OnOption1ButtonClick;
        private static DelegateBridge __Hotfix_OnOption2ButtonClick;
        private static DelegateBridge __Hotfix_OnOption3ButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNpcDialogOptionClick;
        private static DelegateBridge __Hotfix_remove_EventOnNpcDialogOptionClick;

        public event Action<int> EventOnNpcDialogOptionClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void EventCallBackCountdownInProgress(string countdownString)
        {
        }

        [MethodImpl(0x8000)]
        public void EventCallBackOnDialogAutoClosed()
        {
        }

        [MethodImpl(0x8000)]
        public void EventCallBackSetCountdownState(bool countdownStatee)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShorOrHideUIProcess(bool isShow, bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOption1ButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOption2ButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOption3ButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCountdownState(bool isCountdown, string countdownString = "")
        {
        }

        [MethodImpl(0x8000)]
        private void SetDefaultNpcMode()
        {
        }

        [MethodImpl(0x8000)]
        private void SetRemoteNpcMode(bool isRemote)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNpcDialogContent(Sprite npcIcon, string npcName, string strContent, List<KeyValuePair<string, NpcDialogOptType>> btnTextList, bool isRemoteNpc = false, bool isDefaultNpc = false, Dictionary<string, UnityEngine.Object> resDict = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNpcDialogContent(string npcName, Sprite npcIcon, string dialogStr, NpcDialogInfo dialogInfo, int currOptionTopIndex, Dictionary<string, UnityEngine.Object> resDict = null, bool isRemoteNpc = false, bool isDefaultNpc = false)
        {
        }
    }
}

