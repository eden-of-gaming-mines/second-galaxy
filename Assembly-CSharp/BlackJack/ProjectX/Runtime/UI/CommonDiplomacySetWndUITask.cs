﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonDiplomacySetWndUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, DiplomacyState> EventOnDiplomacyTypeChanged;
        private CommonDiplomacySetWndUIController m_mainCtrl;
        private DiplomacyState m_personalDiplomacyState;
        private DiplomacyState m_guildDiplomacyState;
        protected string m_playerUserId;
        protected uint m_guildId;
        protected uint m_allianceId;
        protected Vector3 m_pos;
        public const string ParamKeyPlayerUserId = "PlayerUserId";
        public const string ParamKeyGuildId = "GuildId";
        public const string ParamKeyGuildAllianceId = "AllianceId";
        public const string ParamKeyPos = "Pos";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CommonDiplomacySetWndUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCommonDiplomacySetWndUITask;
        private static DelegateBridge __Hotfix_RegistDiplomacyUpdateEvent;
        private static DelegateBridge __Hotfix_UnRegistDiplomacyUpdateEvent;
        private static DelegateBridge __Hotfix_HideDiplomacySetWnd;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateDiplomacyPanel;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_CloseDiplomacySetPanel;
        private static DelegateBridge __Hotfix_SendGuildDiplomacyUpdateReq;
        private static DelegateBridge __Hotfix_SendPersonalDiplomacyUpdateReq;
        private static DelegateBridge __Hotfix_GetDiplomacyState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<bool, DiplomacyState> EventOnDiplomacyTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonDiplomacySetWndUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseDiplomacySetPanel()
        {
        }

        [MethodImpl(0x8000)]
        private DiplomacyState GetDiplomacyState(string userId, uint guildId, uint allienceId, bool getGuildState)
        {
        }

        [MethodImpl(0x8000)]
        public void HideDiplomacySetWnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyTypeChanged(bool isPersonal, DiplomacyState type, bool isGrayStateClick)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void RegistDiplomacyUpdateEvent(Action<bool, DiplomacyState> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildDiplomacyUpdateReq(string userId, uint guildId, uint allienceId, DiplomacyState wantedDiplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        private void SendPersonalDiplomacyUpdateReq(string userId, uint guildId, uint allienceId, DiplomacyState wantedDiplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        public static CommonDiplomacySetWndUITask StartCommonDiplomacySetWndUITask(string playerId, uint guildId, uint allianceId, Vector3 pos, UIIntent returnIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegistDiplomacyUpdateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDiplomacyPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildDiplomacyUpdateReq>c__AnonStorey0
        {
            internal DiplomacyState wantedDiplomacyState;
            internal CommonDiplomacySetWndUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendPersonalDiplomacyUpdateReq>c__AnonStorey1
        {
            internal DiplomacyState wantedDiplomacyState;
            internal CommonDiplomacySetWndUITask $this;

            internal void <>m__0(Task task)
            {
                int result = ((PersonalDiplomacyUpdateReqNetTask) task).Result;
                if (!((PersonalDiplomacyUpdateReqNetTask) task).IsNetworkError)
                {
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        if (result == -1832)
                        {
                            this.$this.CloseDiplomacySetPanel();
                        }
                        else
                        {
                            this.$this.UpdateDiplomacyPanel();
                        }
                    }
                    else
                    {
                        if (this.wantedDiplomacyState == DiplomacyState.Enemy)
                        {
                            TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyEnemy, new object[0]), new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Friend)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyFriend, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Neutral)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyNeutral, new object[0]), false);
                        }
                        this.$this.m_personalDiplomacyState = this.wantedDiplomacyState;
                        if (this.$this.EventOnDiplomacyTypeChanged != null)
                        {
                            this.$this.EventOnDiplomacyTypeChanged(true, this.$this.m_personalDiplomacyState);
                        }
                    }
                }
            }
        }
    }
}

