﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class StarMapForGuildBuildingPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnBuildingItemClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnBuildItemProcessingEnd;
        public GuildBuildingItemButtonGroupUIController m_buildingItemButtonGroupCtrl;
        private List<GuildBuildingInfo> m_guildBuildingList;
        private List<GuildBuildingInfo> m_controlHubBuildingList;
        private List<GuildBuildingItemUIController> m_buildingItemCtrls;
        private uint m_guildId;
        [AutoBind("./BuildingScrollView/BuildingItemPool", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool BuildingItemEasyPool;
        [AutoBind("./BuildingScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BuildingItemListContentRoot;
        [AutoBind("./UnusedBuildingButtonGroupRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform UnusedBuildingButtonGroupRoot;
        [AutoBind("./BuildingTitle/OccupyBuildingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx OccupyBuildingToggle;
        [AutoBind("./BuildingTitle/GuildBuildingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx GuildBuildingToggle;
        [AutoBind("./BuildingTitle/GuildBuildingDisableButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildBuildingDisableButton;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingPanel;
        private static DelegateBridge __Hotfix_SetBuildingPanelToggleSelect;
        private static DelegateBridge __Hotfix_EnableBuildingPanleToggle;
        private static DelegateBridge __Hotfix_SetBuildingItemSelect;
        private static DelegateBridge __Hotfix_UpdateAntiTeleportButtonGroupStatus;
        private static DelegateBridge __Hotfix_GetControllerByBuildingInstanceId;
        private static DelegateBridge __Hotfix_GetUnsedBuildButtonGroupRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_PrepareBuildingItems;
        private static DelegateBridge __Hotfix_OnBuildingItemCreated;
        private static DelegateBridge __Hotfix_OnBuildingItemClick;
        private static DelegateBridge __Hotfix_OnBuildingItemProcessingEnd;
        private static DelegateBridge __Hotfix_OnGuildBuildingDisableButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBuildingItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnBuildingItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnBuildItemProcessingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnBuildItemProcessingEnd;

        public event Action<int> EventOnBuildingItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnBuildItemProcessingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingPanleToggle(bool isGuildBuilding, bool isInteractable)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingItemUIController GetControllerByBuildingInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetUnsedBuildButtonGroupRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuildingItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuildingItemCreated(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuildingItemProcessingEnd(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBuildingDisableButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareBuildingItems(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuildingItemSelect(int selectIndex, GuildSolarSystemInfoClient guildSolarSystemInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuildingPanelToggleSelect(bool isGuildBuilding)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAntiTeleportButtonGroupStatus(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingPanel(uint guildId, GuildSolarSystemInfoClient guildSolarSystemInfo, List<GuildBuildingInfo> buildingList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

