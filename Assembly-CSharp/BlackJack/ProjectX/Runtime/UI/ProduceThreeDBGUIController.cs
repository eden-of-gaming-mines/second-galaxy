﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceThreeDBGUIController : UIControllerBase
    {
        private bool m_inited;
        public Image ItemNormalIconImage;
        public Image ItemProducingIconImage;
        public Image ShipNormalIconImage;
        public Image ShipProducingIconImage;
        public Animator ModelAnimator;
        [AutoBind("./WorkshopViewDummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModelViewRoot;
        private static DelegateBridge __Hotfix_InitModelView;
        private static DelegateBridge __Hotfix_Update3DBGInfo;
        private static DelegateBridge __Hotfix_SetAnimationState;
        private static DelegateBridge __Hotfix_Clear3DModel;
        private static DelegateBridge __Hotfix_SetItemSprite;

        [MethodImpl(0x8000)]
        public void Clear3DModel()
        {
        }

        [MethodImpl(0x8000)]
        public void InitModelView(GameObject asset)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAnimationState(ProduceLineState state, bool isShip)
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemSprite(ProduceLineState state, Sprite sprite, bool isShip)
        {
        }

        [MethodImpl(0x8000)]
        public void Update3DBGInfo(ProduceLineState state, Sprite sprite, bool isShip)
        {
        }
    }
}

