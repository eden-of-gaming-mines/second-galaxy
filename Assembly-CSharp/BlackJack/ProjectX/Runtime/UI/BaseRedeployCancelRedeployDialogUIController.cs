﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class BaseRedeployCancelRedeployDialogUIController : BaseRedeployDialogUIControllerBase
    {
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        private static DelegateBridge __Hotfix_get_CancelButtonName;
        private static DelegateBridge __Hotfix_get_ConfirmButtonName;

        protected override string CancelButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override string ConfirmButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

