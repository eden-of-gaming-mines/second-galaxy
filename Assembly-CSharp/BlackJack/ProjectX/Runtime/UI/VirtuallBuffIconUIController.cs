﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class VirtuallBuffIconUIController : UIControllerBase
    {
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        private static DelegateBridge __Hotfix_UpdateVirtualBuffItemUI;

        [MethodImpl(0x8000)]
        public void UpdateVirtualBuffItemUI(VirtualBuffDesc virtualBuffDesc, Dictionary<string, Object> resDic)
        {
        }
    }
}

