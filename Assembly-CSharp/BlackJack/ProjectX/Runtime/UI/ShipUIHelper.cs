﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ShipUIHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetSuperWeaponEquipDescByDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetSuperEquipDescByDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetSuperEquipRuningInfoByDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetSuperWeaponDescByDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetSuperEquipDescString;
        private static DelegateBridge __Hotfix_GetSuperEquipConfigRuningInfo;
        private static DelegateBridge __Hotfix_GetSuperWeaponDescString;
        private static DelegateBridge __Hotfix_GetSuperWeaponTotalDamage;
        private static DelegateBridge __Hotfix_GetSuperWeaponTotalDamageByDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetShipArtResFullPath;
        private static DelegateBridge __Hotfix_GetShipArtLOD0ResFullPath;
        private static DelegateBridge __Hotfix_GetWeaponArtResFullPath;
        private static DelegateBridge __Hotfix_GetShipEffectArtResFullPath;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_GetShipWithScanEquipHangarIndex;
        private static DelegateBridge __Hotfix_ChechShipHasSpaceSignalScanEquip;
        private static DelegateBridge __Hotfix_ExistScanEquipInStore;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public static bool ChechShipHasSpaceSignalScanEquip(ILBStaticPlayerShip currShip)
        {
        }

        [MethodImpl(0x8000)]
        public static bool ExistScanEquipInStore()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtLOD0ResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipEffectArtResFullPath(string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipName(LogicBlockShipCompStaticBasicCtxClient shipCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetShipWithScanEquipHangarIndex()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipSuperEquipRunningInfo GetSuperEquipConfigRuningInfo(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSuperEquipDescByDrivingLicenseLevel(ConfigDataSpaceShipInfo shipConfigInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSuperEquipDescString(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipSuperEquipRunningInfo GetSuperEquipRuningInfoByDrivingLicenseLevel(ConfigDataShipSuperEquipInfo superEquipConfigInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSuperWeaponDescByDrivingLicenseLevel(ConfigDataSpaceShipInfo shipConfigInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSuperWeaponDescString(ConfigDataSuperWeaponInfo superWeaponInfo, ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSuperWeaponEquipDescByDrivingLicenseLevel(ConfigDataSpaceShipInfo shipConfigInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSuperWeaponTotalDamage(ConfigDataSuperWeaponInfo superWeapon, ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSuperWeaponTotalDamageByDrivingLicenseLevel(ConfigDataSuperWeaponInfo superWeapon, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponArtResFullPath(string resPrefix, GrandFaction grandFaction, string confResPath)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

