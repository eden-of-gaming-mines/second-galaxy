﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GMDisplayTestUIController : UIControllerBase
    {
        public DataType m_dataType;
        public int m_dataCount = 3;
        [AutoBind("./InputField (2)", AutoBindAttribute.InitState.NotInit, false)]
        public InputField InputValue3;
        [AutoBind("./InputField (1)", AutoBindAttribute.InitState.NotInit, false)]
        public InputField InputValue2;
        [AutoBind("./InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField InputValue1;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetValues;
        private static DelegateBridge __Hotfix_SetValues;
        private static DelegateBridge __Hotfix_GetValue;
        private static DelegateBridge __Hotfix_SetValue;
        private static DelegateBridge __Hotfix_GetIntValue;
        private static DelegateBridge __Hotfix_SetIntValue;
        private static DelegateBridge __Hotfix_GetFloatValue;
        private static DelegateBridge __Hotfix_SetFloatValue;

        [MethodImpl(0x8000)]
        private int GetFloatValue(InputField input)
        {
        }

        [MethodImpl(0x8000)]
        private int GetIntValue(InputField input)
        {
        }

        [MethodImpl(0x8000)]
        private int GetValue(InputField input)
        {
        }

        [MethodImpl(0x8000)]
        public int[] GetValues()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFloatValue(InputField input, int value)
        {
        }

        [MethodImpl(0x8000)]
        private void SetIntValue(InputField input, int value)
        {
        }

        [MethodImpl(0x8000)]
        private void SetValue(InputField input, int value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetValues(int[] values)
        {
        }

        public enum DataType
        {
            Int,
            Float
        }
    }
}

