﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DrivingLiscenseCheckUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, bool> EventOnRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, bool> EventOnRecommendItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        public List<CommonItemIconUIController> m_recommendItemCtrlList;
        public List<CommonItemIconUIController> m_rewardItemCtrlList;
        public List<DrivingQuestMonryOrExpRewardItemUIController> m_otherRewardCtrlList;
        public List<DrivingLicenseNodeInfoUIController> m_nodeListCtrl;
        private GameObject m_commonItemGoInstance;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        private const string PrefabAsset_CommonItem = "CommonItemUIPrefab";
        private const string AllowInImageUIState_AllowIn = "Normal";
        private const string AllowInImageUIState_NotAllowIn = "Gray";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./LicenseInfoTitle/DrivingtitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleNameText;
        [AutoBind("./LicenseInfoTitle/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image GrandFactionImage;
        [AutoBind("./LicenseInfoTitle/DrivingRequirement", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_drivingRequirementStateCtrl;
        [AutoBind("./LicenseInfoTitle/DrivingRequirement/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_skipButton;
        [AutoBind("./LicenseInfoTitle/DrivingRequirement/IconGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        [AutoBind("./LicenseInfoTitle/DrivingRequirement/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./LicenseInfoTitle/DrivingRequirement/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_levelRequireText;
        [AutoBind("./NodeList/Node", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeGoInstance;
        [AutoBind("./CheckQuestInfo/RecommendEquipList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RecommendItemGroup;
        [AutoBind("./CheckQuestInfo/FinishedRewardList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemRewardGroup;
        [AutoBind("./CheckQuestInfo/OtherRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform OtherRewardGroup;
        [AutoBind("./CheckQuestInfo/OtherRewardGroup/RewardItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OtherRewardGoInstance;
        [AutoBind("./CheckQuestInfo/StartCheckButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button StartCheckButton;
        [AutoBind("./CheckQuestInfo/LimitingTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_limitingObj;
        [AutoBind("./CheckQuestInfo/LimitingTitleText/LimitingText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_limitingText;
        [AutoBind("./CheckQuestInfo/LimitingTitleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_limitingBgObj;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./CheckQuestInfo/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public Button AllowInShipButton;
        [AutoBind("./CheckQuestInfo/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeFrigate;
        [AutoBind("./CheckQuestInfo/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeDestroyer;
        [AutoBind("./CheckQuestInfo/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeCruiser;
        [AutoBind("./CheckQuestInfo/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleCruiser;
        [AutoBind("./CheckQuestInfo/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleShip;
        [AutoBind("./CheckQuestInfo/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeIndustryShip;
        [AutoBind("./CheckQuestInfo/StateImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_stateImage;
        [AutoBind("./CheckQuestInfo/QuestDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestDesc;
        [AutoBind("./CheckQuestInfo/RecommendTacticsInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RecommendTactics;
        [AutoBind("./CheckQuestInfo/QuestIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image QuestIconImage;
        [AutoBind("./RewardItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RewardItemSimpleInfoDummy;
        [AutoBind("./RecommondItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RecommondItemSimpleInfoDummy;
        [AutoBind("./AllowInShipGroupUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AllowInShipInfoDummy;
        [AutoBind("./CheckQuestInfo/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateDrivingInfo;
        private static DelegateBridge __Hotfix_UpdateDrivingQuestInfo;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPosition;
        private static DelegateBridge __Hotfix_GetShipAllowInInfoAnchorPosition;
        private static DelegateBridge __Hotfix_GetLossWarningWindowDummyPos;
        private static DelegateBridge __Hotfix_UpdateSkillNode;
        private static DelegateBridge __Hotfix_UpdateRecommendItemList;
        private static DelegateBridge __Hotfix_UpdateRewardItemList;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRecommendItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRecommendItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRecommendItemClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, bool> EventOnRecommendItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, bool> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPosition(bool isRecommendItem)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLossWarningWindowDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetShipAllowInInfoAnchorPosition()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRecommendItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDrivingInfo(LBPassiveSkill m_currSkill, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDrivingQuestInfo(ConfigDataQuestInfo questInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRecommendItemList(List<RecommendItemInfo> recommendItemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRewardItemList(List<QuestRewardInfo> rewardList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSkillNode(ConfigDataDrivingLicenseInfo drvingLisenceInfo, int level, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

