﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainShipManageUIController : UIControllerBase
    {
        private const string CaptainIconItemName = "CaptainIconItem";
        public LBStaticHiredCaptain m_selCaptain;
        public List<int> m_captainStateList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCache;
        public int m_selShipIndex;
        private CommonCaptainIconUIController m_captainIconCtrl;
        private CommonShipRadarChartUIController m_shipRadarChartCtrl;
        private CommonUIStateController m_functionBtnStateCtrl;
        private SystemDescriptionController m_descriptionCtrl;
        private string FunctionButtonState_Activate;
        private string FunctionButtonState_Aboard;
        private string FunctionButtonState_Repair;
        private string m_shipListItemPoolName;
        private bool m_isShipListPanelShow;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnShipListItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnShipCaptainShowButtonClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ShipManagePanel/ShipListPanel/ShipListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ShipListScrollRect;
        [AutoBind("./ShipManagePanel/ShipListPanel/ShipListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ShipListUIItemPool;
        [AutoBind("./ShipManagePanel/ShipListPanel/ShipListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ShipListTempleteItemRoot;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ShipShowPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipShowPanel;
        [AutoBind("./ShipShowPanel/CaptainShowButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CaptainShowButton;
        [AutoBind("./UnwatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnwatchButton;
        [AutoBind("./ShipManagePanel/ShipListPanel/WatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WatchButton;
        [AutoBind("./ShipManagePanel/ShipListPanel/ShowListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShowListButton;
        [AutoBind("./ShipManagePanel/ShipListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipListPanelCtrl;
        [AutoBind("./ShipShowPanel/CaptainShowButton/CaptianInfo/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        [AutoBind("./ShipShowPanel/CaptainShowButton/CaptianInfo/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./ShipShowPanel/CaptainShowButton/CaptianInfo/LvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LvText;
        [AutoBind("./ShipShowPanel/FunctionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FunctionButton;
        [AutoBind("./ShipShowPanel/ShipRadarChart", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipRadarChart;
        [AutoBind("./ShipShowPanel/ShipRadarChart/ShipUseTypeNameText/ShipUseTypeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipUseTypeValueText;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipListInfo;
        private static DelegateBridge __Hotfix_UpdateCaptianIcon;
        private static DelegateBridge __Hotfix_SetShipInfoPanel;
        private static DelegateBridge __Hotfix_UpdateShipHangerSelectedState;
        private static DelegateBridge __Hotfix_UpdateSelectedShipInfo;
        private static DelegateBridge __Hotfix_GetShipManagerState;
        private static DelegateBridge __Hotfix_ChangeShipListShowState;
        private static DelegateBridge __Hotfix_ShowShipInfoPanel;
        private static DelegateBridge __Hotfix_ShowOrHideSystemDescriptionButton;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_GetRectByIndex;
        private static DelegateBridge __Hotfix_InitShipListPanel;
        private static DelegateBridge __Hotfix_ShowShipListPanel;
        private static DelegateBridge __Hotfix_OnCaptainShowButtonClick;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnShipListItemClick;
        private static DelegateBridge __Hotfix_OnShipListItemFill;
        private static DelegateBridge __Hotfix_add_EventOnShipListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipListItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipCaptainShowButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipCaptainShowButtonClick;

        public event Action<UIControllerBase> EventOnShipCaptainShowButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnShipListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ChangeShipListShowState(int startIdx)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(string state, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetRectByIndex(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipManagerState()
        {
        }

        [MethodImpl(0x8000)]
        private void InitShipListPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainShowButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipListItemClick(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipListItemFill(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSystemDescriptionButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipInfoPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipListPanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptianIcon(LBStaticHiredCaptain selCaptain, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedShipInfo(LBStaticHiredCaptain selCaptain, List<int> shipStateList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipHangerSelectedState(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipListInfo(LBStaticHiredCaptain selCaptain, List<int> shipStateList, Dictionary<string, UnityEngine.Object> resDict, int selectShipIndex = 0, int startIndex = 0)
        {
        }
    }
}

