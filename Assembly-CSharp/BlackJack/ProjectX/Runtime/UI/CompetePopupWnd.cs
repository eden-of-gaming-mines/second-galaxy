﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public interface CompetePopupWnd
    {
        void RegisterPopupWndCloseEvent(Action onEnd);
        void ShowPopupWnd();
    }
}

