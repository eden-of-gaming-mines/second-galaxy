﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonItemIconUICtrlHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateCommonItemIconWithItem_1;
        private static DelegateBridge __Hotfix_UpdateCommonItemIconWithItem_2;
        private static DelegateBridge __Hotfix_UpdateCommonItemIconWithItem_0;
        private static DelegateBridge __Hotfix_UpdateCommonItemIconWithShipConfigInfo;

        [MethodImpl(0x8000)]
        public static void UpdateCommonItemIconWithItem(ILBStoreItemClient item, CommonItemIconUIController itemUICtrl, Dictionary<string, Object> resDict, bool showItemCount = true, bool showLowProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void UpdateCommonItemIconWithItem(StoreItemType itemType, object configInfo, bool isBind, bool isFreeing, CommonItemIconUIController itemUICtrl, Dictionary<string, Object> resDict, long itemCount = -1L, bool showLowProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void UpdateCommonItemIconWithItem(StoreItemType itemType, object configInfo, bool isBind, bool isFreezing, CommonItemIconUIController itemUICtrl, Dictionary<string, Object> resDict, string itemCountStr, bool showLowProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void UpdateCommonItemIconWithShipConfigInfo(ConfigDataSpaceShipInfo shipConfigInfo, bool isBind, bool isFreezing, CommonItemIconUIController itemUICtrl, Dictionary<string, Object> resDict)
        {
        }
    }
}

