﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SceneLeaveNotfiyUIController : CommonPopupWndUIControllerBase
    {
        public float m_countDownTotalTime;
        public float m_countDownLeftTime;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnReachCountDownTime;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_maiStateController;
        [AutoBind("./DetailPanel/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NotifyText;
        [AutoBind("./DetailPanel/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CountDownTimeProcessBar;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ComfirmButton;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetEffectInfo;
        private static DelegateBridge __Hotfix_UpdateSceneLeaveNotify;
        private static DelegateBridge __Hotfix_add_EventOnReachCountDownTime;
        private static DelegateBridge __Hotfix_remove_EventOnReachCountDownTime;

        public event Action EventOnReachCountDownTime
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetEffectInfo(string uiStateName)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSceneLeaveNotify(float countDownTime, string notifyString, bool showConfirmButton)
        {
        }
    }
}

