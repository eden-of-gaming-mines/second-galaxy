﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class CustomUserGuideUIController : UIControllerBase
    {
        public Action<UIControllerBase> EventOnCustomLayerButtonClick;
        private string CustomRootPath;
        private string MarginAdjustRootPath;
        private LayerLayoutDesc layerLayoutDesc;
        public CommonUIStateController CustomUserGuideUIStateController;
        public Button CustomLayerButton;
        public Transform CustomRootTransform;
        public Transform MarginAdjustRootTransform;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_CopyLayerByPathList;
        private static DelegateBridge __Hotfix_CopyLayerByPath;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        private void CopyLayerByPath(string path)
        {
        }

        [MethodImpl(0x8000)]
        public void CopyLayerByPathList(List<string> customLayerCopyPathList)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

