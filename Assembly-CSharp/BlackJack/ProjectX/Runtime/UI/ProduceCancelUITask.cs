﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ProduceCancelUITask : UITaskBase
    {
        protected LBProductionLine m_currProduceLine4Self;
        protected GuildProductionLineInfo m_currProduceLine4Guild;
        public const string ProductionLineInfoKey = "LBProductionLineInfo";
        public ProduceCancelUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string UIMode_SelfProduce = "SelfProduceCance";
        public const string UIMode_GuildProduce = "GuildProduceCance";
        public const string TaskName = "ProduceCancelUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowProduceCancelWindow;
        private static DelegateBridge __Hotfix_ShowProduceCancelWindowForGuild;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_ClosePanel;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ProduceCancelUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClosePanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoUIPanel(ItemInfo itemInfo, ProduceCancelItemUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase ShowProduceCancelWindow(LBProductionLine produceLine, UIIntent returnToIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase ShowProduceCancelWindowForGuild(GuildProductionLineInfo lineInfo, UIIntent return2Intent = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey0
        {
            internal bool isGetCurrcyInfo;
            internal bool isGetItemInfo;
            internal ProduceCancelUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task currcyTask)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(Task itemStoreTask)
            {
            }
        }
    }
}

