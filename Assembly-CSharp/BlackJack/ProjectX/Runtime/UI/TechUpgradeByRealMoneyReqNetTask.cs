﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TechUpgradeByRealMoneyReqNetTask : NetWorkTransactionTask
    {
        private int m_techId;
        private int m_upLevel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TechUpgradeByRealMoneyResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTechUpgradeByRealMoneyAck;
        private static DelegateBridge __Hotfix_set_TechUpgradeByRealMoneyResult;
        private static DelegateBridge __Hotfix_get_TechUpgradeByRealMoneyResult;

        [MethodImpl(0x8000)]
        public TechUpgradeByRealMoneyReqNetTask(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeByRealMoneyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int TechUpgradeByRealMoneyResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

