﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ItemConsumptionMsgBoxUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./DetailPanel/PanelNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_text;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        [AutoBind("./DetailPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_slotRoot;
        [AutoBind("./DetailPanel/ItemGroup/Slot0", AutoBindAttribute.InitState.NotInit, false)]
        public ItemSlot m_slotTemplate;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FakeLBStoreItem, Vector3> OnItemDetailOpen;
        private readonly List<ItemSlot> m_slotList;
        private const string NameFormat = "Slot{0}";
        private const string PanelOpen = "PanelOpen";
        private const string PanelClose = "PanelClose";
        private static DelegateBridge __Hotfix_OnShowMsgBoxStart;
        private static DelegateBridge __Hotfix_IsItemEnough;
        private static DelegateBridge __Hotfix_SetOpenState;
        private static DelegateBridge __Hotfix_CreatCloseBothOrConfirmProcess;
        private static DelegateBridge __Hotfix_OnIconClick;
        private static DelegateBridge __Hotfix_SetIconClickAction;
        private static DelegateBridge __Hotfix_add_OnItemDetailOpen;
        private static DelegateBridge __Hotfix_remove_OnItemDetailOpen;

        private event Action<FakeLBStoreItem, Vector3> OnItemDetailOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreatCloseBothOrConfirmProcess(bool isCloseBoth)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsItemEnough()
        {
        }

        [MethodImpl(0x8000)]
        public void OnIconClick(FakeLBStoreItem item, Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShowMsgBoxStart(string title, ICollection<KeyValuePair<ILBItem, int>> items, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconClickAction(Action<FakeLBStoreItem, Vector3> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOpenState(bool isOpen, Action onComplete = null)
        {
        }

        [CompilerGenerated]
        private sealed class <SetOpenState>c__AnonStorey0
        {
            internal Action onComplete;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

