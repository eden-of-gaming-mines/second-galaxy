﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class CharacterChipSlotItemInfoUIController : UIControllerBase
    {
        public Action<PointerEventData, Action<PointerEventData>> EventOnBackGroundClick;
        private CommonItemIconUIController m_chipItemUICtrl;
        private CharacteristicItemUIController m_extraPropertyCtrl;
        private List<CharacteristicItemUIController> m_basicPropertyCtrlList;
        private int m_slotIndex;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StateCtrl;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform PanelTrans;
        [AutoBind("./Detail/ItemInfo/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummy;
        [AutoBind("./Detail/ItemInfo/ChipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./Detail/ItemInfo/ChipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TypeText;
        [AutoBind("./Detail/ExtraPropertyGroup/PropertyItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropertyItem;
        [AutoBind("./Detail/BasicPropretyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BasicPropretyGroup;
        [AutoBind("./Detail/ExtraPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ExtraPropertyGroup;
        [AutoBind("./BgBackGroudImage", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventCtrl;
        [AutoBind("./Detail/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buttonGroup;
        [AutoBind("./Detail/ButtonGroup/ReplaceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_chipChangeButton;
        [AutoBind("./Detail/ButtonGroup/DischargeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_dischargeButton;
        [AutoBind("./Detail/ButtonGroup/ReplaceButton/BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommendUIStateCtr;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetChipSlotInfo;
        private static DelegateBridge __Hotfix_SetChipInfoPosition;
        private static DelegateBridge __Hotfix_CreateChipSlotInfoPanelProcess;
        private static DelegateBridge __Hotfix_UpdateChipBasicPropertyInfoToUI;
        private static DelegateBridge __Hotfix_SetRecommendFlag;
        private static DelegateBridge __Hotfix_UpdateChipExtraPropertyInfoToUI;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_get_SlotIndex;

        [MethodImpl(0x8000)]
        public UIProcess CreateChipSlotInfoPanelProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipInfoPosition(RectTransform slotTransform, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipSlotInfo(int slotIndex, ConfigDataCharChipInfo chipInfo, int randomBufId, bool showButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecommendFlag(bool val)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChipBasicPropertyInfoToUI(ConfigDataBufInfo buffConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChipExtraPropertyInfoToUI(ConfigDataBufInfo bufConfInfo)
        {
        }

        public int SlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

