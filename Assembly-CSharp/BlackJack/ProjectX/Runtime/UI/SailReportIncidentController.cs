﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SailReportIncidentController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_statCtrl;
        [AutoBind("./IncidentDotGroup/StarName/StarNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starNameOnStat;
        [AutoBind("./NoIncidentDotGroup/StarName/StarNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starNameOffStat;
        [AutoBind("./IncidentDotGroup/IncidentDotImage", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_eventsRoot;
        private readonly List<SailReportEventController> m_eventList;
        private static DelegateBridge __Hotfix_SetName;
        private static DelegateBridge __Hotfix_SetEvents;

        [MethodImpl(0x8000)]
        public void SetEvents(IList<SailReportSolarSystemInfo.SailReportEvent> events, Dictionary<string, UnityEngine.Object> resDict, Action<SailReportEventController> playAnim = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetName(string starName)
        {
        }
    }
}

