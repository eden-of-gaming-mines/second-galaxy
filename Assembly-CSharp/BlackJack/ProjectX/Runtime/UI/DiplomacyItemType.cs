﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    [Flags]
    public enum DiplomacyItemType
    {
        None = 0,
        Player = 1,
        Guild = 2,
        Alliance = 4
    }
}

