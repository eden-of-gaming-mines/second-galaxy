﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CrackManagerUITask : UITaskBase
    {
        private bool m_isInCommonWay;
        public const string ParamKey_CrackResultItemList = "CrackResultItemList";
        public const string ParamKey_CrackAddBox = "CrackAddBox";
        public const string TaskMode_Normal = "NoramlMode";
        public const string TaskMode_CrackResult = "CrackResultMode";
        public const string TaskMode_Store = "StoreMode";
        private CanvasGroup m_canvasGroup;
        private CrackStoreUIController.BoxSortState m_currSortType;
        private bool m_isUp;
        private CrackBGTask m_crackBGTask;
        private CrackManagerUIController m_mainCtrl;
        private CrackStoreUIController m_storeCtrl;
        private bool m_isCrackBGTaskResouceLoadComplete;
        private List<ILBStoreItemClient> m_crackBoxList;
        private List<StoreItemUpdateInfo> m_crackResultItemInfoList;
        private ILBStoreItemClient m_selCrackBox;
        private int m_startIndex;
        private int m_currSlotIdx;
        private int m_currSelectedItemIdx;
        private bool m_isSelectedSlotNewItem;
        private bool m_isSpeedUP;
        private bool m_isKeepPlatformStateWhenSpeedUp;
        private bool m_isOpeningBox;
        private bool m_isSimpleUITaskOpen;
        private bool m_enterCrackFristShowCompleteAnim;
        private bool m_isCrackedBoxComplete;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CrackManagerUITask";
        private int m_currStep;
        private TweenPos2 m_destPositionTween;
        private bool m_isInRepeatUserGuide;
        public static string ParameKey_IsInRepeatUserGuide = "IsInRepeatUserGuide";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCrackUITask;
        private static DelegateBridge __Hotfix_StartCrackUITaskWithAddCrackBox;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearCacheData;
        private static DelegateBridge __Hotfix_RegisterPlayerContextEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayerContextEvent;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_FilterForAllCrackBox;
        private static DelegateBridge __Hotfix_FilterForCurrentCrackBoxType;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCrackStateOpenButtonClick;
        private static DelegateBridge __Hotfix_OnCrackStateOpenButtonWithMoneyClick;
        private static DelegateBridge __Hotfix_OnCrackSlotButtonClick;
        private static DelegateBridge __Hotfix_OnCrackSlotButtonClickImp;
        private static DelegateBridge __Hotfix_OnUnlockSlotConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnUnlockSlotReturnButtonClick;
        private static DelegateBridge __Hotfix_OnCrackSlotCloseButtonClick;
        private static DelegateBridge __Hotfix_OnCrackBoxClick;
        private static DelegateBridge __Hotfix_OnAddToCrackListButtonClick;
        private static DelegateBridge __Hotfix_OnSortTypeChanged;
        private static DelegateBridge __Hotfix_OnCrackingBoxInfoWndReturnButtonClick;
        private static DelegateBridge __Hotfix_OnCrackingBoxInfoWndSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_OnCrackReportWndBGButtonClick;
        private static DelegateBridge __Hotfix_OnCrackReportItemClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskPasue;
        private static DelegateBridge __Hotfix_OnCrackBGPlatformMoveEnd;
        private static DelegateBridge __Hotfix_OnCrackBGPlatformStarteEnd;
        private static DelegateBridge __Hotfix_OnCrackBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_OnStorePanelCloseButtonClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_StartCrackBGTask;
        private static DelegateBridge __Hotfix_GetCrackBoxModelResPath;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCrackBGTask;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_UpdateSeletedSlotInfo;
        private static DelegateBridge __Hotfix_UpdateCrackBG;
        private static DelegateBridge __Hotfix_UpdateSelecteSlot;
        private static DelegateBridge __Hotfix_CrackBoxInfoComparer;
        private static DelegateBridge __Hotfix_OnAddToCrackList;
        private static DelegateBridge __Hotfix_GetCanvasGroup;
        private static DelegateBridge __Hotfix_SendBoxGetRewardReq;
        private static DelegateBridge __Hotfix_HideUnlockSlotByRealMoneyWnd;
        private static DelegateBridge __Hotfix_HideCrackinfoWnd;
        private static DelegateBridge __Hotfix_HideCrackReportWnd;
        private static DelegateBridge __Hotfix_OnCrackCompleteNtf;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_RegistCrackBoxOpenTestEvent;
        private static DelegateBridge __Hotfix_UnRegistCrackBoxOpenTestEvent;
        private static DelegateBridge __Hotfix_ShowGetRewardTestWnd;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbItemStoreClient;
        private static DelegateBridge __Hotfix_get_LBCrackClient;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartCrackUITaskWithRepteableGuide;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuideWhenPositionRight;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_StopRepeatableUserGuide;
        private static DelegateBridge __Hotfix_ExistCrackBox;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_ClickFristCrackSlot;

        [MethodImpl(0x8000)]
        public CrackManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearCacheData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFristCrackSlot(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private int CrackBoxInfoComparer(ILBStoreItemClient BoxA, ILBStoreItemClient BoxB)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistCrackBox()
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterForAllCrackBox(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterForCurrentCrackBoxType(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private CanvasGroup GetCanvasGroup()
        {
        }

        [MethodImpl(0x8000)]
        public string GetCrackBoxModelResPath(ConfigDataCrackBoxInfo boxInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void HideCrackinfoWnd(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        private void HideCrackReportWnd(Action<UIProcess, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void HideUnlockSlotByRealMoneyWnd(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddToCrackList(ILBStoreItemClient boxItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddToCrackListButtonClick(ILBStoreItemClient boxItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackBGPlatformMoveEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackBGPlatformStarteEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackBoxClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackBoxOpenAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackCompleteNtf(int boxIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackingBoxInfoWndReturnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackingBoxInfoWndSpeedUpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackReportItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackReportWndBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackSlotButtonClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackSlotButtonClickImp(int idx, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackSlotCloseButtonClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackStateOpenButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackStateOpenButtonWithMoneyClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskPasue(Task task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCrackBGTask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(ILBStoreItemClient selItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeChanged(CrackStoreUIController.BoxSortState state)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStorePanelCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnlockSlotConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnlockSlotReturnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void RegistCrackBoxOpenTestEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendBoxGetRewardReq()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowGetRewardTestWnd(int result, List<StoreItemUpdateInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(StoreItemUpdateInfo itemUpdateInfo, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void StartCrackBGTask()
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartCrackUITask(UIIntent mainUIIntent, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static Task StartCrackUITaskWithAddCrackBox(UIIntent mainUIIntent, ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartCrackUITaskWithRepteableGuide(UIIntent mainUIIntent)
        {
        }

        [MethodImpl(0x8000)]
        public void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public void StartRepeatableUserGuideWhenPositionRight()
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegistCrackBoxOpenTestEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCrackBG(string modelResPath, string taskMode, bool isNew = false, bool isFristCracked = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelecteSlot()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSeletedSlotInfo(bool isChangeBg = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockItemStoreClient LbItemStoreClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCrackClient LBCrackClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnAddToCrackListButtonClick>c__AnonStorey2
        {
            internal ILBStoreItemClient boxItem;
            internal CrackManagerUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.CurrentIntent.TargetMode = "NoramlMode";
                this.$this.m_selCrackBox = this.boxItem;
                this.$this.EnablePipelineStateMask(CrackManagerUITask.PipeLineStateMaskType.AddToCrackList);
                this.$this.StartUpdatePipeLine(this.$this.CurrentIntent, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnCrackSlotButtonClickImp>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(r);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal bool istaskStartOrResume;
            internal CrackManagerUITask $this;

            internal void <>m__0(UIProcess process, bool succes)
            {
                this.$this.m_isOpeningBox = false;
            }

            internal void <>m__1(UIProcess process, bool succes)
            {
                this.$this.StartRepeatableUserGuideWhenPositionRight();
            }

            internal void <>m__2(UIProcess process, bool succes)
            {
                this.$this.StartRepeatableUserGuideWhenPositionRight();
                if (this.istaskStartOrResume)
                {
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterCrackUI, new object[0]);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            BoxSortTypeChange,
            SelectedBoxChanged,
            AddToCrackList,
            OnSelectedSlotChanged,
            CrackSlotChanged,
            IsShowAnimation
        }
    }
}

