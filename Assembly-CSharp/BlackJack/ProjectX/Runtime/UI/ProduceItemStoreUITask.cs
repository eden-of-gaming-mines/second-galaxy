﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ProduceItemStoreUITask : UITaskBase
    {
        protected bool m_isTriggerToggleEvent;
        protected int m_orignalBlueprintId;
        protected int m_selectedCategory;
        protected int m_selectedType;
        protected int m_seletedBlueprintId;
        protected bool m_isBind;
        protected bool m_isFreezing;
        protected bool m_isImmediate;
        protected bool m_isSelectedMode;
        protected List<ILBStoreItemClient> m_productionItemCache;
        protected ProduceItemStoreUIController m_mainCtrl;
        protected ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        protected ProduceThreeDBGUITask m_produceThreeDBGUITask;
        protected bool m_isStartBgTask;
        protected bool m_isProduceThreeDbguiTaskResourceLoadComplete;
        protected LogicBlockItemStoreClient m_lbItemStore;
        protected List<int> m_blueprintCategoryList;
        protected Dictionary<int, List<int>> m_blueprintCategoryToTypeInfoDict;
        protected Dictionary<int, int> m_blueprintTypeToCategoryDict;
        public static string ParamKey_IsSelectedMode;
        public static string ParamKey_SelectedBlueprintId;
        public static string ParamKey_BindState;
        public static string ParamKey_FreezingState;
        public static string ParamKey_IsImmediate;
        public const string UIMode_SelfProduce = "SelfProduce";
        public const string UIMode_GuildProduce = "GuildProduce";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ProduceItemStoreUITask";
        private bool m_isInRepeatableUserGuide;
        private BlueprintCategory m_userGuideSelectCategory;
        private BlueprintType m_userGuideSelectType;
        private GuideState m_currGuideState;
        private const string ParamKey_IsInRepeatbleUserGuide = "IsInRepeatbleUserGuide";
        private const string ParamKey_GuideItemType = "ItemType";
        private const string ParamKey_GuideItemConfig = "ItemConfig";
        private bool m_userGuideItemIsBind;
        private bool m_userGuideItemIsFreezing;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartProduceItemStoreUITask;
        private static DelegateBridge __Hotfix_SetOrignalBlueprint;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCahceForSelfProduce;
        private static DelegateBridge __Hotfix_UpdateDataCahceForGuildProduce;
        private static DelegateBridge __Hotfix_GetParamFormIntentForSelfProduce;
        private static DelegateBridge __Hotfix_GetParamFormIntentForGuildProduce;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnCategoryToggleSelected;
        private static DelegateBridge __Hotfix_OnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_OnItemTypeToggleSelectedImp;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClickImp;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoCloseProcess;
        private static DelegateBridge __Hotfix_GetBgPanelShowProcess;
        private static DelegateBridge __Hotfix_Return2SelfProduceUITask;
        private static DelegateBridge __Hotfix_Return2GuildProduceUITask;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_InitDataCacheForSelfProduce;
        private static DelegateBridge __Hotfix_InitDataCacheForGuildProduce;
        private static DelegateBridge __Hotfix_GetCategory2BlueprintCountDic;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoImp;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskChooseButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskChooseButtonClickImp;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoChooseButtonClick4Self;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoChooseButtonClick4Guild;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUIPause;
        private static DelegateBridge __Hotfix_ClearContext;
        private static DelegateBridge __Hotfix_StartBgUITask;
        private static DelegateBridge __Hotfix_OnProduceBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_CreateProduceBGUIBackgroupManager;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartProduceStoreUITaskForNotForceGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_IsCategoryToggleOpen;
        private static DelegateBridge __Hotfix_OpenCategoryToggleNode;
        private static DelegateBridge __Hotfix_IsTypeToggleSelected;
        private static DelegateBridge __Hotfix_SelectedTypeToggle;
        private static DelegateBridge __Hotfix_SetTypeToggleState;
        private static DelegateBridge __Hotfix_GetProduceItemRectById;
        private static DelegateBridge __Hotfix_GetTypeToggleRectById;
        private static DelegateBridge __Hotfix_GetCategoryToggleRect;
        private static DelegateBridge __Hotfix_ExistAppointItem;
        private static DelegateBridge __Hotfix_ClickItemSimpleInfoUITaskChooseButton;
        private static DelegateBridge __Hotfix_ClickItemIcon;

        [MethodImpl(0x8000)]
        public ProduceItemStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickItemIcon(int id, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickItemSimpleInfoUITaskChooseButton(int id, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoUIPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private ProduceBGUIBackgroupManager CreateProduceBGUIBackgroupManager()
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistAppointItem(int id)
        {
        }

        [MethodImpl(0x8000)]
        protected CommonUIStateEffectProcess GetBgPanelShowProcess(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected Dictionary<int, long> GetCategory2BlueprintCountDic(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCategoryToggleRect(int category)
        {
        }

        [MethodImpl(0x8000)]
        protected CustomUIProcess GetItemSimpleInfoCloseProcess()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetParamFormIntentForGuildProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetParamFormIntentForSelfProduce()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetProduceItemRectById(int blueprintId)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetTypeToggleRectById(int type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataCacheForGuildProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataCacheForSelfProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCategoryToggleOpen(int category)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTypeToggleSelected(int type)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCategoryToggleSelected(ProduceItemStoreCategoryToggleUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClickImp(bool isChoose = true, bool isCloseItemSimpleInfo = true, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoChooseButtonClick4Guild(ILBStoreItemClient item, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoChooseButtonClick4Self(ILBStoreItemClient item, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUIPause(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskChooseButtonClick(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskChooseButtonClickImp(ILBStoreItemClient item, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemTypeToggleSelected(int type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemTypeToggleSelectedImp(int type, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnProduceBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenCategoryToggleNode(int category, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        protected void Return2GuildProduceUITask(UIIntentReturnable intent, int templeteId, bool isChoose, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void Return2SelfProduceUITask(UIIntentReturnable intent, int blueprintId, bool isBind, bool isFreezing, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectedTypeToggle(int type, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOrignalBlueprint(int blueprintId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTypeToggleState(int type)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoImp(ItemInfo itemInfo, Vector3 pos, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBgUITask(UIIntent uitaskIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static ProduceItemStoreUITask StartProduceItemStoreUITask(string mode, UIIntent mainUIIntent, int techId = 0, bool isBind = false, bool isFreezing = false, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ProduceItemStoreUITask StartProduceStoreUITaskForNotForceGuide(UIIntent returnToIntent, StoreItemType type, int configID)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCahceForGuildProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCahceForSelfProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ExistAppointItem>c__AnonStorey5
        {
            internal int id;

            internal bool <>m__0(ILBStoreItemClient item) => 
                ((item.GetItemType() == StoreItemType.StoreItemType_ProduceBlueprint) && (item.GetConfigInfo<ConfigDataProduceBlueprintInfo>().ID == this.id));
        }

        [CompilerGenerated]
        private sealed class <GetItemSimpleInfoCloseProcess>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <InitDataCacheForGuildProduce>c__AnonStorey4
        {
            internal int category;

            internal bool <>m__0(int e) => 
                (e == this.category);
        }

        [CompilerGenerated]
        private sealed class <InitDataCacheForSelfProduce>c__AnonStorey3
        {
            internal int category;
            internal ConfigDataBlueprintTypeInfo blueprintTypeInfo;

            internal bool <>m__0(int e) => 
                (e == this.category);

            internal bool <>m__1(ILBStoreItemClient item)
            {
                if (item.GetItemType() != StoreItemType.StoreItemType_ProduceBlueprint)
                {
                    return false;
                }
                ConfigDataProduceBlueprintInfo configInfo = item.GetConfigInfo<ConfigDataProduceBlueprintInfo>();
                return ((configInfo != null) && (configInfo.BlueprintType == this.blueprintTypeInfo.ID));
            }
        }

        [CompilerGenerated]
        private sealed class <OnCloseButtonClickImp>c__AnonStorey1
        {
            internal bool isChoose;
            internal Action<bool> onEnd;
            internal ProduceItemStoreUITask $this;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                int templeteId = !this.isChoose ? this.$this.m_orignalBlueprintId : this.$this.m_seletedBlueprintId;
                bool isBind = this.$this.m_isBind;
                bool isFreezing = this.$this.m_isFreezing;
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (currIntent != null)
                {
                    string currMode = this.$this.m_currMode;
                    if (currMode != null)
                    {
                        if (currMode == "GuildProduce")
                        {
                            this.$this.Return2GuildProduceUITask(currIntent, templeteId, this.isChoose, this.onEnd);
                        }
                        else if (currMode == "SelfProduce")
                        {
                            this.$this.Return2SelfProduceUITask(currIntent, templeteId, isBind, isFreezing, this.onEnd);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal bool isInitOrResume;
            internal ProduceItemStoreUITask $this;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                if (this.isInitOrResume && (this.$this.m_currMode == "SelfProduce"))
                {
                    if (this.$this.m_currGuideState == ProduceItemStoreUITask.GuideState.Start)
                    {
                        this.$this.ShowNotForceUserGuide();
                    }
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterProduceBlueprintItemStore, new object[0]);
                }
            }
        }

        private enum GuideState
        {
            None,
            Start,
            ChooseCategory,
            ChooseType
        }

        public enum PipeLineMaskType
        {
            UpdatePipeLineing = 1,
            InitOrResume = 2,
            ToggleSwitch = 3
        }
    }
}

