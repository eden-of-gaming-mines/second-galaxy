﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CaptainLevelUpReportUITask : UITaskBase
    {
        public static string ParamKey_CaptainInstanceId;
        public static string ParamKey_PreLevel;
        public static string ParamKey_CurrentLevel;
        public static string ParamKey_NewFeatsList;
        public static string ParamKey_UpFeatsList;
        public static string ParamKey_PreFeatsList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBGButtonClick;
        private CaptainLevelUpReportUIController m_mainCtrl;
        private LBStaticHiredCaptain m_currCaptain;
        private int m_captainPreLevel;
        private int m_captainCurrentLevel;
        private List<LBNpcCaptainFeats> m_preFeatsList;
        private List<LBNpcCaptainFeats> m_newFeatsList;
        private List<LBNpcCaptainFeats> m_upgradedFeatsList;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCaptainLevelUpReportUITask;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_GetCacheDataFromUIIntent;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainLevelUpReportUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void GetCacheDataFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static CaptainLevelUpReportUITask StartCaptainLevelUpReportUITask(ulong captainInsId, int preLevel, int currentLevel, List<LBNpcCaptainFeats> preFeats, List<LBNpcCaptainFeats> newFeats, List<LBNpcCaptainFeats> upFeats)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

