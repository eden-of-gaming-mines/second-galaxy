﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class CharacterSkillTipsController : UIControllerBase
    {
        public Action<PointerEventData, Action<PointerEventData>> EventOnBackGroundClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BgImage;
        [AutoBind("./Details", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController tipStateUICtrl;
        [AutoBind("./BgBackGroudImage", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventCtrl;
        [AutoBind("./Details/PropertyMeanText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SkillSimpleDescText;
        [AutoBind("./Details/SkillDetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SkillDetailText;
        [AutoBind("./Details/BattleDetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BattleDetailText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateCharSkillTipsInfo;
        private static DelegateBridge __Hotfix_SetTipWndPos;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTipWndPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCharSkillTipsInfo(string skillSimpleDesc, string skillDetailDesc, string battleDetailDesc)
        {
        }
    }
}

