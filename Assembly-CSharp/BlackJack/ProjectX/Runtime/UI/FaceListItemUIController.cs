﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class FaceListItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FaceListItemUIController> EventOnItemIconClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_faceButton;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_faceCtrl;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image FaceImg;
        [AutoBind("./SexImage/Male", AutoBindAttribute.InitState.NotInit, false)]
        private Image MaleImg;
        [AutoBind("./SexImage/Female", AutoBindAttribute.InitState.NotInit, false)]
        private Image FeMaleImg;
        public CommonItemIconUIController m_faceIconCtrl;
        public ScrollItemBaseUIController m_scrollItemCtrl;
        public int m_resId;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetFaceInfoToUI;
        private static DelegateBridge __Hotfix_GetItemIconWidth;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemIconClick;

        public event Action<FaceListItemUIController> EventOnItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public float GetItemIconWidth()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFaceInfoToUI(int resId, Dictionary<string, Object> resDict)
        {
        }
    }
}

