﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class WorldEnterReqNetTask : NetWorkTransactionTask
    {
        private WorldEnterReqResultState m_resultState;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationEnterNtf;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;
        private static DelegateBridge __Hotfix_OnWorldEnterAck;
        private static DelegateBridge __Hotfix_get_ResultState;
        private static DelegateBridge __Hotfix_set_ResultState;

        [MethodImpl(0x8000)]
        public WorldEnterReqNetTask(bool skipRelogin = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSpaceStationEnterNtf(SpaceStationEnterNtf msg)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWorldEnterAck(int res)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public WorldEnterReqResultState ResultState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public enum WorldEnterReqResultState
        {
            None,
            SpaceStationEnter,
            SpaceEnter,
            EnterWorldFailed
        }
    }
}

