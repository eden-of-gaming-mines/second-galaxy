﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneScreenEffectUIController : UIControllerBase
    {
        [AutoBind("./ActiveEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_activeEffectTrans;
        [AutoBind("./InactiveEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_inactiveEffectTrans;
        private Dictionary<SceneScreenEffectType, GameObject> m_activeEffectType2ObjDict;
        private Dictionary<SceneScreenEffectType, GameObject> m_inactiveEffectType2ObjDict;
        private static DelegateBridge __Hotfix_AddSceneScreenEffect;
        private static DelegateBridge __Hotfix_RemoveSceneScreenEffect;
        private static DelegateBridge __Hotfix_RemoveAllSceneScreenEffect;

        [MethodImpl(0x8000)]
        public bool AddSceneScreenEffect(SceneScreenEffectType effectType, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAllSceneScreenEffect()
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveSceneScreenEffect(SceneScreenEffectType effectType)
        {
        }
    }
}

