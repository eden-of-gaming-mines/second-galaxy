﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class GuidMiningAndWarBoxUITask : GuildUITaskBase
    {
        public static readonly string ShowWarTime;
        public static readonly string ShowMiningTime;
        private string m_targetMode;
        private GuildMiningAndWarBoxUIController m_guildMiningAndWarBoxUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_GetListData;
        private static DelegateBridge __Hotfix_OnYesButtonClick;
        private static DelegateBridge __Hotfix_OnNoButtonClick;
        private static DelegateBridge __Hotfix_OnCloseDetailInfoPanelButtonClick;
        private static DelegateBridge __Hotfix_OnOpenDetailInfoPanelButtonClick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_PauseProcess;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuidMiningAndWarBoxUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void GetListData(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseDetailInfoPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNoButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenDetailInfoPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnYesButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void PauseProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetListData>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
        }

        [CompilerGenerated]
        private sealed class <GetListData>c__AnonStorey1
        {
            internal GuildMiningBalanceTimeInfoReqNetTask netTask;
            internal GuidMiningAndWarBoxUITask.<GetListData>c__AnonStorey0 <>f__ref$0;

            internal void <>m__0(Task task)
            {
                if (((GuildMiningBalanceTimeInfoReqNetTask) task).IsNetworkError)
                {
                    if (this.<>f__ref$0.onPrepareEnd != null)
                    {
                        this.<>f__ref$0.onPrepareEnd(false);
                    }
                }
                else if (this.netTask.m_result == 0)
                {
                    if (this.<>f__ref$0.onPrepareEnd != null)
                    {
                        this.<>f__ref$0.onPrepareEnd(true);
                    }
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(this.netTask.m_result, true, false);
                    if (this.<>f__ref$0.onPrepareEnd != null)
                    {
                        this.<>f__ref$0.onPrepareEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnYesButtonClick>c__AnonStorey2
        {
            internal GuildBattleReinforcementEndTimeSetReqNetTask netTask;
            internal GuidMiningAndWarBoxUITask $this;

            internal void <>m__0(Task task)
            {
                if (((GuildBattleReinforcementEndTimeSetReqNetTask) task).IsNetworkError)
                {
                }
                if (this.netTask.m_result == 0)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_BattleReinforcementEndTimeSet, false, new object[0]);
                    this.$this.PauseProcess();
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(this.netTask.m_result, true, false);
                    this.$this.EnablePipelineStateMask(GuidMiningAndWarBoxUITask.PipeLineStateMaskType.UpdateTime);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnYesButtonClick>c__AnonStorey3
        {
            internal GuildMiningBalanceTimeResetReqNetTask netTask;
            internal GuidMiningAndWarBoxUITask $this;

            internal void <>m__0(Task task)
            {
                if (((GuildMiningBalanceTimeResetReqNetTask) task).IsNetworkError)
                {
                }
                if (this.netTask.m_result == 0)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_MiningBalanceTimeSet, false, new object[0]);
                    this.$this.PauseProcess();
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(this.netTask.m_result, true, false);
                    this.$this.EnablePipelineStateMask(GuidMiningAndWarBoxUITask.PipeLineStateMaskType.UpdateTime);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpdateTime
        }
    }
}

