﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemNavigationFinalPointUIController : UIControllerBase
    {
        private Image[] m_halfCircleImages;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_navigationPointCtrl;
        [AutoBind("./CircleGroup/CircleGroup/Self/Circle", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_externalCircleImage;
        [AutoBind("./CircleGroup/CircleGroup/Half1/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_halfCircleImage1;
        [AutoBind("./CircleGroup/CircleGroup/Half2/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_halfCircleImage2;
        [AutoBind("./CircleGroup/CircleGroup/Half3/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_halfCircleImage3;
        private static DelegateBridge __Hotfix_ShowNavigationPoint;
        private static DelegateBridge __Hotfix_HideNavigationPoint;
        private static DelegateBridge __Hotfix_SetNavgationPointStateByColorCount;
        private static DelegateBridge __Hotfix_SetNavigationPointColor;
        private static DelegateBridge __Hotfix_get_HalfCircleImages;

        [MethodImpl(0x8000)]
        public void HideNavigationPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void SetNavgationPointStateByColorCount(int colorNum)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNavigationPointColor(List<Color> colorList)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNavigationPoint(List<Color> colorList)
        {
        }

        private Image[] HalfCircleImages
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

