﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipWeaponEquipDetailInfoPanelUIController : UIControllerBase
    {
        protected bool m_isShow;
        protected LBStaticWeaponEquipSlotGroup m_currSlotGroup;
        protected bool m_isAmmo;
        protected AmmoInfo m_ammoInfo;
        protected bool m_isOnlyViewDetail;
        protected Dictionary<string, UnityEngine.Object> m_resDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDetailInfoUITabChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoTechButtonClick;
        private HangarShipWeaponEquipDetailInfoUIController m_weaponEquipDetailInfoUICtrl;
        private string m_weaponEquipDetailInfoAssetName;
        [AutoBind("./WeaponEquipDetailInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_eaponEquipDetailInfoDummy;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./ReplaceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ReplaceButton;
        [AutoBind("./RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowWeaponEquipDetailInfo;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfoAssemblyMode;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfoOnlyViewMode;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfoOnlyViewModeImp;
        private static DelegateBridge __Hotfix_UpdateWeaponEquipInfoAssemblyModeImp;
        private static DelegateBridge __Hotfix_OnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;

        public event Action EventOnDetailInfoUITabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailInfoUITabChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowWeaponEquipDetailInfo(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponEquipInfoAssemblyMode(LBStaticWeaponEquipSlotGroup slotGroup, bool isAmmo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateWeaponEquipInfoAssemblyModeImp(LBStaticWeaponEquipSlotGroup slotGroup, bool isAmmo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponEquipInfoOnlyViewMode(LBStaticWeaponEquipSlotGroup slotGroup, AmmoInfo ammoInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateWeaponEquipInfoOnlyViewModeImp(LBStaticWeaponEquipSlotGroup slotGroup, AmmoInfo ammoInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

