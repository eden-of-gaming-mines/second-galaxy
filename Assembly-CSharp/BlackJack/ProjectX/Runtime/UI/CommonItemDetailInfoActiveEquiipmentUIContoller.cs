﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class CommonItemDetailInfoActiveEquiipmentUIContoller : CommonItemDetailInfoHasEquipedEffectUIBase
    {
        public string m_propertyUIPrefabAssetName;
        [AutoBind("./Viewport/Content/EquipedEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EquipedEffectRoot;
        [AutoBind("./Viewport/Content/LaunchRoot/EnergyConsume/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LaunchEnergyCostNumber;
        [AutoBind("./Viewport/Content/LaunchRoot/EnergyConsume/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchEnergyCostDownArrow;
        [AutoBind("./Viewport/Content/LaunchRoot/EnergyConsume/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchEnergyCostUpArrow;
        [AutoBind("./Viewport/Content/LaunchRoot/Interval/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LaunchCDNumber;
        [AutoBind("./Viewport/Content/LaunchRoot/Interval/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchCDDownArrow;
        [AutoBind("./Viewport/Content/LaunchRoot/Interval/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchCDUpArrow;
        [AutoBind("./Viewport/Content/LaunchRoot/Range", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeRoot;
        [AutoBind("./Viewport/Content/LaunchRoot/Range/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeTitleText;
        [AutoBind("./Viewport/Content/LaunchRoot/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeValueText;
        [AutoBind("./Viewport/Content/LaunchRoot/Range/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMaxDownArrow;
        [AutoBind("./Viewport/Content/LaunchRoot/Range/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMaxUpArrow;
        [AutoBind("./Viewport/Content/ReleaseEffectRoot/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ReleaseEffectInfo;
        [AutoBind("./Viewport/Content/BaseDataRoot/TechLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechLevelValue;
        [AutoBind("./Viewport/Content/BaseDataRoot/QualityRank/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QualityRankValue;
        [AutoBind("./Viewport/Content/BaseDataRoot/PackedSize/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackedSizeValue;
        [AutoBind("./Viewport/Content/BaseDataRoot/SizeType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SizeTypeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateActiveEquipmentUI;
        private static DelegateBridge __Hotfix_UpdateLaunchDataUI_0;
        private static DelegateBridge __Hotfix_UpdateLaunchDataUI_1;
        private static DelegateBridge __Hotfix_UpdateBasDataUI;
        private static DelegateBridge __Hotfix_IsAllActiveEquipment;
        private static DelegateBridge __Hotfix_SetLaunchEnergyCost;
        private static DelegateBridge __Hotfix_SetLanchCD;
        private static DelegateBridge __Hotfix_SetRangeMax;
        private static DelegateBridge __Hotfix_SetReleaseEffectInfo;
        private static DelegateBridge __Hotfix_ChangeArrowWithChangeValue;

        [MethodImpl(0x8000)]
        private void ChangeArrowWithChangeValue(GameObject downArrow, GameObject upArrow, float changeValue = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllActiveEquipment(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetLanchCD(ILBItem itemInfo, int compareValue = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLaunchEnergyCost(ILBItem itemInfo, float changeValue = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRangeMax(ILBItem itemInfo, float changeValue = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void SetReleaseEffectInfo(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActiveEquipmentUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBasDataUI(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLaunchDataUI(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLaunchDataUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }
    }
}

