﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class HangarShipInfoPanelUIController : UIControllerBase
    {
        [AutoBind("./ShipInfoPanel/ReNameButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipRenameButton;
        [AutoBind("./SuperWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SuperWeaponButton;
        [AutoBind("./NormalWeaponButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NormalWeaponButton;
        [AutoBind("./RepairButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RepairButton;
        [AutoBind("./BoosterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BoosterButton;
        [AutoBind("./TacticalEquipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TacticalEquipButton;
        [AutoBind("./SuperWeaponButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image SuperWeaponIcon;
        [AutoBind("./NormalWeaponButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image NormalWeaponIcon;
        [AutoBind("./RepairButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image RepairIcon;
        [AutoBind("./BoosterButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image BoosterIcon;
        [AutoBind("./TacticalEquipButton/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image TacticalEquipIcon;
        [AutoBind("./ShipInfoPanel/ShipTypeIcon", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ShipTypeIcon;
        [AutoBind("./ShipInfoPanel/ForceIconImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ForceIcon;
        [AutoBind("./ShipInfoPanel/ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShipNameText;
        [AutoBind("./ShipInfoPanel/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShipTypeText;
        [AutoBind("./ShipInfoPanel/ShipTechImage", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ShipTechImage;
        private static DelegateBridge __Hotfix_UpdateShipInfo_1;
        private static DelegateBridge __Hotfix_UpdateShipInfo_0;
        private static DelegateBridge __Hotfix_SetShipName;
        private static DelegateBridge __Hotfix_SetShipTypeText;
        private static DelegateBridge __Hotfix_SetShipWeaponEquipIcon;
        private static DelegateBridge __Hotfix_SetShipInfoShipIcon;

        [MethodImpl(0x8000)]
        private void SetShipInfoShipIcon(ILBStaticShip shipHangarInfo, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipName(ILBStaticShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipTypeText(ILBStaticShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipWeaponEquipIcon(LogicBlockShipCompStaticResource lbShipResouce, Dictionary<string, Object> dynamicResCacheDict, bool isPlayerShip = true)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInfo(ILBStaticFlagShip shipInfo, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInfo(ILBStaticPlayerShip shipInfo, Dictionary<string, Object> dynamicResCacheDict)
        {
        }
    }
}

