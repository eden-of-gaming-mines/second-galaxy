﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipTransformSingleItemToMSStoreReqNetTask : NetWorkTransactionTask
    {
        private int m_hangarShipIndex;
        private int m_storeItemIndex;
        private long m_storeItemCount;
        private int m_ackResult;
        public bool m_isDeleteSrcItem;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnHangarShipTransformSingleItemToMSStoreAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;

        [MethodImpl(0x8000)]
        public HangarShipTransformSingleItemToMSStoreReqNetTask(int hangarShipIndex, int storeItemIndex, long storeItemCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarShipTransformSingleItemToMSStoreAck(int result, bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

