﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceStationBGUIBackgroupManager : IUIBackgroundManager
    {
        private UIIntent m_bgIntent;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetManagerIntent;
        private static DelegateBridge __Hotfix_SetManagerIntent;
        private static DelegateBridge __Hotfix_OnLeaveBackground;
        private static DelegateBridge __Hotfix_OnReEnterBackground;

        [MethodImpl(0x8000)]
        public UIIntent GetManagerIntent()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLeaveBackground(string popUITaskName)
        {
        }

        [MethodImpl(0x8000)]
        public void OnReEnterBackground(string popUITaskName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetManagerIntent(UIIntent intent)
        {
        }
    }
}

