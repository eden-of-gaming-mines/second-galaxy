﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class SpaceStationBuildingAnimationController : UIControllerBase
    {
        private static DelegateBridge __Hotfix_PlaySelectedAnimation;
        private static DelegateBridge __Hotfix_PlayBuildingSelectAnimation;

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator PlayBuildingSelectAnimation(Animator anim)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySelectedAnimation()
        {
        }

        [CompilerGenerated]
        private sealed class <PlayBuildingSelectAnimation>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Animator anim;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

