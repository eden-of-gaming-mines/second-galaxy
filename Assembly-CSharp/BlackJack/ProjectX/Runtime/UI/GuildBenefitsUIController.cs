﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBenefitsUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <SovereignItemButtonStateName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GuildSovereigntyBenefitsInfo <SovereigntyBenefits>k__BackingField;
        public const string GuildBenefitsCommonItem = "GuildBenefitsCommonItem";
        public const string CommmonItemNoMoreGroupStateShow = "Show";
        public const string CommonItemNoMoreGroupStateClose = "Close";
        public const int MaxInformationPointBenefitCount = 5;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnInforamtionPointItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonItemRewardsPreviewClick;
        private readonly List<GuildBenefitsInformationPointItemUIController> m_informationPointItemCtrls;
        private Dictionary<string, UnityEngine.Object> m_dynamicResDic;
        private List<GuildCommonBenefitsInfo> m_commonBenefitsList;
        private GuildSovereignMaintenanceDesController m_maintenanceDesCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildBenefitsStateCtrl;
        [AutoBind("./InformationPointSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_informationPointSimpleInfoTrans;
        [AutoBind("./CommonSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_commonSimpleInfoTrans;
        [AutoBind("./LeftGroup/DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_informationPointTipOpenButton;
        [AutoBind("./LeftGroup/ResourceTitleText/ResourceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_selfInformationPointText;
        [AutoBind("./LeftGroup/AwardItemGroup/VitalityRewardPanel5", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_informationPointItemGo5;
        [AutoBind("./LeftGroup/AwardItemGroup/VitalityRewardPanel4", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_informationPointItemGo4;
        [AutoBind("./LeftGroup/AwardItemGroup/VitalityRewardPanel3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_informationPointItemGo3;
        [AutoBind("./LeftGroup/AwardItemGroup/VitalityRewardPanel2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_informationPointItemGo2;
        [AutoBind("./LeftGroup/AwardItemGroup/VitalityRewardPanel1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_informationPointItemGo1;
        [AutoBind("./LeftGroup/AwardItemGroup/AwardBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_progressBarImage;
        [AutoBind("./LeftGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_resetTimeText;
        [AutoBind("./NormalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonItemAreaStateCtrl;
        [AutoBind("./NormalGroup/NormalTitleImage/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_commonItemAreaRedPointGo;
        [AutoBind("./NormalGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_commonGroupScrollRect;
        [AutoBind("./NormalGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_commonItemPool;
        [AutoBind("./NormalGroup/Scroll View/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_templateCommonItemRootTf;
        [AutoBind("./StarSupplyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sovereigntyItemStateCtrl;
        [AutoBind("./StarSupplyGroup/StarTitleImage/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sovereigntyRedPointGo;
        [AutoBind("./StarSupplyGroup/StarTitleImage/RewardText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sovereigntyNumberText;
        [AutoBind("./StarSupplyGroup/StarTitleImage/RewardTitleText/StarDealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sovereigntyTipPanelOpenButton;
        [AutoBind("./StarSupplyGroup/ContentGroup/GSupplyImage/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_sovereigntyCurrencyImage;
        [AutoBind("./StarSupplyGroup/ContentGroup/GSupplyImage/SupplyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sovereigntyCurrencyText;
        [AutoBind("./StarSupplyGroup/ContentGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sovereigntyItemButtonStateCtrl;
        [AutoBind("./StarSupplyGroup/ContentGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sovereigntyItemButton;
        [AutoBind("./StarSupplyGroup/ContentGroup/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sovereigntyShopButton;
        [AutoBind("./StarSupplyGroup/StarTitleImage/RewardText/MaintenanceGroup/MaintenanceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_maintenanceText;
        [AutoBind("./StarSupplyGroup/StarTitleImage/RewardText/MaintenanceGroup/MaintenanceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_maintenanceDetailBtn;
        [AutoBind("./MaintenanceInfoPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_maintenanceDetailDescCtrlObj;
        [AutoBind("./AwardDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_informationPointTipPanelStateCtrl;
        [AutoBind("./AwardDetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_informationPointTipCloseButton;
        [AutoBind("./StarDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sovereigntyTipPanelStateCtrl;
        [AutoBind("./StarDetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sovereigntyTipPanelCloseButton;
        private static DelegateBridge __Hotfix_CreateGuildBenefitsPanelProcess;
        private static DelegateBridge __Hotfix_UpdateInformationPointTipPanelState;
        private static DelegateBridge __Hotfix_UpdateSovereigntyTipPanelState;
        private static DelegateBridge __Hotfix_UpdateInformationPointArea;
        private static DelegateBridge __Hotfix_UpdateCommonArea;
        private static DelegateBridge __Hotfix_UpdateSovereigntyArea;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnInformationPointItemClick;
        private static DelegateBridge __Hotfix_OnCommonItemClick;
        private static DelegateBridge __Hotfix_OnItemPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnCommonItemFill;
        private static DelegateBridge __Hotfix_OnPreviewRewardItemInCommonBenefitsClick;
        private static DelegateBridge __Hotfix_UpdateSovereigntyNumber;
        private static DelegateBridge __Hotfix_get_SovereignItemButtonStateName;
        private static DelegateBridge __Hotfix_set_SovereignItemButtonStateName;
        private static DelegateBridge __Hotfix_get_SovereigntyBenefits;
        private static DelegateBridge __Hotfix_set_SovereigntyBenefits;
        private static DelegateBridge __Hotfix_add_EventOnInforamtionPointItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnInforamtionPointItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonItemRewardsPreviewClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonItemRewardsPreviewClick;

        public event Action<UIControllerBase> EventOnCommonItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCommonItemRewardsPreviewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnInforamtionPointItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildBenefitsPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreviewRewardItemInCommonBenefitsClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCommonArea(string stateName, List<GuildCommonBenefitsInfo> commonItemInfos, bool refreshContentFromBegin, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInformationPointArea(long selfPoint, float guildPointRate, List<string> itemStates, List<GuildInformationPointBenefitsInfo> informationPointInfos, List<ConfigDataGuildBenefitsConfigInfo> defaultInfos, Dictionary<string, UnityEngine.Object> resDict, TimeSpan timeLeft)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInformationPointTipPanelState(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSovereigntyArea(string titleStateName, int solarSystemCount, string buttonStateName, GuildSovereigntyBenefitsInfo sovereigntyItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSovereigntyNumber(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSovereigntyTipPanelState(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        public string SovereignItemButtonStateName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public GuildSovereigntyBenefitsInfo SovereigntyBenefits
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

