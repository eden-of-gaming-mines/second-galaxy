﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum ProduceLineState
    {
        Normal,
        PutIn,
        Producing,
        ProduceComplete,
        Lost
    }
}

