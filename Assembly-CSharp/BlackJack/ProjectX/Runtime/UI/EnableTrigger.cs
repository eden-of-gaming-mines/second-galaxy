﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class EnableTrigger : MonoBehaviour
    {
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }
    }
}

