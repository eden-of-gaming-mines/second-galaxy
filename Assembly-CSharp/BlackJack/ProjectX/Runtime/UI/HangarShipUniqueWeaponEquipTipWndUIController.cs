﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class HangarShipUniqueWeaponEquipTipWndUIController : UIControllerBase
    {
        private ILBStaticShip m_superEquipWeaponInfo;
        private ILBStaticShip m_normalWeaponInfo;
        private ILBStaticShip m_repairerInfo;
        private ILBStaticShip m_boosterInfo;
        private ILBStaticShip m_tacticalEquipInfo;
        private const string PanelOpenState = "PanelOpen";
        private const string PanelCloseState = "PanelClose";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./SuperWeaponInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_superWeaponInfoStateCtrl;
        [AutoBind("./SuperWeaponInfo/IconMask/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SuperWeaponIconImage;
        [AutoBind("./SuperWeaponInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponNameText;
        [AutoBind("./SuperWeaponInfo/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponDescText;
        [AutoBind("./NormalWeaponInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_normalWeaponInfoStateCtrl;
        [AutoBind("./NormalWeaponInfo/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NormalWeaponIconImage;
        [AutoBind("./NormalWeaponInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NormalWeaponNameText;
        [AutoBind("./NormalWeaponInfo/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NormalWeaponDescText;
        [AutoBind("./BoosterInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_boosterInfoStateCtrl;
        [AutoBind("./BoosterInfo/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BoosterIconImage;
        [AutoBind("./BoosterInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BoosterNameText;
        [AutoBind("./BoosterInfo/InfoGroup/Start/StartENText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BoosterStartENText;
        [AutoBind("./BoosterInfo/InfoGroup/Start/StartCDText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BoosterStartCDText;
        [AutoBind("./BoosterInfo/InfoGroup/StartEffect/Start/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BoosterDescText;
        [AutoBind("./RepairerInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_repairerInfoStateCtrl;
        [AutoBind("./RepairerInfo/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RepairerIconImage;
        [AutoBind("./RepairerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RepairerNameText;
        [AutoBind("./RepairerInfo/InfoGroup/Start/StartENText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RepairerStartENText;
        [AutoBind("./RepairerInfo/InfoGroup/Start/StartCDText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RepairerStartCDText;
        [AutoBind("./RepairerInfo/InfoGroup/StartEffect/Start/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RepairerDescText;
        [AutoBind("./TacticalEquipInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tacticalEquipInfoStateCtrl;
        [AutoBind("./TacticalEquipInfo/IconMask/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TacticalEquipIconImage;
        [AutoBind("./TacticalEquipInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TacticalEquipNameText;
        [AutoBind("./TacticalEquipInfo/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TacticalEquipDescText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateSuperWeaponEquipTipWndInfo;
        private static DelegateBridge __Hotfix_UpdateNormalWeaponTipWndInfo;
        private static DelegateBridge __Hotfix_UpdateShipRepairTipWndInfo;
        private static DelegateBridge __Hotfix_UpdateShipBoosterTipWndInfo;
        private static DelegateBridge __Hotfix_UpdateTacticalEquipTipWndInfo;
        private static DelegateBridge __Hotfix_ShowOrHideUniqueWeaponEquipTipWnd;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_ClearOnPause;

        [MethodImpl(0x8000)]
        public void ClearOnPause()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess Close()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideUniqueWeaponEquipTipWnd(UniqueWeaponEquipTipState state, ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict, out bool showTipWindow)
        {
        }

        [MethodImpl(0x8000)]
        private bool UpdateNormalWeaponTipWndInfo(ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private bool UpdateShipBoosterTipWndInfo(ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private bool UpdateShipRepairTipWndInfo(ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private bool UpdateSuperWeaponEquipTipWndInfo(ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private bool UpdateTacticalEquipTipWndInfo(ILBStaticShip staticShip, Dictionary<string, Object> dynamicResCacheDict)
        {
        }

        public enum UniqueWeaponEquipTipState
        {
            SuperWeapon,
            NormalWeapon,
            Repairer,
            Booster,
            TacticalEquip,
            All
        }
    }
}

