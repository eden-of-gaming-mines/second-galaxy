﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildMomentsUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLoadMore;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, string> EventOnLinkTextClick;
        private bool m_allInfosInList;
        private List<GuildMomentsInfo> m_infoList;
        private const string GuildMomentsListItemName = "GuildMomentsListItem";
        [AutoBind("./Scroll View/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRootGo;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildMomentsStateUICtrl;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./Empty", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_emptyGo;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        private static DelegateBridge __Hotfix_UpdateGuildMoments;
        private static DelegateBridge __Hotfix_CreateGuildMomentsPanelProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnGuildMomentsListItemFill;
        private static DelegateBridge __Hotfix_add_EventOnLoadMore;
        private static DelegateBridge __Hotfix_remove_EventOnLoadMore;
        private static DelegateBridge __Hotfix_add_EventOnLinkTextClick;
        private static DelegateBridge __Hotfix_remove_EventOnLinkTextClick;

        public event Action<int, string> EventOnLinkTextClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLoadMore
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildMomentsPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMomentsListItemFill(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMoments(List<GuildMomentsInfo> infoList, bool updateFromBegin, bool allInfosInList)
        {
        }

        [CompilerGenerated]
        private sealed class <OnPoolObjectCreated>c__AnonStorey0
        {
            internal GuildMomentsListItemUIController itemUICtrl;
            internal GuildMomentsUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(string str)
            {
            }
        }
    }
}

