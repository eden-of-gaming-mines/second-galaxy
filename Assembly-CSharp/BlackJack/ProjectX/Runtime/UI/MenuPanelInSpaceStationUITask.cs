﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class MenuPanelInSpaceStationUITask : MenuPanelUITask
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "MenuPanelInSpaceStationUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartByParent;
        private static DelegateBridge __Hotfix_StartShowOrHideMenuPanel;
        private static DelegateBridge __Hotfix_ShowMenuPanel;
        private static DelegateBridge __Hotfix_HideMenuPanel;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public MenuPanelInSpaceStationUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private static void HideMenuPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void ShowMenuPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static MenuPanelInSpaceStationUITask StartByParent(string mode, Action onTaskResLoadFinished)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartShowOrHideMenuPanel(Action<bool> onShowMenuPanelEnd, bool isShowMenuState, bool isImmediately)
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ShowMenuPanel>c__AnonStorey0
        {
            internal bool isImmediate;
            internal Action<bool> onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

