﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemTacticalEquipEffectUIController : UIControllerBase
    {
        private CommonUIStateController[] m_states;
        private List<string[]> m_stateNames;
        private int m_curShowNumber;
        private byte[] m_curStateArray;
        [AutoBind("./BottomGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bottomState;
        [AutoBind("./BottomGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_tacticalEquipIcon;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/CentreGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_centreState;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/CentreGroup/BlackBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_blackBarImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/CentreGroup/CDImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_cdImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/CentreGroup/ContraryImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_contraryImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/CentreGroup/ContraryBlackBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_contraryBlackBarImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_numberState;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_numberText;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/PercentageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_percentageText;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/3TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_3TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/4TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_4TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/5TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_5TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/6TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_6TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/7TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_7TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Number/8TierImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_8TierImage;
        [AutoBindWithParent("../../CanvasVolatile/PlayerSelfInfoPanel/TacticalEquipEffect/Flicker", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flickerState;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tacticalEquipInfoButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tacticalEquipFunctionStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetState;
        private static DelegateBridge __Hotfix_SetNumber_1;
        private static DelegateBridge __Hotfix_SetNumber_0;
        private static DelegateBridge __Hotfix_SetProgress;
        private static DelegateBridge __Hotfix_SetTacticalEqiupFunctionOpenState;
        private static DelegateBridge __Hotfix_SetTaticalEquipIcon;
        private static DelegateBridge __Hotfix_SetNumberPercent;
        private static DelegateBridge __Hotfix_SetNumberText;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNumber(float showValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNumber(float cur, float total)
        {
        }

        [MethodImpl(0x8000)]
        private void SetNumberPercent(float percent)
        {
        }

        [MethodImpl(0x8000)]
        private void SetNumberText(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProgress(float percent)
        {
        }

        [MethodImpl(0x8000)]
        public void SetState(byte[] states, bool resetState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTacticalEqiupFunctionOpenState(bool isOpen)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTaticalEquipIcon(Sprite sprite)
        {
        }
    }
}

