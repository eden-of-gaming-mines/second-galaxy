﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class MsgBoxUITask : UITaskBase
    {
        private const string MsgBoxModeNormalMode = "MsgBoxMode_NormalMode";
        private const string NormalMsg = "NormalMsg";
        private const string ExtraMsg = "ExtraMsg";
        private const string TitleMsg = "TitleMsg";
        private const string OnMsgBoxConfirm = "OnMsgBoxConfirm";
        private const string OnMsgBoxCancelClick = "onMsgBoxCancelClick";
        private const string IsNeedCancelButton = "IsNeedCancelButton";
        private MsgBoxUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "MsgBoxUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowMsgBox_1;
        private static DelegateBridge __Hotfix_ShowMsgBox_0;
        private static DelegateBridge __Hotfix_ShowMsgBox_2;
        private static DelegateBridge __Hotfix_ShowMsgBoxWithErrorCode;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateMsgBoxInfo;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public MsgBoxUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isClose = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMsgBox(string normalInfo, Action onMsgBoxConfirm = null, Action onMsgBoxCancelClick = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMsgBox(string normalInfo, bool isNeedCancelButton = false, Action onMsgBoxConfirm = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMsgBox(string normalInfo, string extraInfo, string titleInfo, Action onMsgBoxConfirm = null, Action onMsgBoxCancelClick = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMsgBoxWithErrorCode(int errCode, Action onMsgBoxConfirm = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMsgBoxInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnCancelButtonClick>c__AnonStorey1
        {
            internal Action onMsgBoxCancelClick;
            internal MsgBoxUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                this.onMsgBoxCancelClick();
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey0
        {
            internal Action onMsgBoxConfirm;
            internal MsgBoxUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey2
        {
            internal Action action;

            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
                if (this.action != null)
                {
                    this.action();
                }
            }
        }
    }
}

