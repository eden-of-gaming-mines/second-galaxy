﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SolarSytemTargetBuffDetailInfoUITask : UITaskBase
    {
        public const string SolarSystemTargetBuffDetailInfoUITaskModeNormal = "SolarSystemTargetBuffDetailInfoUITaskMode_Normal";
        private bool m_lastTickEquipEffectVirtualBufExistState;
        private SolarSystemShipBuffDetailInfoUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string UITaskModeParamTargetShip = "TargetShip";
        public const string UITaskModeParamPosition = "Position";
        private ClientSpaceShip m_targetShip;
        private List<BufDetailUIInfo> m_targetShipBufList;
        private Vector3 m_buffInfoUIPos;
        private int m_initWindowFlagIndex;
        private bool m_allowRefreshBuffChangingToUI;
        public const string TaskName = "SolarSytemTargetBuffDetailInfoUITask";
        [CompilerGenerated]
        private static Comparison<LBShipFightBuf> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_ClearBeforeUpdate;
        private static DelegateBridge __Hotfix_UpdateDataCache_Normal;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_RegisterEventsForBuffContainer;
        private static DelegateBridge __Hotfix_UnRegisterEventsForBuffContainer;
        private static DelegateBridge __Hotfix_OnSelfShipAttachBuf;
        private static DelegateBridge __Hotfix_OnSelfShipDetachBuf;
        private static DelegateBridge __Hotfix_OnSelfShipAttachFakeBuf;
        private static DelegateBridge __Hotfix_OnSelfShipDetachFakeBuf;
        private static DelegateBridge __Hotfix_GetCurrSelectTarget;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public SolarSytemTargetBuffDetailInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private ILBSpaceTarget GetCurrSelectTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfShipAttachBuf(LBBufBase buf, ILBBufSource bufSource)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfShipAttachFakeBuf(EquipEffectVirtualBufInfo bufInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfShipDetachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfShipDetachFakeBuf(EquipEffectVirtualBufInfo bufInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterEventsForBuffContainer(ClientSpaceShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isShow = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterEventsForBuffContainer(ClientSpaceShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_ClearBeforeUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Normal()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey0
        {
            internal Action action;

            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
                if (this.action != null)
                {
                    this.action();
                }
            }
        }

        public class BufDetailUIInfo
        {
            private bool m_isRealBuf;
            private LBShipFightBuf m_realBufInfo;
            private EquipEffectVirtualBufInfo m_virtualBufInfo;
            private static DelegateBridge _c__Hotfix_ctor_1;
            private static DelegateBridge _c__Hotfix_ctor_0;
            private static DelegateBridge __Hotfix_get_IsRealBuf;
            private static DelegateBridge __Hotfix_get_RealBufInfo;
            private static DelegateBridge __Hotfix_get_VirtualBufInfo;

            [MethodImpl(0x8000)]
            public BufDetailUIInfo(EquipEffectVirtualBufInfo bufInfo)
            {
            }

            [MethodImpl(0x8000)]
            public BufDetailUIInfo(LBShipFightBuf bufInfo)
            {
            }

            public bool IsRealBuf
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }

            public LBShipFightBuf RealBufInfo
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }

            public EquipEffectVirtualBufInfo VirtualBufInfo
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }
        }
    }
}

