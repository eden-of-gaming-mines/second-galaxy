﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StoreItemUIHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StoreItemComparer;
        private static DelegateBridge __Hotfix_StoreItemComparerByGuildFlagShip;
        private static DelegateBridge __Hotfix_GetItemTypeSortValue;
        private static DelegateBridge __Hotfix_CollectCommonItemIconCommonResPath;
        private static DelegateBridge __Hotfix_CollectCommonItemInfoCommonResPath;
        private static DelegateBridge __Hotfix_CollectSlotTypeIconResPath;
        private static DelegateBridge __Hotfix_CollectMineralTypeIconResPath;
        private static DelegateBridge __Hotfix_CollectShipTypeIconResPath;
        private static DelegateBridge __Hotfix_CollectShipTypeIconResPathInSpace;
        private static DelegateBridge __Hotfix_CollectCurrecyIconResPathInSpace;
        private static DelegateBridge __Hotfix_CollectCurrecyIconResPath;
        private static DelegateBridge __Hotfix_CollectGuildCurrecyIconResPath;
        private static DelegateBridge __Hotfix_CollectShipBGImageResPath;
        private static DelegateBridge __Hotfix_GetBlackMarketShopPayIconResFullPath;
        private static DelegateBridge __Hotfix_GetShipBGIconPathByFaction;
        private static DelegateBridge __Hotfix_GetItemObtainSourcePath;
        private static DelegateBridge __Hotfix_GetCrackBoxArtResFullPath;
        private static DelegateBridge __Hotfix_GetShipFullTypeName;
        private static DelegateBridge __Hotfix_GetNpcShipFullTypeName;
        private static DelegateBridge __Hotfix_GetShipTypeName;
        private static DelegateBridge __Hotfix_GetShipConfName;
        private static DelegateBridge __Hotfix_GetShipFullForceName_1;
        private static DelegateBridge __Hotfix_GetShipFullForceName_0;
        private static DelegateBridge __Hotfix_GetCurrentItemCount;
        private static DelegateBridge __Hotfix_GetCurrentItemCountInItemStore;
        private static DelegateBridge __Hotfix_CheckCurrItemTechDependence;
        private static DelegateBridge __Hotfix_GetItemTechDependenceList;
        private static DelegateBridge __Hotfix_CollectItemDepenceAndRelativeTechIcon;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public static bool CheckCurrItemTechDependence(StoreItemType itemType, object config)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectCommonItemIconCommonResPath(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectCommonItemInfoCommonResPath(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectCurrecyIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectCurrecyIconResPathInSpace(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectGuildCurrecyIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectItemDepenceAndRelativeTechIcon(StoreItemType itemType, object config, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectMineralTypeIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectShipBGImageResPath(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectShipTypeIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectShipTypeIconResPathInSpace(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectSlotTypeIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBlackMarketShopPayIconResFullPath(BlackMarketShopPayType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCrackBoxArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static long GetCurrentItemCount(StoreItemType itemType, int itemId, bool includeStoreItem = false)
        {
        }

        [MethodImpl(0x8000)]
        public static long GetCurrentItemCountInItemStore(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemObtainSourcePath(ConfigDataItemObtainSourceTypeInfo config)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ConfIdLevelInfo> GetItemTechDependenceList(StoreItemType itemType, object config)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetItemTypeSortValue(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcShipFullTypeName(int npcCaptainShipConfId, int npcMonsterLevel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipBGIconPathByFaction(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipConfName(int shipConfId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipFullForceName(ConfigDataSpaceShipInfo shipConfig)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipFullForceName(ILBStaticShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipFullTypeName(int shipConfId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipTypeName(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public static int StoreItemComparer(ILBStoreItemClient a, ILBStoreItemClient b)
        {
        }

        [MethodImpl(0x8000)]
        public static int StoreItemComparerByGuildFlagShip(ILBStoreItemClient a, ILBStoreItemClient b)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

