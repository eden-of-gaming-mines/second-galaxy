﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private CharacterBasicInfoPanelUIController <CharacterBasicInfoCtrl>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private CharacterOverviewInfoUIController <CharacterOverviewInfoCtrl>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private CharacterWalletUIController <CharacterWalletCtrl>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CharacterInfoUITask.TabType> EventOnTabButtonClick;
        private const string PanelStateOpen = "Show";
        private const string PanelStateClose = "Close";
        private const string TabStateNormal = "Normal";
        private const string TabStateChoose = "Choose";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./TabGroup/CharacterOverviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_overviewButton;
        [AutoBind("./TabGroup/CharacterOverviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_overviewTabStateCtrl;
        [AutoBind("./TabGroup/WalletButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_walletButton;
        [AutoBind("./TabGroup/WalletButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_walletTabStateCtrl;
        [AutoBind("./CharacterBasicInfoPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_basicInfoTrans;
        [AutoBind("./CharacterOverviewInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_overviewInfoTrans;
        [AutoBind("./CharacterWalletUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_characterWalletRoot;
        [AutoBind("./CrimeLVInfoUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_crimeInfoPanelStateCtrl;
        [AutoBind("./CrimeLVInfoUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_crimeInfoPanelBgBAckGroundButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreateCrimeDescPanelProcess;
        private static DelegateBridge __Hotfix_SetTabState;
        private static DelegateBridge __Hotfix_SwitchToOverviewInfo;
        private static DelegateBridge __Hotfix_ShowSetChipSucceedEffect;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCharacterOverviewButtonoClick;
        private static DelegateBridge __Hotfix_OnStatisticButtonClick;
        private static DelegateBridge __Hotfix_OnWalletButtonClick;
        private static DelegateBridge __Hotfix_OnTabButtonClick;
        private static DelegateBridge __Hotfix_get_CharacterBasicInfoCtrl;
        private static DelegateBridge __Hotfix_set_CharacterBasicInfoCtrl;
        private static DelegateBridge __Hotfix_get_CharacterOverviewInfoCtrl;
        private static DelegateBridge __Hotfix_set_CharacterOverviewInfoCtrl;
        private static DelegateBridge __Hotfix_get_CharacterWalletCtrl;
        private static DelegateBridge __Hotfix_set_CharacterWalletCtrl;
        private static DelegateBridge __Hotfix_add_EventOnTabButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTabButtonClick;

        public event Action<CharacterInfoUITask.TabType> EventOnTabButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateCrimeDescPanelProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterOverviewButtonoClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStatisticButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabButtonClick(CharacterInfoUITask.TabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWalletButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTabState(CharacterInfoUITask.TabType selectedTabTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSetChipSucceedEffect(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchToOverviewInfo(bool isSelfInfoMode)
        {
        }

        public CharacterBasicInfoPanelUIController CharacterBasicInfoCtrl
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public CharacterOverviewInfoUIController CharacterOverviewInfoCtrl
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public CharacterWalletUIController CharacterWalletCtrl
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

