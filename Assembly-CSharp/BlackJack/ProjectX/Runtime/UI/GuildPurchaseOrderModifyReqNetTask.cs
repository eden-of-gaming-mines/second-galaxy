﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildPurchaseOrderModifyReqNetTask : NetWorkTransactionTask
    {
        public bool m_updated;
        private readonly ulong m_orderId;
        private readonly int m_itemCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderModifyAck;

        [MethodImpl(0x8000)]
        private GuildPurchaseOrderModifyReqNetTask(ulong orderId, int itemCount)
        {
        }

        [MethodImpl(0x8000)]
        private void GuildPurchaseOrderModifyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(ulong orderId, int itemCount, Action<bool> onStop)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal Action<bool> onStop;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

