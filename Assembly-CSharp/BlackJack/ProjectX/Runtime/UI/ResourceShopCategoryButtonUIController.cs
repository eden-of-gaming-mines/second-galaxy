﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class ResourceShopCategoryButtonUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ResourceShopItemCategory> EventOnButtonClick;
        private ResourceShopItemCategory m_category;
        private bool m_isEnable = true;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CategoryButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CategoryStateCtrl;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ButtonText;
        private static DelegateBridge __Hotfix_SetButtonCategory;
        private static DelegateBridge __Hotfix_SetSelect;
        private static DelegateBridge __Hotfix_SetEnable;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnButtonClick;

        public event Action<ResourceShopItemCategory> EventOnButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonCategory(ResourceShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelect(bool select)
        {
        }
    }
}

