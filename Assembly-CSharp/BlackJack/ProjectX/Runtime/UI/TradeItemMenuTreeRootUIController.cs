﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TradeItemMenuTreeRootUIController : CommonMenuRootUIController
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnTradeItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, bool> EventOnTypeItemClick;
        private bool m_isTraversalRecurseFinish;
        private bool m_responseToUIEvent;
        private TradeItemTypeMenuItemUIController m_curSelectedTypeCtrl;
        private TradeItemMenuItemData m_curSelectedTradeItemData;
        protected Dictionary<string, UnityEngine.Object> m_assetDict;
        protected List<TradeItemTypeMenuItemData> m_tradeItemTypeMenuItemDataList;
        protected Dictionary<int, List<TradeItemMenuItemData>> m_tradeItemMenuItemDataDic;
        [AutoBind("./UnusedMenuItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_unusedMenuItemRoot;
        public ToggleGroup m_menuLeafItemToggleGroup;
        protected const int MenuRootLevel = 0;
        protected const int MenuTradeItemTypeLevel = 1;
        protected const int MenuTradeItemlLevel = 2;
        [CompilerGenerated]
        private static Func<CommonMenuItemUIController, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_ClearMenuTree;
        private static DelegateBridge __Hotfix_UpdateMenuTree;
        private static DelegateBridge __Hotfix_UpdateMenuTreeItemState_0;
        private static DelegateBridge __Hotfix_UpdateMenuTreeItemState_1;
        private static DelegateBridge __Hotfix_ExpandAll;
        private static DelegateBridge __Hotfix_SetInitState;
        private static DelegateBridge __Hotfix_GetTypeCtrl;
        private static DelegateBridge __Hotfix_GetItemCtrl;
        private static DelegateBridge __Hotfix_ClearAllState;
        private static DelegateBridge __Hotfix_SelectItemInternal;
        private static DelegateBridge __Hotfix_SetInitializedSelectedState;
        private static DelegateBridge __Hotfix_EnableUIResponse;
        private static DelegateBridge __Hotfix_TraversalRecurse;
        private static DelegateBridge __Hotfix_GenerateMenuTree;
        private static DelegateBridge __Hotfix_OnMenuItemClick;
        private static DelegateBridge __Hotfix_SelectSpecialItem;
        private static DelegateBridge __Hotfix_OnTradeItemTypeClick;
        private static DelegateBridge __Hotfix_Expand_1;
        private static DelegateBridge __Hotfix_Expand_0;
        private static DelegateBridge __Hotfix_GetTypeCtrlFromID;
        private static DelegateBridge __Hotfix_SetTypeItemSelectState;
        private static DelegateBridge __Hotfix_OnTradeItemClick;
        private static DelegateBridge __Hotfix_SetPreviousTypeExpandState;
        private static DelegateBridge __Hotfix_SetTypeMenuStateInternal;
        private static DelegateBridge __Hotfix_GetTypeItemCtrl;
        private static DelegateBridge __Hotfix_add_EventOnTradeItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnTradeItemClick;
        private static DelegateBridge __Hotfix_add_EventOnTypeItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnTypeItemClick;

        public event Action<int> EventOnTradeItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, bool> EventOnTypeItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ClearAllState()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearMenuTree()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableUIResponse(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void Expand(TradeItemTypeMenuItemUIController typeMenuItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Expand(int typeId)
        {
        }

        [MethodImpl(0x8000)]
        public void ExpandAll()
        {
        }

        [MethodImpl(0x8000)]
        private void GenerateMenuTree()
        {
        }

        [MethodImpl(0x8000)]
        private CommonMenuItemUIController GetItemCtrl(CommonMenuItemUIController typeCtrl, int itemID)
        {
        }

        [MethodImpl(0x8000)]
        private CommonMenuItemUIController GetTypeCtrl(int typeID)
        {
        }

        [MethodImpl(0x8000)]
        private TradeItemTypeMenuItemUIController GetTypeCtrlFromID(int id)
        {
        }

        [MethodImpl(0x8000)]
        private TradeItemTypeMenuItemUIController GetTypeItemCtrl(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(ToggleGroup menuLeafItemToggleGroup)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeItemClick(TradeItemMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeItemTypeClick(TradeItemTypeMenuItemUIController typeMenuItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SelectItemInternal(TradeItemMenuItemUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectSpecialItem(TradeItemTypeMenuItemUIController typeMenuItemCtrl, TradeItemMenuItemData specialItemData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInitializedSelectedState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetInitState(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPreviousTypeExpandState(TradeItemTypeMenuItemUIController nextTypeCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTypeItemSelectState(TradeItemTypeMenuItemUIController nextTypeItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTypeMenuStateInternal(TradeItemTypeMenuItemUIController curTypeCtrl, TradeItemTypeMenuItemUIController nextTypeCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void TraversalRecurse(CommonMenuItemUIController menuItem, Action<CommonMenuItemUIController> processFunc)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuTree(List<TradeItemTypeMenuItemData> tradeItemTypeMenuItemDataList, Dictionary<int, List<TradeItemMenuItemData>> tradeItemMenuItemDataDic, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuTreeItemState(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuTreeItemState(TradeItemTypeMenuItemData itemData)
        {
        }
    }
}

