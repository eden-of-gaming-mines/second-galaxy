﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TechUpgradeSpeedUpWithRealMoneyUIController : UIControllerBase
    {
        [AutoBind("./TextGroup/HintText02/CostMoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyCostValue;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdatePanelInfo;
        private static DelegateBridge __Hotfix_GetMainUIProcess;

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePanelInfo(int cost)
        {
        }
    }
}

