﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CGPlayerUIController : UIControllerBase
    {
        private Action m_endAction;
        protected IVideoPlayer m_cgPlayer;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_margin;
        [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backGroundButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_PlayCG;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnCGEnd;
        private static DelegateBridge __Hotfix_StopPlayCG;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCGEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCG(string cgResPath, Action endAction, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void StopPlayCG()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

