﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonPopupWndManagerUITask : UITaskBase
    {
        public static string CommonPopupWndManagerUITask_Default = "CommonPopupWndManagerUITask_Default";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CommonPopupWndManagerUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_Hide;
        private static DelegateBridge __Hotfix_StartState_1;
        private static DelegateBridge __Hotfix_StartState_0;
        private static DelegateBridge __Hotfix_ExecForCommonPopupWndShownFinished;
        private static DelegateBridge __Hotfix_ExecForCommonPopupWndHideFinished;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public CommonPopupWndManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator ExecForCommonPopupWndHideFinished(DateTime hideFinishedTime, Action action)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator ExecForCommonPopupWndShownFinished(DateTime shownFinishedTime, Action action)
        {
        }

        [MethodImpl(0x8000)]
        public bool Hide(CommonPopupWndUIControllerBase wndCtrl, Action onHideStart = null, Action onHideEnd = null, bool immediateHide = false, UITaskBase mainTask = null, bool isForceHide = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public bool Show(CommonPopupWndUIControllerBase wndCtrl, Action onShownStart = null, Action onShownEnd = null, bool immediateShow = false, UITaskBase mainTask = null, bool isForceShow = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool StartState(CommonUIStateController stateCtrl, string stateName, Action onStateStart = null, Action onStateEnd = null, bool immediateShow = false, bool isForceShow = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool StartState(CommonPopupWndUIControllerBase wndCtrl, string stateName, Action onStateStart = null, Action onStateEnd = null, bool immediateShow = false, bool isForceShow = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public static CommonPopupWndManagerUITask Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ExecForCommonPopupWndHideFinished>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal DateTime hideFinishedTime;
            internal Action action;
            internal CommonPopupWndManagerUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.$this.PlayerCtx.GetCurrServerTime() >= this.hideFinishedTime)
                        {
                            if (this.action != null)
                            {
                                this.action();
                            }
                            this.$PC = -1;
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <ExecForCommonPopupWndShownFinished>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal DateTime shownFinishedTime;
            internal Action action;
            internal CommonPopupWndManagerUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.$this.PlayerCtx.GetCurrServerTime() >= this.shownFinishedTime)
                        {
                            if (this.action != null)
                            {
                                this.action();
                            }
                            this.$PC = -1;
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <Hide>c__AnonStorey3
        {
            internal CommonPopupWndUIControllerBase wndCtrl;
            internal CommonPopupWndManagerUITask $this;

            internal void <>m__0()
            {
                DateTime hideFinishedTime = this.$this.PlayerCtx.GetCurrServerTime().AddSeconds((double) this.wndCtrl.GetWndTweenDurationMax("Close"));
                this.$this.m_corutineHelper.StartCorutine(this.$this.ExecForCommonPopupWndHideFinished(hideFinishedTime, new Action(this.wndCtrl.OnHideFinished)));
            }
        }

        [CompilerGenerated]
        private sealed class <Show>c__AnonStorey2
        {
            internal CommonPopupWndUIControllerBase wndCtrl;
            internal CommonPopupWndManagerUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartState>c__AnonStorey4
        {
            internal CommonPopupWndUIControllerBase wndCtrl;
            internal string stateName;
            internal CommonPopupWndManagerUITask $this;

            internal void <>m__0()
            {
                DateTime shownFinishedTime = this.$this.PlayerCtx.GetCurrServerTime().AddSeconds((double) this.wndCtrl.GetWndTweenDurationMax(this.stateName));
                this.$this.m_corutineHelper.StartCorutine(this.$this.ExecForCommonPopupWndShownFinished(shownFinishedTime, new Action(this.wndCtrl.OnStateFinished)));
            }
        }

        [CompilerGenerated]
        private sealed class <StartState>c__AnonStorey5
        {
            internal CommonUIStateController stateCtrl;
            internal string stateName;
            internal Action onStateEnd;
            internal CommonPopupWndManagerUITask $this;

            internal void <>m__0()
            {
                DateTime shownFinishedTime = this.$this.PlayerCtx.GetCurrServerTime().AddSeconds((double) this.stateCtrl.GetMaxTweenDurationByUIState(this.stateName));
                this.$this.m_corutineHelper.StartCorutine(this.$this.ExecForCommonPopupWndShownFinished(shownFinishedTime, this.onStateEnd));
            }
        }
    }
}

