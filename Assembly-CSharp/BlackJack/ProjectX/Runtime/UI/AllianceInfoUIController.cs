﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceInfoUIController : UIControllerBase
    {
        private const string Show = "Show";
        private const string Close = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ManageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_manageBtn;
        [AutoBind("./ManageButton/Image", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_manageBtnMask;
        [AutoBind("./AllyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_memberBtn;
        [AutoBind("./AllyButton/Image", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_memberBtnMask;
        [AutoBind("./WarButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_battleListBtn;
        [AutoBind("./WarButton/Image", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_battleListBtnMask;
        [AutoBind("./FlagGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_iconController;
        [AutoBind("./FlagGroup/FlagNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceNameText;
        [AutoBind("./FlagGroup/GuildText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_leaderGuildText;
        [AutoBind("./FlagGroup/AllyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allyCount;
        [AutoBind("./FlagGroup/InfluenceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_influenceCount;
        [AutoBind("./AnnouncementGroup/AnnouncementText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_announcement;
        [AutoBind("./FlagGroup/LeaderNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_leaderName;
        private const string GuildNameFormat = "[{0}]{1}";
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_SetData;
        private static DelegateBridge __Hotfix_IsMember;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsMember(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetData(AllianceInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [CompilerGenerated]
        private sealed class <SetData>c__AnonStorey0
        {
            internal AllianceInfo info;

            [MethodImpl(0x8000)]
            internal bool <>m__0(AllianceMemberInfo member)
            {
            }
        }
    }
}

