﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildRegionController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_dropDown;
        private bool m_initialized;
        private readonly Dictionary<GuildAllianceLanguageType, int> m_languageTypeToIndex;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataGuildAllianceLanguageTypeInfo>, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateResDict;
        private static DelegateBridge __Hotfix_SetOptions;
        private static DelegateBridge __Hotfix_GetIndexOfLanguageType;
        private static DelegateBridge __Hotfix_GetLanguageTypeOfIndex;

        [MethodImpl(0x8000)]
        public int GetIndexOfLanguageType(GuildAllianceLanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType GetLanguageTypeOfIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetOptions(Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateResDict(Dictionary<string, Object> resDict)
        {
        }
    }
}

