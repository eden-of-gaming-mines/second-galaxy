﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum EnterSpaceStationType
    {
        FromLogin,
        FromSpace,
        FromSpaceDeath,
        FromStation
    }
}

