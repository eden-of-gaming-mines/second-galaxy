﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildBattleDeclareCostReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_solarSystemId;
        private readonly ulong m_buildingInstanceId;
        private readonly int m_battleType;
        public int m_result;
        public long m_battleCost;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildBattleDeclareCostAck;

        [MethodImpl(0x8000)]
        public GuildBattleDeclareCostReqNetTask(int solarSytemId, ulong buildingInstanceId, GuildBattleType battleType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBattleDeclareCostAck(int result, long cost)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

