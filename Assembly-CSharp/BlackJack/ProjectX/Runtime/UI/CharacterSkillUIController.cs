﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./SkillPoint/FreeSkillPointCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_freeSkillPointCountText;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./SkillTypeListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillTypeListRoot;
        [AutoBind("./SkillTreeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillTreeRoot;
        [AutoBind("./ToggleScrollView/Viewport/SkillCategoryToggleGroup/CategoryPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillCategoryToggleTempObj;
        [AutoBind("./ToggleScrollView/Viewport/SkillCategoryToggleGroup/CategoryPrefab/SkillTypeToggleRoot/SkillItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillTypeToggle;
        [AutoBind("./UnusedSkillTreeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_unusedSkillTreeRoot;
        [AutoBind("./SkillOverViewPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillOverViewRoot;
        private List<CharacterSkillCategoryToggleUIController> m_skillCategoryToggleList;
        private CharacterSkillTreeUIController m_currSkillTree;
        private CharacterSkillOverViewUIController m_SkillOverViewCtrl;
        private List<CharacterSkillTreeUIController> m_unusedSkillTreeList;
        private SystemDescriptionController m_descriptionCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateAllToggles;
        private static DelegateBridge __Hotfix_UpdateSkillTree;
        private static DelegateBridge __Hotfix_SetFreeSkillPointCount;
        private static DelegateBridge __Hotfix_GetCategoryToggleCtrl;
        private static DelegateBridge __Hotfix_SetSelectedToggle;
        private static DelegateBridge __Hotfix_SetToggleRedPoint;
        private static DelegateBridge __Hotfix_ShowOrHideSystemDescriptionButton;
        private static DelegateBridge __Hotfix_ClearToggleSelectedState;
        private static DelegateBridge __Hotfix_UpdateCategoryToggleSelectState;
        private static DelegateBridge __Hotfix_ClearSkillTypeToggleSelectState;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_get_SkillCategoryToggleList;
        private static DelegateBridge __Hotfix_get_SkillTreeCtrl;
        private static DelegateBridge __Hotfix_get_SkillOverViewCtrl;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearSkillTypeToggleSelectState()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearToggleSelectedState()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllToggles(Dictionary<int, List<ConfigDataPassiveSkillTypeInfo>> m_skillCategoryToSkillTypeListInfoDict, Action<CharacterSkillCategoryToggleUIController> onCategoryToggleSelected, Action<int> onSkillTypeToggleSeleced)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillCategoryToggleUIController GetCategoryToggleCtrl(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFreeSkillPointCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedToggle(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleRedPoint(Dictionary<int, List<ConfigDataPassiveSkillTypeInfo>> category2TypeDic, Dictionary<int, List<ConfigDataPassiveSkillInfo>> skillType2SkillDic)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSystemDescriptionButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCategoryToggleSelectState(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSkillTree(UnityEngine.Object skillTreeObj, Sprite skillTypeIcon, SkillTreeUIInfo skillTreeUIInfo, Action<ConfigDataPassiveSkillInfo> onSkillTreeNodeClick, Dictionary<int, List<ConfigDataPassiveSkillInfo>> skillType2SkillDic)
        {
        }

        public List<CharacterSkillCategoryToggleUIController> SkillCategoryToggleList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CharacterSkillTreeUIController SkillTreeCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CharacterSkillOverViewUIController SkillOverViewCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateSkillTree>c__AnonStorey0
        {
            internal CharacterSkillUIController.SkillTreeUIInfo skillTreeUIInfo;

            internal bool <>m__0(CharacterSkillTreeUIController elem) => 
                (elem.SkillTreeTypeInfo.ID == this.skillTreeUIInfo.m_skillTypeInfo.ID);
        }

        public class SkillCategoryToggleUIInfo
        {
            public PropertyCategory m_skillCategory;
            public string m_toggleName;
            public int m_currSkillLevelCount;
            public int m_maxSkillLevelCount;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public class SkillTreeUIInfo
        {
            public ConfigDataPassiveSkillTypeInfo m_skillTypeInfo;
            public string m_skillTreeName;
            public int m_currSkillLevelCount;
            public int m_maxSkillLevelCount;
            public List<CharacterSkillTreeUIController.SkillLearningInfoForSkillTree> m_skillTreeLearningInfoList;
            private static DelegateBridge _c__Hotfix_ctor;

            public SkillTreeUIInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class SkillTypeItemUIInfo
        {
            public int m_skillType;
            public string m_skillTypeItemName;
            public Sprite m_skillTypeIcon;
            public int m_currSkillLevelCount;
            public int m_maxSkillLevelCount;
            private static DelegateBridge _c__Hotfix_ctor;

            public SkillTypeItemUIInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

