﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class GuildMomentsUITask : GuildUITaskBase
    {
        private bool m_allInfosInList;
        private List<GuildMomentsInfo> m_guildMomentsInfoList;
        public const string ParamKeyRefreshList = "ParamKeyRefreshList";
        public const string ParamKeyResumeStationBgWhenResume = "ParamKeyResumeStationBgWhenResume";
        private const int ThresholdNumberOnGetOldMessageEachTime = 10;
        private GuildMomentsUIController m_guildMomentsUICtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_OnLinkTextClick;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildMomentsUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLinkTextClick(int momentIndex, string detailIndexStr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadMoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal GuildMomentsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildMomentsUITask.GuildMomentsLatestListReqNetWorkTask task2 = task as GuildMomentsUITask.GuildMomentsLatestListReqNetWorkTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    this.onPrepareEnd(false);
                }
                else if (task2.Result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    this.onPrepareEnd(false);
                }
                else
                {
                    this.$this.m_allInfosInList = task2.IsLoadComeplete;
                    this.onPrepareEnd(true);
                }
            }
        }

        public class GuildMomentsLatestListReqNetWorkTask : NetWorkTransactionTask
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <Result>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsLoadComeplete>k__BackingField;
            private readonly uint m_seqId;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_IsLoadComeplete;
            private static DelegateBridge __Hotfix_set_IsLoadComeplete;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_OnGuildMomentsLatestListAck;

            public GuildMomentsLatestListReqNetWorkTask(uint seqId) : base(10f, true, false, false)
            {
                this.m_seqId = seqId;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp197(this, seqId);
                }
            }

            private void OnGuildMomentsLatestListAck(int result, bool isLoadComeplete)
            {
                DelegateBridge bridge = __Hotfix_OnGuildMomentsLatestListAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp249(this, result, isLoadComeplete);
                }
                else
                {
                    this.Result = result;
                    this.IsLoadComeplete = isLoadComeplete;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMomentsLatestListAck += new Action<int, bool>(this.OnGuildMomentsLatestListAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildMomentsLatestListReq(this.m_seqId));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMomentsLatestListAck -= new Action<int, bool>(this.OnGuildMomentsLatestListAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public bool IsLoadComeplete
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsLoadComeplete;
                    return ((bridge == null) ? this.<IsLoadComeplete>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_IsLoadComeplete;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsLoadComeplete>k__BackingField = value;
                    }
                }
            }
        }

        public class GuildMomentsOldListReqNetWorkTask : NetWorkTransactionTask
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <Result>k__BackingField;
            private readonly uint m_seqId;
            private int m_count;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_Count;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildMomentsOldListAck;

            public GuildMomentsOldListReqNetWorkTask(uint seqId, int count) : base(10f, true, false, false)
            {
                this.m_seqId = seqId;
                this.m_count = count;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1499(this, seqId, count);
                }
            }

            private void OnGuildMomentsOldListAck(int result, int count)
            {
                DelegateBridge bridge = __Hotfix_OnGuildMomentsOldListAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp221(this, result, count);
                }
                else
                {
                    this.Result = result;
                    this.m_count = count;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMomentsOldListAck += new Action<int, int>(this.OnGuildMomentsOldListAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGGuildMomentsOldListReq(this.m_seqId, this.m_count));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildMomentsOldListAck -= new Action<int, int>(this.OnGuildMomentsOldListAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public int Count
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Count;
                    return ((bridge == null) ? this.m_count : bridge.__Gen_Delegate_Imp70(this));
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpDateDataCache,
            UpdateFromBegin
        }
    }
}

