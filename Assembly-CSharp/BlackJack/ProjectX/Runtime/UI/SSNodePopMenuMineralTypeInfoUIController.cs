﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SSNodePopMenuMineralTypeInfoUIController : UIControllerBase
    {
        [AutoBind("./DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_mineralTypeRootTrans;
        [AutoBind("./DetailGroup/MineralInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_mineralTypeInfoItemObj;
        private List<GameObject> m_mineralTypeInfoItemObjList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetMineralTypeInfoToUI;
        private static DelegateBridge __Hotfix_CreateMineralTypeInfoItemObjToPool;

        [MethodImpl(0x8000)]
        private void CreateMineralTypeInfoItemObjToPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMineralTypeInfoToUI(List<int> mineralTypeList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

