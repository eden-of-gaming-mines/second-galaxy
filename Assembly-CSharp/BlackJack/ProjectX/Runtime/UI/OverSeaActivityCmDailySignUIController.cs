﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class OverSeaActivityCmDailySignUIController : UIControllerBase
    {
        protected const string CommonItemName = "CommonItemUIPrefab";
        public CommonItemIconUIController m_MainRewardUIItem;
        public CommonItemIconUIController[] m_dayRewardItemArray;
        private CommonUIStateController[] m_dayRewardItemUIStateArray;
        private Text[] m_dayRewardItemStageTextArray;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FakeLBStoreItem, CommonItemIconUIController> EventOnDropItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./ContentBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StartButton;
        [AutoBind("./AwardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AwardGroupUIState;
        [AutoBind("./AwardGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./AwardGroup/ItemDummy/CommonItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CommonItemGroupButton;
        [AutoBind("./AwardGroup/ItemDummy/CommonItemGroup/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SubRankImage;
        [AutoBind("./AwardGroup/ItemDummy/CommonItemGroup/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CountText;
        [AutoBind("./AnimationBgGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AnimationBgGroupUIState;
        [AutoBind("./AccumulativeGroup/ItemDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy01;
        [AutoBind("./AccumulativeGroup/IconStateGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IconStateGroup01UIState;
        [AutoBind("./AccumulativeGroup/StageGroup01/StageImage01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group01StageImage01;
        [AutoBind("./AccumulativeGroup/StageGroup01/StageImage02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group01StageImage02;
        [AutoBind("./AccumulativeGroup/StageGroup01/StageImage03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group01StageImage03;
        [AutoBind("./AccumulativeGroup/StageGroup01/StageImage04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group01StageImage04;
        [AutoBind("./AccumulativeGroup/ItemDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy02;
        [AutoBind("./AccumulativeGroup/IconStateGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IconStateGroup02UIState;
        [AutoBind("./AccumulativeGroup/StageGroup02/StageImage01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group02StageImage01;
        [AutoBind("./AccumulativeGroup/StageGroup02/StageImage02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group02StageImage02;
        [AutoBind("./AccumulativeGroup/StageGroup02/StageImage03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group02StageImage03;
        [AutoBind("./AccumulativeGroup/StageGroup02/StageImage04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group02StageImage04;
        [AutoBind("./AccumulativeGroup/ItemDummy03", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy03;
        [AutoBind("./AccumulativeGroup/IconStateGroup03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IconStateGroup03UIState;
        [AutoBind("./AccumulativeGroup/StageGroup03/StageImage01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group03StageImage01;
        [AutoBind("./AccumulativeGroup/StageGroup03/StageImage02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group03StageImage02;
        [AutoBind("./AccumulativeGroup/StageGroup03/StageImage03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group03StageImage03;
        [AutoBind("./AccumulativeGroup/StageGroup03/StageImage04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Group03StageImage04;
        [AutoBind("./AccumulativeGroup/AddTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AddTimeText;
        [AutoBind("./AccumulativeGroup/StageText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text StageText01;
        [AutoBind("./AccumulativeGroup/StageText02", AutoBindAttribute.InitState.NotInit, false)]
        public Text StageText02;
        [AutoBind("./AccumulativeGroup/StageText03", AutoBindAttribute.InitState.NotInit, false)]
        public Text StageText03;
        [AutoBind("./IdentityGroup/HeadImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image HeadImage;
        [AutoBind("./IdentityGroup/StateImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image StateImage;
        [AutoBind("./IdentityGroup/AttestationText/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image StateIAttestationImage;
        [AutoBind("./IdentityGroup/MessageGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./IdentityGroup/MessageGroup/ClassText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ClassText;
        [AutoBind("./IdentityGroup/MessageGroup/GenderText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GenderText;
        [AutoBind("./IdentityGroup/MessageGroup/BackgroundText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BackgroundText;
        [AutoBind("./IdentityGroup/MessageGroup/StationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StationText;
        [AutoBind("./IdentityGroup/MessageGroup/GuildText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildText;
        [AutoBind("./IdentityGroup/IDTitleText/IDText", AutoBindAttribute.InitState.NotInit, false)]
        public Text IDText;
        [AutoBind("./IdentityGroup/PropertyGroup/WeaponControlmage/WeaponControNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponControNumberText;
        [AutoBind("./IdentityGroup/PropertyGroup/DefenseImage/DefenseNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DefenseNumberText;
        [AutoBind("./IdentityGroup/PropertyGroup/ElectronicImage/ElectronicNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectronicNumberText;
        [AutoBind("./IdentityGroup/PropertyGroup/DrivingImage/DrivingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DrivingNumberText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateStateArray;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_CreateTemplateItemEx;
        private static DelegateBridge __Hotfix_UpdateView_MainReward;
        private static DelegateBridge __Hotfix_UpdateView_RewardCoolDown;
        private static DelegateBridge __Hotfix_UpdateView_Item;
        private static DelegateBridge __Hotfix_UpdateView_CharacterPanel;
        private static DelegateBridge __Hotfix_GetSignPanelUIProcess;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnDropItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDropItemClick;

        public event Action<FakeLBStoreItem, CommonItemIconUIController> EventOnDropItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateStateArray()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject CreateTemplateItemEx(string name, Transform parent, bool active = true)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetSignPanelUIProcess(bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_CharacterPanel(Dictionary<string, UnityEngine.Object> restList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_Item(bool isGetReward, int serverCount, int serverSingedId, int rowIndex, int[] dropIdArray, Dictionary<string, UnityEngine.Object> restList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_MainReward(bool isGetRewardCurDay, DateTime getTime, Dictionary<string, UnityEngine.Object> restList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_RewardCoolDown(DateTime dateTime)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

