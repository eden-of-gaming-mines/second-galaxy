﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationContributItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnDonateBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonIconClick;
        public ScrollItemBaseUIController m_scrollItem;
        public CommonItemIconUIController m_itemIconCtrl;
        public LostItem m_lostItemInfo;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CommonItemDummy;
        [AutoBind("./DonateNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DonateNameText;
        [AutoBind("./OwnNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OwnNumberText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./DonateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DonateButton;
        [AutoBind("./DonateButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DonateButtonState;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnDonateBtnClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnDonateBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnDonateBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        public event Action<UIControllerBase> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnDonateBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(GameObject itemIconObj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(LostItem comp, bool isSelf, Dictionary<string, UnityEngine.Object> dict)
        {
        }
    }
}

