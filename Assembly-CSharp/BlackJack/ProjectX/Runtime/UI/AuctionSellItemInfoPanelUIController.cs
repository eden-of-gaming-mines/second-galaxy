﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class AuctionSellItemInfoPanelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnTipButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PointerEventData, Action<PointerEventData>> EventOnBackGroundClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PointerEventData, Action<PointerEventData>> EventOnTipBackGroundClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnItemDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnItemShareButtonClick;
        private AuctionStoreTipUIController m_tipUICtrl;
        private CommonItemIconUIController m_itemIconCtrl;
        private ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        private AuctionItemDetailInfo m_itemDetailInfo;
        private bool m_panelIsShow;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backGroundButton;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController m_passAllCtrl;
        [AutoBind("./SellItemInfoPanel/TradeTax/TodayNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeName;
        [AutoBind("./SellItemInfoPanel/ItemInfo/Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_qualityStateCtrl;
        [AutoBind("./SellItemInfoPanel/ItemInfo/Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_teckRankLevel;
        [AutoBind("./SellItemInfoPanel/ItemInfo/Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankImage;
        [AutoBind("./SellItemInfoPanel/ItemInfo/Tech", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_teckRankObj;
        [AutoBind("./SellItemInfoPanel/ItemInfo/Quality", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_subRankObj;
        [AutoBind("./SellItemInfoPanel/ItemInfo/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemDummy;
        [AutoBind("./SellItemInfoPanel/ItemCount/SellText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sellCount;
        [AutoBind("./SellItemInfoPanel/ItemPrice/UnitlNameText/UnitMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_priceUnitMoreButton;
        [AutoBind("./SellItemInfoPanel/ItemPrice/UnitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_priceUnitText;
        [AutoBind("./SellItemInfoPanel/ExpireTime/UpperLimitNameText/MoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_expireMoreButton;
        [AutoBind("./SellItemInfoPanel/ExpireTime/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_expireTimeText;
        [AutoBind("./SellItemInfoPanel/ExpireTime/UpperLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_expireUpperLimit;
        [AutoBind("./SellItemInfoPanel/AuctionFee/CustodyMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_custodyMoreButton;
        [AutoBind("./SellItemInfoPanel/AuctionFee/CustodyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_custodyFeeText;
        [AutoBind("./SellItemInfoPanel/TradeTax/TodayNameText/TodayMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tradeTaxMoreButton;
        [AutoBind("./SellItemInfoPanel/TradeTax/TodayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeTaxText;
        [AutoBind("./SellItemInfoPanel/ItemInfo/TradeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_auctionName;
        [AutoBind("./SellItemInfoPanel/MarketPrice/BazaarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_marktePriceBazaarText;
        [AutoBind("./SellItemInfoPanel/MarketPrice", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_marktePriceStateCtrl;
        [AutoBind("./SellItemInfoPanel/ButtonGroup/Unshelve", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_unshelveButton;
        [AutoBind("./SellItemInfoPanel/ButtonGroup/ItemDetail", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemDetailButton;
        [AutoBind("./SellItemInfoPanel/ButtonGroup/Reshelve", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_reshelveButton;
        [AutoBind("./SellItemInfoPanel/ButtonGroup/CallBack", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_callBackButton;
        [AutoBind("./SellItemInfoPanel/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonGroup;
        [AutoBind("./SellItemInfoPanel/TipLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipLeftDummy;
        [AutoBind("./SellItemInfoPanel/TipRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipRightDummy;
        [AutoBind("./SellItemInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sellInfoPanelRoot;
        [AutoBind("./SellItemInfoPanel/ItemSimpleInfoLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoLeftDummy;
        [AutoBind("./SellItemInfoPanel/ItemSimpleInfoRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoRightDummy;
        [AutoBind("./SellItemInfoPanel/TotalPrice/TotalText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_totalMoneyText;
        [AutoBind("./SellItemInfoPanel/TotalPrice/TotalText/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_totalMoneyIconImage;
        private const string AssetPathCommonItemPrefab = "CommonItemUIPrefab";
        private const string AssetPathHintGroupPrefab = "HintGroupUIPrefab";
        private const string AssetPathItemSimpleInfoPrefab = "ItemSimpleInfoUIPrefab";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateSellItem;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_GetSellInfoPanelSize;
        private static DelegateBridge __Hotfix_IsShowSellPanelInfoPanel;
        private static DelegateBridge __Hotfix_GetTipWindowUIPrecess;
        private static DelegateBridge __Hotfix_GetSellItemInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_SetMarketPrice;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_OnTipBackGroundClick;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnItemShareButtonClick;
        private static DelegateBridge __Hotfix_OnMoreButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackGroundClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackGroundClick;
        private static DelegateBridge __Hotfix_add_EventOnTipBackGroundClick;
        private static DelegateBridge __Hotfix_remove_EventOnTipBackGroundClick;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemShareButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemShareButtonClick;

        public event Action<PointerEventData, Action<PointerEventData>> EventOnBackGroundClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemShareButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnTipBackGroundClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string> EventOnTipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector2 GetSellInfoPanelSize()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSellItemInfoPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIPrecess(string state, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShowSellPanelInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMoreButtonClick(string button)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMarketPrice(AuctionItemDetailInfo itemDetailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 panelPos, PanelPosType tipPosType)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSellItem(AuctionItemDetailInfo itemDetailInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public enum PanelPosType
        {
            Left,
            Middle,
            Right
        }
    }
}

