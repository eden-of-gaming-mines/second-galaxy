﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class OverSeaActivityBindUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestIntent;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestCloseAllPanels;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRefreshRedPoint;
        private const string ParamKeyIsShow = "IsShow";
        private const string ParamKeyIsHide = "IsHide";
        private const string ParamKeyIsPlayAnimationImmedite = "IsPlayAnimationImmedite";
        private bool m_isBindRewardRedeemed;
        private bool m_isAccountBound;
        private List<FakeLBStoreItem> m_rewardItems;
        private OverSeaActivityBindUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private bool m_processSDKBind;
        private DateTime m_processSDKBindTimeOuTime;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartOverSeaActivityBindingUITaskWithPrepare;
        private static DelegateBridge __Hotfix_ShowBindPanel;
        private static DelegateBridge __Hotfix_HideBindPanel;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRedeemRewardButtonClick;
        private static DelegateBridge __Hotfix_OnBindAccountButtonClick;
        private static DelegateBridge __Hotfix_OnBindGuestEnd;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RequestIntent;
        private static DelegateBridge __Hotfix_add_EventOnRequestIntent;
        private static DelegateBridge __Hotfix_remove_EventOnRequestIntent;
        private static DelegateBridge __Hotfix_add_EventOnRequestCloseAllPanels;
        private static DelegateBridge __Hotfix_remove_EventOnRequestCloseAllPanels;
        private static DelegateBridge __Hotfix_add_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_remove_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnRefreshRedPoint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool> EventOnRequestCloseAllPanels
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestIntent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public OverSeaActivityBindUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void HideBindPanel(bool isImediateComplete, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindAccountButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindGuestEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRedeemRewardButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RequestIntent(Action<UIIntent> action)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBindPanel(bool isImediateComplete, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(FakeLBStoreItem item, Vector3 pos, ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartOverSeaActivityBindingUITaskWithPrepare(Action<bool> onPrepareEnd, Action onResLoadEnd = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideBindPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal OverSeaActivityBindUITask $this;

            internal void <>m__0(UIProcess process, bool b)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(b);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey1
        {
            internal FakeLBStoreItem item;
            internal Vector3 pos;
            internal string mode;
            internal ItemSimpleInfoUITask.PositionType posType;
            internal OverSeaActivityBindUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, returnIntent, true, this.pos, this.mode, this.posType, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            IsShowPanel,
            IsHidePanel,
            IsPlayAnimationImmedite
        }

        public class SendCommanderAuthRewardRecvReqNetWorkTask : NetWorkTransactionTask
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Result>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private List<StoreItemUpdateInfo> <RewardItems>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnCommanderAuthRewardRecvAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_RewardItems;
            private static DelegateBridge __Hotfix_set_RewardItems;

            public SendCommanderAuthRewardRecvReqNetWorkTask() : base(10f, true, false, false)
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            private void OnCommanderAuthRewardRecvAck(CommanderAuthRewardRecvAck ack)
            {
                DelegateBridge bridge = __Hotfix_OnCommanderAuthRewardRecvAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, ack);
                }
                else
                {
                    this.Result = ack.Result;
                    this.RewardItems = new List<StoreItemUpdateInfo>();
                    foreach (ProStoreItemUpdateInfo info in ack.MStoreItemList)
                    {
                        this.RewardItems.Add(CommonLogicUtil.Convert2StoreItemUpdateInfo(info));
                    }
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnCommanderAuthRewardRecvAck += new Action<CommanderAuthRewardRecvAck>(this.OnCommanderAuthRewardRecvAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendCommanderAuthRewardRecvReq());
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnCommanderAuthRewardRecvAck -= new Action<CommanderAuthRewardRecvAck>(this.OnCommanderAuthRewardRecvAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public List<StoreItemUpdateInfo> RewardItems
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_RewardItems;
                    return ((bridge == null) ? this.<RewardItems>k__BackingField : bridge.__Gen_Delegate_Imp4434(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_RewardItems;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<RewardItems>k__BackingField = value;
                    }
                }
            }
        }
    }
}

