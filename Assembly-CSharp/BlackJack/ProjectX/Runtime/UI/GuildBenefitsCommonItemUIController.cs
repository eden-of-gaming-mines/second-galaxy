﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBenefitsCommonItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildCommonBenefitsInfo <ItemInfo>k__BackingField;
        public const string SimpleCommonItem = "SimpleCommonItem";
        public const string RewardItem = "RewardItem";
        public const int MaxRewardCount = 8;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnPreviewItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnObtainRewardsClick;
        private ScrollItemBaseUIController m_scrollItemBaseUICtrl;
        private GuildBenefitsSimpleCommonItemUIController m_simpleIconCtrl;
        private readonly List<CommonItemIconUIController> m_rewardItemCtrlList;
        [AutoBind("./NormalGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_giftBagIconTf;
        [AutoBind("./NormalGroup/IconNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./NormalGroup/IconNameText/ComeFromText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemComeFromText;
        [AutoBind("./NormalGroup/GSupplyImage/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_currencyImage;
        [AutoBind("./NormalGroup/GSupplyImage/SupplyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currencyText;
        [AutoBind("./NormalGroup/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardItemGroup;
        [AutoBind("./NormalGroup/ItemGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_rewardItemTemplateTf;
        [AutoBind("./NormalGroup/ResidueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeUntilExpiredText;
        [AutoBind("./NormalGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeFromCreatedText;
        [AutoBind("./NormalGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_rewardObtainButton;
        [AutoBind("./NoMoreGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_noMoreStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateCommonItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPreviewItemClick;
        private static DelegateBridge __Hotfix_OnObtainRewardsClick;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_get_ItemInfo;
        private static DelegateBridge __Hotfix_set_ItemInfo;
        private static DelegateBridge __Hotfix_add_EventOnPreviewItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnPreviewItemClick;
        private static DelegateBridge __Hotfix_add_EventOnObtainRewardsClick;
        private static DelegateBridge __Hotfix_remove_EventOnObtainRewardsClick;

        public event Action<UIControllerBase> EventOnObtainRewardsClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnPreviewItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnObtainRewardsClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreviewItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCommonItem(string stateName, GuildCommonBenefitsInfo commonItemInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GuildCommonBenefitsInfo ItemInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

