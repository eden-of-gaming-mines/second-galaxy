﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopSelectedItemListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBuyOrSellButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selectedItemListCtrl;
        [AutoBind("./MoneyNunberGroup/IconBGImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./MoneyNunberGroup/MoneyNunberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumberText;
        [AutoBind("./BuyOrSellButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyOrSellButton;
        [AutoBind("./ItemStoreListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemStoreListTrans;
        public ItemStoreListUIController m_itemStoreListUICtrl;
        private bool m_isItemListForBuy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetListState;
        private static DelegateBridge __Hotfix_SetCurrMoneyInfo;
        private static DelegateBridge __Hotfix_GetCurrListStartIndex;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBuyOrSellButtonClick;
        private static DelegateBridge __Hotfix_OnItemFilled;
        private static DelegateBridge __Hotfix_add_EventOnBuyOrSellButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuyOrSellButtonClick;

        public event Action EventOnBuyOrSellButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrListStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyOrSellButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFilled(ILBStoreItemClient item, CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrMoneyInfo(CurrencyType moneyType, ulong moneyNum, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetListState(bool isItemListForBuy, bool isEmpty)
        {
        }
    }
}

