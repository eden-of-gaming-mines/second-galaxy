﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionBuyPanelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnBuyItemIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnBuyItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopItemCategory> EventOnItemCategoryButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItemTypeToggleSelected;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFinishedProductButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBluePrintButtonClick;
        public List<ProAuctionItemBriefInfo> m_buyItemInfoList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private AuctionItemCatagoryToggleListUIController m_toggleListCtrl;
        private const string AuctionBuyItemPoolName = "AuctionBuyItem";
        [AutoBind("./BuyItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_itemListScrollRect;
        [AutoBind("./BuyItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemListUIItemPool;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./ItemCategoryToggleGroupPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_toggleGroupRoot;
        [AutoBind("./ItemSimpleInfoLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoLeftDummy;
        [AutoBind("./ItemSimpleInfoRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoRightDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateBuyItemInfoList;
        private static DelegateBridge __Hotfix_GetFristBuyItem;
        private static DelegateBridge __Hotfix_UpdateaCategoryAndTypeSelectState;
        private static DelegateBridge __Hotfix_ClearAllTypeToggleSelectState;
        private static DelegateBridge __Hotfix_SwitchCateGoryToggleState;
        private static DelegateBridge __Hotfix_SwitchBluePrintOrFinishedProductToggleState;
        private static DelegateBridge __Hotfix_CreateAllCategoryToggles;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnBuyItemClick;
        private static DelegateBridge __Hotfix_OnBuyItemFill;
        private static DelegateBridge __Hotfix_OnBuyItemIconClick;
        private static DelegateBridge __Hotfix_OnClickFinishedProductClick;
        private static DelegateBridge __Hotfix_OnBluePrintClick;
        private static DelegateBridge __Hotfix_OnItemCategoryClick;
        private static DelegateBridge __Hotfix_OnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnBuyItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuyItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnBuyItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuyItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBluePrintButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBluePrintButtonClick;

        public event Action EventOnBluePrintButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnBuyItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnBuyItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFinishedProductButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopItemCategory> EventOnItemCategoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemTypeToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearAllTypeToggleSelectState(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllCategoryToggles(Dictionary<NpcShopItemCategory, List<int>> bluePrintToggleList, Dictionary<NpcShopItemCategory, List<int>> finishedProductToggleList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetFristBuyItem()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos(bool isLeft)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBluePrintClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickFinishedProductClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCategoryClick(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemTypeToggleSelected(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchBluePrintOrFinishedProductToggleState(bool isBluePrint)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchCateGoryToggleState(NpcShopItemCategory category, bool isBluePrint)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateaCategoryAndTypeSelectState(NpcShopItemCategory category, NpcShopItemType type, bool isBluePrint)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuyItemInfoList(List<ProAuctionItemBriefInfo> buyItemInfoList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

