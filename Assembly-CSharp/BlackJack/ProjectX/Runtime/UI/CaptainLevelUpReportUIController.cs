﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainLevelUpReportUIController : UIControllerBase
    {
        private List<CaptainLevelUpFeatItemUIController> m_unusedFeatItemList;
        private const string PropertyValueAddEffect = "Show";
        private const string GetNewFeatOrLevelUpEffect = "SkillLeaveUp";
        private const string NoNewFeatOrLevelUpEffect = "NoSillGet";
        private const string PanelOpenState = "Show";
        private const string PanelCloseState = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./ButtonBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ButtonBG;
        [AutoBind("./CaptainImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainImage;
        [AutoBind("./TitleGroup/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./TitleGroup/SubRankText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainSubRankText;
        [AutoBind("./TitleGroup/Profession/ProfessionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIconImage;
        [AutoBind("./TitleGroup/Profession/ProfessionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProfessionText;
        [AutoBind("./TitleGroup/LevelUpGroup/PreLvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreLvNumberText;
        [AutoBind("./TitleGroup/LevelUpGroup/NewLvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NewLvNumberText;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/PropertyAddGroup/WeaponValueTextGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreWeaponValueText;
        [AutoBind("./Margin/PropertyAddGroup/WeaponValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponValueBonusText;
        [AutoBind("./Margin/PropertyAddGroup/WeaponValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeaponValueBonusEffectCtrl;
        [AutoBind("./Margin/PropertyAddGroup/DriveValueTextGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreDriveValueText;
        [AutoBind("./Margin/PropertyAddGroup/DriveValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DriveValueBonusText;
        [AutoBind("./Margin/PropertyAddGroup/DriveValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DriveValueBonusEffectCtrl;
        [AutoBind("./Margin/PropertyAddGroup/DefenseValueTextGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreDefenseValueText;
        [AutoBind("./Margin/PropertyAddGroup/DefenseValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DefenseValueBonusText;
        [AutoBind("./Margin/PropertyAddGroup/DefenseValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DefenseValueBonusEffectCtrl;
        [AutoBind("./Margin/PropertyAddGroup/ElectronValueTextGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreElectronValueText;
        [AutoBind("./Margin/PropertyAddGroup/ElectronValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectronValueBonusText;
        [AutoBind("./Margin/PropertyAddGroup/ElectronValueTextGroup/AddText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ElectronValueBonusEffectCtrl;
        [AutoBind("./Margin/LevelUpDetail/GetNewFeat/GetNewFeatRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GetNewFeatRoot;
        [AutoBind("./Margin/LevelUpDetail/GetNewFeat", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GetNewFeatEffectCtrl;
        [AutoBind("./Margin/LevelUpDetail/FeatLevelUp/FeatLevelUpRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatLevelUpRoot;
        [AutoBind("./Margin/LevelUpDetail/FeatLevelUp", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatLevelUpEffectCtrl;
        [AutoBind("./Margin/LevelUpDetail/GetNewFeatOrLevelUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GetNewFeatOrLevelUp;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCaptainLevelUpReportInfo;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_RecycleAllFeatItem;
        private static DelegateBridge __Hotfix_GetLevelUpFeatItemCtrl;

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected CaptainLevelUpFeatItemUIController GetLevelUpFeatItemCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void RecycleAllFeatItem()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainLevelUpReportInfo(LBStaticHiredCaptain captain, int preLevel, int currLevel, List<LBNpcCaptainFeats> preFeats, List<LBNpcCaptainFeats> newFeats, List<LBNpcCaptainFeats> upFeats, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

