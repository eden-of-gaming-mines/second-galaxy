﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildMemberApplyForListUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventUpdateMemberCount;
        private GuildApplyForListUIController m_applyForCtrl;
        private uint m_curApplyVersion;
        private List<GuildJoinApplyViewInfo> m_applyList;
        private bool m_scoreSortUpMode;
        private bool m_timeSortUpMode;
        private bool m_mainSortModeIsTime;
        private bool m_isNeedReloadRes;
        private bool m_pauseTaskOnPostUpdateView;
        private UITaskBase.LayerDesc[] layer;
        private UITaskBase.UIControllerDesc[] ctrl;
        public const string TaskName = "GuildMemberApplyForListUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask_0;
        private static DelegateBridge __Hotfix_StartTask_1;
        private static DelegateBridge __Hotfix_RequestApplyForList;
        private static DelegateBridge __Hotfix_PlayShowProcess;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_ApplyForListReq;
        private static DelegateBridge __Hotfix_OnRatifyBtnClick;
        private static DelegateBridge __Hotfix_OnCancelBtnClick;
        private static DelegateBridge __Hotfix_SendApplyComfirm;
        private static DelegateBridge __Hotfix_OnSortByRegTime;
        private static DelegateBridge __Hotfix_OnSortByScore;
        private static DelegateBridge __Hotfix_OnSortList;
        private static DelegateBridge __Hotfix_ComporeByRegTime;
        private static DelegateBridge __Hotfix_ComporeByScore;
        private static DelegateBridge __Hotfix_add_EventUpdateMemberCount;
        private static DelegateBridge __Hotfix_remove_EventUpdateMemberCount;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventUpdateMemberCount
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildMemberApplyForListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ApplyForListReq(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private int ComporeByRegTime(GuildJoinApplyViewInfo lhs, GuildJoinApplyViewInfo rhs, bool up)
        {
        }

        [MethodImpl(0x8000)]
        private int ComporeByScore(GuildJoinApplyViewInfo lhs, GuildJoinApplyViewInfo rhs, bool up)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelBtnClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRatifyBtnClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortByRegTime()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortByScore()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortList()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayShowProcess(bool show, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void RequestApplyForList(bool showErrCode, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendApplyComfirm(int index, bool agree)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberApplyForListUITask StartTask(Action onResLoadComplete)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ApplyForListReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal GuildMemberApplyForListUITask $this;

            internal void <>m__0(bool ret)
            {
                if (!ret)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    LogicBlockGuildClient.GuildApplyListInfo applyListInfo = ((GameManager.Instance.PlayerContext as ProjectXPlayerContext).GetLBGuild() as LogicBlockGuildClient).GetApplyListInfo();
                    this.$this.m_isNeedReloadRes = applyListInfo.version != this.$this.m_curApplyVersion;
                    this.$this.m_curApplyVersion = applyListInfo.version;
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSortList>c__AnonStorey2
        {
            internal Func<GuildJoinApplyViewInfo, GuildJoinApplyViewInfo, bool, int> fisrtCompore;
            internal bool firstSortMode;
            internal Func<GuildJoinApplyViewInfo, GuildJoinApplyViewInfo, bool, int> secondCompore;
            internal bool secondSortMode;

            internal int <>m__0(GuildJoinApplyViewInfo lhs, GuildJoinApplyViewInfo rhs)
            {
                int num = this.fisrtCompore(lhs, rhs, this.firstSortMode);
                if (num == 0)
                {
                    num = this.secondCompore(lhs, rhs, this.secondSortMode);
                }
                return num;
            }
        }

        [CompilerGenerated]
        private sealed class <RequestApplyForList>c__AnonStorey0
        {
            internal bool showErrCode;
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildApplyForListReqNetTask task2 = task as GuildApplyForListReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.ApplyListAck.Result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        if (this.showErrCode)
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.ApplyListAck.Result, true, false);
                        }
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        public enum PipeLineMaskType
        {
            Sort
        }
    }
}

