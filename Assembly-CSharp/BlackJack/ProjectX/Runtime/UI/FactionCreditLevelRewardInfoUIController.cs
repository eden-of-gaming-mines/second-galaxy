﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class FactionCreditLevelRewardInfoUIController : UIControllerBase
    {
        private Dictionary<string, Object> m_resList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GrandFaction <CurrentGrandFaction>k__BackingField;
        public int m_CurFactionID;
        public ConfigDataFactionCreditQuestWeeklyRewardLevelInfo m_curSelectInfo;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsGet>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsEnough>k__BackingField;
        [AutoBind("./ReturnDetail/NationToggle01", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NationToggle01;
        [AutoBind("./ReturnDetail/NationToggle01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationToggleState01;
        [AutoBind("./ReturnDetail/NationToggle02", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NationToggle02;
        [AutoBind("./ReturnDetail/NationToggle02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationToggleState02;
        [AutoBind("./ReturnDetail/NationToggle03", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NationToggle03;
        [AutoBind("./ReturnDetail/NationToggle03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationToggleState03;
        [AutoBind("./ReturnDetail/NationToggle04", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NationToggle04;
        [AutoBind("./ReturnDetail/NationToggle04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationToggleState04;
        [AutoBind("./ReturnDetail/NationToggle05", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NationToggle05;
        [AutoBind("./ReturnDetail/NationToggle05", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NationToggleState05;
        [AutoBind("./YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx YesButton;
        [AutoBind("./YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController YesButtonUIStateCtr;
        [AutoBind("/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NoButton;
        [AutoBind("./MoneyGroup/MoneyGroup01/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIcon01;
        [AutoBind("./MoneyGroup/MoneyGroup01/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyValueText01;
        [AutoBind("./MoneyGroup/MoneyGroup02/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIcon02;
        [AutoBind("./MoneyGroup/MoneyGroup02/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyValueText02;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController VisualUIState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_NationCloseTips;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewByID;
        private static DelegateBridge __Hotfix_SetAllNationButtonToHide;
        private static DelegateBridge __Hotfix_get_CurrentGrandFaction;
        private static DelegateBridge __Hotfix_set_CurrentGrandFaction;
        private static DelegateBridge __Hotfix_get_IsGet;
        private static DelegateBridge __Hotfix_set_IsGet;
        private static DelegateBridge __Hotfix_get_IsEnough;
        private static DelegateBridge __Hotfix_set_IsEnough;

        [MethodImpl(0x8000)]
        private void NationCloseTips(GrandFaction gFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllNationButtonToHide()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(bool isGet, bool isEnough, Dictionary<string, Object> resList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewByID(GrandFaction gFaction)
        {
        }

        public GrandFaction CurrentGrandFaction
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsGet
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsEnough
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateViewByID>c__AnonStorey0
        {
            internal int factionId;

            internal bool <>m__0(CreditRewardInfo item) => 
                (item.FactionId == this.factionId);

            internal bool <>m__1(CreditRewardItemInfo item) => 
                (item.FactionId == this.factionId);
        }
    }
}

