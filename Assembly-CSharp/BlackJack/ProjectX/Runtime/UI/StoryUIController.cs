﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StoryUIController : UIControllerBase
    {
        public ChapterStoryUIController ChapterStoryCtrl;
        [AutoBind("./Story", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StoryPanelUIStateCtrl;
        [AutoBind("./Story", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StoryPanel;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_GetStoryPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_HideStoryPanel;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetStoryPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        public void HideStoryPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }
    }
}

