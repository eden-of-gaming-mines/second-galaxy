﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemShipBuffDetailInfoItemUIController : UIControllerBase
    {
        [AutoBind("./BuffName/BuffNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buffName;
        [AutoBind("./BuffName/BuffTimeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buffTimeCtrl;
        [AutoBind("./BuffName/BuffTimeRoot/BuffTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buffTimeText;
        [AutoBind("./BuffName/BuffIconImageRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buffIconCtrl;
        [AutoBind("./BuffDetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buffDetailText;
        public ScrollItemBaseUIController m_scrollItemCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetBuffNameInfo;
        private static DelegateBridge __Hotfix_SetBuffDescInfo;
        private static DelegateBridge __Hotfix_SetBuffTimeInfo;
        private static DelegateBridge __Hotfix_HideBuffTimeInfo;
        private static DelegateBridge __Hotfix_SetBuffIcon;

        [MethodImpl(0x8000)]
        public void HideBuffTimeInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuffDescInfo(string buffDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuffIcon(ShipFightBufType bufType, bool isEnhance)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuffNameInfo(string buffName, int attachCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuffTimeInfo(bool isForeverBuff, string buffTimeStr)
        {
        }
    }
}

