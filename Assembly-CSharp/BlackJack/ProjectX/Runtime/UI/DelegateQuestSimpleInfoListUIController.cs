﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.Utils;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DelegateQuestSimpleInfoListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SignalType> EventOnFilterItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDelegateQuestItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemShareClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemPlayerNameClick;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private List<DelegateMissionInfo> m_historyDelegateInfoList;
        private List<NpcCaptainInfo> m_showNpcCaptainList;
        public TinyCorutineHelper m_corutineHelepr;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelShowOrHideCommonUIStateCtrl;
        [AutoBind("./SortList/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FliterPanelShowOrHideCommonUIStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T1", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DelegateFightFilterButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T2", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DelegateMineralFilterButtonStateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BgButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./SortList/SortListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FilterShowOrHideButton;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T1", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DelegateFightFilterButton;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T2", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DelegateMineralFilterButton;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollView;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool EasyPool;
        private const string DelegateItemPoolName = "DelegateItemPool";
        private const string DelegateItemAssetName = "DelegateItem";
        [CompilerGenerated]
        private static Predicate<SignalType> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<SignalType> <>f__am$cache1;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateMissionQuestSimpleInfoListPanel;
        private static DelegateBridge __Hotfix_IsScrollViewInEnd;
        private static DelegateBridge __Hotfix_SetFilterListState;
        private static DelegateBridge __Hotfix_GetUIShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetShowOrHideFilterPanelProcess;
        private static DelegateBridge __Hotfix_OnDelegateItemFill;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnDelegateFightFilterItemClick;
        private static DelegateBridge __Hotfix_OnDelegateMineralFilterItemClick;
        private static DelegateBridge __Hotfix_OnItemPlayerNameButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemShareButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnFilterItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnFilterItemClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemShareClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemShareClick;
        private static DelegateBridge __Hotfix_add_EventOnItemPlayerNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemPlayerNameClick;

        public event Action<int> EventOnDelegateQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SignalType> EventOnFilterItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemPlayerNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemShareClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetShowOrHideFilterPanelProcess(bool isShow, bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetUIShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsScrollViewInEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateFightFilterItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDelegateItemFill(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMineralFilterItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPlayerNameButtonClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemShareButtonClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFilterListState(List<SignalType> filterList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMissionQuestSimpleInfoListPanel(List<DelegateMissionInfo> historyDelegateQuestList, List<NpcCaptainInfo> captainInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

