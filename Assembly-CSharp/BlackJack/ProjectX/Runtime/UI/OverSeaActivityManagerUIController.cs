﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class OverSeaActivityManagerUIController : UIControllerBase
    {
        private const string PanelStateShow = "Show";
        private const string PanelStateClose = "Close";
        private const string ButtonStateNormal = "Normal";
        private const string ButtonStateChoose = "Choose";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ButtonGroup/LogInButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_loginButton;
        [AutoBind("./ButtonGroup/LogInButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_loginStateCtrl;
        [AutoBind("./ButtonGroup/LogInButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_loginRedPoint;
        [AutoBind("./ButtonGroup/AttestationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_signInButton;
        [AutoBind("./ButtonGroup/AttestationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_signInStateCtrl;
        [AutoBind("./ButtonGroup/AttestationButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_signInRedPoint;
        [AutoBind("./ButtonGroup/BoundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bindButton;
        [AutoBind("./ButtonGroup/BoundButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bindStateCtrl;
        [AutoBind("./ButtonGroup/BoundButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_bindRedPoint;
        private static DelegateBridge __Hotfix_GetPanelProcess;
        private static DelegateBridge __Hotfix_UpdateViewForPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess GetPanelProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewForPanel(string selectMode, bool showLoginRedPoint, bool showSignInRedPoint, bool showBindRedPoint, bool enableBindTab)
        {
        }
    }
}

