﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildProduceQueueItemUIController : UIControllerBase
    {
        protected const string IdleMode = "Free";
        protected const string LockMode = "Lock";
        protected const string ProduceMode = "Normal";
        protected const string CompleteMode = "Complete";
        protected const string LostMode = "Loss";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildProductionLineInfo, int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildProductionLineInfo> EventOnItemProduceCompleted;
        protected GuildProductionLineInfo m_currProduceLine;
        protected bool m_isProducing;
        protected float m_completeProgress;
        public CommonItemIconUIController m_commonItemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ClickImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemUIStartCtrl;
        [AutoBind("./ProgressBarGroup/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBarImage;
        [AutoBind("./ProgressBarGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ShipInfoGroup/FactoryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FactoryNameText;
        [AutoBind("./ShipInfoGroup/ProduceNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProduceNameText;
        [AutoBind("./ShipInfoGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelText;
        [AutoBind("./ShipInfoGroup/CountTitleText/CountNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CountNumberText;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        [AutoBind("./ProgressBarGroup/BarNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CompleteProcessText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateItemViewInfo;
        private static DelegateBridge __Hotfix_SetSelecteState;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemProduceCompleted;
        private static DelegateBridge __Hotfix_remove_EventOnItemProduceCompleted;
        private static DelegateBridge __Hotfix_get_CurrProduceLine;
        private static DelegateBridge __Hotfix_get_CompleteProgress;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<GuildProductionLineInfo, int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildProductionLineInfo> EventOnItemProduceCompleted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelecteState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(GuildProductionLineInfo produceLine, Dictionary<string, UnityEngine.Object> m_resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateItemViewInfo(DateTime currTime)
        {
        }

        public GuildProductionLineInfo CurrProduceLine
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float CompleteProgress
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

