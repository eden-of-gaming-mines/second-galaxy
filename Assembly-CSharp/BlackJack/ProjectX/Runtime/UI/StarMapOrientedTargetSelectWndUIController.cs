﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class StarMapOrientedTargetSelectWndUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CommanderPositionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_commanderButton;
        [AutoBind("./MotherShipPositionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_motherShipButton;
        [AutoBind("./GuildBasePositionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_guildBaseButton;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateOrientedTargetSelectWndUIProcess;
        private static DelegateBridge __Hotfix_EnableGuildBaseButton;

        [MethodImpl(0x8000)]
        public UIProcess CreateOrientedTargetSelectWndUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableGuildBaseButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

