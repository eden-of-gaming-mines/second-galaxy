﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationShipItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnCommonIconClick;
        public ScrollItemBaseUIController m_scrollItem;
        public CommonItemIconUIController m_shipItemIconCtrl;
        [AutoBind("./CornerMarkGroup/ScopeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ScopeText;
        [AutoBind("./CornerMarkGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CornerMarkState;
        [AutoBind("./MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./LossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LossNumberText;
        [AutoBind("./IssueNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text IssueNumberText;
        [AutoBind("./CompensationProgressText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProgressText;
        [AutoBind("./ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipNameText;
        [AutoBind("./PlayerNmaeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNmaeText;
        [AutoBind("./CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ShipItemDummy;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;

        public event Action<CommonItemIconUIController> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(LossCompensation comp, Dictionary<string, UnityEngine.Object> dict)
        {
        }
    }
}

