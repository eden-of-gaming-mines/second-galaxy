﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildFlagShipHangarDataSyncNetTask : NetWorkTransactionTask
    {
        private bool m_getAllHangarComplete;
        private bool m_getGuildStoreInfoComplete;
        private bool m_getHangarInfoComplete;
        private bool m_getGuildOccupyInfoComplete;
        private int m_result;
        private ulong m_singleHangarInstanceId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarListAck;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarDetailInfoAck;
        private static DelegateBridge __Hotfix_OnGuildGuildItemStoreInfoAck;
        private static DelegateBridge __Hotfix_OnGuildOccupiedSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_IsAllReqComplete;
        private static DelegateBridge __Hotfix_CreateProGuildFlagShipHangarVersionInfoFromHangarInfo;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarDataSyncNetTask(bool getAllHangarInfo, bool getGuildStoreInfo, ulong hangarInstanceId = 0UL, bool blockedUI = true)
        {
        }

        [MethodImpl(0x8000)]
        private ProGuildFlagShipHangarVersionInfo CreateProGuildFlagShipHangarVersionInfoFromHangarInfo(IGuildFlagShipHangarDataContainer info)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllReqComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarDetailInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarListAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildGuildItemStoreInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildOccupiedSolarSystemInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

