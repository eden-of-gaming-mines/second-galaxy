﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class FactionCreditLevelRewarReqTask : NetWorkTransactionTask
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <SetReqResult>k__BackingField;
        private int m_id;
        private int m_grandFaction;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GrandFaction <GrandFactionAck>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnFactionCreditLevelRewarAck;
        private static DelegateBridge __Hotfix_set_SetReqResult;
        private static DelegateBridge __Hotfix_get_SetReqResult;
        private static DelegateBridge __Hotfix_get_GrandFactionAck;
        private static DelegateBridge __Hotfix_set_GrandFactionAck;

        [MethodImpl(0x8000)]
        public FactionCreditLevelRewarReqTask(int id, int grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFactionCreditLevelRewarAck(int result, int id, GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int SetReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public GrandFaction GrandFactionAck
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

