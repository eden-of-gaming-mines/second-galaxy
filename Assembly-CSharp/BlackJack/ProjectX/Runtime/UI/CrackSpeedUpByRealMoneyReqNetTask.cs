﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CrackSpeedUpByRealMoneyReqNetTask : NetWorkTransactionTask
    {
        private int m_boxIdx;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CrackSpeedUpByRealMoneyResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCrackSpeedUpByRealMoneyAck;
        private static DelegateBridge __Hotfix_set_CrackSpeedUpByRealMoneyResult;
        private static DelegateBridge __Hotfix_get_CrackSpeedUpByRealMoneyResult;

        [MethodImpl(0x8000)]
        public CrackSpeedUpByRealMoneyReqNetTask(int boxIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCrackSpeedUpByRealMoneyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CrackSpeedUpByRealMoneyResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

