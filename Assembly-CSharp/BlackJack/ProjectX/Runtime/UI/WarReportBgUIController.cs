﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WarReportBgUIController : UIControllerBase
    {
        public Action EventOnBackButtonClick;
        public Action EventOnCloseButtonClick;
        public Action EventOnWarOverviewButtonClick;
        public Action EventOnLossRatioButtonClick;
        public Action EventOnLossShipButtonClick;
        public Action EventOnKillRankingListButtonClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./ButtonGroup/WarOverviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WarOverviewButton;
        [AutoBind("./ButtonGroup/LossRatioButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LossRatioButton;
        [AutoBind("./ButtonGroup/LossShipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LossShipButton;
        [AutoBind("./ButtonGroup/KillRankingListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx KillRankingListButton;
        [AutoBind("./ButtonGroup/WarOverviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WarOverviewButtonStateCtrl;
        [AutoBind("./ButtonGroup/LossRatioButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LossRatioButtonStateCtrl;
        [AutoBind("./ButtonGroup/LossShipButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LossShipButtonStateCtrl;
        [AutoBind("./ButtonGroup/KillRankingListButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillRankingListButtonStateCtrl;
        [AutoBind("./GuildPandectUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WarOverviewDummy;
        [AutoBind("./GuildWarLossUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossRatioDummy;
        [AutoBind("./GuildShipLossUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossShipDummy;
        [AutoBind("./GuildKillGroupUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RankingListDummy;
        private const string NormalMode = "Normal";
        private const string SelectMode = "Select";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateTabSelectState;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnWarOverviewButtonClick;
        private static DelegateBridge __Hotfix_OnLossRatioButtonClick;
        private static DelegateBridge __Hotfix_OnLossShipButtonClick;
        private static DelegateBridge __Hotfix_OnKillRankingListButtonClick;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRankingListButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossRatioButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarOverviewButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTabSelectState(WarReportManagerUITask.TabType tabType)
        {
        }
    }
}

