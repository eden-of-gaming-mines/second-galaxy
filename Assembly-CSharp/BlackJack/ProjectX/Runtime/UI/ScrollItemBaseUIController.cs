﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ScrollItemBaseUIController : UIControllerBase
    {
        public Action<UIControllerBase> EventOnUIItemNeedFill;
        public Action<UIControllerBase> EventOnUIItemClick;
        public Action<UIControllerBase> EventOnUIItemDoubleClick;
        public Action<UIControllerBase> EventOnUIItem3DTouch;
        public int m_itemIndex;
        public UIControllerBase m_ownerCtrl;
        private ThreeDTouchEventListener m_3dTouchEventTrigger;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_ScrollCellIndex;
        private static DelegateBridge __Hotfix_OnScrollItemClick;
        private static DelegateBridge __Hotfix_OnScrollItemDoubleClick;
        private static DelegateBridge __Hotfix_OnScrollItem3DTouch;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        [MethodImpl(0x8000)]
        public void Init(UIControllerBase ownerCtrl, bool isCareItemClick = true)
        {
        }

        [MethodImpl(0x8000)]
        public void OnScrollItem3DTouch()
        {
        }

        [MethodImpl(0x8000)]
        public void OnScrollItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnScrollItemDoubleClick()
        {
        }

        [MethodImpl(0x8000)]
        public void ScrollCellIndex(int itemIndex)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

