﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum UIGroup
    {
        Entry,
        Login,
        Character,
        Common,
        GameLogicInSpace,
        GameLogicInStation,
        GameLogicCommon
    }
}

