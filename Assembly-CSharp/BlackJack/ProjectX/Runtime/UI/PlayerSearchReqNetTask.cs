﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class PlayerSearchReqNetTask : NetWorkTransactionTask
    {
        private string m_playerName;
        private BlackJack.ProjectX.Common.PlayerSimplestInfo m_playerSimplestInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnPlayerSearchAck;
        private static DelegateBridge __Hotfix_get_PlayerSimplestInfo;

        [MethodImpl(0x8000)]
        public PlayerSearchReqNetTask(string playerName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerSearchAck(BlackJack.ProjectX.Common.PlayerSimplestInfo playerSimplestInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public BlackJack.ProjectX.Common.PlayerSimplestInfo PlayerSimplestInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

