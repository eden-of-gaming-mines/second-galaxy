﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipOptLogUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnLogTypeFilterItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSolarSystemFilterItemClick;
        private const string LogItem = "LogItem";
        private const string LogTypeFilterItem = "LogTypeFilterItem";
        private const string SolarSystemFilterItem = "SolarSystemFilterItem";
        private List<GuildFlagShipOptLogInfo> m_cachedLogInfos;
        private List<GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo> m_cachedLogTypeFilterInfos;
        private List<GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo> m_cachedSolarSystemFilterInfos;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./FilterBackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_filterBackButton;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_logScrollRect;
        [AutoBind("./TypeDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_logTypeDropdownStateCtrl;
        [AutoBind("./TypeDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_logTypeDropdownButton;
        [AutoBind("./TypeDropdown/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_logTypeDropdownScrollRect;
        [AutoBind("./SolarSystemDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_solarSystemDropdownStateCtrl;
        [AutoBind("./SolarSystemDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_solarSystemDropdownButton;
        [AutoBind("./SolarSystemDropdown/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_solarSystemDropdownScrollRect;
        [AutoBind("./EasyObjectPoolRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./EasyObjectPoolRoot/LogItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_logItemRootTf;
        [AutoBind("./EasyObjectPoolRoot/FilterItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_filterItemRootTf;
        [AutoBind("./EasyObjectPoolRoot/FilterItemRoot/Item", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_filterItemTemplateGo;
        private static DelegateBridge __Hotfix_CreateProjectPanelProcess;
        private static DelegateBridge __Hotfix_UpdateViewOnLogArea;
        private static DelegateBridge __Hotfix_UpdateViewOnLogTypeFilterArea;
        private static DelegateBridge __Hotfix_UpdateViewOnSolarSystemFilterArea;
        private static DelegateBridge __Hotfix_UpdateViewOnLogTypeFilterSpecialIndex;
        private static DelegateBridge __Hotfix_UpdateViewOnSolarSystemFilterSpecialIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnEasyObjectItemCreated;
        private static DelegateBridge __Hotfix_OnLogItemFill;
        private static DelegateBridge __Hotfix_OnLogTypeFilterItemFill;
        private static DelegateBridge __Hotfix_OnSolarSystemFilterItemFill;
        private static DelegateBridge __Hotfix_OnLogTypeFilterItemClick;
        private static DelegateBridge __Hotfix_OnSolarSystemFilterItemClick;
        private static DelegateBridge __Hotfix_add_EventOnLogTypeFilterItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnLogTypeFilterItemClick;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemFilterItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemFilterItemClick;

        public event Action<int> EventOnLogTypeFilterItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSolarSystemFilterItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateProjectPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyObjectItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogTypeFilterItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogTypeFilterItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemFilterItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemFilterItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnLogArea(List<GuildFlagShipOptLogInfo> infos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnLogTypeFilterArea(string stateName, bool isOpen, List<GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo> logTypeFilterInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnLogTypeFilterSpecialIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnSolarSystemFilterArea(string stateName, bool isOpen, List<GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo> solarSystemFilterInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnSolarSystemFilterSpecialIndex(int index)
        {
        }
    }
}

