﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MotherShipUITask : UITaskBase
    {
        private bool m_isInAnnouncement;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAnnouncementButtonClick;
        private DateTime m_nextTickTime;
        private SystemAnnouncementUITask m_announcementUITask;
        private const string IsGuildModeIntentParam = "IsGuildModeIntentParam";
        private bool m_isGuildMode;
        protected Vector3 m_cachedPosition;
        private MotherShipUIController m_mainCtrl;
        private List<GuildProductionLineInfo> m_produceLineCache;
        private List<GuildBuildingInfo> m_guildBuildingInfoList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTechButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCrewDormitoryButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnItemStoreButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnHangarButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBuildingButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCrackButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDevelopmentButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnDelegateMissionFleetItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildStoreButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildPurchaseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStarMapForGuildButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnProduceForGuildButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildWarDamageButotnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildFlagShipManagementButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildFlagShipOptLogButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDelegateMissionPanelClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDelegateMissonRecordButtonClick;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string MotherShipUIPrefabAssetPath;
        public static string ParamKey_IsImmediate;
        private bool m_isPauseOnPostUpdateView;
        public const string TaskName = "MotherShipUITask";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfosForGuild;
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartMotherShipUITask;
        private static DelegateBridge __Hotfix_ShowMotherShipPanel;
        private static DelegateBridge __Hotfix_HideMotherShipPanel;
        private static DelegateBridge __Hotfix_SendMotherShipGuildPanelReq;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewFuncPanelState;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnDelegateMissionStateUpdate;
        private static DelegateBridge __Hotfix_OnDelegateMissionBalanced;
        private static DelegateBridge __Hotfix_OnBaseRedeployInCurrSpaceStationNtf;
        private static DelegateBridge __Hotfix_OnGetGuildProductionInfoAck;
        private static DelegateBridge __Hotfix_OnGuildOccupiedSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarSimpleInfoAck;
        private static DelegateBridge __Hotfix_OnGuildPurchaseOrderListAck;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnTechPanelButtonClick;
        private static DelegateBridge __Hotfix_OnCrewDormitoryPanelButtonClick;
        private static DelegateBridge __Hotfix_OnItemStoreButtonClick;
        private static DelegateBridge __Hotfix_OnHangarPanelButtonClick;
        private static DelegateBridge __Hotfix_OnBuildingButtonClick;
        private static DelegateBridge __Hotfix_OnCrackPanelButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateMissionFleetItemClick;
        private static DelegateBridge __Hotfix_OnDelegateMissionPanelClick;
        private static DelegateBridge __Hotfix_OnDelegateMissonRecordButtonClick;
        private static DelegateBridge __Hotfix_OnDevelopmentButtonClick;
        private static DelegateBridge __Hotfix_OnGuildStrategyButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapForGuildButtonClick;
        private static DelegateBridge __Hotfix_OnGuildProduceButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagshipManagementButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipOptLogButtonClick;
        private static DelegateBridge __Hotfix_OnGuildModeStoreButtonClick;
        private static DelegateBridge __Hotfix_OnGuildArmamentOrderButtonClick;
        private static DelegateBridge __Hotfix_OnGuildWarDamageReportButtonClick;
        private static DelegateBridge __Hotfix_OnTechPanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnItemStorePanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnCrackPanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnCrewManagerPanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnProducePanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnShipHangarPanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateMissonPanelOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagshipOfflineButtonClick;
        private static DelegateBridge __Hotfix_OnDevelopmentOffLineButtonClick;
        private static DelegateBridge __Hotfix_ShowMotherShipPanelInternal;
        private static DelegateBridge __Hotfix_HideMotherShipPanelInternal;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_UpdateDataCache_GuildProduceLineSort;
        private static DelegateBridge __Hotfix_UpdateDataCache_GuildBuildingList;
        private static DelegateBridge __Hotfix_UpdateGuildModePanels;
        private static DelegateBridge __Hotfix_UpdateGuildProduceInfo;
        private static DelegateBridge __Hotfix_UpdateGuildGalaxy;
        private static DelegateBridge __Hotfix_UpdateGuildArmamentOrderInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFlagshipManagementInfo;
        private static DelegateBridge __Hotfix_IsStayWithMotherShip;
        private static DelegateBridge __Hotfix_CreateMotherShipUserGuideUIAction;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SetAnnouncementTextInfo;
        private static DelegateBridge __Hotfix_StopAnnouncementTextInfo;
        private static DelegateBridge __Hotfix_OnAnnouncementEnd;
        private static DelegateBridge __Hotfix_OnAnnouncementAdd;
        private static DelegateBridge __Hotfix_OnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_get_LbProduce;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCrewDormitoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCrewDormitoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemStoreButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemStoreButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnHangarButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnHangarButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBuildingButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuildingButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCrackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCrackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDevelopmentButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDevelopmentButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateMissionFleetItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateMissionFleetItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildStoreButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildStoreButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildPurchaseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildPurchaseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapForGuildButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapForGuildButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnProduceForGuildButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnProduceForGuildButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildWarDamageButotnClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildWarDamageButotnClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagShipManagementButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagShipManagementButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagShipOptLogButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagShipOptLogButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateMissionPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateMissionPanelClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateMissonRecordButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateMissonRecordButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_SetAnnouceMentTextInfo;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableCrackPanel;
        private static DelegateBridge __Hotfix_UnlockCrackPanel;
        private static DelegateBridge __Hotfix_EnableCrewDormitoryPanel;
        private static DelegateBridge __Hotfix_UnlockCrewDormitoryPanel;
        private static DelegateBridge __Hotfix_EnableProducePanel;
        private static DelegateBridge __Hotfix_UnlockProducePanel;
        private static DelegateBridge __Hotfix_EnableTechPanel;
        private static DelegateBridge __Hotfix_UnlockTechPanel;
        private static DelegateBridge __Hotfix_EnableShipHangarPanel;
        private static DelegateBridge __Hotfix_UnlockShipHangarPanel;
        private static DelegateBridge __Hotfix_EnableItemStorePanel;
        private static DelegateBridge __Hotfix_UnlockItemStorePanel;
        private static DelegateBridge __Hotfix_EnableDelegateMissonPanel;
        private static DelegateBridge __Hotfix_UnlockDelegateMissonPanel;
        private static DelegateBridge __Hotfix_EnableDevelopmentPanel;
        private static DelegateBridge __Hotfix_UnlockDevelopmentPanel;
        private static DelegateBridge __Hotfix_UnlockGuildStrategyMissonPanel;
        private static DelegateBridge __Hotfix_UnlockGuildGalaxyMissonPanel;
        private static DelegateBridge __Hotfix_UnlockGuildProduceMissonPanel;
        private static DelegateBridge __Hotfix_UnlockGuildFlagshipManagementMissonPanel;
        private static DelegateBridge __Hotfix_OnDevelopmentButtonClick_UserGuide;

        public event Action EventOnAnnouncementButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBuildingButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCrackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCrewDormitoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnDelegateMissionFleetItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDelegateMissionPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDelegateMissonRecordButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDevelopmentButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildFlagShipManagementButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildFlagShipOptLogButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildPurchaseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildStoreButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildWarDamageButotnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnHangarButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemStoreButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnProduceForGuildButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStarMapForGuildButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public MotherShipUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateMotherShipUserGuideUIAction()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableCrackPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableCrewDormitoryPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableDelegateMissonPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableDevelopmentPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableItemStorePanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableProducePanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableShipHangarPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableTechPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public static void HideMotherShipPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void HideMotherShipPanelInternal(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementAdd(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInCurrSpaceStationNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuildingButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackPanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrewDormitoryPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrewManagerPanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionBalanced(int rst, List<ItemInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionFleetItemClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionPanelClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionStateUpdate(ulong missionInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissonPanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissonRecordButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmentButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDevelopmentButtonClick_UserGuide(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmentOffLineButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetGuildProductionInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildArmamentOrderButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildFlagShipHangarSimpleInfoAck(int flagShipCount, int hangarSlotCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagshipManagementButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagshipOfflineButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipOptLogButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildModeStoreButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildOccupiedSolarSystemInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildProduceButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildPurchaseOrderListAck()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildStrategyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildWarDamageReportButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStorePanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnProducePanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipHangarPanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapForGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechPanelOfflineButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public static void SendMotherShipGuildPanelReq()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouceMentTextInfo(string msg, float time)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementTextInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowMotherShipPanel(bool isGuildMode, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMotherShipPanelInternal(bool isGuildMode, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static MotherShipUITask StartMotherShipUITask(bool isGuildMode, Action redirectAction, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAnnouncementTextInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockCrackPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockCrewDormitoryPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDelegateMissonPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDevelopmentPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildFlagshipManagementMissonPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildGalaxyMissonPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildProduceMissonPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildStrategyMissonPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockItemStorePanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockProducePanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockShipHangarPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockTechPanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_GuildBuildingList()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_GuildProduceLineSort()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildArmamentOrderInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildFlagshipManagementInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildGalaxy(bool updateCache = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildModePanels()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildProduceInfo(bool updateCache = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewFuncPanelState()
        {
        }

        private LogicBlockGuildClient LbProduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateMotherShipUserGuideUIAction>c__AnonStorey2
        {
            internal UIManager.UIActionQueueItem item;
            internal MotherShipUITask $this;

            internal bool <>m__0() => 
                (this.$this.State == Task.TaskState.Running);

            internal void <>m__1()
            {
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_MotherShipUIIsOpen, new object[0]);
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <HideMotherShipPanelInternal>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal MotherShipUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowMotherShipPanel>c__AnonStorey0
        {
            internal bool isGuildMode;
            internal bool isImmediate;
            internal Action<bool> onEnd;

            internal void <>m__0(bool res)
            {
                (UIManager.Instance.FindUITaskWithName("MotherShipUITask", true) as MotherShipUITask).ShowMotherShipPanelInternal(this.isGuildMode, this.isImmediate, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockCrackPanel>c__AnonStorey3
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockCrewDormitoryPanel>c__AnonStorey4
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockDelegateMissonPanel>c__AnonStorey9
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockDevelopmentPanel>c__AnonStoreyA
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildFlagshipManagementMissonPanel>c__AnonStoreyE
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildGalaxyMissonPanel>c__AnonStoreyC
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildProduceMissonPanel>c__AnonStoreyD
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildStrategyMissonPanel>c__AnonStoreyB
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockItemStorePanel>c__AnonStorey8
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockProducePanel>c__AnonStorey5
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockShipHangarPanel>c__AnonStorey7
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockTechPanel>c__AnonStorey6
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess process, bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsImmediate
        }
    }
}

