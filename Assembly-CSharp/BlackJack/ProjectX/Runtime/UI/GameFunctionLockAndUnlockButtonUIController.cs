﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GameFunctionLockAndUnlockButtonUIController : UIControllerBase
    {
        public SystemFuncType m_systemFunType;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Button GameFunctionLockButton;
        [AutoBind("./ClickCollider", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform GameFunctionLockArea;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnClick;
        private static DelegateBridge __Hotfix_UpdateLockArea;

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLockArea(Vector3 centerLocalPos, RectTransform buttonTransform, Vector3 centerLocalPos2, RectTransform clickTransform)
        {
        }
    }
}

