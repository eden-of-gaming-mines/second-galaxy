﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class PurchaseItemUIController : UIControllerBase
    {
        public int m_itemId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int> OnCountChanged;
        private long m_price4One;
        private GameObject m_icon;
        private int m_itemIndex;
        public int m_count;
        public int m_count4OneShip;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FakeLBStoreItem, Vector3> OnItemDetailOpen;
        private const string Normal = "Normal";
        private const string NoAcquisition = "NoAcquisition";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./DonatePanel/NumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_totalCount;
        [AutoBind("./DonatePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_totalRemoveBtn;
        [AutoBind("./DonatePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_totalAddBtn;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameLabel;
        [AutoBind("./OwnTitleText/OwnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_guildHasCount;
        [AutoBind("./DonatePanel/ItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_cost;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_iconDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnNumberInputChanged;
        private static DelegateBridge __Hotfix_SetInfo_1;
        private static DelegateBridge __Hotfix_SetInfo_0;
        private static DelegateBridge __Hotfix_SetGuildHasCount;
        private static DelegateBridge __Hotfix_SetCount;
        private static DelegateBridge __Hotfix_SetIcon;
        private static DelegateBridge __Hotfix_SetPriceWithCost;
        private static DelegateBridge __Hotfix_AddCount;
        private static DelegateBridge __Hotfix_MinusCount;
        private static DelegateBridge __Hotfix_SetItemDetailOpenEvent;
        private static DelegateBridge __Hotfix_add_OnCountChanged;
        private static DelegateBridge __Hotfix_remove_OnCountChanged;
        private static DelegateBridge __Hotfix_add_OnItemDetailOpen;
        private static DelegateBridge __Hotfix_remove_OnItemDetailOpen;

        public event Action<int, int> OnCountChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<FakeLBStoreItem, Vector3> OnItemDetailOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddCount()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void MinusCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNumberInputChanged(string value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildHasCount(long count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIcon(GameObject icon, Dictionary<string, UnityEngine.Object> resDict, ItemInfo itemInfo, Vector3 tipsPosition)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfo(ItemTypeId typeId, ProjectXPlayerContext context)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfo(ConfigDataAuctionItemInfo info, int count4OneGroup, int groupCount, ProjectXPlayerContext context)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemDetailOpenEvent(Action<FakeLBStoreItem, Vector3> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPriceWithCost(int count)
        {
        }

        [CompilerGenerated]
        private sealed class <SetIcon>c__AnonStorey0
        {
            internal Vector3 tipsPosition;
            internal PurchaseItemUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIControllerBase ctrl)
            {
            }
        }
    }
}

