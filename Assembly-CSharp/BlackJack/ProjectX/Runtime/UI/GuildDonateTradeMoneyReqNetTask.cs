﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildDonateTradeMoneyReqNetTask : NetWorkTransactionTask
    {
        public int m_result;
        private int m_donateTradeMoney;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_GuildDonateTradeMoneyAck;

        [MethodImpl(0x8000)]
        public GuildDonateTradeMoneyReqNetTask(int donateTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        private void GuildDonateTradeMoneyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

