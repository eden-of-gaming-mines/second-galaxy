﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GiftCodeUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<string> EventOnUseGiftCodeClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCanelBtnClick;
        [AutoBind("./InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_codeInputField;
        [AutoBind("./InputField/WarningText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_warningText;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmBtn;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelBtn;
        [AutoBind("./InputField", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_inputFieldState;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rootState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_OnConfirmBtnClick;
        private static DelegateBridge __Hotfix_OnCancelBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnUseGiftCodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnUseGiftCodeClick;
        private static DelegateBridge __Hotfix_add_EventOnCanelBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnCanelBtnClick;

        public event Action EventOnCanelBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string> EventOnUseGiftCodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCancelBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(string warning)
        {
        }
    }
}

