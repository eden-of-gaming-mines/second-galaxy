﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TechTypeToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TechUITypeId>k__BackingField;
        private List<int> m_techIdList;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public Button TechUITypeButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeStateCtrl;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechUITypeNameText;
        [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelectStateCtrl;
        private static DelegateBridge __Hotfix_InitTechUITypeInfo;
        private static DelegateBridge __Hotfix_SetSelectedState;
        private static DelegateBridge __Hotfix_SetUpgradeState;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_get_LBTechClient;
        private static DelegateBridge __Hotfix_set_TechUITypeId;
        private static DelegateBridge __Hotfix_get_TechUITypeId;

        [MethodImpl(0x8000)]
        public void InitTechUITypeInfo(ConfigDataTechUITypeInfo techUITypeInfo, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedState(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpgradeState(bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        private LogicBlockTechClient LBTechClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int TechUITypeId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

