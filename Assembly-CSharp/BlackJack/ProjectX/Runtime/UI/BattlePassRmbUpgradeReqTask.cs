﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BattlePassRmbUpgradeReqTask : NetWorkTransactionTask
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ReqResult>k__BackingField;
        private bool m_IsPackage;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBattlePassRmbUpgradeAck;
        private static DelegateBridge __Hotfix_set_ReqResult;
        private static DelegateBridge __Hotfix_get_ReqResult;

        [MethodImpl(0x8000)]
        public BattlePassRmbUpgradeReqTask(bool isPackage)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBattlePassRmbUpgradeAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int ReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

