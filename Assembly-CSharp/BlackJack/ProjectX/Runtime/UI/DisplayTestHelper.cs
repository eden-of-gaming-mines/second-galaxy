﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class DisplayTestHelper
    {
        public static BlackJack.ProjectX.Runtime.SolarSystem.SolarSystemTask.DisplayPerformance DisplayPerformance;
        public static int m_importantEffectCountLimit;
        public static int m_importantEffectCountLimitDefault;
        public static int[] m_spaceShipDisplayLimitDefine;
        public static int[] m_spaceObjectDisplayLimitDefine;
        public static int[] m_bulletDisplayLimitDefine;
        public static int[] m_laserBulletDisplayLimitDefine;
        public static int[] m_normalBulletDisplayLimitDefine;
        public static int[] m_spaceEffectDisplayLimitDefine;
        public static int[] m_selfLaunchEffectCreateRate;
        public static int[] m_selfTargetEffectCreateRateOnBusy;
        public static int[] m_selfTargetEffectCreateRateOnNormal;
        public static int[] m_displayShipEffectCreateRate;
        public static int[] m_noneDisplayShipEffectCreateRate;
        public static int[] m_spaceShipDisplayLimitDefineDefault;
        public static int[] m_spaceObjectDisplayLimitDefineDefault;
        public static int[] m_bulletDisplayLimitDefineDefault;
        public static int[] m_laserBulletDisplayLimitDefineDefault;
        public static int[] m_spaceEffectDisplayLimitDefineDefault;
        public static int[] m_normalBulletDisplayLimitDefineDefault;
        public static int[] m_selfLaunchEffectCreateRateDefault;
        public static int[] m_selfTargetEffectCreateRateOnBusyDefault;
        public static int[] m_selfTargetEffectCreateRateOnNormalDefault;
        public static int[] m_displayShipEffectCreateRateDefault;
        public static int[] m_noneDisplayShipEffectCreateRateDefault;
        private static DelegateBridge __Hotfix_ApplySetting;

        [MethodImpl(0x8000)]
        public static void ApplySetting()
        {
        }
    }
}

