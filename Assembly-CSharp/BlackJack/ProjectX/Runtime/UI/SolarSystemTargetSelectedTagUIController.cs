﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class SolarSystemTargetSelectedTagUIController : SolarSystemTargetTagBaseUIController
    {
        [AutoBind("./ProgressBarImage/ArmorProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        private SolarSystemShipProgressUIController m_armorProgressBarCtrl;
        [AutoBind("./ProgressBarImage/ShieldProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        private SolarSystemShipProgressUIController m_shieldProgressBarCtrl;
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetNoticeCtrl;
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetPVPRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetPVPCtrl;
        [AutoBind("./NameAndTargetNotice/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./DistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText DistanceText;
        [AutoBind("./ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProgressBarStateCtrl;
        private static DelegateBridge __Hotfix_UpdateTagInfo;
        private static DelegateBridge __Hotfix_SetTargetName;
        private static DelegateBridge __Hotfix_SetProgressBarStateByObjType;
        private static DelegateBridge __Hotfix_SetTargetPvpFlagState;
        private static DelegateBridge __Hotfix_SetTargetNoticeType;

        [MethodImpl(0x8000)]
        public void SetProgressBarStateByObjType(SpaceObjectType objType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetNoticeType(string targetNoticeState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetPvpFlagState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTagInfo(double dist, float shield = 1f, float armor = 1f)
        {
        }
    }
}

