﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnCommonIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCompensationItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CurrencyType> EventOnAddCurrencyBtnClick;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private Dictionary<ShipType, CommonUIStateController> m_shipBtnStateDic;
        private List<LossCompensation> m_lossComps;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/AllButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllShipBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FrigateBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DestroyerBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BattleCruiserBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Battle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BattleBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CruiserBtn;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/AllButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllShipBtnState;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FirgateBtnState;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DestroyerBtnState;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleCruiserBtnState;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Battle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleBtnState;
        [AutoBind("./ItemCategoryToggleGroup/ShipFilterButtons/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CruiserBtnState;
        [AutoBind("./ScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollView;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RootState;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy;
        [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EmptyPanelState;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnCompensationItemClick;
        private static DelegateBridge __Hotfix_OnCompensationFill;
        private static DelegateBridge __Hotfix_OnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnCompensationItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCompensationItemClick;
        private static DelegateBridge __Hotfix_add_EventOnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddCurrencyBtnClick;

        public event Action<CurrencyType> EventOnAddCurrencyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCompensationItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddCurrencyBtnClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompensationFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompensationItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ShipType type, List<LossCompensation> comps, Dictionary<string, UnityEngine.Object> dict)
        {
        }
    }
}

