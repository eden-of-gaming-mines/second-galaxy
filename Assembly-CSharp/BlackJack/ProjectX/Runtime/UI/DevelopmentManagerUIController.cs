﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class DevelopmentManagerUIController : UIControllerBase
    {
        [AutoBind("./TitleBGImage/MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tradeMoneyAddButton;
        [AutoBind("./TitleBGImage/MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyText;
        [AutoBind("./TitleBGImage/MoneyGroup/CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bindMoneyAddButton;
        [AutoBind("./TitleBGImage/MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyText;
        [AutoBind("./TitleBGImage/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_managerStateCtrl;
        [AutoBind("./TitleBGImage/TitleText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_systemDescriptionCtrl;
        private static DelegateBridge __Hotfix_CreateDevelopmentManagerPanelProcess;
        private static DelegateBridge __Hotfix_SetCurrencyValues;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess CreateDevelopmentManagerPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrencyValues(ulong bindMoneyValue, ulong tradeMoneyValue)
        {
        }
    }
}

