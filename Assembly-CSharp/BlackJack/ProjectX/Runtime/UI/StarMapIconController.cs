﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapIconController : UIControllerBase
    {
        private static readonly Vector2 m_iconPos;
        private bool m_arrowDisplayed;
        [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_iconTrans;
        [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_icon;
        [AutoBind("./Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_arrow;
        [AutoBind("./Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_arrowImg;
        private static DelegateBridge __Hotfix_set_ArrowDisplayed;

        public bool ArrowDisplayed
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

