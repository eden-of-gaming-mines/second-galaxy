﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class WarShipLossUITask : UITaskBase
    {
        public const string ParamKeyGuildBattleReportInfo = "GuildBattleReportInfo";
        public const string ParamKeyGuildOpenImmediate = "OpenImmediate";
        public const string ParamKeyRemainLastIntent = "RemainLastIntent";
        private bool m_isTaskInit;
        private GuildBattleReportInfo m_battleReportInfo;
        private bool m_isShowOpenAnimImmediate;
        private bool m_needUpdate;
        private int m_attackerMaxLossShipCount;
        private int m_defenderMaxLossShipCount;
        private List<KeyValuePair<GuildBattleShipLostStatInfo, GuildBattleShipLostStatInfo>> m_shipLosAttackerDefendersList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType> EventOnShipIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint> EventOnGuildNameClick;
        private WarShipLossUIController m_warShipLossUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartWarShipLossUITask;
        private static DelegateBridge __Hotfix_ShowWarShipLossPanel;
        private static DelegateBridge __Hotfix_HideShipLossPanel;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_EnterToOtherTaskFromSimpleInfo;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectLogoResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnShipIconClick;
        private static DelegateBridge __Hotfix_OnItemFilled;
        private static DelegateBridge __Hotfix_OnGuildNameClick;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_ShipLossComparer;
        private static DelegateBridge __Hotfix_add_EventOnShipIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipIconClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildNameClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbGuild;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<uint> EventOnGuildNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType> EventOnShipIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public WarShipLossUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectLogoResForLoad(GuildLogoInfo logoInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void EnterToOtherTaskFromSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void HideShipLossPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNameClick(bool isDefend)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFilled(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipIconClick(UIControllerBase ctrl, bool isLeft, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private int ShipLossComparer(GuildBattleShipLostStatInfo lossA, GuildBattleShipLostStatInfo lossB)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWarShipLossPanel(GuildBattleReportInfo reportInfo, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static WarShipLossUITask StartWarShipLossUITask(Action onResLoadEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideShipLossPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal WarShipLossUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess process, bool res)
            {
            }
        }
    }
}

