﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryListCategoryGroupUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick;
        private bool m_isInit;
        private bool m_isExpand;
        private int m_category;
        private int m_branchStoryCount;
        private bool m_isTriggerToggleEvent;
        private List<BranchStoryListQuestItemUIController> m_questItemCtrlList;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx CategoryToggle;
        [AutoBind("./CategoryTitle/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DownImage;
        [AutoBind("./CategoryTitle/ArrowImage2", AutoBindAttribute.InitState.NotInit, false)]
        public Image UpImage;
        [AutoBind("./CategoryTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CategoryNameText;
        [AutoBind("./QuestListItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_CreateCategoryGroupUI;
        private static DelegateBridge __Hotfix_InitCategoryUI;
        private static DelegateBridge __Hotfix_UpdateQuestList;
        private static DelegateBridge __Hotfix_UpdateRedPoints;
        private static DelegateBridge __Hotfix_ExpandCategoryRoot;
        private static DelegateBridge __Hotfix_UpdateQuestItemSelectState;
        private static DelegateBridge __Hotfix_OnCategoryToggleValueChange;
        private static DelegateBridge __Hotfix_OnQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestItemClick;
        private static DelegateBridge __Hotfix_get_Category;

        public event Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CreateCategoryGroupUI(List<BranchStoryUITask.BranchStoryItemInfo> questList)
        {
        }

        [MethodImpl(0x8000)]
        public void ExpandCategoryRoot(bool expand)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void InitCategoryUI(int category)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCategoryToggleValueChange(bool select)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestItemClick(BranchStoryUITask.BranchStoryItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestItemSelectState(int selectQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestList(int selectBranchId, int category, List<BranchStoryUITask.BranchStoryItemInfo> questList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRedPoints(int selectBranchId, List<BranchStoryUITask.BranchStoryItemInfo> questList)
        {
        }

        public int Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

