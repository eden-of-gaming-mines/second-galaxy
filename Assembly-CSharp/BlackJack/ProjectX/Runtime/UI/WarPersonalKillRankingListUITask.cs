﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WarPersonalKillRankingListUITask : UITaskBase
    {
        public Action<GuildBattlePlayerKillLostStatInfo, UIControllerBase> EventOnPlayerIconClick;
        private bool m_isTaskInit;
        public const string ParamKey_RankingList = "GuildBattleRankingList";
        public static string ParamKey_IsImmediate = "IsImmediate";
        private List<GuildBattlePlayerKillLostStatInfo> m_rankingList;
        private WarPersonalKillRankingListUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "WarPersonalKillRankingListUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildWarRankingListUITask;
        private static DelegateBridge __Hotfix_ShowRankingListPanel;
        private static DelegateBridge __Hotfix_HideRankingListPanel;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_ShowRankingListPanelInternal;
        private static DelegateBridge __Hotfix_HideRankingListPanelInternal;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnPlayerIconClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public WarPersonalKillRankingListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public static void HideRankingListPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void HideRankingListPanelInternal(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerIconClick(GuildBattlePlayerKillLostStatInfo info, UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowRankingListPanel(List<GuildBattlePlayerKillLostStatInfo> rankingList, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRankingListPanelInternal(List<GuildBattlePlayerKillLostStatInfo> rankingList, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static WarPersonalKillRankingListUITask StartGuildWarRankingListUITask(List<GuildBattlePlayerKillLostStatInfo> rankingList, Action onResLoadEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideRankingListPanelInternal>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal WarPersonalKillRankingListUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowRankingListPanel>c__AnonStorey0
        {
            internal List<GuildBattlePlayerKillLostStatInfo> rankingList;
            internal bool isImmediate;
            internal Action<bool> onEnd;

            internal void <>m__0(bool res)
            {
                (UIManager.Instance.FindUITaskWithName("WarPersonalKillRankingListUITask", true) as WarPersonalKillRankingListUITask).ShowRankingListPanelInternal(this.rankingList, this.isImmediate, this.onEnd);
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsImmediate
        }
    }
}

