﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacterClearPropertyPointsPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCancelButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowCtrl;
        [AutoBind("./DetailPanel/IconNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./DetailPanel/SkillPointCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_skillPointCountText;
        [AutoBind("./DetailPanel/ExpendituretTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_rollbackSkillBindMoneyCostText;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_cancelButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetClearItemInfo;
        private static DelegateBridge __Hotfix_SetClearPropertyPointCount;
        private static DelegateBridge __Hotfix_SetClearRollbackSkillBindMoneyCost;
        private static DelegateBridge __Hotfix_CreatPanelAnimtionProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatPanelAnimtionProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetClearItemInfo(string itemName, long count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetClearPropertyPointCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetClearRollbackSkillBindMoneyCost(int cost)
        {
        }
    }
}

