﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildSovereignMaintenanceDesController : UIControllerBase
    {
        [AutoBind("./MaintenanceInfoPanel/BGClick", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeBtn;
        [AutoBind("./MaintenanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo1/TitleGroup/TitleText1/TitleDes", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_levelDes;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo1/Type1Des", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_type1Des;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo1/Type1Des/Value", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_type1Value;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo1/Type2Des", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_type2Des;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo1/Type2Des/Value", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_type2Value;
        [AutoBind("./MaintenanceInfoPanel/FrameImage/BGImage/DetailInfo2/DetailDes", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_detailDes;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_UpdateState;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void Show()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateState(ConfigDataSovereignMaintenanceCostLevelInfo costLevelData, ConfigDataSovereignMaintenanceCostInfo costData)
        {
        }
    }
}

