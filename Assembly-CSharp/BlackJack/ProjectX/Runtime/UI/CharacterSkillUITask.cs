﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CharacterSkillUITask : UITaskBase
    {
        public static string CharacterSkillUITaskMode_SkillOverViewMode;
        public static string CharacterSkillUITaskMode_SkillTree;
        private bool m_skillDetailInfoResComplete;
        private ConfigDataPassiveSkillInfo m_skillInfo;
        private CharacterSkillDetailInfoUITask m_skillDetailInfoUITask;
        private CharacterSkillUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private PropertyCategory m_currSelectCategory;
        private bool m_isUpdatingView;
        private const int m_initWindowFlagIndex = 0;
        private const int m_notUpdateDynamicResFlagIndex = 1;
        private const int m_updateLearnedSkillFlagIndex = 2;
        private bool m_skillTypeToggleEnabled;
        private Dictionary<int, List<ConfigDataPassiveSkillTypeInfo>> m_skillCategoryToSkillTypeListInfoDict;
        private int m_currUISkillType;
        private int m_currSkillTypeLearntSkillLevelCount;
        private List<ConfigDataPassiveSkillInfo> m_currUISkillConfigInfoList;
        private Dictionary<int, int> m_category2TotalSkillLevelCountDict;
        private Dictionary<int, int> m_category2LearntSkillLevelCountDict;
        private Dictionary<int, int> m_skillType2TotalSkillLevelCountDict;
        public int m_characterLevel;
        public ulong m_bindMoney;
        public float m_weaponProperty;
        public float m_defenseProperty;
        public float m_electronicProperty;
        public Dictionary<int, List<ConfigDataPassiveSkillInfo>> m_skillType2CanLearnSkillDic;
        private const string ParamKey_ShowSkillDetailInfo = "ShowSkillDetailInfo";
        public const string TaskName = "CharacterSkillUITask";
        private GuideState m_currGuideState;
        private LBProcessingQuestBase m_userGuideQuestInfo;
        private const string ParamStrinKey_UserGuideQuestInfo = "UserGuideQuestInfo";
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitlizeSkillLevelCountInfo;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_ClearBeforeUpdate;
        private static DelegateBridge __Hotfix_UpdateDataCache_SkillOverViewMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_SkillTreeMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_SkillCanLearn;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_SkillTreeMode;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_SkillOverViewMode;
        private static DelegateBridge __Hotfix_UpdateView_SkillTreeMode;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_GetSkillDetailPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_OnCategoryPanelButtonClick;
        private static DelegateBridge __Hotfix_OnCategoryToggleSelected;
        private static DelegateBridge __Hotfix_OnSkillTypeToggleSelected;
        private static DelegateBridge __Hotfix_OnSkillTypeToggleSelectedImp;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBindMoneyAddButtonClick_SkillDetailPanel;
        private static DelegateBridge __Hotfix_OnCloseButtonClick_SkillDetailPanel;
        private static DelegateBridge __Hotfix_OnLearnSkillChanged_SkillDetailPanel;
        private static DelegateBridge __Hotfix_ClosePanel;
        private static DelegateBridge __Hotfix_OnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_OnSkillTreeNodeClickImp;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_OnSkillDetailPanelResLoadEnd;
        private static DelegateBridge __Hotfix_InitCategory2SkillTypeInfosDict;
        private static DelegateBridge __Hotfix_GetPropertyCategoryDescStr;
        private static DelegateBridge __Hotfix_RefreshLearningSkillInfo;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBSkillClient;
        private static DelegateBridge __Hotfix_StartCharacterSkillUITaskForNotForceGuide;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetCategoryToggleRect;
        private static DelegateBridge __Hotfix_OpenCategoryNode;
        private static DelegateBridge __Hotfix_GetSkillToggleRect;
        private static DelegateBridge __Hotfix_GetSkillTypeToggleRectByIndex;
        private static DelegateBridge __Hotfix_GetSkillNodeRectByIndex;
        private static DelegateBridge __Hotfix_GetSkillNodeCtrlByIndex;
        private static DelegateBridge __Hotfix_ClickSkillTypeToggle;
        private static DelegateBridge __Hotfix_ClickSkillTreeNode;

        [MethodImpl(0x8000)]
        public CharacterSkillUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickSkillTreeNode(ConfigDataPassiveSkillInfo skillInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickSkillTypeToggle(int skillType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ClosePanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectAllDynamicResForLoad_SkillTreeMode(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCategoryToggleRect(PropertyCategory findCategory)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetPropertyCategoryDescStr(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetSkillDetailPanelShowOrHideProcess(ConfigDataPassiveSkillInfo skillInfo, bool isShow, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillTreeNodeUIController GetSkillNodeCtrlByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSkillNodeRectByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSkillToggleRect(int skillTypeConfId)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSkillTypeToggleRectByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitCategory2SkillTypeInfosDict()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitlizeSkillLevelCountInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindMoneyAddButtonClick_SkillDetailPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCategoryPanelButtonClick(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCategoryToggleSelected(CharacterSkillCategoryToggleUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick_SkillDetailPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLearnSkillChanged_SkillDetailPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillDetailPanelResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillTreeNodeClick(ConfigDataPassiveSkillInfo skillInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillTreeNodeClickImp(ConfigDataPassiveSkillInfo skillInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSkillTypeToggleSelected(int skillType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSkillTypeToggleSelectedImp(int skillType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenCategoryNode(PropertyCategory findCategory, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshLearningSkillInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterSkillUITask StartCharacterSkillUITaskForNotForceGuide(UIIntent returnToIntent, LBProcessingQuestBase questInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_ClearBeforeUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_SkillCanLearn()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_SkillOverViewMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_SkillTreeMode()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_SkillOverViewMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_SkillTreeMode()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterSkillClient LBSkillClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ClickSkillTypeToggle>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal CharacterSkillUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_skillTypeToggleEnabled = true;
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ClosePanel>c__AnonStorey1
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetSkillDetailPanelShowOrHideProcess>c__AnonStorey0
        {
            internal ConfigDataPassiveSkillInfo skillInfo;
            internal bool immediate;
            internal CharacterSkillUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_skillDetailInfoUITask.ShowSkillDetailPanel(this.skillInfo, this.immediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_skillDetailInfoUITask.HideSkillDetailPanel(this.immediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OpenCategoryNode>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal CharacterSkillUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(true, true);
                this.onEnd(true);
            }
        }

        private enum GuideState
        {
            None,
            Start,
            ChooseCategory,
            ChooseType
        }
    }
}

