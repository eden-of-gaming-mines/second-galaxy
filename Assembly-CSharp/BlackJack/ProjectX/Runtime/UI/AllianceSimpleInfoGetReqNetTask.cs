﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AllianceSimpleInfoGetReqNetTask : NetWorkTransactionTask
    {
        private List<uint> m_allianceIdList;
        private List<uint> m_needReqIdList;
        private int m_reqIndex;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAllianceSimpleInfoGetAck;
        private static DelegateBridge __Hotfix_GetReqListByIndex;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public AllianceSimpleInfoGetReqNetTask(List<uint> allianceIdList)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetReqListByIndex(int index, out List<uint> needReqIdList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceSimpleInfoGetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

