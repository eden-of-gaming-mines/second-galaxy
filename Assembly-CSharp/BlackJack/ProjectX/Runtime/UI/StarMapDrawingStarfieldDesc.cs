﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using UnityEngine;

    public class StarMapDrawingStarfieldDesc : MonoBehaviour
    {
        [Header("星图的点和线绘制相关的配置")]
        public StarfieldStarMapDrawingConfig m_starMapDrawingConfig;
        [Header("星图缩略导航线相关配置")]
        public StarfieldStarMapSimpleNavigationConfig m_starMapNavigationConfig;
        [Header("星图中的名称文本框相关配置")]
        public StarfieldStarMapNameTextConfig m_starMapNameTextConfig;
        [Header("星图星域色块配置")]
        public StarfieldStarMapColorBlockConfig m_starMapColorBlockConfig;
        [Header("星图星域描边配置")]
        public StarfieldStarMapBorderLineConfig m_starMapBorderLineConfig;
        [Header("玩家势力色块配置")]
        public StarfieldStarMapGuildColorConfig m_starMapGuildColorConfig;
        [Header("货船路线颜色配置")]
        public Color[] m_starMapTradeShipPathColor;
    }
}

