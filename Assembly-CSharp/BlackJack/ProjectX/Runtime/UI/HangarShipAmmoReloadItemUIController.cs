﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipAmmoReloadItemUIController : UIControllerBase
    {
        private long m_depotCount;
        private int m_ammoClipSize;
        private int m_ammoReloadNumber;
        private int m_wantedAmmoNumber;
        private int m_itemIndex;
        public Action<long, int> EventOnAmmoCountChange;
        private ShipWeaponEquipSlotItemUIController m_highSlotCtrl;
        private CommonItemIconUIController m_ammoCtrl;
        private const string ItemAssetName = "SlotUIPrefab";
        private const string AmmoAssetName = "CommomItemUIPrefab";
        private const string UIStateNoAmmo = "NoAmmo";
        private const string UIStateHaveAmmo = "HaveAmmo";
        private const string UIStateCanNotLoadAmmo = "CanNotAdd";
        private const float LowAmmoValue = 0.3f;
        private bool m_isInNoRemainAmmoAnim;
        [AutoBind("./AmmoItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./AmmoItem/EquipedEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_effectedEffectStateCtrl;
        [AutoBind("./AmmoItem/LowAmmoEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lowAmmoEffectStateCtrl;
        [AutoBind("./AmmoInfo/DepotAmmoNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_depotAmmoNumberTextStateCtrl;
        [AutoBind("./AmmoItem/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AmmoItemDummy;
        [AutoBind("./SlotItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform SlotItemDummy;
        [AutoBind("./AmmoInfo/AmmoNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoNameText;
        [AutoBind("./AmmoInfo/DepotAmmoNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DepotAmmoNumberText;
        [AutoBind("./AmmoInfo/ProgressBar/AmmoNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoNumberText;
        [AutoBind("./AmmoInfo/Slider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider AmmoCountSlider;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Button AmmoReloadItemButton;
        [AutoBind("./AmmoItem", AutoBindAttribute.InitState.NotInit, false)]
        public Button AmmoItemButton;
        [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button AddButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateAmmoReloadItem;
        private static DelegateBridge __Hotfix_UpdateAmmoItem;
        private static DelegateBridge __Hotfix_ClearData;
        private static DelegateBridge __Hotfix_SetLowAmmoEffectState;
        private static DelegateBridge __Hotfix_ShowEquipedEffect;
        private static DelegateBridge __Hotfix_OnSliderValueChange;
        private static DelegateBridge __Hotfix_SetDepotCount;
        private static DelegateBridge __Hotfix_ChangeValue;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;
        private static DelegateBridge __Hotfix_get_WantedAmmoNumber;
        private static DelegateBridge __Hotfix_set_WantedAmmoNumber;

        [MethodImpl(0x8000)]
        public int ChangeValue()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSliderValueChange(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDepotCount(long value)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLowAmmoEffectState(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEquipedEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAmmoItem(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticPlayerShip staticShip, int ammoReloadNumber, long ammoTotalNumber, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAmmoReloadItem(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticPlayerShip staticShip, int ammoReloadNumber, long ammoTotalNumber, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int WantedAmmoNumber
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

