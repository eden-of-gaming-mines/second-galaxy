﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainRetireUIController : UIControllerBase
    {
        private List<CaptainFeatInfoItemUIController> m_featItemList;
        public CaptainFeatInfoPanelUIController m_captainFeatInfoPanelCtrl;
        public CommonItemIconUIController m_commonItemIconCtrl;
        private const string ChooseFeatState_DirectRetire = "DirectRetire";
        private const string ChooseFeatState_KeepTalent = "Normal";
        private const string ChooseFeatState_ChooseTalent = "ChooseTalent";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCaptainFeatItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCaptainFeatBookClick;
        public bool m_canRemainFeatBook;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./ChooseFeatRoot/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./FeatInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatPanelState;
        [AutoBind("./Text/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./Text/SubRankText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainSubRankText;
        [AutoBind("./ProfessionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIconImage;
        [AutoBind("./Text/ProfessionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProfessionText;
        [AutoBind("./Text/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelNumberText;
        [AutoBind("./Text/ExpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainExpText;
        [AutoBind("./ChooseFeatRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ChooseFeatRoot;
        [AutoBind("./ChooseFeatRoot/FeatGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainFeatGroup;
        [AutoBind("./ChooseFeatRoot/AddAndRemove/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FeatBookAddButton;
        [AutoBind("./ChooseFeatRoot/AddAndRemove/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FeatBookRemoveButton;
        [AutoBind("./ChooseFeatRoot/AddAndRemove/FeatBookCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatBookCountText;
        [AutoBind("./ChooseFeatRoot/AddAndRemove/FeatRemainingText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatRemainingProbText;
        [AutoBind("./ChooseFeatRoot/EmptyBookDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EmptyBookDummy;
        [AutoBind("./FeatInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatInfoDummy;
        [AutoBind("./ItemSimpleInfoUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoUIPrefabDummy;
        [AutoBind("./RetairePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RetireConfirmPanel;
        [AutoBind("./RetairePanel/RetireConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RetireConfirmButton;
        [AutoBind("./RetairePanel/RetireConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RetireCancelButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateCaptainRetireInfo;
        private static DelegateBridge __Hotfix_SetChooseFeatState;
        private static DelegateBridge __Hotfix_UpdateFeatItemSelectState;
        private static DelegateBridge __Hotfix_UpdateFeatRemainingProbability;
        private static DelegateBridge __Hotfix_UpdateFeatInfoPanel;
        private static DelegateBridge __Hotfix_UpdateRetireConfirmPanel;
        private static DelegateBridge __Hotfix_UpdateEmptyFeatBookCount;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelDummyPos;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_OnBookItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainFeatBookClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainFeatBookClick;
        private static DelegateBridge __Hotfix_get_CanRemainFeatBook;

        public event Action<UIControllerBase> EventOnCaptainFeatBookClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainFeatItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBookItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainFeatItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChooseFeatState(ChooseFeatState chooseState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainRetireInfo(LBStaticHiredCaptain selCaptain, int emptyBookCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEmptyFeatBookCount(int bookCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFeatInfoPanel(LBNpcCaptainFeats feat, bool isInitFeat, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFeatItemSelectState(int idx, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFeatRemainingProbability(double probVal)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRetireConfirmPanel(bool isShow)
        {
        }

        public bool CanRemainFeatBook
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum ChooseFeatState
        {
            NotRemainFeat,
            NoFeatSelected,
            FeatSelected
        }
    }
}

