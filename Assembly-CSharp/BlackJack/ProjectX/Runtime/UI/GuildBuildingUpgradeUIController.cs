﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBuildingUpgradeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCostItemClick;
        private int m_costItemId;
        private CommonItemIconUIController m_upgradeCostItemCtrl;
        private List<GuildBuildingUpgradeEffectItemUIController> m_upgradeEffetItemList;
        private const int MaxUpgradeItemCount = 3;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/UpgradeEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform UpgradeEffectRoot;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/CostGroup/CostText/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform UpgradeCostItemDummy;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/LevelInfo/NameText/NumberRightText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelInfoNumberRightText;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/LevelInfo/NameText/NumberLeftText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelInfoNumberLeftText;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/LevelInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelInfoNameText;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/CostGroup/CostText/CostNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CostNumberText;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/UpgradeTimeGroup/UpgradeTimeTitleText/UpgradeTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeTimeText;
        [AutoBind("./ContentGroup/BuildingIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BuildingIconImage;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/ConditionGroup/ConditionTitleText/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ConditionText;
        [AutoBind("./ContentGroup/Buttons/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroupButton;
        [AutoBind("./ContentGroup/GuildBuildingUpgradeGroup/ConditionGroup/ConditionTitleText/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_conditionStateCtrl;
        [AutoBind("./BGImages/TitleText/TipsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipWindowStateCtrl;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipWindowCloseButton;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingUpgradePanel;
        private static DelegateBridge __Hotfix_GetTipWindowUIProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_PrepareUpgradeGainItems;
        private static DelegateBridge __Hotfix_UpdateUpgradeEffectItem;
        private static DelegateBridge __Hotfix_OnCostItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCostItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCostItemClick;

        public event Action<int> EventOnCostItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCostItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareUpgradeGainItems(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingUpgradePanel(GuildBuildingInfo guildBuildingInfo, int solarSystemId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUpgradeEffectItem(GuildBuildingUpgradeEffectItemUIController itemCtrl, GuildBuildingLevelDetailInfoGuildLogicEffectList currLevelEffect, GuildBuildingLevelDetailInfoGuildLogicEffectList nextLevelEffect)
        {
        }
    }
}

