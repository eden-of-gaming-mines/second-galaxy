﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildLogoEditorUITask : UITaskBase
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const int BGEveryPageCount = 10;
        private const int PaintingEveryPageCount = 10;
        private int m_curBgPage;
        private int m_curPaintingPage;
        private static int m_curSelectBgId;
        private static int m_curSelectPaintingId;
        private static int m_curSelectBgColorId;
        private static int m_curSelectPaintingColorId;
        private string m_bgResKey;
        private string m_paintingResKey;
        private GuildLogoEditorUIController m_mainCtrl;
        private GuildLogoController m_iconCtrl;
        private int m_bgCount;
        private int m_paintingCount;
        private const string LogoInfo = "LogoInfo";
        private const string CostInfo = "CostInfo";
        private const string OnSubmit = "OnSubmit";
        public const string TaskName = "GuildLogoEditorUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_InitData_0;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_SetBgPreviewColor;
        private static DelegateBridge __Hotfix_SetPaintingPreviewColor;
        private static DelegateBridge __Hotfix_CloseEditorPanel;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBgSubtractionButtonClick;
        private static DelegateBridge __Hotfix_OnBgAddButtonClick;
        private static DelegateBridge __Hotfix_OnPaintingSubtractionButtonClick;
        private static DelegateBridge __Hotfix_OnPaintingAddButtonClick;
        private static DelegateBridge __Hotfix_OnSubmitButtonClick;
        private static DelegateBridge __Hotfix_OnBgItemClick;
        private static DelegateBridge __Hotfix_OnPaintingItemClick;
        private static DelegateBridge __Hotfix_OnBgColorItemClick;
        private static DelegateBridge __Hotfix_OnPaintingColorItemClick;
        private static DelegateBridge __Hotfix_InitData_1;
        private static DelegateBridge __Hotfix_CollectGuildIconBGRes;
        private static DelegateBridge __Hotfix_CollectGuildIconPaintingRes;
        private static DelegateBridge __Hotfix_UpdateBgPageData;
        private static DelegateBridge __Hotfix_UpdatePaintingData;
        private static DelegateBridge __Hotfix_get_BgPageCount;
        private static DelegateBridge __Hotfix_get_PaintingPageCount;
        private static DelegateBridge __Hotfix_SetBgAndPaintingCount;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_CurrSelectBgId;
        private static DelegateBridge __Hotfix_get_CurSelectPaintingId;
        private static DelegateBridge __Hotfix_get_CurSelectBgColorId;
        private static DelegateBridge __Hotfix_get_CurSelectPaintingColorId;

        [MethodImpl(0x8000)]
        public GuildLogoEditorUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseEditorPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectGuildIconBGRes(List<string> resList, LogoHelper.LogoUsageType type)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectGuildIconPaintingRes(List<string> resList, LogoHelper.LogoUsageType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public static void InitData()
        {
        }

        [MethodImpl(0x8000)]
        private void InitData(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgColorItemClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgItemClick(int id, string resKey)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgSubtractionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingColorItemClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingItemClick(int id, string resKey)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingSubtractionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubmitButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetBgAndPaintingCount(LogoHelper.LogoUsageType type)
        {
        }

        [MethodImpl(0x8000)]
        private void SetBgPreviewColor()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPaintingPreviewColor()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent returnToIntent, BlackJack.ProjectX.Runtime.UI.LogoHelper.LogoInfo logoInfo, LogoHelper.CostButtonInfo cost = null, Action onSubmit = null, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private List<GuildLogoEditorUIController.AssetWithId> UpdateBgPageData(LogoHelper.LogoUsageType type, ref int curPage)
        {
        }

        [MethodImpl(0x8000)]
        private List<GuildLogoEditorUIController.AssetWithId> UpdatePaintingData(LogoHelper.LogoUsageType type, ref int curPage)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private int BgPageCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private int PaintingPageCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static int CurrSelectBgId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static int CurSelectPaintingId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static int CurSelectBgColorId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static int CurSelectPaintingColorId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

