﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AllianceCreateUITask : UITaskBase
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private AllianceCreateUIController m_mainCtrl;
        private GuildLogoController m_iconController;
        private readonly LogoHelper.LogoInfo m_logoInfo;
        private const string ModeKey = "Mode";
        private const string LanguageKey = "Language";
        public const string TaskName = "AllianceCreateUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UnregisterEvents;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnModeChanged;
        private static DelegateBridge __Hotfix_OnItemFill;
        private static DelegateBridge __Hotfix_OpenAllianceDetail;
        private static DelegateBridge __Hotfix_OnAcceptInvitation;
        private static DelegateBridge __Hotfix_OnDeclineInvitation;
        private static DelegateBridge __Hotfix_OnCreateAllianceMaskButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnRegionChanged;
        private static DelegateBridge __Hotfix_OnLogoEditButtonClick;
        private static DelegateBridge __Hotfix_OnCreateGuildButtonClick;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_CloseAndOpenAllianceInfo;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public AllianceCreateUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void Close()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseAndOpenAllianceInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAcceptInvitation(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateAllianceMaskButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeclineInvitation(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFill(AllianceSimpleItemController ctrl, GuildAllianceInviteInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogoEditButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnModeChanged(int mode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRegionChanged(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OpenAllianceDetail(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent prevIntent, Action<bool> onPipeLineEnd = null, AllianceCreateUIController.Mode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CloseAndOpenAllianceInfo>c__AnonStorey3
        {
            internal UIIntentReturnable intent;
            internal AllianceCreateUITask $this;

            internal void <>m__0(AllianceInfo info)
            {
                AllianceInfoUITask.StartTask(info, this.intent.PrevTaskIntent, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnCreateGuildButtonClick>c__AnonStorey2
        {
            internal string allianceName;
            internal AllianceCreateUITask $this;

            internal bool <>m__0(string s) => 
                (this.allianceName[0] != ' ');

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_AllianceCreated, false, new object[0]);
                    this.$this.CloseAndOpenAllianceInfo();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(Task task)
            {
                int num1;
                GuildCurrencyInfoReqNetTask task2 = task as GuildCurrencyInfoReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    num1 = 0;
                }
                else
                {
                    num1 = (int) (task2.m_result == 0);
                }
                this.onPrepareEnd((bool) num1);
            }
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal Action<bool> onPipeLineEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

