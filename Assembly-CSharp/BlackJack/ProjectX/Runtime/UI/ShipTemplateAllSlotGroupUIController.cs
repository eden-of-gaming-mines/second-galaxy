﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipTemplateAllSlotGroupUIController : CommonMenuItemUIController
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, ShipEquipSlotType, int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, NpcShopItemType, int> EventOnItemBuyClick;
        private Dictionary<string, UnityEngine.Object> m_assetDict;
        private List<ShipTemplateSlotGroupUIController> m_highSlotGroupCtrlList;
        private List<ShipTemplateSlotGroupUIController> m_middleSlotGroupCtrlList;
        private List<ShipTemplateSlotGroupUIController> m_lowSlotGroupCtrlList;
        private List<FakeLBStoreItem> m_equipedItemInShip;
        private List<FakeLBStoreItem> m_needItemInTemplete;
        private const string PoolName = "WeaponEquipSlotGroupItem";
        [AutoBind("./UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform UnusedItemRoot;
        [AutoBind("./Viewport/SlotGroupPanel/HighSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform HighSlotGroupRoot;
        [AutoBind("./Viewport/SlotGroupPanel/MiddleSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform MiddleSlotGroupRoot;
        [AutoBind("./Viewport/SlotGroupPanel/LowSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform LowSlotGroupRoot;
        [AutoBind("./UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool EasyPool;
        private static DelegateBridge __Hotfix_UpdateShipTemplateWeaponEquipGroup;
        private static DelegateBridge __Hotfix_SetEquipedItemList;
        private static DelegateBridge __Hotfix_SetNeedEquipedItemList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateHighSlotGroup;
        private static DelegateBridge __Hotfix_UpdateMiddleLowSlotGroup;
        private static DelegateBridge __Hotfix_FillHighSlotGroup;
        private static DelegateBridge __Hotfix_FillMiddleLowSlotGroup;
        private static DelegateBridge __Hotfix_CheckIsNeedByItemFromMall;
        private static DelegateBridge __Hotfix_GetEquipedItemCount;
        private static DelegateBridge __Hotfix_GetTotalNeedItemCount;
        private static DelegateBridge __Hotfix_PrepareSlotGroupCtrlList;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemBuyClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemBuyClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemBuyClick;

        public event Action<UIControllerBase, NpcShopItemType, int> EventOnItemBuyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, ShipEquipSlotType, int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsNeedByItemFromMall(FakeLBStoreItem item, int totalNeedCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void FillHighSlotGroup(ShipTemplateSlotGroupUIController highSlotCtrl, ConfHighSlotInfo highSlotInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void FillMiddleLowSlotGroup(ShipTemplateSlotGroupUIController slotCtrl, int itemConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetEquipedItemCount(FakeLBStoreItem item)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetTotalNeedItemCount(FakeLBStoreItem item)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemBuyClick(UIControllerBase ctrl, NpcShopItemType shopType, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl, ShipEquipSlotType slotType, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPoolObjectCreated(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareSlotGroupCtrlList(List<ShipTemplateSlotGroupUIController> ctrlList, int needCount, Transform ctrlRoot)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEquipedItemList(List<FakeLBStoreItem> equipedItemList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNeedEquipedItemList(List<FakeLBStoreItem> equipedItemList)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateHighSlotGroup(List<ConfHighSlotInfo> highSlotList)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateMiddleLowSlotGroup(List<int> slotList, List<ShipTemplateSlotGroupUIController> ctrlList, Transform ctrlRoot, ShipEquipSlotType slotType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipTemplateWeaponEquipGroup(ShipCustomTemplateInfo shipTempalteInfo, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

