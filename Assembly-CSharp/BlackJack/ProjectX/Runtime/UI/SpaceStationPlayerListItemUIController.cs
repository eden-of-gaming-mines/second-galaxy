﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SpaceStationPlayerListItemUIController : UIControllerBase
    {
        public CommonCaptainIconUIController m_captainIconCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LvText;
        [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNameText;
        [AutoBind("./GuildCodeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildCodeNameText;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./CaptainIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform CaptainIconDummy;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(PlayerSimplestInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

