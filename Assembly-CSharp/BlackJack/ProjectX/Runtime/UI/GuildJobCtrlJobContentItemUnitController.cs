﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildJobCtrlJobContentItemUnitController : UIControllerBase
    {
        public bool m_isTranserLeader;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildJobType> EventOnBtnClick;
        public GuildJobType m_job;
        public bool m_canClickBtn;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_button;
        [AutoBind("./OtherTimeImage/CountDownText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_countDown;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_add_EventOnBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnBtnClick;

        public event Action<GuildJobType> EventOnBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(GuildJobType job)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(string stateName, bool isTransferLeader, bool canClickBtn)
        {
        }
    }
}

