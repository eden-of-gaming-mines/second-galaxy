﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CancelQuestReqNetTask : NetWorkTransactionTask
    {
        public int m_questInstanceId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <QuestCancelResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnQuestCancelAck;
        private static DelegateBridge __Hotfix_set_QuestCancelResult;
        private static DelegateBridge __Hotfix_get_QuestCancelResult;

        [MethodImpl(0x8000)]
        public CancelQuestReqNetTask(int questInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCancelAck(int result, int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int QuestCancelResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

