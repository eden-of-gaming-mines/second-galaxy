﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class DrivingToggleListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GrandFaction> EventOnToggleSelected;
        public List<DrivingToggleItemUIController> m_ToggleList;
        [AutoBind("./Viewport/Content/GrandFactionToggle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ToggleItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateAllToggles;
        private static DelegateBridge __Hotfix_SetProcessBar;
        private static DelegateBridge __Hotfix_ResetToggleSelected;
        private static DelegateBridge __Hotfix_OnToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnToggleSelected;

        public event Action<GrandFaction> EventOnToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CreateAllToggles(List<GrandFaction> grandFactionList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnToggleSelected(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetToggleSelected(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProcessBar(List<float> valueList)
        {
        }
    }
}

