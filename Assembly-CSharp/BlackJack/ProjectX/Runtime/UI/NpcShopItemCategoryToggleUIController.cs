﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopItemCategoryToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopItemCategory> EventOnItemCategoryButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemTypeToggleSelected;
        [AutoBind("./ItemCategoryInfo/ToggleImageDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_itemCategoryIcon;
        [AutoBind("./ItemCategoryInfo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemCategoryButton;
        [AutoBind("./ItemCategoryInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemTypeTogglesStateCtrl;
        [AutoBind("./ItemCategoryInfo/CategoryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_categoryNameText;
        [AutoBind("./ItemTypeToggles", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemTypeToggleRootTrans;
        [AutoBind("./ItemCategoryInfo/MumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemCountText;
        [AutoBind("./ItemCategoryInfo/NumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_itemCountBgImage;
        public NpcShopItemCategory m_shopItemCategory;
        public bool m_categoryIsOpen;
        public NpcShopItemType m_selectedItemType;
        private List<NpcShopItemTypeToggleUIController> m_shopItemTypeToggleList;
        private ToggleGroup m_currToggleGroup;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitItemTypeToggleGroupToCloseState;
        private static DelegateBridge __Hotfix_SetToggleGroup;
        private static DelegateBridge __Hotfix_SetToggleIcon;
        private static DelegateBridge __Hotfix_SetShopItemCategory;
        private static DelegateBridge __Hotfix_GetItemToggleUIController;
        private static DelegateBridge __Hotfix_UpdateItemTypeToggles;
        private static DelegateBridge __Hotfix_UpdateCategoryItemCountInfo;
        private static DelegateBridge __Hotfix_SwitchItemTypeTogglesState;
        private static DelegateBridge __Hotfix_CloseTypeToggleItemList;
        private static DelegateBridge __Hotfix_ClearAllTypeToggleSelectState;
        private static DelegateBridge __Hotfix_SelectAppointTypeToggle;
        private static DelegateBridge __Hotfix_IsContainSelectedItemType;
        private static DelegateBridge __Hotfix_SetCategoryState;
        private static DelegateBridge __Hotfix_SelectFirstTypeToggle;
        private static DelegateBridge __Hotfix_TriggerButtonClickEvent;
        private static DelegateBridge __Hotfix_IsSelected;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnShopItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnItemTypeToggleSelected;

        public event Action<NpcShopItemCategory> EventOnItemCategoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemTypeToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllTypeToggleSelectState()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseTypeToggleItemList()
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemTypeToggleUIController GetItemToggleUIController(int type)
        {
        }

        [MethodImpl(0x8000)]
        public void InitItemTypeToggleGroupToCloseState(bool isClearSelected = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsContainSelectedItemType()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSelected()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCategoryButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopItemTypeToggleSelected(int itemType)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectAppointTypeToggle(NpcShopItemType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectFirstTypeToggle()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCategoryState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetShopItemCategory(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleGroup(ToggleGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleIcon(Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchItemTypeTogglesState()
        {
        }

        [MethodImpl(0x8000)]
        public void TriggerButtonClickEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCategoryItemCountInfo(bool isShow, long count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemTypeToggles(List<int> itemTypeList)
        {
        }
    }
}

