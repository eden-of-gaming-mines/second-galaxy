﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class MsgBoxForEntryUIController : MsgBoxUIController
    {
        private static DelegateBridge __Hotfix_IsAutoInitLocalized;

        [MethodImpl(0x8000)]
        protected override bool IsAutoInitLocalized()
        {
            DelegateBridge bridge = __Hotfix_IsAutoInitLocalized;
            return ((bridge != null) && bridge.__Gen_Delegate_Imp9(this));
        }
    }
}

