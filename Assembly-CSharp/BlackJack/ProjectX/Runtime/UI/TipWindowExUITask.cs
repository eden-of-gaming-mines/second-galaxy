﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class TipWindowExUITask : UITaskBase
    {
        private const string NormalMode = "NormalMode";
        private const string IntentKey = "CommonPrompt";
        private readonly LinkedList<CommonPromptData> m_promptInfoList;
        private readonly LinkedList<CommonPromptData> m_remainPromotInfoList;
        private const float TickIntervalTime = 0.3f;
        private float m_lastTime;
        private CommonPromptData m_promptData;
        private TipWindowExUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "TipWindowExUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_PushData;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_ClearMsg;
        private static DelegateBridge __Hotfix_ShowTipWindowEx;
        private static DelegateBridge __Hotfix_BringTipToTop;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public TipWindowExUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringTipToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearMsg()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void PushData(CommonPromptData data)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowTipWindowEx(string info, PromptType promptType = 0, bool isImportant = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private class CommonPromptData
        {
            public readonly string m_info;
            public readonly TipWindowExUITask.PromptType m_promptType;
            public readonly bool m_isImportant;
            private static DelegateBridge _c__Hotfix_ctor;

            public CommonPromptData(string info, TipWindowExUITask.PromptType promptType, bool isImportant)
            {
                this.m_info = info;
                this.m_promptType = promptType;
                this.m_isImportant = isImportant;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.InvokeSessionStart();
                    bridge.InParam<TipWindowExUITask.CommonPromptData>(this);
                    bridge.InParam<string>(info);
                    bridge.InParam<TipWindowExUITask.PromptType>(promptType);
                    bridge.InParam<bool>(isImportant);
                    bridge.Invoke(0);
                    bridge.InvokeSessionEnd();
                }
            }
        }

        public enum PromptType
        {
            Normal
        }
    }
}

