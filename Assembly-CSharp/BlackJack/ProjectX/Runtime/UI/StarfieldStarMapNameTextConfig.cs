﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapNameTextConfig
    {
        [Header("星图中关于显示恒星系名字的配置")]
        public StarfieldStarMapSolarSystemNameConfig m_starMapSolarSystemNameConfig;
        [Header("星图中关于显示星座名字的配置")]
        public StarfieldStarMapStargroupNameConfig m_starMapStargroupNameConfig;
        [Header("星图中关于显示星域名字的配置")]
        public StarfieldStarMapStarfieldNameConfig m_starMapStarfieldNameConfig;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

