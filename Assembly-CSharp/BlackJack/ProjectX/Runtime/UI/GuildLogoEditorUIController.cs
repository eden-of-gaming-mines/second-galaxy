﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildLogoEditorUIController : UIControllerBase
    {
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ContentGroup/ChooseBgGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_bgItemRoot;
        [AutoBind("./ContentGroup/ChooseDesignGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_paintingItemRoot;
        [AutoBind("./ContentGroup/ToggleGroup/BGChooseColorGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_bgColorItemRoot;
        [AutoBind("./ContentGroup/ToggleGroup/PaintingChooseColorGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_paintingColorItemRoot;
        [AutoBind("./ContentGroup/ChooseBgGroup/TitleNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bgCountText;
        [AutoBind("./ContentGroup/ChooseDesignGroup/TitleNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_paintingCountText;
        [AutoBind("./ContentGroup/ChooseBgGroup/DirectionLeftImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgSubtractionButton;
        [AutoBind("./ContentGroup/ChooseBgGroup/DirectionRightImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgAddButton;
        [AutoBind("./ContentGroup/ChooseDesignGroup/DirectionLeftImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_paintingSubtractionButton;
        [AutoBind("./ContentGroup/ChooseDesignGroup/DirectionRightImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_paintingAddButton;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_submitButtonStateCtrl;
        [AutoBind("./ContentGroup/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_submitButton;
        [AutoBind("./ContentGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./ContentGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        [AutoBind("./ContentGroup/ConfirmButton/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_confirmCostText;
        [AutoBind("./ContentGroup/ConfirmButton/ConfirmButton/CostText/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_confirmCostLogo;
        public Action<int, string> m_eventOnBgItemClick;
        public Action<int, string> m_eventOnPaintingItemClick;
        public Action<int> m_eventOnBgColorItemClick;
        public Action<int> m_eventOnPaintingColorItemClick;
        private readonly List<Transform> m_bgItemList;
        private readonly List<Transform> m_paintingItemList;
        private readonly List<TransformWithId> m_bgColorItemList;
        private readonly List<TransformWithId> m_paintingColorItemList;
        private static DelegateBridge __Hotfix_SetBgItems;
        private static DelegateBridge __Hotfix_SetPaintingItems;
        private static DelegateBridge __Hotfix_SetBgColorItems;
        private static DelegateBridge __Hotfix_SetPaintingColorItems;
        private static DelegateBridge __Hotfix_SetSelectBgColor;
        private static DelegateBridge __Hotfix_SetSelectPaintingColor;
        private static DelegateBridge __Hotfix_SetButtonMode;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetColorItems;
        private static DelegateBridge __Hotfix_OnBgItemClick;
        private static DelegateBridge __Hotfix_OnPaintingItemClick;
        private static DelegateBridge __Hotfix_OnBgColorItemClick;
        private static DelegateBridge __Hotfix_OnPaintingColorItemClick;
        private static DelegateBridge __Hotfix_get_PlayerContext;

        [MethodImpl(0x8000)]
        private void OnBgColorItemClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgItemClick(int id, string resKey)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingColorItemClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPaintingItemClick(int id, string resKey)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBgColorItems()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBgItems(int curSelectId, int curBgPage, int bgPageCount, List<AssetWithId> bgConfigList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonMode(LogoHelper.CostButtonInfo buttonInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetColorItems(List<int> colorIdList, List<string> colorList, List<TransformWithId> itemList, Action<int> onItemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPaintingColorItems()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPaintingItems(int curSelectId, int curPaintingPage, int paintingPageCount, List<AssetWithId> paintingConfigList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectBgColor(int id)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectPaintingColor(int id)
        {
        }

        private static ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetBgItems>c__AnonStorey0
        {
            internal int id;
            internal string resKey;
            internal GuildLogoEditorUIController $this;

            internal void <>m__0()
            {
                this.$this.OnBgItemClick(this.id, this.resKey);
            }
        }

        [CompilerGenerated]
        private sealed class <SetColorItems>c__AnonStorey2
        {
            internal Action<int> onItemClick;
        }

        [CompilerGenerated]
        private sealed class <SetColorItems>c__AnonStorey3
        {
            internal int id;
            internal GuildLogoEditorUIController.<SetColorItems>c__AnonStorey2 <>f__ref$2;

            internal void <>m__0()
            {
                if (this.<>f__ref$2.onItemClick != null)
                {
                    this.<>f__ref$2.onItemClick(this.id);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetPaintingItems>c__AnonStorey1
        {
            internal int id;
            internal string resKey;
            internal GuildLogoEditorUIController $this;

            internal void <>m__0()
            {
                this.$this.OnPaintingItemClick(this.id, this.resKey);
            }
        }

        public class AssetWithId
        {
            public string m_assetPath;
            public int m_id;
            private static DelegateBridge _c__Hotfix_ctor;

            public AssetWithId()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        private class TransformWithId
        {
            public Transform m_trans;
            public int m_id;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

