﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceItemStoreUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient> EventOnItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        public List<ILBStoreItemClient> m_itemStoreListCache;
        public List<ProduceItemStoreCategoryToggleUIController> m_categoryToggleCtrlList;
        public ItemStoreListUIController m_itemStoreListCtrl;
        private int ObjPoolSize;
        private const string StateShow = "Show";
        private const string StateClose = "Close";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_simpleInfoDummy;
        [AutoBind("./BGImages/ToggleGroup/Viewport/Content/CategoryPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_categoryToggleTempObj;
        [AutoBind("./BGImages/ToggleGroup/Viewport/Content/CategoryPrefab/ItemTypeToggleRoot/ItemTypePrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_ItemTypeToggle;
        [AutoBind("./ItemStoreListPanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemStoreListDummy;
        [AutoBind("./ItemStoreListPanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public PrefabResourceContainer m_itemStoreListRes;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGImages/ToggleGroup/Viewport", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup m_itemTypeToggleGroup;
        [AutoBind("./BGImages/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_itemTypeToggleScrollView;
        [AutoBind("./EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateItemStoreInfo;
        private static DelegateBridge __Hotfix_SetProduceTypeToggleSelectState;
        private static DelegateBridge __Hotfix_UpdateProduceTypeToggleSelectState;
        private static DelegateBridge __Hotfix_ClearProduceCategoryToggleSelectedState;
        private static DelegateBridge __Hotfix_ClearProduceTypeToggleSelectState;
        private static DelegateBridge __Hotfix_CreateAllToggles;
        private static DelegateBridge __Hotfix_UpdateItemListSelect;
        private static DelegateBridge __Hotfix_UpdateItemListSelect_UserGuide;
        private static DelegateBridge __Hotfix_SetMenuItemScrollToViewBottom;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_CreateAllProduceCategoryToggle;
        private static DelegateBridge __Hotfix_InitProduceCategoryToggle;
        private static DelegateBridge __Hotfix_CreateProduceCategoryAllTypeToggle;
        private static DelegateBridge __Hotfix_InitProduceTypeToggle;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearProduceCategoryToggleSelectedState()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearProduceTypeToggleSelectState()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateAllProduceCategoryToggle(int count, Action<ProduceItemStoreCategoryToggleUIController> onCategoryToggleSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllToggles(string mode, Dictionary<int, long> bluePrintCountDic, List<int> blueprintCategoryList, Dictionary<int, List<int>> categoryToTypeInfoListDict, Dictionary<string, UnityEngine.Object> resDict, Action<ProduceItemStoreCategoryToggleUIController> onCategoryToggleSelected, Action<int> onItemTypeToggleSeleced)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateProduceCategoryAllTypeToggle(ProduceItemStoreCategoryToggleUIController categoryToggleCtrl, int count, Action<int> onBlueprintTypeToggleSeleced)
        {
        }

        [MethodImpl(0x8000)]
        private void InitProduceCategoryToggle(string mode, ProduceItemStoreCategoryToggleUIController categoryToggleCtrl, int category, long bpCount = -1L)
        {
        }

        [MethodImpl(0x8000)]
        private void InitProduceTypeToggle(string mode, ProduceItemStoreTypeToggleUIController itemTypeToggleCtrl, int typeId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemScrollToViewBottom(BlueprintCategory category, BlueprintType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProduceTypeToggleSelectState(int category, int type, bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemListSelect(int blueprintId, bool isBind, bool isFreezing)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemListSelect_UserGuide(int blueprintId, out bool isBind, out bool isFreezing)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreInfo(List<ILBStoreItemClient> itemStoreList, bool isShowItemCount, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceTypeToggleSelectState(int type)
        {
        }
    }
}

