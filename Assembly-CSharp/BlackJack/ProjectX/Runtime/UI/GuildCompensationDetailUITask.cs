﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class GuildCompensationDetailUITask : GuildUITaskBase
    {
        private LossCompensation m_lossComp;
        private List<LostItem> m_sortedLostList;
        private ulong m_lastInstanceId;
        private GuildCompensationDetailUIController m_detailCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildCompensationDetailUITask";
        public const string InstanceID = "InstanceID";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnRefuseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnDonateAllBtnClick;
        private static DelegateBridge __Hotfix_OnDonateAllConfirmClick;
        private static DelegateBridge __Hotfix_OnDonateSingleConfirmClick;
        private static DelegateBridge __Hotfix_OnDonateBtnClick;
        private static DelegateBridge __Hotfix_OnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_OnCaptionIconClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnWinnerCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnLosserCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_CompareLostItem;
        private static DelegateBridge __Hotfix_RefreshCompInfo;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildCompensationDetailUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private int CompareLostItem(LostItem lhs, LostItem rhs, Dictionary<StoreItemType, int> sortDic)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddCurrencyBtnClick(CurrencyType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptionIconClick(bool isWinner, UIControllerBase ctrl, LossCompensation comp)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateAllBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateAllConfirmClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateBtnClick(LostItem item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateSingleConfirmClick(LostItem item)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLosserCaptainIconButtonClick(UIControllerBase uCtrl, KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRefuseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWinnerCaptainIconButtonClick(UIControllerBase uCtrl, KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshCompInfo(ProLossCompensation loss)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(FakeLBStoreItem itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(ulong instanceId, UIIntent preIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnAddCurrencyBtnClick>c__AnonStorey2
        {
            internal CurrencyType currencyType;
            internal GuildCompensationDetailUITask $this;

            internal void <>m__0(UIProcess pre, bool result)
            {
                CommonUITaskHelper.ToAddCurrency(this.currencyType, this.$this.CurrentIntent, delegate (bool ret) {
                    if (ret)
                    {
                        this.$this.Pause();
                    }
                });
            }

            internal void <>m__1(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal GuildCompensationDetailUITask $this;

            internal void <>m__0(Task task)
            {
                GuildCompensationNet net = task as GuildCompensationNet;
                if (net != null)
                {
                    if (net.Ack.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(net.Ack.Result, true, false);
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(false);
                        }
                    }
                    else
                    {
                        this.$this.RefreshCompInfo(net.Ack.LossCompensation);
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(true);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RefreshCompInfo>c__AnonStorey3
        {
            internal Dictionary<StoreItemType, int> sortDic;
            internal GuildCompensationDetailUITask $this;

            internal int <>m__0(LostItem lhs, LostItem rhs) => 
                this.$this.CompareLostItem(lhs, rhs, this.sortDic);
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal UIIntentReturnable intent;

            internal void <>m__0(bool result)
            {
                if (!result)
                {
                    UIManager.Instance.ReturnUITask(this.intent.PrevTaskIntent, null);
                }
            }
        }

        public enum PipelineMask
        {
            DonateSuc,
            Resume
        }
    }
}

