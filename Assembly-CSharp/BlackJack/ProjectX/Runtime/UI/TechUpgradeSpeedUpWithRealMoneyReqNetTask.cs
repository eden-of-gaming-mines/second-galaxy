﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TechUpgradeSpeedUpWithRealMoneyReqNetTask : NetWorkTransactionTask
    {
        private int m_techId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <TechUpgradeSpeedUpWithMoneyResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTechUpgradeSpeedUpWithRealMoneyAck;
        private static DelegateBridge __Hotfix_set_TechUpgradeSpeedUpWithMoneyResult;
        private static DelegateBridge __Hotfix_get_TechUpgradeSpeedUpWithMoneyResult;

        [MethodImpl(0x8000)]
        public TechUpgradeSpeedUpWithRealMoneyReqNetTask(int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeSpeedUpWithRealMoneyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int TechUpgradeSpeedUpWithMoneyResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

