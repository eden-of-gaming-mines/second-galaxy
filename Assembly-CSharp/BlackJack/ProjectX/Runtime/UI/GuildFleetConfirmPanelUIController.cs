﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFleetConfirmPanelUIController : UIControllerBase
    {
        private FleetPosition m_transferPosition;
        private List<uint> m_retirePositions;
        [AutoBind("./TransferPosition/ContentGroup/TitleText03", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText03;
        [AutoBind("./TransferPosition/ContentGroup/TitleText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransferOnePositionDescText;
        [AutoBind("./TransferPosition/ContentGroup/FireController", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TransferFireControllerToggleEx;
        [AutoBind("./TransferPosition/ContentGroup/Navigator", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TransferNavigatorToggleEx;
        [AutoBind("./TransferPosition/ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TransferPositionCancelButton;
        [AutoBind("./TransferPosition/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TransferPositionConfirmButton;
        [AutoBind("./TransferPosition", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TransferPositionCommonUIStateController;
        [AutoBind("./TransferPosition", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TransferPositionGameObject;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildFleetConfirmPanelUIPrefabCommonUIStateController;
        private static DelegateBridge __Hotfix_CreateConfirmPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdateTransferPositionPanel;
        private static DelegateBridge __Hotfix_GetRetirePositions;
        private static DelegateBridge __Hotfix_GetTransferPosition;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnRetireCommanderValueChanged;
        private static DelegateBridge __Hotfix_OnRetireFireControllerValueChanged;
        private static DelegateBridge __Hotfix_OnRetireNavigatorValueChanged;
        private static DelegateBridge __Hotfix_OnTransferFireControllerValueChanged;
        private static DelegateBridge __Hotfix_OnTransferNavigatorValueChanged;

        [MethodImpl(0x8000)]
        public UIProcess CreateConfirmPanelUIProcess(bool isShow, bool immediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetRetirePositions()
        {
        }

        [MethodImpl(0x8000)]
        public FleetPosition GetTransferPosition()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRetireCommanderValueChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRetireFireControllerValueChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRetireNavigatorValueChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransferFireControllerValueChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransferNavigatorValueChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTransferPositionPanel(GuildFleetMemberInfo selfMember, bool isOffLine, string transferName)
        {
        }
    }
}

