﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NewCaptainGetUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCaptainFeatsItemClick;
        private List<NewGetCaptainFeatsItemUIController> m_featsCtrlList;
        public CaptainFeatInfoPanelUIController m_featInfoPanelCtrl;
        private UIProcess m_featProcess;
        private bool m_isShowFeatPanel;
        private bool m_isShowShipSimpleInfo;
        private const string m_hangarShipListItemName = "HangarShipListItem";
        private const string m_ItemSimpleInfoName = "ItemSimpleInfo";
        private ItemSimpleInfoUIController m_ItemSimpleInfo;
        private List<HangarShipListItemUIController> m_shipItemCtrArray;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private List<LBStaticHiredCaptain.ShipInfo> m_captionShipList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ButtonBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./CaptainImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainImage;
        [AutoBind("./NameRoot/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/CaptainInfoRoot/FactionGroup/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainFactionIcon;
        [AutoBind("./Margin/CaptainInfoRoot/FactionGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainFactionText;
        [AutoBind("./Margin/CaptainInfoRoot/AgeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainAgeText;
        [AutoBind("./Margin/CaptainInfoRoot/QualityGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainSubRankText;
        [AutoBind("./Margin/CaptainInfoRoot/ProfessionGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainProfessionText;
        [AutoBind("./Margin/CaptainInfoRoot/BirthdayGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainBirthdayText;
        [AutoBind("./Margin/CaptainInfoRoot/SexGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainGenderText;
        [AutoBind("./Margin/CaptainInfoRoot/ConstellationGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainConstellationText;
        [AutoBind("./Margin/PropertyRoot/PropertyGroup/Weapon/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyWeaponValueText;
        [AutoBind("./Margin/PropertyRoot/PropertyGroup/Driving/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyDrivingValueText;
        [AutoBind("./Margin/PropertyRoot/PropertyGroup/Defense/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyDefenseValueText;
        [AutoBind("./Margin/PropertyRoot/PropertyGroup/Electron/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyElectronValueText;
        [AutoBind("./Margin/FeatsRoot/FeatsGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainFeatsGroup;
        [AutoBind("./DummyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatsInfoDummyGroup;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class01/HangarShipListItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemClass1;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class01/ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemInfoClass1;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class02/HangarShipListItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemClass2;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class02/ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemInfoClass2;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class03/HangarShipListItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemClass3;
        [AutoBind("./Margin/ShipRoot/ShipGroup/Class03/ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemInfoClass3;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_ShowNewCaptainInfo;
        private static DelegateBridge __Hotfix_ShowCaptainFeatsInfo;
        private static DelegateBridge __Hotfix_HideCaptainFeatsInfo;
        private static DelegateBridge __Hotfix_IsCaptainFeatPanelShow;
        private static DelegateBridge __Hotfix_CreateShipPrefab;
        private static DelegateBridge __Hotfix_ShowHangarShipInfo;
        private static DelegateBridge __Hotfix_OnShipItemClickStart;
        private static DelegateBridge __Hotfix_HideShipItemInfo;
        private static DelegateBridge __Hotfix_IsShipitemPanelShow;
        private static DelegateBridge __Hotfix_OnFeatsItemClick;
        private static DelegateBridge __Hotfix_GetFeatsInfoPanelDummyPoint;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_GetFeatUIProcess;
        private static DelegateBridge __Hotfix_add_EventOnCaptainFeatsItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainFeatsItemClick;

        public event Action<int> EventOnCaptainFeatsItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateShipPrefab(GameObject parentObj)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetFeatsInfoPanelDummyPoint(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetFeatUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void HideCaptainFeatsInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void HideShipItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCaptainFeatPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipitemPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFeatsItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipItemClickStart(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainFeatsInfo(LBNpcCaptainFeats featsInfo, int idx, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowHangarShipInfo(LBStaticHiredCaptain newCaptain, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewCaptainInfo(LBStaticHiredCaptain newCaptain, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

