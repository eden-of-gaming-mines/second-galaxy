﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class ColorByteArrayContext
    {
        public byte[] m_byteArray;
        public int m_scaleLevel;
        public int m_x;
        public int m_y;
        public bool m_isComplete;
        public int m_version;
        public Thread m_drawThread;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init_0;
        private static DelegateBridge __Hotfix_Init_1;
        private static DelegateBridge __Hotfix_Release;

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(byte[] data)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }
    }
}

