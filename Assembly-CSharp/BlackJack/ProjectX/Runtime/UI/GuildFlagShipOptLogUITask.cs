﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class GuildFlagShipOptLogUITask : UITaskBase
    {
        public const string TaskName = "GuildFlagShipOptLogUITask";
        private const string PanelStateClose = "Close";
        private const string PanelStateShow = "Show";
        private const string DropdownStateUp = "Up";
        private const string DropdownStaeDown = "Down";
        private List<GuildFlagShipOptLogInfo> m_guildFlagShipOptLogInfos;
        private readonly List<GuildFlagShipOptLogInfo> m_filteredOptLogInfos;
        private bool m_logTypeFilterOpen;
        private bool m_solarSystemFilterOpen;
        private int m_logTypeFilterSpecialIndex;
        private int m_solarSystemFilterSpecialIndex;
        private readonly List<GuildFlagShipLogTypeFilterItemInfo> m_logTypeFilterItemInfos;
        private readonly List<GuildFlagShipLogSolarSystemFilterItemInfo> m_solarSystemFilterItemInfos;
        private GuildFlagShipOptLogUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Comparison<GuildFlagShipLogSolarSystemFilterItemInfo> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildFlagShipOptLogUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnFilterCloseButtonClick;
        private static DelegateBridge __Hotfix_OnTypeDropdownButtonClick;
        private static DelegateBridge __Hotfix_OnSolarSystemDropdownButtonClick;
        private static DelegateBridge __Hotfix_OnLogTypeFilterItemClick;
        private static DelegateBridge __Hotfix_OnSolarSystemFilterItemClick;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_ResetFilterData;
        private static DelegateBridge __Hotfix_FilterLogByFilters;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildFlagShipOptLogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void FilterLogByFilters()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFilterCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogTypeFilterItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemDropdownButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemFilterItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTypeDropdownButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetFilterData()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildFlagShipOptLogUITask(Action redirectOnResLoadComplete, Action<bool> onPiplineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ResetFilterData>c__AnonStorey0
        {
            internal GuildFlagShipOptLogInfo info;

            internal bool <>m__0(GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo item) => 
                (item.m_solarSystemId == this.info.m_solarSystemId);
        }

        public class GuildFlagShipLogSolarSystemFilterItemInfo
        {
            public int m_solarSystemId;
            public string m_solarSystemName;
            public bool m_enable;
            private static DelegateBridge _c__Hotfix_ctor;

            public GuildFlagShipLogSolarSystemFilterItemInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class GuildFlagShipLogTypeFilterItemInfo
        {
            public GuildFlagShipOptLogType m_type;
            public string m_typeName;
            public bool m_enable;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public class GuildFlagShipOptLogInfoReqNetWorkTask : NetWorkTransactionTask
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Result>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private uint <LatestSeqId>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildFlagShipOptLogInfoAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_LatestSeqId;
            private static DelegateBridge __Hotfix_set_LatestSeqId;

            public GuildFlagShipOptLogInfoReqNetWorkTask(uint latestSeqId) : base(10f, true, false, false)
            {
                this.LatestSeqId = latestSeqId;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp197(this, latestSeqId);
                }
            }

            private void OnGuildFlagShipOptLogInfoAck(int result)
            {
                DelegateBridge bridge = __Hotfix_OnGuildFlagShipOptLogInfoAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp31(this, result);
                }
                else
                {
                    this.Result = result;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildFlagShipOptLogInfoAck += new Action<int>(this.OnGuildFlagShipOptLogInfoAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildFlagShipOptLogInfoReq(this.LatestSeqId));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildFlagShipOptLogInfoAck -= new Action<int>(this.OnGuildFlagShipOptLogInfoAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public uint LatestSeqId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_LatestSeqId;
                    return ((bridge == null) ? this.<LatestSeqId>k__BackingField : bridge.__Gen_Delegate_Imp1032(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_LatestSeqId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp197(this, value);
                    }
                    else
                    {
                        this.<LatestSeqId>k__BackingField = value;
                    }
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpdateDataCache,
            UpdateViewOnLogArea,
            UpdateViewOnLogTypeFilterArea,
            UpdateViewOnSolarSystemFilterArea,
            UpdateLogTypeFilterSpecialIndex,
            UpdateSolarSystemFilterSpecialIndex
        }
    }
}

