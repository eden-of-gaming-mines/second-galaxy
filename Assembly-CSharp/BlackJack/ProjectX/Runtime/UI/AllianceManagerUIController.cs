﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceManagerUIController : UIControllerBase
    {
        public DoingAction m_activeAction;
        private const string Show = "Show";
        private const string Close = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./TipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subpanelRootStateController;
        [AutoBind("./TipGroup/ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subpanelStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./NameButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeNameBtn;
        [AutoBind("./RegionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeRegionBtn;
        [AutoBind("./AnnouncementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeAnnouncementBtn;
        [AutoBind("./SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeLogoBtn;
        [AutoBind("./MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sendMailBtn;
        [AutoBind("./QuitButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_quitBtn;
        [AutoBind("./DismissButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_dismissBtn;
        [AutoBind("./NameButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_changeNameBtnMask;
        [AutoBind("./RegionButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_changeRegionBtnMask;
        [AutoBind("./AnnouncementButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_changeAnnouncementBtnMask;
        [AutoBind("./SignButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_changeLogoBtnMask;
        [AutoBind("./MailButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sendMailBtnMask;
        [AutoBind("./QuitButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_quitBtnMask;
        [AutoBind("./DismissButton/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_dismissBtnMask;
        [AutoBind("./TipGroup/ContentGroup/Buttons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_noBtn;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_costedNoBtn;
        [AutoBind("./TipGroup/ContentGroup/Buttons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_yesBtn;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_costedYesBtn;
        [AutoBind("./TipGroup/ContentGroup/NumberButtons/YesButton/Cost/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_changeNameCost;
        [AutoBind("./TipGroup/ContentGroup/ModificationRegion/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public GuildRegionController m_languageTypeController;
        [AutoBind("./TipGroup/ContentGroup/ModificationRegion/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_regionInput;
        [AutoBind("./TipGroup/ContentGroup/ModificationName/TextGroup/NameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_nameText;
        [AutoBind("./TipGroup/ContentGroup/ModificationAnnouncement/TextGroup/TextInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_announcementText;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_OpenCloseSubpanelRoot;
        private static DelegateBridge __Hotfix_ClearAllEvents;
        private static DelegateBridge __Hotfix_ResetAllianceInfo;
        private static DelegateBridge __Hotfix_GetNameInput;
        private static DelegateBridge __Hotfix_GetAnnouncementInput;
        private static DelegateBridge __Hotfix_GetModifiedLanguageType;
        private static DelegateBridge __Hotfix_SetChangeNameCost;
        private static DelegateBridge __Hotfix_SetLanguageType;

        [MethodImpl(0x8000)]
        public void ClearAllEvents()
        {
        }

        [MethodImpl(0x8000)]
        public string GetAnnouncementInput()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType GetModifiedLanguageType()
        {
        }

        [MethodImpl(0x8000)]
        public string GetNameInput()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo OpenCloseSubpanelRoot(bool isShow, bool imm = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetAllianceInfo(AllianceBasicInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChangeNameCost(ulong cost)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLanguageType(GuildAllianceLanguageType languageType)
        {
        }

        public enum DoingAction
        {
            None,
            ChangeName,
            ChangeRegion,
            ChangeAnnouncement,
            Dismiss
        }
    }
}

