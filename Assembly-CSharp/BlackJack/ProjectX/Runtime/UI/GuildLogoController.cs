﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildLogoController : UIControllerBase
    {
        public GameObject m_bgImage;
        [AutoBind("./SignBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_previewBgImage;
        [AutoBind("./SignImage01", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_previewPaintingImage;
        private Sprite m_defaultBg;
        private Sprite m_defaultPainting;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetBgPreview;
        private static DelegateBridge __Hotfix_SetPaintingPreview;
        private static DelegateBridge __Hotfix_SetBgColor;
        private static DelegateBridge __Hotfix_SetPaintingColor;
        private static DelegateBridge __Hotfix_ResetPreview;
        private static DelegateBridge __Hotfix_SetPreview_0;
        private static DelegateBridge __Hotfix_SetLogo_2;
        private static DelegateBridge __Hotfix_SetLogo_1;
        private static DelegateBridge __Hotfix_SetPreview_1;
        private static DelegateBridge __Hotfix_SetLogo_0;
        private static DelegateBridge __Hotfix_SetFullPathLog;
        private static DelegateBridge __Hotfix_SetDefaultLogo;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetPreview()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBgColor(Color color)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetBgPreview(string resKey, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDefaultLogo(Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFullPathLog(string logoPath, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool useDefult = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogo(string logoPath, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogo(AllianceLogoInfo logo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool showBg = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogo(GuildLogoInfo logo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool showBg = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPaintingColor(Color color)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetPaintingPreview(string resKey, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isEnable = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPreview(LogoHelper.LogoInfo logoInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPreview(int bgId, int paitingId, int bgColorId, int paintingColorId, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, LogoHelper.LogoUsageType type = 0, bool showBg = true)
        {
        }
    }
}

