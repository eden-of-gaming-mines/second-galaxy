﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using TouchScript.Gestures;
    using UnityEngine;

    public class SpaceStationUICameraOperationController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType> EventOnSpaceStationUI3DSceneClick;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public ScreenTransformGesture m_screenTransformGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TapGesture m_tapGesture;
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        private float m_cameraYRotationOffsetSensitivity;
        private float m_cameraYRotationMin;
        private float m_cameraYRotationMax;
        private bool m_cameraRotationEnabled;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_manipulationTransformedHandler;
        private static DelegateBridge __Hotfix_manipulationTappedHandler;
        private static DelegateBridge __Hotfix_add_EventOnSpaceStationUI3DSceneClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpaceStationUI3DSceneClick;
        private static DelegateBridge __Hotfix_get_CameraRotationEnable;
        private static DelegateBridge __Hotfix_set_CameraRotationEnable;

        public event Action<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType> EventOnSpaceStationUI3DSceneClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void manipulationTappedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void manipulationTransformedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        public bool CameraRotationEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

