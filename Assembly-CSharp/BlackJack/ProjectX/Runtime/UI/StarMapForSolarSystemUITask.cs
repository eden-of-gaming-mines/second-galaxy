﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StarMapForSolarSystemUITask : UITaskBase
    {
        private bool m_isDelegateSignalFightUserGuideActive;
        private bool m_isDelegateSignalMineralUserGuideActive;
        private bool m_isWormholeUserGuideActive;
        private bool m_isInfectUserGuideActive;
        private bool m_isFreeQuestUserGuideActive;
        private bool m_isPVPSignalUserGuideActive;
        private bool m_isExploreQuestPlanetCountUserGuideActive;
        private bool m_isExploreQuestPlanetUserGuideActive;
        private bool m_isExploreQuestStarUserGuideActive;
        private QuestType m_freeQuestTypeForUserGuide;
        private DelegateSignalFightUserGuideState m_userGuideStateForDelegateSignalFight;
        private DelegateSignalMineralUserGuideState m_userGuideStateForDelegateSignalMineral;
        private ExploreQuestUserGuideState m_userGuideStateForExploreQuest;
        private FreeQuestUserGuideState m_userGuideStateForFreeQuest;
        private InfectUserGuideState m_userGuideStateForInfect;
        private PVPSignalUserGuideState m_userGuideStateForPVPSignal;
        private bool m_isSpaceSignalScanRepeatableUserGuide;
        private SpaceSignalScanUserGuideState m_userGuideStateForSpaceSignalScan;
        private WormholeUserGuideState m_userGuideStateForWormhole;
        public const string StarMapForSolarSystemMode_NormalMode = "StarMapForSolarSystemMode_NormalMode";
        private StarMapForSolarSystemUIController m_mainCtrl;
        private StarMapForSolarSystem3DSceneController m_threeDStarMapCtrl;
        public const string StarMapForSolarSystemCustomParamKeySolarSystemId = "SolarSystemId";
        public const string StarMapForSolarSystemCustomParamKeyIsNeedAutoScanPveSignal = "IsNeedAutoScanPVE";
        public const string StarMapForSolarSystemCustomParamKeyIsNeedAutoScanPvpSignal = "ISNeedAutoScanPVP";
        public const string StarMapForSolarSystemCustomParamKeyIsNeedAutoScanManualSignal = "IsNeedAutoScanManual";
        public const string StarMapForSolarSystemCustomParamKeyIsNeedAutoScanGuildSentrySignal = "IsNeedAutoGuildSentrySignal";
        public const string StarMapForSolarSystemCustomParamKeyIsNeedAutoScanSpaceSignal = "IsNeedAutoScanSpaceSignal";
        public const string StarMapForSolarSystemCustomParamKeyFadeInEffectTimeLength = "FadeInEffectTimeLength";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForDelegateSignalFight = "RepeatableUserGuideForDelegateSignalFight";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForDelegateSignalMineral = "RepeatableUserGuideForDelegateSignalMineral";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForWormhole = "RepeatableUserGuideForWormhole";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForInfect = "RepeatableUserGuideForInfect";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForFreeQuest = "RepeatableUserGuideForFreeQuest";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideFreeQuestType = "RepeatableUserGuideFreeQuestType";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForPvpSignal = "RepeatableUserGuideForPVPSignal";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForScanSpaceSignal = "RepeatableUserGuideForScanSpaceSignal";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForExploreQuestPlanetCount = "RepeatableUserGuideForExploreQuestPlanetCount";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForExploreQuestPlanet = "RepeatableUserGuideForExploreQuestPlanet";
        public const string StarMapForSolarSystemCustomParamKeyRepeatableUserGuideForExploreQuestStar = "RepeatableUserGuideForExploreQuestStar";
        public const string StarMapForSolarSystemCustomParamKeyDefaultSelectedTab = "DefaultSelectedTab";
        public const string StarMapForSolarSystemCustomParamKeyScanSpaceSignalTips = "ScanSpaceSignalTips";
        public const string StarMapForSolarSystemCustomParamKeyUpdateSolarSystemInfoPanel = "UpdateSolarSystemInfoPanel";
        private const int NeedNewSolarSystemArrivedEffectMaskIndex = 0;
        private const int IsPipeLineForUITaskStartOrResume = 1;
        private const int IsSwitchSolarSystemDetailInfoPanelVisibleState = 2;
        private const int IsUpdateSolarSystemInfoPanel = 3;
        public const int DelegateFightExtraRewardItemId = 0x25c;
        private bool m_isScanGuildSentrySingal;
        protected static string m_starMapForSolarSystemUILayerAssetName;
        protected static string m_starMapForSolarSystem3DLayerAssetName;
        protected static string m_startMapAllowInShipGroupUILayerAssetName;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private bool? m_wormholeInfoIsReady;
        private bool? m_infectInfoIsReady;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private List<GuildSentryInterestScene> m_guildSentryInterestScenes;
        private List<Vector2> m_guildSentrySignalHudPosList;
        private Vector2? m_cameraRotation;
        private float? m_cameraOffset;
        private int m_solarSystemId;
        private GDBSolarSystemInfo m_solarSystemInfo;
        private string m_currSelectedPopupMenuTab;
        private string m_currSolarSystemSkyBoxResPath;
        private bool m_isRefreshSignalListFromServer;
        private bool m_isOnCameraOnLockForFocusTarget;
        private Material m_starMapLineMaterial;
        private bool m_isStarMapScanAnimPlaying;
        private GameObject m_currLookAtTarget3DObj;
        private Vector3? m_currLookAtTargetPos;
        private MenuItemUIControllerBase m_currSelectedTargetMenuItem;
        private float m_fadeInTimeLength;
        private DateTime m_starMap3DScenePressedStartTime;
        private AllowInShipGroupUIController m_allowInShipGroupUIController;
        private float m_nextCalcTime;
        private Dictionary<int, LogicBlockStarMapInfoClient.LocationItemInfo> m_locationItemInfoDict;
        private List<LogicBlockStarMapInfoClient.LocationItemInfo> m_locationItemList;
        private bool m_isScreenPressed;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, int, float> EventOnBackToStarfieldUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToMainTask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnStrikeForQuest;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBPVPInvadeRescue> EventOnRescueStrikeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong, Action<bool>> EventOnStrikeForDelegateSignal;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnStrikeForPVPSignal;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NavigationNodeType, int, int, uint> EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, ConfigDataWormholeStarGroupInfo> EventOnStrikeForAnotherSolarSystemWormhole;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildSentryInterestScene> EventOnGuildSentrySignalStrikeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBaseRedeployInfoPanelClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSolarSystemInfo, int> EventOnStartRedeployButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SignalInfo> EventOnSpaceSignalStrikeButtonClick;
        private const float PressDurationForViewModeSwitch = 0.5f;
        public const string PipeLineCtxTaskNotifyDescInitShowEnd = "OnInitShowEnd";
        private const float CcalcalteLocationItemInfoTimeInterval = 2f;
        public const string TaskName = "StarMapForSolarSystemUITask";
        private const string LockLayerName = "StarMapSolarSystem";
        private const string PveFun2 = "PVESignalButton";
        private const string PvpFun2 = "PVPSignalButton";
        private const string GuildSentryFunc = "GuildSentryButton";
        private const string QuestSingnalFun2 = "QuestSignalButton";
        private const string UniverseStarMapFun = "LocButton";
        private const string BaseRepeloy = "BaseRepeloyButton";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_signalFunctionOpenStateDisplayInfos;
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_bseRepeloyFunctionOpenStateDisplayInfo;
        private bool m_isPvpFunButtonShow;
        private bool m_isPveFunButtonShow;
        private bool m_isQuestFunButtonShow;
        private bool m_isGuildSentryFuncButtonShow;
        [CompilerGenerated]
        private static Comparison<SignalInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataNormalItemInfo>, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Action <>f__am$cache2;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataNormalItemInfo>, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Action <>f__am$cache4;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataNormalItemInfo>, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Action <>f__am$cache6;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateView_InfectInfo;
        private static DelegateBridge __Hotfix_TickForInfectInfo;
        private static DelegateBridge __Hotfix_OnInfectDetailInfoInfoButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToFinalBattleButtonClick;
        private static DelegateBridge __Hotfix_TryToShowInfectInfoUI;
        private static DelegateBridge __Hotfix_HideInfectInfoUI;
        private static DelegateBridge __Hotfix_TryToAddInfectMenuItem;
        private static DelegateBridge __Hotfix_UpdateDataCache_RepeatableUserGuide;
        private static DelegateBridge __Hotfix_UpdateView_RepeatableUserGuide;
        private static DelegateBridge __Hotfix_StartUserGuideForDelegateSignalFight;
        private static DelegateBridge __Hotfix_SetUserGuideForDelegateSignalFightToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForDelegateSignalFight;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideDelegateSignalFight;
        private static DelegateBridge __Hotfix_GetUIRectForDelegateFightScanProbe;
        private static DelegateBridge __Hotfix_GetUIRectForFirstDelegateSignalFightMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForFirstDelegateSignalFightMenuItemStrikeButton;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalFight_FirstSignalMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalFight_ScanProbe;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalFight_FirstSignalStrikeButton;
        private static DelegateBridge __Hotfix_StartUserGuideForDelegateSignalMineral;
        private static DelegateBridge __Hotfix_SetUserGuideForDelegateSignalMineralToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForDelegateSignalMineral;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideDelegateSignalMineral;
        private static DelegateBridge __Hotfix_GetUIRectForDelegateMineralScanProbe;
        private static DelegateBridge __Hotfix_GetUIRectForFirstDelegateSignalMineralMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForFirstDelegateSignalMineralMenuItemStrikeButton;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalMineral_ScanProbe;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalStrikeButton;
        private static DelegateBridge __Hotfix_TryRepeatableForSelectPlanetTab;
        private static DelegateBridge __Hotfix_RepeatableForSelectCelestialPlanetTab;
        private static DelegateBridge __Hotfix_RepeatableGuideStateEndForExploreQuest;
        private static DelegateBridge __Hotfix_StartUserGuideForExploreQuestPlanetCount;
        private static DelegateBridge __Hotfix_StartUserGuideForExploreQuestPlanet;
        private static DelegateBridge __Hotfix_TryRepeatableForSelectPlanet;
        private static DelegateBridge __Hotfix_RepeatableForSelectPlanet;
        private static DelegateBridge __Hotfix_StartUserGuideForExploreQuestStar;
        private static DelegateBridge __Hotfix_TryRepeatableForSelectStar;
        private static DelegateBridge __Hotfix_RepeatableForSelectStar;
        private static DelegateBridge __Hotfix_StartUserGuideForFreeQuest;
        private static DelegateBridge __Hotfix_SetUserGuideForFreeQuestToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForFreeQuest;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideFreeQuest;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForFreeQuest_FirstQuestMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForFreeQuest_FirstQuestSignalMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForFreeQuest_FirstQuestStrikeButton;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForFreeQuest_ScanProbe;
        private static DelegateBridge __Hotfix_StartUserGuideForInfect;
        private static DelegateBridge __Hotfix_SetUserGuideForInfectToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForInfect;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideInfect;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForInfect_FirstQuestMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForInfect_FirstQuestSignalMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForQuest_FirstQuestStrikeButton;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForQuestSignal_ScanProbe;
        private static DelegateBridge __Hotfix_GetUIRectForFirstQuestMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForQuestMenuItemStrikeButton;
        private static DelegateBridge __Hotfix_GetUIRectForQuestScanProbe;
        private static DelegateBridge __Hotfix_StartUserGuideForPVPSignal;
        private static DelegateBridge __Hotfix_SetUserGuideForPVPSignalToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForPVPSignal;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuidePVPSignal;
        private static DelegateBridge __Hotfix_GetUIRectForPVPSignalScanProbe;
        private static DelegateBridge __Hotfix_GetUIRectForFirstPVPSignalMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForFirstPVPSignalMenuItemStrikeButton;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForPVPSignal_FirstSignalMenuItem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForPVPSignal_ScanProbe;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForPVPSignal_FirstSignalStrikeButton;
        private static DelegateBridge __Hotfix_StartSpaceSignalScanRepeatableUserGuide;
        private static DelegateBridge __Hotfix_SetUserGuideForSpaceSignalScanToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForSpaceSignalScan;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideSpaceSignal;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForSpaceSignal_FirstSpaceSignalMenuItem;
        private static DelegateBridge __Hotfix_GetUIRectForFirstSpaceSignalMenuItem;
        private static DelegateBridge __Hotfix_StartUserGuideForWormhole;
        private static DelegateBridge __Hotfix_SetUserGuideForWormholeToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForWormhole;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideWormhole;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForWormhole_WormholeStrikeButton;
        private static DelegateBridge __Hotfix_GetUIRectForWormholeStrikeButton;
        private static DelegateBridge __Hotfix_StartStarMapForSolarSystemUITask;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_ShowMainUI;
        private static DelegateBridge __Hotfix_UpdateViewForStrikeEnd;
        private static DelegateBridge __Hotfix_GetCurrIntentAndRecordState;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_PrepareEnterSolarSystem;
        private static DelegateBridge __Hotfix_OnPrepareForStartOrResumeComplete;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateVirtulBuffItem;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_UpdatePopupMenuTabInfo;
        private static DelegateBridge __Hotfix_UpdatePopupMenuQuestSignalInfo;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_UpdatePopupMenuDelegateSignalInfo;
        private static DelegateBridge __Hotfix_UpdatePopupMenuPvpSignalInfo;
        private static DelegateBridge __Hotfix_UpdatePopupMenuQuestInfo;
        private static DelegateBridge __Hotfix_UpdatePopupMenuCelestialInfo;
        private static DelegateBridge __Hotfix_UpdateSolarSystemInfoPanel;
        private static DelegateBridge __Hotfix_IsNeedAutoScan;
        private static DelegateBridge __Hotfix_ClearAutoScan;
        private static DelegateBridge __Hotfix_ClearFadeInEffectMask;
        private static DelegateBridge __Hotfix_get_IsUIInputEnable;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickPlayerPosInfoOn3DStarMap;
        private static DelegateBridge __Hotfix_SetLocationItemPos;
        private static DelegateBridge __Hotfix_TickGuildSentrySignalHud;
        private static DelegateBridge __Hotfix_TickScanProbeRecovery;
        private static DelegateBridge __Hotfix_TickMenuItemInfo;
        private static DelegateBridge __Hotfix_TickStarMap3DScenePressed;
        private static DelegateBridge __Hotfix_TickForUILayerVisible;
        private static DelegateBridge __Hotfix_TickForUILayerVisible_Filter;
        private static DelegateBridge __Hotfix_OnScreenScaleOperationHappend;
        private static DelegateBridge __Hotfix_OnScreenSlippingOperationHappend;
        private static DelegateBridge __Hotfix_OnScreenPressed;
        private static DelegateBridge __Hotfix_OnScreenReleased;
        private static DelegateBridge __Hotfix_OnScreenTapOperationHappend;
        private static DelegateBridge __Hotfix_OnBackToStarfieldButtonClick;
        private static DelegateBridge __Hotfix_OnBackToStarfieldButtonClickImp;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnManualSignalScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentrySignalProbeButtonClick;
        private static DelegateBridge __Hotfix_OnOrientationButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_OnPopupMenuCeletialTabClicked;
        private static DelegateBridge __Hotfix_OnPopupMenuQuestSignalTabClicked;
        private static DelegateBridge __Hotfix_OnPopupMenuDelegateSignalTabClicked;
        private static DelegateBridge __Hotfix_OnPopupMenuPvpSignalTabClicked;
        private static DelegateBridge __Hotfix_OnPopupMenuQuestTabClicked;
        private static DelegateBridge __Hotfix_OnPopupMenuTabClicked;
        private static DelegateBridge __Hotfix_OnMenuItemIconButtonClick;
        private static DelegateBridge __Hotfix_OnMenuItemIconButtonClickImp;
        private static DelegateBridge __Hotfix_OnQuestAssignButtonClick;
        private static DelegateBridge __Hotfix_OnQuestAssignButtonClickImp;
        private static DelegateBridge __Hotfix_OnQuestBonusButtonClick;
        private static DelegateBridge __Hotfix_OnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateSignalAssignButtonClickImp;
        private static DelegateBridge __Hotfix_OnDelegateSignalRewardButtonButtonClick;
        private static DelegateBridge __Hotfix_OnPVPSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_OnRescueOperationButtonClick;
        private static DelegateBridge __Hotfix_OnPVPSignalAssignButtonClick;
        private static DelegateBridge __Hotfix_ShowMsgThenGoFight;
        private static DelegateBridge __Hotfix_PVPSignalGoFight;
        private static DelegateBridge __Hotfix_OnPlanetItemJumpButtonClick;
        private static DelegateBridge __Hotfix_OnStarJumpButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceStationItemJumpButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceStationItemBaseRedeployButtonClick;
        private static DelegateBridge __Hotfix_JumpToWormholeStargateAction;
        private static DelegateBridge __Hotfix_OnJumpToGlobalSceneButtonClick;
        private static DelegateBridge __Hotfix_ClickJumpToWormholeButton_1;
        private static DelegateBridge __Hotfix_OnJumpToUnknownWormholeButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceStationItemBoardButtonClick;
        private static DelegateBridge __Hotfix_OnStargateItemJumpButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnCameraSlerpProcessEnd;
        private static DelegateBridge __Hotfix_OnScannerAnimationFinished;
        private static DelegateBridge __Hotfix_OnMenuItemRewardItemClick;
        private static DelegateBridge __Hotfix_OnMenuItemRewardItem3DTouch;
        private static DelegateBridge __Hotfix_ReturnFromItemDetailInfoTask;
        private static DelegateBridge __Hotfix_OnMenuScrollViewClick;
        private static DelegateBridge __Hotfix_OnRescueCaptainShowingButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_OnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnVirtualBuffButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentryHudItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_ManualScanProbeAddByStoreItemTipPanel_ConfirmButtonClick;
        private static DelegateBridge __Hotfix_PVPScanProbeAddByStoreItemTipPanel_ConfirmButtonClick;
        private static DelegateBridge __Hotfix_PVEScanProbeAddByStoreItemTipPanel_ConfirmButtonClick;
        private static DelegateBridge __Hotfix_ScanProbeAddByStoreItemTipPanel_ComfirmButtonImpl;
        private static DelegateBridge __Hotfix_OnSolarSystemInfoDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnSolarSystemDetailInfoPanelBgButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalCancelButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceSignalPrecisenessDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceSignalPrecisenessDetailPanelBGButtonClick;
        private static DelegateBridge __Hotfix_GetLimitShipTypeList;
        private static DelegateBridge __Hotfix_DoOrientationBySolarSystemId;
        private static DelegateBridge __Hotfix_DoSignalScanEffectOnUI;
        private static DelegateBridge __Hotfix_RefreshDelegateSignalListFromServer;
        private static DelegateBridge __Hotfix_RefreshPvpSignalListFromServer;
        private static DelegateBridge __Hotfix_RefreshMannualSignalListFromServer;
        private static DelegateBridge __Hotfix_RefreshGuildSentrySignalListFromServer;
        private static DelegateBridge __Hotfix_GetQuestLocation;
        private static DelegateBridge __Hotfix_PvpSignalComparer;
        private static DelegateBridge __Hotfix_RestoreThreeDStarMapCamera;
        private static DelegateBridge __Hotfix_CheckConditionForInSpaceJumpToWormholeStargate;
        private static DelegateBridge __Hotfix_SendStarfieldSolarSystemInfectInfoReqNetTask;
        private static DelegateBridge __Hotfix_SendStarFieldWormholeGateInfoReqNetTask;
        private static DelegateBridge __Hotfix_ExpandMenuItemToGuildSentryItem;
        private static DelegateBridge __Hotfix_CheckExistUnAccptFreeQuest;
        private static DelegateBridge __Hotfix_CheckLowRewardForFreeQuest;
        private static DelegateBridge __Hotfix_CheckSolarSystemPoolCount;
        private static DelegateBridge __Hotfix_SwitchToEnotherSolarSystem;
        private static DelegateBridge __Hotfix_MenuItemClick_CelestialStar;
        private static DelegateBridge __Hotfix_MenuItemClick_CelestialPlanet;
        private static DelegateBridge __Hotfix_MenuItemClick_DelegateSignal;
        private static DelegateBridge __Hotfix_MenuItemClick_Quest;
        private static DelegateBridge __Hotfix_MenuItemClick_QuestSignal;
        private static DelegateBridge __Hotfix_MenuItemClick_SpaceStation;
        private static DelegateBridge __Hotfix_MenuItemClick_Stargate;
        private static DelegateBridge __Hotfix_MenuItemClick_PvpSignal;
        private static DelegateBridge __Hotfix_MenuItemClick_Wormhole;
        private static DelegateBridge __Hotfix_MenuItemClick_UnknowWormhole;
        private static DelegateBridge __Hotfix_MenuItemClick_Rescue;
        private static DelegateBridge __Hotfix_MenuItemClick_Infect;
        private static DelegateBridge __Hotfix_MenuItemClick_GuildSentry;
        private static DelegateBridge __Hotfix_MenuItemClick_SpaceSignal;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskReturnFromAnotherUITask;
        private static DelegateBridge __Hotfix_StartEnterStationProcedure;
        private static DelegateBridge __Hotfix_SortGuildSentryScene;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBSignal;
        private static DelegateBridge __Hotfix_get_LBQuest;
        private static DelegateBridge __Hotfix_add_EventOnBackToStarfieldUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToStarfieldUITask;
        private static DelegateBridge __Hotfix_add_EventOnBackToMainTask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToMainTask;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForQuest;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForQuest;
        private static DelegateBridge __Hotfix_add_EventOnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForDelegateSignal;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForDelegateSignal;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForPVPSignal;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForPVPSignal;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForAnotherSolarSystemWormhole;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForAnotherSolarSystemWormhole;
        private static DelegateBridge __Hotfix_add_EventOnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBaseRedeployInfoPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseRedeployInfoPanelClick;
        private static DelegateBridge __Hotfix_add_EventOnStartRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStartRedeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpaceSignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpaceSignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_StartSwitchToStarFiledFadeOutUIProcess;
        private static DelegateBridge __Hotfix_StartEnterFromStarFieldUIProcess;
        private static DelegateBridge __Hotfix_GetFadeOutCamreaOffsetProcessExcuter;
        private static DelegateBridge __Hotfix_GetFadeInCamreaOffsetProcessExcuter;
        private static DelegateBridge __Hotfix_GetFadeOutProcessExecutor;
        private static DelegateBridge __Hotfix_GetFadeInProcessExecutor;
        private static DelegateBridge __Hotfix_GetEnterStarFieldStarMapProcessExecutor;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_InitSignalFunctionOpenStateDisplayInfo;
        private static DelegateBridge __Hotfix_InitBaseRedeployFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableQuestSignal;
        private static DelegateBridge __Hotfix_UnLockQuestSignal;
        private static DelegateBridge __Hotfix_EnableStarMap;
        private static DelegateBridge __Hotfix_UnLockUniverseStarMap;
        private static DelegateBridge __Hotfix_EnablePveSignal;
        private static DelegateBridge __Hotfix_UnLockPveSignal;
        private static DelegateBridge __Hotfix_EnablePvpSignal;
        private static DelegateBridge __Hotfix_UnLockPvPignal;
        private static DelegateBridge __Hotfix_EnableGuildSentrySignalProbe;
        private static DelegateBridge __Hotfix_UnlockGuildSentrySignalProbe;
        private static DelegateBridge __Hotfix_UnLockBaseRedeploy;
        private static DelegateBridge __Hotfix_GetFirstFreeQuestSignalItemRect;
        private static DelegateBridge __Hotfix_GetFristAcceptFreeQuestItemRect;
        private static DelegateBridge __Hotfix_GetFristAcceptFreeQuestItemStrikeButtonRect;
        private static DelegateBridge __Hotfix_IsTabListOpen;
        private static DelegateBridge __Hotfix_ExistAnyAcceptedFreeQuest;
        private static DelegateBridge __Hotfix_ExistDelegateSignalItem;
        private static DelegateBridge __Hotfix_ExistPvpSignalItem;
        private static DelegateBridge __Hotfix_IsInScanAnim;
        private static DelegateBridge __Hotfix_GetSpaceStationItemRect;
        private static DelegateBridge __Hotfix_GetSpaceStation;
        private static DelegateBridge __Hotfix_OnSpaceStationBaseDeployButtonClick;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployButton;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployRect;
        private static DelegateBridge __Hotfix_GetSpaceStationBaseDeployButtonArea;
        private static DelegateBridge __Hotfix_EnableScrollView;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetWormholeItemRect;
        private static DelegateBridge __Hotfix_GetJumpToWormholeRect;
        private static DelegateBridge __Hotfix_ClickWormholeMenuItem;
        private static DelegateBridge __Hotfix_ClickJumpToWormholeButton_0;
        private static DelegateBridge __Hotfix_ClickBackToStarfieldButton;
        private static DelegateBridge __Hotfix_ClickCeletialTabButton;
        private static DelegateBridge __Hotfix_ClickQuestSignalTabButton;
        private static DelegateBridge __Hotfix_ClickMannualSignalButton;
        private static DelegateBridge __Hotfix_ClickFirstFreeQuestSignalItem;
        private static DelegateBridge __Hotfix_ClickFirstAcceptFreeQuestItem;
        private static DelegateBridge __Hotfix_ClickStationItem;
        private static DelegateBridge __Hotfix_ClickFristAcceptFreeQuestItemStrikeButton;
        private static DelegateBridge __Hotfix_EnterStarfieldUserGuide;
        private static DelegateBridge __Hotfix_GetFristDelegateSignalItemRect;
        private static DelegateBridge __Hotfix_GetFristDelegateSignalItemStikeButtonRect;
        private static DelegateBridge __Hotfix_ClickDelegateSignalButton;
        private static DelegateBridge __Hotfix_ClickDelegateSignalList;
        private static DelegateBridge __Hotfix_ClickFirstDelegateSignalItem;
        private static DelegateBridge __Hotfix_ClickFristDelegateSignalStikeButton;
        private static DelegateBridge __Hotfix_GetFristPvpSignalItemRect;
        private static DelegateBridge __Hotfix_GetFristPvpSignalItemStikeButtonRect;
        private static DelegateBridge __Hotfix_ClickPvpSignalButton;
        private static DelegateBridge __Hotfix_ClickPvpSignalList;
        private static DelegateBridge __Hotfix_ClickFirstPvpSignalItem;
        private static DelegateBridge __Hotfix_GetFirstEmergencyQuestSignalItemRect;

        public event Action EventOnBackToMainTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int, float> EventOnBackToStarfieldUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBaseRedeployInfoPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildSentryInterestScene> EventOnGuildSentrySignalStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBPVPInvadeRescue> EventOnRescueStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SignalInfo> EventOnSpaceSignalStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo, int> EventOnStartRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, ConfigDataWormholeStarGroupInfo> EventOnStrikeForAnotherSolarSystemWormhole
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NavigationNodeType, int, int, uint> EventOnStrikeForCelestialOrBuildingOrGlobalScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong, Action<bool>> EventOnStrikeForDelegateSignal
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStrikeForPVPSignal
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStrikeForQuest
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public StarMapForSolarSystemUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckConditionForInSpaceJumpToWormholeStargate(GlobalSceneInfo globalSceneInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckExistUnAccptFreeQuest()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckLowRewardForFreeQuest()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckSolarSystemPoolCount()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearAutoScan()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearFadeInEffectMask()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickBackToStarfieldButton()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickCeletialTabButton(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickDelegateSignalButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickDelegateSignalList(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstAcceptFreeQuestItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstDelegateSignalItem(SignalType signalType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstFreeQuestSignalItem(Action<bool> onEnd, QuestType qType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstPvpSignalItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFristAcceptFreeQuestItemStrikeButton()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFristDelegateSignalStikeButton(SignalType signalType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickJumpToWormholeButton()
        {
        }

        [MethodImpl(0x8000)]
        private void ClickJumpToWormholeButton(ConfigDataWormholeStarGroupInfo wormholeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickMannualSignalButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickPvpSignalButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickPvpSignalList(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickQuestSignalTabButton(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickStationItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickWormholeMenuItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private void DoOrientationBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void DoSignalScanEffectOnUI(string tabMode)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableGuildSentrySignalProbe(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePveSignal(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePvpSignal(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableQuestSignal(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableScrollView(bool isEnable = true)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableStarMap(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnterStarfieldUserGuide(int solarId)
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistAnyAcceptedFreeQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistDelegateSignalItem(SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistPvpSignalItem()
        {
        }

        [MethodImpl(0x8000)]
        private void ExpandMenuItemToGuildSentryItem(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public UIIntent GetCurrIntentAndRecordState()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetEnterStarFieldStarMapProcessExecutor(int orientedSSId)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeInCamreaOffsetProcessExcuter()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeInProcessExecutor()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeOutCamreaOffsetProcessExcuter()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeOutProcessExecutor()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstEmergencyQuestSignalItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstFreeQuestSignalItemRect(QuestType qType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristAcceptFreeQuestItemRect(QuestType qType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristAcceptFreeQuestItemStrikeButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristDelegateSignalItemRect(SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristDelegateSignalItemStikeButtonRect(SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristPvpSignalItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristPvpSignalItemStikeButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        private GuildSolarSystemInfoClient GetGuildSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetJumpToWormholeRect()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private List<ShipType> GetLimitShipTypeList(MenuItemUIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private Vector3D GetQuestLocation(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase GetSpaceStation()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetSpaceStationBaseDeployButton()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationBaseDeployButtonArea()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationBaseDeployRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSpaceStationItemRect()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForDelegateFightScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForDelegateMineralScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstDelegateSignalFightMenuItem(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstDelegateSignalFightMenuItemStrikeButton(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstDelegateSignalMineralMenuItem(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstDelegateSignalMineralMenuItemStrikeButton(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstPVPSignalMenuItem(PvpSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstPVPSignalMenuItemStrikeButton(PvpSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstQuestMenuItem()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForFirstSpaceSignalMenuItem(SpaceSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForMenuItem(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForPVPSignalScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForQuestMenuItemStrikeButton(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForQuestScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForWormholeStrikeButton(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetWormholeItemRect()
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForDelegateSignalFight(DelegateSignalFightUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForDelegateSignalMineral(DelegateSignalMineralUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForFreeQuest(FreeQuestUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForInfect(InfectUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForPVPSignal(PVPSignalUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForSpaceSignalScan(SpaceSignalScanUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForWormhole(WormholeUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void HideInfectInfoUI(bool isImmediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitBaseRedeployFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitSignalFunctionOpenStateDisplayInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInScanAnim()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedAutoScan(out string tabMode)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTabListOpen(string tab)
        {
        }

        [MethodImpl(0x8000)]
        private void JumpToWormholeStargateAction(WormholeMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ManualScanProbeAddByStoreItemTipPanel_ConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_CelestialPlanet(CelestialPlanetMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_CelestialStar(CelestialStarMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_DelegateSignal(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_GuildSentry(GuildSentrySignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_Infect(InfectMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_PvpSignal(PvpSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_Quest(QuestMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_QuestSignal(QuestSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_Rescue(RescueMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_SpaceSignal(SpaceSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_SpaceStation(SpaceStationMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_Stargate(StargateMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_UnknowWormhole(UnknownWormholeMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void MenuItemClick_Wormhole(WormholeMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToStarfieldButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToStarfieldButtonClickImp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCameraSlerpProcessEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideDelegateSignalFight(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideDelegateSignalMineral(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideFreeQuest(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideInfect(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuidePVPSignal(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideSpaceSignal(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideWormhole(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalAssignButtonClick(ulong signalInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalAssignButtonClickImp(ulong signalInstanceId, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalRewardButtonButtonClick(DelegateSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentryHudItemClick(ulong sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentrySignalProbeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentrySignalStrikeButtonClick(GuildSentryInterestScene guildSentryInterestSceneuildSentry)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInfectDetailInfoInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskReturnFromAnotherUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToFinalBattleButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToGlobalSceneButtonClick(GlobalSceneInfo globalSceneInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalButtonClick(SignalInfo spaceSignal)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToUnknownWormholeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger, MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnManualSignalScanProbeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemIconButtonClick(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemIconButtonClickImp(MenuItemUIControllerBase ctrl, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemRewardItem3DTouch(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemRewardItemClick(CommonItemIconUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientationButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_BackgroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CommanderButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_MotherShipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlanetItemJumpButtonClick(GDBPlanetInfo planetInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuCeletialTabClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuDelegateSignalTabClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuPvpSignalTabClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuQuestSignalTabClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuQuestTabClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPopupMenuTabClicked(string tab, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareForStartOrResumeComplete(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVEScanProbeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPScanProbeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPSignalAssignButtonClick(PvpSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPSignalOperationButtonClick(PvpSignalMenuItemUIController control)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAssignButtonClick(ulong questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAssignButtonClickImp(ulong questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestBonusButtonClick(List<VirtualBuffDesc> buffList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueCaptainShowingButtonClick(RescueMenuItemUIController menuItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueOperationButtonClick(LBPVPInvadeRescue signalInfo, Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueStrikeButtonClick(LBPVPInvadeRescue rescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScannerAnimationFinished()
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenPressed()
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenReleased()
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenScaleOperationHappend(float deltaScale)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenSlippingOperationHappend(float slippingX, float slippingY)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenTapOperationHappend()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemDetailInfoPanelBgButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemInfoDetailInfoButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceSignalPrecisenessDetailButtonClick(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceSignalPrecisenessDetailPanelBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSpaceStationBaseDeployButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationItemBaseRedeployButtonClick(GDBSpaceStationInfo spaceStationInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationItemBoardButtonClick(GDBSpaceStationInfo spaceStationInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationItemJumpButtonClick(GDBSpaceStationInfo spaceStationInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStargateItemJumpButtonClick(GDBStargateInfo stargateInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarJumpButtonClick(GDBStarInfo starInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnVirtualBuffButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareEnterSolarSystem(Action<bool> onPrepareEnd, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PVEScanProbeAddByStoreItemTipPanel_ConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void PVPScanProbeAddByStoreItemTipPanel_ConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private int PvpSignalComparer(LBSignalBase sigA, LBSignalBase sigB)
        {
        }

        [MethodImpl(0x8000)]
        private void PVPSignalGoFight(PvpSignalMenuItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshDelegateSignalListFromServer(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshGuildSentrySignalListFromServer(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshMannualSignalListFromServer(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshPvpSignalListFromServer(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForSelectCelestialPlanetTab(bool isPassEvent, Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForSelectPlanet(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForSelectStar(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableGuideStateEndForExploreQuest()
        {
        }

        [MethodImpl(0x8000)]
        private void RestoreThreeDStarMapCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnFromItemDetailInfoTask(Task retTask)
        {
        }

        [MethodImpl(0x8000)]
        private void ScanProbeAddByStoreItemTipPanel_ComfirmButtonImpl(NormalItemType normalItemType)
        {
        }

        [MethodImpl(0x8000)]
        private void SendStarfieldSolarSystemInfectInfoReqNetTask(int solarSystemId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendStarFieldWormholeGateInfoReqNetTask(GDBSolarSystemSimpleInfo solarSystemSimpleInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLocationItemPos(int stationId, bool inSpace, Vector3D pos, ref bool sucessSetPos, LogicBlockStarMapInfoClient.LocationItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForDelegateSignalFightToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForDelegateSignalMineralToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForFreeQuestToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForInfectToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForPVPSignalToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForSpaceSignalScanToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForWormholeToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMainUI(bool show)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMsgThenGoFight(PvpSignalMenuItemUIController ctrl, Queue<string> msgQueue)
        {
        }

        [MethodImpl(0x8000)]
        private int SortGuildSentryScene(GuildSentryInterestScene sceneA, GuildSentryInterestScene sceneB)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess StartEnterFromStarFieldUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterStationProcedure(GDBSolarSystemInfo solarSystemInfo, GDBSpaceStationInfo spaceStationInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void StartSpaceSignalScanRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForSolarSystemUITask(string mode, int solarSystemId, SignalAutoScanIntent autoScanIntent = null, float fadeInTimeLength = 0f, RepeatableUserGuideIntent repeatableUserGuideIntent = null, string defaultSelectedTab = null, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess StartSwitchToStarFiledFadeOutUIProcess(int orientedSSId)
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForDelegateSignalFight()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForDelegateSignalMineral()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForExploreQuestPlanet()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForExploreQuestPlanetCount()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForExploreQuestStar()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForFreeQuest()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForInfect()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForPVPSignal()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForWormhole()
        {
        }

        [MethodImpl(0x8000)]
        private void SwitchToEnotherSolarSystem(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void TickForInfectInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForUILayerVisible()
        {
        }

        [MethodImpl(0x8000)]
        private bool TickForUILayerVisible_Filter(SceneLayerBase layer)
        {
        }

        [MethodImpl(0x8000)]
        private void TickGuildSentrySignalHud()
        {
        }

        [MethodImpl(0x8000)]
        private void TickMenuItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void TickPlayerPosInfoOn3DStarMap()
        {
        }

        [MethodImpl(0x8000)]
        private void TickScanProbeRecovery()
        {
        }

        [MethodImpl(0x8000)]
        private void TickStarMap3DScenePressed()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForSelectPlanet(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForSelectPlanetTab(bool isPassEvent, Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForSelectStar(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private bool TryToAddInfectMenuItem(Action eventOnTimeout)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToShowInfectInfoUI()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalFight_FirstSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalFight_FirstSignalStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalFight_ScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForDelegateSignalMineral_ScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForFreeQuest_FirstQuestMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForFreeQuest_FirstQuestSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForFreeQuest_FirstQuestStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForFreeQuest_ScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForInfect_FirstQuestMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForInfect_FirstQuestSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForPVPSignal_FirstSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForPVPSignal_FirstSignalStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForPVPSignal_ScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForQuest_FirstQuestStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForQuestSignal_ScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForSpaceSignal_FirstSpaceSignalMenuItem(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForWormhole_WormholeStrikeButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockBaseRedeploy(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnlockGuildSentrySignalProbe(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockPveSignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockPvPignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockQuestSignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockUniverseStarMap(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_RepeatableUserGuide(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuCelestialInfo(List<SignalInfo> spaceSignalList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuDelegateSignalInfo(List<LBSignalDelegateBase> allSignalInCurrSolarSystemList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuPvpSignalInfo(List<LBSignalBase> allSignalInCurrSolarSystemList, List<LBSignalBase> pvpSignalInOtherSolarSystemList, List<LBPVPInvadeRescue> rescueInfoList, List<GuildSentryInterestScene> guildSentrySignalList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuQuestInfo(List<LBProcessingQuestBase> questList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuQuestSignalInfo(List<LBSignalManualBase> allSignalInCurrSolarSystemList, List<LBSignalManualBase> emergencySignalList = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePopupMenuTabInfo(string tabMode, Action onUpdateEnd = null, bool needShowOrHideImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSolarSystemInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_InfectInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_RepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewForStrikeEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVirtulBuffItem()
        {
        }

        protected bool IsUIInputEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockSignalClient LBSignal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockQuestClient LBQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckConditionForInSpaceJumpToWormholeStargate>c__AnonStorey26
        {
            internal ShipType selfShipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.selfShipType);
        }

        [CompilerGenerated]
        private sealed class <ExpandMenuItemToGuildSentryItem>c__AnonStorey29
        {
            internal ulong instanceId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.ExpandMenuItemToGuildSentryItem(this.instanceId);
            }
        }

        [CompilerGenerated]
        private sealed class <GetEnterStarFieldStarMapProcessExecutor>c__AnonStorey2F
        {
            internal int orientedSSId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Action<bool> onend)
            {
                <GetEnterStarFieldStarMapProcessExecutor>c__AnonStorey2E storeye = new <GetEnterStarFieldStarMapProcessExecutor>c__AnonStorey2E {
                    <>f__ref$47 = this,
                    onend = onend
                };
                this.$this.ExecAfterTicks(new Action(storeye.<>m__0), 1UL);
            }

            private sealed class <GetEnterStarFieldStarMapProcessExecutor>c__AnonStorey2E
            {
                internal Action<bool> onend;
                internal StarMapForSolarSystemUITask.<GetEnterStarFieldStarMapProcessExecutor>c__AnonStorey2F <>f__ref$47;

                internal void <>m__0()
                {
                    this.<>f__ref$47.$this.m_threeDStarMapCtrl.ResetScaleAnim();
                    this.<>f__ref$47.$this.Pause();
                    if (this.onend != null)
                    {
                        this.onend(true);
                    }
                    if (this.<>f__ref$47.$this.EventOnBackToStarfieldUITask != null)
                    {
                        this.<>f__ref$47.$this.EventOnBackToStarfieldUITask(this.<>f__ref$47.orientedSSId, 0, this.<>f__ref$47.$this.m_threeDStarMapCtrl.GetSwitchToStarfieldFadeInTimeLength());
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetFadeInProcessExecutor>c__AnonStorey2D
        {
            internal Action<bool> execEnd;

            internal void <>m__0()
            {
                if (this.execEnd != null)
                {
                    this.execEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetFadeOutProcessExecutor>c__AnonStorey2C
        {
            internal Action<bool> execEnd;

            internal void <>m__0()
            {
                if (this.execEnd != null)
                {
                    this.execEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <MenuItemClick_QuestSignal>c__AnonStorey2B
        {
            internal int questInsId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                if (this.$this.EventOnStrikeForQuest != null)
                {
                    this.$this.EventOnStrikeForQuest((ulong) this.questInsId);
                }
            }

            internal void <>m__1(bool result)
            {
                this.$this.ShowMainUI(true);
                this.$this.StartUpdatePipeLine(null, false, false, true, res => UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterStarMapForSolarSytemUI, new object[0]));
            }

            internal void <>m__2(bool res)
            {
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterStarMapForSolarSytemUI, new object[0]);
            }

            internal void <>m__3(bool res)
            {
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterStarMapForSolarSytemUI, new object[0]);
            }
        }

        [CompilerGenerated]
        private sealed class <OnDelegateSignalRewardButtonButtonClick>c__AnonStorey1F
        {
            internal DelegateSignalMenuItemUIController ctrl;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                if (!((GetDataForVirtuelBuffReqNetTask) task).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    List<VirtualBuffDesc> virtualBuffsInfluenceToDelegateMission = ((LogicBlockSignalClient) this.$this.PlayerCtx.GetLBSignal()).GetVirtualBuffsInfluenceToDelegateMission(this.$this.PlayerCtx.GetLBSignal().GetSignalByInstanceId(this.ctrl.GetInstanceId()) as LBSignalDelegateBase);
                    if (virtualBuffsInfluenceToDelegateMission.Count > 0)
                    {
                        VirtualBuffDetailUITask.StartSolarSystemBuffDetailUITask(virtualBuffsInfluenceToDelegateMission, "DelegateMission", VirtualBuffDetailUITask.m_defultPos, VirtualBuffDetailUITask.m_centerPivot);
                    }
                    this.ctrl.SetRewardBouus(virtualBuffsInfluenceToDelegateMission);
                    this.$this.m_mainCtrl.SetVirtualBuffIcon(this.$this.PlayerCtx.GetLBStarMapInfo().GetVirtualBuffsFromSolarSystem(this.$this.m_solarSystemId), this.$this.m_dynamicResCacheDict);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildSentryHudItemClick>c__AnonStorey21
        {
            internal ulong sceneInstanceId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(true, true);
                this.$this.ExpandMenuItemToGuildSentryItem(this.sceneInstanceId);
            }
        }

        [CompilerGenerated]
        private sealed class <OnInfectDetailInfoInfoButtonClick>c__AnonStorey0
        {
            internal MenuItemUIControllerBase infectMenuItem;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(true);
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.infectMenuItem);
            }
        }

        [CompilerGenerated]
        private sealed class <OnInfectDetailInfoInfoButtonClick>c__AnonStorey1
        {
            internal MenuItemUIControllerBase infectMenuItem;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(true);
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.infectMenuItem);
            }
        }

        [CompilerGenerated]
        private sealed class <OnMenuItemIconButtonClickImp>c__AnonStorey1D
        {
            internal MenuItemUIControllerBase ctrl;
            internal Action<bool> onEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                if (!((GetDataForVirtuelBuffReqNetTask) task).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    ((DelegateSignalMenuItemUIController) this.ctrl).SetRewardBouus(null);
                }
            }

            internal void <>m__1()
            {
                this.$this.EnableUIInput(true);
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.ctrl);
                switch (this.ctrl.GetMenuItemType())
                {
                    case MenuItemType.CelestialPlanet:
                        this.$this.MenuItemClick_CelestialPlanet(this.ctrl as CelestialPlanetMenuItemUIController);
                        break;

                    case MenuItemType.CelestialStar:
                        this.$this.MenuItemClick_CelestialStar(this.ctrl as CelestialStarMenuItemUIController);
                        break;

                    case MenuItemType.DelegateSignal:
                        this.$this.MenuItemClick_DelegateSignal(this.ctrl as DelegateSignalMenuItemUIController);
                        break;

                    case MenuItemType.PvpSignal:
                        this.$this.MenuItemClick_PvpSignal(this.ctrl as PvpSignalMenuItemUIController);
                        break;

                    case MenuItemType.QuestSignal:
                        this.$this.MenuItemClick_QuestSignal(this.ctrl as QuestSignalMenuItemUIController);
                        break;

                    case MenuItemType.Quest:
                        this.$this.MenuItemClick_Quest(this.ctrl as QuestMenuItemUIController);
                        break;

                    case MenuItemType.SpaceStation:
                        this.$this.MenuItemClick_SpaceStation(this.ctrl as SpaceStationMenuItemUIController);
                        break;

                    case MenuItemType.Stargate:
                        this.$this.MenuItemClick_Stargate(this.ctrl as StargateMenuItemUIController);
                        break;

                    case MenuItemType.Wormhole:
                        this.$this.MenuItemClick_Wormhole(this.ctrl as WormholeMenuItemUIController);
                        break;

                    case MenuItemType.UnknownWormhole:
                        this.$this.MenuItemClick_UnknowWormhole(this.ctrl as UnknownWormholeMenuItemUIController);
                        break;

                    case MenuItemType.Infect:
                        this.$this.MenuItemClick_Infect(this.ctrl as InfectMenuItemUIController);
                        break;

                    case MenuItemType.Rescue:
                        this.$this.MenuItemClick_Rescue(this.ctrl as RescueMenuItemUIController);
                        break;

                    case MenuItemType.GuildSentrySignal:
                        this.$this.MenuItemClick_GuildSentry(this.ctrl as GuildSentrySignalMenuItemUIController);
                        break;

                    case MenuItemType.SpaceSignal:
                        this.$this.MenuItemClick_SpaceSignal(this.ctrl as SpaceSignalMenuItemUIController);
                        break;

                    default:
                        break;
                }
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnPopupMenuTabClicked>c__AnonStorey1C
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestAssignButtonClickImp>c__AnonStorey1E
        {
            internal ulong questInstanceId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                if (this.$this.EventOnStrikeForQuest != null)
                {
                    this.$this.EventOnStrikeForQuest(this.questInstanceId);
                }
                else
                {
                    Debug.LogError("OnQuestAssignButtonClick Event is NULL ______________________________");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareEnterSolarSystem>c__AnonStorey17
        {
            internal Action<bool> onPrepareEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(bool res)
            {
                if (this.$this.State == Task.TaskState.Running)
                {
                    ((UIIntentCustom) this.$this.m_currIntent).SetParam("UpdateSolarSystemInfoPanel", true);
                    this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                }
            }

            internal void <>m__1(bool res)
            {
                this.$this.m_wormholeInfoIsReady = true;
                this.$this.OnPrepareForStartOrResumeComplete(this.onPrepareEnd);
            }

            internal void <>m__2(bool res)
            {
                this.$this.m_infectInfoIsReady = true;
                this.$this.OnPrepareForStartOrResumeComplete(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <RefreshDelegateSignalListFromServer>c__AnonStorey22
        {
            internal Action<bool> onEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                DelegateSignalScanReqNetTask task2 = task as DelegateSignalScanReqNetTask;
                if (((task2 != null) && !task2.IsNetworkError) && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.m_signalScanResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_signalScanResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else
                    {
                        UIIntentCustom currIntent = this.$this.m_currIntent as UIIntentCustom;
                        if (currIntent != null)
                        {
                            currIntent.SetParam("SolarSystemId", this.$this.PlayerCtx.CurrSolarSystemId);
                            currIntent.SetParam("IsNeedAutoScanPVE", true);
                            this.$this.m_currIntent = null;
                            this.$this.StartUpdatePipeLine(currIntent, false, false, true, this.onEnd);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RefreshGuildSentrySignalListFromServer>c__AnonStorey25
        {
            internal LogicBlockGuildClient lbGuild;
            internal Action<bool> onEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                GuildSentryProbeUseReqNetTask task2 = task as GuildSentryProbeUseReqNetTask;
                if (((task2 != null) && !task2.IsNetworkError) && (this.$this.State == Task.TaskState.Running))
                {
                    ConfigDataConfigableConst configDataConfigableConst = ConfigDataHelper.ConfigDataLoader.GetConfigDataConfigableConst(0xd6);
                    if (this.lbGuild != null)
                    {
                        this.lbGuild.UpdateGuildSentryProbeAvailableTime(this.$this.PlayerCtx.GetCurrServerTime().AddSeconds((double) configDataConfigableConst.Value));
                    }
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else
                    {
                        UIIntentCustom currIntent = this.$this.m_currIntent as UIIntentCustom;
                        if (currIntent != null)
                        {
                            currIntent.SetParam("SolarSystemId", this.$this.PlayerCtx.CurrSolarSystemId);
                            currIntent.SetParam("IsNeedAutoGuildSentrySignal", true);
                            this.$this.m_isScanGuildSentrySingal = true;
                            this.$this.m_currIntent = null;
                            this.$this.StartUpdatePipeLine(currIntent, false, false, true, this.onEnd);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RefreshMannualSignalListFromServer>c__AnonStorey24
        {
            internal Action<bool> onEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                MannualSignalScanReqNetTask task2 = task as MannualSignalScanReqNetTask;
                if (((task2 != null) && !task2.IsNetworkError) && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.m_signalScanResult != 0)
                    {
                        this.$this.EnableUIInput(true);
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_signalScanResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else
                    {
                        UIIntentCustom currIntent = this.$this.m_currIntent as UIIntentCustom;
                        if (currIntent != null)
                        {
                            currIntent.SetParam("SolarSystemId", this.$this.PlayerCtx.CurrSolarSystemId);
                            currIntent.SetParam("IsNeedAutoScanManual", true);
                            this.$this.m_currIntent = null;
                            this.$this.StartUpdatePipeLine(currIntent, false, false, true, this.onEnd);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RefreshPvpSignalListFromServer>c__AnonStorey23
        {
            internal Action<bool> onEnd;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(Task task)
            {
                PVPSignalScanReqNetTask task2 = task as PVPSignalScanReqNetTask;
                if (((task2 != null) && !task2.IsNetworkError) && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.m_signalScanResult != 0)
                    {
                        this.$this.EnableUIInput(true);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_signalScanResult, true, false);
                    }
                    else
                    {
                        UIIntentCustom currIntent = this.$this.m_currIntent as UIIntentCustom;
                        if (currIntent != null)
                        {
                            currIntent.SetParam("SolarSystemId", this.$this.PlayerCtx.CurrSolarSystemId);
                            currIntent.SetParam("ISNeedAutoScanPVP", true);
                            this.$this.m_currIntent = null;
                            this.$this.StartUpdatePipeLine(currIntent, false, false, true, this.onEnd);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelectCelestialPlanetTab>c__AnonStorey7
        {
            internal Action<bool> onSucess;

            internal void <>m__0(bool inRect)
            {
                this.onSucess(true);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelectPlanet>c__AnonStorey9
        {
            internal Action<bool> onSucess;

            internal void <>m__0(bool inRect)
            {
                this.onSucess(true);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelectStar>c__AnonStoreyB
        {
            internal Action<bool> onSucess;

            internal void <>m__0(bool inRect)
            {
                this.onSucess(true);
            }
        }

        [CompilerGenerated]
        private sealed class <SendStarfieldSolarSystemInfectInfoReqNetTask>c__AnonStorey27
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(!((NetWorkTransactionTask) task).IsNetworkError);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendStarFieldWormholeGateInfoReqNetTask>c__AnonStorey28
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(!((NetWorkTransactionTask) task).IsNetworkError);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetLocationItemPos>c__AnonStorey1B
        {
            internal int stationId;

            internal bool <>m__0(GDBSpaceStationInfo elem) => 
                (elem.Id == this.stationId);
        }

        [CompilerGenerated]
        private sealed class <ShowMsgThenGoFight>c__AnonStorey20
        {
            internal PvpSignalMenuItemUIController ctrl;
            internal Queue<string> msgQueue;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.ShowMsgThenGoFight(this.ctrl, this.msgQueue);
            }
        }

        [CompilerGenerated]
        private sealed class <StartStarMapForSolarSystemUITask>c__AnonStorey16
        {
            internal Action<bool> onEnd;

            internal void <>m__0(bool ret)
            {
                if (!ret && (this.onEnd != null))
                {
                    this.onEnd(false);
                }
            }

            internal void <>m__1(bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SwitchToEnotherSolarSystem>c__AnonStorey2A
        {
            internal int solarSystemId;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    CustomUIProcess.UIProcessExecutor[] executorList = new CustomUIProcess.UIProcessExecutor[] { this.$this.GetFadeOutProcessExecutor() };
                    this.$this.PlayUIProcess(UIProcessFactory.CreateExecutorProcess(UIProcess.ProcessExecMode.Serial, executorList), true, delegate (UIProcess p, bool b) {
                        CustomUIProcess.UIProcessExecutor[] executorArray1 = new CustomUIProcess.UIProcessExecutor[] { this.$this.GetFadeInProcessExecutor() };
                        this.$this.PlayUIProcess(UIProcessFactory.CreateExecutorProcess(UIProcess.ProcessExecMode.Serial, executorArray1), true, null, false);
                        this.$this.m_currPipeLineCtx.AddUpdateMask(0);
                        ((UIIntentCustom) this.$this.m_currIntent).SetParam("SolarSystemId", this.solarSystemId);
                        this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                    }, false);
                }
            }

            internal void <>m__1(UIProcess p, bool b)
            {
                CustomUIProcess.UIProcessExecutor[] executorList = new CustomUIProcess.UIProcessExecutor[] { this.$this.GetFadeInProcessExecutor() };
                this.$this.PlayUIProcess(UIProcessFactory.CreateExecutorProcess(UIProcess.ProcessExecMode.Serial, executorList), true, null, false);
                this.$this.m_currPipeLineCtx.AddUpdateMask(0);
                ((UIIntentCustom) this.$this.m_currIntent).SetParam("SolarSystemId", this.solarSystemId);
                this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <TickPlayerPosInfoOn3DStarMap>c__AnonStorey19
        {
            internal int motherShipSpaceStationId;
            internal StarMapForSolarSystemUITask $this;

            internal bool <>m__0(GDBSpaceStationInfo elem) => 
                (elem.Id == this.motherShipSpaceStationId);
        }

        [CompilerGenerated]
        private sealed class <TickPlayerPosInfoOn3DStarMap>c__AnonStorey1A
        {
            internal GDBSpaceStationInfo commanderSpaceStation;
            internal StarMapForSolarSystemUITask.<TickPlayerPosInfoOn3DStarMap>c__AnonStorey19 <>f__ref$25;

            internal bool <>m__0(GDBSpaceStationInfo elem) => 
                (elem.Id == this.<>f__ref$25.$this.PlayerCtx.CurrClentSpaceStationContext.SpaceStationID);

            internal bool <>m__1(GDBPlanetInfo elem) => 
                (elem.Id == this.commanderSpaceStation.WithPlanetId);
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelectPlanet>c__AnonStorey8
        {
            internal Action<bool> onSucess;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                if (this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialPlanet) != null)
                {
                    this.$this.RepeatableForSelectPlanet(this.onSucess);
                }
                else
                {
                    this.$this.m_userGuideStateForExploreQuest = StarMapForSolarSystemUITask.ExploreQuestUserGuideState.Finding;
                    this.$this.ExecAfterTicks(delegate {
                        if (this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialPlanet) == null)
                        {
                            this.onSucess(false);
                        }
                        else
                        {
                            this.$this.RepeatableForSelectPlanet(this.onSucess);
                        }
                    }, (ulong) 15);
                }
            }

            internal void <>m__1()
            {
                if (this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialPlanet) == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelectPlanet(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelectPlanetTab>c__AnonStorey6
        {
            internal Action<bool> onSucess;
            internal bool isPassEvent;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                if (this.$this.m_mainCtrl.m_popupMenuCtrl.m_celestialTab == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelectCelestialPlanetTab(this.isPassEvent, this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelectStar>c__AnonStoreyA
        {
            internal Action<bool> onSucess;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                if ((this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialStar) != null) || (this.$this.m_userGuideStateForExploreQuest != StarMapForSolarSystemUITask.ExploreQuestUserGuideState.Start))
                {
                    this.$this.RepeatableForSelectStar(this.onSucess);
                }
                else
                {
                    this.$this.m_userGuideStateForExploreQuest = StarMapForSolarSystemUITask.ExploreQuestUserGuideState.Finding;
                    this.$this.ExecAfterTicks(delegate {
                        if (this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialStar) == null)
                        {
                            this.onSucess(false);
                        }
                        else
                        {
                            this.$this.RepeatableForSelectStar(this.onSucess);
                        }
                    }, (ulong) 15);
                }
            }

            internal void <>m__1()
            {
                if (this.$this.m_mainCtrl.m_popupMenuCtrl.GetFirstScrollViewItemByType(MenuItemType.CelestialStar) == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelectStar(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForDelegateSignalFight_FirstSignalMenuItem>c__AnonStorey2
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForDelegateSignalFight_FirstSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForDelegateSignalFight_FirstSignalStrikeButton>c__AnonStorey3
        {
            internal int itemIndex;
            internal DelegateSignalMenuItemUIController signalMenuItemCtrl;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.$this.m_mainCtrl.m_popupMenuCtrl.GetScrollViewItemCtrlByIndex(this.itemIndex));
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForFirstDelegateSignalFightMenuItemStrikeButton(this.signalMenuItemCtrl), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideDelegateSignalFight), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalMenuItem>c__AnonStorey4
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForDelegateSignalMineral_FirstSignalStrikeButton>c__AnonStorey5
        {
            internal int itemIndex;
            internal DelegateSignalMenuItemUIController signalMenuItemCtrl;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.$this.m_mainCtrl.m_popupMenuCtrl.GetScrollViewItemCtrlByIndex(this.itemIndex));
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForFirstDelegateSignalMineralMenuItemStrikeButton(this.signalMenuItemCtrl), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideDelegateSignalMineral), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForFreeQuest_FirstQuestMenuItem>c__AnonStoreyC
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForFreeQuest_FirstQuestMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForFreeQuest_FirstQuestSignalMenuItem>c__AnonStoreyD
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForFreeQuest_FirstQuestSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForFreeQuest_FirstQuestStrikeButton>c__AnonStoreyE
        {
            internal QuestMenuItemUIController questItem;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.questItem);
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForQuestMenuItemStrikeButton(this.questItem), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideFreeQuest), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForInfect_FirstQuestMenuItem>c__AnonStoreyF
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForInfect_FirstQuestMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForInfect_FirstQuestSignalMenuItem>c__AnonStorey10
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForInfect_FirstQuestSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForPVPSignal_FirstSignalMenuItem>c__AnonStorey12
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForPVPSignal_FirstSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForPVPSignal_FirstSignalStrikeButton>c__AnonStorey13
        {
            internal int itemIndex;
            internal PvpSignalMenuItemUIController signalMenuItemCtrl;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.$this.m_mainCtrl.m_popupMenuCtrl.GetScrollViewItemCtrlByIndex(this.itemIndex));
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForFirstPVPSignalMenuItemStrikeButton(this.signalMenuItemCtrl), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuidePVPSignal), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForQuest_FirstQuestStrikeButton>c__AnonStorey11
        {
            internal QuestMenuItemUIController questItem;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.questItem);
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForQuestMenuItemStrikeButton(this.questItem), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideInfect), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForSpaceSignal_FirstSpaceSignalMenuItem>c__AnonStorey14
        {
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForSpaceSignal_FirstSpaceSignalMenuItem(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForWormhole_WormholeStrikeButton>c__AnonStorey15
        {
            internal MenuItemUIControllerBase wormholeItem;
            internal Action onTriggerFinished;
            internal StarMapForSolarSystemUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.wormholeItem);
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForWormholeStrikeButton(this.wormholeItem), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideWormhole), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }

            internal void <>m__1()
            {
                this.$this.m_mainCtrl.m_popupMenuCtrl.SetViewScrollToMakeItemOnBottom(this.wormholeItem);
                RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(this.$this.MainLayer.LayerCamera, this.$this.GetUIRectForWormholeStrikeButton(this.wormholeItem), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideWormhole), true);
                if (this.onTriggerFinished != null)
                {
                    this.onTriggerFinished();
                }
            }

            internal void <>m__2()
            {
                this.$this.TryToTriggerUserGuideForWormhole_WormholeStrikeButton(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdatePopupMenuTabInfo>c__AnonStorey18
        {
            internal Action onUpdateEnd;
            internal StarMapForSolarSystemUITask $this;

            internal bool <>m__0(LBProcessingQuestBase elem) => 
                (((elem.GetCompleteSolarSystemId() == this.$this.m_solarSystemId) && (elem.IsFreeSignalQuest() || elem.IsStoryQuest())) && !elem.IsQuestFail());

            internal bool <>m__1(LBSignalManualBase elem) => 
                (elem.GetSolarSystemId() == this.$this.m_solarSystemId);

            internal bool <>m__2(LBSignalManualBase elem) => 
                (elem.GetSolarSystemId() == this.$this.m_solarSystemId);

            internal bool <>m__3(LBSignalDelegateBase elem) => 
                (elem.GetSolarSystemId() == this.$this.m_solarSystemId);

            internal bool <>m__4(SignalInfo item) => 
                (item.m_expiryTime <= this.$this.PlayerCtx.GetCurrServerTime());

            internal void <>m__5(UIProcess p, bool r)
            {
                if (this.onUpdateEnd != null)
                {
                    this.onUpdateEnd();
                }
            }

            internal void <>m__6(UIProcess p, bool r)
            {
                if (this.onUpdateEnd != null)
                {
                    this.onUpdateEnd();
                }
            }
        }

        internal enum DelegateSignalFightUserGuideState
        {
            Start,
            FocusOnFirstSignalMenuItem,
            FocusOnFirstSignalStrikeButton,
            FocusOnScanProbeButton,
            End
        }

        internal enum DelegateSignalMineralUserGuideState
        {
            Start,
            FocusOnFirstSignalMenuItem,
            FocusOnFirstSignalStrikeButton,
            FocusOnScanProbeButton,
            End
        }

        internal enum ExploreQuestUserGuideState
        {
            Start,
            Finding,
            End
        }

        internal enum FreeQuestUserGuideState
        {
            Start,
            FocusOnFirstQuestMenuItem,
            FocusOnQuestSignalScanProbo,
            FocusOnFirstQuestStrikeButton,
            FocusOnFirstQuestSignalMenuItem,
            End
        }

        internal enum InfectUserGuideState
        {
            Start,
            FocusOnFirstQuestMenuItem,
            FocusOnQuestSignalScanProbo,
            FocusOnFirstQuestStrikeButton,
            FocusOnFirstQuestSignalMenuItem,
            End
        }

        internal enum PVPSignalUserGuideState
        {
            Start,
            FocusOnFirstSignalMenuItem,
            FocusOnFirstSignalStrikeButton,
            FocusOnScanProbeButton,
            End
        }

        public class RepeatableUserGuideIntent
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsDelegateSignalFightUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsDelegateSignalMineralUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsWormholeUserGuideActive>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsInfectUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsFreeQuestUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsExploreQuestPlanetCountUserGuideActive>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsExploreQuestStarUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsExploreQuestPlanetUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsPVPSignalUserGuideActive>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private QuestType <FreeQuestTypeForUserGuide>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsSpaceSignalScanUserGuideActive>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_IsDelegateSignalFightUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsDelegateSignalFightUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsDelegateSignalMineralUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsDelegateSignalMineralUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsWormholeUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsWormholeUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsInfectUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsInfectUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsFreeQuestUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsFreeQuestUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsExploreQuestPlanetCountUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsExploreQuestPlanetCountUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsExploreQuestStarUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsExploreQuestStarUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsExploreQuestPlanetUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsExploreQuestPlanetUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsPVPSignalUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsPVPSignalUserGuideActive;
            private static DelegateBridge __Hotfix_get_FreeQuestTypeForUserGuide;
            private static DelegateBridge __Hotfix_set_FreeQuestTypeForUserGuide;
            private static DelegateBridge __Hotfix_get_IsSpaceSignalScanUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsSpaceSignalScanUserGuideActive;

            public bool IsDelegateSignalFightUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsDelegateSignalMineralUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsWormholeUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsInfectUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsFreeQuestUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsExploreQuestPlanetCountUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsExploreQuestStarUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsExploreQuestPlanetUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsPVPSignalUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public QuestType FreeQuestTypeForUserGuide
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsSpaceSignalScanUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }
        }

        public class SignalAutoScanIntent
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsNeedAutoScanPVESignal>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsNeedAutoScanPVPSignal>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsNeedAutoScanManualSignal>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsNeedAutoScanGuildSentrySignal>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsNeedAutoScanSpaceSignal>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private string <ScanSpaceSignalResult>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_IsNeedAutoScanPVESignal;
            private static DelegateBridge __Hotfix_set_IsNeedAutoScanPVESignal;
            private static DelegateBridge __Hotfix_get_IsNeedAutoScanPVPSignal;
            private static DelegateBridge __Hotfix_set_IsNeedAutoScanPVPSignal;
            private static DelegateBridge __Hotfix_get_IsNeedAutoScanManualSignal;
            private static DelegateBridge __Hotfix_set_IsNeedAutoScanManualSignal;
            private static DelegateBridge __Hotfix_get_IsNeedAutoScanGuildSentrySignal;
            private static DelegateBridge __Hotfix_set_IsNeedAutoScanGuildSentrySignal;
            private static DelegateBridge __Hotfix_get_IsNeedAutoScanSpaceSignal;
            private static DelegateBridge __Hotfix_set_IsNeedAutoScanSpaceSignal;
            private static DelegateBridge __Hotfix_get_ScanSpaceSignalResult;
            private static DelegateBridge __Hotfix_set_ScanSpaceSignalResult;

            public SignalAutoScanIntent()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public bool IsNeedAutoScanPVESignal
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsNeedAutoScanPVESignal;
                    return ((bridge == null) ? this.<IsNeedAutoScanPVESignal>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_IsNeedAutoScanPVESignal;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsNeedAutoScanPVESignal>k__BackingField = value;
                    }
                }
            }

            public bool IsNeedAutoScanPVPSignal
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsNeedAutoScanPVPSignal;
                    return ((bridge == null) ? this.<IsNeedAutoScanPVPSignal>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_IsNeedAutoScanPVPSignal;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsNeedAutoScanPVPSignal>k__BackingField = value;
                    }
                }
            }

            public bool IsNeedAutoScanManualSignal
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsNeedAutoScanManualSignal;
                    return ((bridge == null) ? this.<IsNeedAutoScanManualSignal>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_IsNeedAutoScanManualSignal;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsNeedAutoScanManualSignal>k__BackingField = value;
                    }
                }
            }

            public bool IsNeedAutoScanGuildSentrySignal
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsNeedAutoScanGuildSentrySignal;
                    return ((bridge == null) ? this.<IsNeedAutoScanGuildSentrySignal>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_IsNeedAutoScanGuildSentrySignal;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsNeedAutoScanGuildSentrySignal>k__BackingField = value;
                    }
                }
            }

            public bool IsNeedAutoScanSpaceSignal
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsNeedAutoScanSpaceSignal;
                    return ((bridge == null) ? this.<IsNeedAutoScanSpaceSignal>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_IsNeedAutoScanSpaceSignal;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsNeedAutoScanSpaceSignal>k__BackingField = value;
                    }
                }
            }

            public string ScanSpaceSignalResult
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_ScanSpaceSignalResult;
                    return ((bridge == null) ? this.<ScanSpaceSignalResult>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_ScanSpaceSignalResult;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<ScanSpaceSignalResult>k__BackingField = value;
                    }
                }
            }
        }

        internal enum SpaceSignalScanUserGuideState
        {
            Start,
            FocusOnFirstSignalMenuItem,
            StaticSpaceSignalLayer,
            End
        }

        internal enum WormholeUserGuideState
        {
            Start,
            FocusOnWormholeStrikeButton,
            End
        }
    }
}

