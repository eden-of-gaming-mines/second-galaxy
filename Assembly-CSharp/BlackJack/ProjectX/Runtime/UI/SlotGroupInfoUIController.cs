﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SlotGroupInfoUIController : UIControllerBase
    {
        protected CommonItemIconUIController m_itemCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./AmmoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoRoot;
        [AutoBind("./EmptySlotImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EmptySlotImage;
        [AutoBind("./AmmoRoot/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HighSlotBulletValue;
        [AutoBind("./AmmoRoot/BulletProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image HighSlotBulletPB;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitSlotGroupInfo;
        private static DelegateBridge __Hotfix_SetSlotState;

        [MethodImpl(0x8000)]
        public void InitSlotGroupInfo(LBStaticWeaponEquipSlotGroup slotGroup, LogicBlockShipCompStaticResource lbShipResource, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetSlotState(bool isEmptySlot)
        {
        }
    }
}

