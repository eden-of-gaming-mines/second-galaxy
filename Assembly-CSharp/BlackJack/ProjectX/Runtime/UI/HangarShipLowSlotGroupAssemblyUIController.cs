﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class HangarShipLowSlotGroupAssemblyUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnLowSlotAssemblyItemButtonClick;
        private List<ShipWeaponEquipSlotItemUIController> m_lowSlotCtrls;
        private const int m_lowSlotGroupMaxCount = 9;
        private string m_hangarShipLowSlotItemAssetName;
        private bool m_isLowSlotAssemblyPanelShow;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImage/FrameImage/LowSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lowSlotGroupDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreatePopupWndWindowProcess;
        private static DelegateBridge __Hotfix_CreatePopupWndCustomProcess;
        private static DelegateBridge __Hotfix_UpdateLowSlotGroup;
        private static DelegateBridge __Hotfix_GetCurrentSlotRecommendState;
        private static DelegateBridge __Hotfix_ShowLowSlotEquipedEffect;
        private static DelegateBridge __Hotfix_GetRectTranformBySlotIndex;
        private static DelegateBridge __Hotfix_OnLowSlotAssemblyItemButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLowSlotAssemblyItemButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLowSlotAssemblyItemButtonClick;
        private static DelegateBridge __Hotfix_get_IsLowSlotAssemblyPanelShow;

        public event Action<int> EventOnLowSlotAssemblyItemButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatePopupWndCustomProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, Action onStart = null, Action onEnd = null, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatePopupWndWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetCurrentSlotRecommendState(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticShip shipInfo, bool isPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetRectTranformBySlotIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotAssemblyItemButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLowSlotEquipedEffect(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLowSlotGroup(ILBStaticShip staticShip, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isPlayerShip = true)
        {
        }

        public bool IsLowSlotAssemblyPanelShow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreatePopupWndCustomProcess>c__AnonStorey0
        {
            internal Action onStart;
            internal bool isShow;
            internal bool immediateComplete;
            internal bool allowToRefreshSameState;
            internal Action onEnd;
            internal HangarShipLowSlotGroupAssemblyUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Action<bool> end)
            {
            }

            private sealed class <CreatePopupWndCustomProcess>c__AnonStorey1
            {
                internal Action<bool> end;
                internal HangarShipLowSlotGroupAssemblyUIController.<CreatePopupWndCustomProcess>c__AnonStorey0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0(bool res)
                {
                }
            }
        }
    }
}

