﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class WarBothSidesInfoUIController : UIControllerBase
    {
        [AutoBind("./LeftSovereigntyGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_sovereignSideLogoCtrl;
        [AutoBind("./RightSovereigntyGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_attackSideLogoCtrl;
        [AutoBind("./StarAngBuildingTitleBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BGImageStateCtrl;
        [AutoBind("./LeftSovereigntyGroup/SovereignSideGuildCodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sovereignSideGuildCodeText;
        [AutoBind("./LeftSovereigntyGroup/SovereignSideGuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sovereignSideGuildNameText;
        [AutoBind("./RightSovereigntyGroup/AttackSideGuildCodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_attackSideGuildCodeText;
        [AutoBind("./RightSovereigntyGroup/AttackSideGuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_attackSideGuildNameText;
        private static DelegateBridge __Hotfix_UpdateWarBothSidesInfo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetBgImage;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetBgImage(GuildBattleType battleType, GuildBuildingType buildType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWarBothSidesInfo(GuildBattleReportInfo reportInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

