﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class FactionCreditTraceDataUIController : UIControllerBase
    {
        private string m_waveState;
        [AutoBind("./YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx YesButton;
        [AutoBind("./RecordButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecordButton;
        [AutoBind("./ViewDataGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GoStartMapButtonUIState;
        [AutoBind("./ViewDataGroup/ViewDataText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoStartMapButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./StarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StarText;
        [AutoBind("./PlanetTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlanetTitleText;
        [AutoBind("./ComparisonTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ComparisonTitleTextUIState;
        [AutoBind("./BGImages/TitleGroup/Title+I/IconGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IconImageButton;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseTipButton;
        [AutoBind("./BGImages/CheckWaveImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WaveImageState;
        [AutoBind("./BGImages/CheckWaveImage/Fixed/Right", AutoBindAttribute.InitState.NotInit, false)]
        public Image FixedRightImage;
        [AutoBind("./BGImages/CheckWaveImage/Fixed/Wrong", AutoBindAttribute.InitState.NotInit, false)]
        public Image FixedWrongImage;
        [AutoBind("./BGImages/CheckWaveImage/Fixed", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FixedImageState;
        [AutoBind("./BGImages/CheckWaveImage/Planet/Right", AutoBindAttribute.InitState.NotInit, false)]
        public Image PlanetRightImage;
        [AutoBind("./BGImages/CheckWaveImage/Planet/Wrong", AutoBindAttribute.InitState.NotInit, false)]
        public Image PlanetWrongImage;
        [AutoBind("./BGImages/CheckWaveImage/Planet", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PlanetImageState;
        [AutoBind("./BGImages/CheckWaveImage/Galaxy/Right", AutoBindAttribute.InitState.NotInit, false)]
        public Image GalaxyRightImage;
        [AutoBind("./BGImages/CheckWaveImage/Galaxy/Wrong", AutoBindAttribute.InitState.NotInit, false)]
        public Image GalaxyWrongImage;
        [AutoBind("./BGImages/CheckWaveImage/Galaxy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GalaxyImageState;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoPanelUIState;
        [AutoBind("./DetailInfoPanel/FrameImage/BGImage/DetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoPanelDeatailUIState;
        [AutoBind("./BGImages/TitleGroup/Title+I", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TitlelUIState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateViewScan;
        private static DelegateBridge __Hotfix_GetStringQuestConName;
        private static DelegateBridge __Hotfix_SetCurStateImage;
        private static DelegateBridge __Hotfix_UpdateViewCheckEnd;

        [MethodImpl(0x8000)]
        private string GetStringQuestConName(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurStateImage(float val, bool checkResult, bool end = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewCheckEnd(bool reslut)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewScan(string waveState, bool result, QuestCompleteCondInfo condInfo, ConfigDataQuestInfo questInfo)
        {
        }
    }
}

