﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class RechargeItemShopListItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnRechargeItemBuyButtonClick;
        private LBRechargeItem m_rechargeItem;
        private static string m_rechargeItemfirstBonusDesc;
        private static string m_normalBonusDesc;
        [AutoBind("./BuyButton/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonPriceText;
        [AutoBind("./BuyButton/IconGroup/IconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonCurrencyText;
        [AutoBind("./BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./BonusValueRoot/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BonusValueNumberText;
        [AutoBind("./TotalValueNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalValueNumberText;
        [AutoBind("./BonusValueRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BonusValueRoot;
        [AutoBind("./ExtraRewardRoot/ExtraRewardText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ExtraRewardText;
        [AutoBind("./ExtraRewardRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ExtraRewardStateCtrl;
        [AutoBind("./AnimationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EffectStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Image BGImage;
        [AutoBind("./ExtraRewardRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExtraRewardRoot;
        private static DelegateBridge __Hotfix_UpdateRechageItem;
        private static DelegateBridge __Hotfix_ShowOrHideEffect;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnRechargeItemBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRechargeItemBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRechargeItemBuyButtonClick;
        private static DelegateBridge __Hotfix_get_RechargeItemfirstItemBonusDesc;
        private static DelegateBridge __Hotfix_set_RechargeItemfirstItemBonusDesc;
        private static DelegateBridge __Hotfix_get_RechargetItemNormalBonusDesc;
        private static DelegateBridge __Hotfix_set_RechargetItemNormalBonusDesc;

        public event Action<int> EventOnRechargeItemBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeItemBuyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideEffect(bool isShow, int index, int total)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRechageItem(LBRechargeItem rechargeItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private static string RechargeItemfirstItemBonusDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private static string RechargetItemNormalBonusDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

