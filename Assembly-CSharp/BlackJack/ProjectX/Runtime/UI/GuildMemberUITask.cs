﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class GuildMemberUITask : GuildUITaskBase
    {
        private GuildMemberUIController m_memberUICtrl;
        private SubSysType m_curSubSysType;
        private GuildMemberListUITask m_memberListTask;
        private GuildMemberStaffingLogUITask m_staffingLogTask;
        private GuildMemberApplyForListUITask m_applyForTask;
        private GuildMemberJobCtrlUITask m_jobCtrlTask;
        private bool m_isMemberListResLoadComplete;
        private bool m_isStaffingLogResLoadComplete;
        private bool m_isApplyForListLoadComplete;
        private bool m_isJobCtrlLoadComplete;
        private Dictionary<CharacterSimpleInfoUITask.ButtonType, CharacterSimpleInfoUITask.ButtonState> m_CharSimpleBtnStatesDic;
        private bool m_isReturnPretaskWithPrepare;
        private IUIBackgroundManager m_backgroundManager;
        private UITaskBase.LayerDesc[] layerDescArray;
        private UITaskBase.UIControllerDesc[] uiCtrlDescArray;
        public const string ModeMemberList = "MemberDetail";
        public const string ModeJobCtrl = "JobCtrl";
        public const string ModeApplyList = "ApplyList";
        public const string ModeStaffingLog = "StaffingLog";
        public const string TaskName = "GuildMemberUITask";
        public const string ParamKeyReturnPreTaskWithPrepare = "ReturnPreTaskWithPrepare";
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        [CompilerGenerated]
        private static Predicate<GuildMemberInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnMemberToggleChange;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_UpdateMemberCount;
        private static DelegateBridge __Hotfix_OnMemberListResLoadComplete;
        private static DelegateBridge __Hotfix_OnStaffingLogesLoadComplete;
        private static DelegateBridge __Hotfix_OnApplyForResLoadComplete;
        private static DelegateBridge __Hotfix_OnJobCtrlResLoadComplete;
        private static DelegateBridge __Hotfix_HasAppointPermission;
        private static DelegateBridge __Hotfix_UpdateMemberFuncToogleState;
        private static DelegateBridge __Hotfix_ChangeTab;
        private static DelegateBridge __Hotfix_PauseAllSubPanel;
        private static DelegateBridge __Hotfix_ResetMembersSort;
        private static DelegateBridge __Hotfix_UpdateViewForAllRedPoints;
        private static DelegateBridge __Hotfix_OnMemberListItemClick;
        private static DelegateBridge __Hotfix_GetSimpleInfoButtonDic;
        private static DelegateBridge __Hotfix_CalSimpleInfoPanelOrientation;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_UnregisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_ExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_EditJobButtonClick;
        private static DelegateBridge __Hotfix_ReRequestApplyList;
        private static DelegateBridge __Hotfix_RegisterEvent;
        private static DelegateBridge __Hotfix_UnregisterEvent;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBackPreTask;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackToBasicTask;
        private static DelegateBridge __Hotfix_OnMemberToggleClick;
        private static DelegateBridge __Hotfix_OnJobCtrlToggleClick;
        private static DelegateBridge __Hotfix_OnApplyListToggleClick;
        private static DelegateBridge __Hotfix_OnStaffingLogToggleClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildMemberUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private CharacterSimpleInfoUITask.PostionType CalSimpleInfoPanelOrientation(Transform memberItemTrs, Vector3 touchPos)
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeTab(SubSysType type)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_EditJobButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_ExpelGuildButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private Dictionary<CharacterSimpleInfoUITask.ButtonType, CharacterSimpleInfoUITask.ButtonState> GetSimpleInfoButtonDic(GuildMemberInfo selfInfo, GuildMemberInfo memberInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasAppointPermission()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnApplyForResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplyListToggleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackPreTask(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToBasicTask(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJobCtrlResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJobCtrlToggleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberListItemClick(UIControllerBase ctrl, GuildMemberInfo memberInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMemberListResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemberToggleChange(SubSysType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberToggleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStaffingLogesLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStaffingLogToggleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void PauseAllSubPanel()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ReRequestApplyList(int res)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetMembersSort()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberCount()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMemberFuncToogleState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewForAllRedPoints()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CharSimpleTask_ExpelGuildButtonClick>c__AnonStorey1
        {
            internal ProPlayerSimplestInfo playerInfo;
            internal GuildMemberUITask $this;

            internal void <>m__0()
            {
                GuildMemberRemoveReqNetTask task = new GuildMemberRemoveReqNetTask(this.playerInfo.PlayerGameUserId);
                task.EventOnStop += delegate (Task task) {
                    GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                    if ((task2 != null) && !task2.IsNetworkError)
                    {
                        if (task2.Result != 0)
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                        else
                        {
                            GuildMemberUITask.SubSysType curSubSysType = this.$this.m_curSubSysType;
                            if (curSubSysType == GuildMemberUITask.SubSysType.JobCtrl)
                            {
                                this.$this.m_jobCtrlTask.ExpelGuildSucess();
                            }
                            else if (curSubSysType == GuildMemberUITask.SubSysType.MemberList)
                            {
                                this.$this.m_memberListTask.ExpelGuildSucess();
                            }
                        }
                    }
                };
                task.Start(null, null);
            }

            internal void <>m__1(Task task)
            {
                GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        GuildMemberUITask.SubSysType curSubSysType = this.$this.m_curSubSysType;
                        if (curSubSysType == GuildMemberUITask.SubSysType.JobCtrl)
                        {
                            this.$this.m_jobCtrlTask.ExpelGuildSucess();
                        }
                        else if (curSubSysType == GuildMemberUITask.SubSysType.MemberList)
                        {
                            this.$this.m_memberListTask.ExpelGuildSucess();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(bool memberListRet)
            {
                if (memberListRet)
                {
                    GuildMemberStaffingLogUITask.RequstLogInfo(staffingLogRet => GuildMemberApplyForListUITask.RequestApplyForList(false, delegate (bool applyForRet) {
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(true);
                        }
                    }));
                }
                else if (this.onPrepareEnd != null)
                {
                    this.onPrepareEnd(false);
                }
            }

            internal void <>m__1(bool staffingLogRet)
            {
                GuildMemberApplyForListUITask.RequestApplyForList(false, delegate (bool applyForRet) {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(true);
                    }
                });
            }

            internal void <>m__2(bool applyForRet)
            {
                if (this.onPrepareEnd != null)
                {
                    this.onPrepareEnd(true);
                }
            }
        }

        public enum PipeLineMaskType
        {
            ChangeToggleType
        }

        public enum SubSysType
        {
            MemberList,
            JobCtrl,
            StaffingLog,
            ApplyFor
        }
    }
}

