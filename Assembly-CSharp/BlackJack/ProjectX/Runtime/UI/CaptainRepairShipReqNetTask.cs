﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CaptainRepairShipReqNetTask : NetWorkTransactionTask
    {
        private ulong m_captainInstanceId;
        private int m_shipId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <RepairShipResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnRepairShip;
        private static DelegateBridge __Hotfix_set_RepairShipResult;
        private static DelegateBridge __Hotfix_get_RepairShipResult;

        [MethodImpl(0x8000)]
        public CaptainRepairShipReqNetTask(ulong captainInsId, int shipId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRepairShip(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int RepairShipResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

