﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class CharacterUIHelper
    {
        private const int IntervalLevel = 5;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_GetCircularIconFromAvatarId;
        private static DelegateBridge __Hotfix_GetSquareIconFromAvatarId;
        private static DelegateBridge __Hotfix_GetAllCharacterSquareIcon;
        private static DelegateBridge __Hotfix_SetTaskLevelState;
        private static DelegateBridge __Hotfix_CheckTaskIsNeedPrompted;
        private static DelegateBridge __Hotfix_CollectAllResForPlayer;
        private static DelegateBridge __Hotfix_GetPlayerFullName;
        private static DelegateBridge __Hotfix_GetCurrentPlayerFullName;
        private static DelegateBridge __Hotfix_GetCriminalLevelStr;
        private static DelegateBridge __Hotfix_PlayCaptainGreetingAudio;
        private static DelegateBridge __Hotfix_PlayCaptainLeaveTalkAudio;
        private static DelegateBridge __Hotfix_GetChipAreaName;
        private static DelegateBridge __Hotfix_GetSkillTreeArtResFullPath;

        [MethodImpl(0x8000)]
        public static void CheckTaskIsNeedPrompted(int needLevel, string msgBoxString, QuestType type, Action taskAction)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectAllResForPlayer(PlayerSimplestInfo player, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetAllCharacterSquareIcon(List<string> resPaths)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetChipAreaName(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCircularIconFromAvatarId(int avatarId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCriminalLevelStr(float criminalLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCurrentPlayerFullName()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPlayerFullName(string playerName, string guildCode)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSkillTreeArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSquareIconFromAvatarId(int avatarId)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayCaptainGreetingAudio(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayCaptainLeaveTalkAudio(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetTaskLevelState(CommonUIStateController stateController, int needLevel)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckTaskIsNeedPrompted>c__AnonStorey0
        {
            internal Action taskAction;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

