﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildPurchaseUIController : UIControllerBase
    {
        private readonly List<ProductCategoryController> m_productItemCategoryToggleList;
        private int m_selectedType;
        private List<ConfigDataAuctionItemInfo> m_productList;
        private List<GuildPurchaseOrderInfo> m_orderList;
        private Dictionary<NpcShopItemCategory, List<int>> m_toggleList;
        private const string Show = "Show";
        private const string Open = "Open";
        private const string Close = "Close";
        private const string PanelOpen = "PanelOpen";
        private const string PanelClose = "PanelClose";
        private const string Normal = "Normal";
        private const string Batch = "Batch";
        private const string Edit = "Edit";
        private const string Gray = "Gray";
        private const string Up = "Up";
        private const string Down = "Down";
        private const string PurchaseItemTemplate = "PurchaseItemTemplate";
        private const string PurchaseItem = "PurchaseItem";
        private const string FilterPrefab = "Filter";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> OnSelectTypeChanged;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsOpened>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsEditMode>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsBatchEditMode>k__BackingField;
        public ItemFilterUIController m_filter;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <FilterShown>k__BackingField;
        private readonly List<int> m_checkedList;
        public AuctionBuyItemFilterType m_filterType;
        private CommonItemIconUIController m_sellItemIcon;
        private CommonItemIconUIController m_editItemIcon;
        private long m_eachCost;
        private long m_orderNeedCount;
        private readonly ItemSorter m_sorter;
        private readonly OrderSorter m_orderSorter;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, int> EventOnItemIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ConfigDataAuctionItemInfo> EventOnSellPanelItemIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildPurchaseOrderInfo, long, Action> OnOrderModified;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<int>, Action> OnOrderCancelled;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, long, Action> OnOrderCreated;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, long, Action> OnOrderMatched;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action> OnDisplayGuildMoney;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action OnSellCountTooMany;
        private bool m_isSellAddLongPressing;
        private bool m_numberOverflowDuringLongPressing;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<AuctionBuyItemFilterType> OnFilterChanged;
        private const float ScrollMaxHeight = 626f;
        private const float ScrollMinHeight = 546f;
        private ConfigDataAuctionItemInfo m_operatingItem;
        private List<Text> m_dailyTextList;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backBtn;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainPanel;
        [AutoBind("./TitleText/DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipsBtn;
        [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyUIStateCtrl;
        [AutoBind("./ToggleGroup/GuildItemCategory/Viewport/Content/ItemCategoryToggle/TotalItems", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_total;
        [AutoBind("./ToggleGroup/GuildItemCategory/Viewport/Content/ItemCategoryToggle/TotalItems", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_totalCtrl;
        [AutoBind("./ToggleGroup/GuildItemCategory/Viewport/Content/ItemCategoryToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_l1ToggleRoot;
        [AutoBind("./ToggleGroup/GuildItemCategory/Viewport/Content/ItemCategoryToggle/MenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_l1Toggle;
        [AutoBind("./ToggleGroup/GuildItemCategory/Viewport/Content/ItemCategoryToggle/ItemTypeToggles", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_l2ToggleRoot;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_prodRoot;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_prodPool;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_prodScroll;
        [AutoBind("./TipsPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipsPos;
        [AutoBind("./TipsPos/0", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipsPos0;
        [AutoBind("./TipsPos/1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipsPos1;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tips;
        [AutoBind("./DetailInfoPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipsCloseBtn;
        [AutoBind("./CompileGroup/CompileButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editBtn;
        [AutoBind("./CompileGroup/QuitCompileButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_stopEditBtn;
        [AutoBind("./CompileGroup/BatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_batchEditBtn;
        [AutoBind("./CompileGroup/RevocationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_revocationBtn;
        [AutoBind("./CompileGroup/RevocationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_revocationBtnCtrl;
        [AutoBind("./CompileGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelBtn;
        [AutoBind("./CompileGroup/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_filterBtn;
        [AutoBind("./CompileGroup/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_filterBtnCtrl;
        [AutoBind("./CompileGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_editPanel;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_scrollView;
        [AutoBind("./HintGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hint1;
        [AutoBind("./HintGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hint2;
        [AutoBind("./HintGroup01/BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hint1Back;
        [AutoBind("./HintGroup02/BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hint2Back;
        [AutoBind("./FilterPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_filterPos;
        [AutoBind("./HintGroup02/STitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_guildMoney;
        [AutoBind("./HintGroup01/ContentGroup/ItemImage/OwnTitleText/OwnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_sellHasCount;
        [AutoBind("./HintGroup01/ContentGroup/ItemImage/PriceTitleText/OwnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_sellPrice;
        [AutoBind("./HintGroup01/ContentGroup/DonatePanel/ItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_sellTotalCost;
        [AutoBind("./HintGroup01/ContentGroup/DonatePanel/NumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_sellCount;
        public long m_sellCountNum;
        [AutoBind("./HintGroup01/ContentGroup/ItemImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sellNameText;
        [AutoBind("./HintGroup01/ContentGroup/ItemImage/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_sellIconDummy;
        [AutoBind("./HintGroup01/ContentGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_dailyView;
        [AutoBind("./HintGroup01/ContentGroup/Scroll View/Viewport/Content/MessageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_dailyText;
        [AutoBind("./HintGroup01/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellConfirm;
        [AutoBind("./HintGroup01/ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellCancel;
        [AutoBind("./HintGroup01/ContentGroup/DonatePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellAdd;
        [AutoBind("./HintGroup01/ContentGroup/DonatePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellMinus;
        [AutoBind("./HintGroup01/DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellTitleBtn;
        [AutoBind("./HintGroup01/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sellTips;
        [AutoBind("./HintGroup02/ContentGroup/ItemImage/OwnTitleText/OwnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_editHasCount;
        [AutoBind("./HintGroup02/ContentGroup/ItemImage/PriceTitleText/OwnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_editPrice;
        [AutoBind("./HintGroup02/ContentGroup/DonatePanel/ItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_editTotalCost;
        [AutoBind("./HintGroup02/ContentGroup/DonatePanel/NumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_editCount;
        public long m_editCountNum;
        [AutoBind("./HintGroup02/ContentGroup/ItemImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_editNameText;
        [AutoBind("./HintGroup02/ContentGroup/ItemImage/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_editIconDummy;
        [AutoBind("./HintGroup02/ContentGroup/DonatePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editAdd;
        [AutoBind("./HintGroup02/ContentGroup/DonatePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editMinus;
        [AutoBind("./HintGroup02/ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editConfirm;
        [AutoBind("./HintGroup02/ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editCancel;
        [AutoBind("./HintGroup02/DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editTitleBtn;
        [AutoBind("./HintGroup02/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_editTips;
        [AutoBind("./MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradeMoneyValueText;
        [AutoBind("./MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildTradeMoneyAddButton;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataAuctionItemInfo>, ConfigDataAuctionItemInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<GuildPurchaseLogInfo, DateTime> <>f__am$cache1;
        private static DelegateBridge __Hotfix_WindowOpen;
        private static DelegateBridge __Hotfix_WindowClose;
        private static DelegateBridge __Hotfix_DisplayMoneyCount;
        private static DelegateBridge __Hotfix_ShowTips;
        private static DelegateBridge __Hotfix_ShowEditButton;
        private static DelegateBridge __Hotfix_CreateAllCategoryToggles;
        private static DelegateBridge __Hotfix_SetMenuInfo;
        private static DelegateBridge __Hotfix_SetResDict;
        private static DelegateBridge __Hotfix_InitTotalButton;
        private static DelegateBridge __Hotfix_RefreshProductList;
        private static DelegateBridge __Hotfix_IsSuitToFilter_1;
        private static DelegateBridge __Hotfix_IsSuitToFilter_0;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemFill;
        private static DelegateBridge __Hotfix_FindOrderCount;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_SetEditMode;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_SetFilter;
        private static DelegateBridge __Hotfix_ShowFilter;
        private static DelegateBridge __Hotfix_BatchRevocate;
        private static DelegateBridge __Hotfix_ClearSelect;
        private static DelegateBridge __Hotfix_ShowSellPanel;
        private static DelegateBridge __Hotfix_ShowEditPanel;
        private static DelegateBridge __Hotfix_DisplayGuildMoney;
        private static DelegateBridge __Hotfix_GuildPurchaseLogInfo2String;
        private static DelegateBridge __Hotfix_SellCancel;
        private static DelegateBridge __Hotfix_SellConfirm;
        private static DelegateBridge __Hotfix_EditCancel;
        private static DelegateBridge __Hotfix_EditConfirm;
        private static DelegateBridge __Hotfix_SetSellTotalCost;
        private static DelegateBridge __Hotfix_SetEditTotalCost;
        private static DelegateBridge __Hotfix_OnDetailPanelItemIconClick;
        private static DelegateBridge __Hotfix_ShowSellTips;
        private static DelegateBridge __Hotfix_ShowEditTips;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SellAddLongPressStart;
        private static DelegateBridge __Hotfix_SellAddLongPressEnd;
        private static DelegateBridge __Hotfix_OnSellInputChanged;
        private static DelegateBridge __Hotfix_OnEditInputChanged;
        private static DelegateBridge __Hotfix_AddSellCount;
        private static DelegateBridge __Hotfix_MinusSellCount;
        private static DelegateBridge __Hotfix_AddEditCount;
        private static DelegateBridge __Hotfix_MinusEditCount;
        private static DelegateBridge __Hotfix_get_SelectedType;
        private static DelegateBridge __Hotfix_set_SelectedType;
        private static DelegateBridge __Hotfix_ResetEvents;
        private static DelegateBridge __Hotfix_add_OnSelectTypeChanged;
        private static DelegateBridge __Hotfix_remove_OnSelectTypeChanged;
        private static DelegateBridge __Hotfix_set_IsOpened;
        private static DelegateBridge __Hotfix_get_IsOpened;
        private static DelegateBridge __Hotfix_set_IsEditMode;
        private static DelegateBridge __Hotfix_get_IsEditMode;
        private static DelegateBridge __Hotfix_set_IsBatchEditMode;
        private static DelegateBridge __Hotfix_get_IsBatchEditMode;
        private static DelegateBridge __Hotfix_get_FilterShown;
        private static DelegateBridge __Hotfix_set_FilterShown;
        private static DelegateBridge __Hotfix_add_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnSellPanelItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnSellPanelItemIconClick;
        private static DelegateBridge __Hotfix_add_OnOrderModified;
        private static DelegateBridge __Hotfix_remove_OnOrderModified;
        private static DelegateBridge __Hotfix_add_OnOrderCancelled;
        private static DelegateBridge __Hotfix_remove_OnOrderCancelled;
        private static DelegateBridge __Hotfix_add_OnOrderCreated;
        private static DelegateBridge __Hotfix_remove_OnOrderCreated;
        private static DelegateBridge __Hotfix_add_OnOrderMatched;
        private static DelegateBridge __Hotfix_remove_OnOrderMatched;
        private static DelegateBridge __Hotfix_add_OnDisplayGuildMoney;
        private static DelegateBridge __Hotfix_remove_OnDisplayGuildMoney;
        private static DelegateBridge __Hotfix_add_OnSellCountTooMany;
        private static DelegateBridge __Hotfix_remove_OnSellCountTooMany;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_OnFilterChanged;
        private static DelegateBridge __Hotfix_remove_OnFilterChanged;

        public event Action<int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int> EventOnItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataAuctionItemInfo> EventOnSellPanelItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action> OnDisplayGuildMoney
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<AuctionBuyItemFilterType> OnFilterChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<int>, Action> OnOrderCancelled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, long, Action> OnOrderCreated
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, long, Action> OnOrderMatched
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildPurchaseOrderInfo, long, Action> OnOrderModified
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> OnSelectTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action OnSellCountTooMany
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddEditCount()
        {
        }

        [MethodImpl(0x8000)]
        private void AddSellCount()
        {
        }

        [MethodImpl(0x8000)]
        public void BatchRevocate()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSelect()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllCategoryToggles()
        {
        }

        [MethodImpl(0x8000)]
        private void DisplayGuildMoney()
        {
        }

        [MethodImpl(0x8000)]
        public void DisplayMoneyCount(ulong currency)
        {
        }

        [MethodImpl(0x8000)]
        public void EditCancel()
        {
        }

        [MethodImpl(0x8000)]
        public void EditConfirm()
        {
        }

        [MethodImpl(0x8000)]
        private long FindOrderCount(int prodId)
        {
        }

        [MethodImpl(0x8000)]
        private static string GuildPurchaseLogInfo2String(GuildPurchaseLogInfo log, int groupCount = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void InitTotalButton()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSuitToFilter(ConfigDataAuctionItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSuitToFilter(GuildPurchaseOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        private void MinusEditCount()
        {
        }

        [MethodImpl(0x8000)]
        private void MinusSellCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailPanelItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditInputChanged(string value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFilterChange(AuctionBuyItemFilterType filter)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellInputChanged(string value)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshProductList(IEnumerable<KeyValuePair<int, ConfigDataAuctionItemInfo>> itemInfo, List<GuildPurchaseOrderInfo> orderList)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void SellAddLongPressEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void SellAddLongPressStart()
        {
        }

        [MethodImpl(0x8000)]
        public void SellCancel()
        {
        }

        [MethodImpl(0x8000)]
        public void SellConfirm()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEditMode(bool editMode, bool batchMode)
        {
        }

        [MethodImpl(0x8000)]
        private void SetEditTotalCost()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFilter(AuctionBuyItemFilterType filter)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuInfo(Dictionary<NpcShopItemCategory, List<int>> productToggleList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetResDict(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSellTotalCost()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEditButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEditPanel(bool isShow, int prodId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEditTips(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowFilter(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSellPanel(bool isShow, int prodId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSellTips(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowTips(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void WindowClose(Action<bool> onFinish = null)
        {
        }

        [MethodImpl(0x8000)]
        public void WindowOpen(Action<bool> onFinish = null)
        {
        }

        public int SelectedType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsOpened
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsEditMode
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsBatchEditMode
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool FilterShown
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <WindowClose>c__AnonStorey1
        {
            internal Action<bool> onFinish;
            internal GuildPurchaseUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.IsOpened = false;
                if (this.onFinish != null)
                {
                    this.onFinish(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <WindowOpen>c__AnonStorey0
        {
            internal Action<bool> onFinish;
            internal GuildPurchaseUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.IsOpened = true;
                if (this.onFinish != null)
                {
                    this.onFinish(res);
                }
            }
        }

        public class ItemSorter : IComparer<ConfigDataAuctionItemInfo>
        {
            private readonly Dictionary<int, int> m_hasCount;
            private List<GuildPurchaseOrderInfo> m_orderList;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCount;
            private static DelegateBridge __Hotfix_FindOrderCount;
            private static DelegateBridge __Hotfix_SetListAndClearTempData;

            [MethodImpl(0x8000)]
            private long FindOrderCount(int prodId)
            {
            }

            [MethodImpl(0x8000)]
            private int GetCount(ConfigDataAuctionItemInfo info)
            {
            }

            [MethodImpl(0x8000)]
            public void SetListAndClearTempData(List<GuildPurchaseOrderInfo> orderList)
            {
            }

            [MethodImpl(0x8000)]
            int IComparer<ConfigDataAuctionItemInfo>.Compare(ConfigDataAuctionItemInfo v1, ConfigDataAuctionItemInfo v2)
            {
            }
        }

        public class OrderSorter : IComparer<GuildPurchaseOrderInfo>
        {
            private readonly Dictionary<int, long> m_hasCount = new Dictionary<int, long>();
            private readonly LogicBlockItemStoreBase m_lbStore = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBItemStore();
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCount;
            private static DelegateBridge __Hotfix_ClearTempData;

            public OrderSorter()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void ClearTempData()
            {
                DelegateBridge bridge = __Hotfix_ClearTempData;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_hasCount.Clear();
                }
            }

            private long GetCount(GuildPurchaseOrderInfo order)
            {
                DelegateBridge bridge = __Hotfix_GetCount;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp2537(this, order);
                }
                ConfigDataAuctionItemInfo configDataAuctionItemInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataAuctionItemInfo(order.m_auctionItemId);
                if (configDataAuctionItemInfo == null)
                {
                    return 0L;
                }
                if (this.m_hasCount.ContainsKey(configDataAuctionItemInfo.ID))
                {
                    return this.m_hasCount[configDataAuctionItemInfo.ID];
                }
                LBStoreItem item = this.m_lbStore.GetMSStoreItemById(configDataAuctionItemInfo.ItemType, configDataAuctionItemInfo.ItemId, false, false);
                long num = (item != null) ? item.GetItemCount() : 0L;
                this.m_hasCount[configDataAuctionItemInfo.ID] = num;
                return num;
            }

            int IComparer<GuildPurchaseOrderInfo>.Compare(GuildPurchaseOrderInfo v1, GuildPurchaseOrderInfo v2)
            {
                if (v1 == null)
                {
                    return -1;
                }
                if (v2 != null)
                {
                    if ((this.GetCount(v1) > 0L) && (this.GetCount(v2) == 0L))
                    {
                        return -1;
                    }
                    if ((this.GetCount(v1) != 0L) || (this.GetCount(v2) <= 0L))
                    {
                        return v1.m_auctionItemId.CompareTo(v2.m_auctionItemId);
                    }
                }
                return 1;
            }
        }
    }
}

