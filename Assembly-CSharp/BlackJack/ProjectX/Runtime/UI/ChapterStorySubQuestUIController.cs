﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ChapterStorySubQuestUIController : UIControllerBase
    {
        private int m_questId;
        private bool m_isBranchQuest;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, bool> EventOnSubQuestButtonClick;
        private const string StateQuestCompleted = "Gray";
        private const string StateGoToFinidhQuest = "Goto";
        private const string StateQuestWaitForComplete = "GetReward";
        private const string StateChapterSubItemQuest = "ChapterSubItem";
        private const string StateStoryQuest = "Story";
        [AutoBind("./SubQuestButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_valueText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./SubQuestButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_subQuestButton;
        [AutoBind("./RewardInfo/RewardIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_rewardIconImage;
        [AutoBind("./RewardInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_rewardValueText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitCtrl;
        private static DelegateBridge __Hotfix_UpdataSubChapterStoryInfo;
        private static DelegateBridge __Hotfix_UpdateSubChapterQuestInfo;
        private static DelegateBridge __Hotfix_UpdataBranchStoryInfo;
        private static DelegateBridge __Hotfix_GetTotalCount;
        private static DelegateBridge __Hotfix_GetCurrCount;
        private static DelegateBridge __Hotfix_GetQuestId;
        private static DelegateBridge __Hotfix_OnSubQuestButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSubQuestButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSubQuestButtonClick;

        public event Action<int, bool> EventOnSubQuestButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private int GetCurrCount(LBProcessingQuestBase quest, LogicBlockQuestBase lbQust)
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestId()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTotalCount(ConfigDataQuestInfo configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void InitCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubQuestButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataBranchStoryInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataSubChapterStoryInfo(LogicBlockQuestBase lbQust, int questId, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSubChapterQuestInfo(LogicBlockQuestBase lbQust, int questId, ConfigDataQuestInfo configInfo)
        {
        }
    }
}

