﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SSNodePopMenuUIController : UIControllerBase
    {
        public ulong m_selectedTunnelId;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./PopMenuButtons/SolarSystemDetailInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_detailInfoButton;
        [AutoBind("./PopMenuButtons/JumpToSolarSystemButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./PopMenuButtons/BaseRedeployButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_baseRedeployButton;
        [AutoBind("./PopMenuButtons/JumpToSolarSystemButton/Image", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarJumpDisObj;
        [AutoBind("./PopMenuButtons/JumpToSolarSystemButton/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_SolarJumpDisText;
        [AutoBind("./PopMenuButtons/EnterStarMapForSolarSystemButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_enterStarMapForSSButton;
        [AutoBind("./StarfieldInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_brushTypeUIStateCtrl;
        [AutoBind("./StarfieldInfoPanel/AllowIn", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allowShipTypeRoot;
        [AutoBind("./StarfieldInfoPanel/SovereignInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sovereignInfoRoot;
        [AutoBind("./StarfieldInfoPanel/AllowIn/LevelRoot/Level", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_holeRecommandLevel;
        [AutoBind("./StarfieldInfoPanel/ExplodedShipCountInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_explodedShipCountInfoText;
        [AutoBind("./StarfieldInfoPanel/MiningVitalityInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_miningVitalityInfoText;
        [AutoBind("./StarfieldInfoPanel/PiratesVitalityInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_piratesVitalityInfoText;
        [AutoBind("./StarfieldInfoPanel/CommercialVitalityInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_commercialVitalityInfoText;
        [AutoBind("./StarfieldInfoPanel/SovereignInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sovereignInfoText;
        [AutoBind("./StarfieldInfoPanel/StargroupInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_stargroupInfoText;
        [AutoBind("./StarfieldInfoPanel/StarfieldInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starfieldInfoText;
        [AutoBind("./StarfieldInfoPanel/InfectiosityInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_infectStateCtrl;
        [AutoBind("./StarfieldInfoPanel/InfectiosityInfo/DetailText/InfectPercentText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectPercentText;
        [AutoBind("./StarfieldInfoPanel/InfectiosityInfo/DetailText/ProgressBG/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_infectProgressBar;
        [AutoBind("./StarfieldInfoPanel/SecurityLevelInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_securityLevelText;
        [AutoBind("./StarfieldInfoPanel/MineralTypesRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_mineralTypeRootTrans;
        [AutoBind("./StarfieldInfoPanel/SignalLevelInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalLevelText;
        [AutoBind("./StarfieldInfoPanel/WormHoleTypeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_wormholeTypeInfoObj;
        [AutoBind("./StarfieldInfoPanel/WormHoleTypeInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_wormholeNameText;
        [AutoBind("./StarfieldInfoPanel/QuestPoolCountInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questPoolCountInfoText;
        [AutoBind("./StarfieldInfoPanel/FightStargroupInfo/DetailText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemGuildBattleStatusText;
        [AutoBind("./JumpUIGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_jumpPanel;
        [AutoBind("./JumpUIGroup/ContentGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler m_ScrollViewHandler;
        [AutoBind("./JumpUIGroup/ContentGroup/Scroll View/Viewport/Content/JumpItemGruop", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapJumpTargetUIController m_jumpItem;
        [AutoBind("./JumpUIGroup/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnTunnelJump;
        [AutoBind("./JumpUIGroup/JumpButton/TextRandJump", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_jumpButtonTextRandom;
        [AutoBind("./JumpUIGroup/JumpButton/TextJump", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_jumpButtonTextNormal;
        private readonly List<StarMapJumpTargetUIController> m_jumpTargetList;
        private int m_currSolarSystemId;
        private int m_currStarfieldId;
        private SSNodePopMenuMineralTypeInfoUIController m_mineralTypeUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_SetUIInfoByBrushType;
        private static DelegateBridge __Hotfix_SetSolarSystemJumpDistanceText;
        private static DelegateBridge __Hotfix_SetJumpTarget;
        private static DelegateBridge __Hotfix_SelectTunnel;
        private static DelegateBridge __Hotfix_CreatePopupWndWindowProcess;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public UIProcess CreatePopupWndWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SelectTunnel(ulong tunnelId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetJumpTarget(List<GuildTeleportTunnelEffectInfoClient> tunnelList, bool includeRandomJump)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemJumpDistanceText(int targetSolarId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIInfoByBrushType(GEBrushType brushType, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

