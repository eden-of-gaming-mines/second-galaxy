﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(CanvasGroup))]
    public class NPCChatUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNPChatEnd;
        protected float m_leftLifeTime;
        protected float m_totalLifeTime;
        private Material m_noiseMat;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsBusy>k__BackingField;
        private bool m_canInterrupt;
        private string m_currentNPCName;
        private string UIState_PanelOpen;
        private string UIState_PanelDetail;
        private string UIState_PanelClose;
        private string UIState_DetailRight;
        private string UIState_DeatilLeft;
        [AutoBind("./NpcFaceImages/NPCImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NpcImage;
        [AutoBind("./ChatDetail/NPCNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NPCNameText;
        [AutoBind("./ChatDetail/ChatDetailText", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ChatDetailTextScrollView;
        [AutoBind("./ChatDetail/ChatDetailText/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChatDetailText;
        [AutoBind("./ChatDetail/ClickEffectImagesRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ClickEffectImagesRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NPCChatStateCtrl;
        [AutoBind("./ChatDetail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NPCChatDetailStateCtrl;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ShowNPCChatInfo_1;
        private static DelegateBridge __Hotfix_HideAndClear;
        private static DelegateBridge __Hotfix_SetPos;
        private static DelegateBridge __Hotfix_NPCChatAudioClear;
        private static DelegateBridge __Hotfix_ShowNPCChatInfo_0;
        private static DelegateBridge __Hotfix_DelayClose;
        private static DelegateBridge __Hotfix_SetChatScrollViewPos;
        private static DelegateBridge __Hotfix_SetRemoteNpcMode;
        private static DelegateBridge __Hotfix_add_EventOnNPChatEnd;
        private static DelegateBridge __Hotfix_remove_EventOnNPChatEnd;
        private static DelegateBridge __Hotfix_get_IsBusy;
        private static DelegateBridge __Hotfix_set_IsBusy;
        private static DelegateBridge __Hotfix_get_CanInterrupt;
        private static DelegateBridge __Hotfix_set_CanInterrupt;

        public event Action EventOnNPChatEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void DelayClose()
        {
        }

        [MethodImpl(0x8000)]
        public void HideAndClear()
        {
        }

        [MethodImpl(0x8000)]
        private void NPCChatAudioClear()
        {
        }

        [MethodImpl(0x8000)]
        private void SetChatScrollViewPos()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRemoteNpcMode(bool isRemote)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNPCChatInfo(Sprite npcIcon, string npcName, string chatStr, bool detailIsRight, bool isShowNextPage, string audioAssetPath, float durationTime, bool needPlayNPCTween, bool isRemoteNpc = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNPCChatInfo(string npcIconPath, string npcName, string chatStr, bool canInterrupt, Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict, bool isRemoteNpc = false, bool detailInRight = true, bool showNextPage = true, bool posIsChange = false, string audioPath = "", float durationTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        public bool IsBusy
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool CanInterrupt
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

