﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class FactionCreditUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action, bool, bool> EventOnFactionCreditUIClose;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnFactionItemSwitchTask;
        private Dictionary<int, float> m_FactionServerInfo;
        public FactionCreditUIController m_MainCtrl;
        public FactionCreditLevelRewardInfoUIController m_weekRewardInfoCtrl;
        public const FactionCreditLevel MAX_FACTION_CREDIT_LEVEL = FactionCreditLevel.FactionCreditLevel_P3;
        public const GrandFaction Max_GRAND_FACTION = GrandFaction.GrandFaction_USG;
        private IUIBackgroundManager m_backgroundManager;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static int MAX_WEEKLY_COUNT;
        public static int MAX_WEEKLY_COUNT_TABLE_ID = 1;
        public const string IsPlayPanelOpenAaim_ParamKey = "IsPlayPanelOpenAaim";
        public const string TaskName = "FactionCreditUITask";
        public const float QuestRewardFactionCreditFactor = 0.1f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartFactionCreditUITask;
        private static DelegateBridge __Hotfix_ShowFactionCreditPanel;
        private static DelegateBridge __Hotfix_HideFactionCreditPanel;
        private static DelegateBridge __Hotfix_SetBackGroundManager;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_CacalFactionLevelProgress;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_GetFactionServerInfo;
        private static DelegateBridge __Hotfix_GetMaxWeekCount;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_OnTipOpenButtonClick;
        private static DelegateBridge __Hotfix_OnTipCloseOpenButtonClick;
        private static DelegateBridge __Hotfix_CloseFactionCreditUIPanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_OnFactionItemButtonClick;
        private static DelegateBridge __Hotfix_OnRewardButtonClick;
        private static DelegateBridge __Hotfix_OnRewardBoxCloseClick;
        private static DelegateBridge __Hotfix_OnRewardBoxGetClick;
        private static DelegateBridge __Hotfix_RewardBoxGetEnd;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnFactionCreditUIClose;
        private static DelegateBridge __Hotfix_remove_EventOnFactionCreditUIClose;
        private static DelegateBridge __Hotfix_add_EventOnFactionItemSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnFactionItemSwitchTask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnFactionItemButtonClick_UserGuide;
        private static DelegateBridge __Hotfix_GetItemRectTransByGrandFaction;
        private static DelegateBridge __Hotfix_GetFristFactionItemPath;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;

        public event Action<Action, bool, bool> EventOnFactionCreditUIClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnFactionItemSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public FactionCreditUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        public static float CacalFactionLevelProgress(int curServerVal, FactionCreditLevel fcLv, ConfigDataFactionCreditLevelInfo fcLvInfo, out int curShowVal, out int curShowMaxVal)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseFactionCreditUIPanel(Action evntAfterPause, bool isIgnoreAnim = false, bool isReapeatableUserGuide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataFactionCreditLevelInfo GetFactionCreditLevelInfo(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        private Dictionary<int, float> GetFactionServerInfo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetFristFactionItemPath()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetItemRectTransByGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void GetMaxWeekCount()
        {
        }

        [MethodImpl(0x8000)]
        public void HideFactionCreditPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionItemButtonClick(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFactionItemButtonClick_UserGuide(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardBoxCloseClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardBoxGetClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipCloseOpenButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipOpenButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void RewardBoxGetEnd(int id, GrandFaction faction, FactionCreditLevelRewardInfoUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundManager(IUIBackgroundManager backgroundManager)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowFactionCreditPanel(bool isImmediate = false, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static FactionCreditUITask StartFactionCreditUITask(bool isPlayOpenAnim = true, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideFactionCreditPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal FactionCreditUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnFactionItemButtonClick_UserGuide>c__AnonStorey3
        {
            internal int selfFactionID;
            internal Action<bool> onEnd;
            internal FactionCreditUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(returnIntent => FactionCreditDetailUITask.StartFactionCreditItemUITask(returnIntent, this.selfFactionID, this.onEnd, null));
            }

            internal void <>m__1(UIIntent returnIntent)
            {
                FactionCreditDetailUITask.StartFactionCreditItemUITask(returnIntent, this.selfFactionID, this.onEnd, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnFactionItemButtonClick>c__AnonStorey1
        {
            internal int factionId;
            internal FactionCreditUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(returnIntent => FactionCreditDetailUITask.StartFactionCreditItemUITask(returnIntent, this.factionId, null, null));
            }

            internal void <>m__1(UIIntent returnIntent)
            {
                FactionCreditDetailUITask.StartFactionCreditItemUITask(returnIntent, this.factionId, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnRewardBoxGetClick>c__AnonStorey2
        {
            internal FactionCreditLevelRewardInfoUIController targetCtr;
            internal FactionCreditUITask $this;

            internal void <>m__0(Task task)
            {
                FactionCreditQuestWeeklyRewardReqTask task2 = task as FactionCreditQuestWeeklyRewardReqTask;
                if (!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.SetReqResult == 0)
                    {
                        this.$this.RewardBoxGetEnd(FactionCreditUITask.MAX_WEEKLY_COUNT_TABLE_ID, task2.GrandFactionAck, this.targetCtr);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.SetReqResult, true, false);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            MainPanel,
            RewardBoxPanel
        }
    }
}

