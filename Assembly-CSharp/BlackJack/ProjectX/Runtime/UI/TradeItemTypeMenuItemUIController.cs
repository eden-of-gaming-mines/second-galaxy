﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TradeItemTypeMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./CategoryTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text TypeNameText;
        [AutoBind("./CategoryTitle", AutoBindAttribute.InitState.NotInit, false)]
        private Button NormalItemTypeButton;
        [AutoBind("./CategoryTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonStateCtrl;
        [AutoBind("./CategoryTitle/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image IconImage;
        private static DelegateBridge __Hotfix_GetItemData;
        private static DelegateBridge __Hotfix_SetTypeItemState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;

        [MethodImpl(0x8000)]
        public TradeItemTypeMenuItemData GetItemData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTypeItemState(string state)
        {
        }
    }
}

