﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class OverSeaActivityCmDailySignUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action, bool> EventOnRequestOverSeaActivityManagerUIPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRefreshRedPoint;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private int m_serverSingedId;
        private int m_showItemRowIndex;
        private int m_showItemFirstId;
        private int m_showItemSecondId;
        private int m_showItemThirdId;
        private static string ParamKeyShowState;
        private static string ParamKeyIsPlayAniImmedite;
        public OverSeaActivityCmDailySignUIController m_mainCtrl;
        public const string TaskName = "OverSeaActivityCmDailySignUITask";
        private int[] m_configDataCMDailySignAccRewardData;
        public static int CMDAILY_CONFIG_MAX_COUNT;
        public const int PERIOD_REWARD_DAY = 5;
        public const int PERIOD_REWARD_ROW_DAY = 15;
        public static int CUR_PRRIOD_TOTAL_DAY;
        private float m_refreshDelayTime;
        private const float RefreshDelayTime = 0.5f;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitIntentFromStartOrResume;
        private static DelegateBridge __Hotfix_StartOverSeaActivityCmDailySignUITaskWithPrepare;
        private static DelegateBridge __Hotfix_GetShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_ShowOverSeaActivityCmDailySignUITask;
        private static DelegateBridge __Hotfix_HideOverSeaActivityCmDailySignUITask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsGetedRewardForCurDay_0;
        private static DelegateBridge __Hotfix_IsGetedRewardForCurDay_1;
        private static DelegateBridge __Hotfix_GetDailySignCount;
        private static DelegateBridge __Hotfix_isNeedRestarUpdate;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_PauseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_GetSignAccRewardCount;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_Self;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ShowRewardWindowTip;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ResisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnDailyRefreshNtf;
        private static DelegateBridge __Hotfix_OnStartButtonClick;
        private static DelegateBridge __Hotfix_OnMainRewardButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestOverSeaActivityManagerUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestOverSeaActivityManagerUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_remove_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnRefreshRedPoint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool> EventOnRequestOverSeaActivityManagerUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public OverSeaActivityCmDailySignUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_Self(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private int GetDailySignCount()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        private void GetSignAccRewardCount()
        {
        }

        [MethodImpl(0x8000)]
        public void HideOverSeaActivityCmDailySignUITask(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitIntentFromStartOrResume(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGetedRewardForCurDay()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsGetedRewardForCurDay(out DateTime dateTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private bool isNeedRestarUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyRefreshNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMainRewardButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStartButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void PauseItemSimpleInfoPanel(Action evntAfterPause, bool isIgnoreAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void ResisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(FakeLBStoreItem item, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOverSeaActivityCmDailySignUITask(bool immedite, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowRewardWindowTip(List<ProStoreItemUpdateInfo> proInfoItems)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartOverSeaActivityCmDailySignUITaskWithPrepare(UIIntent returnToIntent, string mode, Action<bool> onPrepareEnd, Action onResLoadEnd, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideOverSeaActivityCmDailySignUITask>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal OverSeaActivityCmDailySignUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey3
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(Task task)
            {
                CMDailySignInfoReqNetTask task2 = task as CMDailySignInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    this.onPrepareEnd(false);
                    Debug.LogError(" CMDailySignRewardRecvReqNetTask returnTask.IsNetworkError ");
                }
                else if (task2.m_result == 0)
                {
                    this.onPrepareEnd(true);
                }
                else
                {
                    this.onPrepareEnd(false);
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey2
        {
            internal FakeLBStoreItem item;
            internal Vector3 pos;
            internal ItemSimpleInfoUITask.PositionType type;
            internal OverSeaActivityCmDailySignUITask $this;

            internal void <>m__0(UIIntent intent)
            {
                string mode = "NoButton";
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, intent, true, this.pos, mode, this.type, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowOverSeaActivityCmDailySignUITask>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(bool ret)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(ret);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            Init,
            HideState,
            IsPlayAniImmedite,
            Singning
        }
    }
}

