﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipLeaveStationForCelestialNetTask : NetWorkTransactionTask
    {
        public ulong m_shipInstanceId;
        public int m_destType;
        public int m_destId;
        public int m_destSolarSystemId;
        public uint m_dstSceneInstanceId;
        public ulong m_targetInstanceId;
        public int m_leaveStationForCelestialAckResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationLeaveStationForCelestialAck;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;

        [MethodImpl(0x8000)]
        public HangarShipLeaveStationForCelestialNetTask(ulong shipInstanceId, int dstType, int dstId, int dstSolarSystemId, uint dstSceneInstanceId, ulong targetInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationLeaveStationForCelestialAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

