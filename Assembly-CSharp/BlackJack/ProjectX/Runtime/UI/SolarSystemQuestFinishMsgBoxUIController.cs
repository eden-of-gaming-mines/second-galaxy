﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class SolarSystemQuestFinishMsgBoxUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToStationButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNextButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./Detail/IconImage/BackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backToStationButton;
        [AutoBind("./Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_msgText;
        [AutoBind("./Detail/IconImage/NextImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_nextButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowQuestFinishMsgBox;
        private static DelegateBridge __Hotfix_ShowGotoNextScienceExploreQuestMsgBox;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBackToStationButtonClick;
        private static DelegateBridge __Hotfix_OnNextButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackToStationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToStationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNextButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnNextButtonClick;

        public event Action EventOnBackToStationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNextButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToStationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNextButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowGotoNextScienceExploreQuestMsgBox(bool isShow, bool immediately = false)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowQuestFinishMsgBox(bool isShow, string msg = "", bool immediateHide = false, bool withBack = true)
        {
        }
    }
}

