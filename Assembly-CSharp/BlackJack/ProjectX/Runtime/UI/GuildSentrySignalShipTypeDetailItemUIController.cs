﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildSentrySignalShipTypeDetailItemUIController : UIControllerBase
    {
        [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CountText;
        [AutoBind("./ShipTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTypeImage;
        private static DelegateBridge __Hotfix_UpdateShipTypeDetailInfo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipTypeDetailInfo(ShipType shipType, int shipCount, Dictionary<string, Object> resDict)
        {
        }
    }
}

