﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CommonShipTypeLimitUIController : UIControllerBase
    {
        private uint m_currShipTypeLimitFlag;
        private List<ShipType> m_shipTypeEnumValueArray;
        [AutoBind("./Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_frigateIcon;
        [AutoBind("./Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_destroyerIcon;
        [AutoBind("./Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cruiserIcon;
        [AutoBind("./BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_battleCruiserIcon;
        [AutoBind("./BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_battleShipIcon;
        [AutoBind("./IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_industryShipIcon;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetAllowAllShipType;
        private static DelegateBridge __Hotfix_SetAllowShipTypeList_0;
        private static DelegateBridge __Hotfix_SetAllowShipTypeList_1;
        private static DelegateBridge __Hotfix_UpdateUIEffectByShipLimitFlag;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllowAllShipType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllowShipTypeList(List<ShipType> shipTypeList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllowShipTypeList(List<int> shipTypeList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUIEffectByShipLimitFlag()
        {
        }
    }
}

