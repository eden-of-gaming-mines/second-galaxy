﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class UploadClientLogUIController : UIControllerBase
    {
        private UploadClientLogReasonUIController[] m_reasonUICtrlList;
        [AutoBind("./InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_inputField;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmBtn;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetUIProcess;
        private static DelegateBridge __Hotfix_InitUIState;
        private static DelegateBridge __Hotfix_GetIssueDescString;

        [MethodImpl(0x8000)]
        public string GetIssueDescString()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void InitUIState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

