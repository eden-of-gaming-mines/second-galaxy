﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogoHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;

        public class CostButtonInfo
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private CommonUIUtil.CostCurrencyType <CurrencyType>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Cost>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_CurrencyType;
            private static DelegateBridge __Hotfix_set_CurrencyType;
            private static DelegateBridge __Hotfix_get_Cost;
            private static DelegateBridge __Hotfix_set_Cost;
            private static DelegateBridge __Hotfix_GetLogo;

            public CostButtonInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public string GetLogo()
            {
                DelegateBridge bridge = __Hotfix_GetLogo;
                return ((bridge == null) ? CommonUIUtil.GetCurrencyLogo(this.CurrencyType) : bridge.__Gen_Delegate_Imp33(this));
            }

            public CommonUIUtil.CostCurrencyType CurrencyType
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_CurrencyType;
                    return ((bridge == null) ? this.<CurrencyType>k__BackingField : bridge.__Gen_Delegate_Imp4872(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_CurrencyType;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp4873(this, value);
                    }
                    else
                    {
                        this.<CurrencyType>k__BackingField = value;
                    }
                }
            }

            public int Cost
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Cost;
                    return ((bridge == null) ? this.<Cost>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_Cost;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Cost>k__BackingField = value;
                    }
                }
            }
        }

        public class LogoInfo
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private LogoHelper.LogoUsageType <Type>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <BgId>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <PaintingId>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <BgColorId>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <PaintingColorId>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_Type;
            private static DelegateBridge __Hotfix_set_Type;
            private static DelegateBridge __Hotfix_get_BgId;
            private static DelegateBridge __Hotfix_set_BgId;
            private static DelegateBridge __Hotfix_get_PaintingId;
            private static DelegateBridge __Hotfix_set_PaintingId;
            private static DelegateBridge __Hotfix_get_BgColorId;
            private static DelegateBridge __Hotfix_set_BgColorId;
            private static DelegateBridge __Hotfix_get_PaintingColorId;
            private static DelegateBridge __Hotfix_set_PaintingColorId;

            public LogoInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public LogoHelper.LogoUsageType Type
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Type;
                    return ((bridge == null) ? this.<Type>k__BackingField : bridge.__Gen_Delegate_Imp4871(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_Type;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp4836(this, value);
                    }
                    else
                    {
                        this.<Type>k__BackingField = value;
                    }
                }
            }

            public int BgId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_BgId;
                    return ((bridge == null) ? this.<BgId>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_BgId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<BgId>k__BackingField = value;
                    }
                }
            }

            public int PaintingId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_PaintingId;
                    return ((bridge == null) ? this.<PaintingId>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_PaintingId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<PaintingId>k__BackingField = value;
                    }
                }
            }

            public int BgColorId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_BgColorId;
                    return ((bridge == null) ? this.<BgColorId>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_BgColorId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<BgColorId>k__BackingField = value;
                    }
                }
            }

            public int PaintingColorId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_PaintingColorId;
                    return ((bridge == null) ? this.<PaintingColorId>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_PaintingColorId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<PaintingColorId>k__BackingField = value;
                    }
                }
            }
        }

        public enum LogoPositionType
        {
            BG,
            Painting
        }

        public enum LogoUsageType
        {
            Guild,
            Alliance
        }
    }
}

