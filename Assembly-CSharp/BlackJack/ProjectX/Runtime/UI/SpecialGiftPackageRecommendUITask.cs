﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class SpecialGiftPackageRecommendUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestGiftContentInfoUIPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        private List<LBRechargeGiftPackage> m_specialGiftPackageList;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private Action m_onEndAction;
        public static string ParamKey_OnEndAction = "OnEndAction";
        private SpecialGiftPackageRecommendUIController m_specialGiftPackageRecommendUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Comparison<LBRechargeGiftPackage> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<PurchaseRechargeGoodsResult> <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartSpecialGiftPackageRecommendUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RegisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_OnRechargeGiftPackageDeliverNtf;
        private static DelegateBridge __Hotfix_OnGiftContentItemIconClick;
        private static DelegateBridge __Hotfix_OnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_CloseRecommendGiftPackagePanel;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_OnItemRewardConfirmButtonClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_get_LBSdkInterface;
        private static DelegateBridge __Hotfix_get_LBGiftPackageClient;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRequestGiftContentInfoUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestGiftContentInfoUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<Action, bool> EventOnRequestGiftContentInfoUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SpecialGiftPackageRecommendUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseRecommendGiftPackagePanel(bool isImmedite)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftContentItemIconClick(int packageIndex, ILBStoreItemClient rewardItem, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemBuyButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemRewardConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeGiftPackageDeliverNtf(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(int packageIndex, ILBStoreItemClient rewardItem, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public static SpecialGiftPackageRecommendUITask StartSpecialGiftPackageRecommendUITask(UIIntent preIntent, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockClientSdkInterface LBSdkInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockRechargeGiftPackageClient LBGiftPackageClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CloseRecommendGiftPackagePanel>c__AnonStorey0
        {
            internal UIIntentReturnable returnIntent;
            internal SpecialGiftPackageRecommendUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess p, bool r)
            {
            }
        }
    }
}

