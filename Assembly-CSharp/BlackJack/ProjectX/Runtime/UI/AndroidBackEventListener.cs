﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [AddComponentMenu("UI/BlackJack/AndroidBackEventListener")]
    public class AndroidBackEventListener : MonoBehaviour
    {
        [Header("合适点击区域(填写相对于当前节点的位置)")]
        public Vector2 m_suitableClickPosition;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Click;
        private static DelegateBridge __Hotfix_IsClickable;
        private static DelegateBridge __Hotfix_WorldPosToScreenPos;
        private static DelegateBridge __Hotfix_GetCenterScreenPosition;
        private static DelegateBridge __Hotfix_IsInsideScreen;
        private static DelegateBridge __Hotfix_IsChildTransform;

        [MethodImpl(0x8000)]
        private bool Click(GameObject o)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetCenterScreenPosition(RectTransform rt, ref Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsChildTransform(Transform parent, Transform child)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsClickable(GameObject obj, out string error)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsInsideScreen(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private bool WorldPosToScreenPos(RectTransform rt, Vector3 worldPos, ref Vector2 screenPos)
        {
        }
    }
}

