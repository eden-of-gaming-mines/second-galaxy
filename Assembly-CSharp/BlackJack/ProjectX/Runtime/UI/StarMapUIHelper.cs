﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public static class StarMapUIHelper
    {
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_SetSolarSystemOccupyLogo;
        private static DelegateBridge __Hotfix_GetSolarSystemOccupyOwnerName;
        private static DelegateBridge __Hotfix_GetStarFieldGuildOccupyInfoBySSystemId;
        private static DelegateBridge __Hotfix_GetStarFieldGuildOccupInfoByStarFieldId;
        private static DelegateBridge __Hotfix_GetSpaceObjectNameByGDBInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemName_0;
        private static DelegateBridge __Hotfix_GetSolarSystemName_1;
        private static DelegateBridge __Hotfix_GetStargroupName;
        private static DelegateBridge __Hotfix_GetStarfieldName_0;
        private static DelegateBridge __Hotfix_GetStarfieldName_1;
        private static DelegateBridge __Hotfix_GetSpaceStationDes;
        private static DelegateBridge __Hotfix_GetStargateDestSolarSystemName;
        private static DelegateBridge __Hotfix_GetSpaceObjectIDByType;
        private static DelegateBridge __Hotfix_GetStarfieldStarMapIconResPath;
        private static DelegateBridge __Hotfix_GetSolarSystemStarMapIconResPath;
        private static DelegateBridge __Hotfix_GetSolarSystemGrandFactionName;
        private static DelegateBridge __Hotfix_GetSolarSystemGrandFactionIconResPath_0;
        private static DelegateBridge __Hotfix_GetSolarSystemGrandFactionIconResPath_1;
        private static DelegateBridge __Hotfix_GetColorBySolarSystemBrushInfo;
        private static DelegateBridge __Hotfix_GetColorByBrushColorElementInfoList;
        private static DelegateBridge __Hotfix_GetMineralTypeName;
        private static DelegateBridge __Hotfix_GetSolarSystemQuestPoolInfoValue;
        private static DelegateBridge __Hotfix_GetSolarSystemGuildBattleStatusName;
        private static DelegateBridge __Hotfix_GetGuildSentryTargetTrackingLevelInfo;

        [MethodImpl(0x8000)]
        private static Color GetColorByBrushColorElementInfoList(ConfigDataGEBrushInfo brushConfInfo, float brushValue)
        {
        }

        [MethodImpl(0x8000)]
        public static Color GetColorBySolarSystemBrushInfo(GEBrushType brushType, GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataGuildSentryTargetTrackingLevelInfo GetGuildSentryTargetTrackingLevelInfo(float threatValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetMineralTypeName(int mineralType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemGrandFactionIconResPath(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemGrandFactionIconResPath(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemGrandFactionName(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemGuildBattleStatusName(SolarSystemGuildBattleStatus status)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemOccupyOwnerName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSolarSystemQuestPoolInfoValue(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetSolarSystemStarMapIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetSpaceObjectIDByType(SpaceObjectType type, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSpaceObjectNameByGDBInfo(SpaceObjectType type, object obj, GDBSolarSystemInfo solarSystemInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSpaceStationDes(GDBSpaceStationInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetStarFieldGuildOccupInfoByStarFieldId(int starFieldId, Action<int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetStarFieldGuildOccupyInfoBySSystemId(int solarSystemId, Action<int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStarfieldName(GDBStarfieldsInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStarfieldName(GDBStarfieldsSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetStarfieldStarMapIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStargateDestSolarSystemName(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStargroupName(GDBStargroupInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetSolarSystemOccupyLogo(int solarSystemId, GuildLogoController logoUIController, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetStarFieldGuildOccupInfoByStarFieldId>c__AnonStorey0
        {
            internal Action<int> onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

