﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class FactionCreditFactionDetailInfoUIController : UIControllerBase
    {
        private GameObject m_CommonItemTemplateGo;
        private Dictionary<string, UnityEngine.Object> m_resData;
        private List<ConfigDataNpcShopItemInfo> m_NpcAllShopItemList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataNpcShopItemInfo, int> EventOnDropItemClick;
        protected const string CommonItemName = "CommonItemUIPrefab";
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./SugnatureGroup/SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SignButton;
        [AutoBind("./SugnatureGroup/SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SignButtonUIState;
        [AutoBind("./ContractGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SignUIStateCtr;
        [AutoBind("./DetailInfoPanel/Scroll View/Viewport/Content/DescText02/SignTitleText/SignText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText02;
        [AutoBind("./DetailInfoPanel/Scroll View/Viewport/Content/TextGroup/DescText04", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText04;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./SugnatureGroup/AccomplishText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AccomplishText;
        [AutoBind("./SugnatureGroup/AccomplishText/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ContractGroup/SignGroup/ConditionGroup/ConditionTitleText/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ConditionText;
        [AutoBind("./ContractGroup/SignGroup/EarningsGroup/EarningsGroup/EarningsText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EarningsText;
        [AutoBind("./ContractGroup/SignGroup/AwardGroup/ConditionTitleText/Image (1)", AutoBindAttribute.InitState.NotInit, false)]
        public Image FactionImage;
        [AutoBind("./ContractGroup/SignGroup/AwardGroup/ConditionTitleText/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AwardText;
        [AutoBind("./TitleImage/ContractTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContractTitleText;
        [AutoBind("./ContractGroup/ContractContentGroup/PlayerText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerText;
        [AutoBind("./ContractGroup/ContractContentGroup/ContractContentText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContractContentText;
        [AutoBind("./ContractGroup/SignGroup/ExplainGroup/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ExplainText;
        [AutoBind("./ContractGroup/SignGroup/UnlockGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect LoopScrollView;
        [AutoBind("./ContractGroup/SignGroup/UnlockGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SelfEasyObjectPool;
        [AutoBind("./ContractGroup/SignGroup/UnlockGroup/ConditionTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnlockConditionTitleText;
        [AutoBind("./ContractGroup/SignGroup/UnlockGroup/TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolCommonItemCreated;
        private static DelegateBridge __Hotfix_OnCommonItemFill;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetFcLevelByCreditNeed;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnDropItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDropItemClick;

        public event Action<ConfigDataNpcShopItemInfo, int> EventOnDropItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataFactionCreditLevelInfo GetFcLevelByCreditNeed(long ceditNeed)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolCommonItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(int factionId, string stateGroupName, int getedCount, bool creditedAll, bool enoughCon, int waitedInfoId, int preWaitedInfoId, int nextInfoId, List<ConfigDataNpcShopItemInfo> npcWaitedItemInfoList, Dictionary<string, UnityEngine.Object> dict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

