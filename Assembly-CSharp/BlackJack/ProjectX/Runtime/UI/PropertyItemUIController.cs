﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class PropertyItemUIController : UIControllerBase
    {
        [AutoBind("./PropertyValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyValue;
        [AutoBind("./PropertyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyName;
        private static DelegateBridge __Hotfix_SetPropertyValue;
        private static DelegateBridge __Hotfix_SetPropertyName;

        [MethodImpl(0x8000)]
        public void SetPropertyName(string propName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyValue(string val)
        {
        }
    }
}

