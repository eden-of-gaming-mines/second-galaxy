﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public static class BlackMarketMode
    {
        public const string ModeMonthCard = "MonthCard";
        public const string ModeSpecialGiftPackage = "GiftPackage";
        public const string ModeWeeklyGiftPackage = "WeeklyGiftPackage";
        public const string ModeDailyGiftPackage = "DailyGiftPackage";
        public const string ModeRecharge = "Recharge";
        public const string ModeResourceShop = "ResourceShop";
        public const string ModeIrShop = "IrShop";
    }
}

