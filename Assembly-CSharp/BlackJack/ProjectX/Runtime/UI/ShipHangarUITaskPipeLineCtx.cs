﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipHangarUITaskPipeLineCtx : UITaskPipeLineCtx
    {
        public int m_selectHangarShipIndex;
        public List<int> m_autoReloadAmmoAddCountList;
        public List<long> m_ammoReloadTotalCountList;
        public int m_ammoReloadSlotIndex;
        public int m_ammoReloadAddCount;
        public long m_ammoReloadTotalCount;
        public bool m_returnFromTrading;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }
    }
}

