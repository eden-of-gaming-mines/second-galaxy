﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public sealed class GuildPurchaseUITask : GuildUITaskBase
    {
        private GuildPurchaseUIController m_mainCtrl;
        private Dictionary<NpcShopItemCategory, List<int>> m_productCatagory2TypeDic;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private NpcShopItemType <SelectedShopItemType>k__BackingField;
        private const string Show = "Show";
        private const string Close = "Close";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildPurchaseUITask";
        private IEnumerable<KeyValuePair<int, ConfigDataAuctionItemInfo>> m_itemInfo;
        private bool m_needRefreshList;
        private DateTime m_cachePurchaseOrderListUpdateTime;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildPurchaseUITask> OnReturnToPrevTask;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataAuctionItemInfo>, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_RequestData;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_RegisterEvents;
        private static DelegateBridge __Hotfix_UnregisterEvents;
        private static DelegateBridge __Hotfix_ShowTips_0;
        private static DelegateBridge __Hotfix_GuildMoneyReq;
        private static DelegateBridge __Hotfix_OnOrderModified;
        private static DelegateBridge __Hotfix_OrderModifySubmit;
        private static DelegateBridge __Hotfix_OnOrderCreated;
        private static DelegateBridge __Hotfix_OrderCreateSubmit;
        private static DelegateBridge __Hotfix_OnOrderCancelled;
        private static DelegateBridge __Hotfix_OnOrderMatched;
        private static DelegateBridge __Hotfix_OnSellItemIconClick;
        private static DelegateBridge __Hotfix_OnFilterChanged;
        private static DelegateBridge __Hotfix_OnSelectTypeChanged;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_CloseWindow;
        private static DelegateBridge __Hotfix_ShowTips_1;
        private static DelegateBridge __Hotfix_HideTips;
        private static DelegateBridge __Hotfix_SetEdit;
        private static DelegateBridge __Hotfix_SetBatchEdit;
        private static DelegateBridge __Hotfix_BatchRevocate;
        private static DelegateBridge __Hotfix_ShowFilter;
        private static DelegateBridge __Hotfix_SellConfirm;
        private static DelegateBridge __Hotfix_SellCancel;
        private static DelegateBridge __Hotfix_EditConfirm;
        private static DelegateBridge __Hotfix_EditCancel;
        private static DelegateBridge __Hotfix_ShowSellTips;
        private static DelegateBridge __Hotfix_HideSellTips;
        private static DelegateBridge __Hotfix_ShowEditTips;
        private static DelegateBridge __Hotfix_HideEditTips;
        private static DelegateBridge __Hotfix_OnGuildTradeMoneyAddButton;
        private static DelegateBridge __Hotfix_CollectNpcItemCategroy2TypeDic;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_get_SelectedShopItemType;
        private static DelegateBridge __Hotfix_set_SelectedShopItemType;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_OnReturnToPrevTask;
        private static DelegateBridge __Hotfix_remove_OnReturnToPrevTask;

        public event Action<GuildPurchaseUITask> OnReturnToPrevTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildPurchaseUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void BatchRevocate(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseWindow(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectNpcItemCategroy2TypeDic()
        {
        }

        [MethodImpl(0x8000)]
        public void EditCancel(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void EditConfirm(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void GuildMoneyReq(Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        public void HideEditTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void HideSellTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void HideTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFilterChanged(AuctionBuyItemFilterType filter)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradeMoneyAddButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(int prodId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(int prodId, int tipsPosition)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrderCancelled(List<int> prodIds, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrderCreated(int prodId, long count, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrderMatched(int prodId, long count, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrderModified(GuildPurchaseOrderInfo order, long count, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelectTypeChanged(int selectedType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellItemIconClick(ConfigDataAuctionItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OrderCreateSubmit(int prodId, int count, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        private void OrderModifySubmit(GuildPurchaseOrderInfo order, int count, Action onFinished, Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void RequestData(Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        public void SellCancel(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SellConfirm(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetBatchEdit(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetEdit(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEditTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowFilter(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSellTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowTips()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowTips(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent prevIntent, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public NpcShopItemType SelectedShopItemType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GuildMoneyReq>c__AnonStorey2
        {
            internal Action callBack;

            internal void <>m__0(Task task)
            {
                this.callBack();
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrderCancelled>c__AnonStorey7
        {
            internal List<int> prodIds;
            internal Action callBack;
            internal GuildPurchaseUITask $this;
            private static Func<GuildPurchaseOrderInfo, ulong> <>f__am$cache0;

            internal void <>m__0()
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = order => order.m_instanceId;
                }
                GuildPurchaseOrderCancelReqNetTask.StartTask((from order in ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo()
                    where this.prodIds.Contains(order.m_auctionItemId)
                    select order).Select<GuildPurchaseOrderInfo, ulong>(<>f__am$cache0).ToArray<ulong>(), delegate (bool cancelRes) {
                    if (this.callBack != null)
                    {
                        this.callBack();
                    }
                    GuildPurchaseOrderListReqNetTask.StartTask(delegate (bool res) {
                        if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                        {
                            this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                        }
                    });
                });
            }

            internal bool <>m__1(GuildPurchaseOrderInfo order) => 
                this.prodIds.Contains(order.m_auctionItemId);

            private static ulong <>m__2(GuildPurchaseOrderInfo order) => 
                order.m_instanceId;

            internal void <>m__3(bool cancelRes)
            {
                if (this.callBack != null)
                {
                    this.callBack();
                }
                GuildPurchaseOrderListReqNetTask.StartTask(delegate (bool res) {
                    if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                    {
                        this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                    }
                });
            }

            internal void <>m__4(bool res)
            {
                if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                {
                    this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrderCreated>c__AnonStorey5
        {
            internal int prodId;
            internal int iCount;
            internal Action callBack;
            internal GuildPurchaseUITask $this;

            internal void <>m__0()
            {
                this.$this.OrderCreateSubmit(this.prodId, this.iCount, this.callBack);
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrderMatched>c__AnonStorey8
        {
            internal Action callBack;
            internal GuildPurchaseUITask $this;

            internal void <>m__0(bool sellRes)
            {
                GuildPurchaseOrderListReqNetTask.StartTask(delegate (bool res) {
                    GuildItemStoreInfoReqNetTask task = new GuildItemStoreInfoReqNetTask(false);
                    task.EventOnStop += delegate (Task task) {
                        if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                        {
                            this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                            LogicBlockGuildClient lBGuild = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild() as LogicBlockGuildClient;
                            if (lBGuild != null)
                            {
                                this.$this.m_mainCtrl.DisplayMoneyCount(lBGuild.GetGuildCurrency().m_tradeMoney);
                            }
                        }
                    };
                    task.Start(null, null);
                });
                if (this.callBack != null)
                {
                    this.callBack();
                }
            }

            internal void <>m__1(bool res)
            {
                GuildItemStoreInfoReqNetTask task = new GuildItemStoreInfoReqNetTask(false);
                task.EventOnStop += delegate (Task task) {
                    if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                    {
                        this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                        LogicBlockGuildClient lBGuild = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild() as LogicBlockGuildClient;
                        if (lBGuild != null)
                        {
                            this.$this.m_mainCtrl.DisplayMoneyCount(lBGuild.GetGuildCurrency().m_tradeMoney);
                        }
                    }
                };
                task.Start(null, null);
            }

            internal void <>m__2(Task task)
            {
                if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                {
                    this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                    LogicBlockGuildClient lBGuild = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild() as LogicBlockGuildClient;
                    if (lBGuild != null)
                    {
                        this.$this.m_mainCtrl.DisplayMoneyCount(lBGuild.GetGuildCurrency().m_tradeMoney);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrderModified>c__AnonStorey3
        {
            internal GuildPurchaseOrderInfo order;
            internal Action callBack;
            internal int iCount;
            internal GuildPurchaseUITask $this;
            private static Action <>f__am$cache0;
            private static Action <>f__am$cache1;

            internal void <>m__0()
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = () => TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildPurchaseCanceled, new object[0]), false);
                }
                this.$this.OrderModifySubmit(this.order, 0, <>f__am$cache0, this.callBack);
            }

            internal void <>m__1()
            {
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = delegate {
                    };
                }
                this.$this.OrderModifySubmit(this.order, this.iCount, <>f__am$cache1, this.callBack);
            }

            private static void <>m__2()
            {
                TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildPurchaseCanceled, new object[0]), false);
            }

            private static void <>m__3()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OrderCreateSubmit>c__AnonStorey6
        {
            internal Action callBack;
            internal GuildPurchaseUITask $this;

            internal void <>m__0(bool res)
            {
                if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                {
                    this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                }
                if (this.callBack != null)
                {
                    this.callBack();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OrderModifySubmit>c__AnonStorey4
        {
            internal Action callBack;
            internal Action onFinished;
            internal GuildPurchaseUITask $this;

            internal void <>m__0(bool res)
            {
                if ((this.$this.m_mainCtrl != null) && this.$this.m_mainCtrl.IsOpened)
                {
                    this.$this.m_mainCtrl.RefreshProductList(this.$this.m_itemInfo, ((LogicBlockGuildClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild()).GetGuildPurchaseOrderListInfo());
                }
                if (this.callBack != null)
                {
                    this.callBack();
                }
                if (this.onFinished != null)
                {
                    this.onFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <RequestData>c__AnonStorey1
        {
            internal bool listReqComplete;
            internal bool priceReqComplete;
            internal bool storeInfoReqComplete;
            internal bool currencyInfoReqComplete;
            internal Action callBack;

            internal void <>m__0(bool res)
            {
                this.listReqComplete = true;
                if ((this.priceReqComplete && (this.storeInfoReqComplete && this.currencyInfoReqComplete)) && (this.callBack != null))
                {
                    this.callBack();
                }
            }

            internal void <>m__1(bool res)
            {
                this.priceReqComplete = true;
                if ((this.listReqComplete && (this.storeInfoReqComplete && this.currencyInfoReqComplete)) && (this.callBack != null))
                {
                    this.callBack();
                }
            }

            internal void <>m__2(Task task)
            {
                this.storeInfoReqComplete = true;
                if ((this.listReqComplete && (this.priceReqComplete && this.currencyInfoReqComplete)) && (this.callBack != null))
                {
                    this.callBack();
                }
            }

            internal void <>m__3(Task task)
            {
                this.currencyInfoReqComplete = true;
                if ((this.listReqComplete && (this.storeInfoReqComplete && this.priceReqComplete)) && (this.callBack != null))
                {
                    this.callBack();
                }
            }
        }
    }
}

