﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BattlePassPurchaseUIController : UIControllerBase
    {
        [AutoBind("./GiftGroup/ConfirmButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GitButtonState;
        [AutoBind("./GiftGroup/ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./GiftGroup/ConfirmButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CostText;
        [AutoBind("./BindingGiftGroup/ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindConfirmButton;
        [AutoBind("./BindingGiftGroup/ConfirmButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindCostText;
        [AutoBind("./BindingGiftGroup/ConfirmButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BindGitButtonState;
        [AutoBind("./BlackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BlackButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView()
        {
        }
    }
}

