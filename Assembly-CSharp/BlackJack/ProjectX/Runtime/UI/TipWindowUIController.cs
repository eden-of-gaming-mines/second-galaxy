﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class TipWindowUIController : UIControllerBase
    {
        private List<KeyValuePair<Text, TweenMain>> m_tipItemList;
        private List<Transform> m_tipTextPosDummyList;
        [AutoBind("./DummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_dummyRoot;
        private const string TipStationMode = "InStation";
        private const string TipSpaceMode = "InSpace";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsAnyTipItemEmpty;
        private static DelegateBridge __Hotfix_UpdateTipWindowInfo;
        private static DelegateBridge __Hotfix_UpdateUsedTipItemPosDummy;
        private static DelegateBridge __Hotfix_CloseAlTip;
        private static DelegateBridge __Hotfix_GetTipItem;

        [MethodImpl(0x8000)]
        public void CloseAlTip()
        {
        }

        [MethodImpl(0x8000)]
        private KeyValuePair<Text, TweenMain> GetTipItem()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnyTipItemEmpty()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTipWindowInfo(string tipInfo, TipWindowUITask.TipType tipType, bool isInSpace)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUsedTipItemPosDummy()
        {
        }
    }
}

