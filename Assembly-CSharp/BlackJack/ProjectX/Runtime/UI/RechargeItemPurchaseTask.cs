﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class RechargeItemPurchaseTask : RechargeGoodsPurchaseTaskBase
    {
        private int m_lbRechargeItem;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckCanDoPurchase;
        private static DelegateBridge __Hotfix_IsIosPromoting;
        private static DelegateBridge __Hotfix_SendPurchaseReq;
        private static DelegateBridge __Hotfix_GetGoodsFilter;
        private static DelegateBridge __Hotfix_SendPayCancelOrFailedReq;

        [MethodImpl(0x8000)]
        public RechargeItemPurchaseTask(int rechargetItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool CheckCanDoPurchase()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetGoodsFilter(RechargeGoodsInfo goods)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsIosPromoting()
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendPayCancelOrFailedReq(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendPurchaseReq(Action<bool> onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <SendPayCancelOrFailedReq>c__AnonStorey1
        {
            internal Action onEnd;

            internal void <>m__0(Task task)
            {
                RechargeItemBuyCancelReqNetTask task2 = task as RechargeItemBuyCancelReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
                else
                {
                    if (task2.Result != 0)
                    {
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendPurchaseReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal RechargeItemPurchaseTask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

