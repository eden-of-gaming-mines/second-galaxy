﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class PurchaseOrderFormBoxUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainPanel;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipInfo;
        [AutoBind("./DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_infoBtn;
        [AutoBind("./DetailInfoPanel/BackBtn", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backImgBtn;
        [AutoBind("./ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmBtn;
        [AutoBind("./ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelBtn;
        [AutoBind("./ContentGroup/STitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_totalGuildMoney;
        [AutoBind("./ContentGroup/STitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addGuildMoneyBtn;
        [AutoBind("./ContentGroup/PurchasePanel/NumberImage", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_totalCount;
        [AutoBind("./ContentGroup/PurchasePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_totalRemoveBtn;
        [AutoBind("./ContentGroup/PurchasePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_totalAddBtn;
        [AutoBind("./ContentGroup/PurchasePanel/ItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_totalPrice;
        [AutoBind("./ContentGroup/Scroll View/Viewport/Content/PurchaseItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public PurchaseItemUIController m_purchaseItem;
        private const string Show = "Show";
        private const string Close = "Close";
        private const string PanelOpen = "PanelOpen";
        private const string PanelClose = "PanelClose";
        private long m_totalInputValue;
        private readonly int m_defaultCount;
        private readonly Dictionary<int, int> m_id2CountDict;
        private readonly Dictionary<int, ConfigDataAuctionItemInfo> m_id2InfoDict;
        private readonly List<ItemTypeId> m_unpurchasedItems;
        private readonly List<PurchaseItemUIController> m_itemList;
        public readonly Dictionary<int, int> m_selectedItemCount;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FakeLBStoreItem, Vector3> OnItemDetailOpen;
        private static DelegateBridge __Hotfix_WindowOpen;
        private static DelegateBridge __Hotfix_WindowClose;
        private static DelegateBridge __Hotfix_SetInfoTipsState;
        private static DelegateBridge __Hotfix_SetContent;
        private static DelegateBridge __Hotfix_OpenDetail;
        private static DelegateBridge __Hotfix_PurchaseOrderFormBoxUIController_OnCountChanged;
        private static DelegateBridge __Hotfix_UpdateSelectedCount;
        private static DelegateBridge __Hotfix_ChangeAllItemCount;
        private static DelegateBridge __Hotfix_AddItemIntoDict;
        private static DelegateBridge __Hotfix_GetShipTradeId;
        private static DelegateBridge __Hotfix_GetItemTradeId;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnNumberInputChanged;
        private static DelegateBridge __Hotfix_AddTotalCount;
        private static DelegateBridge __Hotfix_MinusTotalCount;
        private static DelegateBridge __Hotfix_RecalculatePrice;
        private static DelegateBridge __Hotfix_ResetEvents;
        private static DelegateBridge __Hotfix_add_OnItemDetailOpen;
        private static DelegateBridge __Hotfix_remove_OnItemDetailOpen;

        public event Action<FakeLBStoreItem, Vector3> OnItemDetailOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddItemIntoDict(int itemId, int count)
        {
        }

        [MethodImpl(0x8000)]
        private void AddTotalCount()
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeAllItemCount(int deltaTotalCount)
        {
        }

        [MethodImpl(0x8000)]
        private int GetItemTradeId(StoreItemType itemType, int itemId, ProjectXPlayerContext context)
        {
        }

        [MethodImpl(0x8000)]
        private int GetShipTradeId(int shipId, ProjectXPlayerContext context)
        {
        }

        [MethodImpl(0x8000)]
        private void MinusTotalCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNumberInputChanged(string value)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenDetail(FakeLBStoreItem item, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void PurchaseOrderFormBoxUIController_OnCountChanged(int count, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void RecalculatePrice()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetEvents()
        {
        }

        [MethodImpl(0x8000)]
        public void SetContent(ShipCustomTemplateInfo templateInfo, ProjectXPlayerContext context, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfoTipsState(bool isOpen, bool immediatly = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectedCount(int itemId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void WindowClose(Action<bool> onFinish = null)
        {
        }

        [MethodImpl(0x8000)]
        public void WindowOpen(Action<bool> onFinish = null)
        {
        }
    }
}

