﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CommonUIStateCtrlAnimatorBehaviour : StateMachineBehaviour
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Animator, AnimatorStateInfo, int> EventOnStateExit;
        private static DelegateBridge __Hotfix_OnStateExit;
        private static DelegateBridge __Hotfix_add_EventOnStateExit;
        private static DelegateBridge __Hotfix_remove_EventOnStateExit;

        public event Action<Animator, AnimatorStateInfo, int> EventOnStateExit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
        }
    }
}

