﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SolarSystemTargetTagController : PrefabControllerBase
    {
        protected SolarSystemTargetNormalTagUIController m_normalTagCtrl;
        protected SolarSystemTargetNormalWithNameTagUIController m_normalWithNameTagCtrl;
        protected SolarSystemTargetLockedTagUIController m_lockedTagCtrl;
        protected SolarSystemTargetSelectedTagUIController m_selectedTagCtrl;
        protected string m_targetName;
        private static DelegateBridge __Hotfix_UpdateSelectedSolarSystemConfigObjectsState;
        private static DelegateBridge __Hotfix_UpdateSolarSystemConfigObjectsState;
        private static DelegateBridge __Hotfix_UpdateNormalTargetTagState;
        private static DelegateBridge __Hotfix_UpdateNormalWithNameTagState;
        private static DelegateBridge __Hotfix_UpdateSelectedTargetTagState;
        private static DelegateBridge __Hotfix_UpdateLockedTargetTagState;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_AttachHudItem;
        private static DelegateBridge __Hotfix_set_TargetName;

        [MethodImpl(0x8000)]
        private void AttachHudItem(GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void Release(Action<GameObject> freeTagItemFunc)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLockedTargetTagState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, Vector3 pos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, bool isTargetVisible, string targetNoticeState, string targetCrimeState, double dist, Sprite sprite, float sheildPerc = 1f, float armorPerc = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNormalTargetTagState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, Vector3 pos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, string targetNoticeState, string targetCrimeState, bool isTargetVisible, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNormalWithNameTagState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, Vector3 pos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, bool isTargetVisible, string targetNoticeState, string targetCrimeState, double dist, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedSolarSystemConfigObjectsState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, bool isVisible, SpaceObjectType spcaObjectType, Vector3 pos, double distance, string targetCrimeState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedTargetTagState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, Vector3 pos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, bool isTargetVisible, string targetNoticeState, string targetCrimeState, double dist, Sprite sprite, float shieldPerc = 1f, float armorPerc = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemConfigObjectsState(Func<string, GameObject> allocTagItemFunc, Action<GameObject> freeTagItemFunc, bool isVisible, SpaceObjectType spcaObjectType, Vector3 pos, double distance, string targetCrimeState)
        {
        }

        public string TargetName
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

