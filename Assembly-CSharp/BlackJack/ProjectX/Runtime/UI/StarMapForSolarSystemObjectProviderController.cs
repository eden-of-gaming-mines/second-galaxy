﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StarMapForSolarSystemObjectProviderController : PrefabControllerBase
    {
        [AutoBind("./CelestialRoot/RockPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rockPlanet;
        [AutoBind("./CelestialRoot/DesertPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_desertPlanet;
        [AutoBind("./CelestialRoot/FrozenPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_frozenPlanet;
        [AutoBind("./CelestialRoot/EarthPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_earthPlanet;
        [AutoBind("./CelestialRoot/StormPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_stormPlanet;
        [AutoBind("./CelestialRoot/OceanPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_oceanPlanet;
        [AutoBind("./CelestialRoot/GasPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_gasPlanet;
        [AutoBind("./CelestialRoot/LavaPlanetObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lavaPlanet;
        [AutoBind("./CelestialRoot/SunObj", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sun;
        private static DelegateBridge __Hotfix_CreateNewPlanetGameObjByPlanetInfo;
        private static DelegateBridge __Hotfix_CreateNewSunGameObjByStarInfo;

        [MethodImpl(0x8000)]
        public GameObject CreateNewPlanetGameObjByPlanetInfo(GDBPlanetInfo planetInfo)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject CreateNewSunGameObjByStarInfo(GDBStarInfo gdbStarInfo)
        {
        }
    }
}

