﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BranchStoryUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTipBgButtonClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./ShowCloseOpenGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_openPanelUIStateCtrl;
        [AutoBind("./ShowCloseThreadScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_puzzleQuestPanelStateCtrl;
        [AutoBind("./ShowPlot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_realQuestPanelUIStateCtrl;
        [AutoBind("./ShowAward", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rewardListPanelUIStateCtrl;
        [AutoBind("./BranchDetailInfoPanelUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelCtrl;
        [AutoBind("./BranchDetailInfoPanelUIPrefab/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TipBgButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_GetQuestPanelShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetBranchOpenPanelShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetPuzzleQuestPanelShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetRealQuestPanelShowUIProcess;
        private static DelegateBridge __Hotfix_GetRewardListanelShowUIProcess;
        private static DelegateBridge __Hotfix_GetTipPanelShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_SetTipPanelPostion;
        private static DelegateBridge __Hotfix_OnTipBgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTipBgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTipBgButtonClick;

        public event Action EventOnTipBgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetBranchOpenPanelShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPuzzleQuestPanelShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetQuestPanelShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetRealQuestPanelShowUIProcess(bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetRewardListanelShowUIProcess(bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetTipPanelShowOrHideUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 postion)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTipPanelPostion(Vector3 pos)
        {
        }
    }
}

