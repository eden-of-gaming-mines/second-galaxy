﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildLogoSetReqNetworkTask : LogoSetReqNetworkTask
    {
        private int m_result;
        private readonly int m_bgId;
        private readonly int m_paintingId;
        private readonly int m_bgColorId;
        private readonly int m_paintingColorId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildLogoSetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildLogoSetReqNetworkTask(int bgId, int paintingId, int bgColorId, int paintingColorId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLogoSetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

