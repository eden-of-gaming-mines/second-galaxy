﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemNavigationPointUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_navigationPointCtrl;
        [AutoBind("./circle", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_externalCircleImage;
        [AutoBind("./point", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_innerCircleImage;
        private static DelegateBridge __Hotfix_ShowNavigationPoint;
        private static DelegateBridge __Hotfix_HideNavigationPoint;
        private static DelegateBridge __Hotfix_SetNavigationPointColor;

        [MethodImpl(0x8000)]
        public void HideNavigationPoint()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNavigationPointColor(Color color)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNavigationPoint(bool isCurrSolarSystem)
        {
        }
    }
}

