﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using UnityEngine.UI;

    public class StarMapMainAndBackButtomUIController : UIControllerBase
    {
        [AutoBind("./MainButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_mainButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backButton;
    }
}

