﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class ReturnRewardUIController : UIControllerBase
    {
        private DateTime m_nextAvailableTickTime;
        private DateTime m_nextDailyRefreshTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataDailyLoginInfo> EventOnRewardGetBtnClick;
        private List<DailyLoginRewardInfo> m_itemRewardReadonlyInfoList;
        private List<ReturnRewardItemUIController> m_itemCtrlList;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private const string RewardUIItem = "ReturnRewardItem";
        [AutoBind("./ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemGroup;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [AutoBind("./7dayGroup/AwardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_day7AwardUIStateCtrl;
        [AutoBind("./7dayGroup/AwardGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_day7AwardGetBtn;
        [AutoBind("./SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_simpleItemDummy;
        [AutoBind("./7dayGroup/AwardGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_countDownText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnRewardGetBtnClick;
        private static DelegateBridge __Hotfix_OnDay7GetBtnClick;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateRewardItemList;
        private static DelegateBridge __Hotfix_GetSimpleInfoPos;
        private static DelegateBridge __Hotfix_UpdateDay7Reward;
        private static DelegateBridge __Hotfix_UpdateDay6Reward;
        private static DelegateBridge __Hotfix_Update4CountDown;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_InitPool;
        private static DelegateBridge __Hotfix_CreateRewardItemList;
        private static DelegateBridge __Hotfix_OnPoolObjectCreate;
        private static DelegateBridge __Hotfix_OnPoolObjectReturnPool;
        private static DelegateBridge __Hotfix_add_EventOnRewardGetBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardGetBtnClick;

        public event Action<ConfigDataDailyLoginInfo> EventOnRewardGetBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateRewardItemList()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimpleInfoPos()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDay7GetBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreate(string name, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectReturnPool(string name, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardGetBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void Update4CountDown()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDay6Reward()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDay7Reward()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardItemList(List<DailyLoginRewardInfo> rewardInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

