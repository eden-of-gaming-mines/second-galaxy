﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonCaptainIconUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public ScrollItemBaseUIController m_scrollCtrl;
        private bool m_showBigLevelText;
        private string UIState_Normal;
        private string UIState_Selected;
        private string CaptainState_Normal;
        private string CaptainState_WorkState;
        private string CaptainState_ExpAdd;
        private string CaptainState_LevelUp;
        private string CaptainState_Commander;
        private string CaptainState_Malfunction;
        private string CaptainState_MalfunctionWorking;
        private string CaptainState_Upgrade;
        private string CaptainState_UpgradeWorking;
        private string CaptainState_NormalBigLevelText;
        private string CaptainState_WorkStateBigLevelText;
        private string CaptainState_ExpAddBigLevelText;
        private string CaptainState_LevelUpBigLevelText;
        private string CaptainState_CommanderBigLevelText;
        private string CaptainState_MalfunctionBigLevelText;
        private string CaptainState_MalfunctionWorkingBigLevelText;
        private string CaptainState_UpgradeBigLevelText;
        private string CaptainState_UpgradeWorkingBigLevelText;
        private string CaptainWorkState_WingMan;
        private string CaptainWorkState_WingManNoIndex;
        private string CaptainWorkState_Working;
        private string CaptainWorkState_Delegating;
        private string CaptainWorkState_Standby;
        private const string CharacterDiplomacyStateEnemy = "Enemy";
        private const string CharacterDiplomacyStateFriend = "Friend";
        private const string CharacterDiplomacyStateNeutral = "Neutral";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IconButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selectStateCtrl;
        [AutoBind("./CaptainIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CaptainIcon;
        [AutoBind("./LvState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainStateChangeEffectCtrl;
        [AutoBind("./LvState/CaptainState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainWorkingStateCtrl;
        [AutoBind("./LvState/CaptainState/WingManText/IndexText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingManIndexText;
        [AutoBind("./LvState/LvTextGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelText;
        [AutoBind("./LvState/SmallCaptainLvTextGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BigLevelText;
        [AutoBind("./LvState/ProgressBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExpProgressBarRoot;
        [AutoBind("./LvState/ProgressBGImage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ExpProgressBar;
        [AutoBind("./RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RankImage;
        [AutoBind("./ProfessionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIcon;
        [AutoBind("./StrikeResultState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StrikeFailState;
        [AutoBind("./DiplomacyState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DiplomacyStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_SetCaptainInfo_2;
        private static DelegateBridge __Hotfix_SetCaptainInfo_1;
        private static DelegateBridge __Hotfix_SetCaptainInfo_0;
        private static DelegateBridge __Hotfix_SetCaptianSelectState;
        private static DelegateBridge __Hotfix_SetStrikeDestroyedState;
        private static DelegateBridge __Hotfix_SetExpAddState;
        private static DelegateBridge __Hotfix_SetLevelUpState;
        private static DelegateBridge __Hotfix_SetToCommanderUIState;
        private static DelegateBridge __Hotfix_SetCommanderInfo_0;
        private static DelegateBridge __Hotfix_SetCommanderInfo_5;
        private static DelegateBridge __Hotfix_SetCommanderInfo_4;
        private static DelegateBridge __Hotfix_SetCommanderInfo_3;
        private static DelegateBridge __Hotfix_SetCommanderInfo_1;
        private static DelegateBridge __Hotfix_SetCommanderInfo_2;
        private static DelegateBridge __Hotfix_SetCommanderInfo_6;
        private static DelegateBridge __Hotfix_SetLevelInfo;
        private static DelegateBridge __Hotfix_SetAvatarIcon;
        private static DelegateBridge __Hotfix_SetPrefessionIcon;
        private static DelegateBridge __Hotfix_SetLevelText;
        private static DelegateBridge __Hotfix_IsCaptainHasShipReadyForActivate;
        private static DelegateBridge __Hotfix_IsCaptainHasShipNeedRepair_1;
        private static DelegateBridge __Hotfix_IsCaptainHasShipNeedRepair_0;
        private static DelegateBridge __Hotfix_SetDiplomacyState_0;
        private static DelegateBridge __Hotfix_SetDiplomacyState_1;
        private static DelegateBridge __Hotfix_GetCaptainNormalState;
        private static DelegateBridge __Hotfix_GetCaptainWorkState;
        private static DelegateBridge __Hotfix_GetCaptainExpAddState;
        private static DelegateBridge __Hotfix_GetCaptainLevelUpState;
        private static DelegateBridge __Hotfix_GetCaptainCommanderState;
        private static DelegateBridge __Hotfix_GetCaptainMalfunctionState;
        private static DelegateBridge __Hotfix_GetCaptainMalfunctionWorkingState;
        private static DelegateBridge __Hotfix_GetCaptainUpgardeState;
        private static DelegateBridge __Hotfix_GetCaptainUpgardeWorkingState;

        [MethodImpl(0x8000)]
        private string GetCaptainCommanderState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainExpAddState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainLevelUpState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainMalfunctionState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainMalfunctionWorkingState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainNormalState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainUpgardeState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainUpgardeWorkingState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCaptainWorkState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareItemClick = true)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCaptainHasShipNeedRepair(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCaptainHasShipNeedRepair(NpcCaptainInfo captain)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCaptainHasShipReadyForActivate(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAvatarIcon(int avatarId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainInfo(NpcCaptainStaticInfo captain, int captainLevel, bool isDestoryed, Dictionary<string, UnityEngine.Object> resDict, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainInfo(LBStaticHiredCaptain captain, bool isSelected, Dictionary<string, UnityEngine.Object> resDict, int wingmanIndex = -1, DiplomacyState diplomacyState = 2, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainInfo(NpcCaptainInfo captain, bool isHistoryInfo, bool isSelected, Dictionary<string, UnityEngine.Object> resDict, int wingmanIndex = -1, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptianSelectState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(ChatInfo chatInfo, Dictionary<string, UnityEngine.Object> resDict, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(KillRecordOpponentInfo info, Dictionary<string, UnityEngine.Object> resDict, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(RankingPlayerBasicInfo info, Dictionary<string, UnityEngine.Object> resDict, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(TeamMainUIController.TeamMemberUIItemInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(PlayerSimpleInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict, bool showLevel = true, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(PlayerSimplestInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict, bool showLevel = false, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderInfo(int avatarId, ProfessionType profession, int level, Dictionary<string, UnityEngine.Object> resDict, DiplomacyState diplomacyState = 2, bool isBigLevelText = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDiplomacyState(DiplomacyState state)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDiplomacyState(uint guildId, uint allianceId, string userId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExpAddState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLevelInfo(int level, long exp)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLevelText(int level, bool showLevel = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLevelUpState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPrefessionIcon(ProfessionType profession, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStrikeDestroyedState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToCommanderUIState()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

