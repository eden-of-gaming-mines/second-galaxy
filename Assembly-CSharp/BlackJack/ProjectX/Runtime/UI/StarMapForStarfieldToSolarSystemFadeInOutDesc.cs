﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    public class StarMapForStarfieldToSolarSystemFadeInOutDesc : MonoBehaviour
    {
        [Header("摄像机平移操作的曲线")]
        public AnimationCurve m_curveForCameraMove;
        [Header("摄像机平移操作开始时的延迟时间")]
        public float m_delayTimeForCameraMove;
        [Header("摄像机平移操作持续时间")]
        public float m_timeLengthForCameraMove;
        [Header("摄像机平移操作：最小触发距离")]
        public float m_distanceMinForCameraMoveActive;
        [Header("摄像机缩放操作的曲线")]
        public AnimationCurve m_curveForCameraScale;
        [Header("摄像机缩放操作开始时的延迟时间")]
        public float m_delayTimeForCameraScale;
        [Header("摄像机缩放操作持续时间")]
        public float m_timeLengthForCameraScale;
        [Header("摄像机缩放操作结束后的目标缩放值")]
        public float m_targetCameraScaleValue;
        [Header("摄像机缩放操作：触发操作的缩放值最小差值")]
        public float m_deltaScaleMinForCameraScaleActive;
        [Header("渐黑开始延迟时间")]
        public float m_delayTimeForFadeOut;
        [Header("星域星图渐黑持续时间")]
        public float m_fadeOutTimeLength;
        [Header("切换到恒星系星图时渐白持续时间")]
        public float m_fadeInTimeLength;
    }
}

