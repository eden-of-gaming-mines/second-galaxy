﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class RescueSignalButtonUIControllerBase : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRescueButtonClick;
        private RescueSignalType m_curRescueSignalType;
        private int m_emergencyCount;
        private int m_selfRescueCount;
        private int m_guildRescueCount;
        protected GameObject m_notifyLoopEffectGo;
        private const string UIStateEmergency = "Emergency";
        private const string UIStateSelfRescue = "Self";
        private const string UIStateGuildRescue = "Alliance";
        private const string UIStateNone = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RescueButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RescueButtonStateCtrl;
        [AutoBind("./NumberBG/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text RescueCountText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRescueButton;
        private static DelegateBridge __Hotfix_InitRescueSignalCount;
        private static DelegateBridge __Hotfix_GetCurrExistRescueSignalType;
        private static DelegateBridge __Hotfix_IsNeedChangeRescueButtonState;
        private static DelegateBridge __Hotfix_IsNeedApplyRescueButtonCountChange;
        private static DelegateBridge __Hotfix_GetRescueSignalCount;
        private static DelegateBridge __Hotfix_SetRescueSignalCount;
        private static DelegateBridge __Hotfix_GetUIStateNameByRescueSignalType;
        private static DelegateBridge __Hotfix_OnRescueButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRescueButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueButtonClick;
        private static DelegateBridge __Hotfix_get_CurRescueSignalType;

        public event Action EventOnRescueButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private RescueSignalType GetCurrExistRescueSignalType()
        {
        }

        [MethodImpl(0x8000)]
        private int GetRescueSignalCount(RescueSignalType type)
        {
        }

        [MethodImpl(0x8000)]
        private string GetUIStateNameByRescueSignalType(RescueSignalType type)
        {
        }

        [MethodImpl(0x8000)]
        public void InitRescueSignalCount(RescueSignalType rescueSignalType, int count)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedApplyRescueButtonCountChange(RescueSignalType rescueSignalType, int count)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedChangeRescueButtonState(RescueSignalType rescueSignalType, int count, bool forceChangeToLowerState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRescueSignalCount(RescueSignalType type, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRescueButton(RescueSignalType rescueSignalType, int count, bool forceChangeToLowerState = false)
        {
        }

        public RescueSignalType CurRescueSignalType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

