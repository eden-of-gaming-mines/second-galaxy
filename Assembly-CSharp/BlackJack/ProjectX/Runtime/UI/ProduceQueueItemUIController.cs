﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceQueueItemUIController : UIControllerBase
    {
        protected const string IdleMode = "Free";
        protected const string LockMode = "Lock";
        protected const string ProduceMode = "Normal";
        protected const string CompleteMode = "Complete";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProductionLine, int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBProductionLine> EventOnItemProduceCompleted;
        protected LBProductionLine m_currProduceLine;
        protected bool m_isProducing;
        protected float m_completeProgress;
        public CommonItemIconUIController m_commonItemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ClickImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemUIStartCtrl;
        [AutoBind("./ProgressBarGroup/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBarImage;
        [AutoBind("./ProgressBarGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ShipInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ShipInfoGroup/CountTitleText/CountNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CountNumberText;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateItemViewInfo;
        private static DelegateBridge __Hotfix_SetSelecteState;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemProduceCompleted;
        private static DelegateBridge __Hotfix_remove_EventOnItemProduceCompleted;
        private static DelegateBridge __Hotfix_get_CurrProduceLine;
        private static DelegateBridge __Hotfix_get_CompleteProgress;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<LBProductionLine, int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProductionLine> EventOnItemProduceCompleted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelecteState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(LBProductionLine produceLine, Dictionary<string, UnityEngine.Object> m_resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateItemViewInfo(DateTime currTime)
        {
        }

        public LBProductionLine CurrProduceLine
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float CompleteProgress
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

