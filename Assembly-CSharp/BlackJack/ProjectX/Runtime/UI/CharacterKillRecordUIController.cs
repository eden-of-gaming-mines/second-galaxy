﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterKillRecordUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOnBGButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnKillToggleSelected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLoseToggleSelected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnItemDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnItemIconButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo> EventOnItemSendLinkButtonClick;
        public List<KillRecordInfo> m_killRecordListCache;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCache;
        public string m_currListMode;
        public const string KillMode = "Kill";
        public const string BeKillMode = "BeKilled";
        private const string m_itemAssetName = "KillReportInfoUIPrefab";
        private const string m_itemPoolName = "KillReportInfoUIPrefab";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./NoItemText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NoRecordTipText;
        [AutoBind("./KillReportPanel/SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SimpleInfoDummy;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./ToggleScroll View/Viewport/Content/KillToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx KillToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/BeKilledToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx BeKilledToggle;
        [AutoBind("./ToggleScroll View/Viewport/Content/BeKilledToggle/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_beKilledRedPoint;
        [AutoBind("./KillReportPanel/KillReportScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_objPool;
        [AutoBind("./KillReportPanel/KillReportScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect LoopScrollView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateKillRecordListInfo;
        private static DelegateBridge __Hotfix_UpdateRedPoint;
        private static DelegateBridge __Hotfix_OnUIItemFill;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnKillToggleValueChanged;
        private static DelegateBridge __Hotfix_OnLoseToggleValueChanged;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnItemIconButtonClick;
        private static DelegateBridge __Hotfix_OnItemSendLinkButtonClick;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_add_EventOnOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnKillToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnKillToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnLoseToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnLoseToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemSendLinkButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemSendLinkButtonClick;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnItemIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnItemSendLinkButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnKillToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLoseToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDetailButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemIconButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSendLinkButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnKillToggleValueChanged(UIControllerBase uCtrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoseToggleValueChanged(UIControllerBase uCtrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUIItemFill(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateKillRecordListInfo(string mode, List<KillRecordInfo> killRecordList, Dictionary<string, UnityEngine.Object> dynamicResDict, bool isRefillFromHead = true)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRedPoint(bool isShowKillSuccessRedPoint, bool isShowBeKilledRedPoint)
        {
        }
    }
}

