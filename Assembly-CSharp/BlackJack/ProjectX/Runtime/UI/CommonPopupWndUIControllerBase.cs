﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonPopupWndUIControllerBase : UIControllerBase
    {
        public CommonUIStateController m_commonUIStateCtrl;
        protected UITaskBase m_mainTask;
        protected Action m_onStateEnd;
        protected Action m_onShowEnd;
        protected Action m_onHideEnd;
        protected WndState m_wndState;
        protected WndState m_prevState;
        protected bool m_isDuringState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnShowFinished;
        private static DelegateBridge __Hotfix_OnHideFinished;
        private static DelegateBridge __Hotfix_OnStateFinished;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_Hide;
        private static DelegateBridge __Hotfix_StartState_0;
        private static DelegateBridge __Hotfix_StartState_1;
        private static DelegateBridge __Hotfix_GetWndTweenDurationMax;
        private static DelegateBridge __Hotfix_IsOpeningOrClosing;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_IsEnable;

        [MethodImpl(0x8000)]
        public float GetWndTweenDurationMax(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public bool Hide(string stateName, Action onStateStart, Action onStateEnd, bool isImmediateStart, bool isForceStart, out bool hasImmediateStarted)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnable()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOpeningOrClosing()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void OnHideFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShowFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStateFinished()
        {
        }

        [MethodImpl(0x8000)]
        public bool Show(string stateName, Action onStateStart, Action onStateEnd, bool isImmediateStart, bool isForceStart, out bool hasImmediateStarted)
        {
        }

        [MethodImpl(0x8000)]
        public bool StartState(string stateName, Action onStateStart, Action onStateEnd, bool isImmediateStart, bool isForceStart, out bool hasImmediateStarted)
        {
        }

        [MethodImpl(0x8000)]
        public bool StartState(CommonUIStateController stateCtrl, string stateName, Action onStateStart, Action onStateEnd, bool isImmediateStart, bool isForceStart, out bool hasImmediateStarted)
        {
        }

        protected enum WndState
        {
            None,
            Opening,
            Opened,
            Closing,
            Closed
        }
    }
}

