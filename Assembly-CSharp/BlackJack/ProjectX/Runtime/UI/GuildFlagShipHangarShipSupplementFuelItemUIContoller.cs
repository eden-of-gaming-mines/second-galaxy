﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarShipSupplementFuelItemUIContoller : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private bool m_isShowFuelCount;
        private IStaticFlagShipDataContainer m_shipInfo;
        private ConfigDataFlagShipRelativeData m_shipConfig;
        public ScrollItemBaseUIController m_scrollCtrl;
        public GuildFlagShipHangarShipListItemUIController m_shipItemCtrl;
        public CommonItemIconUIController m_fuelItemCtrl;
        private static string m_fuelChangeFormatStr;
        private const string ShipAssetName = "ShipItem";
        private const string FuelAssetName = "Fuel";
        private const string StateNormal = "Normal";
        private const string StateEmpty = "Empty";
        private const string StateDestroyed = "Destroy";
        private const string StateLeave = "Leave";
        private const string StateUnpack = "UnPacking";
        private const string StateRed = "Red";
        private const string StateGreen = "Green";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnFuelValueChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnAddButtonClickChanged;
        [AutoBind("./ItemSimpleInfoUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemSimpleInfoUIDummy;
        [AutoBind("./FuelInfo/FuelNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FuelNameText;
        [AutoBind("./FuelInfo/FuelStoreNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FuelStoreNumberStateCtrl;
        [AutoBind("./FuelInfo/FuelStoreNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipFuelStoreNumberText;
        [AutoBind("./FuelInfo/ProgressBar/FuelNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FuelNumberText;
        [AutoBind("./FuelInfo/Slider", AutoBindAttribute.InitState.NotInit, false)]
        public UnityEngine.UI.Slider Slider;
        [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummy;
        [AutoBind("./GuilgHangarShipListItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ShipDummy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StateCtrl;
        private static DelegateBridge __Hotfix_UpdateGuildHangarListItem;
        private static DelegateBridge __Hotfix_SetFlagShipFuelChangeCount;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnUnPackingEnd;
        private static DelegateBridge __Hotfix_OnValueChanged;
        private static DelegateBridge __Hotfix_OnAddButtonClick;
        private static DelegateBridge __Hotfix_UpdateFuelValue;
        private static DelegateBridge __Hotfix_add_EventOnFuelValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnFuelValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnAddButtonClickChanged;
        private static DelegateBridge __Hotfix_remove_EventOnAddButtonClickChanged;

        public event Action<int, GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnAddButtonClickChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnFuelValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareClick = true)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnPackingEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnValueChanged(float currCount)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlagShipFuelChangeCount(long changeCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFuelValue()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildHangarListItem(IStaticFlagShipDataContainer ship, int unlockSlotCount, int buildingLevel, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

