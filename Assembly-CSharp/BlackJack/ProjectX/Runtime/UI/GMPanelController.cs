﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GMPanelController : UIControllerBase
    {
        private List<GameObject> m_audioItemList;
        private List<GameObject> m_npcItemList;
        private List<int> m_npcItemIdList;
        private const string StrYes = "是";
        private const string StrNo = "否";
        private int m_npcItemCount;
        private Dictionary<string, UnityEngine.Object> m_resDic;
        [AutoBind("./GM/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_scrollViewRoot;
        [AutoBind("./GM/ScrollViewElem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_scrollViewElem;
        [AutoBind("./GM/TextImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_gmText;
        [AutoBind("./GM/GMDescription", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_GMDescription;
        [AutoBind("./GM/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_gmiInputField;
        [AutoBind("./GM/Button", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_submitButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Type/Gm", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_gmButton;
        [AutoBind("./Type/Npc", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_npcButton;
        [AutoBind("./Npc/Global", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_globalNpcToggle;
        [AutoBind("./Npc/NormalRes", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_npcResToggle;
        [AutoBind("./Npc/NormalAudio", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_npcAudioToggle;
        [AutoBind("./Npc/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_npcInputField;
        [AutoBind("./Npc/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_npcSearchButton;
        [AutoBind("./Npc/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_npcDescText;
        [AutoBind("./Npc/NpcScroll", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_npcScrollRect;
        [AutoBind("./Npc/NpcScroll/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_npcItemRoot;
        [AutoBind("./Npc/NpcDailogId/Viewport/NpcDailogId/NpcDialog", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_dailogRoot;
        [AutoBind("./Npc/NpcDailogId/Viewport/NpcDailogId/NpcIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_npcIconImage;
        [AutoBind("./Npc/NpcDailogId/Viewport/DialogAudioItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_npcDailogTemplete;
        [AutoBind("./Npc/ScrollViewElem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_npcTemplete;
        [AutoBind("./Type/ReviewToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_review;
        [AutoBind("./Type/HuaweiToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_huawei;
        [AutoBind("./Type/ReviewToggle/check", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_reviewTxt;
        [AutoBind("./Type/HuaweiToggle/check", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_huaweiTxt;
        [AutoBind("./Type/DisplayPriory", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_displayButton;
        [AutoBind("./DisplayPriority", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_displayPanelGo;
        private GMDisplayTestUIController m_shipCountLimitCtrl;
        private GMDisplayTestUIController m_sceneObjectCountLimitCtrl;
        private GMDisplayTestUIController m_bulletCountLimitCtrl;
        private GMDisplayTestUIController m_effectCountLimitCtrl;
        private GMDisplayTestUIController m_laserBulletCountLimitCtrl;
        private GMDisplayTestUIController m_normalBulletCountLimitCtrl;
        private GMDisplayTestUIController m_selfLaunchEffectRateCtrl;
        private GMDisplayTestUIController m_selfTargetNormalEffectRateCtrl;
        private GMDisplayTestUIController m_selfTargetBusyEffectRateCtrl;
        private GMDisplayTestUIController m_displayShipEffectRateCtrl;
        private GMDisplayTestUIController m_noneDisplayShipEffectRateCtrl;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/ShipCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/SceneObjectCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sceneObjectCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/BulletCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_bulletCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/LaserBulletCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_laserBulletCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/NormalBulletCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_normalBulletCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/EffectCountLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_effectCountLimitGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/SelfLaunchEffectRate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfLaunchEffectRateGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/SelfTargetNormalEffectRate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfTargetNormalEffectRateGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/SelfTargetBusylEffectRate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfTargetBusyEffectRateGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/DisplayShipEffectRate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_displayShipEffectRateGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/NoneDisplayShipEffectRate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_noneDisplayShipEffectRateGo;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/ApplyButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_applyButton;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/DefaultButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_defaultButton;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/LastButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_lastButton;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/Level/Low", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_lowToggle;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/Level/Middle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_middleToggle;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/Level/High", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_highToggle;
        [AutoBind("./DisplayPriority/Scroll View/Viewport/Content/ImportantEffectCountLimit/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_importantEffectCountInputField;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetGMPanelState;
        private static DelegateBridge __Hotfix_SetPanelInfo;
        private static DelegateBridge __Hotfix_InitPanel;
        private static DelegateBridge __Hotfix_get_GMCommond;
        private static DelegateBridge __Hotfix_SyncSwitchStatus;
        private static DelegateBridge __Hotfix_InitNPCPanel;
        private static DelegateBridge __Hotfix_SerachNpcButtonClick;
        private static DelegateBridge __Hotfix_SetNpcItemInfo;
        private static DelegateBridge __Hotfix_SetAudioInfo;
        private static DelegateBridge __Hotfix_OnNpcItemClick;
        private static DelegateBridge __Hotfix_OnAudioItemClick;
        private static DelegateBridge __Hotfix_GetStringFromList;
        private static DelegateBridge __Hotfix_InitDisplayCtrl;
        private static DelegateBridge __Hotfix_OnApplyButtonClick;
        private static DelegateBridge __Hotfix_OnDefaultButtonClick;
        private static DelegateBridge __Hotfix_OnLastButtonClick;
        private static DelegateBridge __Hotfix_OnLowToggleClick;
        private static DelegateBridge __Hotfix_OnMiddleToggleClick;
        private static DelegateBridge __Hotfix_OnHighToggleClick;
        private static DelegateBridge __Hotfix_GetCurrPreformanceSetting;

        [MethodImpl(0x8000)]
        private SolarSystemTask.DisplayPerformance GetCurrPreformanceSetting()
        {
        }

        [MethodImpl(0x8000)]
        private string GetStringFromList<T>(List<T> paramList)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDisplayCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void InitNPCPanel(NPCState state, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void InitPanel(IEnumerable<KeyValuePair<int, ConfigDataGMInfoList>> GMInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnApplyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAudioItemClick(object config)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDefaultButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHighToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLastButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLowToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMiddleToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcItemClick(object config)
        {
        }

        [MethodImpl(0x8000)]
        private void SerachNpcButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAudioInfo(int index, string buttonText, object config)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGMPanelState(string state)
        {
        }

        [MethodImpl(0x8000)]
        private void SetNpcItemInfo(int index, string buttonText, object config, int configId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPanelInfo(string str)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncSwitchStatus()
        {
        }

        public string GMCommond
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <InitPanel>c__AnonStorey0
        {
            internal KeyValuePair<int, ConfigDataGMInfoList> one;
            internal GMPanelController $this;

            internal void <>m__0()
            {
                this.$this.m_GMDescription.text = this.one.Value.GMDescription;
                this.$this.m_gmiInputField.text = string.Empty;
                this.$this.SetPanelInfo(this.one.Value.GMCommand);
            }
        }

        [CompilerGenerated]
        private sealed class <SetAudioInfo>c__AnonStorey2
        {
            internal object config;
            internal GMPanelController $this;

            internal void <>m__0()
            {
                this.$this.OnAudioItemClick(this.config);
            }
        }

        [CompilerGenerated]
        private sealed class <SetNpcItemInfo>c__AnonStorey1
        {
            internal object config;
            internal GMPanelController $this;

            internal void <>m__0()
            {
                this.$this.OnNpcItemClick(this.config);
            }
        }

        public enum NPCState
        {
            Global,
            Res,
            Audio
        }
    }
}

