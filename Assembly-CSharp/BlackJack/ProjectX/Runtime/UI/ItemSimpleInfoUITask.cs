﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemSimpleInfoUITask : UITaskBase
    {
        public const string ItemSimpleInfoUIMode_OnlyDetail = "OnlyDetail";
        public const string ItemSimpleInfoUIMode_NoButton = "NoButton";
        public const string ItemSimpleInfoUIMode_PreviewAndProduce = "PreviewAndProduce";
        public const string ItemSimpleInfoUIMode_PreviewAndTech = "PreviewAndTech";
        public const string ItemSimpleInfoUIMode_PreviewAndChoose = "PreviewAndChoose";
        public const string ParamKey_WindowWorldPos = "WindowWorldPos";
        public const string ParamKey_PositionType = "PositionType";
        public const string ParamKey_ReturnToUIIntent = "ReturnToUIIntent";
        public const string ParamKey_LBItem = "LBItem";
        public const string ParamKey_IsShowItemObtainSource = "IsShowItemObtainSource";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        public const string ParamKey_CanGotoTechForRelativeTech = "CanGotoTechForRelativeTech";
        public const string ParamKey_CanShare = "ForbiddenShare";
        public const string ParamKey_FromIntentIsInStatck = "FromIntentIsInStatck";
        private ItemSimpleInfoUIMainController m_mainCtrl;
        private ItemSimpleInfoUIController m_itemSimpleInfoCtrl;
        private ItemObtainSourceUIController m_itemObtainSourceCtrl;
        private UIIntent m_cachedNextItemUIIntent;
        private IUIBackgroundManager m_backgroundManager;
        private bool m_canShare;
        private bool m_showGoTechButtonForRelativeTech;
        private bool m_fromIntentIsInStatck;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private ILBStoreItemClient m_lbItem;
        private ILBStoreItemClient m_lastLbItem;
        private UIIntent m_returnToUIIntent;
        private Vector3 m_windowWorldPos;
        private PositionType m_positionType;
        private int m_notCloseWindowFrameIndex;
        private bool m_isShowItemObtainSource;
        private bool m_closeWindowFlag;
        private Action m_onWindowClosed;
        private bool m_isOpening;
        private bool m_isClosing;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnEnterAnotherTask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient> EventOnChooseButtonClick;
        public const string TaskName = "ItemSimpleInfoUITask";
        private bool m_isNeedBG;
        private bool m_isDuringNotForceUserGuide;
        private GuideState m_currGuideState;
        private const string ParamKey_IsInRepeatableGuide = "IsInRepeatableGuide";
        private const string ParamKey_EnableBg = "EnableBg";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartItemSimpleInfoUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnItemObtainSourceButtonClick;
        private static DelegateBridge __Hotfix_OnChooseButtonClick;
        private static DelegateBridge __Hotfix_OnGoProduceButtonClick;
        private static DelegateBridge __Hotfix_OnGoTechButtonClick;
        private static DelegateBridge __Hotfix_OnItemPreviewButtonClick;
        private static DelegateBridge __Hotfix_OnGoToCrackButtonClick;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_IsTheSameItem;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoWindow;
        private static DelegateBridge __Hotfix_CalcPanelPos;
        private static DelegateBridge __Hotfix_UnregisterAllEvent;
        private static DelegateBridge __Hotfix_CloseSimpleInfoWindowImpl;
        private static DelegateBridge __Hotfix_CheckIsNeedToRefresh;
        private static DelegateBridge __Hotfix_StartProduceReapeatableGuide;
        private static DelegateBridge __Hotfix_StartWormHoleReapeatableGuide;
        private static DelegateBridge __Hotfix_StartCrackReapeatableGuide;
        private static DelegateBridge __Hotfix_StartDelegateMineralReapeatableGuide;
        private static DelegateBridge __Hotfix_StartDelegateFightReapeatableGuide;
        private static DelegateBridge __Hotfix_StartNpcShopReapeatableGuide;
        private static DelegateBridge __Hotfix_StartFreeQuestReapeatableGuide;
        private static DelegateBridge __Hotfix_StartInfectMissonRepeatableGuide;
        private static DelegateBridge __Hotfix_StartEnterAuctionGuide;
        private static DelegateBridge __Hotfix_StartEnterGuildProduction;
        private static DelegateBridge __Hotfix_StartEnterGuildAction;
        private static DelegateBridge __Hotfix_StartEnterBlackMarket_Personal;
        private static DelegateBridge __Hotfix_StartEnterBlackMarket_Guild;
        private static DelegateBridge __Hotfix_StartEnterBlackMarket_IrShop;
        private static DelegateBridge __Hotfix_StartItemDetailUITask;
        private static DelegateBridge __Hotfix_StartEnterGuildGalaNpcShop;
        private static DelegateBridge __Hotfix_StartEnterPanGalaNpcShop;
        private static DelegateBridge __Hotfix_EnterNpcShop;
        private static DelegateBridge __Hotfix_GotoScienceExplore;
        private static DelegateBridge __Hotfix_GotoSpaceSignal;
        private static DelegateBridge __Hotfix_StartFactionCreditUI;
        private static DelegateBridge __Hotfix_LeaveBackground;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnEnterAnotherTask;
        private static DelegateBridge __Hotfix_remove_EventOnEnterAnotherTask;
        private static DelegateBridge __Hotfix_add_EventOnChooseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChooseButtonClick;
        private static DelegateBridge __Hotfix_StartItemSimpleInfoForNotForceGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;

        public event Action<ILBStoreItemClient> EventOnChooseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string> EventOnEnterAnotherTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ItemSimpleInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop()
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 CalcPanelPos()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsNeedToRefresh()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseItemSimpleInfoWindow(Action onWindowClosed)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseSimpleInfoWindowImpl(Action onWindowClosed)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnterNpcShop(NpcShopFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private void GotoScienceExplore()
        {
        }

        [MethodImpl(0x8000)]
        protected void GotoSpaceSignal()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTheSameItem(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundClick(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnChooseButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoProduceButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoTechButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoToCrackButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemObtainSourceButtonClick(ItemObtainSourceType srcType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemPreviewButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartCrackReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartDelegateFightReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartDelegateMineralReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterAuctionGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterBlackMarket_Guild()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterBlackMarket_IrShop()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterBlackMarket_Personal()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterGuildAction()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterGuildGalaNpcShop()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterGuildProduction()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterPanGalaNpcShop()
        {
        }

        [MethodImpl(0x8000)]
        private void StartFactionCreditUI()
        {
        }

        [MethodImpl(0x8000)]
        private void StartFreeQuestReapeatableGuide(QuestType type = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void StartInfectMissonRepeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartItemDetailUITask(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ItemSimpleInfoUITask StartItemSimpleInfoForNotForceGuide(ILBStoreItemClient item, UIIntent returnToUIIntent, bool isReturnIntentInStatck, Vector3 windowWorldPosition, string mode, PositionType posType, bool isShowItemObtainSource = false, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null, bool showGoTechForRelativeTech = true, bool canShare = true, bool enableBg = true)
        {
        }

        [MethodImpl(0x8000)]
        public static ItemSimpleInfoUITask StartItemSimpleInfoUITask(ILBStoreItemClient item, UIIntent returnToUIIntent, bool isReturnIntentInStatck, Vector3 windowWorldPosition, string mode, PositionType posType, bool isShowItemObtainSource = false, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null, bool showGoTechForRelativeTech = true, bool canShare = true, bool enableBg = true)
        {
        }

        [MethodImpl(0x8000)]
        private void StartNpcShopReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartProduceReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartWormHoleReapeatableGuide()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CloseSimpleInfoWindowImpl>c__AnonStorey2
        {
            internal Action onWindowClosed;
            internal ItemSimpleInfoUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.m_isClosing = false;
                if (!this.$this.CheckIsNeedToRefresh())
                {
                    this.$this.Pause();
                    this.$this.m_lastLbItem = this.$this.m_lbItem;
                    this.$this.m_lbItem = null;
                    if (this.onWindowClosed != null)
                    {
                        this.onWindowClosed();
                    }
                    this.$this.m_lastLbItem = null;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal ItemSimpleInfoUITask $this;

            internal void <>m__0()
            {
                Vector3 worldPos = this.$this.CalcPanelPos();
                this.$this.m_mainCtrl.SetPosition(worldPos);
                this.$this.m_mainCtrl.CanvasAlphaCtrl.alpha = 1f;
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        private enum GuideState
        {
            None,
            Start,
            Comfirm
        }

        public enum PositionType
        {
            UseInput,
            OnLeft,
            OnTop,
            OnRight,
            OnBottom,
            LeftTop,
            RightTop
        }
    }
}

