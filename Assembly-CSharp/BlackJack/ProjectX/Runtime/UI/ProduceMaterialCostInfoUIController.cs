﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceMaterialCostInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBlueprintButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int, int, Vector3, long> EventOnMaterialIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAddBlueprintButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDeleteBlueprintButtonClick;
        protected bool m_isSelfProduce;
        protected CommonItemIconUIController m_blueprintItemCtrl;
        protected List<CostInfo> m_viewCostInfoList;
        protected List<GameObject> m_captainEffectImageList;
        protected const string TextShowState = "Show";
        protected const string TextHideState = "Close";
        private GameObject[] m_items;
        private ButtonEx[] m_itemIconButtons;
        private Image[] m_materialIcons;
        private Text[] m_materialNumTexts;
        private GameObject[] m_materialDecreaseIcons;
        private long[] m_lackCount;
        [AutoBind("./BGFrame", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemBG;
        [AutoBind("./MaterialsItem1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Item1;
        [AutoBind("./MaterialsItem2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Item2;
        [AutoBind("./MaterialsItem3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Item3;
        [AutoBind("./MaterialsItem4", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Item4;
        [AutoBind("./MaterialsItem5", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Item5;
        [AutoBind("./DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeleteButton;
        [AutoBind("./MaterialsItem1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemIconButton1;
        [AutoBind("./MaterialsItem2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemIconButton2;
        [AutoBind("./MaterialsItem3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemIconButton3;
        [AutoBind("./MaterialsItem4/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemIconButton4;
        [AutoBind("./MaterialsItem5/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemIconButton5;
        [AutoBind("./DrawingButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddBlueprintButton;
        [AutoBind("./DrawingButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AddButtoNStateCtrl;
        [AutoBind("./MaterialsItem1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image Material1_Icon;
        [AutoBind("./MaterialsItem2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image Material2_Icon;
        [AutoBind("./MaterialsItem3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image Material3_Icon;
        [AutoBind("./MaterialsItem4/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image Material4_Icon;
        [AutoBind("./MaterialsItem5/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image Material5_Icon;
        [AutoBind("./MaterialsItem1/NumberPanel/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Material1_NumText;
        [AutoBind("./MaterialsItem2/NumberPanel/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Material2_NumText;
        [AutoBind("./MaterialsItem3/NumberPanel/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Material3_NumText;
        [AutoBind("./MaterialsItem4/NumberPanel/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Material4_NumText;
        [AutoBind("./MaterialsItem5/NumberPanel/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Material5_NumText;
        [AutoBind("./MaterialsItem1/NumberPanel/DecreaseImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Material1_DecreaseIcon;
        [AutoBind("./MaterialsItem2/NumberPanel/DecreaseImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Material2_DecreaseIcon;
        [AutoBind("./MaterialsItem3/NumberPanel/DecreaseImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Material3_DecreaseIcon;
        [AutoBind("./MaterialsItem4/NumberPanel/DecreaseImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Material4_DecreaseIcon;
        [AutoBind("./MaterialsItem5/NumberPanel/DecreaseImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Material5_DecreaseIcon;
        [AutoBind("./ItemSImpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSImpleDummy;
        [AutoBind("./EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProductReadyEffect;
        [AutoBind("./ItemSimpleUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleUIPrefabDummy;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController uiStateCtrl;
        [AutoBind("./TextStateCtrl", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController textStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public TweenPos2 TweenPosAnim;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public PrefabResourceContainer ResContainer;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateMaterialCostInfo_1;
        private static DelegateBridge __Hotfix_UpdateMaterialCostInfoByInfoList;
        private static DelegateBridge __Hotfix_UpdateMaterialCostInfo_0;
        private static DelegateBridge __Hotfix_GetFristMaterial;
        private static DelegateBridge __Hotfix_GetFristMaterialPos;
        private static DelegateBridge __Hotfix_SetViewCostInfoList;
        private static DelegateBridge __Hotfix_GetProduceCount;
        private static DelegateBridge __Hotfix_SetCaptainEffectState;
        private static DelegateBridge __Hotfix_SetIconAndText;
        private static DelegateBridge __Hotfix_IsPanelArriveTargetPos;
        private static DelegateBridge __Hotfix_GetSprite;
        private static DelegateBridge __Hotfix_GetMaterialCostNumShowStr;
        private static DelegateBridge __Hotfix_DisableShowMatreialItemCost;
        private static DelegateBridge __Hotfix_EnableShowMatreialItemCost;
        private static DelegateBridge __Hotfix_OnBlueprintButtonClick;
        private static DelegateBridge __Hotfix_OnAddBlueprintButtonClick;
        private static DelegateBridge __Hotfix_OnMaterialIconClick_1;
        private static DelegateBridge __Hotfix_OnMaterialIconClick_2;
        private static DelegateBridge __Hotfix_OnMaterialIconClick_3;
        private static DelegateBridge __Hotfix_OnMaterialIconClick_4;
        private static DelegateBridge __Hotfix_OnMaterialIconClick_5;
        private static DelegateBridge __Hotfix_OnMaterialIconClick;
        private static DelegateBridge __Hotfix_OnDeleteBlueprintButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBlueprintButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBlueprintButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMaterialIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnMaterialIconClick;
        private static DelegateBridge __Hotfix_add_EventOnAddBlueprintButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddBlueprintButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDeleteBlueprintButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDeleteBlueprintButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnAddBlueprintButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBlueprintButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDeleteBlueprintButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int, int, Vector3, long> EventOnMaterialIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void DisableShowMatreialItemCost()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableShowMatreialItemCost(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public CostInfo GetFristMaterial()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetFristMaterialPos()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetMaterialCostNumShowStr(long ownCnt, int needCnt)
        {
        }

        [MethodImpl(0x8000)]
        private int GetProduceCount(int selectBlueprintIdInIdle, int produceBatch)
        {
        }

        [MethodImpl(0x8000)]
        protected Sprite GetSprite(StoreItemType type, int configId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPanelArriveTargetPos()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddBlueprintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBlueprintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDeleteBlueprintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick_1(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick_2(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick_3(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick_4(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick_5(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainEffectState(int itemCount, List<bool> effectList)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetIconAndText(Image img, Text txt, CostInfo costInfo, Dictionary<string, UnityEngine.Object> resDict, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewCostInfoList(List<CostInfo> viewInfoList, List<CostInfo> costInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMaterialCostInfo(int blueprintId, List<CostInfo> orignalCostInfoList, List<CostInfo> costInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMaterialCostInfo(ProduceLineState state, Dictionary<string, UnityEngine.Object> resDict, int selectBlueprintId, List<CostInfo> orignalCostInfoList, List<CostInfo> realCostInfoList, int produceBatch, bool isSelfProduce = true)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMaterialCostInfoByInfoList(int blueprintId, int produceCount, ProduceLineState lineState, List<CostInfo> orignalCostInfoList, List<CostInfo> costInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

