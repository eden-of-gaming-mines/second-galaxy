﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum ShipHangarRepeatableUserGuideType
    {
        Invalid,
        LowSlotSet,
        MiddleSlotSet,
        AmmoSet,
        AddShip,
        SpaceSignalScanSet
    }
}

