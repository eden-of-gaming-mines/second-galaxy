﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DoShareUIController : UIControllerBase
    {
        private Sprite m_sprite;
        private bool m_isInternal = true;
        [AutoBind("./ShareGroup/ShareButtonGroup/QQButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4QQButton;
        [AutoBind("./ShareGroup/ShareButtonGroup/WeiboButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4WeiBoButton;
        [AutoBind("./ShareGroup/ShareButtonGroup/WechatButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4WeChatFriendButton;
        [AutoBind("./ShareGroup/ShareButtonGroup/FaceBookButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4FaceBookButton;
        [AutoBind("./ShareGroup/ShareButtonGroup/TwitterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4TwitterButton;
        [AutoBind("./ShareGroup/ShareButtonGroup/InstagramButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_doShare4InsButton;
        [AutoBind("./ShareGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_colseButton;
        [AutoBind("./ShareGroup/ShareImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_screenShootImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./ShareGroup/ShareButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonGroupStateCtrl;
        private static DelegateBridge __Hotfix_Update4ScreenShoot;
        private static DelegateBridge __Hotfix_Update4Preview;
        private static DelegateBridge __Hotfix_ClearUpData;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public void ClearUpData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void Update4Preview(Texture2D screenTexture, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void Update4ScreenShoot()
        {
        }
    }
}

