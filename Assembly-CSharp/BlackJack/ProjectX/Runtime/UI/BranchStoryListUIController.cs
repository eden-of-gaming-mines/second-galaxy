﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BranchStoryListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick;
        private BranchStoryListCategoryGroupUIController[] m_categoryGroupCtrlList;
        [AutoBind("./Viewport/Content/StoryQuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CategoryGroupItem;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform Content;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateBranchStoryListUI;
        private static DelegateBridge __Hotfix_UpdateQuestList;
        private static DelegateBridge __Hotfix_UpdateRedPoints;
        private static DelegateBridge __Hotfix_ResetListExpandState;
        private static DelegateBridge __Hotfix_OnQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestItemClick;

        public event Action<BranchStoryUITask.BranchStoryItemInfo> EventOnQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CreateBranchStoryListUI(Dictionary<int, List<BranchStoryUITask.BranchStoryItemInfo>> branchStoryDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestItemClick(BranchStoryUITask.BranchStoryItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetListExpandState(BranchStoryUITask.BranchStoryItemInfo selectBranch)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestList(int selectBranchId, Dictionary<int, List<BranchStoryUITask.BranchStoryItemInfo>> branchStoryDict, bool isResetExpandState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRedPoints(int selectBranchId, Dictionary<int, List<BranchStoryUITask.BranchStoryItemInfo>> branchStoryDict)
        {
        }
    }
}

