﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemNavigationUIController : UIControllerBase
    {
        public List<SolarSystemNavigationPointUIController> m_navigationCtrlListBeforeCurrSolarSystem;
        public List<SolarSystemNavigationPointUIController> m_navigationCtrlListAfterCurrSolarSystem;
        public SolarSystemNavigationFinalPointUIController m_lastNavigationCtrl;
        public SolarSystemNavigationPointUIController m_navigationCtrlForCurrSolarSystem;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStopNavigatonButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStarMapButtonClick;
        [AutoBind("./NaviPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_navigationPointRoot;
        [AutoBind("./StopNavigationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_stopNavigationButton;
        [AutoBind("./JumpCountInfo/JumpCountInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_jumoCountInfoText;
        [AutoBind("./NavigationInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_navigationInfoText;
        [AutoBind("./StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_starMapButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetNavigationUIInfo;
        private static DelegateBridge __Hotfix_ShowOrHideNavigationUI;
        private static DelegateBridge __Hotfix_ShowNavigationUIPoints;
        private static DelegateBridge __Hotfix_ShowNavigationUIText;
        private static DelegateBridge __Hotfix_OnStopNavigationButtonClick;
        private static DelegateBridge __Hotfix_OnEnterStarMapButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStopNavigatonButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStopNavigatonButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapButtonClick;

        public event Action EventOnStarMapButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStopNavigatonButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterStarMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStopNavigationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNavigationUIInfo(int currSolarSystemId, List<GDBSolarSystemSimpleInfo> navigationPathList)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNavigationUIPoints(int currSolarSystemId, List<GDBSolarSystemSimpleInfo> navigationPathList)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNavigationUIText(int currSolarSystemId, List<GDBSolarSystemSimpleInfo> navigationPathList)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideNavigationUI(bool isShow)
        {
        }

        [CompilerGenerated]
        private sealed class <SetNavigationUIInfo>c__AnonStorey0
        {
            internal int currSolarSystemId;

            internal bool <>m__0(GDBSolarSystemSimpleInfo item) => 
                (item.Id == this.currSolarSystemId);
        }

        [CompilerGenerated]
        private sealed class <ShowNavigationUIPoints>c__AnonStorey1
        {
            internal int currSolarSystemId;

            internal bool <>m__0(GDBSolarSystemSimpleInfo elem) => 
                (elem.Id == this.currSolarSystemId);
        }

        [CompilerGenerated]
        private sealed class <ShowNavigationUIText>c__AnonStorey2
        {
            internal int currSolarSystemId;

            internal bool <>m__0(GDBSolarSystemSimpleInfo elem) => 
                (elem.Id == this.currSolarSystemId);
        }
    }
}

