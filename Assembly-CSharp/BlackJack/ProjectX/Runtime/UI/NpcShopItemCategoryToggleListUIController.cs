﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopItemCategoryToggleListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopItemCategory> EventOnItemCategoryButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItemTypeToggleSelected;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_toggleScrollView;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_categoryToggleTrans;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup m_categoryTogglesGroup;
        private List<NpcShopItemCategoryToggleUIController> m_itemCategoryToggleList;
        private static DelegateBridge __Hotfix_UpdateaCategoryAndTypeSelectState;
        private static DelegateBridge __Hotfix_CloseAllCategoryTypeToggleItemList;
        private static DelegateBridge __Hotfix_ClearAllTypeToggleSelectState;
        private static DelegateBridge __Hotfix_OpenFirstCategoryToggle;
        private static DelegateBridge __Hotfix_GetNpcShopCategoryController;
        private static DelegateBridge __Hotfix_SwitchCategoryToggleState;
        private static DelegateBridge __Hotfix_CreateAllCategoryToggles;
        private static DelegateBridge __Hotfix_UpdateCateogryItemCount;
        private static DelegateBridge __Hotfix_HideCategoryItemCountInfo;
        private static DelegateBridge __Hotfix_SetMenuItemScrollToViewBottom;
        private static DelegateBridge __Hotfix_OnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnShopItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnItemTypeToggleSelected;

        public event Action<NpcShopItemCategory> EventOnItemCategoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemTypeToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearAllTypeToggleSelectState(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void CloseAllCategoryTypeToggleItemList()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllCategoryToggles(Dictionary<NpcShopItemCategory, List<int>> toggleList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemCategoryToggleUIController GetNpcShopCategoryController(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void HideCategoryItemCountInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCategoryButtonClick(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopItemTypeToggleSelected(int itemType)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenFirstCategoryToggle()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemScrollToViewBottom(NpcShopItemCategoryToggleUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchCategoryToggleState(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateaCategoryAndTypeSelectState(NpcShopItemCategory category, NpcShopItemType type)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCateogryItemCount(Dictionary<NpcShopItemCategory, long> CategoryToCountDict)
        {
        }
    }
}

