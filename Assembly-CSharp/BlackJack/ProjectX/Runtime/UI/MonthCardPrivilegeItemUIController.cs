﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class MonthCardPrivilegeItemUIController : UIControllerBase
    {
        [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemInfoText;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        private static DelegateBridge __Hotfix_UpdatePrivilegeItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePrivilegeItem(MonthlyCardPriviledgeInfo itemInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

