﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ActivityInfoUIController : UIControllerBase
    {
        private const int m_minusForOneHour = 0x3b;
        public ActivityDetailInfoUIController ActivityDetailInfoCtrl;
        public ActivityToggleListUIController ActivityListCtrl;
        public ActivityVitalityPanelUIController ActivityVitalityPanelCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDailyExpExplainButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action> EventOnBackGroundClick;
        [AutoBind("./Explore", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ExplorePanelUIStateCtrl;
        [AutoBind("./Explore", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExplorePanel;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./Explore/DailyText/UpperLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DailyExpUpperLimitText;
        [AutoBind("./Explore/DailyText/UpperLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DailyExpUpperLimitStateCtrl;
        [AutoBind("./Explore/DailyText/UpperLimitText/EveryDayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DailyExpEveryDayText;
        [AutoBind("./Explore/DailyText/ExplainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DailyExpExplainButton;
        [AutoBind("./Explore/DailyText/ExperienceBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DailyExpBarImage;
        [AutoBind("./Explore/DailyText/ExperienceBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DailyExpBarStateCtrl;
        [AutoBind("./Explore/DailyText/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CoolDownTimeText;
        [AutoBind("./TipsGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DailyExpTipsGroup;
        [AutoBind("./TipsGroup/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventCtrl;
        [AutoBind("./TipsGroup/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/ServerTitleText/ServerText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DailyExpServerLevel;
        [AutoBind("./TipsGroup/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/ExperienceTitleText/ExperienceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DailyExpServerAdd;
        [AutoBind("./TipsGroup/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/TodayTitleText/TodayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DailyExpCurTotalExp;
        [AutoBind("./TipsGroup/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DailyExpCommonUIStateController;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_GetExplorePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateDailyExp;
        private static DelegateBridge __Hotfix_UpdateRemainRefeshTime;
        private static DelegateBridge __Hotfix_UpdateDailyExpTips;
        private static DelegateBridge __Hotfix_OnDailyExpExplainButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_add_EventOnDailyExpExplainButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDailyExpExplainButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackGroundClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackGroundClick;

        public event Action<Action> EventOnBackGroundClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDailyExpExplainButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetExplorePanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDailyExpExplainButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDailyExp(long curExp, long maxExp, bool hasServerLevelFactor, bool hasAfkval)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDailyExpTips(int serverLv, float addExp, long restExp, int restMaxExp)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRemainRefeshTime(TimeSpan remainTime)
        {
        }

        [CompilerGenerated]
        private sealed class <OnBackGroundClick>c__AnonStorey0
        {
            internal Action<PointerEventData> passAction;
            internal PointerEventData eventData;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

