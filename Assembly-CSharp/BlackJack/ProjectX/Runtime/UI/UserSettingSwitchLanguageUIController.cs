﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class UserSettingSwitchLanguageUIController : UIControllerBase
    {
        protected Dictionary<int, UserSettingSwitchLanguageItemUIController> m_itemCtrls;
        private LanguageType m_currentSelectLanguageType;
        private LanguageType m_currentRealLanguageType;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LanguageType> EventOnSwitchLanguageItemClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_switchLanguageGroup;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_content;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup m_toggleGroup;
        [AutoBind("./Buttons/UseButtonGroup/UseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageAplayButton;
        [AutoBind("./Buttons/UseButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_languageAplayButtonState;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageCancelButton;
        [AutoBind("./Template/LanguageItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_languageItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnLanguageGroupCloseButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageGroupAplayButtonClick;
        private static DelegateBridge __Hotfix_SetSwitchLanguagePanelShowOrHide;
        private static DelegateBridge __Hotfix_UpdateContentInfo;
        private static DelegateBridge __Hotfix_OnLanguageItemClick;
        private static DelegateBridge __Hotfix_SetAplayButtonUIState;
        private static DelegateBridge __Hotfix_add_EventOnSwitchLanguageItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSwitchLanguageItemClick;

        public event Action<LanguageType> EventOnSwitchLanguageItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupAplayButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupCloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageItemClick(LanguageType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAplayButtonUIState(bool isGray)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSwitchLanguagePanelShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateContentInfo(LanguageType type)
        {
        }
    }
}

