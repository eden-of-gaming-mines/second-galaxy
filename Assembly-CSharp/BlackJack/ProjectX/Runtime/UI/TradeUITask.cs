﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class TradeUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private int m_curSelectedItemType;
        private LogicBlockTradeClient m_lbTradeClient;
        private LogicBlockGuildClient m_lbGuildTradeClient;
        private TradeUIController m_tradeUIController;
        private TradeItemMenuTreeRootUIController m_tradeMenuTree;
        private bool m_isDropdownExpand;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        public const string ParamKey_Refresh = "Refresh";
        public const string TradeUITaskMode_Personal = "TradeUITaskMode_Personal";
        public const string TradeUITaskMode_Guild = "TradeUITaskMode_Guild";
        public const string TaskName = "TradeUITask";
        private Dictionary<int, TradePortJumNumInfo> m_nearestTradePortDic;
        private bool? m_isTradeItemStoreSuccessPrepareEnd;
        private bool? m_isGuildOccupyPrepareEnd;
        private bool? m_isGuildCurrencyPrepareEnd;
        private bool m_isGuildTradeInited;
        private readonly List<ConfigDataNormalItemInfo> m_guildTradeItemConfigList;
        private readonly List<TradeItemTypeMenuItemData> m_guildTradeItemTypeMenuItemDataList;
        private readonly Dictionary<int, List<TradeItemMenuItemData>> m_guildTradeItemMenuItemDataDic;
        private readonly List<GuildTradePurchaseInfo> m_guildTradePurchaseInfoList;
        private readonly Dictionary<int, TradeFilterInfo> m_dropDownInfoDict;
        private int m_curGuildTradeItemId;
        private int m_curGuildTradeDropdownIndex;
        private TradeUITaskTabType m_curGuildTradeTabType;
        private GuildTradePortInfoUITask m_guildTradePortInfoUITask;
        private GuildTradeOrderEditUITask.GuildTradeOrderInfo m_editOrderInfo;
        private const string CurSelectItemKey_G = "GuildTradeCurItemKey";
        private const string CurSelectTab_G = "GuildTradeCurTabKey";
        private const string CurSelectDropdownIndexKey_G = "GuildTradeCurDropdownIndexKey";
        private const string CurSelectDropdownIsUpKey_G = "GuildTradeCurDropdownIsUpKey";
        private DateTime m_resetTimeRefreshReqCooldownTime;
        private DateTime m_illegalNextRefreshTime;
        private readonly List<TradeItemTypeMenuItemData> m_personalTradeItemTypeMenuItemDataList;
        private readonly List<TradeItemTypeMenuItemData> m_personalIlLegalTradeItemTypeMenuItemDataList;
        private readonly Dictionary<int, List<TradeItemMenuItemData>> m_personTradeItemMenuItemDataDic;
        private bool m_isPersonalTradeInited;
        private readonly List<TradeListItemInfo> m_tradeListItemPurchaseInfo;
        private readonly List<TradeListItemInfo> m_tradeListItemIlLegalInfo;
        private uint m_curPurchaseInfoVersion;
        private readonly Dictionary<NormalItemType, List<ConfigDataNormalItemInfo>> m_personalTradeItemConfigDics;
        private readonly Dictionary<int, List<TradeListItemSellInfo>> m_personTradeItemSellerDics;
        private TradeUITaskTabType m_curPersonalTradeTabType;
        private int m_curPersonalTradeItemId;
        private int m_curIlLegalItemId;
        private int m_curPersonalTradeDropdownIndex;
        private const string CurSelectItemKey_P = "PersonalTradeCurItemKey";
        private const string CurSelectIllegalItemKey_P = "PersonalTradeCurIllegalItemKey";
        private const string CurSelectTab_P = "PersonalTradeCurTabKey";
        private const string CurSelectDropdownIndexKey_P = "PersonalTradeCurDropdownIndexKey";
        private const string CurSelectDropdownIsUpKey_P = "PersonalTradeCurDropdownIsUpKey";
        private bool m_isRepeatableUserGuide;
        private const string ParamKeyIsRepatebleUserGuide = "IsRepatebleUserGuide";
        [CompilerGenerated]
        private static Func<GuildTradePortExtraInfo, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTradeUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearAllContextAndRes;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsSameSubPanelStateSwtich;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnTradeItemClick;
        private static DelegateBridge __Hotfix_OnTradeTypeItemClick;
        private static DelegateBridge __Hotfix_OnRegionButtonClick;
        private static DelegateBridge __Hotfix_OnTabChanged;
        private static DelegateBridge __Hotfix_OnDropdownBtnClick;
        private static DelegateBridge __Hotfix_OnDropdownItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnAddTradeMoneyBtnClick;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_RecordInnerData;
        private static DelegateBridge __Hotfix_IniteDataFromIntent;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_HideTradeUIPanel;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_LeaveCurrBackground;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume_GuildTrade;
        private static DelegateBridge __Hotfix_OnStop_GuildTrade;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache_GuildTrade;
        private static DelegateBridge __Hotfix_UpdateDataCache_GuildTrade;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes_GuildTrade;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_GuildTrade;
        private static DelegateBridge __Hotfix_UpdateView_GuildTrade;
        private static DelegateBridge __Hotfix_InitDataFromIntent_GuildTrade;
        private static DelegateBridge __Hotfix_TryRecoverEditOrderData;
        private static DelegateBridge __Hotfix_OnTransportButtonClick;
        private static DelegateBridge __Hotfix_OnTradeItemClick_GuildTrade;
        private static DelegateBridge __Hotfix_OnTabChanged_GuildTrade;
        private static DelegateBridge __Hotfix_OnDropdownChanged_GuildTrade;
        private static DelegateBridge __Hotfix_SendGuildCurrencyInfoReq;
        private static DelegateBridge __Hotfix_SendGuildOccupyiedInfoReq;
        private static DelegateBridge __Hotfix_GetItemDataFromID_GuildTrade;
        private static DelegateBridge __Hotfix_UpdateTradeItemState_GuildTrade;
        private static DelegateBridge __Hotfix_SortGuildTradeListItem;
        private static DelegateBridge __Hotfix_GetGuildTradePurchaseInfoBasicSorter;
        private static DelegateBridge __Hotfix_BuildGuildTradeDropDownMenuList;
        private static DelegateBridge __Hotfix_OpenGuildTradePortInfoUITask;
        private static DelegateBridge __Hotfix_CloseGuildTradePortInfoUITask;
        private static DelegateBridge __Hotfix_RegisterTradePortEvents;
        private static DelegateBridge __Hotfix_UnregisterTradePortEvents;
        private static DelegateBridge __Hotfix_OnOpenStarmapToTradeCenter;
        private static DelegateBridge __Hotfix_OnGuildTradePortNotFound;
        private static DelegateBridge __Hotfix_SendGuildTradePurchaseOrderListInfoReq;
        private static DelegateBridge __Hotfix_SendGuildTradeStoreItemInfoReq;
        private static DelegateBridge __Hotfix_OnPrepareStepEnd;
        private static DelegateBridge __Hotfix_OnAllPrepareEnd;
        private static DelegateBridge __Hotfix_IsNeedSendGuildTradePurchaseOrderListInfoReq;
        private static DelegateBridge __Hotfix_InitGuildTradeItemInfoConfigList;
        private static DelegateBridge __Hotfix_GetNearestTradePortInfo;
        private static DelegateBridge __Hotfix_GetTheFirstGuildTradeItemId;
        private static DelegateBridge __Hotfix_InitGuildTradeMenuTreeDate;
        private static DelegateBridge __Hotfix_SetGuildSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_RecordInnerData_GuildTrade;
        private static DelegateBridge __Hotfix_TryRecoverContextFromRecord_Guild;
        private static DelegateBridge __Hotfix_OnStop_Personal;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes_PersonalTrade;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_PersonalTrade;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache_PersonalTrade;
        private static DelegateBridge __Hotfix_UpdateDataCache_PersonTrade;
        private static DelegateBridge __Hotfix_UpdateView_PersonalTrade;
        private static DelegateBridge __Hotfix_OnTradeItemClick_PersonTrade;
        private static DelegateBridge __Hotfix_OnTabChanged_PersonalTrade;
        private static DelegateBridge __Hotfix_OnDropdownChanged_PersonalTrade;
        private static DelegateBridge __Hotfix_OnGoButtonClick;
        private static DelegateBridge __Hotfix_OnExchangeShopButtonClick;
        private static DelegateBridge __Hotfix_OnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickForIllegalRefresh;
        private static DelegateBridge __Hotfix_CheckItemIdIsSame;
        private static DelegateBridge __Hotfix_CheckFactionCreditValid;
        private static DelegateBridge __Hotfix_IsNeedToSendTradeListInfoReq;
        private static DelegateBridge __Hotfix_SendTradeListInfoReq;
        private static DelegateBridge __Hotfix_BuildPersonalTradeDropDownMenuDict;
        private static DelegateBridge __Hotfix_InitDataFromIntent_Personal;
        private static DelegateBridge __Hotfix_UpdateTradeListItemPurchaseInfo;
        private static DelegateBridge __Hotfix_SetPersonalSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_UpdateTradeItemState_PersonalTrade;
        private static DelegateBridge __Hotfix_OnTradeTypeItemClick_Personal;
        private static DelegateBridge __Hotfix_UpdateIlLegalItemInfo;
        private static DelegateBridge __Hotfix_SortPersonalTradeListItem;
        private static DelegateBridge __Hotfix_IsNpcShopItemListInfoContain;
        private static DelegateBridge __Hotfix_GetPersonalTradeItemSellers;
        private static DelegateBridge __Hotfix_InitPersonalTradeItemSellersDic;
        private static DelegateBridge __Hotfix_GetTheFirstPersonalTradeItemId;
        private static DelegateBridge __Hotfix_GetFirstIlLegalItemId;
        private static DelegateBridge __Hotfix_RecordInnerData_Personal;
        private static DelegateBridge __Hotfix_TryRecoverContextFromRecord_Personal;
        private static DelegateBridge __Hotfix_GetItemDataFromID_Personal;
        private static DelegateBridge __Hotfix_InitPersonTradeItemMenuTreeData;
        private static DelegateBridge __Hotfix_InitRepatableUserGuideData;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_StopRepeatableUserGuide;
        private static DelegateBridge __Hotfix_GetFristTradeItemPath;

        [MethodImpl(0x8000)]
        public TradeUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void BuildGuildTradeDropDownMenuList(TradeUITaskTabType curType)
        {
        }

        [MethodImpl(0x8000)]
        private void BuildPersonalTradeDropDownMenuDict(TradeUITaskTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckFactionCreditValid(int itemID)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckItemIdIsSame(int itemID)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearAllContextAndRes()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseGuildTradePortInfoUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectAllDynamicResForLoad_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectAllDynamicResForLoad_PersonalTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(TradeUITaskUpdateMask state)
        {
        }

        [MethodImpl(0x8000)]
        private int GetFirstIlLegalItemId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetFristTradeItemPath()
        {
        }

        [MethodImpl(0x8000)]
        public Func<GuildTradePurchaseInfo, GuildTradePurchaseInfo, int> GetGuildTradePurchaseInfoBasicSorter(TradeItemSortType sortType, bool isUp)
        {
        }

        [MethodImpl(0x8000)]
        private TradeItemMenuItemData GetItemDataFromID_GuildTrade(int id)
        {
        }

        [MethodImpl(0x8000)]
        private TradeItemMenuItemData GetItemDataFromID_Personal(int id)
        {
        }

        [MethodImpl(0x8000)]
        private TradePortJumNumInfo GetNearestTradePortInfo(int targetSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private List<TradeListItemSellInfo> GetPersonalTradeItemSellers(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        private int GetTheFirstGuildTradeItemId()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTheFirstPersonalTradeItemId()
        {
        }

        [MethodImpl(0x8000)]
        private void HideTradeUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent_GuildTrade(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public void InitDataFromIntent_Personal(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void IniteDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitGuildTradeItemInfoConfigList()
        {
        }

        [MethodImpl(0x8000)]
        private void InitGuildTradeMenuTreeDate()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPersonalTradeItemSellersDic()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPersonTradeItemMenuTreeData()
        {
        }

        [MethodImpl(0x8000)]
        private void InitRepatableUserGuideData(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedLoadDynamicRes_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedLoadDynamicRes_PersonalTrade()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedSendGuildTradePurchaseOrderListInfoReq(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedToSendTradeListInfoReq(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedUpdateDataCache_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedUpdateDataCache_PersonalTrade()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNpcShopItemListInfoContain(ConfigDataNpcShopItemListInfo saleList, int tradeItemId, out ConfigDataNpcShopItemInfo correspondingNpcShopItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(TradeUITaskUpdateMask state)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSameSubPanelStateSwtich()
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddTradeMoneyBtnClick(GuildTradeOrderEditUITask.GuildTradeOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private bool OnAllPrepareEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownChanged_GuildTrade(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownChanged_PersonalTrade(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnExchangeShopButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoButtonClick(int solarSystemId, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradePortNotFound()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenShopButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenStarmapToTradeCenter(GuildTradeTransportInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareStepEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRegionButtonClick(int solarSystemID)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStop_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStop_Personal()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabChanged(TradeUITaskTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabChanged_GuildTrade(TradeUITaskTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTabChanged_PersonalTrade(TradeUITaskTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeItemClick(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeItemClick_GuildTrade(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeItemClick_PersonTrade(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeTypeItemClick(int typeID, bool expand)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTradeTypeItemClick_Personal(int typeID, bool expand)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransportButtonClick(GuildTradePurchaseInfo guildTradePurchaseInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenGuildTradePortInfoUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareForStartOrResume_GuildTrade(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RecordInnerData()
        {
        }

        [MethodImpl(0x8000)]
        private void RecordInnerData_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void RecordInnerData_Personal()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterTradePortEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildCurrencyInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildOccupyiedInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildTradePurchaseOrderListInfoReq(int tradeItemId, Action<bool, int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildTradeStoreItemInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendTradeListInfoReq(bool isLegal, int tradeItemId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPersonalSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void SortGuildTradeListItem()
        {
        }

        [MethodImpl(0x8000)]
        private void SortPersonalTradeListItem()
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTradeUITask(UIIntent returnToIntent, string mode, Action<bool> onEnd = null, bool isInRepeatableGuide = false, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForIllegalRefresh()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRecoverContextFromRecord_Guild()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRecoverContextFromRecord_Personal()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRecoverEditOrderData()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterTradePortEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_PersonTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateIlLegalItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeItemState_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeItemState_PersonalTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeListItemPurchaseInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_GuildTrade()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_PersonalTrade()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildTradePurchaseInfoBasicSorter>c__AnonStorey7
        {
            internal bool isUp;
            internal TradeUITask $this;

            internal int <>m__0(GuildTradePurchaseInfo item1, GuildTradePurchaseInfo item2) => 
                (!this.isUp ? ((int) (item2.m_price - item1.m_price)) : ((int) (item1.m_price - item2.m_price)));

            internal int <>m__1(GuildTradePurchaseInfo item1, GuildTradePurchaseInfo item2)
            {
                int jumpNum = this.$this.m_nearestTradePortDic[item1.m_solarSystemId].JumpNum;
                int num2 = this.$this.m_nearestTradePortDic[item2.m_solarSystemId].JumpNum;
                return (!this.isUp ? (num2 - jumpNum) : (jumpNum - num2));
            }

            internal int <>m__2(GuildTradePurchaseInfo item1, GuildTradePurchaseInfo item2) => 
                (!this.isUp ? (item2.m_count - item1.m_count) : (item1.m_count - item2.m_count));
        }

        [CompilerGenerated]
        private sealed class <GetItemDataFromID_GuildTrade>c__AnonStorey6
        {
            internal int id;

            internal bool <>m__0(TradeItemMenuItemData item) => 
                (item.m_itemId == this.id);
        }

        [CompilerGenerated]
        private sealed class <GetItemDataFromID_Personal>c__AnonStoreyD
        {
            internal int id;

            internal bool <>m__0(TradeItemMenuItemData itemData) => 
                (itemData.m_itemId == this.id);
        }

        [CompilerGenerated]
        private sealed class <OnAddTradeMoneyBtnClick>c__AnonStorey0
        {
            internal GuildTradeOrderEditUITask.GuildTradeOrderInfo info;
            internal TradeUITask $this;

            internal void <>m__0(bool ret)
            {
                if (ret)
                {
                    this.$this.m_editOrderInfo = this.info;
                    this.$this.CloseGuildTradePortInfoUITask();
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnTabChanged_PersonalTrade>c__AnonStoreyB
        {
            internal TradeUITaskTabType tabType;
            internal TradeUITask $this;

            internal void <>m__0(bool res)
            {
                if ((this.$this.State == Task.TaskState.Running) && res)
                {
                    this.$this.m_curPersonalTradeTabType = this.tabType;
                    this.$this.m_curSelectedItemType = this.$this.GetItemDataFromID_Personal(this.$this.m_curPersonalTradeItemId).m_typeId;
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.UpdateTradeList);
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.ExpandDropdown);
                    this.$this.m_curPersonalTradeDropdownIndex = 0;
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }

            internal void <>m__1(bool res)
            {
                if ((this.$this.State == Task.TaskState.Running) && res)
                {
                    this.$this.m_curPersonalTradeTabType = this.tabType;
                    this.$this.m_curSelectedItemType = this.$this.GetItemDataFromID_Personal(this.$this.m_curIlLegalItemId).m_typeId;
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.ExpandDropdown);
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.UpdateMenuTree);
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.UpdateTradeList);
                    this.$this.m_curPersonalTradeDropdownIndex = 0;
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnTradeItemClick_GuildTrade>c__AnonStorey3
        {
            internal int tradeItemId;
            internal TradeUITask $this;

            internal void <>m__0(bool res, int errCode)
            {
                if ((this.$this.State == Task.TaskState.Running) && res)
                {
                    if (errCode == -2916)
                    {
                        MsgBoxUITask.ShowMsgBox(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildTradeNoGuildTradePortOffline, new object[0]), false, () => this.$this.OnCloseButtonClick(null));
                    }
                    else if (errCode != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(errCode, true, false);
                    }
                    else
                    {
                        this.$this.m_curGuildTradeItemId = this.tradeItemId;
                        this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.UpdateTradeList);
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    }
                }
            }

            internal void <>m__1()
            {
                this.$this.OnCloseButtonClick(null);
            }

            internal void <>m__2()
            {
                this.$this.OnCloseButtonClick(null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnTradeItemClick_PersonTrade>c__AnonStoreyA
        {
            internal int tradeItemId;
            internal TradeUITask $this;

            internal void <>m__0(bool res)
            {
                if ((this.$this.State == Task.TaskState.Running) && res)
                {
                    this.$this.m_curPersonalTradeItemId = this.tradeItemId;
                    this.$this.m_curSelectedItemType = this.$this.GetItemDataFromID_Personal(this.$this.m_curPersonalTradeItemId).m_typeId;
                    this.$this.EnablePipelineStateMask(TradeUITask.TradeUITaskUpdateMask.UpdateTradeList);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnTransportButtonClick>c__AnonStorey2
        {
            internal GuildTradePurchaseInfo guildTradePurchaseInfo;

            internal int <>m__0(GuildTradePortExtraInfo item1, GuildTradePortExtraInfo item2) => 
                (TradeUIHelper.GetJumpNum(item1.m_solarSystemId, this.guildTradePurchaseInfo.m_solarSystemId) - TradeUIHelper.GetJumpNum(item2.m_solarSystemId, this.guildTradePurchaseInfo.m_solarSystemId));
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume_GuildTrade>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal TradeUITask $this;

            internal void <>m__0(bool result)
            {
                this.$this.m_isTradeItemStoreSuccessPrepareEnd = new bool?(result);
                this.$this.OnPrepareStepEnd(this.onPrepareEnd);
            }

            internal void <>m__1(bool result)
            {
                this.$this.m_isGuildOccupyPrepareEnd = new bool?(result);
                this.$this.OnPrepareStepEnd(this.onPrepareEnd);
            }

            internal void <>m__2(bool result)
            {
                this.$this.m_isGuildCurrencyPrepareEnd = new bool?(result);
                this.$this.OnPrepareStepEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildCurrencyInfoReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildCurrencyInfoReqNetTask task2 = task as GuildCurrencyInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        this.onEnd(false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildOccupyiedInfoReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task2 = task as GuildOccupiedSolarSystemInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildTradePurchaseOrderListInfoReq>c__AnonStorey8
        {
            internal Action<bool, int> onEnd;

            internal void <>m__0(Task task)
            {
                GetGuildTradePurchaseOrderListInfoNetTask task2 = (GetGuildTradePurchaseOrderListInfoNetTask) task;
                if (task2.IsNetworkError)
                {
                    this.onEnd(false, task2.Result);
                }
                else
                {
                    this.onEnd(true, task2.Result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildTradeStoreItemInfoReq>c__AnonStorey9
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildItemStoreInfoReqNetTask task2 = (GuildItemStoreInfoReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    this.onEnd(false);
                }
                else
                {
                    int result = task2.m_result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendTradeListInfoReq>c__AnonStoreyC
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GetTradeListInfoReq req = (GetTradeListInfoReq) task;
                if (!req.IsNetworkError)
                {
                    int result = req.Result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        public sealed class TradeFilterInfo
        {
            public int Index;
            public TradeItemSortType SortType;
            public string ShowText;
            public bool IsUp;
            private static DelegateBridge _c__Hotfix_ctor;

            public TradeFilterInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public sealed class TradePortJumNumInfo
        {
            public int SolarSystemID;
            public int JumpNum;
            private static DelegateBridge _c__Hotfix_ctor;

            public TradePortJumNumInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        private enum TradeUITaskUpdateMask
        {
            IsUITaskModeChanged,
            UpdateItemState,
            ExpandTypeMenu,
            UpdateTradeList,
            UpdateMenuTree,
            UpdateSingleDropdownItem,
            UpdateAllDropDownItem,
            ExpandDropdown,
            HideDropdownBtn,
            OpenGuildPortInfo,
            CloseGuildPortInfo,
            TabChanged
        }
    }
}

