﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class SpecialGiftPackageNormalItemUIController : UIControllerBase
    {
        private const string ContentItemName = "CommonItemIcon";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnCommonItemIconClick;
        private DateTime m_nextUpdateTime;
        private DateTime m_nextUpdateDayTime;
        private LBRechargeGiftPackage m_lbGiftPackage;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        [AutoBind("./DiscountGroup/OffText", AutoBindAttribute.InitState.NotInit, false)]
        private Text DescText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./SimpleInfoDummyLeft", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SimpleInfoDummyLeft;
        [AutoBind("./SimpleInfoDummyCenter", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SimpleInfoDummyCenter;
        [AutoBind("./SimpleInfoDummyRight", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SimpleInfoDummyRight;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GridLayoutGroup LayoutGroup;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect m_scrollViewRect;
        [AutoBind("./TimeLeftLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeLeftLimitRoot;
        [AutoBind("./FeatureInfoItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FeatureInfoItemRoot;
        [AutoBind("./BuyButton/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonPriceText;
        [AutoBind("./BuyButton/IocnGroup/IconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonCurrencyText;
        [AutoBind("./BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./TotalValueGroup/TotalValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalValueText;
        [AutoBind("./DiscountGroup/DiscountNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DiscountNumberText;
        [AutoBind("./BuyCountLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyCountLimitText;
        [AutoBind("./TimeLeftLimit/TimeLeftLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText TimeLeftLimitText;
        [AutoBind("./TileText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TileText;
        [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        [CompilerGenerated]
        private static Func<QuestRewardInfo, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Update4GiftPackageNormalItem;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnReturnToPool;
        private static DelegateBridge __Hotfix_GetSimpleInfoIconPosition;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_IsNeedUpdateLeftTime;
        private static DelegateBridge __Hotfix_OnContentInfoFill;
        private static DelegateBridge __Hotfix_OnCommonItemIconClick;
        private static DelegateBridge __Hotfix_UpdatePackageContent;
        private static DelegateBridge __Hotfix_GetRewardInfoByIndex;
        private static DelegateBridge __Hotfix_InitPoolAndScrollView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_add_EventOnCommonItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonItemIconClick;

        public event Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnCommonItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private QuestRewardInfo GetRewardInfoByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimpleInfoIconPosition(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void InitPoolAndScrollView()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedUpdateLeftTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnContentInfoFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnReturnToPool()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void Update4GiftPackageNormalItem(LBRechargeGiftPackage giftPackage, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePackageContent(List<QuestRewardInfo> contentList)
        {
        }
    }
}

