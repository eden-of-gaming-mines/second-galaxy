﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class UserSettingUIController : UIControllerBase
    {
        public UserSettingSwitchLanguageUIController m_userSettingSwitchLanguageUIController;
        public GiftCodeUIController m_giftCodeCtrl;
        public UploadClientLogUIController m_uploadClientLogUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_titleButton;
        [AutoBind("./ToggleGroup/CommonSetting", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_commonSetting;
        [AutoBind("./ToggleGroup/DisplaySetting", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_displaySetting;
        [AutoBind("./CommonGroup/GameImage/TroopGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_troopOpenToggle;
        [AutoBind("./CommonGroup/GameImage/TroopGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_troopOffToggle;
        [AutoBind("./CommonGroup/GameImage/MusicGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_musicOpenToggle;
        [AutoBind("./CommonGroup/GameImage/MusicGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_musicOffToggle;
        [AutoBind("./CommonGroup/GameImage/SoundEffectGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_soundEffectOpenToggle;
        [AutoBind("./CommonGroup/GameImage/SoundEffectGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_soundEffectOffToggle;
        [AutoBind("./CommonGroup/GameImage/IllegaAttackGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_illegaAttackOpenToggle;
        [AutoBind("./CommonGroup/GameImage/IllegaAttackGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_illegaAttackOffToggle;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/LanguageButtonGroup/LanguageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageButton;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/QuitButtonGroup/QuitButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_quitButton;
        [AutoBind("./CommonGroup/OtherImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GMButton;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/ServiceCenterButtonGroup/ServiceCenterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_helpButton;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/AdministrationButtonGroup/AdministrationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_administrationButton;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/LogButtonGroup/LogButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_logButtonGroup;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/ServeButtonGroup/ServeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_serveButton;
        [AutoBind("./CommonGroup/OtherImage/ContentImage/PrivacyButtonGroup/PrivacyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_privacyButton;
        [AutoBind("./DisplayGroup/FrameRateGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_frameRateOpenToggle;
        [AutoBind("./DisplayGroup/FrameRateGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_frameRateOffToggle;
        [AutoBind("./DisplayGroup/ShipGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_shipOpenToggle;
        [AutoBind("./DisplayGroup/ShipGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_shipOffToggle;
        [AutoBind("./DisplayGroup/SpecialEffectsGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_specialEffectsOpenToggle;
        [AutoBind("./DisplayGroup/SpecialEffectsGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_specialEffectsOffToggle;
        [AutoBind("./DisplayGroup/NumberGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_damageNumberOpenToggle;
        [AutoBind("./DisplayGroup/NumberGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_damageNumberOffToggle;
        [AutoBind("./DisplayGroup/GraphicQuality/ToggleGroup/GraphicQualityLow", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicQualityLowToggle;
        [AutoBind("./DisplayGroup/GraphicQuality/ToggleGroup/GraphicQualityMedium", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicQualityMediumToggle;
        [AutoBind("./DisplayGroup/GraphicQuality/ToggleGroup/GraphicQualityHigh", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicQualityHighToggle;
        [AutoBind("./DisplayGroup/EffectQuality/ToggleGroup/EffectQualityLow", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicEffectLowToggle;
        [AutoBind("./DisplayGroup/EffectQuality/ToggleGroup/EffectQualityMedium", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicEffectMediumToggle;
        [AutoBind("./DisplayGroup/EffectQuality/ToggleGroup/EffectQualityHigh", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_graphicEffectHighToggle;
        [AutoBind("./DisplayGroup/ExplainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_explainButton;
        [AutoBind("./DisplayGroup/HintGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_hintGroup;
        [AutoBind("./DisplayGroup/HintGroup/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hintGroupCloseButton;
        [AutoBind("./SwitchLanguagePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_switchLanguageGroup;
        [AutoBind("./LanguageConfirmBox", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_languageConfirmBoxUIState;
        [AutoBind("./LanguageConfirmBox/Buttons/UseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageConfirmBoxReloginButton;
        [AutoBind("./LanguageConfirmBox/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageConfirmBoxCancelButton;
        [AutoBind("./GetGiftBox", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_giftCDKeyObj;
        [AutoBind("./CommonGroup/GameImage/ReplaykitGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_recorderOn;
        [AutoBind("./CommonGroup/GameImage/ReplaykitGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_recorderOff;
        [AutoBind("./CommonGroup/GameImage/ReplaykitGroup/ToggleGroup/Warning", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_recordWarning;
        [AutoBind("/OperationGroup/FrameRateGroup/ToggleGroup/OpenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_lock4AutoSurroundOn;
        [AutoBind("/OperationGroup/FrameRateGroup/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_lock4AutoSurroundOff;
        [AutoBind("./CommonGroup/OtherImage/ContentImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stat;
        [AutoBind("./FeedbackBox", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_uploadClientLogGameObj;
        private const string StateNormal = "Normal";
        private const string StateReview = "IOSReview";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetSettingButtonsStateByPackageConfig;
        private static DelegateBridge __Hotfix_UpdateUserSettingPanel;
        private static DelegateBridge __Hotfix_SetLanguageSwitchPanelShowOrHide;
        private static DelegateBridge __Hotfix_SetHintGroupShowOrHide;
        private static DelegateBridge __Hotfix_GetUIProcess;
        private static DelegateBridge __Hotfix_UpdateLanguageSelectedPanel;
        private static DelegateBridge __Hotfix_SetSwitchLanguagePanelShowOrHide;
        private static DelegateBridge __Hotfix_SetScreenRecordOn;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetHintGroupShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLanguageSwitchPanelShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScreenRecordOn(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetSettingButtonsStateByPackageConfig()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSwitchLanguagePanelShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLanguageSelectedPanel(LanguageType currentLanguageType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUserSettingPanel()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

