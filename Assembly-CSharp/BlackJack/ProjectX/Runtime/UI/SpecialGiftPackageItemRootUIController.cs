﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class SpecialGiftPackageItemRootUIController : UIControllerBase, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGiftPackageItemBuyButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnCommonItemIconClick;
        private EasyObjectPool m_easyPool;
        private SpecialGiftPackageShipItemUIController m_giftPackageShipItemCtrl;
        private SpecialGiftPackageNormalItemUIController m_giftPackageNormalItemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateSpecialGiftPackageItem;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_Update4GiftPackageNormalItem;
        private static DelegateBridge __Hotfix_Update4GiftPackageShipItem;
        private static DelegateBridge __Hotfix_OnGiftPackageItemBuyButtonClicked;
        private static DelegateBridge __Hotfix_OnRewardItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonItemIconClick;

        public event Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnCommonItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGiftPackageItemBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemBuyButtonClicked(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemIconClick(LBRechargeGiftPackage giftPackage, ILBStoreItemClient rewardInfo, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void Update4GiftPackageNormalItem(LBRechargeGiftPackage giftPackage, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void Update4GiftPackageShipItem(LBRechargeGiftPackage giftPackage, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpecialGiftPackageItem(LBRechargeGiftPackage giftPackage, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

