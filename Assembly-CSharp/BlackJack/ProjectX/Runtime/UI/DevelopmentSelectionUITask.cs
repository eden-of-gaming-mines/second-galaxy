﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class DevelopmentSelectionUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<object, object> EventOnRequestOpenTransformationPanel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, SystemFuncDescType> EventOnRequestSetSystemDescriptionButtonState;
        public const string TaskName = "DevelopmentSelectionUITask";
        public const string ParamKeyResetTask = "ParamKeyResetTask";
        private const string ModeDefineProjectPanel = "ProjectPanel";
        private const string ModeDefineDetailPanel = "DetailPanel";
        private const string PanelStateShow = "Show";
        private const string PanelStateClose = "Close";
        private readonly ConfigDataDevelopmentNodeDescInfo[] m_nodeInfos;
        private DevelopmentSelectionUIController m_developmentSelectionUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowOrHideSelectionPanel;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnProjectButtonClick;
        private static DelegateBridge __Hotfix_OnProjectChangeButtonClick;
        private static DelegateBridge __Hotfix_OnDetailPanelNodeClick;
        private static DelegateBridge __Hotfix_OnApplicationButtonClick;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_SetDetailPanelUpdatePipeLineStateMask;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_SetNodeInfoWithViewNotChange;
        private static DelegateBridge __Hotfix_SetDefaultNodeInfo;
        private static DelegateBridge __Hotfix_ClearNodeInfos;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_add_EventOnRequestOpenTransformationPanel;
        private static DelegateBridge __Hotfix_remove_EventOnRequestOpenTransformationPanel;
        private static DelegateBridge __Hotfix_add_EventOnRequestSetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_get_DevelopmentNodeInfos;
        private static DelegateBridge __Hotfix_get_DevelopmentQuaternaryNodeInfos;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnChipProjectButtonClick_UseGuide;
        private static DelegateBridge __Hotfix_GetItemUITypeByGrandFaction_UserGuide;
        private static DelegateBridge __Hotfix_DevelopmentTertiaryNodeUIControllerClick_UserGuide;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetItemRectTransByGrandFaction;
        private static DelegateBridge __Hotfix_SelectItemByGrandFaction_UserGuide;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;

        public event Action<object, object> EventOnRequestOpenTransformationPanel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, SystemFuncDescType> EventOnRequestSetSystemDescriptionButtonState
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public DevelopmentSelectionUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearNodeInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public void DevelopmentTertiaryNodeUIControllerClick_UserGuide(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetItemRectTransByGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        private int GetItemUITypeByGrandFaction_UserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetProcess(string mode, string stateName, bool immediateComplate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams(UIIntentCustom intentCustom)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationButtonClick(ConfigDataDevelopmentProjectInfo applicationInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnChipProjectButtonClick_UseGuide(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailPanelNodeClick(ConfigDataDevelopmentNodeDescInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProjectButtonClick(DevelopmentNodeType newType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProjectChangeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public string SelectItemByGrandFaction_UserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void SetDefaultNodeInfo(int nodeIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDetailPanelUpdatePipeLineStateMask()
        {
        }

        [MethodImpl(0x8000)]
        private bool SetNodeInfoWithViewNotChange(int nodeIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSelectionPanel(bool isShow, bool immediateComplete, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private Dictionary<int, ConfigDataDevelopmentNodeDescInfo> DevelopmentNodeInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private Dictionary<int, ConfigDataDevelopmentProjectInfo> DevelopmentQuaternaryNodeInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnApplicationButtonClick>c__AnonStorey1
        {
            internal ConfigDataDevelopmentProjectInfo applicationInfo;
            internal DevelopmentSelectionUITask $this;

            internal void <>m__0(UIProcess process, bool b)
            {
                if (b)
                {
                    this.$this.Pause();
                    if (this.$this.EventOnRequestOpenTransformationPanel != null)
                    {
                        this.$this.EventOnRequestOpenTransformationPanel(this.$this.m_nodeInfos, this.applicationInfo);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowOrHideSelectionPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess process, bool b)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(b);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpdateViewOnProjectArea,
            UpdateViewOnSecondaryArea,
            UpdateViewOnTertiaryArea,
            UpdateViewOnQuaternaryArea,
            PanelChange
        }
    }
}

