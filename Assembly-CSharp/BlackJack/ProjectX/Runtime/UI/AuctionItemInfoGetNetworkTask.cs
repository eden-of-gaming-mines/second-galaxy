﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AuctionItemInfoGetNetworkTask : NetWorkTransactionTask
    {
        private int m_result;
        public int AuctionItemId;
        public AuctionItemInfoAck ackInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAuctionItemInfoGetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public AuctionItemInfoGetNetworkTask(int AuctionItemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAuctionItemInfoGetAck(AuctionItemInfoAck result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

