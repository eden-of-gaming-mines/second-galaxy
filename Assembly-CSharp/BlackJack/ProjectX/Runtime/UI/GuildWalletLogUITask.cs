﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class GuildWalletLogUITask : GuildUITaskBase
    {
        private bool m_showDealGroup;
        private LogTabType m_logTabType;
        private int m_startIndex;
        private int m_totalPages;
        private readonly List<GuildCurrencyLogInfo> m_currencyLogInfoShowList;
        private readonly List<GuildCurrencyLogInfo> m_currencyLogInfoList;
        private readonly FlagMask m_flagMask;
        private GuildWalletLogUIController m_guildWalletLogUIController;
        private GuildWalletDealGroupUIController m_guildWalletDealGroupUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpateViewWalletData;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_GetLogListData;
        private static DelegateBridge __Hotfix_FilterTypeIsNeed;
        private static DelegateBridge __Hotfix_OnToggleValueChanged;
        private static DelegateBridge __Hotfix_OnFrameImageButtonClick;
        private static DelegateBridge __Hotfix_OnPreviousPageButtonClick;
        private static DelegateBridge __Hotfix_OnNextPageButtonClick;
        private static DelegateBridge __Hotfix_OnTacticalPointButtonClick;
        private static DelegateBridge __Hotfix_OnInformationPointButtonClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCheckboxGroupColliderButtonClick;
        private static DelegateBridge __Hotfix_StartGuildStoreUITask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnCurrencyLogItemFill;
        private static DelegateBridge __Hotfix_ClearData;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildWalletLogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearData()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterTypeIsNeed(GuildCurrencyLogFilterType guildCurrencyLogFilterType)
        {
        }

        [MethodImpl(0x8000)]
        private void GetLogListData(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCheckboxGroupColliderButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCurrencyLogItemFill(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFrameImageButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNextPageButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreviousPageButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTacticalPointButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleValueChanged(UIControllerBase ctrlBase, GuildCurrencyLogFilterType logTypeype, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeMoneyButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildStoreUITask(UIIntent prevTaskIntent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UpateViewWalletData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetLogListData>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal GuildCurrencyLogInfoReqNetTask netTask;
            internal GuildWalletLogUITask $this;

            internal void <>m__0(Task task)
            {
                if (((GuildCurrencyLogInfoReqNetTask) task).IsNetworkError)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else if (this.netTask.Result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(this.netTask.Result, true, false);
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else
                {
                    this.$this.m_startIndex = this.netTask.StartIndex;
                    this.$this.m_totalPages = this.netTask.TotalPages;
                    this.$this.m_currencyLogInfoList.Clear();
                    this.$this.m_currencyLogInfoList.AddRange(this.netTask.CurrencyLogInfo);
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(true);
                    }
                }
            }
        }

        public enum LogTabType
        {
            TradeMoney,
            TacticalPoint,
            InformationPoint
        }

        private enum PipeLineStateMaskType
        {
            UpdateShowDealGroup,
            UpdateLogTabType,
            UpdateLogList,
            ResetAllToggle
        }
    }
}

