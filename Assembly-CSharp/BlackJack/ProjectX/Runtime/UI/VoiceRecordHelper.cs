﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class VoiceRecordHelper
    {
        private bool m_isRecording;
        private bool m_isCancelRecord;
        private int m_preRecordPos;
        private AudioClip m_recordClip;
        private bool m_isResumeBGSound;
        private int m_frequency;
        private DateTime m_startTime;
        private DateTime m_endTime;
        private string m_deviceName;
        private DateTime m_nextTickTime;
        private static VoiceRecordHelper m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TryRequestAudioPermission;
        private static DelegateBridge __Hotfix_HasMicAvailable;
        private static DelegateBridge __Hotfix_StartRecord;
        private static DelegateBridge __Hotfix_StopRecord;
        private static DelegateBridge __Hotfix_IsRecording;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CancelRecord;
        private static DelegateBridge __Hotfix_SetNeedResumeBGM;
        private static DelegateBridge __Hotfix_IsArrivalMaxLength;
        private static DelegateBridge __Hotfix_GetRecordLength;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_ClearRecord;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_ClearInstance;

        [MethodImpl(0x8000)]
        private VoiceRecordHelper(string deviceName)
        {
        }

        [MethodImpl(0x8000)]
        public void CancelRecord()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearInstance()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearRecord()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRecordLength()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasMicAvailable()
        {
        }

        [MethodImpl(0x8000)]
        private void Init()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsArrivalMaxLength()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRecording()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNeedResumeBGM()
        {
        }

        [MethodImpl(0x8000)]
        public bool StartRecord()
        {
        }

        [MethodImpl(0x8000)]
        public void StopRecord(ChatChannel channel, ChatLanguageChannel languageChannel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        public void TryRequestAudioPermission()
        {
        }

        public static VoiceRecordHelper Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

