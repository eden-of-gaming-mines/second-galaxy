﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildCompensationHintUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmDonateClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonIconClick;
        public GameObject m_itemIconTemplate;
        public List<GuildCompensationHintItemUIController> m_itemCtrlList;
        public LossCompensation m_lossComp;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ConfirmButtonState;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./Scroll View/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummy;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform Content;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RootState;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnConfirmDonateClick;
        private static DelegateBridge __Hotfix_OnCancelDonateClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmDonateClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmDonateClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;

        public event Action<UIControllerBase> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmDonateClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelDonateClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmDonateClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show, bool immediate, UIProcess.OnEnd onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<LostItem> sortedList, Dictionary<string, UnityEngine.Object> dict)
        {
        }
    }
}

