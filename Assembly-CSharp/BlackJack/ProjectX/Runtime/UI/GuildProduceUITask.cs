﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildProduceUITask : GuildUITaskBase
    {
        private int m_selectTempleteId;
        private bool m_inPanelClosing;
        protected bool m_isAutoSetProduceTempleteMode;
        private bool m_isGuildProductionDataGet;
        private bool m_isGuildItemStoreDataGet;
        private bool m_isGuildSolarSystemDataGet;
        private bool m_isGuildCurrceyDataGet;
        private bool m_isGuildCurrencyAckRec;
        private bool m_isGuildProductionAckRec;
        private bool m_isGuildItemStoreAckRec;
        private bool m_isGuildSolarSystemAckRec;
        private ItemSimpleInfoUITask m_itemSimpleInfoUiTask;
        private bool m_isNeedRedirect;
        private bool m_isProduceBgTaskResLoadCompleted;
        private bool m_isNeedClearCacheOnPause;
        private bool m_isNeedAutoResetSelectProduceLine;
        private ProduceLineState m_currProduceLineState;
        private ulong m_wantSelectProduceLineId;
        private GuildProductionLineInfo m_selectedProduceLine;
        private ProduceUIController m_mainCtrl;
        private readonly List<GuildProductionLineInfo> m_produceLineCache;
        private ProduceThreeDBGUITask m_produceBgUITask;
        public const int StartGuildProductionUIErrorNotFindIdleLine = -1;
        public const int StartGuildProductionUIErrorConditionFail = -2;
        public const int StartGuildProductionUIErrorDestTempleteNotFind = -3;
        public const int StartGuildProdcutionUIErrorTaskPrepareFail = -4;
        public const string SelectProduceLineIdKey = "ProduceLineId";
        public const string SelectProduceTempleteKey = "TempleteId";
        public const string ResetSelectProduceLineKey = "ResetSelectProduceLine";
        public const string ParamKey_IsAutoSetProduceTempleteMode = "IsAutoSetProduceTempleteMode";
        public const string TaskName = "GuildProduceUITask";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Comparison<GuildProductionLineInfo> <>f__mg$cache0;
        [CompilerGenerated]
        private static Comparison<GuildProductionLineInfo> <>f__mg$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildProduceUITaskWithPrepare;
        private static DelegateBridge __Hotfix_StartGuildProduceUITask2PointTempleteWithPrepare;
        private static DelegateBridge __Hotfix_UpdateViewByAppointId;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_GetParamInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateSelectLine;
        private static DelegateBridge __Hotfix_UpdateDataCache_ProduceLineSort;
        private static DelegateBridge __Hotfix_UpdateDataCache_InitStartBgTask;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateSelectLineState;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnProduceTempleteIconButtonClick;
        private static DelegateBridge __Hotfix_OnAddProduceTempleteButtonClick;
        private static DelegateBridge __Hotfix_OnAddProduceTempleteButtonClickImp;
        private static DelegateBridge __Hotfix_OnMaterialIconClick;
        private static DelegateBridge __Hotfix_OnMaterialIconClickImp;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_OnDeleteTempleteIconClick;
        private static DelegateBridge __Hotfix_OnStartProduceButtonClick;
        private static DelegateBridge __Hotfix_OnStopProduceButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_OnGetProduceButtonClick;
        private static DelegateBridge __Hotfix_OnGuildTradeMoneyAddButton;
        private static DelegateBridge __Hotfix_OnProduceQueueItemClick;
        private static DelegateBridge __Hotfix_OnItemProduceCompleted;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_OnGuildProductionLineDestoryed;
        private static DelegateBridge __Hotfix_GuildProductionLineListSort;
        private static DelegateBridge __Hotfix_ClosePanel;
        private static DelegateBridge __Hotfix_ClearForNetWorkReq;
        private static DelegateBridge __Hotfix_IsAllPerpareAckReceive;
        private static DelegateBridge __Hotfix_IsAllDataPerpareEnd;
        private static DelegateBridge __Hotfix_GetGuildProductionData;
        private static DelegateBridge __Hotfix_GetGuildItemStoreInfoData;
        private static DelegateBridge __Hotfix_GetGuildCurrencyInfoData;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemData;
        private static DelegateBridge __Hotfix_GetSingleProductionLostReportData;
        private static DelegateBridge __Hotfix_LineStartTimeCompare;
        private static DelegateBridge __Hotfix_CheckClientDataUnSync;
        private static DelegateBridge __Hotfix_HasGuild;
        private static DelegateBridge __Hotfix_HasGuildAndPermission;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_OnProduceBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_PlaySoundForProduceUI;
        private static DelegateBridge __Hotfix_StopSound4ProduceUI;
        private static DelegateBridge __Hotfix_StartProduce;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_GetProduceItemRealCost;
        private static DelegateBridge __Hotfix_GetProduceRealTimeCost;
        private static DelegateBridge __Hotfix_IsNeedUpdateProduceQueueUI;
        private static DelegateBridge __Hotfix_IsNeedUpdateMidAndRightUI;
        private static DelegateBridge __Hotfix_ClearSelectContext;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_CreateMainPanelPlayeingEffectInfo;
        private static DelegateBridge __Hotfix_get_LbProduce;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildProduceUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckClientDataUnSync(int taskResult, int mask = -1)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearForNetWorkReq()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSelectContext()
        {
        }

        [MethodImpl(0x8000)]
        private void ClosePanel(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private UIPlayingEffectInfo CreateMainPanelPlayeingEffectInfo(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildCurrencyInfoData(Action<bool> onEnd, bool isForceSend = false)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildItemStoreInfoData(Action<bool> onEnd, bool isForceSend = false)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildProductionData(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildSolarSystemData(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private List<CostInfo> GetProduceItemRealCost(int solarSystemId, int templeteId, out ulong moneyCost)
        {
        }

        [MethodImpl(0x8000)]
        private int GetProduceRealTimeCost(int solarSystemId, int templeteId)
        {
        }

        [MethodImpl(0x8000)]
        private static void GetSingleProductionLostReportData(ulong reportId, Action<int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static List<GuildProductionLineInfo> GuildProductionLineListSort()
        {
        }

        [MethodImpl(0x8000)]
        private bool HasGuild()
        {
        }

        [MethodImpl(0x8000)]
        private bool HasGuildAndPermission()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllDataPerpareEnd()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllPerpareAckReceive()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedUpdateMidAndRightUI()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedUpdateProduceQueueUI()
        {
        }

        [MethodImpl(0x8000)]
        private static int LineStartTimeCompare(GuildProductionLineInfo infoA, GuildProductionLineInfo infoB)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddProduceTempleteButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddProduceTempleteButtonClickImp(GuildProductionLineInfo produceLine, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteTempleteIconClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGetProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildProductionLineDestoryed(List<ulong> deleteLineIdList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradeMoneyAddButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemProduceCompleted(GuildProductionLineInfo produceLine)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMaterialIconClick(int type, int confId, int index, Vector3 pos, long needCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMaterialIconClickImp(int type, int confId, int index, Vector3 pos, long needCount, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnProduceBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnProduceQueueItemClick(GuildProductionLineInfo lineUIInfo, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProduceTempleteIconButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSpeedUpButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStartProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStopProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void PlaySoundForProduceUI(LogicalSoundResTableID soundResId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoUIPanel(ItemInfo itemInfo, int index, Vector3 pos, long needCount, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildProduceUITask2PointTempleteWithPrepare(StoreItemType type, int configId, UIIntent return2Intent = null, Action<int, int> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildProduceUITaskWithPrepare(UIIntent return2Intent = null, Action<bool> onPrepareEnd = null, ulong buildingInsId = 0UL)
        {
        }

        [MethodImpl(0x8000)]
        private void StartProduce()
        {
        }

        [MethodImpl(0x8000)]
        private void StopSound4ProduceUI()
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_GetParamInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_InitStartBgTask()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_ProduceLineSort()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_UpdateSelectLine()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_UpdateSelectLineState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewByAppointId(StoreItemType type, int configId)
        {
        }

        private LogicBlockGuildClient LbProduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckClientDataUnSync>c__AnonStoreyC
        {
            internal Action<bool> onEnd;
            internal GuildProduceUITask $this;

            internal void <>m__0(bool isGet)
            {
                if (isGet)
                {
                    this.$this.m_currPipeLineCtx.AddUpdateMask(4);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }

            internal void <>m__1(bool result)
            {
                this.$this.m_isGuildProductionAckRec = true;
                this.$this.m_isGuildProductionDataGet = result;
                if (this.$this.IsAllPerpareAckReceive())
                {
                    this.onEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__2(bool result)
            {
                this.$this.m_isGuildSolarSystemAckRec = true;
                this.$this.m_isGuildSolarSystemDataGet = result;
                if (this.$this.IsAllPerpareAckReceive())
                {
                    this.onEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__3(bool result)
            {
                this.$this.m_isGuildItemStoreAckRec = true;
                this.$this.m_isGuildItemStoreDataGet = true;
                if (this.$this.IsAllPerpareAckReceive())
                {
                    this.onEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__4(bool result)
            {
                this.$this.m_isGuildCurrencyAckRec = true;
                this.$this.m_isGuildCurrceyDataGet = result;
                if (this.$this.IsAllPerpareAckReceive())
                {
                    this.onEnd(this.$this.IsAllDataPerpareEnd());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ClosePanel>c__AnonStorey6
        {
            internal Action onEnd;
            internal GuildProduceUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.m_inPanelClosing = false;
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildCurrencyInfoData>c__AnonStorey9
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildItemStoreInfoData>c__AnonStorey8
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildProductionData>c__AnonStorey7
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(((GetGuildProductionInfoReqNetTask) task).Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildSolarSystemData>c__AnonStoreyA
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task2 = task as GuildOccupiedSolarSystemInfoReqNetTask;
                if (this.onEnd != null)
                {
                    this.onEnd((task2 != null) && (task2.m_result == 0));
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetSingleProductionLostReportData>c__AnonStoreyB
        {
            internal Action<int> onEnd;

            internal void <>m__0(Task task)
            {
                LostReportGetReqNetTask task2 = task as LostReportGetReqNetTask;
                if ((this.onEnd != null) && (task2 != null))
                {
                    this.onEnd(task2.Result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnAddProduceTempleteButtonClickImp>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal GuildProduceUITask $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                ProduceItemStoreUITask.StartProduceItemStoreUITask("GuildProduce", this.$this.m_currIntent, this.$this.m_selectTempleteId, false, false, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnGetProduceButtonClick>c__AnonStorey3
        {
            internal int templeteId;
            internal GuildProduceUITask $this;

            internal void <>m__0(Task task)
            {
                GuildProductionLineCompleteComfirmReqNetTask task2 = task as GuildProductionLineCompleteComfirmReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        this.$this.CheckClientDataUnSync(task2.Result, -1);
                    }
                    else
                    {
                        string stringInStringTableWithId = StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_PickDropBoxItemSuccess, new object[0]);
                        ConfigDataGuildProduceTempleteInfo configDataGuildProduceTempleteInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataGuildProduceTempleteInfo(this.templeteId);
                        if (configDataGuildProduceTempleteInfo != null)
                        {
                            TipWindowUITask.ShowTipWindow(string.Format(stringInStringTableWithId, SolarSystemUIHelper.GetDropBoxItemDescStr(configDataGuildProduceTempleteInfo.ProduceCategory, configDataGuildProduceTempleteInfo.ProductId, 1L)), false);
                        }
                        this.$this.m_isGuildItemStoreAckRec = false;
                        this.$this.GetGuildItemStoreInfoData(delegate (bool ret) {
                            this.$this.m_isGuildItemStoreAckRec = true;
                            if (this.$this.m_isGuildItemStoreAckRec)
                            {
                                this.$this.m_currPipeLineCtx.AddUpdateMask(4);
                                this.$this.m_selectedProduceLine = null;
                                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                            }
                        }, true);
                    }
                }
            }

            internal void <>m__1(bool ret)
            {
                this.$this.m_isGuildItemStoreAckRec = true;
                if (this.$this.m_isGuildItemStoreAckRec)
                {
                    this.$this.m_currPipeLineCtx.AddUpdateMask(4);
                    this.$this.m_selectedProduceLine = null;
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnProduceQueueItemClick>c__AnonStorey4
        {
            internal LogicBlockGuildClient lbGuild;
            internal GuildProduceUITask.<OnProduceQueueItemClick>c__AnonStorey5 <>f__ref$5;

            internal void <>m__0(int result)
            {
                if (result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                }
                else
                {
                    this.<>f__ref$5.$this.Pause();
                    GuildLostReportUITask.StartBuildingLostReportUITask(this.<>f__ref$5.$this.m_currIntent, this.lbGuild.GetBuildingLostReport(this.<>f__ref$5.lineUIInfo.m_reportId), this.<>f__ref$5.lineUIInfo.m_lineLevel);
                }
                this.<>f__ref$5.$this.CheckClientDataUnSync(result, -1);
            }
        }

        [CompilerGenerated]
        private sealed class <OnProduceQueueItemClick>c__AnonStorey5
        {
            internal GuildProductionLineInfo lineUIInfo;
            internal GuildProduceUITask $this;
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal GuildProduceUITask $this;

            internal void <>m__0(bool result)
            {
                this.$this.m_isGuildProductionAckRec = true;
                this.$this.m_isGuildProductionDataGet = result;
                if (this.$this.IsAllPerpareAckReceive() && (this.onPrepareEnd != null))
                {
                    this.onPrepareEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__1(bool result)
            {
                this.$this.m_isGuildSolarSystemAckRec = true;
                this.$this.m_isGuildSolarSystemDataGet = result;
                if (this.$this.IsAllPerpareAckReceive() && (this.onPrepareEnd != null))
                {
                    this.onPrepareEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__2(bool result)
            {
                this.$this.m_isGuildItemStoreAckRec = true;
                this.$this.m_isGuildItemStoreDataGet = true;
                if (this.$this.IsAllPerpareAckReceive() && (this.onPrepareEnd != null))
                {
                    this.onPrepareEnd(this.$this.IsAllDataPerpareEnd());
                }
            }

            internal void <>m__3(bool result)
            {
                this.$this.m_isGuildCurrencyAckRec = true;
                this.$this.m_isGuildCurrceyDataGet = result;
                if (this.$this.IsAllPerpareAckReceive() && (this.onPrepareEnd != null))
                {
                    this.onPrepareEnd(this.$this.IsAllDataPerpareEnd());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartGuildProduceUITask2PointTempleteWithPrepare>c__AnonStorey0
        {
            internal Action<int, int> onPrepareEnd;
            internal ConfigDataGuildProduceTempleteInfo templeteInfo;
            internal LogicBlockGuildClient lbGuild;
            internal UIIntent return2Intent;

            internal void <>m__0(Task task)
            {
                GuildProductionLineInfo info = null;
                foreach (GuildProductionLineInfo info2 in GuildProduceUITask.GuildProductionLineListSort())
                {
                    if ((info2.m_reportId == 0L) && info2.IsIdle())
                    {
                        info = info2;
                        break;
                    }
                }
                if (info == null)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(-1, this.templeteInfo.ID);
                    }
                }
                else
                {
                    int num;
                    if (!this.lbGuild.CheckProduceTempleteUnlockCondition(this.templeteInfo, out num))
                    {
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(-2, this.templeteInfo.ID);
                        }
                    }
                    else
                    {
                        UIIntentReturnable intent = new UIIntentReturnable(this.return2Intent, "GuildProduceUITask", null);
                        intent.SetParam("ProduceLineId", info.m_productionLineId);
                        intent.SetParam("TempleteId", this.templeteInfo.ID);
                        UIManager.Instance.StartUITaskWithPrepare(intent, ret => this.onPrepareEnd(!ret ? -4 : 0, this.templeteInfo.ID), true, null, null);
                    }
                }
            }

            internal void <>m__1(bool ret)
            {
                this.onPrepareEnd(!ret ? -4 : 0, this.templeteInfo.ID);
            }
        }

        public enum PipeLineMaskType
        {
            Init,
            Resume,
            TempleteChanged,
            ItemSelected,
            NetworkAck,
            ItemComplete,
            ServerDataSync
        }
    }
}

