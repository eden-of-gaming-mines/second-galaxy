﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SendGuildTradePurchaseOrderUpdateNetTask : NetWorkTransactionTask
    {
        private int m_result;
        private readonly ulong m_instanceId;
        private readonly int m_solarSystemId;
        private readonly int m_count;
        private readonly long m_price;
        private readonly ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public SendGuildTradePurchaseOrderUpdateNetTask(ulong instanceId, int solarSystemId, int count, long price, ushort version)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

