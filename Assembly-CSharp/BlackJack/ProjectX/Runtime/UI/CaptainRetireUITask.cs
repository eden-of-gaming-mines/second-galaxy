﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CaptainRetireUITask : UITaskBase
    {
        public const string ParamKey_CaptainInstanceId = "CaptainInstanceId";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseItemSimpleInfo;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action OnCloseUI;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action OnCaptainRetired;
        private CaptainRetireUIController m_mainCtrl;
        private LBStaticHiredCaptain m_selCaptain;
        private int m_remainFeatId;
        private int m_selEmptyFeatBookCount;
        private long m_emptyFeatBookCountInItemStore;
        private double m_remainRate;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCaptainRetireUITask;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnRetireCancelButtonClick;
        private static DelegateBridge __Hotfix_OnRetireConfirmButtonClick;
        private static DelegateBridge __Hotfix_SendCaptainRetireReq;
        private static DelegateBridge __Hotfix_OnEmptyFeatBookAddButtonClick;
        private static DelegateBridge __Hotfix_OnEmptyFeatBookAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnEmptyFeatBookRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnEmptyFeatBookRemoveButtonLongPressing;
        private static DelegateBridge __Hotfix_OnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_OnEmptyFeatBookButtonClick;
        private static DelegateBridge __Hotfix_GetCacheDataFromUIIntent;
        private static DelegateBridge __Hotfix_UpdateFeatBookCountAndRemainProbability;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_add_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_remove_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_add_EventOnCloseItemSimpleInfo;
        private static DelegateBridge __Hotfix_remove_EventOnCloseItemSimpleInfo;
        private static DelegateBridge __Hotfix_add_OnCloseUI;
        private static DelegateBridge __Hotfix_remove_OnCloseUI;
        private static DelegateBridge __Hotfix_add_OnCaptainRetired;
        private static DelegateBridge __Hotfix_remove_OnCaptainRetired;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnCloseItemSimpleInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action OnCaptainRetired
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action OnCloseUI
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainRetireUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void GetCacheDataFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainFeatItemClick(int featId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyFeatBookAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyFeatBookAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyFeatBookButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyFeatBookRemoveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyFeatBookRemoveButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRetireCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRetireConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void SendCaptainRetireReq()
        {
        }

        [MethodImpl(0x8000)]
        public static CaptainRetireUITask StartCaptainRetireUITask(ulong captainInsId, Action onCloseAction, Action onRetireCompleteAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFeatBookCountAndRemainProbability()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            ShowRetirePanel
        }
    }
}

