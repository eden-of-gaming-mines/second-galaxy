﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class BlackMarketResourcesShopItemUIController : UIControllerBase
    {
        private Action<UIControllerBase> m_eventOnUIItemIconClick;
        public ConfigDataBlackMarketShopItemInfo m_shopItemInfo;
        public ScrollItemBaseUIController m_scrollCtrl;
        public CommonItemIconUIController m_itemIconUICtrl;
        [AutoBind("./MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_moneyTextCommonUIStateController;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateController;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_itemIconDummy;
        [AutoBind("./TitileText/NymberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nymberText;
        [AutoBind("./MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumText;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemDescText;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        private static DelegateBridge __Hotfix_UpdateItem;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemIconClickEvent;
        private static DelegateBridge __Hotfix_SetSelect;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemClick;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_SetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool careButtonClick = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemIconClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelect(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItem(ConfigDataBlackMarketShopItemInfo shopItemInfo, Dictionary<string, UnityEngine.Object> resDict, int canBuyCount)
        {
        }
    }
}

