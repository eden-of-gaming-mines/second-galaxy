﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapStarfieldNodeUIController : UIControllerBase
    {
        private int m_starfieldId;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./StarfieldInfoRoot/StarfieldNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starfieldNameText;
        [AutoBind("./StarfieldInfoRoot/StarfieldSecurityLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starfieldSecurityLevelText;
        [AutoBind("./StarfieldIconButtonRoot/StarfieldIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_starfieldIconButton;
        private static DelegateBridge __Hotfix_SetUIStateForStarfieldNodeClicked;
        private static DelegateBridge __Hotfix_SetStarfieldId;
        private static DelegateBridge __Hotfix_GetStarfieldId;
        private static DelegateBridge __Hotfix_SetStarfieldNodeName;
        private static DelegateBridge __Hotfix_SetStarfieldSecurityLevel;

        [MethodImpl(0x8000)]
        public int GetStarfieldId()
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarfieldId(int id)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarfieldNodeName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarfieldSecurityLevel(float level)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess SetUIStateForStarfieldNodeClicked(bool immediateComplete, bool allowToRefreshSameState)
        {
        }
    }
}

