﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RankingListUIController : UIControllerBase
    {
        private const string StatePersonal = "Personage";
        private const string StateGuild = "Guild";
        private const string StateAlliance = "Alliance";
        private const string StateSeleted = "Select";
        private const string StateUnSeleted = "Unselect";
        public RankingListSingleRankingUIController m_singleRankingUICtrl;
        public RankingListTotalRankingUIController m_totalRankingUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPersonRankingToggleClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildRankingToggleClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllianceRankingToggleClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowsStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./TitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_titleStateCtrl;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ToggleGroup/PersonRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PersonRankingButton;
        [AutoBind("./ToggleGroup/PersonRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PersonRankingButtonStateCtrl;
        [AutoBind("./ToggleGroup/AllianceRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllianceRankingButton;
        [AutoBind("./ToggleGroup/AllianceRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllianceRankingButtonStateCtrl;
        [AutoBind("./ToggleGroup/GuildRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildRankingButton;
        [AutoBind("./ToggleGroup/GuildRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildRankingButtonStateCtrl;
        [AutoBind("./SumDataGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TotalRankingUIRoot;
        [AutoBind("./ClassifyDataGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SingleRankingUIRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetBackButtonUIActive;
        private static DelegateBridge __Hotfix_UpdateRankingListUI;
        private static DelegateBridge __Hotfix_OnPersonRankingToggleClick;
        private static DelegateBridge __Hotfix_OnAllianceRankingToggleClick;
        private static DelegateBridge __Hotfix_OnGuildRankingToggleClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPersonRankingToggleClick;
        private static DelegateBridge __Hotfix_remove_EventOnPersonRankingToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildRankingToggleClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildRankingToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnAllianceRankingToggleClick;
        private static DelegateBridge __Hotfix_remove_EventOnAllianceRankingToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;

        public event Action EventOnAllianceRankingToggleClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildRankingToggleClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPersonRankingToggleClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnAllianceRankingToggleClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildRankingToggleClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPersonRankingToggleClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackButtonUIActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRankingListUI(RankingListType rankingListType)
        {
        }
    }
}

