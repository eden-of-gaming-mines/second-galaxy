﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class TechUpgradeTreeUIController : UIControllerBase
    {
        private List<TechUpgradeTreeNodeUIController> m_techItemNodeCtrlList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnTechNodeClick;
        [AutoBind("./TechTree/TechItems", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechItemGroup;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetTechTreeUIProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateTechTreeInfo;
        private static DelegateBridge __Hotfix_OnTechNodeClick;
        private static DelegateBridge __Hotfix_add_EventOnTechNodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnTechNodeClick;

        public event Action<int> EventOnTechNodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetTechTreeUIProcess(bool isShow, bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTechNodeClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechTreeInfo(List<ConfigDataTechInfo> techInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

