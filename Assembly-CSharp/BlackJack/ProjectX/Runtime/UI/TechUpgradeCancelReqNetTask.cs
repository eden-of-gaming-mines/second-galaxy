﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TechUpgradeCancelReqNetTask : NetWorkTransactionTask
    {
        private int m_techId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TechUpgradeCancelResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTechUpgradeCancelAck;
        private static DelegateBridge __Hotfix_set_TechUpgradeCancelResult;
        private static DelegateBridge __Hotfix_get_TechUpgradeCancelResult;

        [MethodImpl(0x8000)]
        public TechUpgradeCancelReqNetTask(int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeCancelAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int TechUpgradeCancelResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

