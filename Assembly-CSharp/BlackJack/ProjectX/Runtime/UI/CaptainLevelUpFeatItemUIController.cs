﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainLevelUpFeatItemUIController : UIControllerBase
    {
        [AutoBind("./FeatGroup/FeatIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FeatIconImage;
        [AutoBind("./DescTextGroup/FeatName/FeatNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatNameText;
        [AutoBind("./DescTextGroup/FeatName/LvGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatLvGroup;
        [AutoBind("./DescTextGroup/FeatName/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NewFeatLvText;
        [AutoBind("./DescTextGroup/FeatName/LevelUpGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatLevelUpGroup;
        [AutoBind("./DescTextGroup/FeatName/LevelUpGroup/PreLvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreFeatLvText;
        [AutoBind("./DescTextGroup/FeatName/LevelUpGroup/CurrLvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrFeatLvText;
        [AutoBind("./DescTextGroup/FeatDesc1/FeatDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatPropertyDesc01;
        [AutoBind("./DescTextGroup/FeatDesc1/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatPropertyValue01;
        private static DelegateBridge __Hotfix_SetLevelUpFeatItemInfo;

        [MethodImpl(0x8000)]
        public void SetLevelUpFeatItemInfo(LBNpcCaptainFeats featInfo, Dictionary<string, UnityEngine.Object> resDict, int preLevel = -1)
        {
        }
    }
}

