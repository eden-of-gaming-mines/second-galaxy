﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class CharacterSimpleInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCharacterDetailButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSendChatMsgButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnTeamInviteButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSendMailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnExpelGuildButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEditJobButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDiplomacyUpdateButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnReportButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData, Action<PointerEventData>> EventOnPassBkgClick;
        private const string ButtonStateLock = "Forbid";
        private const string ButtonStateUnLock = "Normal";
        private Rect m_viewSize;
        private Dictionary<CharacterSimpleInfoUITask.ButtonType, CommonUIStateController> m_buttonCtrlStates;
        private Dictionary<CharacterSimpleInfoUITask.ButtonType, CharacterSimpleInfoUITask.ButtonState> m_buttonStates;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./Details/Buttons/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_characterDetailButton;
        [AutoBind("./Details/Buttons/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_characterDetailButtonStateCtrl;
        [AutoBind("./Details/Buttons/SendMsgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sendChatMsgButton;
        [AutoBind("./Details/Buttons/SendMsgButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sendChatMsgButtonStateCtrl;
        [AutoBind("./Details/Buttons/ReportButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_reportButton;
        [AutoBind("./Details/Buttons/ReportButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_reportButtonStateCtrl;
        [AutoBind("./Details/Buttons/FormTeamButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_teamInviteButton;
        [AutoBind("./Details/Buttons/FormTeamButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamInviteButtonStateCtrl;
        [AutoBind("./Details/Buttons/SendMailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sendMailButton;
        [AutoBind("./Details/Buttons/SendMailButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sendMailButtonStateCtrl;
        [AutoBind("./Details/Buttons/RelationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_diplomacyUpdateButton;
        [AutoBind("./Details/Buttons/RelationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_diplomacyUpdateButtonStateCtrl;
        [AutoBind("./Details/Buttons/ExpelGuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_expelGuildButton;
        [AutoBind("./Details/Buttons/ExpelGuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_expelGuildButtonStateCtrl;
        [AutoBind("./Details/Buttons/EditJob", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_editJobdButton;
        [AutoBind("./Details/Buttons/EditJob", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_editJobStateCtrl;
        [AutoBind("./Details/PlayerInfo/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerNameText;
        [AutoBind("./Details/PlayerInfo/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerLevelText;
        [AutoBind("./Details/PlayerInfo/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_playerIconImage;
        [AutoBind("./Details/PlayerInfo/UnionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unionNameText;
        [AutoBind("./BackgroundImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backgroundButton;
        [AutoBind("./Details", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_panelRoot;
        [AutoBind("./BackgroundImage", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController m_passEventCtrl;
        [AutoBind("./Details/DiplomacySetPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_diplomacySetPanelDummy;
        [AutoBind("./Details", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailStateCtrl;
        [AutoBind("./center", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_center;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateCharacterSimpleInfo;
        private static DelegateBridge __Hotfix_EnableBGImage;
        private static DelegateBridge __Hotfix_RefreshButtonState;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_SetDiplomacyPosition;
        private static DelegateBridge __Hotfix_GetDiplomacySetPanelPosition;
        private static DelegateBridge __Hotfix_OnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSendChatMsgButtonClick;
        private static DelegateBridge __Hotfix_OnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_OnSendMailButtonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_OnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_OnEditJobButtonCilck;
        private static DelegateBridge __Hotfix_OnReportButtonCilck;
        private static DelegateBridge __Hotfix_IsButtonForbid;
        private static DelegateBridge __Hotfix_OnPassBkgClick;
        private static DelegateBridge __Hotfix_GetViewRect;
        private static DelegateBridge __Hotfix_GetPanelSize;
        private static DelegateBridge __Hotfix_add_EventOnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendChatMsgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendChatMsgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendMailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendMailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnEditJobButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnEditJobButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyUpdateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnReportButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnReportButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPassBkgClick;
        private static DelegateBridge __Hotfix_remove_EventOnPassBkgClick;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_SetMailButtonState;
        private static DelegateBridge __Hotfix_SetTeamButtonState;
        private static DelegateBridge __Hotfix_SetChatButtonState;
        private static DelegateBridge __Hotfix_get_ViewSize;

        public event Action EventOnCharacterDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDiplomacyUpdateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEditJobButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnExpelGuildButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData, Action<PointerEventData>> EventOnPassBkgClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnReportButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSendChatMsgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSendMailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTeamInviteButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBGImage(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetDiplomacySetPanelPosition()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetPanelSize()
        {
        }

        [MethodImpl(0x8000)]
        private Rect GetViewRect()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsButtonForbid(CharacterSimpleInfoUITask.ButtonType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterDetailButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyUpdateButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditJobButtonCilck(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnExpelGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPassBkgClick(PointerEventData eventData, Action<PointerEventData> eventAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReportButtonCilck(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendChatMsgButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendMailButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamInviteButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonState(Dictionary<CharacterSimpleInfoUITask.ButtonType, CharacterSimpleInfoUITask.ButtonState> buttonStates)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChatButtonState(bool isFunOpen)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDiplomacyPosition(CharacterSimpleInfoUITask.DiplomacyPostionType posType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMailButtonState(bool isFunOpen)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 pos, float space)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTeamButtonState(bool isFunOpen)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCharacterSimpleInfo(ProPlayerSimplestInfo playerSimpleInfo, CharacterSimpleInfoUITask.DiplomacyPostionType diplomacyPos, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public Rect ViewSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

