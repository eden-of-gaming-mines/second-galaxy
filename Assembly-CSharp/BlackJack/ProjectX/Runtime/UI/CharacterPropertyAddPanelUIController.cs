﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterPropertyAddPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnConfirmButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnResetPointButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnClearBasicPropertiesButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackButtonClick;
        public CharacterPropertyEditorItemUIController m_weaponPropertyEditorCtrl;
        public CharacterPropertyEditorItemUIController m_drivePropertyEditorCtrl;
        public CharacterPropertyEditorItemUIController m_defensePropertyEditorCtrl;
        public CharacterPropertyEditorItemUIController m_electronicPropertyEditorCtrl;
        private float m_weaponPropertyEffectValue;
        private float m_drivePropertyEffectValue;
        private float m_defensePropertyEffectValue;
        private float m_electronicPropertyEffectValue;
        private float m_realWeaponPropertyValue;
        private float m_realDrivePropertyValue;
        private float m_realDefensePropertyValue;
        private float m_realElectronicPropertyValue;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowCtrl;
        [AutoBind("./DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_propertyPointChangeStateCtrl;
        [AutoBind("./DetailPanel/PropertyPointCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyPointCountText;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./DetailPanel/ResetPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_resetPointButton;
        [AutoBind("./DetailPanel/ClearBasicPropertiesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_clearBasicPropertiesButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./DetailPanel/WeaponPropertyPointInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_weaponPropertyPointEditorObj;
        [AutoBind("./DetailPanel/DrivePropertyPointInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_drivePropertyPointEditorObj;
        [AutoBind("./DetailPanel/DefensePropertyPointInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_defensePropertyPointEditorObj;
        [AutoBind("./DetailPanel/ElectronicPropertyPointInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_electronicPropertyPointEditorObj;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ClearData;
        private static DelegateBridge __Hotfix_InitData;
        private static DelegateBridge __Hotfix_SetFreePropertyPoint;
        private static DelegateBridge __Hotfix_SetPropertyPointInfo;
        private static DelegateBridge __Hotfix_SetPropertyPointChangeState;
        private static DelegateBridge __Hotfix_CreatPanelAnimtionProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnResetPointButtonClick;
        private static DelegateBridge __Hotfix_OnClearBasicPropertiesButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnResetPointButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnResetPointButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnClearBasicPropertiesButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnClearBasicPropertiesButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_get_LBChar;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnClearBasicPropertiesButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnResetPointButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearData()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatPanelAnimtionProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void InitData()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClearBasicPropertiesButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResetPointButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFreePropertyPoint(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyPointChangeState(bool isChanged)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyPointInfo(PropertyCategory propertyCategory, int currPointCount, int minPointCount, int maxPointCount, int freePointCount)
        {
        }

        private LogicBlockCharacterClient LBChar
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

