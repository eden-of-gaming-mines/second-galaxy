﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestDetailUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnQuestRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnQuestRewardItem3DTouch;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./ItemSimpleInfoPanel/ItemSimpleInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoPanelDummy;
        [AutoBind("./QuestItem/TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestNameText;
        [AutoBind("./QuestItem/TargetInfo/Info/TextGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestLevelText;
        [AutoBind("./QuestItem/TargetInfo/Info/TextGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_questLevelTextStateCtrl;
        [AutoBind("./QuestItem/TargetInfo/Info/ShipTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTypeImage;
        [AutoBind("./QuestItem/TargetInfo/Info/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image QuestIconImage;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/MissionDetail/MissionText/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestCompleteCondText;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestReward;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/ExtraReward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExtraQuestReward;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardBonusStateCtrl;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public Button RewardBonusButton;
        [AutoBind("./QuestItem/ScrollView/Viewport/Detail/Reward/TextTitle/RewardBonus/BonusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardBonusText;
        [AutoBind("./PoolParent", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        private CommonRewardMoneyItemsUIController m_questRewardCtrl;
        private CommonRewardMoneyItemsUIController m_extraQuestRewardCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateQuestDetail;
        private static DelegateBridge __Hotfix_SetQuestReward;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelDummyPos;
        private static DelegateBridge __Hotfix_GetPanelShorOrHideUIProcess;
        private static DelegateBridge __Hotfix_SetPanelShow;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItem3DTouch;

        public event Action<CommonItemIconUIController> EventOnQuestRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShorOrHideUIProcess(bool isShow, bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItem3DTouch(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelShow(bool isShow, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestReward(ConfigDataQuestInfo confQuestInfo, Dictionary<string, UnityEngine.Object> resDict, List<VirtualBuffDesc> virtualBuffList, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestDetail(ConfigDataQuestInfo confQuestInfo, Dictionary<string, UnityEngine.Object> resDict, int level)
        {
        }
    }
}

