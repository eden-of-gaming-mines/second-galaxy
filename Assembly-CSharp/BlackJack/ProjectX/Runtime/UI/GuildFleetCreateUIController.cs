﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildFleetCreateUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_mainStateCtrl;
        [AutoBind("./ContentGroup/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField m_inputFieldImage;
        [AutoBind("./ContentGroup/InputField/Placeholder1", AutoBindAttribute.InitState.NotInit, false)]
        private Graphic m_inputFieldPlaceholder1;
        [AutoBind("./ContentGroup/InputField/Placeholder2", AutoBindAttribute.InitState.NotInit, false)]
        private Graphic m_inputFieldPlaceholder2;
        [AutoBind("./ContentGroup/SubstitutionText02", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_guildFleetName;
        [AutoBind("./ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx m_cancelButton;
        [AutoBind("./ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx m_confirmButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCurGuildFleetName;
        private static DelegateBridge __Hotfix_SetIsCreateFleetMode;
        private static DelegateBridge __Hotfix_get_GuildFleetName;
        private static DelegateBridge __Hotfix_set_GuildFleetName;
        private static DelegateBridge __Hotfix_GetCreateFleetUIProcess;

        [MethodImpl(0x8000)]
        public UIProcess GetCreateFleetUIProcess(string targetMode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurGuildFleetName(string fleetName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsCreateFleetMode(bool value)
        {
        }

        public string GuildFleetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

