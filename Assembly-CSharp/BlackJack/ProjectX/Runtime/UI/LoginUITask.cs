﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.Tools;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.LibClient;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime.SDK;
    using BlackJack.ProjectX.Static;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LoginUITask : LoginUITaskBase
    {
        private LanguageType m_languageType;
        private LoginUIController m_mainCtrl;
        public UserSettingSwitchLanguageUIController m_userSettingSwitchLanguageUIController;
        private ServerInfoListUIController m_serverListCtrl;
        private ServerInfo m_currServerInfo;
        private string m_password;
        private bool m_hasUserSettingChanged;
        protected bool m_isNeedSdkRelogin;
        private static string m_prefKeyForLastLoginServerIP;
        private static string m_prefKeyForLastLoginServerPort;
        private static string m_prefKeyForAccountName;
        private static string m_prefKeyForLastLoginedServerId;
        public const string ParamKeySwitchAccount = "SwitchAccount";
        private string m_pdsdkLoginReturnData;
        private string m_pdsdkLoginReturnOpcode;
        private string m_pdsdkLoginReturnChannelId;
        private string m_pdsdkLoginReturnCustomParams;
        private string m_pdsdkLoginReturnOperators;
        private string m_loginAgentReturnAuthToken;
        private string m_loginAgentReturnGameUserId;
        private float m_netLoseConnectFlagTime;
        private const float NetLoseConnectWaitInterval = 0.5f;
        private bool m_returnFromCreateCharacter;
        private int m_chapterQuestCutsceneStartQuestId;
        private const float LoginAgentTimeoutSeconds = 20f;
        private string m_loginAnnouncementStr;
        private Action m_actionOnloginAnnouncementConfirm;
        private string m_lastErrorMessage;
        private const int SendLogClickNeedCount = 5;
        private int m_sendLogClickCountNow;
        private int m_loginSessionOutTimeCount;
        private const int LoginSessionOutTimeMaxCount = 3;
        protected string m_serverInfoTxt;
        private LoginBGTask m_bgTask;
        private bool m_isCreateCharacter;
        private static bool m_isPreludeCGPlayed;
        protected bool m_isLoginBGTaskReadyToShow;
        protected const int GetServerInfoFixedTime = 30;
        private bool m_processingQrLogin;
        private DateTime m_qrLoginTimeOutTime;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private List<ServerInfo> m_serverInfoList;
        private bool m_isAllUserHasPrivilege;
        private HashSet<string> m_privilegeUserNameSet;
        private SessionTokenCacheDataContainer m_sessionTokenContainer;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        [CompilerGenerated]
        private static Action <>f__am$cache2;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map6;
        [CompilerGenerated]
        private static Action <>f__am$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsSDKLogin;
        private static DelegateBridge __Hotfix_ClearContextOnIntentChange;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_RegisterConfigDataInfoEvent;
        private static DelegateBridge __Hotfix_UnRegisterConfigDataInfoEvent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnLogoAnimationFinished;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_MakeSDKLogout;
        private static DelegateBridge __Hotfix_OnSDKLogout;
        private static DelegateBridge __Hotfix_DownloadGameServerLoginAnnouncementBeforeSDKLogin;
        private static DelegateBridge __Hotfix_GetLoginAnnouncementUrl;
        private static DelegateBridge __Hotfix_OnDownloadGameServerLoginAnnouncementEndBeforeSDKLogin;
        private static DelegateBridge __Hotfix_StartSDKLogin_1;
        private static DelegateBridge __Hotfix_StartSDKLogin_0;
        private static DelegateBridge __Hotfix_OnSDKLoginEnd;
        private static DelegateBridge __Hotfix_PostSDKLogin;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithGameSettingTokenAndServer;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithUIInputAccountAndServer;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithServerListInEditor;
        private static DelegateBridge __Hotfix_DownloadGameServerLoginAnnouncement;
        private static DelegateBridge __Hotfix_ShowGameServerLoginAnnouncementUI;
        private static DelegateBridge __Hotfix_ShowEnterGameUI;
        private static DelegateBridge __Hotfix_IsNeedSelectServer;
        private static DelegateBridge __Hotfix_DownloadServerList;
        private static DelegateBridge __Hotfix_OnDownloadServerListEnd;
        private static DelegateBridge __Hotfix_ShowServerListUI;
        private static DelegateBridge __Hotfix_OnDownloadServerListFailed;
        private static DelegateBridge __Hotfix_GetLastLoginedServerId;
        private static DelegateBridge __Hotfix_GetLastLoginUserId;
        private static DelegateBridge __Hotfix_InitServerCtxByServerId;
        private static DelegateBridge __Hotfix_StartLoginAgentLogin;
        private static DelegateBridge __Hotfix_StartSdkLoginAgentLogin;
        private static DelegateBridge __Hotfix_GetLoginAgentUrlFromSdk;
        private static DelegateBridge __Hotfix_LoginAgentLoginEnd;
        private static DelegateBridge __Hotfix_StartAuthLogin;
        private static DelegateBridge __Hotfix_StartSessionLogin;
        private static DelegateBridge __Hotfix_StartGameAuthLogin;
        private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_StartGameSessionLogin;
        private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_OnConfigDataMd5InfoNtf;
        private static DelegateBridge __Hotfix_StartPlayerInfoInitReq;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_StartCreateCharacter;
        private static DelegateBridge __Hotfix_OnCreateCharacterEnd;
        private static DelegateBridge __Hotfix_SendEnterWorldReq;
        private static DelegateBridge __Hotfix_OnEnterWorldAck;
        private static DelegateBridge __Hotfix_SendNecessaryReqToServer;
        private static DelegateBridge __Hotfix_LauncheMainUI;
        private static DelegateBridge __Hotfix_LaunchCharacterUI;
        private static DelegateBridge __Hotfix_OnWaitingMsgAckOutTime;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_StartPdSdkLogout;
        private static DelegateBridge __Hotfix_OnPdSdkLoginOutEnd;
        private static DelegateBridge __Hotfix_StartPdSdkLogin;
        private static DelegateBridge __Hotfix_OnPdSdkLoginEnd;
        private static DelegateBridge __Hotfix_PostPdSdkLogin;
        private static DelegateBridge __Hotfix_GetPdSdkConvertErrorCode;
        private static DelegateBridge __Hotfix_StartQRLogin;
        private static DelegateBridge __Hotfix_OnQRLoginEnd;
        private static DelegateBridge __Hotfix_OnGameServerConnected;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_OnGameServerNetworkError;
        private static DelegateBridge __Hotfix_OnChapterQuestCutsceneStartNtf;
        private static DelegateBridge __Hotfix_OnEnterGameButtonClick;
        private static DelegateBridge __Hotfix_EnterGameImpl4IosReview;
        private static DelegateBridge __Hotfix_EnterGameImpl4Normal;
        private static DelegateBridge __Hotfix_CheckBundleDataVersion;
        private static DelegateBridge __Hotfix_OnSendLogBtnClick;
        private static DelegateBridge __Hotfix_OnMovieButtonClick;
        private static DelegateBridge __Hotfix_EnterGame;
        private static DelegateBridge __Hotfix_LoginWithNewUserId;
        private static DelegateBridge __Hotfix_OnChangeAccountButtonClick;
        private static DelegateBridge __Hotfix_OnLoginAnnoucementButtonClick;
        private static DelegateBridge __Hotfix_OnLoginAnnoucementConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageButtonClick;
        private static DelegateBridge __Hotfix_OnSwitchUserSuccessCallback;
        private static DelegateBridge __Hotfix_OnCodeScanButtonClick;
        private static DelegateBridge __Hotfix_UpdateLanguageSelectedPanel;
        private static DelegateBridge __Hotfix_OnLanguageApplyButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageGroupConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnLanguageGroupCloseButtonClick;
        private static DelegateBridge __Hotfix_FixedTimeForGetServerInfo;
        private static DelegateBridge __Hotfix_StartLoginBGTask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInLoginBGTask;
        private static DelegateBridge __Hotfix_IsSessionTokenValid;
        private static DelegateBridge __Hotfix_GetLastSessionToken;
        private static DelegateBridge __Hotfix_GetLastSessionOutDateTime;
        private static DelegateBridge __Hotfix_ClearSessionTokenCache;
        private static DelegateBridge __Hotfix_InitAuthLoginServerInfo;
        private static DelegateBridge __Hotfix_RecordUserLoginInfo;
        private static DelegateBridge __Hotfix_SetLoginUICtrlMode;
        private static DelegateBridge __Hotfix_IsCurrServerMaintenance;
        private static DelegateBridge __Hotfix_PlayLoginUITaskBGM;
        private static DelegateBridge __Hotfix_ShowLoadingState;
        private static DelegateBridge __Hotfix_SetClientSystemPushInfoSyncNtf;
        private static DelegateBridge __Hotfix_OnStartRetryExecuteNetWork;
        private static DelegateBridge __Hotfix_ShowNetworkErrorMsgBox;
        private static DelegateBridge __Hotfix_GetNetworkErrorMsg;
        private static DelegateBridge __Hotfix_RegistChapterQuestCutsceneEvent;
        private static DelegateBridge __Hotfix_UnRegistChapterQuestCutsceneEvent;
        private static DelegateBridge __Hotfix_GetConvertErrorCode;
        private static DelegateBridge __Hotfix_ParseLoginAgentAck;
        private static DelegateBridge __Hotfix_get_CurrServerId;
        private static DelegateBridge __Hotfix_get_CurrGameUserId;
        private static DelegateBridge __Hotfix_get_MainLayer;
        private static DelegateBridge __Hotfix_get_IsNeedSdkRelogin;
        private static DelegateBridge __Hotfix_set_IsNeedSdkRelogin;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdateServerInfoList;
        private static DelegateBridge __Hotfix_ParseServerInfoList;
        private static DelegateBridge __Hotfix_ClearServerListCache;
        private static DelegateBridge __Hotfix_GetServerInfoByServerId;
        private static DelegateBridge __Hotfix_GetRecommedServerInfo;
        private static DelegateBridge __Hotfix_SetPrivilegeUserList;
        private static DelegateBridge __Hotfix_IsUserIdValidUnderMaintenance;
        private static DelegateBridge __Hotfix_OnServerListButtonClick;
        private static DelegateBridge __Hotfix_OnServerListBgButtonClick;
        private static DelegateBridge __Hotfix_OnServerListItemClick;
        private static DelegateBridge __Hotfix_SortServerList;
        private static DelegateBridge __Hotfix_GetCachedSessionToken;
        private static DelegateBridge __Hotfix_ClearCachedSessionToken;
        private static DelegateBridge __Hotfix_SetCachedSessionToken;
        private static DelegateBridge __Hotfix_InitSessionTokenCache;
        private static DelegateBridge __Hotfix_FlushSessionTokenCache;
        private static DelegateBridge __Hotfix_get_SessionTokenCacheFilePath;

        [MethodImpl(0x8000)]
        public LoginUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckBundleDataVersion(Action<bool, int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearCachedSessionToken(string userId, string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnIntentChange(UIIntent newIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearServerListCache()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSessionTokenCache()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator DownloadGameServerLoginAnnouncementBeforeSDKLogin(bool loginSDk, Action<bool, bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator DownloadServerList(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void EnterGame()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator EnterGameImpl4IosReview()
        {
        }

        [MethodImpl(0x8000)]
        private void EnterGameImpl4Normal()
        {
        }

        [MethodImpl(0x8000)]
        protected void FixedTimeForGetServerInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void FlushSessionTokenCache()
        {
        }

        [MethodImpl(0x8000)]
        private SessionTokenCacheItem GetCachedSessionToken(string userId, string serverId)
        {
        }

        [MethodImpl(0x8000)]
        private StringTableId GetConvertErrorCode(string errorcode)
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetLastLoginedServerId()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetLastLoginUserId()
        {
        }

        [MethodImpl(0x8000)]
        protected DateTime GetLastSessionOutDateTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetLastSessionToken()
        {
        }

        [MethodImpl(0x8000)]
        private string GetLoginAgentUrlFromSdk()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetLoginAnnouncementUrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetNetworkErrorMsg()
        {
        }

        [MethodImpl(0x8000)]
        protected StringTableId GetPdSdkConvertErrorCode(string errorcode)
        {
        }

        [MethodImpl(0x8000)]
        private ServerInfo GetRecommedServerInfo()
        {
        }

        [MethodImpl(0x8000)]
        private ServerInfo GetServerInfoByServerId(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitAuthLoginServerInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitServerCtxByServerId(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        private SessionTokenCacheDataContainer InitSessionTokenCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCurrServerMaintenance()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedSelectServer()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSessionTokenValid(string sessionToken, DateTime outDateTime)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsUserIdValidUnderMaintenance(string userName)
        {
        }

        [MethodImpl(0x8000)]
        protected void LaunchCharacterUI(string intentMode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void LauncheMainUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void LaunchEnterGameUIWithGameSettingTokenAndServer()
        {
        }

        [MethodImpl(0x8000)]
        protected override void LaunchEnterGameUIWithServerListInEditor()
        {
        }

        [MethodImpl(0x8000)]
        protected override void LaunchEnterGameUIWithUIInputAccountAndServer()
        {
        }

        [MethodImpl(0x8000)]
        protected override void LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
        {
        }

        [MethodImpl(0x8000)]
        private void LoginWithNewUserId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void MakeSDKLogout()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeAccountButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChapterQuestCutsceneStartNtf(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCodeScanButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnConfigDataMd5InfoNtf(string fileNameWithErrorMd5, string localMd5, string serverMd5)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCreateCharacterEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnDownloadGameServerLoginAnnouncementEndBeforeSDKLogin(bool loginSDK, bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnDownloadServerListEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnDownloadServerListFailed()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterGameButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterWorldAck(WorldEnterReqNetTask task)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnGameServerConnected()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnGameServerNetworkError()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageApplyButtonClick(LanguageType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupCloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageGroupConfirmButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadAllResCompletedInLoginBGTask()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoginAnnoucementButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoginAnnoucementConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnLoginBySessionTokenAck(int ret)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogoAnimationFinished()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMovieButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPdSdkLoginEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPdSdkLoginOutEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPlayerInfoInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQRLoginEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSDKLoginEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSDKLogout(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendLogBtnClick(UIControllerBase controller)
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerListBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerListButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerListItemClick(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartRetryExecuteNetWork()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSwitchUserSuccessCallback()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnWaitingMsgAckOutTime()
        {
        }

        [MethodImpl(0x8000)]
        private bool ParseLoginAgentAck(string ackText, ref string status, ref string platformName, ref string userId, ref string token, ref string error)
        {
        }

        [MethodImpl(0x8000)]
        private void ParseServerInfoList(string serverInfoListContent)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayLoginUITaskBGM()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected bool PostPdSdkLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RecordUserLoginInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void RegistChapterQuestCutsceneEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterConfigDataInfoEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendEnterWorldReq()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendNecessaryReqToServer()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCachedSessionToken(string userId, string serverId, string sessionToken)
        {
        }

        [MethodImpl(0x8000)]
        private void SetClientSystemPushInfoSyncNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void SetLoginUICtrlMode()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPrivilegeUserList(string privilegeUserList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowEnterGameUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowGameServerLoginAnnouncementUI(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowLoadingState(bool isShow, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowNetworkErrorMsgBox(string msg, Action onConfirm)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowServerListUI()
        {
        }

        [MethodImpl(0x8000)]
        private int SortServerList(ServerInfo a, ServerInfo b)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartAuthLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartCreateCharacter()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartGameAuthLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartGameSessionLogin()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected bool StartLoginBGTask()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartPdSdkLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartPdSdkLogout()
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartPlayerInfoInitReq()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartQRLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        private void StartSDKLogin(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator StartSdkLoginAgentLogin(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartSessionLogin()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegistChapterQuestCutsceneEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterConfigDataInfoEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLanguageSelectedPanel()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator UpdateServerInfoList(Action<bool> onEnd, bool isClearOnFail = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private string CurrServerId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private string CurrGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override SceneLayerBase MainLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override bool IsNeedSdkRelogin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private string SessionTokenCacheFilePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckBundleDataVersion>c__AnonStoreyE
        {
            internal Action<bool, int> onEnd;
            internal LoginUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainCtrl.OutputDebugMsg("检查bundle版本号中...");
                ResourceManager.Instance.StartCheckBundleDataVersion(this.onEnd, false);
            }
        }

        [CompilerGenerated]
        private sealed class <DownloadGameServerLoginAnnouncement>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.EnableUIInput(false);
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.onEnd(false);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool, bool> onEnd;
            internal bool loginSDk;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__AnonStorey7 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__AnonStorey7();
                        this.$locvar0.<>f__ref$0 = this;
                        this.$locvar0.onEnd = this.onEnd;
                        this.$locvar0.loginSDk = this.loginSDk;
                        Debug.Log("LoginUITask：DownloadGameServerLoginAnnouncementBeforeSDKLogin");
                        this.$this.m_mainCtrl.OutputDebugMsg("sdk登录前下载公告...");
                        if (!this.$this.IsSDKLogin())
                        {
                            break;
                        }
                        this.$this.EnableUIInput(false);
                        this.$this.ShowLoadingState(true, false);
                        this.$locvar0.isDownLoadComplete = false;
                        this.$locvar0.isDownLoadSuccess = false;
                        if (ServerSettingManager.Instance.CheckServerSettingInfo(ServerSettingManager.Instance.LoginAnnouncementUrlKey))
                        {
                            this.$this.m_corutineHelper.StartCorutine(HttpUtil.DownloadFileFromHttp(this.$this.GetLoginAnnouncementUrl(), new Action<bool, WWW>(this.$locvar0.<>m__0), null, 3, 0x3a98));
                        }
                        else
                        {
                            this.$locvar0.isDownLoadSuccess = false;
                            Debug.Log("DownloadGameServerLoginAnnouncementBeforeSDKLogin CheckServerSettingInfo Fail");
                            goto TR_0006;
                        }
                        goto TR_000D;

                    case 1:
                        goto TR_000D;

                    default:
                        break;
                }
            TR_0000:
                return false;
            TR_0006:
                if (!this.$locvar0.isDownLoadSuccess)
                {
                    this.$this.EnableUIInput(true);
                    this.$this.ShowLoadingState(false, false);
                    MsgBoxUITask.ShowMsgBox(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_DownloadAnnouncementFailed, new object[0]), false, new Action(this.$locvar0.<>m__1));
                }
                else
                {
                    SDKHelper.SDKDebug("21", string.Empty);
                    this.$this.ShowLoadingState(false, false);
                    if (this.$locvar0.onEnd != null)
                    {
                        this.$locvar0.onEnd(this.$locvar0.loginSDk, this.$locvar0.isDownLoadSuccess);
                    }
                }
                this.$PC = -1;
                goto TR_0000;
            TR_000D:
                if (!this.$locvar0.isDownLoadComplete)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                Debug.Log($"DownloadGameServerLoginAnnouncementBeforeSDKLogin {this.$locvar0.isDownLoadSuccess}");
                goto TR_0006;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__AnonStorey7
            {
                internal bool isDownLoadComplete;
                internal bool isDownLoadSuccess;
                internal Action<bool, bool> onEnd;
                internal bool loginSDk;
                internal LoginUITask.<DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__Iterator0 <>f__ref$0;

                internal void <>m__0(bool res, WWW www)
                {
                    this.isDownLoadComplete = true;
                    this.isDownLoadSuccess = res;
                    this.<>f__ref$0.$this.m_loginAnnouncementStr = !this.isDownLoadSuccess ? StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_DownloadAnnouncementFailed, new object[0]) : www.text;
                }

                internal void <>m__1()
                {
                    this.<>f__ref$0.$this.EnableUIInput(false);
                    SDKHelper.SDKDebug("21", string.Empty);
                    if (this.onEnd != null)
                    {
                        this.onEnd(this.loginSDk, this.isDownLoadSuccess);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <DownloadServerList>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Debug.Log("LoginUITask:::DownloadServerList 下载服务器信息文本");
                        this.$this.m_mainCtrl.OutputDebugMsg("下载服务器列表中...");
                        this.$this.EnableUIInput(false);
                        this.$this.ShowLoadingState(true, false);
                        this.$current = this.$this.UpdateServerInfoList(this.onEnd, true);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <EnterGameImpl4IosReview>c__Iterator5 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal ProjectXGameManager <gameManager>__0;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            internal void <>m__0(bool res, int errCode)
            {
                if (errCode == -1)
                {
                    TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_LoginConnectServerError, new object[0]), true);
                    this.$this.EnableUIInput(true);
                    this.$this.ShowLoadingState(false, false);
                    this.$this.ShowEnterGameUI();
                }
                else if (!res)
                {
                    Debug.LogError("LoginUITask::CheckBundleDataVersion Fail");
                    this.$this.ShowLoadingState(false, false);
                    TopLayerMsgBoxUITask.ShowMsgBox(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_BundleVersionMisMatch, new object[0]), false, delegate {
                        if (Application.platform == RuntimePlatform.WindowsEditor)
                        {
                            this.$this.ShowEnterGameUI();
                        }
                        else
                        {
                            Application.Quit();
                        }
                    });
                }
                else
                {
                    string logStr = new JsonData { ["serverid"] = 0.ToString() }.ToJson();
                    SDKHelper.SDKDebug("25", logStr);
                    if (!this.$this.IsSDKLogin())
                    {
                        Debug.Log("LoginUITask::!IsSDKLogin()");
                        this.$this.ExecuteNetWorkOnNetReachable(new Action(this.$this.EnterGame));
                    }
                    else if ((this.$this.m_currServerInfo != null) && (this.$this.m_currServerInfo.IsServerVaild() || this.$this.IsUserIdValidUnderMaintenance(this.$this.m_gameUserId)))
                    {
                        Debug.Log("LoginUITask::OnEnterGameButtonClick 服务器状态正常 || UserId 正常");
                        this.$this.ExecuteNetWorkOnNetReachable(new Action(this.$this.EnterGame));
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_ServerMaintenance, new object[0]), false);
                        this.$this.ShowEnterGameUI();
                        Debug.Log($"LoginUITask::OnEnterGameButtonClick 服务器状态-{(this.$this.m_currServerInfo != null) ? this.$this.m_currServerInfo.m_serverState.ToString() : string.Empty}，UserId-{this.$this.m_gameUserId}");
                    }
                }
            }

            internal void <>m__1()
            {
                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    this.$this.ShowEnterGameUI();
                }
                else
                {
                    Application.Quit();
                }
            }

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.EnableUIInput(false);
                        this.$this.ShowLoadingState(true, false);
                        this.<gameManager>__0 = (ProjectXGameManager) GameManager.Instance;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (!this.<gameManager>__0.IsIosReviewResHandleComplete)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$this.CheckBundleDataVersion(new Action<bool, int>(this.<>m__0));
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <OnCreateCharacterEnd>c__AnonStoreyB
        {
            internal bool isSuccess;
            internal LoginUITask $this;

            internal void <>m__0(bool ret)
            {
                Debug.Log("LoginUITask::OnCreateCharacterEnd  resume end");
                if (this.isSuccess)
                {
                    this.$this.ShowLoadingState(true, true);
                    this.$this.StartPlayerInfoInitReq();
                }
                else
                {
                    this.$this.m_returnFromCreateCharacter = false;
                    this.$this.ClearGameServerLoginState();
                    this.$this.ShowEnterGameUI();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnServerListItemClick>c__AnonStorey10
        {
            internal string serverId;
            internal LoginUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.InitServerCtxByServerId(this.serverId);
                this.$this.ShowEnterGameUI();
            }
        }

        [CompilerGenerated]
        private sealed class <SendEnterWorldReq>c__AnonStoreyC
        {
            internal int loadedAssetCount;
            internal int reserveAssetTotalCount;
            internal Action onAssetLoaded;
            internal HashSet<string> reserveSet;
            internal LoginUITask.<SendEnterWorldReq>c__AnonStoreyD <>f__ref$13;

            internal void <>m__0()
            {
                this.loadedAssetCount++;
                float num = ((float) this.loadedAssetCount) / ((float) this.reserveAssetTotalCount);
                ScreenFadeInOutUITask.UpdateProcessValue(((num <= 1f) ? num : 1f) * 0.8f);
            }

            internal void <>m__1()
            {
                ResourceManager.Instance.EventOnAssetLoaded += this.onAssetLoaded;
                ResourceManager.Instance.StartReserveAssetsCorutine(this.reserveSet, new Dictionary<string, UnityEngine.Object>(), delegate {
                    ResourceManager.Instance.EventOnAssetLoaded -= this.onAssetLoaded;
                    this.<>f__ref$13.$this.OnEnterWorldAck(this.<>f__ref$13.returnTask);
                }, true, 0x708);
            }

            internal void <>m__2()
            {
                ResourceManager.Instance.EventOnAssetLoaded -= this.onAssetLoaded;
                this.<>f__ref$13.$this.OnEnterWorldAck(this.<>f__ref$13.returnTask);
            }
        }

        [CompilerGenerated]
        private sealed class <SendEnterWorldReq>c__AnonStoreyD
        {
            internal WorldEnterReqNetTask returnTask;
            internal LoginUITask $this;
        }

        [CompilerGenerated]
        private sealed class <StartLoginAgentLogin>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string <msg>__0;
            internal Action<int, string, string> onEnd;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <StartLoginAgentLogin>c__AnonStorey8 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <StartLoginAgentLogin>c__AnonStorey8();
                        this.$locvar0.<>f__ref$3 = this;
                        Debug.Log("LoginUITask::StartLoginAgentLogin");
                        this.$this.m_loginTaskState = LoginUITaskBase.LoginState.StartLoginAgentLogin;
                        this.$this.m_mainCtrl.OutputDebugMsg("EnterGame-LoginAgent中...");
                        this.<msg>__0 = StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_StartConnectGameServer, new object[0]);
                        this.$this.m_mainCtrl.UpdateLoginMessage(this.<msg>__0);
                        this.$locvar0.ret = null;
                        if (this.$this.IsSDKLogin())
                        {
                            this.$current = this.$this.StartSdkLoginAgentLogin(new Action<bool>(this.$locvar0.<>m__0));
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            goto TR_0001;
                        }
                        goto TR_000B;

                    case 1:
                        if (this.$locvar0.ret.GetValueOrDefault() || (this.$locvar0.ret == null))
                        {
                            this.$this.m_gameUserId = this.$this.m_loginAgentReturnGameUserId;
                            this.$this.m_authtoken = this.$this.m_loginAgentReturnAuthToken;
                            Debug.Log($"LoginUITask::OnLoginAgentLoginSuccess userId:{this.$this.m_gameUserId}, token:{this.$this.m_authtoken}");
                            goto TR_000B;
                        }
                        else
                        {
                            this.$this.ClearGameServerLoginState();
                            this.onEnd(-1, string.Empty, string.Empty);
                        }
                        break;

                    case 2:
                        this.onEnd(0, this.$this.m_gameUserId, this.$this.m_authtoken);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            TR_0001:
                return true;
            TR_000B:
                if (PlayerPrefs.HasKey(LoginUITask.m_prefKeyForAccountName))
                {
                    string str = PlayerPrefs.GetString(LoginUITask.m_prefKeyForAccountName);
                    if (str != this.$this.m_gameUserId)
                    {
                        Debug.Log($"LoginUITask::sdk登录过程中切换账号 preUserId = {str} : m_gameUserId = {this.$this.m_gameUserId} ");
                        this.$this.m_hasUserSettingChanged = true;
                    }
                }
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 2;
                }
                goto TR_0001;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <StartLoginAgentLogin>c__AnonStorey8
            {
                internal bool? ret;
                internal LoginUITask.<StartLoginAgentLogin>c__Iterator3 <>f__ref$3;

                internal void <>m__0(bool lret)
                {
                    this.ret = new bool?(lret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartPlayerInfoInitReq>c__AnonStoreyA
        {
            internal ProjectXPlayerContext playerContext;
            internal LoginUITask $this;

            internal void <>m__0(Task task)
            {
                PlayerInfoInitReqNetTask task2 = task as PlayerInfoInitReqNetTask;
                if (task2.IsNetworkError)
                {
                    Debug.Log("LoginUITask::StartPlayerInfoInitReq returnTask.IsNetworkError");
                    this.$this.ClearGameServerLoginState();
                    this.$this.ShowEnterGameUI();
                }
                else
                {
                    PlayerInfoInitReqNetTask.InitReqResultState initReqState = task2.InitReqState;
                    if (initReqState == PlayerInfoInitReqNetTask.InitReqResultState.CreateCharactor)
                    {
                        this.$this.StartCreateCharacter();
                    }
                    else if (initReqState == PlayerInfoInitReqNetTask.InitReqResultState.InitEndNtf)
                    {
                        this.$this.OnPlayerInfoInitEnd();
                        PlayerPrefs.SetInt("SavedGrandFaction", (int) this.playerContext.m_motherLand);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartSdkLoginAgentLogin>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string <loginAgentUrl>__0;
            internal Action<bool> onEnd;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <StartSdkLoginAgentLogin>c__AnonStorey9 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <StartSdkLoginAgentLogin>c__AnonStorey9();
                        this.$locvar0.<>f__ref$4 = this;
                        this.$this.m_lastErrorMessage = string.Empty;
                        this.<loginAgentUrl>__0 = this.$this.GetLoginAgentUrlFromSdk();
                        Debug.Log($"LoginUITask::StartSdkLoginAgentLogin url: {this.<loginAgentUrl>__0}");
                        this.$locvar0.www = null;
                        try
                        {
                            this.$locvar0.www = new WWW(this.<loginAgentUrl>__0);
                            this.$locvar0.timeoutTime = Time.unscaledTime + 20f;
                            this.$locvar0.isTimeout = false;
                            this.$current = CommonUIUtil.WaitUntil(new Func<bool>(this.$locvar0.<>m__0));
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            return true;
                        }
                        catch (Exception exception)
                        {
                            Debug.LogError($"{"LoginUITask.StartSdkLoginAgentLogin"} : {exception}");
                            this.$this.m_lastErrorMessage = StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_LoginConnectServerError, new object[0]);
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        break;

                    case 1:
                        if (this.$locvar0.isTimeout)
                        {
                            Debug.LogError($"LoginUITask::StartSdkLoginAgentLogin Failure : {this.<loginAgentUrl>__0} out off time {20f}");
                        }
                        else if (!string.IsNullOrEmpty(this.$locvar0.www.error))
                        {
                            object[] paramList = new object[] { "LoginUITask::StartSdkLoginAgentLogin Failure www error : {0} {1}", this.<loginAgentUrl>__0, this.$locvar0.www.error };
                            Debug.LogError(paramList);
                        }
                        else
                        {
                            Debug.Log($"LoginUITask::StartSdkLoginAgentLogin Success : {this.<loginAgentUrl>__0} data : {this.$locvar0.www.text}");
                            string status = string.Empty;
                            string platformName = string.Empty;
                            string userId = string.Empty;
                            string token = string.Empty;
                            string error = string.Empty;
                            if (!this.$this.ParseLoginAgentAck(this.$locvar0.www.text, ref status, ref platformName, ref userId, ref token, ref error))
                            {
                                Debug.LogError("LoginUITask::StartSdkLoginAgentLogin ParseLoginAgentAck ERROR!!!");
                            }
                            else
                            {
                                if (status.Equals("ok"))
                                {
                                    Debug.Log($"LoginUITask::StartSdkLoginAgentLogin Successed
platformName={platformName} userID={userId}
token={token} error={error}-");
                                    this.$this.m_loginAgentReturnAuthToken = token;
                                    this.$this.m_loginAgentReturnGameUserId = userId;
                                    if (this.onEnd != null)
                                    {
                                        this.onEnd(true);
                                    }
                                    break;
                                }
                                StringTableId convertErrorCode = this.$this.GetConvertErrorCode(error);
                                this.$this.m_lastErrorMessage = StringFormatUtil.GetStringInStringTableWithId(convertErrorCode, new object[0]);
                                Debug.LogError($"LoginUITask::StartSdkLoginAgentLogin http ERROR org = {error} Type = {convertErrorCode}");
                                if (error == "-1")
                                {
                                    this.$this.IsNeedSdkRelogin = true;
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(this.$this.m_lastErrorMessage))
                        {
                            this.$this.m_lastErrorMessage = StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_LoginConnectServerError, new object[0]);
                        }
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <StartSdkLoginAgentLogin>c__AnonStorey9
            {
                internal float timeoutTime;
                internal WWW www;
                internal bool isTimeout;
                internal LoginUITask.<StartSdkLoginAgentLogin>c__Iterator4 <>f__ref$4;

                internal bool <>m__0()
                {
                    if (Time.unscaledTime <= this.timeoutTime)
                    {
                        return this.www.isDone;
                    }
                    this.www.Dispose();
                    this.isTimeout = true;
                    return true;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateServerInfoList>c__Iterator6 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal bool isClearOnFail;
            internal LoginUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <UpdateServerInfoList>c__AnonStoreyF $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <UpdateServerInfoList>c__AnonStoreyF();
                        this.$locvar0.<>f__ref$6 = this;
                        if (!ServerSettingManager.Instance.CheckServerSettingInfo(ServerSettingManager.Instance.ServerListUrlKey))
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                            break;
                        }
                        this.$locvar0.serverListContent = string.Empty;
                        this.$locvar0.url = ServerSettingManager.Instance.GetServerSettingInfoValue(ServerSettingManager.Instance.ServerListUrlKey);
                        Debug.Log("LoginUITask::Start Download ServerInfoList from: " + this.$locvar0.url);
                        this.$current = HttpUtil.DownloadFileFromHttp(this.$locvar0.url, new Action<bool, WWW>(this.$locvar0.<>m__0), null, 3, 0x3a98);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (!string.IsNullOrEmpty(this.$locvar0.serverListContent))
                        {
                            this.$this.ParseServerInfoList(this.$locvar0.serverListContent);
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                            this.$PC = -1;
                        }
                        else
                        {
                            if (this.isClearOnFail)
                            {
                                this.$this.ClearServerListCache();
                            }
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <UpdateServerInfoList>c__AnonStoreyF
            {
                internal string serverListContent;
                internal string url;
                internal LoginUITask.<UpdateServerInfoList>c__Iterator6 <>f__ref$6;

                internal void <>m__0(bool res, WWW www)
                {
                    if (res)
                    {
                        this.serverListContent = www.text;
                    }
                    Debug.Log($"LoginUITask:: Download ServerInfoList from: {this.url} ret: {res}");
                }
            }
        }

        [Serializable]
        public class SessionTokenCacheData
        {
            private Dictionary<string, LoginUITask.SessionTokenCacheItem> m_serverSessionTokenDict = new Dictionary<string, LoginUITask.SessionTokenCacheItem>();
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCacheSessionToken;
            private static DelegateBridge __Hotfix_ClearCachedSessionToken;
            private static DelegateBridge __Hotfix_SetCachedSessionToken;

            public SessionTokenCacheData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void ClearCachedSessionToken(string serverId)
            {
                DelegateBridge bridge = __Hotfix_ClearCachedSessionToken;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, serverId);
                }
                else if (this.m_serverSessionTokenDict.ContainsKey(serverId))
                {
                    this.m_serverSessionTokenDict.Remove(serverId);
                }
            }

            public LoginUITask.SessionTokenCacheItem GetCacheSessionToken(string serverId)
            {
                LoginUITask.SessionTokenCacheItem item;
                DelegateBridge bridge = __Hotfix_GetCacheSessionToken;
                return ((bridge == null) ? (!this.m_serverSessionTokenDict.TryGetValue(serverId, out item) ? null : item) : bridge.__Gen_Delegate_Imp4995(this, serverId));
            }

            public void SetCachedSessionToken(string serverId, string sessionToken)
            {
                DelegateBridge bridge = __Hotfix_SetCachedSessionToken;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, serverId, sessionToken);
                }
                else
                {
                    LoginUITask.SessionTokenCacheItem item;
                    if (!this.m_serverSessionTokenDict.TryGetValue(serverId, out item))
                    {
                        item = new LoginUITask.SessionTokenCacheItem();
                        this.m_serverSessionTokenDict[serverId] = item;
                    }
                    item.m_sessionToken = sessionToken;
                    SessionToken sessionObject = SessionToken.GetSessionObject(sessionToken);
                    item.m_outDateTime = DateTime.Now.AddMilliseconds((sessionObject == null) ? ((double) 0x3e8) : ((double) sessionObject.TokenTimeoutTime));
                }
            }
        }

        [Serializable]
        public class SessionTokenCacheDataContainer
        {
            private Dictionary<string, LoginUITask.SessionTokenCacheData> m_userSessionTokenDict;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCacheSessionToken;
            private static DelegateBridge __Hotfix_ClearCachedSessionToken;
            private static DelegateBridge __Hotfix_SetCachedSessionToken;

            [MethodImpl(0x8000)]
            public void ClearCachedSessionToken(string userId, string serverId)
            {
            }

            [MethodImpl(0x8000)]
            public LoginUITask.SessionTokenCacheItem GetCacheSessionToken(string userId, string serverId)
            {
            }

            [MethodImpl(0x8000)]
            public void SetCachedSessionToken(string userId, string serverId, string sessionToken)
            {
            }
        }

        [Serializable]
        public class SessionTokenCacheItem
        {
            public string m_sessionToken;
            public DateTime m_outDateTime;
            private static DelegateBridge _c__Hotfix_ctor;

            public SessionTokenCacheItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

