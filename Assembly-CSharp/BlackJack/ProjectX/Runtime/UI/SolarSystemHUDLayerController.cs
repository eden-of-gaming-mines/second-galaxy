﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemHUDLayerController : PrefabControllerBase
    {
        protected Rect? m_currCanvasRect;
        protected string m_damageCountAssetName;
        protected Dictionary<uint, SolarSystemUIDamageCountController> m_damageCountUsedDic;
        protected LinkedList<KeyValuePair<float, SolarSystemUIDamageCountController>> m_unusedDamageCountUIList;
        protected LinkedList<KeyValuePair<float, SolarSystemUIDamageCountController>> m_waitForRecoveryDamageCountUIList;
        protected string m_targetTagAssetName;
        protected Dictionary<uint, SolarSystemTargetTagController> m_targetTagDic;
        protected Dictionary<uint, SolarSystemTargetTagController> m_staticTargetTagDic;
        protected Dictionary<uint, SolarSystemTargetTagController> m_globalSceneTargetTagDic;
        protected string m_touchEffectAssetName;
        protected List<GameObject> m_touchEffectObjectList;
        protected List<GameObject> m_disableTouchEffectObjectList;
        protected string m_selfShipTacticalViewTagAssetName;
        protected GameObject m_selfShipTacticalViewTagObj;
        protected string m_dropBoxTargetTagAssetName;
        protected Dictionary<uint, SolarSystemDropBoxTargetTagUIController> m_dropBoxTargetTagDic;
        private Action<GameObject> m_freeTagItemFunc;
        private Func<string, GameObject> m_allocTagItemFunc;
        public const string PoolNameNormalTargetTag = "NormalTargetTag";
        public const string PoolNameNormalWithNameTargetTag = "NormalWithNameTargetTag";
        public const string PoolNameLockedTargetTag = "LockedTargetTag";
        public const string PoolNameSelectedTargetTag = "SelectedTargetTag";
        public const string PoolNameSolarSystemTargetTag = "SolarSystemTargetTag";
        public const string PoolNameDropItemTargetTag = "DropItemTargetTag";
        private const float TargetTagClearTime = 60f;
        private const float PoolCheckIntervalTime = 3f;
        private float m_nextCheckTime;
        private const int FrameCreateMaxHudCount = 5;
        private int m_frameCreateHudCount;
        private readonly Dictionary<string, float> m_lastCreatePoolObjectDictionary;
        [AutoBind("./DamageCountUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_damageCountUIRoot;
        [AutoBind("./TargetTagRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_targetTagRoot;
        [AutoBind("./SelfShipRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfShipRoot;
        [AutoBind("./TouchEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_touchEffectRoot;
        [AutoBind("./DropItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_dropItemRoot;
        [AutoBind("./HUD_Pool", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_HUD_Pool;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetDamageUICtrlToFree;
        private static DelegateBridge __Hotfix_UpdateDamageCountCtrlLocation;
        private static DelegateBridge __Hotfix_AddDamageCountText;
        private static DelegateBridge __Hotfix_GetSolarSystemUIDamageCountController;
        private static DelegateBridge __Hotfix_AllocSolarSystemUIDamageCountController;
        private static DelegateBridge __Hotfix_get_IsAllTagsVisible;
        private static DelegateBridge __Hotfix_set_IsAllTagsVisible;
        private static DelegateBridge __Hotfix_ReducePoolCapacity;
        private static DelegateBridge __Hotfix_ClearPool;
        private static DelegateBridge __Hotfix_UpdateSolarSystemConfigObjects;
        private static DelegateBridge __Hotfix_UpdateNormalTargetTag;
        private static DelegateBridge __Hotfix_UpdateNormalWithNameTargetTag;
        private static DelegateBridge __Hotfix_UpdateLockedTargetTag;
        private static DelegateBridge __Hotfix_UpdateSelectedTargetTag;
        private static DelegateBridge __Hotfix_UpdateSelfShipTacticalViewTag;
        private static DelegateBridge __Hotfix_UpdateDropBoxTag;
        private static DelegateBridge __Hotfix_RemoveTargetTag;
        private static DelegateBridge __Hotfix_RemoveTGlobalSceneargetTag;
        private static DelegateBridge __Hotfix_GetSolarSystemTagController;
        private static DelegateBridge __Hotfix_GetStaticSolarSystemTagController;
        private static DelegateBridge __Hotfix_GetGlobalSceneSolarSystemTagController;
        private static DelegateBridge __Hotfix_GetDropBoxTagController;
        private static DelegateBridge __Hotfix_AllocSolarSystemTagController;
        private static DelegateBridge __Hotfix_AllocSelfShipTacticalViewTag;
        private static DelegateBridge __Hotfix_AllocDropBoxTargetTag;
        private static DelegateBridge __Hotfix_ShowClickEffect;
        private static DelegateBridge __Hotfix_EnableClickEffect;
        private static DelegateBridge __Hotfix_GetTouchEffectObject;
        private static DelegateBridge __Hotfix_AllocTouchEffectObject;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_GetUIPos;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_GetCurrentCanvas;
        private static DelegateBridge __Hotfix_get_CurrentCanvasRect;

        [MethodImpl(0x8000)]
        public void AddDamageCountText(uint id, LBSyncEventOnHitInfo hitInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemDropBoxTargetTagUIController AllocDropBoxTargetTag()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject AllocSelfShipTacticalViewTag()
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemTargetTagController AllocSolarSystemTagController(string tgtName)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemUIDamageCountController AllocSolarSystemUIDamageCountController(uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject AllocTouchEffectObject()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearPool()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableClickEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected Canvas GetCurrentCanvas()
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemDropBoxTargetTagUIController GetDropBoxTagController(uint tgtId)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemTargetTagController GetGlobalSceneSolarSystemTagController(uint tgtId, string tgtName)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemTargetTagController GetSolarSystemTagController(uint tgtId, string tgtName)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemUIDamageCountController GetSolarSystemUIDamageCountController(uint id)
        {
        }

        [MethodImpl(0x8000)]
        protected SolarSystemTargetTagController GetStaticSolarSystemTagController(uint tgtId, string tgtName)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetTouchEffectObject()
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 GetUIPos(Vector3 objPos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void ReducePoolCapacity(string poolName, float clearTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveTargetTag(uint tgtId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveTGlobalSceneargetTag(uint tgtId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDamageUICtrlToFree(uint id)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowClickEffect(Vector3 scrPos)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDamageCountCtrlLocation(uint id, Vector3 goVPPos, bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDropBoxTag(ClientSpaceDropBox dropBox, Vector3 goVpPos, bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLockedTargetTag(uint tgtId, string tgtName, Vector3 goVpPos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, string targetNoticeState, string targetCrimeState, double dist, float sheildPerc, float armorPerc, bool isVisible, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNormalTargetTag(uint tgtId, string tgtName, Vector3 goVpPos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, string targetNoticeState, string targetCrimeState, bool isVisible, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNormalWithNameTargetTag(uint tgtId, string tgtName, Vector3 goVpPos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, string targetNoticeState, string targetCrimeState, double dist, bool isVisible, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedTargetTag(uint tgtId, string tgtName, Vector3 goVpPos, SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, string targetNoticeState, string targetCrimeState, double dist, float sheildPerc, float armorPerc, bool isVisible, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelfShipTacticalViewTag(Vector3 goVpPos, bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemConfigObjects(bool isVisible, uint id, SpaceObjectType spcaObjectType, Vector3 pos, string name, bool isSelected, double distance)
        {
        }

        public bool IsAllTagsVisible
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        protected Rect CurrentCanvasRect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ShowClickEffect>c__AnonStorey0
        {
            internal EffectPlayManager emg;
            internal SolarSystemHUDLayerController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

