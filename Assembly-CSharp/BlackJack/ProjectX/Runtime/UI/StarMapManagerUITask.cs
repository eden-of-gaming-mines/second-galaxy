﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarMapManagerUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        protected StarMapForStarfieldUITask m_starMapForStarfieldUITask;
        protected StarMapForSolarSystemUITask m_starMapForSolarSystemUITask;
        protected StarMapForGalaxyUITask m_starMapForGalaxyUITask;
        protected StarMapForGuildUITask m_starMapForGuildUITask;
        protected static UIIntent m_lastActiveStarMapTaskUIIntent;
        protected static bool m_useDefaultOrientedSolarSystemIdForStarMapForStarField = true;
        protected const string ParamKeyStarMapForStarfieldUITask = "StarMapForStarfieldUITask";
        protected const string ParamKeyStarMapForSolarSystemUITask = "StarMapForSolarSystemUITask";
        protected const string ParamKeyStarMapForGalaxyUITask = "StarMapForGalaxyUITask";
        protected const string ParamKeyStarMapForGuildUITask = "StarMapForGuildUITask";
        protected const string ParamKeyReturnToBasicState = "ReturnToBasicState";
        protected const string ParamKeyIntentForRealTask = "IntentForRealTask";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "StarMapManagerUITask";
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartStarMapUITask;
        private static DelegateBridge __Hotfix_StartStarMapForSolarSystemUITask;
        private static DelegateBridge __Hotfix_StartStarMapForStarfieldUITask;
        private static DelegateBridge __Hotfix_StartStarMapForStarfieldUITaskForTradeShip;
        private static DelegateBridge __Hotfix_StartStarMapForGalaxyUITask;
        private static DelegateBridge __Hotfix_StartStarMapForGuildUITask;
        private static DelegateBridge __Hotfix_StartWormHoleRepeatableGuide;
        private static DelegateBridge __Hotfix_StartStarMapForWormHoleRepeatableGuide;
        private static DelegateBridge __Hotfix_StartInfectMissonRepeatableGuide;
        private static DelegateBridge __Hotfix_StartStarMapForInfectRepeatableGuide;
        private static DelegateBridge __Hotfix_StartFreeSingnalQuestGuide;
        private static DelegateBridge __Hotfix_StartDelegateMineralGuide;
        private static DelegateBridge __Hotfix_StartStarMapUITaskWithDelegateFightGuide;
        private static DelegateBridge __Hotfix_StartInvadeSingnalGuide;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_StartStarMapUITaskFromOutInput;
        private static DelegateBridge __Hotfix_ReturnStarMapUITaskFromIntent;
        private static DelegateBridge __Hotfix_StartStarMapUITaskFromLastTask;
        private static DelegateBridge __Hotfix_IsCurrStarMapIsSolarSystem;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_GetOrientedSolarSyetemIdForStarMapForStarFiled;
        private static DelegateBridge __Hotfix_GetLastActiveStarMapTaskUIIntent;
        private static DelegateBridge __Hotfix_SetCurrStarMapForStarfieldUITask;
        private static DelegateBridge __Hotfix_SetCurrStarMapForSolarSystemUITask;
        private static DelegateBridge __Hotfix_SetCurrStarMapForGalaxyUITask;
        private static DelegateBridge __Hotfix_SetCurrStarMapForGuildUITask;
        private static DelegateBridge __Hotfix_BackToMainUITask;
        private static DelegateBridge __Hotfix_OnBackToMainUITask;
        private static DelegateBridge __Hotfix_ReturnToBasicUITaskWhenRequireDataFailed;
        private static DelegateBridge __Hotfix_CreateStarMapUIBackgroundManager;
        private static DelegateBridge __Hotfix_RegisterStarMapForSolarSystemUITaskEvent;
        private static DelegateBridge __Hotfix_UnRegisterStarMapForSolarSystemUITaskEvent;
        private static DelegateBridge __Hotfix_OnStarMapForSolarSystemUITaskStop;
        private static DelegateBridge __Hotfix_OnBacktoStarfiledUITaskFromSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStrikeForQuestFromSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStrikeForDelegateSignalFromSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStrikeForPVPSignalFromSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStrikeForRescueFromSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStrikeForNormalNavigationInStation;
        private static DelegateBridge __Hotfix_OnGuildSentrySignalStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnStrikeForAnotherSolarSysemWormholeInStation;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoPanelClick;
        private static DelegateBridge __Hotfix_OnStartRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnStrikrForSpaceSignal;
        private static DelegateBridge __Hotfix_RegisterStarMapForStarfieldUITaskEvent;
        private static DelegateBridge __Hotfix_UnRegisterStarMapForStarfieldUITaskEvent;
        private static DelegateBridge __Hotfix_OnStarMapForStarfieldUITaskStop;
        private static DelegateBridge __Hotfix_OnStarMapForStarfieldUITaskBacktoGalaxyUITask;
        private static DelegateBridge __Hotfix_OnStarMapForStarfieldUITaskBacktoSolarSystemUITask;
        private static DelegateBridge __Hotfix_OnStarMapForStarfieldUITaskSendToChatButtonClick;
        private static DelegateBridge __Hotfix_RegisterStarMapForGalaxyUITaskEvent;
        private static DelegateBridge __Hotfix_UnRegisterStarMapForGalaxyUITaskEvent;
        private static DelegateBridge __Hotfix_OnStarMapForGalaxyUITaskStop;
        private static DelegateBridge __Hotfix_OnBacktoStarfieldUITask;
        private static DelegateBridge __Hotfix_RegisterStarMapForGuildUITaskEvent;
        private static DelegateBridge __Hotfix_UnRegisterStarMapForGuildUITaskEvent;
        private static DelegateBridge __Hotfix_OnStarMapForGuildUITaskStop;
        private static DelegateBridge __Hotfix_OnBacktoStarfiledUITaskFromGuildUITask;
        private static DelegateBridge __Hotfix_OnOpenGuildSovereignBattleUITask;
        private static DelegateBridge __Hotfix_OnOpenGuildAction;
        private static DelegateBridge __Hotfix_OnGotoGuildProduceUITask;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public StarMapManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BackToMainUITask()
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private StarMapUIBackgroundManager CreateStarMapUIBackgroundManager()
        {
        }

        [MethodImpl(0x8000)]
        protected UIIntent GetLastActiveStarMapTaskUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        private int GetOrientedSolarSyetemIdForStarMapForStarFiled(int solarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCurrStarMapIsSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackToMainUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBacktoStarfieldUITask(GDBStarfieldsInfo starfieldInfo, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBacktoStarfiledUITaskFromGuildUITask(int orientedSSId, float fadeInEffectTimeLength = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBacktoStarfiledUITaskFromSolarSystemUITask(int orientedSSId, int questInstanceId, float fadeInEffectTimeLength = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBaseRedeployInfoPanelClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGotoGuildProduceUITask(ulong buildingInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentrySignalStrikeButtonClick(GuildSentryInterestScene guildSentryInterestSceneuildSentry)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOpenGuildAction()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOpenGuildSovereignBattleUITask(int solarSystemId, GuildSolarSystemInfoClient guildSolarSystemInfo, Action closePanelCallBack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForGalaxyUITaskStop(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForGuildUITaskStop(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForSolarSystemUITaskStop(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForStarfieldUITaskBacktoGalaxyUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForStarfieldUITaskBacktoSolarSystemUITask(string mode, int solarSystemId, StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent, StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent, float fadeInTimeLength, bool fromGuild, string defaultSelectedTab)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForStarfieldUITaskSendToChatButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapForStarfieldUITaskStop(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStartRedeployButtonClick(GDBSolarSystemInfo solarSystemInfo, int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForAnotherSolarSysemWormholeInStation(int solarSystemId, ConfigDataWormholeStarGroupInfo wormholeInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForDelegateSignalFromSolarSystemUITask(ulong signalInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForNormalNavigationInStation(NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForPVPSignalFromSolarSystemUITask(ulong signalInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForQuestFromSolarSystemUITask(ulong questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikeForRescueFromSolarSystemUITask(LBPVPInvadeRescue lbRescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStrikrForSpaceSignal(SignalInfo signal)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterStarMapForGalaxyUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterStarMapForGuildUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterStarMapForSolarSystemUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterStarMapForStarfieldUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected bool ReturnStarMapUITaskFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private static void ReturnToBasicUITaskWhenRequireDataFailed()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrStarMapForGalaxyUITask(StarMapForGalaxyUITask galaxyUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrStarMapForGuildUITask(StarMapForGuildUITask starMapForGuildUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrStarMapForSolarSystemUITask(StarMapForSolarSystemUITask solarSystemUITask)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrStarMapForStarfieldUITask(StarMapForStarfieldUITask starfieldUITask, bool recordAsLastTask)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartDelegateMineralGuide(UIIntent returnToIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartFreeSingnalQuestGuide(UIIntent returnToIntent, QuestType questType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartInfectMissonRepeatableGuide(UIIntent returnToIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartInvadeSingnalGuide(UIIntent returnToIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForGalaxyUITask(string mode, UIIntent mainUIIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForGuildUITask(UIIntent mainUIIntent, int solarSystemId, bool forceUseDefaultSolarSystem = false, StarMapForGuildUITask.SubTaskPanel subPanel = 0, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartStarMapForInfectRepeatableGuide(UIIntent returnToIntent, int currStarfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForSolarSystemUITask(string mode, int solarSystemId, UIIntent mainUIIntent, string defaultSelectedTab = null, StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent = null, StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent = null, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForStarfieldUITask(UIIntent mainUIIntent, int orientedSolarSystemId = 0, int navigationStartSSId = 0, int navigationEndSSId = 0, GEBrushType brushType = 2, Action onPreparedForStarUITask = null, int selQuestInstanceId = 0, bool isGuild = false, StarMapForStarfieldUITask.RepeatableUserGuideIntent repeatedUserGuideIntent = null, bool returnToBasicState = false, int showTunnelType = 0, ulong tunnelInsId = 0UL, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapForStarfieldUITaskForTradeShip(UIIntent mainUIIntent, int orientedSolarSystemId = 0, ulong tradeId = 0UL, Action onPreparedForStarUITask = null, bool returnToBasicState = false, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartStarMapForWormHoleRepeatableGuide(UIIntent returnToIntent, int currStarfieldId, bool checkRecommendLevel = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapUITask(UIIntent mainUIIntent, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartStarMapUITaskFromLastTask()
        {
        }

        [MethodImpl(0x8000)]
        protected bool StartStarMapUITaskFromOutInput(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartStarMapUITaskWithDelegateFightGuide(UIIntent returnToIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartWormHoleRepeatableGuide(UIIntent returnToIntent, bool checkRecommendLevel = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterStarMapForGalaxyUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterStarMapForGuildUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterStarMapForSolarSystemUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterStarMapForStarfieldUITaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnOpenGuildAction>c__AnonStorey6
        {
            internal StarMapForGuildUITask starMapTask;
            internal StarMapManagerUITask $this;

            internal void <>m__0(bool starRet)
            {
                if (starRet)
                {
                    GuildActionUITask.StartTaskWithPrepare("ModeInStarMap", this.$this.CurrentIntent, this.starMapTask.SolarSystemID, new Action(this.starMapTask.Pause), null, this.$this.CreateStarMapUIBackgroundManager());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartStarMapForGuildUITask>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal UIIntent mainUIIntent;
            internal StarMapForGuildUITask.SubTaskPanel subPanel;

            internal void <>m__0(bool ret)
            {
                <StartStarMapForGuildUITask>c__AnonStorey4 storey = new <StartStarMapForGuildUITask>c__AnonStorey4 {
                    <>f__ref$3 = this
                };
                if (!ret && (this.onEnd != null))
                {
                    this.onEnd(false);
                }
                else
                {
                    storey.starMapForGuildTask = UIManager.Instance.FindUITaskWithName("StarMapForGuildUITask", true);
                    storey.uiIntent = new UIIntentReturnable(this.mainUIIntent, "StarMapManagerUITask", null);
                    storey.uiIntent.SetParam("StarMapForGuildUITask", storey.starMapForGuildTask);
                    UIManager.Instance.StartUITask(storey.uiIntent, true, false, null, new Action<bool>(storey.<>m__0));
                }
            }

            private sealed class <StartStarMapForGuildUITask>c__AnonStorey4
            {
                internal UITaskBase starMapForGuildTask;
                internal UIIntentReturnable uiIntent;
                internal StarMapManagerUITask.<StartStarMapForGuildUITask>c__AnonStorey3 <>f__ref$3;

                internal void <>m__0(bool lret)
                {
                    <StartStarMapForGuildUITask>c__AnonStorey5 storey = new <StartStarMapForGuildUITask>c__AnonStorey5 {
                        <>f__ref$3 = this.<>f__ref$3,
                        <>f__ref$4 = this
                    };
                    if (this.<>f__ref$3.subPanel == StarMapForGuildUITask.SubTaskPanel.Action)
                    {
                        storey.starMapTask = this.starMapForGuildTask as StarMapForGuildUITask;
                        storey.starMapTask.HideMainPanel(new Action<bool>(storey.<>m__0));
                    }
                    else if (this.<>f__ref$3.onEnd != null)
                    {
                        this.<>f__ref$3.onEnd(true);
                    }
                }

                private sealed class <StartStarMapForGuildUITask>c__AnonStorey5
                {
                    internal StarMapForGuildUITask starMapTask;
                    internal StarMapManagerUITask.<StartStarMapForGuildUITask>c__AnonStorey3 <>f__ref$3;
                    internal StarMapManagerUITask.<StartStarMapForGuildUITask>c__AnonStorey3.<StartStarMapForGuildUITask>c__AnonStorey4 <>f__ref$4;

                    internal void <>m__0(bool starRet)
                    {
                        if (starRet)
                        {
                            GuildActionUITask.StartTaskWithPrepare("ModeInStarMap", this.<>f__ref$4.uiIntent, this.starMapTask.SolarSystemID, new Action(this.starMapTask.Pause), delegate (bool actionRet) {
                                if (this.<>f__ref$3.onEnd != null)
                                {
                                    this.<>f__ref$3.onEnd(actionRet);
                                }
                            }, (UIManager.Instance.FindUITaskWithName("StarMapManagerUITask", true) as StarMapManagerUITask).CreateStarMapUIBackgroundManager());
                        }
                    }

                    internal void <>m__1(bool actionRet)
                    {
                        if (this.<>f__ref$3.onEnd != null)
                        {
                            this.<>f__ref$3.onEnd(actionRet);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartStarMapForSolarSystemUITask>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartStarMapForStarfieldUITask>c__AnonStorey1
        {
            internal Action onPreparedForStarUITask;
            internal UIIntent mainUIIntent;
            internal bool returnToBasicState;
            internal Action<bool> onPipeLineEnd;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    StarMapManagerUITask.ReturnToBasicUITaskWhenRequireDataFailed();
                }
                if (this.onPreparedForStarUITask != null)
                {
                    this.onPreparedForStarUITask();
                }
            }

            internal void <>m__1(bool res)
            {
                UIIntentReturnable intent = new UIIntentReturnable(this.mainUIIntent, "StarMapManagerUITask", null);
                intent.SetParam("ReturnToBasicState", this.returnToBasicState);
                intent.SetParam("StarMapForStarfieldUITask", UIManager.Instance.FindUITaskWithName("StarMapForStarfieldUITask", true));
                Action<bool> onPipeLineEnd = this.onPipeLineEnd;
                UIManager.Instance.StartUITask(intent, true, false, null, onPipeLineEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <StartStarMapForStarfieldUITaskForTradeShip>c__AnonStorey2
        {
            internal Action onPreparedForStarUITask;
            internal UIIntent mainUIIntent;
            internal bool returnToBasicState;
            internal Action<bool> onPipeLineEnd;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    StarMapManagerUITask.ReturnToBasicUITaskWhenRequireDataFailed();
                }
                if (this.onPreparedForStarUITask != null)
                {
                    this.onPreparedForStarUITask();
                }
            }

            internal void <>m__1(bool res)
            {
                UIIntentReturnable intent = new UIIntentReturnable(this.mainUIIntent, "StarMapManagerUITask", null);
                intent.SetParam("ReturnToBasicState", this.returnToBasicState);
                intent.SetParam("StarMapForStarfieldUITask", UIManager.Instance.FindUITaskWithName("StarMapForStarfieldUITask", true));
                Action<bool> onPipeLineEnd = this.onPipeLineEnd;
                UIManager.Instance.StartUITask(intent, true, false, null, onPipeLineEnd);
            }
        }
    }
}

