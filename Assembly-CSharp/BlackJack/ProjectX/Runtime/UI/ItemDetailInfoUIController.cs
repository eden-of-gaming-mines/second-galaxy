﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class ItemDetailInfoUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_mainStateCtrl;
        public Camera UICamera;
        public CommonItemIconUIController m_ItemIconUICtrl;
        [AutoBind("./DetailPanel/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./DetailPanel/ItemPropertyDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemPropertyDummy;
        [AutoBind("./DetailPanel/ItemInfoPanel/Image", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemIconBGImageGo;
        [AutoBind("./DetailPanel/ItemInfoPanel/ItemIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject IconDummyGo;
        [AutoBind("./DetailPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./DetailPanel/ItemInfoPanel/ItemTitleRoot/ItemName", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemName;
        [AutoBind("./DetailPanel/ItemInfoPanel/ItemModelViewBG", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemModelViewBG;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BGButton;
        private const string CommonItemIconUIPrefabAssetName = "CommonItemUIPrefab";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitUICamera;
        private static DelegateBridge __Hotfix_GetTabsUILayerDummyPosition;
        private static DelegateBridge __Hotfix_GetItemDummyRect;
        private static DelegateBridge __Hotfix_GetItemPropertyDummyRect;
        private static DelegateBridge __Hotfix_GetDetailInfoViewportSize;
        private static DelegateBridge __Hotfix_Get3DModelViewScreenRect;
        private static DelegateBridge __Hotfix_UpdateItemDetailInfo;
        private static DelegateBridge __Hotfix_UpdateModelViewItemDetailInfo;
        private static DelegateBridge __Hotfix_UpdateNoModelViewItemDetailInfo;
        private static DelegateBridge __Hotfix_GetUIProcess;

        [MethodImpl(0x8000)]
        public Rect Get3DModelViewScreenRect()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetDetailInfoViewportSize()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetItemDummyRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetItemPropertyDummyRect()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetTabsUILayerDummyPosition()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(string uiStateName, bool isImmediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void InitUICamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemDetailInfo(ILBStoreItemClient item, Dictionary<string, UnityEngine.Object> recDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateModelViewItemDetailInfo(ILBStoreItemClient item, Dictionary<string, UnityEngine.Object> recDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateNoModelViewItemDetailInfo(ILBStoreItemClient item, Dictionary<string, UnityEngine.Object> recDict)
        {
        }
    }
}

