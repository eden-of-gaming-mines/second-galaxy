﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForSolarSystemUIController : UIControllerBase
    {
        public StarMapInfectDetailUIController m_infectDetailUICtrl;
        public StarMapInfectFinalBattleUIController m_infectFinalBattleUICtrl;
        private bool m_isInfectDetailUICtrlShown;
        private bool m_isInfectFinalBattleUIShown;
        private List<VirtuallBuffIconUIController> m_virtualBufIconCtrlList;
        [AutoBind("./VirtualBuffButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_virtualBuffButton;
        [AutoBind("./VirtualBuffButton/BuffItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buffItem;
        [AutoBind("./VirtualBuffDetailUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_virtualBuffDetailUIDummy;
        public StarMapOrientedTargetSelectWndUIController m_orientedTargetSelectWndUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToPreviousStarMapButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOrientationButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackToPreviousUIButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnHomeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPVEScanProbeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPVPScanProbeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnManualScanProbeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGuildSentryProbeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCelestialTabClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnQuestTabClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnQuestSignalTabClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDelegateSignalTabClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPvpSignalTabClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBaseRedeployInfoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnVirtualBuffButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnGuildSentryHudItemClicked;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSpaceSignalPrecisenessDetailBGButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnJunpToSpaceSignalConfrimButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnJumpToSpaceSignalCancelButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CanvasGroup m_starMapCanvasGroup;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_starMapRootTrans;
        [AutoBind("./EasyPoolRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [AutoBind("./WndRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_wndRootTrans;
        [AutoBind("./ButtonGroupRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_buttonGroupTrans;
        [AutoBind("./RightPopupMenuRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_popupMenuRootTrans;
        [AutoBind("./OrientationUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_orientationUIRoot;
        [AutoBind("./LeftBottomRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftBottomTrans;
        [AutoBind("./RightTopRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightTopTrans;
        [AutoBind("./NewAreaArriveDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_newAreaArriveDummy;
        [AutoBind("./AllowInShipGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_allowInShipGroupDummy;
        [AutoBind("./InfectInfoUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_infectInfoUITrans;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lossWarningDummy;
        [AutoBind("./SolarSystemInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_solarSystemInfoPanelDummy;
        [AutoBind("./SpaceSignalPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_spaceSignalPanelDummy;
        public StarMapCommonButtonGroupUIController m_commonButtonGroupCtrl;
        public StarMapForSolarSystemRightPopupMenuUIController m_popupMenuCtrl;
        public CommonNewAreaInfoUIController m_newAreaInfoUICtrl;
        public BaseRedeploySimpleInfoUIController m_baseRedeployInfoUIController;
        public StarMapSolarSystemInfoPanelUIController m_solarSystemInfoPanelCtrl;
        public SpaceSignalLowPrecisenessUIController m_spaceSignalLowPrecisenessUICtrl;
        public SpaceSignalPrecisenessDetailInfoPanelUIController m_spaceSignalPrecisenessDetailInfoPanelUICtrl;
        private Vector2 m_starMapLeftBottomPos;
        private Vector2 m_starMapRightTopPos;
        private Canvas m_canvas;
        private StarMapOrientedTargetSimpleInfoUIController m_orientedTargetSimpleInfoUICtrl;
        private const string GuildSentrySingalHudItem = "GuildSentrySingalHudItem";
        public List<StarMapItemLocationUIController> m_locationUIItemList;
        public List<GuildSentrySignalHudUIController> m_guildSentrySignalHudList;
        private static DelegateBridge __Hotfix_ShowInfectDetailUI;
        private static DelegateBridge __Hotfix_ShowInfectFinalBattleUI;
        private static DelegateBridge __Hotfix_HideInfectUI;
        private static DelegateBridge __Hotfix_SetCurrInfectUIPos;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_TransformFromScreenPosToUIPos;
        private static DelegateBridge __Hotfix_SetUICanvas;
        private static DelegateBridge __Hotfix_SetUIActive;
        private static DelegateBridge __Hotfix_ShowOrientedTargetSimpleInfo;
        private static DelegateBridge __Hotfix_HideOrientedTargetSimpleInfo;
        private static DelegateBridge __Hotfix_SetVirtualBuffIcon;
        private static DelegateBridge __Hotfix_GetVirtualBuffDetailUIPosition;
        private static DelegateBridge __Hotfix_ClearOnPause;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_OnHomeButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CancelButtonClick;
        private static DelegateBridge __Hotfix_OnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnOrientationButtonClick;
        private static DelegateBridge __Hotfix_OnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentryProbeButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnCelestialTabToggleStateChanged;
        private static DelegateBridge __Hotfix_OnQuestTabToggleStateChanged;
        private static DelegateBridge __Hotfix_OnQuestSignalTabToggleStateChanged;
        private static DelegateBridge __Hotfix_OnDelegateSignalTabToggleStateChanged;
        private static DelegateBridge __Hotfix_OnPvpSignalTabToggleStateChanged;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_OnVirtualBuffButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentryHudItemClicked;
        private static DelegateBridge __Hotfix_OnSpaceSignalPrecisenessDetailBGButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnJumpToSpaceSignalCancelButtonClick;
        private static DelegateBridge __Hotfix_ShowOrientedTargetSelectWnd;
        private static DelegateBridge __Hotfix_HideOrientedTargetSelectWnd;
        private static DelegateBridge __Hotfix_ShowBaseRedeployInfo;
        private static DelegateBridge __Hotfix_GetOrientedTargetSimpleInfoUICtrl;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnHomeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnHomeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildSentryProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildSentryProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCelestialTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnCelestialTabClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestTabClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestSignalTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestSignalTabClick;
        private static DelegateBridge __Hotfix_add_EventOnDelegateSignalTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateSignalTabClick;
        private static DelegateBridge __Hotfix_add_EventOnPvpSignalTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnPvpSignalTabClick;
        private static DelegateBridge __Hotfix_add_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnVirtualBuffButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnVirtualBuffButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildSentryHudItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnGuildSentryHudItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnSpaceSignalPrecisenessDetailBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpaceSignalPrecisenessDetailBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJunpToSpaceSignalConfrimButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJunpToSpaceSignalConfrimButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToSpaceSignalCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToSpaceSignalCancelButtonClick;
        private static DelegateBridge __Hotfix_UpdateOrientationUIInfo;
        private static DelegateBridge __Hotfix_CreateGuildSentrySignalHud;
        private static DelegateBridge __Hotfix_UpdateGuildSentrySignalHudPosition;
        private static DelegateBridge __Hotfix_CreateStarMapLocationUIItem;

        public event Action EventOnBackToPreviousStarMapButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackToPreviousUIButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBaseRedeployInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCelestialTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDelegateSignalTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnGuildSentryHudItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildSentryProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnHomeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnJumpToSpaceSignalCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnJunpToSpaceSignalConfrimButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnManualScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_BackgroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_CommanderButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_MotherShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPVEScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPVPScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPvpSignalTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnQuestSignalTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnQuestTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSpaceSignalPrecisenessDetailBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnVirtualBuffButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearOnPause()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateGuildSentrySignalHud(List<GuildSentryInterestScene> sceneInfos)
        {
        }

        [MethodImpl(0x8000)]
        protected StarMapItemLocationUIController CreateStarMapLocationUIItem()
        {
        }

        [MethodImpl(0x8000)]
        private StarMapOrientedTargetSimpleInfoUIController GetOrientedTargetSimpleInfoUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetVirtualBuffDetailUIPosition()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideInfectUI(bool immediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideOrientedTargetSelectWnd(bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void HideOrientedTargetSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToPreviousStarMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToPreviousUIButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCelestialTabToggleStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateSignalTabToggleStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentryHudItemClicked(ulong sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentryProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHomeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToSpaceSignalConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnManualScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CommanderButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_MotherShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVEScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPvpSignalTabToggleStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestSignalTabToggleStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestTabToggleStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceSignalPrecisenessDetailBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVirtualBuffButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrInfectUIPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUICanvas(Canvas canvas)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVirtualBuffIcon(List<VirtualBuffDesc> buffList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBaseRedeployInfo(MSRedeployInfo redeployInfo)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowInfectDetailUI(float infectProgress, DateTime selfHealingTime)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowInfectFinalBattleUI()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrientedTargetSelectWnd(bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrientedTargetSimpleInfo(Vector2 screenPos, string targetName, Sprite targetIcon)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 TransformFromScreenPosToUIPos(Vector2 screenPos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSentrySignalHudPosition(List<Vector2> uiPosList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOrientationUIInfo(List<LogicBlockStarMapInfoClient.LocationItemInfo> uiItemInfoList)
        {
        }
    }
}

