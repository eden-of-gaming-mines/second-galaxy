﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceItemStoreCategoryToggleUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProduceItemStoreCategoryToggleUIController> EventOnToggleSelected;
        public List<ProduceItemStoreTypeToggleUIController> m_itemTypeToggleList;
        [AutoBind("./CategoryTitle/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./CategoryTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ToggleText;
        [AutoBind("./CategoryTitle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx CategoryToggle;
        [AutoBind("./ItemTypeToggleRoot/ItemTypePrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemTypePrefab;
        [AutoBind("./ItemTypeToggleRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemTypeToggleRoot;
        [AutoBind("./CategoryTitle/NumberGroup/MumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        [AutoBind("./CategoryTitle/NumberGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NumberGroup;
        private int m_category;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetToggleName;
        private static DelegateBridge __Hotfix_SetItemNumber;
        private static DelegateBridge __Hotfix_SetToggleIcon;
        private static DelegateBridge __Hotfix_SetBlueprintCategory;
        private static DelegateBridge __Hotfix_RegEventOnToggleSelected;
        private static DelegateBridge __Hotfix_SwitchToggleState;
        private static DelegateBridge __Hotfix_SetProduceTypeToggleSelectState;
        private static DelegateBridge __Hotfix_IsToggleOpen;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_get_Category;

        public event Action<ProduceItemStoreCategoryToggleUIController> EventOnToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsToggleOpen()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleClick(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnToggleSelected(Action<ProduceItemStoreCategoryToggleUIController> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBlueprintCategory(int category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemNumber(long count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProduceTypeToggleSelectState(int type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleIcon(Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchToggleState(int type = 0)
        {
        }

        public int Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

