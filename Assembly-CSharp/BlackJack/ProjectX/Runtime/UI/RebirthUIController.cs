﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class RebirthUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRebirthButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./DetailPanel/StationNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_stationNameText;
        [AutoBind("./DetailPanel/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_rebirthCountDownProgressBar;
        [AutoBind("./DetailPanel/ResurgenceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_rebirthButton;
        [AutoBind("./DetailPanel/ResurgenceButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rebirthButtonStateCtrl;
        private float m_totalRebirthCountDownSec;
        private float m_remainingRebirthCountDownSec;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ActivateRebirthCountDownProcess;
        private static DelegateBridge __Hotfix_SetSpaceStationName;
        private static DelegateBridge __Hotfix_CreateMainWindownEffectInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnRebirthButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRebirthButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRebirthButtonClick;

        public event Action EventOnRebirthButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ActivateRebirthCountDownProcess(float rebirthCountDownSec)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateMainWindownEffectInfo(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRebirthButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceStationName(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

