﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceSearchReqNetTask : NetWorkTransactionTask
    {
        private string m_key;
        private List<AllianceBasicInfo> m_allianceInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAllianceSearchAck;
        private static DelegateBridge __Hotfix_get_AllianceInfoList;

        [MethodImpl(0x8000)]
        public AllianceSearchReqNetTask(string key)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceSearchAck(List<AllianceBasicInfo> allianceInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public List<AllianceBasicInfo> AllianceInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

