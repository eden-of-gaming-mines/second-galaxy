﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class PvpSignalMenuItemRootUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/SolarSystemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemNameText;
        [AutoBind("./TargetInfo/JumpNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpNumberText;
        private GDBSolarSystemInfo m_solarSystemInfo;
        private static DelegateBridge __Hotfix_SetPvpSignalRootInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPvpSignalRootInfo(GDBSolarSystemInfo info, int jumpVal)
        {
        }
    }
}

