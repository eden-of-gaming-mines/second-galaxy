﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildAntiTeleportEffectActivateReqNetworkTask : NetWorkTransactionTask
    {
        private int m_result;
        private readonly ulong m_instanceId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildAntiTeleportEffectActivateAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        private GuildAntiTeleportEffectActivateReqNetworkTask(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildAntiTeleportEffectActivateAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(ulong instanceId, Action<bool> callBack = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal Action<bool> callBack;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

