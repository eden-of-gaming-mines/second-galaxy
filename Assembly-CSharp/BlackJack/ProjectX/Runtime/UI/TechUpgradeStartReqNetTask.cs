﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TechUpgradeStartReqNetTask : NetWorkTransactionTask
    {
        private int m_techId;
        private int m_upgradeLevel;
        private ulong m_captainInstanceId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <UpgradeStartResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTechUpgradeStartAck;
        private static DelegateBridge __Hotfix_set_UpgradeStartResult;
        private static DelegateBridge __Hotfix_get_UpgradeStartResult;

        [MethodImpl(0x8000)]
        public TechUpgradeStartReqNetTask(int techId, int level, ulong captainInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeStartAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int UpgradeStartResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

