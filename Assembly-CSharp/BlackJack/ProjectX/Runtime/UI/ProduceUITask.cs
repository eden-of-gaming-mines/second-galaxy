﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ProduceUITask : UITaskBase
    {
        protected int m_selectBlueprintId;
        protected bool m_isBind;
        protected bool m_isFreezing;
        protected ulong m_selectCaptainId;
        protected int m_produceBatch;
        protected bool m_bindItemProduceNoTipCheckState;
        protected ItemSimpleInfoUITask m_itemSimpleInfoUiTask;
        protected ProduceLineState m_currProduceLineState;
        private bool m_isNeedRedirect;
        protected bool m_isProduceBgTaskResLoadCompleted;
        protected bool m_isNeedClearCacheOnPause;
        protected bool m_isNeedAutoResetSelectProduceLine;
        protected bool m_isAutoSetBlueprintMode;
        protected DateTime m_nextCheckProduceStateTime;
        protected int m_checkSpanTime;
        protected LBProductionLine m_selectedProduceLine;
        protected ProduceUIController m_mainCtrl;
        protected List<LBProductionLine> m_produceLineCache;
        protected ProduceThreeDBGUITask m_produceBgUITask;
        protected LogicBlockProduceClient m_lbProduce;
        public const string SelectBlueprintKey = "BlueprintId";
        public const string SelectBluePrintBindState = "SelectBluePrintBindState";
        public const string SelectBluePrintFreezingState = "SelectBluePrintFreezingState";
        public const string ResetSelectProduceLineKey = "ResetSelectProduceLine";
        public const string ParamKey_AssistCaptainInsId = "AssistCaptainInsId";
        public const string ParamKey_IsAutoSetBlueprintMode = "IsAutoSetBlueprintMode";
        public const string ParamKey_BindItemProduceTipShowTime = "BindItemProduceTipShowTime";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ProduceUITask";
        private bool m_isDuringNotForceUserGuide;
        private StoreItemType m_guideItemType;
        private int m_guideItemConfigData;
        private GuideState m_currGuideState;
        private const string ParamKey_IsRepeatUserGuide = "IsInRepeatUserGuide";
        private const string ParamKey_GuideItemType = "ItemType";
        private const string ParamKey_GuideItemConfig = "ItemConfig";
        [CompilerGenerated]
        private static Comparison<LBProductionLine> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<LBProductionLine> <>f__am$cache1;
        [CompilerGenerated]
        private static Comparison<LBProductionLine> <>f__am$cache2;
        [CompilerGenerated]
        private static Comparison<ProduceQueueItemUIController> <>f__am$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_SortProduceLine;
        private static DelegateBridge __Hotfix_UpdateDataCache_GetParamInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateSelectLine;
        private static DelegateBridge __Hotfix_UpdateDataCache_InitStartBGTask;
        private static DelegateBridge __Hotfix_UpdateDataCache_UpdateSelectLineState;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnProductionLineAddNtf;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnWatchButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainPanelClick;
        private static DelegateBridge __Hotfix_OnRemoveCaptainButtonClick;
        private static DelegateBridge __Hotfix_OnBlueprintButtonClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel_0;
        private static DelegateBridge __Hotfix_OnAddBlueprintButtonClick;
        private static DelegateBridge __Hotfix_OnAddBlueprintButtonClickImp;
        private static DelegateBridge __Hotfix_OnMaterialIconClick;
        private static DelegateBridge __Hotfix_OnMaterialIconClickImp;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel_1;
        private static DelegateBridge __Hotfix_OnDeleteBlueprintIconClick;
        private static DelegateBridge __Hotfix_OnAddProduceCountButtonClick;
        private static DelegateBridge __Hotfix_OnReduceProduceCountButtonClick;
        private static DelegateBridge __Hotfix_OnStartProduceButtonClick;
        private static DelegateBridge __Hotfix_OnStopProduceButtonClick;
        private static DelegateBridge __Hotfix_OnGetProduceButtonClick;
        private static DelegateBridge __Hotfix_OnBindMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnProduceQueueItemClick;
        private static DelegateBridge __Hotfix_OnItemProduceCompleted;
        private static DelegateBridge __Hotfix_OnCancelButtonClick_ProduceConfirmPanel;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick_ProduceConfirmPanel;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick_BindItemProduceTipPanel;
        private static DelegateBridge __Hotfix_OnCancelButtonClick_BindItemProduceTipPanel;
        private static DelegateBridge __Hotfix_OnNoTipButtonClick_BindItemProduceTipPanel;
        private static DelegateBridge __Hotfix_CanShowBindItemProduceTip;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_OnProduceBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_PlaySoundForProduceUI;
        private static DelegateBridge __Hotfix_StopSound4ProduceUI;
        private static DelegateBridge __Hotfix_StartProduce;
        private static DelegateBridge __Hotfix_CheckProduceState;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetEvent;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_GetProduceRealMaterialAndCurrencyCost;
        private static DelegateBridge __Hotfix_GetProduceRealTimeCost;
        private static DelegateBridge __Hotfix_IsNeedUpdateProduceQueueUI;
        private static DelegateBridge __Hotfix_IsNeedUpdateMidAndRightUI;
        private static DelegateBridge __Hotfix_ClearSelectContext;
        private static DelegateBridge __Hotfix_CreateProduceBGUIBackgroupManager;
        private static DelegateBridge __Hotfix_GetPerfsKey;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_CreateMainPanelPlayeingEffectInfo;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartProduceUITaskForNotForceGuide;
        private static DelegateBridge __Hotfix_TryStartNotForceGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_ClickAddBlueprintButton;
        private static DelegateBridge __Hotfix_ClickFristMaterialIcon;

        [MethodImpl(0x8000)]
        public ProduceUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CanShowBindItemProduceTip()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckProduceState()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearSelectContext()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickAddBlueprintButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFristMaterialIcon(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private UIPlayingEffectInfo CreateMainPanelPlayeingEffectInfo(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private ProduceBGUIBackgroupManager CreateProduceBGUIBackgroupManager()
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetPerfsKey()
        {
        }

        [MethodImpl(0x8000)]
        protected List<CostInfo> GetProduceRealMaterialAndCurrencyCost(int blueprintId, ulong captainId, int count)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetProduceRealTimeCost(int blueprintId, ulong captainId, int batch)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNeedUpdateMidAndRightUI()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNeedUpdateProduceQueueUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddBlueprintButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddBlueprintButtonClickImp(LBProductionLine produceLine, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddProduceCountButtonClick(List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBlueprintButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick_BindItemProduceTipPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick_ProduceConfirmPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainPanelClick(LBProductionLine produceLine)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick_BindItemProduceTipPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick_ProduceConfirmPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDeleteBlueprintIconClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGetProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemProduceCompleted(LBProductionLine produceLine)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaterialIconClick(int type, int confId, int index, Vector3 pos, long needCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMaterialIconClickImp(int type, int confId, int index, Vector3 pos, long needCount, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNoTipButtonClick_BindItemProduceTipPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnProduceBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnProduceQueueItemClick(LBProductionLine produceLine, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnProductionLineAddNtf(int produceLineCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReduceProduceCountButtonClick(List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRemoveCaptainButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStartProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStopProduceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnWatchButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySoundForProduceUI(LogicalSoundResTableID soundResId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNetEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoUIPanel(ItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoUIPanel(ItemInfo itemInfo, int index, Vector3 pos, long needCount, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartProduce()
        {
        }

        [MethodImpl(0x8000)]
        public static ProduceUITask StartProduceUITaskForNotForceGuide(UIIntent returnToIntent, StoreItemType type, int configID)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopSound4ProduceUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void TryStartNotForceGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDataCache_GetParamInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_InitStartBGTask()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDataCache_SortProduceLine()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_UpdateSelectLine()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_UpdateSelectLineState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnAddBlueprintButtonClickImp>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal ProduceUITask $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                if (this.$this.m_isDuringNotForceUserGuide)
                {
                    ProduceItemStoreUITask.StartProduceStoreUITaskForNotForceGuide(this.$this.m_currIntent, this.$this.m_guideItemType, this.$this.m_guideItemConfigData);
                }
                else
                {
                    ProduceItemStoreUITask.StartProduceItemStoreUITask("SelfProduce", this.$this.m_currIntent, this.$this.m_selectBlueprintId, this.$this.m_isBind, this.$this.m_isFreezing, this.onEnd);
                }
            }
        }

        private enum GuideState
        {
            None,
            Start,
            ChooseEmptyProduceLine,
            ChooseBlueprint
        }

        public enum PipeLineMaskType
        {
            UpdatePipeLineing = 1,
            Init = 2,
            Resume = 3,
            BlueprintChanged = 4,
            ProduceProgressChange = 5,
            ProduceLineCountAdd = 6,
            ItemSelected = 7,
            NetworkAck = 8,
            ProduceCountAdd = 9,
            ItemComplete = 10,
            CaptainChanged = 11
        }
    }
}

