﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TradeLoadingPanelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, long, long> EventOnLoadingTradeItemOkButtonClick;
        private int m_tradeItemId;
        private ConfigDataNormalItemInfo m_tradeItemConfig;
        private ulong m_shipSpaceMax;
        private ulong m_shipSpaceFree;
        private ulong m_shipSpaceUsed;
        private long m_ownNum;
        private long m_shipPreLoadeNum;
        private long m_maxLoadNum;
        private long m_curLoadNum;
        private CommonItemIconUIController m_commonItemIconUIController;
        private Dictionary<string, UnityEngine.Object> m_assetDic;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./ItemNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNumberText;
        [AutoBind("./NumberTitleText/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LoadingNumberText;
        [AutoBind("./OccupationTitleText/OccupationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OccupationText;
        [AutoBind("./Slider", AutoBindAttribute.InitState.NotInit, false)]
        public UnityEngine.UI.Slider Slider;
        [AutoBind("./Buttons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button YesButton;
        [AutoBind("./Buttons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button NoButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateUIProcess;
        private static DelegateBridge __Hotfix_UpdateTradeItemInfo;
        private static DelegateBridge __Hotfix_OnSlideValueChanged;
        private static DelegateBridge __Hotfix_OnYesButtonClick;
        private static DelegateBridge __Hotfix_InitSliderAndLoadNum;
        private static DelegateBridge __Hotfix_NeedLimitValue;
        private static DelegateBridge __Hotfix_LimitValue;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_UpdateNumsInUI;
        private static DelegateBridge __Hotfix_add_EventOnLoadingTradeItemOkButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLoadingTradeItemOkButtonClick;

        public event Action<int, long, long> EventOnLoadingTradeItemOkButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSliderAndLoadNum(long preLoadNum)
        {
        }

        [MethodImpl(0x8000)]
        private void LimitValue(int curAmount, long minAmount, long maxAmount)
        {
        }

        [MethodImpl(0x8000)]
        private bool NeedLimitValue(long curAmount, out long minAmount, out long maxAmount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSlideValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnYesButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateNumsInUI()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradeItemInfo(int tradeItemId, long matherShipOwnNum, long curShipOwnNum, ulong shipSpaceMax, ulong shipSpaceUsed, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }
    }
}

