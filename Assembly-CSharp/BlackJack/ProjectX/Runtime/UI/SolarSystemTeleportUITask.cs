﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SolarSystemTeleportUITask : UITaskBase
    {
        public const string SolarSystemTeleportUIMode_BeginMode = "SolarSystemTeleportUIMode_BeginMode";
        public static string SolarSystemTeleportUIMode_FadeInMode;
        public static string SolarSystemTeleportUIMode_LoopMode;
        public static string SolarSystemTeleportUIMode_FadeOutDelayMode;
        public static string SolarSystemTeleportUIMode_FadeOutMode;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnTeleportCompleted;
        private SolarSystemTeleportUIController m_mainCtrl;
        private SolarSystemTeleport3DController m_threeDCtrl;
        private SolarSystemTeleportState m_state;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "SolarSystemTeleportUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsInTeleportBeforeNewSolarSystemStart;
        private static DelegateBridge __Hotfix_IsInTeleport;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetLoopMaintainPeriodEndTime;
        private static DelegateBridge __Hotfix_OnSolarSystemStartEnd;
        private static DelegateBridge __Hotfix_add_EventOnTeleportCompleted;
        private static DelegateBridge __Hotfix_remove_EventOnTeleportCompleted;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnTeleportCompleted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SolarSystemTeleportUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public float GetLoopMaintainPeriodEndTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsInTeleport()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsInTeleportBeforeNewSolarSystemStart()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemStartEnd(SolarSystemTask solarSystemTask)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum SolarSystemTeleportState
        {
            NotBegin,
            Begin,
            CameraMove,
            FadeInOpacity,
            Loop,
            FadeOutDelay,
            FadeOutFromOpacity,
            Complete
        }
    }
}

