﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DelegateQuestItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnShareButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPlayerNameButtonClick;
        public ScrollItemBaseUIController ScrollItemCtrl;
        public CommonCaptainIconUIController CaptainIconCtrl;
        public TinyCorutineHelper m_corutineHelper;
        [AutoBind("./ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ShareButton;
        [AutoBind("./TaskNameText/WarningImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PvpInfoObj;
        [AutoBind("./CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonCaptainDummy;
        [AutoBind("./TaskNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestNameAndResultText;
        [AutoBind("./ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public TextExpand PvpInfoText;
        [AutoBind("./ResultText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardText;
        [AutoBind("./GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemNameText;
        [AutoBind("./GalaxyTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        private const string CaptainIconItemAssetName = "CaptainIconItem";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_SetItemNameAndResult;
        private static DelegateBridge __Hotfix_SetPvpInfoTip;
        private static DelegateBridge __Hotfix_SetPvpInfoText;
        private static DelegateBridge __Hotfix_SetRewardInfo;
        private static DelegateBridge __Hotfix_GetRewardItemNameAndCount;
        private static DelegateBridge __Hotfix_GetLossItemNameAndCount;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerNameClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShareButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShareButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerNameButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerNameButtonClick;

        public event Action<int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnPlayerNameButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnShareButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void GetLossItemNameAndCount(DelegateMissionInfo info, out string name, out long count)
        {
        }

        [MethodImpl(0x8000)]
        private void GetRewardItemNameAndCount(DelegateMissionInfo info, out string name, out long count)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(TinyCorutineHelper corutineHelper)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameClick(string typeStr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemNameAndResult(string name, DelegateMissionState resultState)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPvpInfoText(DelegateMissionInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPvpInfoTip(List<DelegateMissionInvadeInfo> pvpInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRewardInfo(DelegateMissionInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(DelegateMissionInfo itemInfo, NpcCaptainInfo showCaptainInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

