﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.Runtime.UI.CrossScroll;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildMemberJobCtrlUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildJobCtrlMemberNameItemController> EventOnMemberNameItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnTitleItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildMemberJobCtrlUITask.TabType> EventOnTabBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, GuildJobType> EventOnEditJobUnitClick;
        public GuildMemberJobCtrlChangeJobHintController m_changeJobHintCtrl;
        private string m_curEditUserId;
        private GuildMemberJobCtrlUITask.TabType m_curTab;
        private List<GuildMemberJobCtrlUITask.JobInfo> m_jobInfos;
        private List<ConfigDataGuildPermissionInfo> m_permissionInfoList;
        private List<GuildMemberInfo> m_members;
        private Dictionary<string, UnityEngine.Object> m_resData;
        private List<GuildPermission> m_selfPermissions;
        private Dictionary<GuildJobType, GuildPermission> m_perToJobDic;
        private string m_leaderTransferTargetUserId;
        private DateTime m_leaderTransferEndTime;
        private List<GuildJobCtrlTitleItemController> m_memberTitleItemList;
        private List<GuildJobCtrlTitleItemController> m_jurTitleItemList;
        private bool m_resetMemberHorizontalScroll;
        private bool m_resetJurHorizontalScroll;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rootState;
        [AutoBind("./Member/TitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossHorScrollRect m_memberTitleScrollRect;
        [AutoBind("./Jurisdiction/TitleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossHorScrollRect m_jurTitleScrollRect;
        [AutoBind("./ItemRoot/TitleItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_titleItem;
        [AutoBind("./Member/TitleGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_memberTitleSrContent;
        [AutoBind("./Jurisdiction/TitleGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_jurTitleSrContent;
        [AutoBind("./Jurisdiction/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollRect m_jurNameSr;
        [AutoBind("./Jurisdiction/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_jurNamePool;
        [AutoBind("./Jurisdiction/ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollRect m_jurContentSr;
        [AutoBind("./Jurisdiction/ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_jurContentPool;
        [AutoBind("./Member/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollRect m_memberNameSr;
        [AutoBind("./Member/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_memberNamePool;
        [AutoBind("./Member/ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollRect m_memberContentSr;
        [AutoBind("./Member/ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_memberContentPool;
        [AutoBind("./MemberButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_memberTabBtn;
        [AutoBind("./MemberButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_memberTabState;
        [AutoBind("./JurisdictionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_jurTabBtn;
        [AutoBind("./JurisdictionButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_jurTabState;
        [AutoBind("./Jurisdiction", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollController m_scrollJurCtrl;
        [AutoBind("./Member", AutoBindAttribute.InitState.NotInit, false)]
        public CrossScrollController m_scrollMemberCtrl;
        [AutoBind("./ChangeJobHint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_changeJobHint;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetFirstItemIndex;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateMemberView;
        private static DelegateBridge __Hotfix_ResetContentHorizontalPos;
        private static DelegateBridge __Hotfix_UpdateJurView;
        private static DelegateBridge __Hotfix_UpdateTitleState;
        private static DelegateBridge __Hotfix_GetJobInfo;
        private static DelegateBridge __Hotfix_UpdateTabBtnState;
        private static DelegateBridge __Hotfix_InitSelfPermission;
        private static DelegateBridge __Hotfix_OnJurItemFill;
        private static DelegateBridge __Hotfix_OnJurContentItemFill;
        private static DelegateBridge __Hotfix_OnMemberNameItemFill;
        private static DelegateBridge __Hotfix_OnMemberContentItemFill;
        private static DelegateBridge __Hotfix_OnTitleItemClick;
        private static DelegateBridge __Hotfix_OnMemberNameItemClick;
        private static DelegateBridge __Hotfix_OnMemberTabBtnClick;
        private static DelegateBridge __Hotfix_OnContentUnitItemClick;
        private static DelegateBridge __Hotfix_OnJurisdictionTabBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnMemberNameItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemberNameItemClick;
        private static DelegateBridge __Hotfix_add_EventOnTitleItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnTitleItemClick;
        private static DelegateBridge __Hotfix_add_EventOnTabBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnTabBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnEditJobUnitClick;
        private static DelegateBridge __Hotfix_remove_EventOnEditJobUnitClick;

        public event Action<int, GuildJobType> EventOnEditJobUnitClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildJobCtrlMemberNameItemController> EventOnMemberNameItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildMemberJobCtrlUITask.TabType> EventOnTabBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnTitleItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetFirstItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private GuildMemberJobCtrlUITask.JobInfo GetJobInfo(GuildJobCtrlTitleItemController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSelfPermission()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnContentUnitItemClick(int index, GuildJobType job)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJurContentItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJurisdictionTabBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJurItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberContentItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberNameItemClick(GuildJobCtrlMemberNameItemController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberNameItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberTabBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetContentHorizontalPos()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateJurView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMemberView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTabBtnState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTitleState(GuildJobType mainSortType, LogicBlockGuildClient.MemberTitleArrowState mainJobArrow, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<GuildMemberInfo> members, List<GuildMemberJobCtrlUITask.JobInfo> jobInfos, GuildMemberJobCtrlUITask.TabType tab, string curEditUseId, GuildJobType mainSortType, LogicBlockGuildClient.MemberTitleArrowState mainJobArrow, Dictionary<string, UnityEngine.Object> resData, bool resetHorizontalScroll = false)
        {
        }
    }
}

