﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class AuctionUITask : UITaskBase
    {
        private string m_currSellItemTipState;
        private int m_selectedBuyItemAuctionId;
        private AuctionItemDetailInfo m_selectedSellItemInfo;
        private AuctionItemDetailInfo m_lastSelectedSellItemInfo;
        private int m_incrementCount;
        private int m_buyItemCount;
        private int m_maxBuyItemCount;
        private int m_maxCanBuyItemCountInConfig;
        private MaxCountState m_maxCountState;
        private int m_longPressingEventInvokeCount;
        private int m_defaultSellItemCount;
        private int m_maxExtraSellItemCount;
        private bool m_isShowZeroItem;
        private bool m_cancelBlurWhenPause;
        private bool m_isClearOnPause;
        private DateTime m_nextFreshTime;
        private float m_freshIntervalTime;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private AuctionUIController m_mainCtrl;
        private AuctionSellPanelUIController m_sellPanelUICtrl;
        private AuctionBuyPanelUIController m_buyPanelUICtrl;
        private AuctionSellItemInfoPanelUIController m_sellItemInfoPanelUICtrl;
        private AuctionBuyItemInfoPanelUIController m_buyItemInfoPanelUICtrl;
        private NpcShopItemType m_currSelectedAuctionItemType;
        private AuctionBuyItemFilterType m_currAuctionFilter;
        private List<ProAuctionItemBriefInfo> m_auctionBuyItemCacheList;
        private List<ProAuctionItemBriefInfo> m_auctionBuyItemList;
        private List<AuctionItemDetailInfo> m_auctionSellItemList;
        private List<string> m_auctionSellItemStateList;
        private Dictionary<NpcShopItemCategory, List<int>> m_bluePrintCatagory2TypeDic;
        private Dictionary<NpcShopItemCategory, List<int>> m_finishedProductCatagory2TypeDic;
        private IUIBackgroundManager m_backgroundManager;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamsKeyResetFilterState = "ResetFilterState";
        public const string ParamsKeySelectedNpcShopItemType = "SelectedItemType";
        public const string ParamsKeySelectedNpcShopItemId = "SelectedItemId";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        public const string UIModeSell = "Mode_Sell";
        public const string UIModeBuy = "Mode_Buy";
        public const string UIShowBuyState = "ShowBuy";
        public const string UIShowSellState = "ShowSell";
        public const string UICloseState = "Close";
        public const string TaskName = "AuctionUITask";
        public const string SellItemStateNormal = "Normal";
        public const string SellItemStateSoleOut = "SoleOut";
        public const string SellItemStateEmpty = "Empty";
        public const string SellItemStateMonthCardLock = "MonthCardLock";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetShowZeroItemState;
        private static DelegateBridge __Hotfix_ShowZeroItemState;
        private static DelegateBridge __Hotfix_GetFristBuyItemPath;
        private static DelegateBridge __Hotfix_StartAuctionUITaskWithPrepare_2;
        private static DelegateBridge __Hotfix_StartAuctionUITaskWithPrepare_0;
        private static DelegateBridge __Hotfix_StartAuctionUITaskWithPrepare_1;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_Buy;
        private static DelegateBridge __Hotfix_UpdateView_Sell;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_EnablePipeLineMask;
        private static DelegateBridge __Hotfix_IsPipeLineMaskEnabled;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetworkEvent;
        private static DelegateBridge __Hotfix_SetTaskMode;
        private static DelegateBridge __Hotfix_GetBuyItemList;
        private static DelegateBridge __Hotfix_GetSellItemSortedList;
        private static DelegateBridge __Hotfix_GetSellItemStateList;
        private static DelegateBridge __Hotfix_BuyItemInfoComparer;
        private static DelegateBridge __Hotfix_SellItemInfoComparer;
        private static DelegateBridge __Hotfix_CollectNpcItemCategroy2TypeDic;
        private static DelegateBridge __Hotfix_RemoveEmptyListFromCatagory2TypeDic;
        private static DelegateBridge __Hotfix_SendAuctionItemTypeListReq;
        private static DelegateBridge __Hotfix_SendPlayerAuctionItemReq;
        private static DelegateBridge __Hotfix_GetMaxBuyCount;
        private static DelegateBridge __Hotfix_GetChangeCountByLongPressingEventInvokeCount;
        private static DelegateBridge __Hotfix_StartItemDetailUITask;
        private static DelegateBridge __Hotfix_StartShareUITask;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_GetTipWindowUIPrecess;
        private static DelegateBridge __Hotfix_GetSellItemInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_CheckBuyCount;
        private static DelegateBridge __Hotfix_ShowMaxCountTip;
        private static DelegateBridge __Hotfix_ExistUnShelveItemInSellList;
        private static DelegateBridge __Hotfix_SetMaxBuyItemCount;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBuyButtonClick;
        private static DelegateBridge __Hotfix_OnSellButtonClick;
        private static DelegateBridge __Hotfix_OnShowZeroItemButtonClick;
        private static DelegateBridge __Hotfix_OnRefreshButtonClick;
        private static DelegateBridge __Hotfix_OnBindMoneyGetButtonClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyGetButtonClick;
        private static DelegateBridge __Hotfix_OnSortButtonClick;
        private static DelegateBridge __Hotfix_OnSearchButtonClick;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_OnFilterBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_OnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_OnBluePrintButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemClick;
        private static DelegateBridge __Hotfix_UpdateBuyItemInfo;
        private static DelegateBridge __Hotfix_ShowBuyPanel;
        private static DelegateBridge __Hotfix_OnBuyItemIconClick;
        private static DelegateBridge __Hotfix_OnBuyItemInfoPanelItemIconClick;
        private static DelegateBridge __Hotfix_OnBuyItemInfoPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemInfoPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemInfoPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemInfoPanelBuyCountInPutFieldEditEnd;
        private static DelegateBridge __Hotfix_OnBuyItemAddButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemAddButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnBuyItemAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnBuyItemAddButtonLongPressed;
        private static DelegateBridge __Hotfix_OnBuyItemSubtractButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemSubtractButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnBuyItemSubtractButtonLongPressing;
        private static DelegateBridge __Hotfix_OnBuyItemSubtractButtonLongPressed;
        private static DelegateBridge __Hotfix_OnSellItemClick;
        private static DelegateBridge __Hotfix_OnSellItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemShareButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelTipBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelTipButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelReshelveButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelCallBackButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSellItemInfoPanelUnshelveButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerAuctionItemUpdateNtf;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public AuctionUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private int BuyItemInfoComparer(ProAuctionItemBriefInfo itemA, ProAuctionItemBriefInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckBuyCount(int itemBuyCount, bool showTip = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectNpcItemCategroy2TypeDic()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipeLineMask(PipeLineMaskType maskType)
        {
        }

        [MethodImpl(0x8000)]
        private bool ExistUnShelveItemInSellList()
        {
        }

        [MethodImpl(0x8000)]
        protected List<ProAuctionItemBriefInfo> GetBuyItemList()
        {
        }

        [MethodImpl(0x8000)]
        private int GetChangeCountByLongPressingEventInvokeCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public string GetFristBuyItemPath()
        {
        }

        [MethodImpl(0x8000)]
        private int GetMaxBuyCount(List<ProAuctionOrderBriefInfo> orderBriefInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetSellItemInfoPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private List<AuctionItemDetailInfo> GetSellItemSortedList()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> GetSellItemStateList()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIPrecess(string state, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipeLineMaskEnabled(PipeLineMaskType maskType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindMoneyGetButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBluePrintButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemAddButtonLongPressed(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemAddButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemIconClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemInfoPanelBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemInfoPanelBuyCountInPutFieldEditEnd(int buyCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemInfoPanelCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemInfoPanelConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemInfoPanelItemIconClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemSubtractButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemSubtractButtonLongPressed(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemSubtractButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuyItemSubtractButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFinishedProductButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemCategoryButtonClick(NpcShopItemCategory itemCategory)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemTypeToggleSelected(int npcShopType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerAuctionItemUpdateNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRefreshButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSearchButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelBackGroundButtonClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelCallBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelItemDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelReshelveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelTipBackGroundButtonClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelTipButtonClick(string state)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemInfoPanelUnshelveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSellItemShareButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShowZeroItemButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTradeMoneyGetButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveEmptyListFromCatagory2TypeDic(Dictionary<NpcShopItemCategory, List<int>> catagory2TypeDic)
        {
        }

        [MethodImpl(0x8000)]
        private int SellItemInfoComparer(AuctionItemDetailInfo itemA, AuctionItemDetailInfo itemB)
        {
        }

        [MethodImpl(0x8000)]
        private void SendAuctionItemTypeListReq(NpcShopItemType npcShopType, Action<bool> onNetTaskEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendPlayerAuctionItemReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMaxBuyItemCount(int totalItemCount, int showItemCount)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetShowZeroItemState(bool show)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTaskMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowBuyPanel(ConfigDataAuctionItemInfo itemInfo, AuctionItemInfoGetNetworkTask returnTask, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfo(int auctionItemId, Vector3 pos, ItemSimpleInfoUITask.PositionType posType = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMaxCountTip()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowZeroItemState(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartAuctionUITaskWithPrepare(UIIntent returnToIntent, ILBStoreItemClient item, Action<bool> onPreparedEnd = null, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartAuctionUITaskWithPrepare(UIIntent returnToIntent, StoreItemType itemType, int configId, Action<bool> onPreparedEnd = null, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartAuctionUITaskWithPrepare(UIIntent returnToIntent, string mode = "Mode_Buy", NpcShopItemType itemType = 0x67, int selectedAuctionId = -1, Action<bool> onPreparedEnd = null, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartItemDetailUITask(int auctionItemId)
        {
        }

        [MethodImpl(0x8000)]
        private void StartShareUITask(int auctionItemId)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBuyItemInfo(ProAuctionItemBriefInfo auctionItemInfo, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Buy()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Sell()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStorey9
        {
            internal ulong tradeMoneyCount;
            internal AuctionUITask $this;

            internal void <>m__0(Task task)
            {
                AuctionItemBuyReqNetTask task2 = task as AuctionItemBuyReqNetTask;
                if (((task2 != null) && (task2.Ack != null)) && !task2.IsNetworkError)
                {
                    if (task2.Ack.Result == 0)
                    {
                        ConfigDataAuctionItemInfo configDataAuctionItemInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataAuctionItemInfo(this.$this.m_selectedBuyItemAuctionId);
                        string key = (configDataAuctionItemInfo == null) ? string.Empty : ClientStoreItemBaseHelper.GetItemNameStringKey(configDataAuctionItemInfo.ItemType, configDataAuctionItemInfo.ItemId);
                        TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_AuctionBuyItemSuccess, new object[0]), ConfigDataHelper.StringTableProvider.GetStringInDefaultStringTable(key), this.$this.m_buyItemCount), false);
                        this.$this.m_selectedBuyItemAuctionId = -1;
                        CustomUIProcess.UIProcessExecutor[] executorList = new CustomUIProcess.UIProcessExecutor[] { delegate (Action<bool> onEnd) {
                            <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStoreyA ya = new <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStoreyA {
                                <>f__ref$9 = this,
                                onEnd = onEnd
                            };
                            this.$this.SendAuctionItemTypeListReq(this.$this.m_currSelectedAuctionItemType, new Action<bool>(ya.<>m__0));
                        } };
                        CustomUIProcess process = UIProcessFactory.CreateExecutorProcess(UIProcess.ProcessExecMode.Parallel, executorList);
                        process.AddChild(this.$this.m_buyItemInfoPanelUICtrl.GetBuyItemInfoPanelUIProcess(false, false, false));
                        this.$this.PlayUIProcess(process, true, delegate (UIProcess p, bool r) {
                            this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.BuyItemListChanged);
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }, false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Ack.Result, true, false);
                        List<ProAuctionOrderBriefInfo> orderListInfo = task2.Ack.OrderListInfo;
                        if ((orderListInfo == null) || (orderListInfo.Count == 0))
                        {
                            this.$this.m_selectedBuyItemAuctionId = -1;
                            this.$this.PlayUIProcess(this.$this.m_buyItemInfoPanelUICtrl.GetBuyItemInfoPanelUIProcess(false, false, false), true, null, false);
                        }
                        else
                        {
                            this.$this.SetMaxBuyItemCount(task2.Ack.TotalItemCount, this.$this.GetMaxBuyCount(orderListInfo));
                            this.$this.m_buyItemCount = Math.Min(this.$this.m_maxBuyItemCount, this.$this.m_buyItemCount);
                            this.$this.m_buyItemInfoPanelUICtrl.UpdateProductInformation(orderListInfo, this.$this.m_buyItemCount, task2.Ack.TotalItemCount, this.tradeMoneyCount);
                        }
                    }
                }
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStoreyA ya = new <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStoreyA {
                    <>f__ref$9 = this,
                    onEnd = onEnd
                };
                this.$this.SendAuctionItemTypeListReq(this.$this.m_currSelectedAuctionItemType, new Action<bool>(ya.<>m__0));
            }

            internal void <>m__2(UIProcess p, bool r)
            {
                this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.BuyItemListChanged);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }

            private sealed class <OnBuyItemInfoPanelConfirmButtonClick>c__AnonStoreyA
            {
                internal Action<bool> onEnd;
                internal AuctionUITask.<OnBuyItemInfoPanelConfirmButtonClick>c__AnonStorey9 <>f__ref$9;

                internal void <>m__0(bool res)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnItemTypeToggleSelected>c__AnonStorey5
        {
            internal int npcShopType;
            internal AuctionUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.BuyItemListChanged);
                    this.$this.m_currSelectedAuctionItemType = (NpcShopItemType) this.npcShopType;
                    this.$this.m_currAuctionFilter = ~AuctionBuyItemFilterType.None;
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSellItemInfoPanelBackGroundButtonClick>c__AnonStoreyB
        {
            internal Action<PointerEventData> passAction;
            internal PointerEventData eventData;
            internal AuctionUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.passAction != null)
                {
                    this.passAction(this.eventData);
                }
                this.$this.m_lastSelectedSellItemInfo = null;
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal bool requestSellListComplete;
            internal bool requestBuyListComplete;
            internal AuctionUITask.<PrepareForStartOrResume>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(bool res)
            {
                this.requestSellListComplete = true;
                if (this.requestBuyListComplete && (this.<>f__ref$1.onPrepareEnd != null))
                {
                    this.<>f__ref$1.onPrepareEnd(res);
                }
            }

            internal void <>m__1(bool res)
            {
                this.requestBuyListComplete = true;
                if (this.requestSellListComplete && (this.<>f__ref$1.onPrepareEnd != null))
                {
                    this.<>f__ref$1.onPrepareEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
        }

        [CompilerGenerated]
        private sealed class <SendAuctionItemTypeListReq>c__AnonStorey3
        {
            internal Action<bool> onNetTaskEnd;
            internal AuctionUITask $this;

            internal void <>m__0(Task task)
            {
                AuctionItemTypeListReqNetTask task2 = task as AuctionItemTypeListReqNetTask;
                if ((task2.IsNetworkError || (task2 == null)) || (task2.Ack == null))
                {
                    if (this.onNetTaskEnd != null)
                    {
                        this.onNetTaskEnd(false);
                    }
                }
                else if (task2.Ack.Result != 0)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Ack.Result, true, false);
                    if (this.onNetTaskEnd != null)
                    {
                        this.onNetTaskEnd(false);
                    }
                }
                else
                {
                    this.$this.m_auctionBuyItemCacheList = task2.Ack.ItemList;
                    if (this.onNetTaskEnd != null)
                    {
                        this.onNetTaskEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendPlayerAuctionItemReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (((PlayerAuctionItemReqNetTask) task).IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    int result = ((PlayerAuctionItemReqNetTask) task).Result;
                    if (result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetTaskMode>c__AnonStorey2
        {
            internal string mode;
            internal AuctionUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.SwitchMode);
                    this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.BuyItemListChanged);
                    this.$this.m_mainCtrl.SetPanelState(true);
                    this.$this.m_currIntent.TargetMode = this.mode;
                    this.$this.m_currAuctionFilter = ~AuctionBuyItemFilterType.None;
                    this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetMainUIProcess("ShowBuy", true, false), true, null, false);
                    this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                }
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.SwitchMode);
                    this.$this.m_currIntent.TargetMode = this.mode;
                    this.$this.m_mainCtrl.SetPanelState(false);
                    this.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.SellItemListChanged);
                    this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetMainUIProcess("ShowSell", true, false), true, null, false);
                    this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateBuyItemInfo>c__AnonStorey8
        {
            internal int index;
            internal AuctionUITask $this;

            internal void <>m__0(Task task)
            {
                <UpdateBuyItemInfo>c__AnonStorey7 storey = new <UpdateBuyItemInfo>c__AnonStorey7 {
                    <>f__ref$8 = this,
                    returnTask = task as AuctionItemInfoGetNetworkTask
                };
                if ((storey.returnTask != null) && !storey.returnTask.IsNetworkError)
                {
                    if (storey.returnTask.Result != 0)
                    {
                        this.$this.m_selectedBuyItemAuctionId = 0;
                        TipWindowUITask.ShowTipWindowWithErrorCode(storey.returnTask.Result, true, false);
                    }
                    else if (storey.returnTask.ackInfo.TotalItemCount == 0)
                    {
                        this.$this.SendAuctionItemTypeListReq(this.$this.m_currSelectedAuctionItemType, new Action<bool>(storey.<>m__0));
                    }
                    else
                    {
                        <UpdateBuyItemInfo>c__AnonStorey6 storey2 = new <UpdateBuyItemInfo>c__AnonStorey6 {
                            <>f__ref$8 = this,
                            <>f__ref$7 = storey,
                            itemInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataAuctionItemInfo(this.$this.m_selectedBuyItemAuctionId)
                        };
                        if ((storey2.itemInfo == null) || (storey2.itemInfo.ItemType != StoreItemType.StoreItemType_Ship))
                        {
                            this.$this.ShowBuyPanel(storey2.itemInfo, storey.returnTask, this.index);
                        }
                        else
                        {
                            int num;
                            ConfigDataSpaceShipInfo configDataSpaceShipInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataSpaceShipInfo(storey2.itemInfo.ItemId);
                            if (CommonUIUtil.CheckShipDrivingLicenseRequest(configDataSpaceShipInfo, out num))
                            {
                                this.$this.ShowBuyPanel(storey2.itemInfo, storey.returnTask, this.index);
                            }
                            else
                            {
                                string itemSubRankLevelColorStr = ClientStoreItemBaseHelper.GetItemSubRankLevelColorStr(configDataSpaceShipInfo.SubRank);
                                string stringInDefaultStringTable = ConfigDataHelper.StringTableProvider.GetStringInDefaultStringTable(ClientStoreItemBaseHelper.GetItemRankLevelStrFKey(configDataSpaceShipInfo.Rank));
                                string str4 = ConfigDataHelper.StringTableProvider.GetStringInDefaultStringTable(ConfigDataHelper.ConfigDataLoader.GetConfigDataDrivingLicenseInfo(num).NameStrKey);
                                string str5 = GameManager.Instance.StringTableManager.GetStringInDefaultStringTable(ClientStoreItemBaseHelper.GetItemNameStringKey(storey2.itemInfo.ItemType, storey2.itemInfo.ItemId));
                                MsgBoxUITask.ShowMsgBox(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_ConfirmBuyShipWithoutDrivingLicense, new object[0]), new object[] { itemSubRankLevelColorStr, stringInDefaultStringTable, str5, str4 }), new Action(storey2.<>m__0), new Action(storey2.<>m__1));
                            }
                        }
                    }
                }
            }

            private sealed class <UpdateBuyItemInfo>c__AnonStorey6
            {
                internal ConfigDataAuctionItemInfo itemInfo;
                internal AuctionUITask.<UpdateBuyItemInfo>c__AnonStorey8 <>f__ref$8;
                internal AuctionUITask.<UpdateBuyItemInfo>c__AnonStorey8.<UpdateBuyItemInfo>c__AnonStorey7 <>f__ref$7;

                internal void <>m__0()
                {
                    this.<>f__ref$8.$this.ShowBuyPanel(this.itemInfo, this.<>f__ref$7.returnTask, this.<>f__ref$8.index);
                }

                internal void <>m__1()
                {
                    this.<>f__ref$8.$this.m_selectedBuyItemAuctionId = -1;
                }
            }

            private sealed class <UpdateBuyItemInfo>c__AnonStorey7
            {
                internal AuctionItemInfoGetNetworkTask returnTask;
                internal AuctionUITask.<UpdateBuyItemInfo>c__AnonStorey8 <>f__ref$8;

                internal void <>m__0(bool res)
                {
                    this.<>f__ref$8.$this.m_selectedBuyItemAuctionId = 0;
                    TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_CurrItemNotSaleInAuction, new object[0]), false);
                    this.<>f__ref$8.$this.EnablePipeLineMask(AuctionUITask.PipeLineMaskType.BuyItemListChanged);
                    this.<>f__ref$8.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        protected enum MaxCountState
        {
            CanBuyItemCount,
            TotalItemCount,
            ShowItemCount
        }

        public enum PipeLineMaskType
        {
            BuyItemListChanged,
            SellItemListChanged,
            SwitchMode
        }
    }
}

