﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberStaffingLogItemController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGetMoreBtnClick;
        public ScrollItemBaseUIController m_srollItem;
        [AutoBind("./ContentGroup/Title", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_title;
        [AutoBind("./ContentGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_content;
        [AutoBind("./ContentGroup/Time", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_time;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_contentGroup;
        [AutoBind("./ToLoadGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_loadMore;
        [AutoBind("./ToLoadGroup/ToLoadImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_getMore;
        [AutoBind("./ToLoadGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_loadState;
        [AutoBind("./ContentGroup/Title", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_titleState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateLast;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_SetLogTime;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_OnGetMoreBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnGetMoreBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnGetMoreBtnClick;

        public event Action EventOnGetMoreBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetMoreBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLogTime(DateTime regTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLast(bool hasMore)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(GuildStaffingLogInfo log)
        {
        }
    }
}

