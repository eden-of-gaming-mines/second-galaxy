﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapStarfieldNameConfig
    {
        [Header("星域名称开始显示的缩放比")]
        public float m_starfieldNameTextVisibleScaleMin;
        [Header("星域名称显示透明度达到完全不透明时的缩放比")]
        public float m_starfieldNameTextVisibleScaleMax;
        [Header("星域名称Text完全不透明时候的透明度，取0-1")]
        public float m_starfieldNameTextAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

