﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class ItemSimpleInfoUIMainController : UIControllerBase
    {
        public ItemSimpleInfoUIController ItemSimpleInfoUICtrl;
        public ItemObtainSourceUIController ItemObtainSourceUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action> EventOnBackGroundClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoTechButtonClick;
        protected bool m_isShowItemObtainSourcePanel;
        protected Action m_onShowEnd;
        protected Action m_onHideEnd;
        protected UITaskBase m_blockUITask;
        protected bool m_isItemSimpleInfoPanelShowEnd;
        protected bool m_isItemSimpleInfoPanelHideEnd;
        protected bool m_isItemObtainSourcePanelShowEnd;
        protected bool m_isItemObtainSourcePanelHideEnd;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BgButton;
        [AutoBind("./ItemInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemInfoPanel;
        [AutoBind("./ItemInfoPanel/ItemSimpleInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoPanelDummy;
        [AutoBind("./ItemInfoPanel/ItemObtainSourcePanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemObtainSourcePanelDummy;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CanvasGroup CanvasAlphaCtrl;
        private const float InfoPanelWidthSize = 51f;
        private const float SourcePanelWidthSize = 51f;
        private const float InfoPanelHeightSize = 82f;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetObtainSourceDirect;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_EnableBg;
        private static DelegateBridge __Hotfix_GetMainPanelWidth;
        private static DelegateBridge __Hotfix_GetObtainSourcePanelWidth;
        private static DelegateBridge __Hotfix_GetPanelSize;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_Hide;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackGroundClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackGroundClick;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;

        public event Action<Action> EventOnBackGroundClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableBg(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public float GetMainPanelWidth()
        {
        }

        [MethodImpl(0x8000)]
        public float GetObtainSourcePanelWidth()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetPanelSize()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess Hide(bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetObtainSourceDirect(float positionXOnCamera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess Show(bool isShowItemObtainSourcePanel, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [CompilerGenerated]
        private sealed class <OnBackGroundClick>c__AnonStorey0
        {
            internal Action<PointerEventData> passAction;
            internal PointerEventData eventData;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

