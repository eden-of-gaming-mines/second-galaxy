﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class TradeListItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int> EventOnGotoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOpenShopButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildTradePurchaseInfo> EventOnTransportButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnRegionButtonClick;
        private int m_ticks;
        private int m_locateSolarSystemId;
        private int m_locateStationId;
        private GuildTradePurchaseInfo m_guildTradePurchaseInfo;
        private GuildLogoController m_guildLogoController;
        [AutoBind("./RegionText/ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RegionButton;
        [AutoBind("./GuildLogoDummy/GuildLogo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildLogoGameObject;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./NameText/GuildText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildName;
        [AutoBind("./NpcIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NpcIconImage;
        [AutoBind("./RegionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RegionText;
        [AutoBind("./PurchasePriceGroup/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchasePriceText;
        [AutoBind("./PurchasePriceGroup/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./SellPriceGroup/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SellPriceText;
        [AutoBind("./TradeNumGroup/TradeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeNumText;
        [AutoBind("./LingeringGroup/LingeringTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText LingeringTimeFrequentChangeText;
        [AutoBind("./ButtonGroup/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoButton;
        [AutoBind("./ButtonGroup/OpenShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OpenShopButton;
        [AutoBind("./ButtonGroup/TransportButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button TransportButton;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupUIStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_OnPoolObjectReturnPool;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_UpdateAsPersonalTradeItemSellInfo;
        private static DelegateBridge __Hotfix_UpdateAsPersonalTradeItemPurchaseInfo;
        private static DelegateBridge __Hotfix_UpdateAsGuildTradeItemPurchaseInfo;
        private static DelegateBridge __Hotfix_GetGuildLogoController;
        private static DelegateBridge __Hotfix_UpdateButtonGroup;
        private static DelegateBridge __Hotfix_UpdateNpcInfo;
        private static DelegateBridge __Hotfix_UpdateGuildInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnGotoButtonClick;
        private static DelegateBridge __Hotfix_OnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_OnTransportButtonClick;
        private static DelegateBridge __Hotfix_OnRegionButtonClick;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_RegistEvents;
        private static DelegateBridge __Hotfix_UnRegistEvent;
        private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTransportButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTransportButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRegionButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRegionButtonClick;

        public event Action<int, int> EventOnGotoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOpenShopButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnRegionButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildTradePurchaseInfo> EventOnTransportButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private GuildLogoController GetGuildLogoController()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenShopButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPoolObjectReturnPool()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRegionButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransportButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void RegistEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegistEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAsGuildTradeItemPurchaseInfo(GuildTradePurchaseInfo tradeListItemInfo, int startSolarSystemId, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAsPersonalTradeItemPurchaseInfo(TradeListItemInfo tradeListItemInfo, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAsPersonalTradeItemSellInfo(TradeListItemSellInfo tradeListItemSellInfo, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateButtonGroup(int solarSystemId, int stationId, bool isSell)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildInfo(StarMapGuildSimpleInfo guildSimplestInfo, int startSolarSystemId, int tradePortSolarSystemId, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateNpcInfo(int npcTalkerId, int locateSolarSystemId, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }
    }
}

