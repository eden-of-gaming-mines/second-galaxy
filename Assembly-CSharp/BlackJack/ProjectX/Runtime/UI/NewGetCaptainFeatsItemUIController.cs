﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class NewGetCaptainFeatsItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <ItemIndex>k__BackingField;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FeatsIcon;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatsName;
        private static DelegateBridge __Hotfix_SetCaptainFeatsItemInfo;
        private static DelegateBridge __Hotfix_CreateFeatItemProcess;
        private static DelegateBridge __Hotfix_set_ItemIndex;
        private static DelegateBridge __Hotfix_get_ItemIndex;

        [MethodImpl(0x8000)]
        public UIProcess CreateFeatItemProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainFeatsItemInfo(LBNpcCaptainFeats npcFeats, Dictionary<string, Object> resDict)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

