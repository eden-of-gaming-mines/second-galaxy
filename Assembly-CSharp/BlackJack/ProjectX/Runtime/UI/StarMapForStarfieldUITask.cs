﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StarMapForStarfieldUITask : UITaskBase
    {
        private bool m_isWormholeUserGuideActive;
        private bool m_isInfectUserGuideActive;
        private bool m_checkWormholeRecommendLevel;
        private bool m_isExploreQuestUserGuideActive;
        private int m_exploreQuestUserGuideQuestConfigID;
        private ExploreQuestUserGuideState m_userGuideStateForExploreQuest;
        private ExploreQuestUserGuideType m_userGuideTypeForExploreQuest;
        private InfectUserGuideState m_userGuideStateForInfect;
        private GDBSolarSystemSimpleInfo m_userGuideForInfectTargetSolarSystem;
        private WormholeUserGuideState m_userGuideStateForWormhole;
        private GDBSolarSystemSimpleInfo m_userGuideForWormholeTargetSolarSystem;
        private bool m_needAutoEnterSolarSystemStarMap;
        public const string StarMapForStarfieldMode_NormalMode = "StarMapForStarfieldMode_NormalMode";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToMainUITask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackToGalaxyUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string, int, StarMapForSolarSystemUITask.SignalAutoScanIntent, StarMapForSolarSystemUITask.RepeatableUserGuideIntent, float, bool, string> EventOnBackToSolarSystemUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSolarSystemInfo> EventOnSendToChatButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NavigationNodeType, int, int, uint> EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBaseRedeployInfoPanelClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSolarSystemInfo, int> EventOnStartRedeployButtonClick;
        public const string ParamKeyOrientedSolarSystemId = "OrientedSolarSystemId";
        public const string ParamKeyBrushTypeInfo = "BrushTypeInfo";
        public const string ParamKeyNavigationInfo = "NavigationInfo";
        public const string ParamKeyQuestInstaceId = "QuestInstanceId";
        public const string ParamKeyFadeInEffectTimeLength = "FadeInEffectTimeLength";
        public const string ParamKeyRepeatableUserGuideForInfect = "RepeatableUserGuideForInfect";
        public const string ParamKeyRepeatableUserGuideForWormhole = "RepeatableUserGuideForWormhole";
        public const string ParamKeyRepeatableUserGuideForWormholeCheckLevel = "ParamKey_RepeatableUserGuideForWormholeCheckLevel";
        public const string ParamKeyRepeatableUserGuideForExploreQuest = "RepeatableUserGuideForExploreQuest";
        public const string ParamKeyRepeatableUserGuideForExploreQuestConfigId = "RepeatableUserGuideForExploreQuestInstId";
        public const string ParamKeyFromGuild = "FromGuild";
        public const string ParamKeyNeedSavePosAndScaleWhenPause = "NeedSavePosAndScaleWhenPause";
        private const string ParamKeyTradeId = "TradeId";
        private StarMapForStarfieldUIController m_mainCtrl;
        private StarMapForStarfieldButtonWndUIController m_uiCtrl;
        private int m_selQuestInstanceId;
        private bool m_isDrawSolarSystemListChanged;
        private LogicBlockSignalClient m_lbSignal;
        protected static string m_starMapForStarfieldLayerAssetName;
        protected static string m_starMapButtonWndForStarfieldLayerAssetName;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private Vector2? m_starMapCenterPos;
        private float? m_starMapScale;
        private KeyValuePair<int, int>? m_navigationInfo;
        private GEBrushType m_brushType;
        private int m_orientedSolarSystemId;
        private readonly List<LogicBlockStarMapInfoClient.LocationItemInfo> m_locationUIInfoList;
        private float m_fadeInTimeLength;
        private bool m_isSlippingScaleDisable;
        public const string PipeLineCtxTaskNotifyDescInitShowEnd = "OnInitShowEnd";
        private bool m_isGuildInfoReqTaskEnd;
        private bool m_shouldTradeShipDisplay;
        private ulong m_followingTradeShipId;
        private bool m_shouldFlagShipHangarDisplay;
        public const string TaskName = "StarMapForStarfieldUITask";
        private readonly StarfieldDataRequestBase m_dataRequest4GuildBattleState;
        private readonly StarfieldDataRequestBase m_dataRequest4Wormhole;
        private readonly StarfieldDataRequestBase m_dataRequest4GuildOccupy;
        private readonly StarfieldDataRequestBase m_dataRequest4QuestPoolInfo;
        private readonly StarfieldDataRequestBase m_dataRequest4Infect;
        private readonly HashSet<int> m_starfieldIdSet4Viewport;
        private readonly List<StarfieldDataRequestCacheItem> m_starfieldDataRequestCacheItems;
        private bool m_isDuringRequestData;
        private HashSet<int> m_cachedStarfileIdSet;
        private readonly Dictionary<uint, GuildAllianceOccupyInfo> m_guildOccupyInfoDict;
        private readonly Dictionary<uint, GuildAllianceOccupyInfo> m_allianceOccupyInfoDict;
        private readonly Dictionary<int, GuildAllianceId> m_solarSystemGuildMappingDict;
        private float? m_starMapScaleBeforeFadeOut;
        private Vector2? m_starMapCenterPosBeforeFadeOut;
        private bool m_isNeedSavePosAndScale;
        private const string LockLayerName = "StarMapField";
        private const string PveFunName = "PVESignalButton";
        private const string PvpFunName = "PVPSignalButton";
        private const string QuestSingnalFunName = "QuestSignalButton";
        private const string UniverseStarMapFunName = "UniverseButton";
        private const string GuildSentryFunc = "GuildSentryButton";
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        private bool m_isPvpFunButtonShow;
        private bool m_isPveFunButtonShow;
        private bool m_isQuestFunButtonShow;
        private bool m_isGuildSentryFuncButtonShow;
        private static string m_sCurrLangType;
        private static string m_sSolarSystemNameStrings;
        private static Dictionary<string, int> m_solarSystemIDNameDic;
        private List<int> m_strIndexlist;
        private SolarSystemSimpleInfoCompare m_solarSimpleInfoCompare;
        private readonly List<int> m_findedNameIDList;
        private const string ParamKeyShowTunnelType = "ShowTunnelType";
        private const string ParamKeyTunnelInstanceId = "TunnelInstanceId";
        private int solarSystemIdInUserGuide;
        [CompilerGenerated]
        private static Func<KeyValuePair<ulong, int>, int> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateDataCache_RepeatableUserGuide;
        private static DelegateBridge __Hotfix_UpdateView_RepeatableUserGuide;
        private static DelegateBridge __Hotfix_StartUserGuideForExploreQuest;
        private static DelegateBridge __Hotfix_GetGuideStateByQuestConfId;
        private static DelegateBridge __Hotfix_StartRepeatableGuideInfected;
        private static DelegateBridge __Hotfix_TryRepeatableForBrush;
        private static DelegateBridge __Hotfix_RepeatableForBrush;
        private static DelegateBridge __Hotfix_TryRepeatableForSelectInjectTab;
        private static DelegateBridge __Hotfix_InjectTabIsDeActive;
        private static DelegateBridge __Hotfix_RepeatableForSelectInjectTab;
        private static DelegateBridge __Hotfix_StartRepeatableGuidePlanetCount;
        private static DelegateBridge __Hotfix_TryRepeatableForSelSolarSystem;
        private static DelegateBridge __Hotfix_RepeatableForSelSolarSystem;
        private static DelegateBridge __Hotfix_TryRepeatableForSelectPlanetTip;
        private static DelegateBridge __Hotfix_RepeatableForSelectPlanetTip;
        private static DelegateBridge __Hotfix_RepeatableGuideStateEnd;
        private static DelegateBridge __Hotfix_StartUserGuideForInfect;
        private static DelegateBridge __Hotfix_SetUserGuideForInfectToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForInfect;
        private static DelegateBridge __Hotfix_FocusOnNearestInfectSolarSystem;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideInfect;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForInfect_NearestInfectSolarSystem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForInfect_JumpButton;
        private static DelegateBridge __Hotfix_GetUIRectForInfectSolarSystem;
        private static DelegateBridge __Hotfix_GetUIRectForJumpToSolarSystemButton;
        private static DelegateBridge __Hotfix_StartUserGuideForWormhole;
        private static DelegateBridge __Hotfix_SetUserGuideForWormholeToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForWormhole;
        private static DelegateBridge __Hotfix_FoucusOnNearestWormholeSolarSystem;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuideWormhole;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForWormhole_NearestWormholeSolarSystem;
        private static DelegateBridge __Hotfix_TryToTriggerUserGuideForWormhole_EnterSolarSystemStarMapButton;
        private static DelegateBridge __Hotfix_GetUIRectForWormholeSolarSystem;
        private static DelegateBridge __Hotfix_GetUIRectForEnterSolarSystemStarMapButton;
        private static DelegateBridge __Hotfix_StartUITaskByDefault_1;
        private static DelegateBridge __Hotfix_StartUITaskByDefault_0;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_NavigationInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache_BrushType;
        private static DelegateBridge __Hotfix_UpdateDataCache_OrientedSolarSystem;
        private static DelegateBridge __Hotfix_UpdateDataCache_QuestInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache_LocationUIItemInfo;
        private static DelegateBridge __Hotfix_UpdateDataCache_FadeIn;
        private static DelegateBridge __Hotfix_UpdateDataCache_SaveFlag;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateFlagShipHangarList;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_EnableUIInput_0;
        private static DelegateBridge __Hotfix_EnableUIInput_1;
        private static DelegateBridge __Hotfix_OnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnSetUpButtonClick;
        private static DelegateBridge __Hotfix_OnSolarSystemSelected;
        private static DelegateBridge __Hotfix_OnSolarSystemSelectedImp;
        private static DelegateBridge __Hotfix_OnSolarSystem3DTouch;
        private static DelegateBridge __Hotfix_OnEmptyAreaClick;
        private static DelegateBridge __Hotfix_OnScreenScaleOperationHappend;
        private static DelegateBridge __Hotfix_OnScreenSlippingOperationHappend;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_DetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_TunnelJumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_EnterStarMapForSSButtonClickImp_1;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_EnterStarMapForSSButtonClickImp_0;
        private static DelegateBridge __Hotfix_OnSSNodePopMenuBaseRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_SendToChatButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_BackgroundClick;
        private static DelegateBridge __Hotfix_OnHomeButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapItemLocationUIClick;
        private static DelegateBridge __Hotfix_OnOrientationButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_GuildBaseButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_OnBrushTypeChanged;
        private static DelegateBridge __Hotfix_OnBrushTypeChangedImp;
        private static DelegateBridge __Hotfix_UpdateBrushType;
        private static DelegateBridge __Hotfix_OnDrawingSolarSystemListChanged;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickScanProbeRecovery;
        private static DelegateBridge __Hotfix_SimulateBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_GetCurrIntentAndRecordState;
        private static DelegateBridge __Hotfix_GetBrushType;
        private static DelegateBridge __Hotfix_DoOrientationBySolarSystemId;
        private static DelegateBridge __Hotfix_StartShowingSSDetailInfoWnd;
        private static DelegateBridge __Hotfix_StartEnterSolarSystemStarMap;
        private static DelegateBridge __Hotfix_ClearFadeInEffectMask;
        private static DelegateBridge __Hotfix_GetNearestSolarSystemHasWormholeStargate;
        private static DelegateBridge __Hotfix_GetNearestSolarSystemHasSafeWormholeStargate;
        private static DelegateBridge __Hotfix_GetNearestSolarSystemHasInfect;
        private static DelegateBridge __Hotfix_StartEnterStationProcedure;
        private static DelegateBridge __Hotfix_GetNearestEmergencyQuestSolarSystsem;
        private static DelegateBridge __Hotfix_GetEnterSolarSystemSelectedType;
        private static DelegateBridge __Hotfix_SetTradeShipDisplay;
        private static DelegateBridge __Hotfix_SetFlagShipHangarDisplay;
        private static DelegateBridge __Hotfix_add_EventOnBackToMainUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToMainUITask;
        private static DelegateBridge __Hotfix_add_EventOnBackToGalaxyUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToGalaxyUITask;
        private static DelegateBridge __Hotfix_add_EventOnBackToSolarSystemUITask;
        private static DelegateBridge __Hotfix_remove_EventOnBackToSolarSystemUITask;
        private static DelegateBridge __Hotfix_add_EventOnSendToChatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendToChatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeForCelestialOrBuildingOrGlobalScene;
        private static DelegateBridge __Hotfix_add_EventOnBaseRedeployInfoPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseRedeployInfoPanelClick;
        private static DelegateBridge __Hotfix_add_EventOnStartRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStartRedeployButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBSignal;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_RequestStarMapData4BrushType;
        private static DelegateBridge __Hotfix_RequestStarMapData4BrushTypeImp;
        private static DelegateBridge __Hotfix_OnRequestStarMapDataStart;
        private static DelegateBridge __Hotfix_OnRequestStarMapDataEnd;
        private static DelegateBridge __Hotfix_ProcessNextRequest;
        private static DelegateBridge __Hotfix_PrepareStarfieldIdSet4Viewport;
        private static DelegateBridge __Hotfix_RefreshGuildAllianceOccupyInfo;
        private static DelegateBridge __Hotfix_RefreshGuildAllianceOccupyInfo4Starfield;
        private static DelegateBridge __Hotfix_GetEnterSolarSystemStarMapProcess;
        private static DelegateBridge __Hotfix_GetEnterFromSolarSystemStarMapProcess;
        private static DelegateBridge __Hotfix_GetFadeOutProcessExecutor;
        private static DelegateBridge __Hotfix_GetFadeInProcessExecutor;
        private static DelegateBridge __Hotfix_GetEnterSolaySystemStarMapProcessExecutor;
        private static DelegateBridge __Hotfix_GetStarMapInitCenterPos;
        private static DelegateBridge __Hotfix_GetStarMapInitScale;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfo;
        private static DelegateBridge __Hotfix_EnabledStarMapQuestProbe;
        private static DelegateBridge __Hotfix_UnLockQuestSignal;
        private static DelegateBridge __Hotfix_EnabledStarMapGuildSentrySignalProbe;
        private static DelegateBridge __Hotfix_UnLockGuildSentrySignal;
        private static DelegateBridge __Hotfix_EnabledStarMap;
        private static DelegateBridge __Hotfix_UnLockUniverseStarMap;
        private static DelegateBridge __Hotfix_EnabledPveSignal;
        private static DelegateBridge __Hotfix_UnLockPveSignal;
        private static DelegateBridge __Hotfix_EnabledPvpSignal;
        private static DelegateBridge __Hotfix_UnLockPvPignal;
        private static DelegateBridge __Hotfix_OnSearchItemClick;
        private static DelegateBridge __Hotfix_ClearSearchTask;
        private static DelegateBridge __Hotfix_OnScrollviewDragBegin;
        private static DelegateBridge __Hotfix_OnScrollviewDragEnd;
        private static DelegateBridge __Hotfix_ClearSearchScrollItem_0;
        private static DelegateBridge __Hotfix_ClearSearchScrollItem_1;
        private static DelegateBridge __Hotfix_ClearSearchScrollItem_2;
        private static DelegateBridge __Hotfix_OnSearchButtonClick;
        private static DelegateBridge __Hotfix_GetSolarSystemNames;
        private static DelegateBridge __Hotfix_FindPatternStringIndexInFile;
        private static DelegateBridge __Hotfix_ClearShowingTunnel;
        private static DelegateBridge __Hotfix_SetTunnelEffect;
        private static DelegateBridge __Hotfix_DragInUserGuide;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetSecurityDorpDownItemRect;
        private static DelegateBridge __Hotfix_SetDropDownState;
        private static DelegateBridge __Hotfix_GetBrushRectByType;
        private static DelegateBridge __Hotfix_GetCurrSelectSolarSystemTranform;
        private static DelegateBridge __Hotfix_SetQuestSolaySystemInCenter;
        private static DelegateBridge __Hotfix_OnSolarSystemClick;
        private static DelegateBridge __Hotfix_ClickSSNodePopMenu_EnterStarMapForSSButton;
        private static DelegateBridge __Hotfix_ClickBruchType;
        private static DelegateBridge __Hotfix_FocusOnNearestWormholeSolarSystem;
        private static DelegateBridge __Hotfix_FocusOnEmergencyQuestSolarSystem;
        private static DelegateBridge __Hotfix_FocousOnSolarSystem;

        public event Action EventOnBackToGalaxyUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackToMainUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string, int, StarMapForSolarSystemUITask.SignalAutoScanIntent, StarMapForSolarSystemUITask.RepeatableUserGuideIntent, float, bool, string> EventOnBackToSolarSystemUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBaseRedeployInfoPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSendToChatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo, int> EventOnStartRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NavigationNodeType, int, int, uint> EventOnStrikeForCelestialOrBuildingOrGlobalScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public StarMapForStarfieldUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearFadeInEffectMask()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSearchScrollItem()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSearchScrollItem(float scale)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSearchScrollItem(float x, float y)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearSearchTask()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearShowingTunnel()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickBruchType(GEBrushType brushType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickSSNodePopMenu_EnterStarMapForSSButton()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private void DoOrientationBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void DragInUserGuide(bool isLeft)
        {
        }

        [MethodImpl(0x8000)]
        private void EnabledPveSignal(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnabledPvpSignal(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnabledStarMap(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnabledStarMapGuildSentrySignalProbe(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnabledStarMapQuestProbe(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable, bool isGlobalEnable)
        {
        }

        [MethodImpl(0x8000)]
        private void FindPatternStringIndexInFile(string pattern)
        {
        }

        [MethodImpl(0x8000)]
        public void FocousOnSolarSystem(int solarSystemId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void FocusOnEmergencyQuestSolarSystem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void FocusOnNearestInfectSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public void FocusOnNearestWormholeSolarSystem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void FoucusOnNearestWormholeSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetBrushRectByType(GEBrushType type)
        {
        }

        [MethodImpl(0x8000)]
        private GEBrushType GetBrushType(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        public UIIntent GetCurrIntentAndRecordState()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCurrSelectSolarSystemTranform()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetEnterFromSolarSystemStarMapProcess()
        {
        }

        [MethodImpl(0x8000)]
        private string GetEnterSolarSystemSelectedType(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetEnterSolarSystemStarMapProcess(string mode, int solarSystemId, StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent, StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent, string defaultSelectedTab = null)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetEnterSolaySystemStarMapProcessExecutor(string mode, int solarSystemId, StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent, StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent, string defaultSelectedTab)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeInProcessExecutor()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess.UIProcessExecutor GetFadeOutProcessExecutor()
        {
        }

        [MethodImpl(0x8000)]
        private ExploreQuestUserGuideType GetGuideStateByQuestConfId(int questconfId)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private GDBSolarSystemSimpleInfo GetNearestEmergencyQuestSolarSystsem()
        {
        }

        [MethodImpl(0x8000)]
        private GDBSolarSystemSimpleInfo GetNearestSolarSystemHasInfect()
        {
        }

        [MethodImpl(0x8000)]
        private GDBSolarSystemSimpleInfo GetNearestSolarSystemHasSafeWormholeStargate()
        {
        }

        [MethodImpl(0x8000)]
        private GDBSolarSystemSimpleInfo GetNearestSolarSystemHasWormholeStargate(bool checkRecommendLevel = false)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSecurityDorpDownItemRect()
        {
        }

        [MethodImpl(0x8000)]
        private string GetSolarSystemNames()
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetStarMapInitCenterPos(bool needShowAnim)
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarMapInitScale(bool needShowAnim)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForEnterSolarSystemStarMapButton()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForInfectSolarSystem(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForJumpToSolarSystemButton()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetUIRectForWormholeSolarSystem(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForInfect(InfectUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForWormhole(WormholeUserGuideState state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFunctionOpenStateDisplayInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private bool InjectTabIsDeActive()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToPreviousStarMapButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBrushTypeChanged(GEBrushType brushType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBrushTypeChangedImp(GEBrushType brushType, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideInfect(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuideWormhole(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrawingSolarSystemListChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyAreaClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHomeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientationButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_BackgroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CommanderButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_GuildBaseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_MotherShipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestStarMapDataEnd(GEBrushType brushType, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestStarMapDataStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenScaleOperationHappend(float deltaScale)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenSlippingOperationHappend(float offsetX, float offsetY)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScrollviewDragBegin()
        {
        }

        [MethodImpl(0x8000)]
        private void OnScrollviewDragEnd()
        {
        }

        [MethodImpl(0x8000)]
        private List<int> OnSearchButtonClick(string findStr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchItemClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetUpButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystem3DTouch(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemClick(Action<UIProcess, bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemSelected(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemSelectedImp(GDBSolarSystemSimpleInfo solarSystemInfo, Action<UIProcess, bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_BackgroundClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_EnterStarMapForSSButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_JumpButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_SendToChatButtonClick(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_DetailInfoButtonClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_EnterStarMapForSSButtonClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSSNodePopMenu_EnterStarMapForSSButtonClickImp(int solarSystemId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_EnterStarMapForSSButtonClickImp(int solarSystemId, string selectedTab = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_JumpButtonClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_TunnelJumpButtonClick(ulong enterTunnel, ulong targetTunnel, int targetSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenuBaseRedeployButtonClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapItemLocationUIClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareStarfieldIdSet4Viewport()
        {
        }

        [MethodImpl(0x8000)]
        private void ProcessNextRequest()
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshGuildAllianceOccupyInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshGuildAllianceOccupyInfo4Starfield(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForBrush(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        protected void RepeatableForSelectInjectTab(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForSelectPlanetTip(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableForSelSolarSystem(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void RepeatableGuideStateEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void RequestStarMapData4BrushType(GEBrushType brushType, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator RequestStarMapData4BrushTypeImp(GEBrushType brushType, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDropDownState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFlagShipHangarDisplay(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestSolaySystemInCenter(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTradeShipDisplay(bool display)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTunnelEffect()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForInfectToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideForWormholeToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        public void SimulateBackToPreviousStarMapButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterSolarSystemStarMap(string mode, int solarSystemId, StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent, float fadeInTimeLength, StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent, Action<bool> onEnd, string defaultSelectedTab = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartEnterStationProcedure(GDBSolarSystemInfo solarSystemInfo, GDBSpaceStationInfo spaceStationInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableGuideInfected()
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableGuidePlanetCount()
        {
        }

        [MethodImpl(0x8000)]
        private void StartShowingSSDetailInfoWnd(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartUITaskByDefault(int orientedSolarSystemId = 0, ulong tradeId = 0UL, Action<bool> onPrepareAction = null, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartUITaskByDefault(int orientedSolarSystemId = 0, int navigationStartSSId = 0, int navigationEndSSId = 0, GEBrushType brushType = 1, int selQuestInstanceId = 0, float fadeInEffectTimeLength = 0f, bool fromGuild = false, bool needSavePos = false, RepeatableUserGuideIntent repeatedUserGuideIntent = null, int showTunnelType = 0, ulong tunnelInsId = 0UL, Action<bool> onPrepareAction = null, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForExploreQuest(int questconfId)
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForInfect()
        {
        }

        [MethodImpl(0x8000)]
        private void StartUserGuideForWormhole()
        {
        }

        [MethodImpl(0x8000)]
        private void TickScanProbeRecovery()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForBrush(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        protected void TryRepeatableForSelectInjectTab(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForSelectPlanetTip(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void TryRepeatableForSelSolarSystem(Action<bool> onSucess)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForInfect_JumpButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForInfect_NearestInfectSolarSystem(Action<bool> onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForWormhole_EnterSolarSystemStarMapButton(Action onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void TryToTriggerUserGuideForWormhole_NearestWormholeSolarSystem(Action<bool> onTriggerFinished)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockGuildSentrySignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockPveSignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockPvPignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockQuestSignal(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockUniverseStarMap(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBrushType(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_BrushType(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_FadeIn(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_LocationUIItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_NavigationInfo(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_OrientedSolarSystem(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_QuestInfo(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_RepeatableUserGuide(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_SaveFlag(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFlagShipHangarList(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_RepeatableUserGuide()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockSignalClient LBSignal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <FocousOnSolarSystem>c__AnonStorey23
        {
            internal Action<bool> onEnd;

            internal void <>m__0()
            {
                this.onEnd(true);
            }
        }

        [CompilerGenerated]
        private sealed class <GetEnterSolaySystemStarMapProcessExecutor>c__AnonStorey1E
        {
            internal string mode;
            internal int solarSystemId;
            internal StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent;
            internal StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent;
            internal string defaultSelectedTab;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(Action<bool> onend)
            {
                <GetEnterSolaySystemStarMapProcessExecutor>c__AnonStorey1F storeyf = new <GetEnterSolaySystemStarMapProcessExecutor>c__AnonStorey1F {
                    <>f__ref$30 = this,
                    onend = onend
                };
                this.$this.ExecAfterTicks(new Action(storeyf.<>m__0), 1UL);
            }

            private sealed class <GetEnterSolaySystemStarMapProcessExecutor>c__AnonStorey1F
            {
                internal Action<bool> onend;
                internal StarMapForStarfieldUITask.<GetEnterSolaySystemStarMapProcessExecutor>c__AnonStorey1E <>f__ref$30;

                internal void <>m__0()
                {
                    this.<>f__ref$30.$this.StartEnterSolarSystemStarMap(this.<>f__ref$30.mode, this.<>f__ref$30.solarSystemId, this.<>f__ref$30.autoScanIntent, this.<>f__ref$30.$this.m_mainCtrl.GetFadeInEffectTimeLength(), this.<>f__ref$30.repeatableUserGuideIntent, this.onend, this.<>f__ref$30.defaultSelectedTab);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetFadeInProcessExecutor>c__AnonStorey1D
        {
            internal Action<bool> execEnd;

            internal void <>m__0()
            {
                if (this.execEnd != null)
                {
                    this.execEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetFadeOutProcessExecutor>c__AnonStorey1C
        {
            internal Action<bool> execEnd;

            internal void <>m__0()
            {
                if (this.execEnd != null)
                {
                    this.execEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnBrushTypeChangedImp>c__AnonStorey19
        {
            internal GEBrushType brushType;
            internal Action<bool> onEnd;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                Debug.Log("StarMapForStarfieldUITask::OnBrushTypeChangedImp stop type:" + this.brushType);
                this.$this.EnableUIInput(true);
                this.$this.UpdateBrushType(this.onEnd);
            }

            internal void <>m__1(bool res)
            {
                this.$this.SetTradeShipDisplay(true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrientedTargetSelectWnd_CommanderButtonClick>c__AnonStorey16
        {
            internal int commanderOrientation;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.DoOrientationBySolarSystemId(this.commanderOrientation);
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrientedTargetSelectWnd_GuildBaseButtonClick>c__AnonStorey18
        {
            internal GuildClient guildClient;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.DoOrientationBySolarSystemId(this.guildClient.GetBasicInfo().m_baseSolarSystem);
            }
        }

        [CompilerGenerated]
        private sealed class <OnOrientedTargetSelectWnd_MotherShipButtonClick>c__AnonStorey17
        {
            internal int motherShipOrientation;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.DoOrientationBySolarSystemId(this.motherShipOrientation);
            }
        }

        [CompilerGenerated]
        private sealed class <OnSolarSystemSelectedImp>c__AnonStoreyF
        {
            internal GDBSolarSystemSimpleInfo solarSystemInfo;
            internal Action<UIProcess, bool> onEnd;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(int result)
            {
                LogicBlockGuildClient lBGuild = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild() as LogicBlockGuildClient;
                if (lBGuild != null)
                {
                    this.$this.m_mainCtrl.SetJumpPanel(this.solarSystemInfo, lBGuild.GetCurrAvailableTeleportTunnelInfo());
                }
                else
                {
                    this.$this.m_mainCtrl.SetJumpPanel(this.solarSystemInfo, null);
                }
                if (!this.$this.m_mainCtrl.IsSSNodeClickPopMenuEnable())
                {
                    this.$this.PlayUIProcess(this.$this.m_mainCtrl.ShowSSNodeClickPopMenu(this.solarSystemInfo.Id, this.$this.m_dynamicResCacheDict, false), true, this.onEnd, false);
                }
                else
                {
                    CommonUIStateEffectProcess process = UIProcessFactory.CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode.Serial, new UIPlayingEffectInfo[0]);
                    process.AddChild(this.$this.m_mainCtrl.HideSSNodeClickPopMenu(true));
                    process.AddChild(this.$this.m_mainCtrl.ShowSSNodeClickPopMenu(this.solarSystemInfo.Id, this.$this.m_dynamicResCacheDict, false));
                    this.$this.PlayUIProcess(process, true, this.onEnd, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSDetailInfoWnd_JumpButtonClick>c__AnonStorey15
        {
            internal GDBSolarSystemInfo solarSystemInfo;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.$this.EventOnStrikeForCelestialOrBuildingOrGlobalScene != null)
                {
                    this.$this.EventOnStrikeForCelestialOrBuildingOrGlobalScene(NavigationNodeType.SolarSystem, 0, this.solarSystemInfo.Id, 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSNodePopMenu_DetailInfoButtonClick>c__AnonStorey10
        {
            internal int solarSystemId;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.StartShowingSSDetailInfoWnd(this.solarSystemId);
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSNodePopMenu_EnterStarMapForSSButtonClickImp>c__AnonStorey12
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess proc, bool res)
            {
                this.onEnd(res);
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSNodePopMenu_JumpButtonClick>c__AnonStorey11
        {
            internal int solarSystemId;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.$this.EventOnStrikeForCelestialOrBuildingOrGlobalScene != null)
                {
                    this.$this.EventOnStrikeForCelestialOrBuildingOrGlobalScene(NavigationNodeType.SolarSystem, 0, this.solarSystemId, 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSSNodePopMenuBaseRedeployButtonClick>c__AnonStorey13
        {
            internal int solarSystemId;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(GDBSolarSystemInfo solarSystemInfo)
            {
                <OnSSNodePopMenuBaseRedeployButtonClick>c__AnonStorey14 storey = new <OnSSNodePopMenuBaseRedeployButtonClick>c__AnonStorey14 {
                    <>f__ref$19 = this,
                    solarSystemInfo = solarSystemInfo
                };
                this.$this.EnableUIInput(true, true);
                if (storey.solarSystemInfo.SpaceStations.Count == 0)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_NotExistSpaceStationInSolarSystem, false, new object[0]);
                }
                else
                {
                    bool flag;
                    int num;
                    storey.spaceStationInfo = storey.solarSystemInfo.SpaceStations[0];
                    if (!this.$this.PlayerCtx.GetLBMotherShipBasic().CheckRedeployStart(this.solarSystemId, storey.spaceStationInfo.Id, out flag, out num))
                    {
                        MsgBoxUITask.ShowMsgBoxWithErrorCode(num, null);
                    }
                    else
                    {
                        this.$this.PlayUIProcess(this.$this.m_mainCtrl.HideSSNodeClickPopMenu(false), true, new Action<UIProcess, bool>(storey.<>m__0), false);
                    }
                }
            }

            private sealed class <OnSSNodePopMenuBaseRedeployButtonClick>c__AnonStorey14
            {
                internal GDBSolarSystemInfo solarSystemInfo;
                internal GDBSpaceStationInfo spaceStationInfo;
                internal StarMapForStarfieldUITask.<OnSSNodePopMenuBaseRedeployButtonClick>c__AnonStorey13 <>f__ref$19;

                internal void <>m__0(UIProcess p, bool r)
                {
                    if (this.<>f__ref$19.$this.EventOnStartRedeployButtonClick != null)
                    {
                        this.<>f__ref$19.$this.EventOnStartRedeployButtonClick(this.solarSystemInfo, this.spaceStationInfo.Id);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStoreyD
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(bool res)
            {
                if (this.onPrepareEnd != null)
                {
                    this.onPrepareEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForBrush>c__AnonStorey2
        {
            internal Action<bool> onSucess;

            internal void <>m__0(bool inRect)
            {
                this.onSucess(inRect);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelectInjectTab>c__AnonStorey4
        {
            internal Action<bool> onSucess;

            internal void <>m__0(bool inRect)
            {
                this.onSucess(inRect);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelectPlanetTip>c__AnonStorey8
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(bool inRect)
            {
                if (inRect)
                {
                    UIProcess process = UIProcessFactory.CreateExecutorProcess(UIProcess.ProcessExecMode.Serial, new CustomUIProcess.UIProcessExecutor[0]);
                    process.AddChild(this.$this.m_mainCtrl.HideSSNodeClickPopMenu(false));
                    StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent = new StarMapForSolarSystemUITask.RepeatableUserGuideIntent {
                        IsExploreQuestPlanetCountUserGuideActive = true
                    };
                    process.AddChild(this.$this.GetEnterSolarSystemStarMapProcess("StarMapForSolarSystemMode_NormalMode", this.$this.PlayerCtx.CurrSolarSystemId, null, repeatableUserGuideIntent, null));
                    this.$this.PlayUIProcess(process, true, null, false);
                }
                this.onSucess(inRect);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatableForSelSolarSystem>c__AnonStorey6
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(bool inRect)
            {
                if (inRect)
                {
                    this.$this.OnSolarSystemClick((p, end) => this.onSucess(end));
                }
                else
                {
                    this.onSucess(false);
                }
            }

            internal void <>m__1(UIProcess p, bool end)
            {
                this.onSucess(end);
            }
        }

        [CompilerGenerated]
        private sealed class <RequestStarMapData4BrushTypeImp>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GEBrushType brushType;
            internal Action onEnd;
            internal StarMapForStarfieldUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (!GameManager.Instance.GameClientSetting.DisableUnimportantLog)
                        {
                            Debug.Log("StarMapForStarfieldUITask:: RequestStarMapData4BrushTypeImp: " + this.brushType);
                        }
                        this.$this.OnRequestStarMapDataStart();
                        switch (this.brushType)
                        {
                            case GEBrushType.GEBrushType_Sovereign:
                            case GEBrushType.GEBrushType_AllianceSovereign:
                                this.$current = this.$this.m_dataRequest4GuildOccupy.RequestData4StarfieldSet(this.$this.m_starfieldIdSet4Viewport);
                                if (!this.$disposing)
                                {
                                    this.$PC = 1;
                                }
                                goto TR_0002;

                            case GEBrushType.GEBrushType_WormHoleDistribution:
                            case GEBrushType.GEBrushType_RareWormHoleDistribution:
                                this.$current = this.$this.m_dataRequest4Wormhole.RequestData4StarfieldSet(this.$this.m_starfieldIdSet4Viewport);
                                if (!this.$disposing)
                                {
                                    this.$PC = 2;
                                }
                                goto TR_0002;

                            case GEBrushType.GEBrushType_InfectProgress:
                                this.$current = this.$this.m_dataRequest4Infect.RequestData4StarfieldSet(this.$this.m_starfieldIdSet4Viewport);
                                if (!this.$disposing)
                                {
                                    this.$PC = 3;
                                }
                                goto TR_0002;

                            case GEBrushType.GEBrushType_QuestPoolCount:
                                this.$current = this.$this.m_dataRequest4QuestPoolInfo.RequestData4StarfieldSet(this.$this.m_starfieldIdSet4Viewport);
                                if (!this.$disposing)
                                {
                                    this.$PC = 4;
                                }
                                goto TR_0002;

                            case GEBrushType.GEBrushType_GuildBattleState:
                                this.$current = this.$this.m_dataRequest4GuildBattleState.RequestData4StarfieldSet(this.$this.m_starfieldIdSet4Viewport);
                                if (!this.$disposing)
                                {
                                    this.$PC = 5;
                                }
                                goto TR_0002;

                            default:
                                break;
                        }
                        goto TR_0001;

                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        goto TR_0001;

                    default:
                        goto TR_0000;
                }
                goto TR_0002;
            TR_0000:
                return false;
            TR_0001:
                this.$this.OnRequestStarMapDataEnd(this.brushType, this.onEnd);
                this.$PC = -1;
                goto TR_0000;
            TR_0002:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <SetQuestSolaySystemInCenter>c__AnonStorey22
        {
            internal int solarSystemId;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                this.$this.solarSystemIdInUserGuide = this.solarSystemId;
                (this.$this.m_currIntent as UIIntentCustom).SetParam("OrientedSolarSystemId", this.$this.solarSystemIdInUserGuide);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SetTunnelEffect>c__AnonStorey20
        {
            internal UIIntentCustom intent;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                <SetTunnelEffect>c__AnonStorey21 storey = new <SetTunnelEffect>c__AnonStorey21 {
                    <>f__ref$32 = this,
                    currSolarSystemId = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBCharacter().GetSolarSystemId()
                };
                if (this.intent.GetStructParam<int>("ShowTunnelType") != 1)
                {
                    if (this.intent.GetStructParam<int>("ShowTunnelType") == 2)
                    {
                        ClientSelfPlayerSpaceShip4FlagShip selfShip = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).SelfShip as ClientSelfPlayerSpaceShip4FlagShip;
                        if (selfShip != null)
                        {
                            this.$this.m_mainCtrl.SetTunnelEffect(storey.currSolarSystemId, 0UL, selfShip.CalcPropertiesById(PropertiesId.PropertiesId_FlagShipTeleportRadius_Final));
                        }
                    }
                }
                else
                {
                    LogicBlockGuildClient lBGuild = ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBGuild() as LogicBlockGuildClient;
                    if (lBGuild != null)
                    {
                        GuildTeleportTunnelEffectInfoClient client2 = (this.intent.GetStructParam<ulong>("TunnelInstanceId") != 0L) ? lBGuild.GetCurrAvailableTeleportTunnelInfo().FirstOrDefault<GuildTeleportTunnelEffectInfoClient>(new Func<GuildTeleportTunnelEffectInfoClient, bool>(storey.<>m__1)) : lBGuild.GetCurrAvailableTeleportTunnelInfo().FirstOrDefault<GuildTeleportTunnelEffectInfoClient>(new Func<GuildTeleportTunnelEffectInfoClient, bool>(storey.<>m__0));
                        if (client2 != null)
                        {
                            this.$this.m_mainCtrl.SetTunnelEffect(storey.currSolarSystemId, client2.m_instanceId, client2.m_teleportDistance);
                        }
                    }
                }
            }

            private sealed class <SetTunnelEffect>c__AnonStorey21
            {
                internal int currSolarSystemId;
                internal StarMapForStarfieldUITask.<SetTunnelEffect>c__AnonStorey20 <>f__ref$32;

                internal bool <>m__0(GuildTeleportTunnelEffectInfoClient tunnel) => 
                    ((tunnel.m_solarSystemId == this.currSolarSystemId) && !tunnel.m_isCreatedByFlagShip);

                internal bool <>m__1(GuildTeleportTunnelEffectInfoClient tunnel) => 
                    (tunnel.m_instanceId == this.<>f__ref$32.intent.GetStructParam<ulong>("TunnelInstanceId"));
            }
        }

        [CompilerGenerated]
        private sealed class <StartEnterSolarSystemStarMap>c__AnonStorey1B
        {
            internal float fadeInTimeLength;
            internal Action<bool> onEnd;
            internal string mode;
            internal int solarSystemId;
            internal StarMapForSolarSystemUITask.SignalAutoScanIntent autoScanIntent;
            internal StarMapForSolarSystemUITask.RepeatableUserGuideIntent repeatableUserGuideIntent;
            internal bool fromGuild;
            internal string defaultSelectedTab;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(GDBSolarSystemInfo solarSystemInfo)
            {
                if (this.fadeInTimeLength > 0.01f)
                {
                    this.$this.m_mainCtrl.SetFinalCameraStateDirectly();
                }
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
                if (this.$this.EventOnBackToSolarSystemUITask != null)
                {
                    this.$this.EventOnBackToSolarSystemUITask(this.mode, this.solarSystemId, this.autoScanIntent, this.repeatableUserGuideIntent, this.fadeInTimeLength, this.fromGuild, this.defaultSelectedTab);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartShowingSSDetailInfoWnd>c__AnonStorey1A
        {
            internal GDBStarfieldsInfo starfieldInfo;
            internal GDBStargroupInfo stargroupInfo;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0(GDBSolarSystemInfo solarSystemInfo)
            {
                this.$this.EnableUIInput(true, true);
                this.$this.PlayUIProcess(this.$this.m_uiCtrl.ShowSSDetailInfoWnd(this.starfieldInfo, this.stargroupInfo, solarSystemInfo), true, null, false);
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForBrush>c__AnonStorey1
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                if (this.$this.m_mainCtrl.m_brushInfoUICtrl.m_brushTypeSelectedDropdown == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForBrush(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelectInjectTab>c__AnonStorey3
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                if (this.$this.InjectTabIsDeActive())
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelectInjectTab(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelectPlanetTip>c__AnonStorey7
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                if (this.$this.GetUIRectForEnterSolarSystemStarMapButton() == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelectPlanetTip(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryRepeatableForSelSolarSystem>c__AnonStorey5
        {
            internal Action<bool> onSucess;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                if (this.$this.GetCurrSelectSolarSystemTranform() == null)
                {
                    this.onSucess(false);
                }
                else
                {
                    this.$this.RepeatableForSelSolarSystem(this.onSucess);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForInfect_JumpButton>c__AnonStoreyA
        {
            internal Action onTriggerFinished;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForInfect_JumpButton(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForInfect_NearestInfectSolarSystem>c__AnonStorey9
        {
            internal Action<bool> onTriggerFinished;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                ((UIIntentCustom) this.$this.m_currIntent).SetParam("OrientedSolarSystemId", this.$this.m_userGuideForInfectTargetSolarSystem.Id);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                this.$this.ExecAfterTicks(delegate {
                    RectTransform uIRectForInfectSolarSystem = this.$this.GetUIRectForInfectSolarSystem(this.$this.m_userGuideForInfectTargetSolarSystem.Id);
                    if (uIRectForInfectSolarSystem == null)
                    {
                        Debug.LogError($"RepeatableUserGuideForInfect:Cannot find solarSystem:{this.$this.m_userGuideForWormholeTargetSolarSystem.Id}({this.$this.m_userGuideForWormholeTargetSolarSystem.Name}) in StarMap");
                        this.$this.GoToUserGuideStateForWormhole(StarMapForStarfieldUITask.WormholeUserGuideState.End);
                        if (this.onTriggerFinished != null)
                        {
                            this.onTriggerFinished(false);
                        }
                    }
                    else
                    {
                        Vector3 localScale = uIRectForInfectSolarSystem.localScale;
                        RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(RectTransformUtility.WorldToScreenPoint(this.$this.MainLayer.LayerCamera, uIRectForInfectSolarSystem.position), new Vector2(uIRectForInfectSolarSystem.rect.width * localScale.x, uIRectForInfectSolarSystem.rect.height * localScale.y), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideInfect), true);
                        if (this.onTriggerFinished != null)
                        {
                            this.onTriggerFinished(true);
                        }
                    }
                }, 1UL);
            }

            internal void <>m__1()
            {
                RectTransform uIRectForInfectSolarSystem = this.$this.GetUIRectForInfectSolarSystem(this.$this.m_userGuideForInfectTargetSolarSystem.Id);
                if (uIRectForInfectSolarSystem == null)
                {
                    Debug.LogError($"RepeatableUserGuideForInfect:Cannot find solarSystem:{this.$this.m_userGuideForWormholeTargetSolarSystem.Id}({this.$this.m_userGuideForWormholeTargetSolarSystem.Name}) in StarMap");
                    this.$this.GoToUserGuideStateForWormhole(StarMapForStarfieldUITask.WormholeUserGuideState.End);
                    if (this.onTriggerFinished != null)
                    {
                        this.onTriggerFinished(false);
                    }
                }
                else
                {
                    Vector3 localScale = uIRectForInfectSolarSystem.localScale;
                    RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(RectTransformUtility.WorldToScreenPoint(this.$this.MainLayer.LayerCamera, uIRectForInfectSolarSystem.position), new Vector2(uIRectForInfectSolarSystem.rect.width * localScale.x, uIRectForInfectSolarSystem.rect.height * localScale.y), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideInfect), true);
                    if (this.onTriggerFinished != null)
                    {
                        this.onTriggerFinished(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForWormhole_EnterSolarSystemStarMapButton>c__AnonStoreyC
        {
            internal Action onTriggerFinished;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                this.$this.TryToTriggerUserGuideForWormhole_EnterSolarSystemStarMapButton(this.onTriggerFinished);
            }
        }

        [CompilerGenerated]
        private sealed class <TryToTriggerUserGuideForWormhole_NearestWormholeSolarSystem>c__AnonStoreyB
        {
            internal Action<bool> onTriggerFinished;
            internal StarMapForStarfieldUITask $this;

            internal void <>m__0()
            {
                RectTransform uIRectForWormholeSolarSystem = this.$this.GetUIRectForWormholeSolarSystem(this.$this.m_userGuideForWormholeTargetSolarSystem.Id);
                if (uIRectForWormholeSolarSystem == null)
                {
                    Debug.LogError($"RepeatableUserGuideForWormhole:Cannot find solarSystem:{this.$this.m_userGuideForWormholeTargetSolarSystem.Id}({this.$this.m_userGuideForWormholeTargetSolarSystem.Name}) in StarMap");
                    this.$this.GoToUserGuideStateForWormhole(StarMapForStarfieldUITask.WormholeUserGuideState.End);
                    if (this.onTriggerFinished != null)
                    {
                        this.onTriggerFinished(false);
                    }
                }
                else
                {
                    Vector3 localScale = uIRectForWormholeSolarSystem.localScale;
                    RepeatableUserGuideUITask.StartRepeatableUserGuideUITask(RectTransformUtility.WorldToScreenPoint(this.$this.MainLayer.LayerCamera, uIRectForWormholeSolarSystem.position), new Vector2(uIRectForWormholeSolarSystem.rect.width * localScale.x, uIRectForWormholeSolarSystem.rect.height * localScale.y), new Action<bool>(this.$this.OnClickReturn4NotForceUserGuideWormhole), true);
                    if (this.onTriggerFinished != null)
                    {
                        this.onTriggerFinished(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateFlagShipHangarList>c__AnonStoreyE
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildFlagShipHangarDataSyncNetTask task2 = task as GuildFlagShipHangarDataSyncNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (this.onEnd != null)
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        internal enum ExploreQuestUserGuideState
        {
            Start,
            Finding,
            End
        }

        internal enum ExploreQuestUserGuideType
        {
            Error,
            Infected,
            PlanetCount,
            Star,
            Planet
        }

        internal enum InfectUserGuideState
        {
            Start,
            FocusOnNearestInfectSolarSystem,
            FocusOnJumpToButton,
            End
        }

        public class RepeatableUserGuideIntent
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsWormholeUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsInfectUserGuideActive>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsCheckWormholeRecommendLevel>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <IsExploreQuestUserGuideActive>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <ExplorQuestConfigID>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_IsWormholeUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsWormholeUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsInfectUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsInfectUserGuideActive;
            private static DelegateBridge __Hotfix_get_IsCheckWormholeRecommendLevel;
            private static DelegateBridge __Hotfix_set_IsCheckWormholeRecommendLevel;
            private static DelegateBridge __Hotfix_get_IsExploreQuestUserGuideActive;
            private static DelegateBridge __Hotfix_set_IsExploreQuestUserGuideActive;
            private static DelegateBridge __Hotfix_get_ExplorQuestConfigID;
            private static DelegateBridge __Hotfix_set_ExplorQuestConfigID;

            public bool IsWormholeUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsInfectUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsCheckWormholeRecommendLevel
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public bool IsExploreQuestUserGuideActive
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public int ExplorQuestConfigID
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }
        }

        public class SolarSystemSimpleInfoCompare : IComparer<int>
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Compare;

            public SolarSystemSimpleInfoCompare()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public int Compare(int indexA, int indexB)
            {
                DelegateBridge bridge = __Hotfix_Compare;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp1326(this, indexA, indexB);
                }
                int questLevelBySignalDifficult = CommonLogicUtil.GetQuestLevelBySignalDifficult(CommonLogicUtil.GetSolarSystemDifficultLevel(indexA, null));
                int num4 = CommonLogicUtil.GetQuestLevelBySignalDifficult(CommonLogicUtil.GetSolarSystemDifficultLevel(indexB, null));
                if (questLevelBySignalDifficult != num4)
                {
                    return questLevelBySignalDifficult.CompareTo(num4);
                }
                Dictionary<int, GDBSolarSystemSimpleInfo> allGDBSolarSystemSimpleInfo = ConfigDataHelper.GDBDataLoader.GetAllGDBSolarSystemSimpleInfo();
                GDBSolarSystemSimpleInfo info2 = allGDBSolarSystemSimpleInfo[indexB];
                return allGDBSolarSystemSimpleInfo[indexA].Name.CompareTo(info2.Name);
            }
        }

        public class StarfieldDataRequest4GuildBattleState : StarMapForStarfieldUITask.StarfieldDataRequestBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;

            public StarfieldDataRequest4GuildBattleState()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected override bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp169(this, starfieldId);
                }
                return ((base.PlayerCtx.GetLBGuild() as LogicBlockGuildClient).GetStarfieldGuildBattleStatus(starfieldId) != null);
            }

            [DebuggerHidden]
            protected override IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4Starfield>c__Iterator0 { starfieldIdSet = starfieldIdSet };
            }

            [CompilerGenerated]
            private sealed class <RequestData4Starfield>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal HashSet<int> starfieldIdSet;
                internal StarfieldGuildBattleInfoMultiReqNetTask <guildBattleInfoNetTask>__0;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <RequestData4Starfield>c__AnonStorey1 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <RequestData4Starfield>c__AnonStorey1();
                            this.$locvar0.isFinish = null;
                            this.<guildBattleInfoNetTask>__0 = new StarfieldGuildBattleInfoMultiReqNetTask(this.starfieldIdSet, false);
                            this.<guildBattleInfoNetTask>__0.EventOnStop += new Action<Task>(this.$locvar0.<>m__0);
                            this.<guildBattleInfoNetTask>__0.Start(null, null);
                            break;

                        case 1:
                            break;

                        default:
                            goto TR_0000;
                    }
                    if (this.$locvar0.isFinish == null)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;
                    }
                    this.$PC = -1;
                TR_0000:
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <RequestData4Starfield>c__AnonStorey1
                {
                    internal bool? isFinish;

                    internal void <>m__0(Task task)
                    {
                        this.isFinish = true;
                    }
                }
            }
        }

        public class StarfieldDataRequest4GuildOccpyInfo : StarMapForStarfieldUITask.StarfieldDataRequestBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsDataExpire;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;

            public StarfieldDataRequest4GuildOccpyInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected override bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp169(this, starfieldId);
                }
                return (base.PlayerCtx.GetLBStarMapGuildOccupy().GetStarFieldGuildOccupyStaticInfoVersion(starfieldId) != 0);
            }

            protected override bool IsDataExpire()
            {
                DelegateBridge bridge = __Hotfix_IsDataExpire;
                return ((bridge == null) || bridge.__Gen_Delegate_Imp9(this));
            }

            [DebuggerHidden]
            protected override IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4Starfield>c__Iterator0 { starfieldIdSet = starfieldIdSet };
            }

            [CompilerGenerated]
            private sealed class <RequestData4Starfield>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal HashSet<int> starfieldIdSet;
                internal StarMapGuildOccupyInfoMultiReqNetTask <occupyInfoNetTask>__0;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <RequestData4Starfield>c__AnonStorey1 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <RequestData4Starfield>c__AnonStorey1();
                            this.$locvar0.isFinish = null;
                            this.<occupyInfoNetTask>__0 = new StarMapGuildOccupyInfoMultiReqNetTask(this.starfieldIdSet, false);
                            this.<occupyInfoNetTask>__0.EventOnStop += new Action<Task>(this.$locvar0.<>m__0);
                            this.<occupyInfoNetTask>__0.Start(null, null);
                            break;

                        case 1:
                            break;

                        default:
                            goto TR_0000;
                    }
                    if (this.$locvar0.isFinish == null)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;
                    }
                    this.$PC = -1;
                TR_0000:
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <RequestData4Starfield>c__AnonStorey1
                {
                    internal bool? isFinish;

                    internal void <>m__0(Task task)
                    {
                        this.isFinish = true;
                    }
                }
            }
        }

        public class StarfieldDataRequest4Infect : StarMapForStarfieldUITask.StarfieldDataRequestBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;

            public StarfieldDataRequest4Infect()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected override bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp169(this, starfieldId);
                }
                return (base.PlayerCtx.GetLBInfect().GetInfectInfoDataVersion(starfieldId) != 0);
            }

            [DebuggerHidden]
            protected override IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4Starfield>c__Iterator0 { starfieldIdSet = starfieldIdSet };
            }

            [CompilerGenerated]
            private sealed class <RequestData4Starfield>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal HashSet<int> starfieldIdSet;
                internal StarFieldInfectInfoReqMultiNetTask <infoReqMultiNetTask>__0;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <RequestData4Starfield>c__AnonStorey1 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <RequestData4Starfield>c__AnonStorey1();
                            this.$locvar0.isFinish = null;
                            this.<infoReqMultiNetTask>__0 = new StarFieldInfectInfoReqMultiNetTask(this.starfieldIdSet, false);
                            this.<infoReqMultiNetTask>__0.EventOnStop += new Action<Task>(this.$locvar0.<>m__0);
                            this.<infoReqMultiNetTask>__0.Start(null, null);
                            break;

                        case 1:
                            break;

                        default:
                            goto TR_0000;
                    }
                    if (this.$locvar0.isFinish == null)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;
                    }
                    this.$PC = -1;
                TR_0000:
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <RequestData4Starfield>c__AnonStorey1
                {
                    internal bool? isFinish;

                    internal void <>m__0(Task task)
                    {
                        this.isFinish = true;
                    }
                }
            }
        }

        public class StarfieldDataRequest4WormholeGate : StarMapForStarfieldUITask.StarfieldDataRequestBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;

            public StarfieldDataRequest4WormholeGate()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected override bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp169(this, starfieldId);
                }
                return (base.PlayerCtx.GetLBWormhole().GetWormholeInfoDataVersion(starfieldId) != 0);
            }

            [DebuggerHidden]
            protected override IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4Starfield>c__Iterator0 { starfieldIdSet = starfieldIdSet };
            }

            [CompilerGenerated]
            private sealed class <RequestData4Starfield>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal HashSet<int> starfieldIdSet;
                internal StarFieldWormholeGateInfoMultiReqNetTask <wormHoleNetTask>__0;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <RequestData4Starfield>c__AnonStorey1 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <RequestData4Starfield>c__AnonStorey1();
                            this.$locvar0.isFinish = null;
                            this.<wormHoleNetTask>__0 = new StarFieldWormholeGateInfoMultiReqNetTask(true, false, this.starfieldIdSet, false);
                            this.<wormHoleNetTask>__0.EventOnStop += new Action<Task>(this.$locvar0.<>m__0);
                            this.<wormHoleNetTask>__0.Start(null, null);
                            break;

                        case 1:
                            break;

                        default:
                            goto TR_0000;
                    }
                    if (this.$locvar0.isFinish == null)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;
                    }
                    this.$PC = -1;
                TR_0000:
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <RequestData4Starfield>c__AnonStorey1
                {
                    internal bool? isFinish;

                    internal void <>m__0(Task task)
                    {
                        this.isFinish = true;
                    }
                }
            }
        }

        public class StarfieldDataRequestBase
        {
            protected readonly HashSet<int> m_dirtyStarfieldIdSet = new HashSet<int>();
            public DateTime m_expireTime = DateTime.MinValue;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_RequestData4StarfieldSet;
            private static DelegateBridge __Hotfix_IsDataExpire;
            private static DelegateBridge __Hotfix_ResetDateExpireTime;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;
            private static DelegateBridge __Hotfix_get_DirtyStarfieldIdSet;
            private static DelegateBridge __Hotfix_get_PlayerCtx;

            public StarfieldDataRequestBase()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected virtual bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                return ((bridge != null) && bridge.__Gen_Delegate_Imp169(this, starfieldId));
            }

            protected virtual bool IsDataExpire()
            {
                DelegateBridge bridge = __Hotfix_IsDataExpire;
                return ((bridge == null) ? (this.m_expireTime < Timer.m_currTime) : bridge.__Gen_Delegate_Imp9(this));
            }

            protected virtual IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge == null)
                {
                    throw new NotImplementedException();
                }
                return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
            }

            [DebuggerHidden]
            public IEnumerator RequestData4StarfieldSet(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4StarfieldSet;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4StarfieldSet>c__Iterator0 { 
                    starfieldIdSet = starfieldIdSet,
                    $this = this
                };
            }

            protected void ResetDateExpireTime()
            {
                DelegateBridge bridge = __Hotfix_ResetDateExpireTime;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_expireTime = Timer.m_currTime.AddSeconds(30.0);
                }
            }

            public HashSet<int> DirtyStarfieldIdSet
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_DirtyStarfieldIdSet;
                    return ((bridge == null) ? this.m_dirtyStarfieldIdSet : bridge.__Gen_Delegate_Imp5369(this));
                }
            }

            protected ProjectXPlayerContext PlayerCtx
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_PlayerCtx;
                    return ((bridge == null) ? (GameManager.Instance.PlayerContext as ProjectXPlayerContext) : bridge.__Gen_Delegate_Imp1339(this));
                }
            }

            [CompilerGenerated]
            private sealed class <RequestData4StarfieldSet>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal bool <isDataExprie>__0;
                internal HashSet<int> starfieldIdSet;
                internal HashSet<int>.Enumerator $locvar0;
                internal StarMapForStarfieldUITask.StarfieldDataRequestBase $this;
                internal object $current;
                internal bool $disposing;
                internal int $PC;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$this.m_dirtyStarfieldIdSet.Clear();
                            this.<isDataExprie>__0 = this.$this.IsDataExpire();
                            this.$locvar0 = this.starfieldIdSet.GetEnumerator();
                            try
                            {
                                while (this.$locvar0.MoveNext())
                                {
                                    int current = this.$locvar0.Current;
                                    if (this.<isDataExprie>__0)
                                    {
                                        this.$this.m_dirtyStarfieldIdSet.Add(current);
                                        continue;
                                    }
                                    if (!this.$this.IsData4StarfieldViald(current))
                                    {
                                        this.$this.m_dirtyStarfieldIdSet.Add(current);
                                    }
                                }
                            }
                            finally
                            {
                                this.$locvar0.Dispose();
                            }
                            if (this.$this.m_dirtyStarfieldIdSet.Count == 0)
                            {
                                break;
                            }
                            this.$current = this.$this.RequestData4Starfield(this.$this.m_dirtyStarfieldIdSet);
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            return true;

                        case 1:
                            if (this.<isDataExprie>__0)
                            {
                                this.$this.ResetDateExpireTime();
                            }
                            this.$PC = -1;
                            break;

                        default:
                            break;
                    }
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;
            }
        }

        public class StarfieldDataRequestCacheItem
        {
            public GEBrushType m_brushType;
            public Action m_onEnd;
            private static DelegateBridge _c__Hotfix_ctor;

            public StarfieldDataRequestCacheItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class StarfiledDataRequest4QuestPoolInfo : StarMapForStarfieldUITask.StarfieldDataRequestBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsData4StarfieldViald;
            private static DelegateBridge __Hotfix_RequestData4Starfield;

            public StarfiledDataRequest4QuestPoolInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected override bool IsData4StarfieldViald(int starfieldId)
            {
                DelegateBridge bridge = __Hotfix_IsData4StarfieldViald;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp169(this, starfieldId);
                }
                return (base.PlayerCtx.GetLBSolarSystemPoolInfo().GetStarfieldPoolInfo(starfieldId) != null);
            }

            [DebuggerHidden]
            protected override IEnumerator RequestData4Starfield(HashSet<int> starfieldIdSet)
            {
                DelegateBridge bridge = __Hotfix_RequestData4Starfield;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp58(this, starfieldIdSet);
                }
                return new <RequestData4Starfield>c__Iterator0 { starfieldIdSet = starfieldIdSet };
            }

            [CompilerGenerated]
            private sealed class <RequestData4Starfield>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal HashSet<int> starfieldIdSet;
                internal StarFieldPoolCountInfoReqMultiNetTask <reqMultiNetTask>__0;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <RequestData4Starfield>c__AnonStorey1 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <RequestData4Starfield>c__AnonStorey1();
                            this.$locvar0.isFinish = null;
                            this.<reqMultiNetTask>__0 = new StarFieldPoolCountInfoReqMultiNetTask(this.starfieldIdSet, false);
                            this.<reqMultiNetTask>__0.EventOnStop += new Action<Task>(this.$locvar0.<>m__0);
                            this.<reqMultiNetTask>__0.Start(null, null);
                            break;

                        case 1:
                            break;

                        default:
                            goto TR_0000;
                    }
                    if (this.$locvar0.isFinish == null)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;
                    }
                    this.$PC = -1;
                TR_0000:
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <RequestData4Starfield>c__AnonStorey1
                {
                    internal bool? isFinish;

                    internal void <>m__0(Task task)
                    {
                        this.isFinish = true;
                    }
                }
            }
        }

        internal enum WormholeUserGuideState
        {
            Start,
            FocusOnNearestWormholeSolarSystem,
            FocusOnEnterSolarSystemStarMapButton,
            End
        }
    }
}

