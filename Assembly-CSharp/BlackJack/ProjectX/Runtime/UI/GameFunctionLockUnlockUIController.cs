﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GameFunctionLockUnlockUIController : UIControllerBase
    {
        [AutoBind("./CircleEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_circleEffectUIObj;
        [AutoBind("./RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rectEffectUIObj;
        private LinkedList<GameObject> m_unusedCircleEffectUIObjList;
        private LinkedList<GameObject> m_unusedRectEffectUIObjList;
        private Dictionary<string, GameObject> m_lockEffectObjDict;
        private Camera m_canvasCamera;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_SetLockEffectStateByName;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetSourceEffectUIObjByType;
        private static DelegateBridge __Hotfix_GetUnusedEffectUIObjByType;
        private static DelegateBridge __Hotfix_GetEffectUIObjNameByType;
        private static DelegateBridge __Hotfix_SetLockEffectStateImpl;
        private static DelegateBridge __Hotfix_SetLockEffectUITransform;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private string GetEffectUIObjNameByType(GameFunctionLockEffectType effectType)
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetSourceEffectUIObjByType(GameFunctionLockEffectType effectType)
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetUnusedEffectUIObjByType(GameFunctionLockEffectType effectType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLockEffectStateByName(GameFunctionLockEffectType effectType, string name, RectTransform showAreaTransform, RectTransform clickAreaTransform, bool isLocked, SystemFuncType funcType, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLockEffectStateImpl(GameObject lockEffectObj, bool isLocked)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLockEffectUITransform(GameObject lockEffectObj, RectTransform showAreaRectTrans, RectTransform clickAreaRectTrans)
        {
        }

        [CompilerGenerated]
        private sealed class <SetLockEffectStateByName>c__AnonStorey0
        {
            internal Action onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

