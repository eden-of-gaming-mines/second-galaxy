﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class NpcListItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <NpcId>k__BackingField;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NPCStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ChooseToggle;
        [AutoBind("./NpcIconRoot/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NpcIcon;
        [AutoBind("./NpcNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NpcNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetNpcInfo;
        private static DelegateBridge __Hotfix_GetFactionNameByNpcID;
        private static DelegateBridge __Hotfix_set_NpcId;
        private static DelegateBridge __Hotfix_get_NpcId;

        [MethodImpl(0x8000)]
        private string GetFactionNameByNpcID(int npcId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNpcInfo(LBNpcTalkerBase npcTalker, Dictionary<string, Object> resDict)
        {
        }

        public int NpcId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

