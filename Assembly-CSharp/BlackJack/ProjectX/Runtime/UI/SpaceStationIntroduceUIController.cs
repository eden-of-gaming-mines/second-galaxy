﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SpaceStationIntroduceUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBaseRedeployInfoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <SolarystemID>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <SpaceStationId>k__BackingField;
        private const string CommonSystemTimePrefabName = "CommonSystemTimePrefab";
        private const string BaseRedeployPrefabName = "BaseRedeployPrefab";
        private CommonSystemTimeUIController m_timeUICtrl;
        private BaseRedeploySimpleInfoUIController m_baseRedeployUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IntroducePanelStateCtrl;
        [AutoBind("./DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DescriptionButton;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./DescriptionImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailPanelStateCtrl;
        [AutoBind("./DescriptionImage/FrameImage/BGImage/DetailInfo/TitleGroup01/NameText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text StationName;
        [AutoBind("./DescriptionButton/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StationNameInDetail;
        [AutoBind("./DescriptionImage/FrameImage/BGImage/DetailInfo/DescriptionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StationDescText;
        [AutoBind("./RechargeNotifyRoot/InvestigateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx InvestigateButton;
        [AutoBind("./RechargeNotifyRoot/ReportButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_reportButton;
        [AutoBind("./RechargeNotifyRoot/BattlePassButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BattlePassGo;
        [AutoBind("./RechargeNotifyRoot/BattlePassButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BattlePassButton;
        [AutoBind("./RechargeNotifyRoot/BattlePassButton/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BattlePassButtonRedImage;
        [AutoBind("./RechargeNotifyRoot/BattlePassButton/FX_UI_SpaceStationMedal", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattlePassEffectStateCtrl;
        [AutoBind("./RechargeNotifyRoot/MonthCardButton/FX_UI_GiftIcon", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MonthCardEffectStateCtrl;
        [AutoBind("./RechargeNotifyRoot/GiftPackageButton/FX_UI_GiftIcon", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SpecialGiftEffectStateCtrl;
        [AutoBind("./RechargeNotifyRoot/MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MonthCardGo;
        [AutoBind("./RechargeNotifyRoot/MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MonthCardButton;
        [AutoBind("./RechargeNotifyRoot/GiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GiftPackageGo;
        [AutoBind("./RechargeNotifyRoot/GiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GiftPackageButton;
        [AutoBind("./RechargeNotifyRoot/SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OperatingActivityGo;
        [AutoBind("./RechargeNotifyRoot/SignButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OperatingActivityButton;
        [AutoBind("./RechargeNotifyRoot/SignButton/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OperatingActivityRedImage;
        [AutoBind("./TimeRootDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_timeRootTf;
        [AutoBind("./BaseRedeployDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_baseRedeployRootTf;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateViewForIntroduction;
        private static DelegateBridge __Hotfix_ShowOrHideIntroducePanel;
        private static DelegateBridge __Hotfix_ShowShowPanelAnimation;
        private static DelegateBridge __Hotfix_ChangeDetailState;
        private static DelegateBridge __Hotfix_ShowOrHideDetail;
        private static DelegateBridge __Hotfix_EnableGiftPackageNotify;
        private static DelegateBridge __Hotfix_EnableMonthCardNotify;
        private static DelegateBridge __Hotfix_EnableOperatingActivityButton;
        private static DelegateBridge __Hotfix_UpdateInvestigateButtonState;
        private static DelegateBridge __Hotfix_EnableBattlePassEffect;
        private static DelegateBridge __Hotfix_EnableMonthCardBtnEffect;
        private static DelegateBridge __Hotfix_EnableSpecialGiftBtnEffect;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_get_SolarystemID;
        private static DelegateBridge __Hotfix_set_SolarystemID;
        private static DelegateBridge __Hotfix_get_SpaceStationId;
        private static DelegateBridge __Hotfix_set_SpaceStationId;

        public event Action EventOnBaseRedeployInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ChangeDetailState()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBattlePassEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableGiftPackageNotify(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableMonthCardBtnEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableMonthCardNotify(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableOperatingActivityButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableSpecialGiftBtnEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideDetail(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideIntroducePanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShowPanelAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInvestigateButtonState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewForIntroduction(int solarsyatemId, int spaceStationId, string stationName, string desc, MSRedeployInfo redeployInfo)
        {
        }

        public int SolarystemID
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int SpaceStationId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

