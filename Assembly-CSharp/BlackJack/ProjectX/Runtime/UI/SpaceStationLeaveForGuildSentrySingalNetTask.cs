﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceStationLeaveForGuildSentrySingalNetTask : NetWorkTransactionTask
    {
        private readonly ulong m_shipInstanceId;
        private readonly ulong m_instanceId;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationLeaveForGuildSentrySingalAck;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;

        [MethodImpl(0x8000)]
        public SpaceStationLeaveForGuildSentrySingalNetTask(ulong shipInstanceId, ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationLeaveForGuildSentrySingalAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

