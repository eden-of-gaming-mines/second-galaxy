﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class FunctionOpenStateUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsFunctionOpen;
        private static DelegateBridge __Hotfix_CheckFunctionOpenNotIncludeAnim;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_PlayFunctionOpenEffect;
        private static DelegateBridge __Hotfix_BeginPlayFunctionOpenState;
        private static DelegateBridge __Hotfix_OnUnlockAnimationEnd;
        private static DelegateBridge __Hotfix_OnSendUnlockAniamtionReqEnd;

        [MethodImpl(0x8000)]
        public static void BeginPlayFunctionOpenState(UITaskBase task, FunctionOpenStateDisplayInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckFunctionOpenNotIncludeAnim(SystemFuncType funcType, bool showTip = true)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFunctionOpen(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnSendUnlockAniamtionReqEnd(UIManager.UIActionQueueItem actionItem, UITaskBase srcTask, FunctionOpenStateDisplayInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnUnlockAnimationEnd(SystemFuncType funcType, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayFunctionOpenEffect(UITaskBase task, List<FunctionOpenStateDisplayInfo> funcStateDisplayInfos)
        {
        }

        [MethodImpl(0x8000)]
        public static void UpdateFunctionOpenStateInview(List<FunctionOpenStateDisplayInfo> funcStateDisplayInfos)
        {
        }

        [CompilerGenerated]
        private sealed class <BeginPlayFunctionOpenState>c__AnonStorey0
        {
            internal FunctionOpenStateUtil.FunctionOpenStateDisplayInfo currInfo;
            internal UIManager.UIActionQueueItem actionItem;
            internal UITaskBase task;

            internal void <>m__0()
            {
                if (this.currInfo.m_unLockAction != null)
                {
                    this.currInfo.m_unLockAction(() => FunctionOpenStateUtil.OnUnlockAnimationEnd(this.currInfo.m_funcType, () => FunctionOpenStateUtil.OnSendUnlockAniamtionReqEnd(this.actionItem, this.task, this.currInfo)));
                }
                else
                {
                    FunctionOpenStateUtil.OnUnlockAnimationEnd(this.currInfo.m_funcType, () => FunctionOpenStateUtil.OnSendUnlockAniamtionReqEnd(this.actionItem, this.task, this.currInfo));
                }
            }

            internal void <>m__1()
            {
                FunctionOpenStateUtil.OnUnlockAnimationEnd(this.currInfo.m_funcType, () => FunctionOpenStateUtil.OnSendUnlockAniamtionReqEnd(this.actionItem, this.task, this.currInfo));
            }

            internal void <>m__2()
            {
                FunctionOpenStateUtil.OnSendUnlockAniamtionReqEnd(this.actionItem, this.task, this.currInfo);
            }

            internal void <>m__3()
            {
                FunctionOpenStateUtil.OnSendUnlockAniamtionReqEnd(this.actionItem, this.task, this.currInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <OnUnlockAnimationEnd>c__AnonStorey1
        {
            internal Action onEnd;

            internal void <>m__0(Task task)
            {
                if (!(task as FunctionOpenAnimationEndReqNetTask).IsNetworkError && (this.onEnd != null))
                {
                    this.onEnd();
                }
            }
        }

        public class FunctionOpenStateDisplayInfo
        {
            public SystemFuncType m_funcType;
            public Action<bool> m_enableAction;
            public Action<Action> m_unLockAction;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

