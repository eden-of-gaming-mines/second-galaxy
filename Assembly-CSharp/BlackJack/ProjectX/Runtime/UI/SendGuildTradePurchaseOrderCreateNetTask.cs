﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SendGuildTradePurchaseOrderCreateNetTask : NetWorkTransactionTask
    {
        private int m_result;
        private readonly int m_solarSystemId;
        private readonly StoreItemType m_storeItemType;
        private readonly int m_configId;
        private readonly int m_count;
        private readonly long m_price;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public SendGuildTradePurchaseOrderCreateNetTask(int solarSystemId, StoreItemType storeItemType, int configId, int count, long price)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

