﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    public static class GuildUIHelper
    {
        private static Dictionary<GuildBuildingType, string> m_guildBuildingTypeResPathDic;
        public const string GuildNpcGuildIconEditorResPath = "Assets/GameProject/RuntimeAssets/UI/FunUIImages/Common/Group02_ABS/";
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_SendGuildBaseInfoReq;
        private static DelegateBridge __Hotfix_SendGuildRedPointReq;
        private static DelegateBridge __Hotfix_CollectLogoResForLoad;
        private static DelegateBridge __Hotfix_GetGuildFleetName;
        private static DelegateBridge __Hotfix_GetGuildFleetDefaultName;
        private static DelegateBridge __Hotfix_GetGuildJobResPath;
        private static DelegateBridge __Hotfix_GetGuildJobResPathByJob;
        private static DelegateBridge __Hotfix_GetGuildContributionByRank;
        private static DelegateBridge __Hotfix_GetAllGuildContributeRanResPath;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemFlourishTotalRequest;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemFlourishLevel;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemFlourishProcess;
        private static DelegateBridge __Hotfix_GetGuildBuildingName;
        private static DelegateBridge __Hotfix_GetWarDestStr;
        private static DelegateBridge __Hotfix_GetGuildBuildingNameWithLevel;
        private static DelegateBridge __Hotfix_GetGuildBuildingMaxLevel;
        private static DelegateBridge __Hotfix_GetGuildBuildingTypeDesc;
        private static DelegateBridge __Hotfix_GetGuildBuildingTypeDetailDesc;
        private static DelegateBridge __Hotfix_GetGuildBuildingLevelDesc;
        private static DelegateBridge __Hotfix_GetGuildBuildingSpriteResPath;
        private static DelegateBridge __Hotfix_GetGuildBuildingIsStarMapArtResPath;
        private static DelegateBridge __Hotfix_GetGuildBuildingBigDisplayResPath;
        private static DelegateBridge __Hotfix_GetDelpoyItemIconResPath;
        private static DelegateBridge __Hotfix_CollectRecycleItemIconResPath;
        private static DelegateBridge __Hotfix_IsGuildBuildingInProcessing_1;
        private static DelegateBridge __Hotfix_IsGuildBuildingInProcessing_0;
        private static DelegateBridge __Hotfix_GetGuildBuildingProcessTimeTotal_1;
        private static DelegateBridge __Hotfix_GetGuildBuildingProcessTimeTotal_0;
        private static DelegateBridge __Hotfix_GetGuildBuildingProcessTimeLeft_1;
        private static DelegateBridge __Hotfix_GetGuildBuildingProcessTimeLeft_0;
        private static DelegateBridge __Hotfix_IsGuildBuilding;
        private static DelegateBridge __Hotfix_IsControlHudBuilding;
        private static DelegateBridge __Hotfix_GetSceneInfoBySignalInfo;
        private static DelegateBridge __Hotfix_GetGuildSentryShipLimitTypes;
        private static DelegateBridge __Hotfix_SendGuildSolarSystemInfoReq;
        private static DelegateBridge __Hotfix_OutputGuildBuildingUpdateRequiredError;
        private static DelegateBridge __Hotfix_CheckGuildBuildUpdateRequirement;
        private static DelegateBridge __Hotfix_GetGuildSpeedUpLevelDesc;
        private static DelegateBridge __Hotfix_GetGuildProductionLineName;
        private static DelegateBridge __Hotfix_GetGuildProductionData;
        private static DelegateBridge __Hotfix_GetGuildPurchaseItemTypes;
        private static DelegateBridge __Hotfix_GetFleetMemberPositionStr_2;
        private static DelegateBridge __Hotfix_GetFleetMemberPositionStr_0;
        private static DelegateBridge __Hotfix_GetFleetMemberPositionStr_1;
        private static DelegateBridge __Hotfix_GetGuildFleetPositionName;
        private static DelegateBridge __Hotfix_SendGuildOccupiedSolarSystemInfoReq;
        private static DelegateBridge __Hotfix_CompareForGuildBuildlingList;
        private static DelegateBridge __Hotfix_GetGuildCodeNameShowStr;
        private static DelegateBridge __Hotfix_GetKill2DestoryRate;
        private static DelegateBridge __Hotfix_IsNpcDefender;
        private static DelegateBridge __Hotfix_CollectSSystemOccupyFactionIconResPath;
        private static DelegateBridge __Hotfix_GetColorFromString;
        private static DelegateBridge __Hotfix_get_DefaultLogoResPath;
        private static DelegateBridge __Hotfix_GetNpcLogoResPath;
        private static DelegateBridge __Hotfix_GetLogoResByLogoInfo_1;
        private static DelegateBridge __Hotfix_GetLogoResByLogoInfo_0;
        private static DelegateBridge __Hotfix_GetAllRegionResources;
        private static DelegateBridge __Hotfix_GetLogoResPathById;
        private static DelegateBridge __Hotfix_GetDefaultLogoResPathById;
        private static DelegateBridge __Hotfix_GetLogoColorById;
        private static DelegateBridge __Hotfix_GetDefaultIdOfLogo;
        private static DelegateBridge __Hotfix_CollectAllGuildBuildingRes;
        private static DelegateBridge __Hotfix_GetGuildBuildingTypeResPath;
        private static DelegateBridge __Hotfix_IsSolarSystemInSettlementStage;
        private static DelegateBridge __Hotfix_CheckGuildPermissionForBlackMarket;
        private static DelegateBridge __Hotfix_GetGuildFilteredItemList;
        private static DelegateBridge __Hotfix_TryLockingGuildFlagShipHangar;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarLockingReq;
        private static DelegateBridge __Hotfix_IsFlagShipShowDestroyState;
        private static DelegateBridge __Hotfix_ConvertToGuildAllianceLanguageType;

        [MethodImpl(0x8000)]
        public static bool CheckGuildBuildUpdateRequirement(int solarSystemId, GuildBuildingInfo buildingInfo, out string requirementStr)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckGuildPermissionForBlackMarket()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectAllGuildBuildingRes(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectLogoResForLoad(GuildLogoInfo logoInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectRecycleItemIconResPath(List<string> resList, GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectSSystemOccupyFactionIconResPath(int solarSystemId, List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static int CompareForGuildBuildlingList(GuildBuildingInfo a, GuildBuildingInfo b)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildAllianceLanguageType ConvertToGuildAllianceLanguageType(LanguageType language)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetAllGuildContributeRanResPath(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetAllRegionResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static Color GetColorFromString(string colorData)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetDefaultIdOfLogo(LogoHelper.LogoPositionType position, LogoHelper.LogoUsageType usage)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDefaultLogoResPathById(LogoHelper.LogoPositionType position, LogoHelper.LogoUsageType usage)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDelpoyItemIconResPath(GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFleetMemberPositionStr(List<FleetPosition> positionList, string separator)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFleetMemberPositionStr(List<uint> positionList, string separator)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFleetMemberPositionStr(GuildFleetMemberInfo basicInfo, string separator, bool showSubstituteSeq = true)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingBigDisplayResPath(GuildBuildingType buildingType, int buildingLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingIsStarMapArtResPath(GuildBuildingType buildingType, int buildingLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingLevelDesc(GuildBuildingType buildingType, int level, string levelZeroDescription = "", string levelMaxDescription = "")
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildBuildingMaxLevel(GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingName(GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingNameWithLevel(GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static double GetGuildBuildingProcessTimeLeft(GuildBuildingInfo buildingInfo, GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static double GetGuildBuildingProcessTimeLeft(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildBuildingProcessTimeTotal(GuildBuildingInfo buildingInfo, GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildBuildingProcessTimeTotal(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo, bool isNextLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingSpriteResPath(GuildBuildingType buildingType, int buildingLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingTypeDesc(GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingTypeDetailDesc(GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildBuildingTypeResPath(GuildBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildCodeNameShowStr(string codeName)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildContributionByRank(int rank)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ILBStoreItemClient> GetGuildFilteredItemList(List<Func<ILBStoreItemClient, bool>> filterlist)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildFleetDefaultName(int fleetSeqId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildFleetName(string orgFleetName, int fleetSeqId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildFleetPositionName(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetGuildJobResPath(List<string> resPaths)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildJobResPathByJob(GuildJobType job)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetGuildProductionData(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildProductionLineName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerable<NpcShopItemCategory> GetGuildPurchaseItemTypes()
        {
        }

        [MethodImpl(0x8000)]
        public static List<ShipType> GetGuildSentryShipLimitTypes(GuildSentryInterestScene guildSentrySignalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildSolarSystemFlourishLevel(int flourishValue, int maxValue, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetGuildSolarSystemFlourishProcess(int level, int maxLevel, float currLevelProcess)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildSolarSystemFlourishTotalRequest(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildSpeedUpLevelDesc(GuildProductionSpeedUpLevel level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetKill2DestoryRate(double kill, double loss)
        {
        }

        [MethodImpl(0x8000)]
        public static Color GetLogoColorById(int id, LogoHelper.LogoPositionType position)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetLogoResByLogoInfo(AllianceLogoInfo logoInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetLogoResByLogoInfo(GuildLogoInfo logoInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetLogoResPathById(int id, LogoHelper.LogoPositionType position, LogoHelper.LogoUsageType usage)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetNpcLogoResPath(int npcGuildId, List<string> resList, GuildLogoInfo logoInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSceneInfo GetSceneInfoBySignalInfo(GuildSentryInterestScene guildSentrySignalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWarDestStr(GuildBattleType battleType, GuildBuildingType buildType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsControlHudBuilding(GuildBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFlagShipShowDestroyState(IGuildFlagShipHangarDataContainer shipHangar, int index, out IStaticFlagShipDataContainer ship)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGuildBuilding(GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGuildBuildingInProcessing(GuildBuildingInfo buildingInfo, GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGuildBuildingInProcessing(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo, int timeRevise)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsNpcDefender(this GuildBattleReportInfo battleReport)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSolarSystemInSettlementStage(GuildSolarSystemInfoClient solarSystemInfo, int solarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static void OutputGuildBuildingUpdateRequiredError(int solarSystemId, GuildBuildingInfo buildingInfo, bool isDeploy)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildBaseInfoReq(uint guildId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void SendGuildFlagShipHangarLockingReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildOccupiedSolarSystemInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildRedPointReq(bool useNetWorkTask, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildSolarSystemInfoReq(int solarSystemId, Action<bool> onEnd, bool forceRefresh = false, bool skipRelogin = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void TryLockingGuildFlagShipHangar(bool needConfirm, Action<bool> onEnd)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string DefaultLogoResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildProductionData>c__AnonStorey4
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(((GetGuildProductionInfoReqNetTask) task).Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildPurchaseItemTypes>c__Iterator0 : IEnumerable, IEnumerable<NpcShopItemCategory>, IEnumerator, IDisposable, IEnumerator<NpcShopItemCategory>
        {
            internal NpcShopItemCategory $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = NpcShopItemCategory.NpcShopItemCategory_Ship;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        break;

                    case 1:
                        this.$current = NpcShopItemCategory.NpcShopItemCategory_Weapon;
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        break;

                    case 2:
                        this.$current = NpcShopItemCategory.NpcShopItemCategory_Device;
                        if (!this.$disposing)
                        {
                            this.$PC = 3;
                        }
                        break;

                    case 3:
                        this.$current = NpcShopItemCategory.NpcShopItemCategory_Component;
                        if (!this.$disposing)
                        {
                            this.$PC = 4;
                        }
                        break;

                    case 4:
                        this.$current = NpcShopItemCategory.NpcShopItemCategory_Ammo;
                        if (!this.$disposing)
                        {
                            this.$PC = 5;
                        }
                        break;

                    case 5:
                        this.$PC = -1;
                        goto TR_0000;

                    default:
                        goto TR_0000;
                }
                return true;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<NpcShopItemCategory> IEnumerable<NpcShopItemCategory>.GetEnumerator() => 
                ((Interlocked.CompareExchange(ref this.$PC, 0, -2) != -2) ? new GuildUIHelper.<GetGuildPurchaseItemTypes>c__Iterator0() : this);

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<BlackJack.ConfigData.NpcShopItemCategory>.GetEnumerator();

            NpcShopItemCategory IEnumerator<NpcShopItemCategory>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <SendGuildBaseInfoReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFlagShipHangarLockingReq>c__AnonStorey7
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildFlagShipHangarLockingReqNetTask task2 = task as GuildFlagShipHangarLockingReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if ((task2.Result == 0) || (task2.Result == -3024))
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildOccupiedSolarSystemInfoReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task2 = task as GuildOccupiedSolarSystemInfoReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildRedPointReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                this.onEnd(true);
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildSolarSystemInfoReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildSolarSystemInfoReqNetTask task2 = task as GuildSolarSystemInfoReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryLockingGuildFlagShipHangar>c__AnonStorey6
        {
            internal Action<bool> onEnd;

            internal void <>m__0()
            {
                GuildUIHelper.SendGuildFlagShipHangarLockingReq(this.onEnd);
            }

            internal void <>m__1()
            {
                if (this.onEnd != null)
                {
                    this.onEnd(false);
                }
            }
        }
    }
}

