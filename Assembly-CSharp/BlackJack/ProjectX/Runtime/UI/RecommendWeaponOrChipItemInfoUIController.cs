﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RecommendWeaponOrChipItemInfoUIController : UIControllerBase
    {
        public FakeLBStoreItem m_FakeLBStoreItem;
        private CommonItemIconUIController m_commenItemUICtr;
        [AutoBind("./Content/CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemUIDummy;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommendItemUIState;
        [AutoBind("./Content/OpenButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecommendItemOpenBtn;
        [AutoBind("./Content/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RecommendItemCloseBtn;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRecommendItem;
        private static DelegateBridge __Hotfix_ClearRecommend;

        [MethodImpl(0x8000)]
        public void ClearRecommend()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRecommendItem(FakeLBStoreItem fakeItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

