﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GetOrUnLoadItemWindowUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClick;
        private const string AssetNameItemUI = "ItemUIPrefab";
        private List<CommonItemIconUIController> m_itemCtrlList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backGroundButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_confirmButton;
        [AutoBind("./ItemListScrollView/Viewport/Content/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummy;
        [AutoBind("./ItemListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemRoot;
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoDummy;
        [AutoBind("./CurrencyGroup/C-Group", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_bindMoney;
        [AutoBind("./CurrencyGroup/C-Group/C-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyCount;
        [AutoBind("./CurrencyGroup/S-Group", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tradeMoney;
        [AutoBind("./CurrencyGroup/S-Group/S-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyCount;
        [AutoBind("./CurrencyGroup/IR-Group", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_realMoney;
        [AutoBind("./CurrencyGroup/IR-Group/IR-Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_realMoneyCount;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateItemListInfo;
        private static DelegateBridge __Hotfix_CreatWindowUIPrecess;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPosType;
        private static DelegateBridge __Hotfix_InitCommonUIStateUICtrl;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatWindowUIPrecess(bool isShow, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos(int index)
        {
        }

        [MethodImpl(0x8000)]
        public ItemSimpleInfoUITask.PositionType GetItemSimpleInfoPosType(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void InitCommonUIStateUICtrl(GameObject itemObj, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemListInfo(List<ILBStoreItemClient> itemList, List<CurrencyUpdateInfo> currencyList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

