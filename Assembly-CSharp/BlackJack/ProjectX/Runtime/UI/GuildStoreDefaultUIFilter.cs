﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildStoreDefaultUIFilter : ItemStoreUIFilterBase
    {
        private int m_currDropdownValue;
        private GuildItemListUIController.GuildStoreTabType m_currGuildTabType;
        private bool m_isFixedItemType;
        private bool m_isFixedDropdownType;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache7;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ResetState;
        private static DelegateBridge __Hotfix_SetFixedItemType;
        private static DelegateBridge __Hotfix_SetFixedDropDownType;
        private static DelegateBridge __Hotfix_GetItemFilters;
        private static DelegateBridge __Hotfix_GetDropDownMenuList;
        private static DelegateBridge __Hotfix_GetDropDownListByType;
        private static DelegateBridge __Hotfix_get_CurrDropdownValue;
        private static DelegateBridge __Hotfix_set_CurrDropdownValue;
        private static DelegateBridge __Hotfix_get_CurrGuildTabType;
        private static DelegateBridge __Hotfix_set_CurrGuildTabType;

        [MethodImpl(0x8000)]
        public List<string> GetDropDownListByType(GuildItemListUIController.GuildStoreTabType type, bool needShowAll = false)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> GetDropDownMenuList(bool needShowAll = false)
        {
        }

        [MethodImpl(0x8000)]
        public List<Func<ILBStoreItemClient, bool>> GetItemFilters()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFixedDropDownType(int dropdownIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFixedItemType(GuildItemListUIController.GuildStoreTabType guildType)
        {
        }

        public int CurrDropdownValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GuildItemListUIController.GuildStoreTabType CurrGuildTabType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

