﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class PlayerVoiceHandleThread
    {
        public int m_size;
        public bool m_startHandle;
        public Thread m_voiceHandleThread;
        private Queue<VoicePacket> m_inputQueue;
        private Queue<VoicePacket> m_outputQueue;
        private readonly object m_lock;
        private readonly object m_seclock;
        private static PlayerVoiceHandleThread m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_AddDataToInputBuffer;
        private static DelegateBridge __Hotfix_AddDataContentToInputBuffer;
        private static DelegateBridge __Hotfix_AddDataHeadToInputBuffer;
        private static DelegateBridge __Hotfix_AddDataEndToInputBuffer;
        private static DelegateBridge __Hotfix_GetInputBufferData;
        private static DelegateBridge __Hotfix_AddDataToOutputBuffer;
        private static DelegateBridge __Hotfix_GetOutputBufferData;
        private static DelegateBridge __Hotfix_GetTickTime;
        private static DelegateBridge __Hotfix_VoiceThreadProc;
        private static DelegateBridge __Hotfix_ByteArrayListToByteArray;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_ClearInstance;

        [MethodImpl(0x8000)]
        private PlayerVoiceHandleThread()
        {
        }

        [MethodImpl(0x8000)]
        public void AddDataContentToInputBuffer(float[] audioData)
        {
        }

        [MethodImpl(0x8000)]
        public void AddDataEndToInputBuffer(ChatChannel chatChannel, ChatContentVoice chatInfo, bool isValid)
        {
        }

        [MethodImpl(0x8000)]
        public void AddDataHeadToInputBuffer()
        {
        }

        [MethodImpl(0x8000)]
        private void AddDataToInputBuffer(VoicePacket packet)
        {
        }

        [MethodImpl(0x8000)]
        private void AddDataToOutputBuffer(VoicePacket packet)
        {
        }

        [MethodImpl(0x8000)]
        private byte[] ByteArrayListToByteArray(List<byte[]> list)
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearInstance()
        {
        }

        [MethodImpl(0x8000)]
        private VoicePacket GetInputBufferData()
        {
        }

        [MethodImpl(0x8000)]
        public VoicePacket GetOutputBufferData()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTickTime()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        [MethodImpl(0x8000)]
        private void VoiceThreadProc()
        {
        }

        public static PlayerVoiceHandleThread Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class VoicePacket
        {
            public float[] m_srcData;
            public ChatContentVoice m_chatInfo;
            public ChatChannel m_chatChannel;
            public bool m_isCompressed;
            public BlackJack.ProjectX.Runtime.UI.DataType m_type;
            public bool m_isValid;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

