﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildInfoEditNetTask : NetWorkTransactionTask
    {
        public int m_result;
        public readonly GuildInfoEditAction m_action;
        private readonly object[] m_parameters;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildRequestAck;
        private static DelegateBridge __Hotfix_GetPlayerContext;

        [MethodImpl(0x8000)]
        public GuildInfoEditNetTask(GuildInfoEditAction action, object[] parameters)
        {
        }

        [MethodImpl(0x8000)]
        private ProjectXPlayerContext GetPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildRequestAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public enum GuildInfoEditAction
        {
            ChangeName,
            ChangeRegion,
            ChangeAnnouncement,
            ChangeManifesto,
            Dismiss,
            Undismiss,
            SetJoinRight,
            Quit
        }
    }
}

