﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildMemberJobCtrlChangeJobHintController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildMemberInfo, GuildJobType, bool> EventOnConfirmClick;
        private bool m_isAppoint = true;
        private GuildJobType m_job;
        private GuildMemberInfo m_memberInfo;
        [AutoBind("./ContentGroup/TextGroup/MemberName", AutoBindAttribute.InitState.NotInit, false)]
        public Text PrefixText;
        [AutoBind("./ContentGroup/TextGroup/JobText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PostfixText;
        [AutoBind("./ContentGroup/TextGroup/JobIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image JobIcon;
        [AutoBind("./ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ConfirmButton;
        [AutoBind("./ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RootState;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button BkgButton;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmClick;

        public event Action<GuildMemberInfo, GuildJobType, bool> EventOnConfirmClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(bool isAppoint, GuildJobType job, GuildMemberInfo memberInfo, Dictionary<string, Object> resData)
        {
        }
    }
}

