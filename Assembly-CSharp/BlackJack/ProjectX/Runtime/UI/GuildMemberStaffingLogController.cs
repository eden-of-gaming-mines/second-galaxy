﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberStaffingLogController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGetMoreBtnClick;
        private bool m_hasMoreLog;
        private List<GuildStaffingLogInfo> m_logList;
        [AutoBind("./PeopleGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./PeopleGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_content;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopScrollView;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [AutoBind("./Empty", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_empty;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_OnMemberItemCreated;
        private static DelegateBridge __Hotfix_OnMemberItemFill;
        private static DelegateBridge __Hotfix_OnGetMoreBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnGetMoreBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnGetMoreBtnClick;

        public event Action EventOnGetMoreBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool isImmediateComplete)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetMoreBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<GuildStaffingLogInfo> logList, bool hasMore)
        {
        }
    }
}

