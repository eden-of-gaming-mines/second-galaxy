﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WormholeMenuItemUIController : MenuItemUIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        private GlobalSceneInfo m_wormholeGateInfo;
        private ConfigDataWormholeStarGroupInfo m_wormholeConfInfo;
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_wormholeNameText;
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./Detail/JumpButton/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_jumpButtonShowArea;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowIn/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        [AutoBind("./Detail/AllowIn/LimitLevelValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_limitLv;
        [AutoBind("./Detail/AllowIn/RecommendLevelValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_recommendLv;
        [AutoBind("./Detail/AllowIn/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./Detail/LossWarningButton/SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningPos;
        private CommonUIStateController[] m_allowInShipIconList;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetWormholeMenuItemByGlobalSceneInfo;
        private static DelegateBridge __Hotfix_SetWormholeMenuItemByConfInfo;
        private static DelegateBridge __Hotfix_GetWormholeStargateSceneInfo;
        private static DelegateBridge __Hotfix_GetWormholeConfigStarGroupInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetLossWarningButtonPos;
        private static DelegateBridge __Hotfix_GetLossWarningButtonSize;
        private static DelegateBridge __Hotfix_SetCaptainShipInfoUIItemList;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_get_AllowInShipIconList;

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetLossWarningButtonPos()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector2 GetLossWarningButtonSize()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeStarGroupInfo GetWormholeConfigStarGroupInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo GetWormholeStargateSceneInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainShipInfoUIItemList(ConfigDataWormholeStarGroupInfo wormholeStargroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWormholeMenuItemByConfInfo(ConfigDataWormholeStarGroupInfo wormholeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWormholeMenuItemByGlobalSceneInfo(GlobalSceneInfo wormholeSceneInfo)
        {
        }

        private CommonUIStateController[] AllowInShipIconList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetCaptainShipInfoUIItemList>c__AnonStorey0
        {
            internal int index;

            internal bool <>m__0(ShipType e) => 
                (e == (this.index + 1));
        }
    }
}

