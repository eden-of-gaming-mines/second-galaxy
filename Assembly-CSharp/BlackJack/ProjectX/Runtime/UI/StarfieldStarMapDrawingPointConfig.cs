﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapDrawingPointConfig
    {
        [Header("绘制的星域图中，点的最小宽度")]
        public float m_pointWidthMin;
        [Header("绘制的星域图中，点的最大宽度")]
        public float m_pointWidthMax;
        [Header("点取最小宽度时对应的缩放比")]
        public float m_scaleValueForPointWidthMin;
        [Header("点取最大宽度时对应的缩放比（必须大于m_scaleValueForPointWidthMin）")]
        public float m_scaleValueForPointWidthMax;
        [Header("恒星系点prefab开始fadeout时的缩放比")]
        public float m_scaleValueForSSPrefabFadeOutStart;
        [Header("恒星系点prefab，fadeout完成时的缩放比")]
        public float m_scaleValueForSSPrefabFadeOutEnd;
        [Header("恒星系点prefab完全不透明时的透明度，0-1，1是完全不透明")]
        public float m_SSPrefabAlphaMax;
        [Header("恒星系点简化显示开始fadein时的缩放比")]
        public float m_scaleValueForSSPointFadeInStart;
        [Header("恒星系点简化显示，fadein完成时的缩放比")]
        public float m_scaleValueForSSPointFadeInEnd;
        [Header("恒星系点简化显示，完全不透明时的透明度，0-1，1是完全不透明")]
        public float m_SSPointAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

