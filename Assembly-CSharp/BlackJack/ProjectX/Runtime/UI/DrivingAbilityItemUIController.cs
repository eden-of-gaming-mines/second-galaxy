﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DrivingAbilityItemUIController : UIControllerBase
    {
        private CommonItemIconUIController m_shipCtrl;
        private List<CharacteristicItemUIController> m_propertyListCtrl;
        private const string PrefabAsset_CommonItem = "CommonItemUIPrefab";
        public const string AbilityState_DrivingLiscen = "LiscenseGet";
        public const string AbilityState_MiddleSlot = "MiddleSlotGet";
        public const string AbilityState_LowSlot = "LowSlotGet";
        public const string AbilityState_None = "None";
        [AutoBind("./ShipInfo/IconNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipName;
        [AutoBind("./ShipInfo/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ShipDummy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AbilityGetCtrl;
        [AutoBind("./Property", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipCharacteristicItemGoInstance;
        [AutoBind("./SuperDeviceGroup/PropertyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponEquipDesc;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateDrivingShipInfo;
        private static DelegateBridge __Hotfix_CreateCharacteristicItemListWithInfo;

        [MethodImpl(0x8000)]
        private void CreateCharacteristicItemListWithInfo(List<PropertyValueStringPair> infoList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDrivingShipInfo(ConfigDataSpaceShipInfo shipConf, Dictionary<string, UnityEngine.Object> resDict, string State, int level = 1)
        {
        }
    }
}

