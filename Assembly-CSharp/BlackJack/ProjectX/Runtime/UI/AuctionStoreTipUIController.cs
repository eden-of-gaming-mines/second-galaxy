﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class AuctionStoreTipUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PointerEventData, Action<PointerEventData>> EventOnBGPointClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController m_bgPassEvent;
        [AutoBind("./TextRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_textRoot;
        public const string StateClose = "Close";
        public const string StatePrice = "ShowUnit";
        public const string StateAuctionFee = "ShowCustodr";
        public const string StateTradeTax = "ShowToday";
        public const string StateFreezing = "ShowFreeze";
        public const string StateExpireTime = "ShowUpperLimit";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetTextRootPos;
        private static DelegateBridge __Hotfix_GetShowStateUIProcess;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBGPointClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGPointClick;

        public event Action<PointerEventData, Action<PointerEventData>> EventOnBGPointClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetShowStateUIProcess(string state, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick(PointerEventData data, Action<PointerEventData> act)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTextRootPos(Vector3 pos)
        {
        }
    }
}

