﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RightPopupMenuItemProviderController : UIControllerBase
    {
        [AutoBind("./QuestItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_questMenuItemObj;
        [AutoBind("./QuestSignalItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_questSignalItemObj;
        [AutoBind("./DelegateSignalItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_delegateSignalItemObj;
        [AutoBind("./PvpSignalItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_pvpSignalItemObj;
        [AutoBind("./PvpSignalItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_pvpSignalItemRootObj;
        [AutoBind("./SpaceSignalItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_spaceSignalItemObj;
        [AutoBind("./PlanetItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_planetItemObj;
        [AutoBind("./StarItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starItemObj;
        [AutoBind("./SpaceStationItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_spaceStationItemObj;
        [AutoBind("./StarGateItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_stargateItemObj;
        [AutoBind("./WormholeItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_wormholeItemObj;
        [AutoBind("./InfectItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_infectItemObj;
        [AutoBind("./UnknownWormholeItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_unknownWormholeItemObj;
        [AutoBind("./RescuelItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rescueItemObj;
        [AutoBind("./GuildSentrySignalItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_guildSentrySignalItemObj;
        [AutoBind("./PoolParent", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ReturnObjectToPool;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_GetQuestItem;
        private static DelegateBridge __Hotfix_GetQuestSignalItem;
        private static DelegateBridge __Hotfix_GetDelegateSignalItem;
        private static DelegateBridge __Hotfix_GetPvpSignalItem;
        private static DelegateBridge __Hotfix_GetPvpSignalItemRoot;
        private static DelegateBridge __Hotfix_GetSpaceSignalItem;
        private static DelegateBridge __Hotfix_GetCelestialPlanetItem;
        private static DelegateBridge __Hotfix_GetCelestialStarItem;
        private static DelegateBridge __Hotfix_GetSpaceStationItem;
        private static DelegateBridge __Hotfix_GetStargateItem;
        private static DelegateBridge __Hotfix_GetWormholeItem;
        private static DelegateBridge __Hotfix_GetUnknownWormholeItem;
        private static DelegateBridge __Hotfix_GetInfectItem;
        private static DelegateBridge __Hotfix_GetRescueItem;
        private static DelegateBridge __Hotfix_GetGuildSentrySignalItem;

        [MethodImpl(0x8000)]
        public CelestialPlanetMenuItemUIController GetCelestialPlanetItem()
        {
        }

        [MethodImpl(0x8000)]
        public CelestialStarMenuItemUIController GetCelestialStarItem()
        {
        }

        [MethodImpl(0x8000)]
        public DelegateSignalMenuItemUIController GetDelegateSignalItem()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentrySignalMenuItemUIController GetGuildSentrySignalItem()
        {
        }

        [MethodImpl(0x8000)]
        public InfectMenuItemUIController GetInfectItem()
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemUIController GetPvpSignalItem()
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemRootUIController GetPvpSignalItemRoot()
        {
        }

        [MethodImpl(0x8000)]
        public QuestMenuItemUIController GetQuestItem()
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController GetQuestSignalItem()
        {
        }

        [MethodImpl(0x8000)]
        public RescueMenuItemUIController GetRescueItem()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSignalMenuItemUIController GetSpaceSignalItem()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationMenuItemUIController GetSpaceStationItem()
        {
        }

        [MethodImpl(0x8000)]
        public StargateMenuItemUIController GetStargateItem()
        {
        }

        [MethodImpl(0x8000)]
        public UnknownWormholeMenuItemUIController GetUnknownWormholeItem()
        {
        }

        [MethodImpl(0x8000)]
        public WormholeMenuItemUIController GetWormholeItem()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ReturnObjectToPool(GameObject go)
        {
        }
    }
}

