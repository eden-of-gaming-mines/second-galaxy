﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipHangarUIController : UIControllerBase
    {
        protected UIMode m_currMode;
        protected bool m_isRenamePanelShow;
        protected bool m_isShipPackPanelShow;
        public static string ErrorInfoPanelButtonState_Confirm;
        public static string ErrorInfoPanelButtonState_CancelAndGo;
        private const string ShowState = "Show";
        private const string CloseState = "Close";
        private SystemDescriptionController m_descriptionCtrl;
        [AutoBind("./ShipListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public HangarShipListUIController m_hangarShipListUICtrl;
        [AutoBind("./ShipUniqueWeaponEquipTipWindows", AutoBindAttribute.InitState.NotInit, false)]
        public HangarShipUniqueWeaponEquipTipWndUIController m_shipUniqueWeaponEquipTipWindowUICtrl;
        [AutoBind("./ShipFeatureTipWindow", AutoBindAttribute.InitState.NotInit, false)]
        public HangarShipFeaturesTipWndUIController m_shipFeaturesTipWindowUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./FeaturesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FeaturesButton;
        [AutoBind("./CancelWatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelWatchButton;
        [AutoBind("./ShipPackPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipPackStateCtrl;
        [AutoBind("./ShipPackPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipPackConfirmButton;
        [AutoBind("./ShipPackPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipPackCancelButton;
        [AutoBind("./ShipRenamePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipRenameStateCtrl;
        [AutoBind("./ShipRenamePanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipRenameConfirmButton;
        [AutoBind("./ShipRenamePanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipRenameCancelButton;
        [AutoBind("./ShipRenamePanel/ShipNameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField ShipRenameInputField;
        [AutoBind("{./CommonEquipInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonEquipInfoPanelDummy;
        [AutoBind("./ErrorInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ErrorInfoPanelStateCtrl;
        [AutoBind("./ErrorInfoPanel/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ErrorInfoPanelButtonGroupStateCtrl;
        [AutoBind("./ErrorInfoPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ErrorInfoPanelBackGroupButton;
        [AutoBind("./ErrorInfoPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ErrorInfoPanelConfirmButton;
        [AutoBind("./ErrorInfoPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ErrorInfoPanelCancelButton;
        [AutoBind("./ErrorInfoPanel/ButtonGroup/GoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ErrorInfoPanelGoButton;
        [AutoBind("./ErrorInfoPanel/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ErrorInfoPanelDetailText;
        [AutoBind("./AssemblyOptimizationNotice", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AssemblyOptimizationNoticeStateCtrl;
        [AutoBind("./AssemblyOptimizationNotice/ShowOrHideControlNode/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AssemblyOptimizationNoticeButton;
        [AutoBind("./AssemblyOptimizationInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public HangarShipAssemblyOptimizationInfoPanelUIController AssemblyOptimizationInfoPanelUICtrl;
        [AutoBind("./AssemblyOptimizationInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AssemblyOptimizationInfoPanelStateCtrl;
        [AutoBind("./ShipWeaponEquipSlotGroup/ShipPackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ShipPackButton;
        [AutoBind("./ShipWeaponEquipSlotGroup/ShipPackButton/TouchImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ShipPackButtonClickImage;
        [AutoBind("./ShipWeaponEquipSlotGroup/ShipTemplateButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ShipTemplateButton;
        [AutoBind("./ShipWeaponEquipSlotGroup/ShipTemplateButton/TouchImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ShipTemplateButtonClickImage;
        [AutoBind("./ShipWeaponEquipSlotGroup/ItemStoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ItemStoreButton;
        [AutoBind("./ShipWeaponEquipSlotGroup/ItemStoreButton/Notice", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreButtonNoticeRoot;
        [AutoBind("./ShipWeaponEquipSlotGroup/AmmoReloadButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button AmmoReloadButton;
        [AutoBind("./ShipWeaponEquipSlotGroup/AmmoReloadButton/TouchImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AmmoReloadButtonClickImage;
        [AutoBind("./ShipWeaponEquipSlotGroup/AmmoReloadButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AmmoReloadButtonStateCtrl;
        public bool m_isErrorInfoOpen;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_ClearOnPause;
        private static DelegateBridge __Hotfix_GetUIMode;
        private static DelegateBridge __Hotfix_ShowShipPackPanel;
        private static DelegateBridge __Hotfix_IsShipPackPanelShow;
        private static DelegateBridge __Hotfix_ShowShipRenamePanel;
        private static DelegateBridge __Hotfix_IsRenamePanelShow;
        private static DelegateBridge __Hotfix_CheckShipRenameLegal;
        private static DelegateBridge __Hotfix_GetShipRenameStr;
        private static DelegateBridge __Hotfix_EnableBackGroundButton;
        private static DelegateBridge __Hotfix_ShowErrorInfoPanel;
        private static DelegateBridge __Hotfix_UpdateFuntionButton;
        private static DelegateBridge __Hotfix_ShowOrHideSystemDescriptionButton;
        private static DelegateBridge __Hotfix_GetWeaponEquipInfoPanelDummyLocation;
        private static DelegateBridge __Hotfix_ShowOrHideUniqueEquipWeaponTip;
        private static DelegateBridge __Hotfix_ShowOrHideShipFetureInfoTip;
        private static DelegateBridge __Hotfix_HideUniqueEquipWeaponTip;
        private static DelegateBridge __Hotfix_HideShipFetureInfoTip;
        private static DelegateBridge __Hotfix_CreateMainWndWindowProcess;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationNotice;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationInfoPanel;

        [MethodImpl(0x8000)]
        public bool CheckShipRenameLegal()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearOnPause()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWndWindowProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBackGroundButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipRenameStr()
        {
        }

        [MethodImpl(0x8000)]
        public UIMode GetUIMode()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWeaponEquipInfoPanelDummyLocation()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideShipFetureInfoTip()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideUniqueEquipWeaponTip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRenamePanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipPackPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess SetUIMode(UIMode mode, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationInfoPanel(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationNotice(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowErrorInfoPanel(bool isShow, string buttonState, string errorStr)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideShipFetureInfoTip(ILBStaticPlayerShip staticShip, bool showDrivingLisenceButton, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSystemDescriptionButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideUniqueEquipWeaponTip(HangarShipUniqueWeaponEquipTipWndUIController.UniqueWeaponEquipTipState state, ILBStaticPlayerShip staticShip, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowShipPackPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowShipRenamePanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFuntionButton(ILBStaticPlayerShip shipInfo)
        {
        }

        [CompilerGenerated]
        private sealed class <CreateMainWndWindowProcess>c__AnonStorey0
        {
            internal string stateName;
            internal bool immediateComplete;
            internal bool allowToRefreshSameState;
            internal ShipHangarUIController $this;

            internal void <>m__0(Action<bool> end)
            {
                this.$this.m_stateCtrl.SetToUIStateExtra(this.stateName, this.immediateComplete, this.allowToRefreshSameState, end);
            }
        }

        public enum UIMode
        {
            ShipListOverViewNormal,
            ShipListOverViewWatch,
            ShipListOverViewNoShip,
            ShipListOverViewNoShipWatch,
            HideAll,
            Close
        }
    }
}

