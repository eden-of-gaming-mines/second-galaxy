﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapInfectSimpleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnInfectInfoButtonClick;
        [AutoBind("./InfectProgress", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectProgressText;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_infectInfoButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowOrHideInfectSimpleUI;
        private static DelegateBridge __Hotfix_SetInfectSimpleUIInfo;
        private static DelegateBridge __Hotfix_OnInfectInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnInfectInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnInfectInfoButtonClick;

        public event Action EventOnInfectInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInfectInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfectSimpleUIInfo(float infectProgress)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideInfectSimpleUI(bool isShow)
        {
        }
    }
}

