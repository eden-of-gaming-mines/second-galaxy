﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarShipSupplementFuelUIContoller : UIControllerBase
    {
        private bool m_isShow;
        private const string ShipSupplementFuelAssetName = "ShipFuelItem";
        private string m_fuelChangValue;
        private const string StateShow = "PanelOpen";
        private const string StateClose = "PanelClose";
        private const string StateNormal = "Normal";
        private const string StateRed = "Red";
        private const string StateGreen = "Green";
        private int m_hangarLevel;
        private int m_unlockSlotCount;
        private long m_fuelTotalCount;
        private List<int> m_changedFuelList;
        private List<IStaticFlagShipDataContainer> m_hangarShipListCache;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnGuildFlagHangarShipSupplementFuelItemClick;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./GuildStoreItemCount", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildStoreItemCountStateCtrl;
        [AutoBind("./GuildStoreItemCount", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildStoreItemCount;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollRect;
        [AutoBind("./ScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StateCtrl;
        private static DelegateBridge __Hotfix_ShowOrHidePanel;
        private static DelegateBridge __Hotfix_UpdateViewGuildFlagShipHangarListUI;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_GetChangeFuelList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnGuildFlagHangarShipSupplementFuelItemClick;
        private static DelegateBridge __Hotfix_OnGuildFlagHangarShipSupplementFuelItemFill;
        private static DelegateBridge __Hotfix_OnFuelValueChanged;
        private static DelegateBridge __Hotfix_OnFullFuelButtonClick;
        private static DelegateBridge __Hotfix_GetFuelStoreCount;
        private static DelegateBridge __Hotfix_GetTotalChange;
        private static DelegateBridge __Hotfix_UpdateFuelStoreItemCount;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagHangarShipSupplementFuelItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagHangarShipSupplementFuelItemClick;

        public event Action<GuildFlagShipHangarShipSupplementFuelItemUIContoller> EventOnGuildFlagHangarShipSupplementFuelItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public List<int> GetChangeFuelList()
        {
        }

        [MethodImpl(0x8000)]
        private long GetFuelStoreCount()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTotalChange()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFuelValueChanged(int fuelCount, GuildFlagShipHangarShipSupplementFuelItemUIContoller ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFullFuelButtonClick(int currFuelCount, GuildFlagShipHangarShipSupplementFuelItemUIContoller ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagHangarShipSupplementFuelItemClick(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagHangarShipSupplementFuelItemFill(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHidePanel(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFuelStoreItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewGuildFlagShipHangarListUI(List<IStaticFlagShipDataContainer> hangarShipList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int unlockSlotCount, int buildingLevel, long fuelStoreCount, int startIndex = 0)
        {
        }
    }
}

