﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WarReportManagerUITask : GuildUITaskBase
    {
        private bool m_isWarOverviewPanelResLoadEnd;
        private bool m_isLossRatioPanelResLoadEnd;
        private bool m_isLossShipPanelResLoadEnd;
        private bool m_isRankingListPanelResLoadEnd;
        private TabType m_currTab;
        private ulong m_reportId;
        private WarOverviewUITask m_overviewUITask;
        private WarPersonalKillRankingListUITask m_rankingListUITask;
        private WarShipLossUITask m_shipLossUITask;
        private WarGuildKillLostUITask m_guildKillLossListUITask;
        private GuildBattleReportInfo m_reportInfo;
        private WarReportBgUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKey_ReportId4Self = "GuildBattleReportInfoId";
        public const string ParamKey_NeedResumeSpaceStaionBG = "NeedResumeSpaceStaionBG";
        public const string TaskName = "WarReportManagerUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartWarReportManagerUITaskWithPrepare;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnPlayerIconClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnWarSolarSystemNameButtonClick;
        private static DelegateBridge __Hotfix_OnBattleStatusEnd;
        private static DelegateBridge __Hotfix_OnWarOverviewButtonClick;
        private static DelegateBridge __Hotfix_OnLossRatioButtonClick;
        private static DelegateBridge __Hotfix_OnLossShipButtonClick;
        private static DelegateBridge __Hotfix_OnKillRankingListButtonClick;
        private static DelegateBridge __Hotfix_OnGuildIconClick;
        private static DelegateBridge __Hotfix_OnGuildNameClick;
        private static DelegateBridge __Hotfix_OnShipIconClick;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_IsSelfGuildInvalid;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_GetGuildBattleReportInfoData;
        private static DelegateBridge __Hotfix_OnWarOverviewPanelResLoadEnd;
        private static DelegateBridge __Hotfix_OnLossRatioPanelResLoadEnd;
        private static DelegateBridge __Hotfix_OnLossShipPanelResLoadEnd;
        private static DelegateBridge __Hotfix_OnKillRankingListResLoadEnd;
        private static DelegateBridge __Hotfix_GetWarOverviewPanelOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetLossRatioPanelOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetLossShipPanelOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetRankingListOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetAllUIPanelHideProcessImmediate;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public WarReportManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetAllUIPanelHideProcessImmediate()
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildBattleReportInfoData(ulong reportId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetLossRatioPanelOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetLossShipPanelOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetRankingListOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetWarOverviewPanelOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsSelfGuildInvalid()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleStatusEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildIconClick(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNameClick(uint guildId, bool isNpcGuild)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRankingListButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRankingListResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossRatioButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossRatioPanelResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossShipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossShipPanelResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerIconClick(GuildBattlePlayerKillLostStatInfo playerInfo, UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipIconClick(ILBStoreItemClient item, Vector3 pos, string mode, ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarOverviewButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarOverviewPanelResLoadEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarSolarSystemNameButtonClick(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartWarReportManagerUITaskWithPrepare(ulong reportId, UIIntent returnToIntent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildBattleReportInfoData>c__AnonStorey1
        {
            internal GuildBattleReportInfoReqNetTask netTask;
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (this.netTask.IsNetworkError)
                {
                    this.onEnd(false);
                }
                else
                {
                    GuildBattleReportInfoReqNetTask task2 = (GuildBattleReportInfoReqNetTask) task;
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetLossRatioPanelOpenOrHideUIProcess>c__AnonStorey3
        {
            internal bool isImmediately;
            internal WarReportManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_guildKillLossListUITask.ShowWarGuildKillLostPanel(this.$this.m_reportInfo, this.isImmediately, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_guildKillLossListUITask.HideGuildKillLostPanel(this.isImmediately, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetLossShipPanelOpenOrHideUIProcess>c__AnonStorey4
        {
            internal bool isImmediately;
            internal WarReportManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_shipLossUITask.ShowWarShipLossPanel(this.$this.m_reportInfo, this.isImmediately, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_shipLossUITask.HideShipLossPanel(this.isImmediately, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetRankingListOpenOrHideUIProcess>c__AnonStorey5
        {
            internal bool isImmediately;
            internal WarReportManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                WarPersonalKillRankingListUITask.ShowRankingListPanel(this.$this.m_reportInfo.m_playerKillLostStatInfoList, this.isImmediately, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                WarPersonalKillRankingListUITask.HideRankingListPanel(this.isImmediately, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetWarOverviewPanelOpenOrHideUIProcess>c__AnonStorey2
        {
            internal bool isImmediately;
            internal WarReportManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                WarOverviewUITask.ShowWarOverviewPanel(this.$this.m_reportInfo, this.isImmediately, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                WarOverviewUITask.HideWarOverviewPanel(this.isImmediately, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal WarReportManagerUITask $this;

            internal void <>m__0(bool result)
            {
                this.$this.m_reportInfo = ((GameManager.Instance.PlayerContext as ProjectXPlayerContext).GetLBGuild() as LogicBlockGuildClient).GetGuildBattleReportInfo(this.$this.m_reportId);
                this.$this.m_currTab = WarReportManagerUITask.TabType.WarOverview;
                this.onPrepareEnd(result);
            }
        }

        protected enum PipeLineStateMaskType
        {
            ClosePanel
        }

        public enum TabType
        {
            WarOverview,
            LossRatio,
            LossShip,
            RankingList
        }
    }
}

