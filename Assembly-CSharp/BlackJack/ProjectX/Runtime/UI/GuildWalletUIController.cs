﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildWalletUIController : UIControllerBase
    {
        private Vector3 m_detailOffset;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnSimpleLogItemFill;
        private const string DonateSimpleLogItemAssetName = "DonateSimpleLog";
        private const string RankingListItemAssetName = "RankingItemUIPrefab";
        private const string StateNormal = "Normal";
        private const string StateGray = "Gray";
        private const string StateShow = "Show";
        private const string StateClose = "Close";
        private GameObject m_rankingTemplete;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private const int MaxRankingCount = 8;
        private List<GuildCurrencyLogInfo> m_LogItemList;
        private List<DonateTradeMoneyRankingListItemUIController> m_donateRankingItemUICtrlList;
        [AutoBind("./BalanceGroup/TacticalPoint/TacticalPointDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TacticalPointDetailButton;
        [AutoBind("./BalanceGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuyButtonCommonUIStateController;
        [AutoBind("./BalanceGroup/LogButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LogButtonCommonUIStateController;
        [AutoBind("./BalanceGroup/TradeMoney", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradeMoneyCommonUIStateController;
        [AutoBind("./AnnouncementGroup/ChangeImage", AutoBindAttribute.InitState.NotInit, false)]
        public InputField ChangeImageInputField;
        [AutoBind("./TempleteItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TempleteItemRoot;
        [AutoBind("./DonateGroup/DonateSimpleLog", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool DonateSimpleLogEasyObjectPool;
        [AutoBind("./DonateGroup/DonateSimpleLog", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect DonateSimpleLogLoopVerticalScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WalletGroupCommonUIStateController;
        [AutoBind("./7DayGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DonateRankingItemRoot;
        [AutoBind("./BalanceGroup/DonateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DonateButton;
        [AutoBind("./BalanceGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./BalanceGroup/LogButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LogButton;
        [AutoBind("./BalanceGroup/InformationPoint/InformationPointDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx InformationPointDetailButton;
        [AutoBind("./BalanceGroup/InformationPoint/InformationPointValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text InformationPointValueText;
        [AutoBind("./BalanceGroup/InformationPoint", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController InformationPointCommonUIStateController;
        [AutoBind("./BalanceGroup/TacticalPoint/TacticalPointDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public Image TacticalPointDetailButtonImage;
        [AutoBind("./BalanceGroup/TacticalPoint/TacticalPointValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TacticalPointValueText;
        [AutoBind("./BalanceGroup/TacticalPoint", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TacticalPointCommonUIStateController;
        [AutoBind("./BalanceGroup/TradeMoney/TradeMoneyDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradeMoneyDetailButton;
        [AutoBind("./BalanceGroup/TradeMoney/TradeMoneyValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeMoneyValueText;
        [AutoBind("./DonateGroup/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DonateSimpleLogStateCtrl;
        [AutoBind("./7DayGroup/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DonateRankongListStateCtrl;
        private static DelegateBridge __Hotfix_UpdateWalletUIInfo;
        private static DelegateBridge __Hotfix_GetGuildWalletUIProcess;
        private static DelegateBridge __Hotfix_UpdateDonateAnnouncement;
        private static DelegateBridge __Hotfix_PostProcessAnnouncementInputText;
        private static DelegateBridge __Hotfix_GetDonateAnnouncement;
        private static DelegateBridge __Hotfix_GetTradeMoneyDetailPos;
        private static DelegateBridge __Hotfix_GetInformationPointDetailPos;
        private static DelegateBridge __Hotfix_GetTacticalPointDetailPos;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnLogUIItemFill;
        private static DelegateBridge __Hotfix_UpdateDonateRankingList;
        private static DelegateBridge __Hotfix_UpdateCurrencyInfo;
        private static DelegateBridge __Hotfix_CheckWalletNoticeInput;
        private static DelegateBridge __Hotfix_add_EventOnSimpleLogItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnSimpleLogItemFill;

        public event Action<UIControllerBase> EventOnSimpleLogItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CheckWalletNoticeInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public string GetDonateAnnouncement()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildWalletUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetInformationPointDetailPos()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetTacticalPointDetailPos()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetTradeMoneyDetailPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogUIItemFill(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void PostProcessAnnouncementInputText()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCurrencyInfo(bool hasPermission, int occupySolarSystemCount, ulong informationPointAdd)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDonateAnnouncement(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDonateRankingList(List<ProGuildDonateRankingItemInfo> guildDonateRankingList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWalletUIInfo(string announcement, List<ProGuildDonateRankingItemInfo> guildDonateRankingList, List<GuildCurrencyLogInfo> guildDonateLogList, bool hasPermission, int occupySolarSystemCount, ulong informationPointAdd, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

