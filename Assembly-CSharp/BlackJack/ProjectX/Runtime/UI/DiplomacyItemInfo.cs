﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class DiplomacyItemInfo
    {
        private bool m_showTitle;
        private BlackJack.ProjectX.Runtime.UI.DiplomacyItemType m_diplomacyItemType;
        private BlackJack.ProjectX.Common.DiplomacyState m_diplomacyState;
        private BlackJack.ProjectX.Common.PlayerSimplestInfo m_playerSimplestInfo;
        private BlackJack.ProjectX.Common.GuildSimplestInfo m_guildSimplestInfo;
        private BlackJack.ProjectX.Common.AllianceBasicInfo m_allianceBasicInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShowTitle;
        private static DelegateBridge __Hotfix_set_ShowTitle;
        private static DelegateBridge __Hotfix_get_DiplomacyItemType;
        private static DelegateBridge __Hotfix_get_DiplomacyState;
        private static DelegateBridge __Hotfix_set_DiplomacyState;
        private static DelegateBridge __Hotfix_get_PlayerSimplestInfo;
        private static DelegateBridge __Hotfix_get_GuildSimplestInfo;
        private static DelegateBridge __Hotfix_get_AllianceBasicInfo;

        [MethodImpl(0x8000)]
        public DiplomacyItemInfo(bool showTitle, BlackJack.ProjectX.Runtime.UI.DiplomacyItemType itemType, BlackJack.ProjectX.Common.DiplomacyState diplomacyState, object obj)
        {
        }

        public bool ShowTitle
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public BlackJack.ProjectX.Runtime.UI.DiplomacyItemType DiplomacyItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public BlackJack.ProjectX.Common.DiplomacyState DiplomacyState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public BlackJack.ProjectX.Common.PlayerSimplestInfo PlayerSimplestInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public BlackJack.ProjectX.Common.GuildSimplestInfo GuildSimplestInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public BlackJack.ProjectX.Common.AllianceBasicInfo AllianceBasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

