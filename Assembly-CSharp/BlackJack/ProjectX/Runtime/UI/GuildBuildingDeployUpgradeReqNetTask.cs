﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildBuildingDeployUpgradeReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_solarSystemId;
        private readonly int m_buildingType;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildBuildingDeployUpgradeAck;

        [MethodImpl(0x8000)]
        public GuildBuildingDeployUpgradeReqNetTask(int solarSytemId, int buildingType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBuildingDeployUpgradeAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

