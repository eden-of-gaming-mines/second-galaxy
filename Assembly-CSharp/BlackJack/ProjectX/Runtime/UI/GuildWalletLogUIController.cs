﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildWalletLogUIController : UIControllerBase
    {
        [AutoBind("./STitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text STitleMoneyNumberText;
        [AutoBind("./STitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject STitleMoneyGroupGameObject;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildWalletLogUIStateController;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/InformationPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController InformationPointButtonCommonUIStateController;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/TacticalPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TacticalPointButtonCommonUIStateController;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/TradeMoneyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradeMoneyButtonCommonUIStateController;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildWalletLogUIPrefabGameObject;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/TacticalPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TacticalPointButton;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/InformationPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx InformationPointButton;
        [AutoBind("./GuildWarehouseWalletLogItemCategoryToggleGroup/GuildWarehouseWalletLogItemCategory/Viewport/Content/NpcShopItemCategoryToggleUIPrefab/TradeMoneyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradeMoneyButton;
        [AutoBind("./CTitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CTitleMoneyNumberText;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_SetLogTypeActive;
        private static DelegateBridge __Hotfix_UpdateMoney;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogTypeActive(GuildWalletLogUITask.LogTabType logTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMoney(ulong bindMoney, ulong tradeMoney)
        {
        }
    }
}

