﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActivityToggleUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActivityInfo> EventOnToggleSelected;
        protected bool m_isDailyActivity = true;
        protected ActivityInfo m_currActivityInfo;
        protected const string ActivityMode_DailyNormal = "DailyNormal";
        protected const string ActivityMode_DailyNotStart = "DailyNotStart";
        protected const string ActivityMode_TimeLimitOpen = "TimeLimitOpen";
        protected const string ActivityMode_TimeLimitNotOpen = "TimeLimitNotOpen";
        protected const string ActivityMode_TimeLimitNotStart = "TimeLimitNotStart";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActivityItemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ActivityToggle;
        [AutoBind("./Icon/ActivitiesIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ActivitiesIconImage;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./OpenConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OpenConditionText;
        [AutoBind("./Frequency", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActivityFrequencyColorCtrl;
        [AutoBind("./OpenTime/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActivityOpenTimeText;
        [AutoBind("./Frequency/NumberText1", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActivityPlayFrequencyText;
        [AutoBind("./Frequency/NumberText3", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActivityFullFrequencyText;
        [AutoBind("./IconGruop/MineralImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MineralRewardObject;
        [AutoBind("./IconGruop/ExImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExpRewardObject;
        [AutoBind("./IconGruop/CImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CurrencyRewardObject;
        [AutoBind("./IconGruop/CImage/CIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CurrencyRewardIconImage;
        [AutoBind("./IconGruop/BoxImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BoxRewardObject;
        [AutoBind("./IconGruop/InformationPointImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InformationPointObject;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitToggleEvent;
        private static DelegateBridge __Hotfix_UpdateActivityItemInfo;
        private static DelegateBridge __Hotfix_UpdateDailyActivitiyItemInfo;
        private static DelegateBridge __Hotfix_UpdateTimeLimitActivityItemInfo;
        private static DelegateBridge __Hotfix_OnToggleSelected;
        private static DelegateBridge __Hotfix_SetActivityName;
        private static DelegateBridge __Hotfix_SetActivityIcon;
        private static DelegateBridge __Hotfix_SetActivityFrequency;
        private static DelegateBridge __Hotfix_SetActivityOpenTime;
        private static DelegateBridge __Hotfix_SetActivityReward;
        private static DelegateBridge __Hotfix_GetActivityConfigInfo;
        private static DelegateBridge __Hotfix_add_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_get_IsDailyActivity;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<ActivityInfo> EventOnToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected ConfigDataActivityInfo GetActivityConfigInfo(ActivityType type)
        {
        }

        [MethodImpl(0x8000)]
        public void InitToggleEvent(Action<ActivityInfo> action)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnToggleSelected(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetActivityFrequency(PlayerActivityInfo playerInfo, ConfigDataActivityInfo activityInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetActivityIcon(string subPath, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetActivityName(string strKey)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetActivityOpenTime(DateTime startTime, DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetActivityReward(List<ActivityRewardInfo> rewardList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActivityItemInfo(ActivityInfo activityInfo, PlayerActivityInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDailyActivitiyItemInfo(ActivityInfo activityInfo, ConfigDataActivityInfo confInfo, PlayerActivityInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTimeLimitActivityItemInfo(ActivityInfo activityInfo, ConfigDataActivityInfo confInfo, PlayerActivityInfo playerInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public bool IsDailyActivity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

