﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemUIManipulationGuideUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_UIManipulationCtrl;
        [AutoBind("./CircleEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_circleEffectUIObj;
        [AutoBind("./RectEffectUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rectEffectUIObj;
        private Camera m_canvasCamera;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_ShowOrHideTargetListUI;
        private static DelegateBridge __Hotfix_SetUIManipulationGuideUIEffect;
        private static DelegateBridge __Hotfix_UnsetUIManipulationGuideUIEffect;
        private static DelegateBridge __Hotfix_GetSourceEffectUIObjByType;
        private static DelegateBridge __Hotfix_SetUIManipulationEffectTransform;
        private static DelegateBridge __Hotfix_get_CanvasCamera;

        [MethodImpl(0x8000)]
        private GameObject GetSourceEffectUIObjByType(SolarSystemUIManipulationSharp effectType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUIManipulationEffectTransform(GameObject effectObj, RectTransform sourceRectTrans)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIManipulationGuideUIEffect(RectTransform targetTrans, SolarSystemUIManipulationSharp guideType)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideTargetListUI(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void UnsetUIManipulationGuideUIEffect()
        {
        }

        private Camera CanvasCamera
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

