﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemSelfShipUIController : UIControllerBase
    {
        public SolarSystemShipProgressUIController ShieldProgressCtrl;
        public SolarSystemShipProgressUIController ExtraShieldProgressCtrl;
        public SolarSystemShipProgressUIController ArmorProgressCtrl;
        public SolarSystemShipProgressUIController EnergyProgressCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfShipInfoPanelStateCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PlayerSelfInfoVolatileObj;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/ShieldProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shieldProgressCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/ExtraShieldBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraShieldProgressCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/ArmorProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_armorProgressCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/EnergyProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_nergyProgressCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/SpeedProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image SpeedProgressBar;
        [AutoBind("./BuffAndDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemSelfShipBuffInfoUIController BuffInfoCtrl;
        [AutoBind("./TacticalEquipEffect", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemTacticalEquipEffectUIController TacticalEffectCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/ArmorProgressBarGroup/TopProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LowArmorWarningCtrl;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/SpeedButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DecelerateButton;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/SpeedButton/SpeedText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText SpeedText;
        [AutoBindWithParent("../CanvasVolatile/PlayerSelfInfoPanel/SpeedButton/JumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpText;
        [AutoBind("./SolarSystemSelfShipBuffDetailInfoUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_shipBuffDetailInfoTrans;
        [AutoBind("./SelfPVPFlag", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selfPVPFlagsStateCtrl;
        [AutoBind("./SelfPVPFlag", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_selfPVPFlagButton;
        [AutoBind("./SelfPVPFlag/PVP", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selfCrimeStateCtrl;
        [AutoBind("./SelfPVPFlag/Limitation", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selfLimitationStateCtrl;
        [AutoBind("./SelfPVPFlag/Smuggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selfSmuggleStateCtrl;
        [AutoBind("./SelfPVPFlagInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SelfPVPFlagInfoPanelDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_EnableSelfShipInfoUIPanel;
        private static DelegateBridge __Hotfix_ShowSelfShipInfoPanelUnlockEffect;
        private static DelegateBridge __Hotfix_CreateSelfShipInfoPanelUnlockEffectProcess;
        private static DelegateBridge __Hotfix_SetShieldValue;
        private static DelegateBridge __Hotfix_SetExtraShieldValue;
        private static DelegateBridge __Hotfix_SetArmorValue;
        private static DelegateBridge __Hotfix_SetEngryValue;
        private static DelegateBridge __Hotfix_SetFuelValue;
        private static DelegateBridge __Hotfix_SetSpeedValue;
        private static DelegateBridge __Hotfix_SetCrimeState;
        private static DelegateBridge __Hotfix_SetPVPFlag;
        private static DelegateBridge __Hotfix_SetSmuggleState;
        private static DelegateBridge __Hotfix_SetPVPFlagsState;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedShipBuffUIInfo;
        private static DelegateBridge __Hotfix_UpdateTacticalEffectState;
        private static DelegateBridge __Hotfix_UpdateTacticalEffectNumber_1;
        private static DelegateBridge __Hotfix_UpdateTacticalEffectNumber_0;
        private static DelegateBridge __Hotfix_UpdateTacticalEffectProgress;
        private static DelegateBridge __Hotfix_UpdateTacticalEquipIcon;
        private static DelegateBridge __Hotfix_InitTacticalEquipUIInfo;
        private static DelegateBridge __Hotfix_SetTacticalEquipButtonClickEvent;
        private static DelegateBridge __Hotfix_SetTacticalEquipState;

        [MethodImpl(0x8000)]
        public UIProcess CreateSelfShipInfoPanelUnlockEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableSelfShipInfoUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void InitTacticalEquipUIInfo(LBTacticalEquipGroupBase tacticalEquip, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorValue(float currentValue, float maxValue, float lowArmorWarningRatio)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCrimeState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEngryValue(float currentValue, float maxValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraShieldValue(float currentValue, float maxValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFuelValue(float currentValue, float maxValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPVPFlag(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPVPFlagsState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldValue(float currentValue, float maxValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSmuggleState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpeedValue(float currentValue, float maxValue, bool isJumping)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTacticalEquipButtonClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTacticalEquipState(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSelfShipInfoPanelUnlockEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrentSelectedShipBuffUIInfo(ClientSpaceShip selfShip, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEffectNumber(float showValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEffectNumber(float cur, float total)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEffectProgress(float percent)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEffectState(byte[] states, bool resetState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalEquipIcon(string assetName, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

