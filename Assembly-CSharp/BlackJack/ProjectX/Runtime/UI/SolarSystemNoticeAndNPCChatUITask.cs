﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SolarSystemNoticeAndNPCChatUITask : UITaskBase
    {
        private bool m_isInAnnouncement;
        private SystemAnnouncementUITask m_announcementUITask;
        private CanvasGroup m_noticeCanvasGroup;
        private CanvasGroup m_npcChatCanvasGroup;
        private bool m_isPanelShow;
        private TopNoticeType m_currTopNoticeType;
        private SolarSystemNoticeUIController m_mainCtrl;
        private NPCChatUIController m_npcChatUICtrl;
        private ClientSpaceObject m_currentTargetObj;
        private SimSpaceShip.MoveCmd m_currentMoveCmd;
        private float NPCTalkerChatDurationTime;
        private float SceneNoticeDurationTime;
        private bool m_isLastQuestCompleteAnimationFinished;
        private LBProcessingQuestBase m_nextScienceExploreQuest;
        private bool m_isDisableReturnNotice;
        private InSpaceSceneProcessBarNtf m_cachedSceneProcessBarNtf;
        private int m_lastNpcChatDialogId;
        private int m_currTextNoticeDialogId;
        private List<KeyValuePair<NpcDNId, List<int>>> m_cacheNPCChatInfoList;
        private LinkedList<Action> m_waitingHandlerQueue;
        private bool m_isWatingHandlerExecuting;
        private Action m_actionAfterQuestAnimation;
        private bool m_isFollowingGuildFleetFormation;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string SolarSystemPlayerNoticUIPrefabAssetPath;
        protected static string NPCChatPanelUIPrefabAssetPath;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToSpaceStationButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProcessingQuestBase> EventOnGoToNextQeustButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAnnouncementButtonClick;
        public const string TaskName = "SolarSystemNoticeAndNPCChatUITask";
        private bool m_isNavigationStartNoticeWaitingForDisplay;
        private bool m_isNavigationStopNoticeWaitingForDisplay;
        private bool m_isNavigationDestChangedNoticeWaitingForDisplay;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnClickNavigationUIStarMapButton;
        private string UIActionNameForReturnStationTipWindowUI;
        private Action m_showReturnStationTipAction;
        private Action m_showGoToNextScienceExploreQuestTipAction;
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Action <>f__am$cache2;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache6;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_EnableQuestNotice;
        private static DelegateBridge __Hotfix_ShowQuestNoticeUnlockEffect;
        private static DelegateBridge __Hotfix_ShowRelativeQuestMsg;
        private static DelegateBridge __Hotfix_HideQuestFinishMsgBox;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_CreatePipeLineCtx;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_OnLoadDynamicResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_SceneNotice;
        private static DelegateBridge __Hotfix_ShowNormalNoticeText;
        private static DelegateBridge __Hotfix_RegisterNoticeEvent;
        private static DelegateBridge __Hotfix_UnregisterNoticeEvent;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnInSpaceNpcTalkerChatNtf;
        private static DelegateBridge __Hotfix_StartInSpaceNpcTalkerChat;
        private static DelegateBridge __Hotfix_OnInSpaceSceneTextNoticeNtf;
        private static DelegateBridge __Hotfix_AddWaitingHandlerToQueue;
        private static DelegateBridge __Hotfix_TryToStartFirstWaitingHandler;
        private static DelegateBridge __Hotfix_OnSceneBindQuest;
        private static DelegateBridge __Hotfix_ClearNotice;
        private static DelegateBridge __Hotfix_OnEnterScene;
        private static DelegateBridge __Hotfix_OnLeaveScene;
        private static DelegateBridge __Hotfix_OnQuestCompCondParamUpdate;
        private static DelegateBridge __Hotfix_OnQuestCompleteWait;
        private static DelegateBridge __Hotfix_OnQuestCompletConfirmAck;
        private static DelegateBridge __Hotfix_OnQuestFailNtf;
        private static DelegateBridge __Hotfix_OnGuildActionCompletedOrFailedNtf;
        private static DelegateBridge __Hotfix_OnInSpaceSceneProcessBarNtf;
        private static DelegateBridge __Hotfix_OnInSpaceSceneProcessBarUpdateNtf;
        private static DelegateBridge __Hotfix_OnBackToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_OnNextButtonClick;
        private static DelegateBridge __Hotfix_IsReadyToShowNextQuestDialog;
        private static DelegateBridge __Hotfix_OnNPCChatEnd;
        private static DelegateBridge __Hotfix_PlayFirstChatInCache;
        private static DelegateBridge __Hotfix_ShowShipMoveState4MoveCmd;
        private static DelegateBridge __Hotfix_ShowShipMoveState4Target;
        private static DelegateBridge __Hotfix_ShowWingShipState;
        private static DelegateBridge __Hotfix_ShowShipMoveCmdState_0;
        private static DelegateBridge __Hotfix_ShowShipMoveCmdState_1;
        private static DelegateBridge __Hotfix_ClearShipMoveCmdState;
        private static DelegateBridge __Hotfix_ClearUI;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_ShowAttachedBuffInfo;
        private static DelegateBridge __Hotfix_ShowEquipEffectVirtualBufInfo;
        private static DelegateBridge __Hotfix_GetLastNpcChatDialogId;
        private static DelegateBridge __Hotfix_GetCurrTextNoticeDialogId;
        private static DelegateBridge __Hotfix_ShowNewArriveSolarSystemInfo;
        private static DelegateBridge __Hotfix_UpdateView_NPCChat;
        private static DelegateBridge __Hotfix_ClearDataCache;
        private static DelegateBridge __Hotfix_ShowNewArriveSolarSystemInfoImpl;
        private static DelegateBridge __Hotfix_CheckTopNoticeCanShow;
        private static DelegateBridge __Hotfix_ShowReturnStationBoxForLeaveWornhole;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SetAnnouncementTextInfo;
        private static DelegateBridge __Hotfix_StopAnnouncementTextInfo;
        private static DelegateBridge __Hotfix_OnAnnouncementEnd;
        private static DelegateBridge __Hotfix_OnAnnouncementAdd;
        private static DelegateBridge __Hotfix_OnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_SelfShip;
        private static DelegateBridge __Hotfix_get_CurrentPipeLineCtx;
        private static DelegateBridge __Hotfix_get_CurrentTargetObj;
        private static DelegateBridge __Hotfix_get_CurrentMoveCmd;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_EventOnBackToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToSpaceStationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGoToNextQeustButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoToNextQeustButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAnnouncementButtonClick;
        private static DelegateBridge __Hotfix_InitNavigationUIEvent;
        private static DelegateBridge __Hotfix_OnNavigationUIStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnNavigationUIStopNavigationButtonClick;
        private static DelegateBridge __Hotfix_RegisterNavigationEvent;
        private static DelegateBridge __Hotfix_UnRegisterNavigationEvent;
        private static DelegateBridge __Hotfix_OnNavigationStart;
        private static DelegateBridge __Hotfix_OnNavigationStop;
        private static DelegateBridge __Hotfix_OnNavigationDestChanged;
        private static DelegateBridge __Hotfix_OnNavigationFinished;
        private static DelegateBridge __Hotfix_InitNavigationUIState;
        private static DelegateBridge __Hotfix_TickForNavigationEvent;
        private static DelegateBridge __Hotfix_add_EventOnClickNavigationUIStarMapButton;
        private static DelegateBridge __Hotfix_remove_EventOnClickNavigationUIStarMapButton;
        private static DelegateBridge __Hotfix_IsAllowToTriggerQuestAcceptDialog;
        private static DelegateBridge __Hotfix_ShowQuestCompleteEffectTipWindow;
        private static DelegateBridge __Hotfix_ShowGuildActionCompleteEffectTipWindow;
        private static DelegateBridge __Hotfix_ShowReturnStationTipWindow;
        private static DelegateBridge __Hotfix_ShowReturnStationTipWindowImp;
        private static DelegateBridge __Hotfix_CreateShowReturnStationTipWindowUIQuene;
        private static DelegateBridge __Hotfix_ShowGoToNextScieneExploreQuest;
        private static DelegateBridge __Hotfix_ShowGoToNextScienceExploreQuestTipWindowImp;
        private static DelegateBridge __Hotfix_CreateShowGoToNextScienceExploreQuestTipWindowUIQuene;
        private static DelegateBridge __Hotfix_CheckAcceptDialogCharacterState;
        private static DelegateBridge __Hotfix_ShowFullScreenEffectForWormholeDebuffWarning;
        private static DelegateBridge __Hotfix_StopFullScreenEffectForWormholeDebuffWarning;

        public event Action EventOnAnnouncementButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackToSpaceStationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnClickNavigationUIStarMapButton
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase> EventOnGoToNextQeustButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SolarSystemNoticeAndNPCChatUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddWaitingHandlerToQueue(Action action)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckAcceptDialogCharacterState()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckTopNoticeCanShow(TopNoticeType type)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearNotice(SceneTextNoticeType noticeType)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearShipMoveCmdState()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override UITaskPipeLineCtx CreatePipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateShowGoToNextScienceExploreQuestTipWindowUIQuene()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateShowReturnStationTipWindowUIQuene()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableQuestNotice(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrTextNoticeDialogId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLastNpcChatDialogId()
        {
        }

        [MethodImpl(0x8000)]
        public void HideQuestFinishMsgBox(bool immediateHide = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitNavigationUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void InitNavigationUIState()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAllowToTriggerQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsReadyToShowNextQuestDialog()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementAdd(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAnnouncementEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToSpaceStationButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterScene(int sceneConfId, uint sceneInstanceId, int signalId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildActionCompletedOrFailedNtf(GuildActionCompletedOrFailedNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInSpaceNpcTalkerChatNtf(InSpaceNpcTalkerChatNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInSpaceSceneProcessBarNtf(InSpaceSceneProcessBarNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInSpaceSceneProcessBarUpdateNtf(InSpaceSceneProcessBarUpdateNtf updateNtf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInSpaceSceneTextNoticeNtf(bool isClear, int dialogID, int noticTypeInt, string[] paramList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveScene(int sceneConfId, uint sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationDestChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationFinished()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationStart()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationUIStarMapButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNavigationUIStopNavigationButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNextButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNPCChatEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCompCondParamUpdate(LBProcessingQuestBase quest, int condIndex, int param)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompletConfirmAck(int result, int questConfigId, List<QuestRewardInfo> rewardList, bool disableReturnNotice)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCompleteWait(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestFailNtf(int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSceneBindQuest()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void PlayFirstChatInCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNavigationEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNoticeEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementTextInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isShow, bool isShowNPCChat = true)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAttachedBuffInfo(LBShipFightBuf lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEquipEffectVirtualBufInfo(EquipEffectVirtualBufInfo bufInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowFullScreenEffectForWormholeDebuffWarning(Action onBackButtonClick)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowGoToNextScienceExploreQuestTipWindowImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowGoToNextScieneExploreQuest()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowGuildActionCompleteEffectTipWindow()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewArriveSolarSystemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNewArriveSolarSystemInfoImpl()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNormalNoticeText(string msg, float durationTime = -1f)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestCompleteEffectTipWindow(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestNoticeUnlockEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRelativeQuestMsg()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowReturnStationBoxForLeaveWornhole()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowReturnStationTipWindow()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowReturnStationTipWindowImp()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipMoveCmdState(string str)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipMoveCmdState(string str, string targetName, bool hideTween)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipMoveState4MoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipMoveState4Target(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWingShipState(string wingShipState, bool needResetShipIcon, Sprite shipIcon)
        {
        }

        [MethodImpl(0x8000)]
        private void StartInSpaceNpcTalkerChat(NpcDNId replaceNpc, int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAnnouncementTextInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void StopFullScreenEffectForWormholeDebuffWarning()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForNavigationEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void TryToStartFirstWaitingHandler()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNavigationEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterNoticeEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_NPCChat()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_SceneNotice()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ClientSelfPlayerSpaceShip SelfShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected SolarSystemNoticePipeLineCtx CurrentPipeLineCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientSpaceObject CurrentTargetObj
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimSpaceShip.MoveCmd CurrentMoveCmd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateShowGoToNextScienceExploreQuestTipWindowUIQuene>c__AnonStorey8
        {
            internal UIManager.UIActionQueueItem item;

            internal void <>m__0()
            {
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <CreateShowReturnStationTipWindowUIQuene>c__AnonStorey7
        {
            internal UIManager.UIActionQueueItem item;

            internal void <>m__0()
            {
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildActionCompletedOrFailedNtf>c__AnonStorey4
        {
            internal UIManager.UIActionQueueItem item;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                SolarSystemUITask.ResetSolarSystemUITaskToNormalState(delegate (bool res) {
                    this.item.OnEnd();
                    this.$this.ShowGuildActionCompleteEffectTipWindow();
                }, true, true, false);
            }

            internal void <>m__1(bool res)
            {
                this.item.OnEnd();
                this.$this.ShowGuildActionCompleteEffectTipWindow();
            }
        }

        [CompilerGenerated]
        private sealed class <OnInSpaceNpcTalkerChatNtf>c__AnonStorey1
        {
            internal InSpaceNpcTalkerChatNtf ntf;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                NpcDNId key = CommonLogicUtil.Convert2NpcDNId(this.ntf.ReplaceNpc);
                if ((this.$this.State == Task.TaskState.Paused) || (this.$this.m_npcChatUICtrl.IsBusy && !this.$this.m_npcChatUICtrl.CanInterrupt))
                {
                    this.$this.m_cacheNPCChatInfoList.Add(new KeyValuePair<NpcDNId, List<int>>(key, this.ntf.DailogIdList));
                }
                else
                {
                    this.$this.StartInSpaceNpcTalkerChat(key, this.ntf.DailogIdList[0]);
                    if (this.ntf.DailogIdList.Count > 1)
                    {
                        this.ntf.DailogIdList.RemoveAt(0);
                        this.$this.m_cacheNPCChatInfoList.Add(new KeyValuePair<NpcDNId, List<int>>(key, this.ntf.DailogIdList));
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnInSpaceSceneTextNoticeNtf>c__AnonStorey2
        {
            internal int dialogID;
            internal bool isClear;
            internal int noticTypeInt;
            internal string[] paramList;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                this.$this.m_currTextNoticeDialogId = this.dialogID;
                object[] param = new object[] { this.dialogID };
                UserGuideUITask.ExecuteUserGuideTriggerPoint(UIManager.Instance.FindUITaskWithName("SolarSystemUITask", true), UserGuideTriggerPoint.UserGuideTriggerPoint_TextNotice, param);
                this.$this.EnablePipelineStateMask(SolarSystemNoticeAndNPCChatUITask.PipeLineStateMaskType.ShowSceneNotice);
                this.$this.CurrentPipeLineCtx.m_noticeIsClear = this.isClear;
                this.$this.CurrentPipeLineCtx.m_noticeType = (SceneTextNoticeType) this.noticTypeInt;
                if (!this.isClear)
                {
                    bool flag;
                    ConfigDataNpcTalkerInfo talkerInfo = NPCChatHelper.GetNpcTalkerInfoForDialog(this.dialogID, out flag, null, null, 0);
                    if (this.noticTypeInt != 4)
                    {
                        this.$this.CurrentPipeLineCtx.m_textNoticeStr = NPCChatHelper.GetDialogContent(this.dialogID, talkerInfo, this.$this.PlayerCtx.GetLBSpaceScene().GetRelativeQuestId(), null);
                    }
                    else
                    {
                        ConfigDataDialogInfo configDataDialogInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataDialogInfo(this.dialogID);
                        string stringInStoryStringTable = this.$this.PlayerCtx.GetStringInStoryStringTable(configDataDialogInfo.ContentStrKey);
                        this.$this.CurrentPipeLineCtx.m_textNoticeStr = stringInStoryStringTable;
                    }
                    this.$this.CurrentPipeLineCtx.m_textNoticeDurationTime = this.$this.SceneNoticeDurationTime;
                    this.$this.CurrentPipeLineCtx.m_dialogStrParams = this.paramList;
                }
                this.$this.m_isWatingHandlerExecuting = true;
                this.$this.m_currPipeLineCtx.m_blockGlobalUIInput = false;
                this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestCompleteWait>c__AnonStorey3
        {
            internal bool isCanStart;
            internal UIManager.UIActionQueueItem item;
            internal ConfigDataQuestInfo confQuest;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal bool <>m__0() => 
                this.isCanStart;

            internal void <>m__1()
            {
                SolarSystemUITask.ResetSolarSystemUITaskToNormalState(delegate (bool res) {
                    this.item.OnEnd();
                    this.$this.ShowQuestCompleteEffectTipWindow(this.confQuest);
                }, true, true, false);
            }

            internal void <>m__2(bool res)
            {
                this.item.OnEnd();
                this.$this.ShowQuestCompleteEffectTipWindow(this.confQuest);
            }
        }

        [CompilerGenerated]
        private sealed class <PlayFirstChatInCache>c__AnonStorey5
        {
            internal NpcDNId replaceNpcId;
            internal int dlgId;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                this.$this.StartInSpaceNpcTalkerChat(this.replaceNpcId, this.dlgId);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNormalNoticeText>c__AnonStorey0
        {
            internal string msg;
            internal float durationTime;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                if (this.$this.CheckTopNoticeCanShow(SolarSystemNoticeAndNPCChatUITask.TopNoticeType.NormalNotice))
                {
                    this.$this.m_mainCtrl.ShowNormalNoticeText(this.msg, this.durationTime);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowQuestCompleteEffectTipWindow>c__AnonStorey6
        {
            internal ConfigDataQuestInfo confQuest;
            internal UIManager.UIActionQueueItem item;
            internal SolarSystemNoticeAndNPCChatUITask $this;

            internal void <>m__0()
            {
                if (LBProcessingQuestBase.CheckQuestFlag(this.confQuest, QuestFlag.QuestFlag_NotShowQuestCompleteEffect) || !this.$this.CheckTopNoticeCanShow(SolarSystemNoticeAndNPCChatUITask.TopNoticeType.QuestComplete))
                {
                    this.item.OnEnd();
                }
                else
                {
                    this.$this.m_isLastQuestCompleteAnimationFinished = false;
                    PlayLogicalSoundUtil.PlaySoundIndependently(LogicalSoundResTableID.LogicalSoundResTableID_QuestCompleteAudioResFullPath, null, 1f);
                    this.$this.EnablePipelineStateMask(SolarSystemNoticeAndNPCChatUITask.PipeLineStateMaskType.ShowQuestCompleteEffect);
                    this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                }
            }

            internal void <>m__1()
            {
                this.$this.m_isLastQuestCompleteAnimationFinished = true;
                this.$this.m_nextScienceExploreQuest = null;
                if (this.confQuest.QuestType == QuestType.QuestType_ScienceExplore)
                {
                    int num = 0x7fffffff;
                    foreach (LBProcessingQuestBase base2 in this.$this.PlayerCtx.GetLBQuest().GetProcessingQuestList())
                    {
                        if (!base2.IsScienceExploreQuest())
                        {
                            continue;
                        }
                        int num2 = this.$this.PlayerCtx.GDBHelper.CalcSolarSystemJumpDistanceWithCache(base2.GetSceneSolarSystemId(), this.$this.PlayerCtx.CurrSolarSystemId);
                        if (num2 == 0)
                        {
                            this.$this.m_nextScienceExploreQuest = base2;
                            break;
                        }
                        if ((num2 > 0) && (num2 < num))
                        {
                            num = num2;
                            this.$this.m_nextScienceExploreQuest = base2;
                        }
                    }
                }
                if (this.$this.m_nextScienceExploreQuest == null)
                {
                    this.$this.ShowReturnStationTipWindow();
                }
                else
                {
                    this.$this.ShowGoToNextScieneExploreQuest();
                }
                UIManager.Instance.GlobalUIInputBlockForTicks(10);
            }
        }

        public class CacheNPChatInfo
        {
            public NpcDNId m_npcDNId;
            public int m_dialogId;
            public int m_emotionType;
            public int m_durationTime;
            public bool m_canInterrupt;
            private static DelegateBridge _c__Hotfix_ctor;

            public CacheNPChatInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            ShowNPCChat,
            ShowSceneNotice,
            ShowQuestCompleteEffect,
            ShowGuildActionCompleteEffect,
            ShowNewArriveSolarSystemInfo
        }

        public class SolarSystemNoticePipeLineCtx : UITaskPipeLineCtx
        {
            public string m_npcIconPath = string.Empty;
            public string m_npcName = string.Empty;
            public bool m_isRemoteNpc;
            public bool m_noticeIsClear;
            public SceneTextNoticeType m_noticeType;
            public string m_dialogStr = string.Empty;
            public string m_textNoticeStr = string.Empty;
            public string m_dialogAudioPath = string.Empty;
            public string[] m_dialogStrParams;
            public bool m_canInterrupt = true;
            public float m_npcChatDurationTime = 3f;
            public float m_textNoticeDurationTime = 3f;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Clear;

            public SolarSystemNoticePipeLineCtx()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_npcIconPath = string.Empty;
                    this.m_isRemoteNpc = false;
                    this.m_dialogStr = string.Empty;
                    this.m_textNoticeStr = string.Empty;
                    this.m_dialogAudioPath = string.Empty;
                    this.m_dialogStrParams = null;
                    this.m_noticeIsClear = false;
                    base.Clear();
                }
            }
        }

        protected enum TopNoticeType
        {
            None,
            WormholeDebuffWarning,
            Navigation,
            QuestComplete,
            QuestFinishMsgBox,
            NormalNotice,
            QuestInfo
        }
    }
}

