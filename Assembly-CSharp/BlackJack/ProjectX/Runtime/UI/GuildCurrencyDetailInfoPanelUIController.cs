﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCurrencyDetailInfoPanelUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform PanelRectTransform;
        [AutoBind("./FrameImage/BGImage/DetailInfo/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText;
        [AutoBind("./FrameImage/BGImage/DetailInfo/TitleGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroundButton;
        private static DelegateBridge __Hotfix_GetGuildCurrencyDetailInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdateTrdeMoneyDetail;
        private static DelegateBridge __Hotfix_UpdateTacticalPointDetail;
        private static DelegateBridge __Hotfix_UpdateInformationPointDetail;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPos;

        [MethodImpl(0x8000)]
        public UIProcess GetGuildCurrencyDetailInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInformationPointDetail(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalPointDetail(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTrdeMoneyDetail(Vector3 pos)
        {
        }
    }
}

