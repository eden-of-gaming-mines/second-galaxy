﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class WarGuildKillLostUITask : UITaskBase
    {
        public const string ParamKeyGuildBattleReportInfo = "GuildBattleReportInfo";
        public const string ParamKeyGuildOpenImmediate = "OpenImmediate";
        private GuildBattleReportInfo m_battleReportInfo;
        private bool m_isShowOpenAnimImmediate;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<uint> EventOnGuildIconClick;
        private List<KeyValuePair<GuildBattleGuildKillLostStatInfo, GuildBattleGuildKillLostStatInfo>> m_guildKillLossAttackerDefenderList;
        private bool m_isTaskInit;
        private WarGuildKillLostUIController m_warGuildKillLostUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartWarGuildKillLostUITask;
        private static DelegateBridge __Hotfix_ShowWarGuildKillLostPanel;
        private static DelegateBridge __Hotfix_HideGuildKillLostPanel;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectLogoResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnGuildIconClick;
        private static DelegateBridge __Hotfix_OnItemFilled;
        private static DelegateBridge __Hotfix_OnGuildNameClick;
        private static DelegateBridge __Hotfix_GuildKillLossComparer;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_add_EventOnGuildIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildIconClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<uint> EventOnGuildIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public WarGuildKillLostUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectLogoResForLoad(GuildLogoInfo logoInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildKillLossComparer(GuildBattleGuildKillLostStatInfo lossA, GuildBattleGuildKillLostStatInfo lossB)
        {
        }

        [MethodImpl(0x8000)]
        public void HideGuildKillLostPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildIconClick(UIControllerBase ctrl, bool isLeft)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNameClick(bool isDefend)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFilled(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWarGuildKillLostPanel(GuildBattleReportInfo reportInfo, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static WarGuildKillLostUITask StartWarGuildKillLostUITask(Action onResLoadEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideGuildKillLostPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal WarGuildKillLostUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess process, bool res)
            {
            }
        }
    }
}

