﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildActionInfoUpdateReqNetTask : NetWorkTransactionTask
    {
        public GuildActionInfoUpdateAck Ack;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ulong <InstanceId>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnGuildActionInfoUpdateAck;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_InstanceId;

        [MethodImpl(0x8000)]
        public GuildActionInfoUpdateReqNetTask(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildActionInfoUpdateAck(GuildActionInfoUpdateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public ulong InstanceId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

