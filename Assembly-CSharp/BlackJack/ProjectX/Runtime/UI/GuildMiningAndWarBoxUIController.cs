﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildMiningAndWarBoxUIController : UIControllerBase
    {
        private const string CommonSystemTimePrefabName = "CommonSystemTimePrefab";
        private CommonSystemTimeUIController m_timeUICtrl;
        private int m_longPressingEventInvokeCount;
        private int m_eventInvokeCount;
        public int m_hour;
        public int m_minute;
        [AutoBind("./MinuteGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MinuteNumberText;
        [AutoBind("./HourGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HourNumberText;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoPanelCommonUIStateController;
        [AutoBind("./DetailInfoPanel/DetailInfoPanelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseDetailInfoPanelButton;
        [AutoBind("./MinuteGroup/DownButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MinuteDownButton;
        [AutoBind("./MinuteGroup/UpButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MinuteUpButton;
        [AutoBind("./HourGroup/DownButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx HourDownButton;
        [AutoBind("./HourGroup/UpButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx HourUpButton;
        [AutoBind("./ExplainButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OpenDetailInfoPanelButton;
        [AutoBind("./ContentGroup/Buttons/YesButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx YesButton;
        [AutoBind("./ContentGroup/Buttons/NoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NoButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MiningAndWarBoxUIPrefabCommonUIStateController;
        [AutoBind("./TimeRootDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_timeRootTf;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_GetDetailInfoUIProcess;
        private static DelegateBridge __Hotfix_SetTime;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnHourAddButtonClick;
        private static DelegateBridge __Hotfix_OnHourAddButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnHourAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnHourAddButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnHourSubButtonClick;
        private static DelegateBridge __Hotfix_OnHourSubButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnHourSubButtonLongPressing;
        private static DelegateBridge __Hotfix_OnHourSubButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnMinuteAddButtonClick;
        private static DelegateBridge __Hotfix_OnMinuteAddButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnMinuteAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnMinuteAddButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnMinuteSubButtonClick;
        private static DelegateBridge __Hotfix_OnMinuteSubButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnMinuteSubButtonLongPressing;
        private static DelegateBridge __Hotfix_OnMinuteSubButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnValueChanged;
        private static DelegateBridge __Hotfix_GetChangeCountByLongPressingEventInvokeCount;

        [MethodImpl(0x8000)]
        private int GetChangeCountByLongPressingEventInvokeCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetDetailInfoUIProcess(bool isShow, bool mode, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow, bool mode, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourAddButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourAddButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourSubButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourSubButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourSubButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHourSubButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteAddButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteAddButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteSubButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteSubButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteSubButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMinuteSubButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnValueChanged(int hour, int minute)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTime(int hour, int minute)
        {
        }
    }
}

