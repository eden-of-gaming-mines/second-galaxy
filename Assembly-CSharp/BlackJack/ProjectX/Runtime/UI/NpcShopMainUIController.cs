﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopMainUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBuyItemToggleSelected;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnShipStoreSellToggleSelected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnItemStoreSellToggleSelected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSizeSortButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSubRankSortButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPriceSortButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSortListButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, bool> EventOnItemFilterToggleValueChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSortListBgButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAddMoneyClick;
        private bool m_isT1ButtonSelect;
        private bool m_isT2ButtonSelect;
        private bool m_isT3ButtonSelect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./TitleMoneyGroup/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./TitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumberText;
        [AutoBind("./TitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_moneyAddButton;
        [AutoBind("./TitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_addButtonStateUICtrl;
        [AutoBind("./BuyAndSellToggleGroup/BuyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_buyItemToggle;
        [AutoBind("./BuyAndSellToggleGroup/ShipStoreSellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_shipStoreSellToggle;
        [AutoBind("./BuyAndSellToggleGroup/ShipStoreSellToggle/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipStoreSellToggleRedpoint;
        [AutoBind("./BuyAndSellToggleGroup/ItemStoreSellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_itemStoreSellToggle;
        [AutoBind("./BuyAndSellToggleGroup/ItemStoreSellToggle/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemStoreSellToggleRedpoint;
        [AutoBind("./BuyAndSellToggleGroup/BuyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyItemToggleCtrl;
        [AutoBind("./BuyAndSellToggleGroup/ShipStoreSellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipStoreSellToggleCtrl;
        [AutoBind("./BuyAndSellToggleGroup/ItemStoreSellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemStoreSellToggleCtrl;
        [AutoBind("./NpcShopItemCategoryToggleGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_categoryToggleListTrans;
        [AutoBind("./NpcShopItemListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_npcShopItemListTrans;
        [AutoBind("./NpcShopPrepareForBuyOrSellDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_npcShopSelectedItemListTrans;
        [AutoBind("./NpcShopBuyItemConfirmWndDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_npcShopBuyItemConfirmWndTrans;
        [AutoBind("./ItemSimpleLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoLeftDummy;
        [AutoBind("./ItemSimpleRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoRightDummy;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/SizeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SizeSortButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/RareButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SubRankSortButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/PriceButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PriceSortButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/SizeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SizeSortButton;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/RareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SubRankSortButton;
        [AutoBind("./SortList/SortPanel/BGFrameImage/SortButtonPanel/PriceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PriceSortButton;
        [AutoBind("./SortList/SortListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortListButton;
        [AutoBind("./SortList/SortListButton/TextShowAll", AutoBindAttribute.InitState.NotInit, false)]
        public Text SortListButtonText;
        [AutoBind("./SortList/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortListPanelStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T1", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx T1Button;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T2", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx T2Button;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T3", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx T3Button;
        [AutoBind("./SortList/BgImages/BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortListBgButton;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T1", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController T1ButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T2", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController T2ButtonStateCtrl;
        [AutoBind("./SortList/SortPanel/BGFrameImage/T3", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController T3ButtonStateCtrl;
        [AutoBind("./NpcShopAutomaticSellBoxDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform NpcShopAutomaticSellBoxDummy;
        [AutoBind("./TitleText/CountryGroup/CountryNameText/CountryImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image GrandFactionIconImage;
        [AutoBind("./TitleText/CountryGroup/CountryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GrandFactionNameText;
        [AutoBind("./TitleText/CountryGroup/CountryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GrandFactionGroup;
        public NpcShopItemListUIController m_npcShopItemListCtrl;
        public NpcShopSelectedItemListUIController m_selectedItemListUICtrl;
        public NpcShopItemCategoryToggleListUIController m_categoryToggleListCtrl;
        public NpcShopBuyItemConfirmWndUIController m_npcShopBuyItemConfirmWndCtrl;
        public NpcShopAutomaticSellBoxUIController m_npcShopAutomaticSellBoxUIWndCtrl;
        private SystemDescriptionController m_descriptionCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GreatMainUIProcess;
        private static DelegateBridge __Hotfix_SetTitleText;
        private static DelegateBridge __Hotfix_SetGrandFactionInfo;
        private static DelegateBridge __Hotfix_SetCurrMoneyInfo;
        private static DelegateBridge __Hotfix_SetBuyItemToggleState;
        private static DelegateBridge __Hotfix_SetAddButtonState;
        private static DelegateBridge __Hotfix_SetShipStoreSellToggleState;
        private static DelegateBridge __Hotfix_SetShipStoreSellToggleRedPointState;
        private static DelegateBridge __Hotfix_SetItemStoreSellToggleState;
        private static DelegateBridge __Hotfix_SetItemStoreSellToggleRedPointState;
        private static DelegateBridge __Hotfix_GetNpcShopItemListPanelPosition;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoDummyPosition;
        private static DelegateBridge __Hotfix_SetFilterToggleListState;
        private static DelegateBridge __Hotfix_SetFilterSelectState;
        private static DelegateBridge __Hotfix_ResetFilterSelectState;
        private static DelegateBridge __Hotfix_SetSortButtonSelectState;
        private static DelegateBridge __Hotfix_SetSortStateText;
        private static DelegateBridge __Hotfix_ShowOrHideSortListPanel;
        private static DelegateBridge __Hotfix_SetAutomaticItemUIState;
        private static DelegateBridge __Hotfix_ShowOrHideSystemDescriptionButton;
        private static DelegateBridge __Hotfix_OnSortListBgButtonClick;
        private static DelegateBridge __Hotfix_OnPriceSortButtonClick;
        private static DelegateBridge __Hotfix_OnSizeSortButtonClick;
        private static DelegateBridge __Hotfix_OnSubRankSortButtonClick;
        private static DelegateBridge __Hotfix_OnSortListButtonClick;
        private static DelegateBridge __Hotfix_OnT1FilterToggleValueClick;
        private static DelegateBridge __Hotfix_OnT2FilterToggleValueClick;
        private static DelegateBridge __Hotfix_OnT3FilterToggleValueClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBuyItemToggleClick;
        private static DelegateBridge __Hotfix_OnShipStoreSellToggleClick;
        private static DelegateBridge __Hotfix_OnItemStoreSellToggleClick;
        private static DelegateBridge __Hotfix_OnMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBuyItemToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnBuyItemToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnShipStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnShipStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnItemStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnItemStoreSellToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnSizeSortButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSizeSortButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSubRankSortButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSubRankSortButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPriceSortButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPriceSortButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSortListButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSortListButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemFilterToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnItemFilterToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnSortListBgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSortListBgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddMoneyClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddMoneyClick;

        public event Action EventOnAddMoneyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBuyItemToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, bool> EventOnItemFilterToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemStoreSellToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPriceSortButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnShipStoreSellToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSizeSortButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSortListBgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSortListButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSubRankSortButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoDummyPosition(bool onLeft)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetNpcShopItemListPanelPosition()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GreatMainUIProcess(bool isShow, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyItemToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreSellToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPriceSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStoreSellToggleClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSizeSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortListBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortListButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubRankSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT1FilterToggleValueClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT2FilterToggleValueClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT3FilterToggleValueClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetFilterSelectState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAddButtonState(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutomaticItemUIState(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuyItemToggleState(bool isValid)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrMoneyInfo(CurrencyType moneyType, ulong moneyNum, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFilterSelectState(NpcShopMainUITask.NpcShopItemFilter filterType, bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFilterToggleListState(bool[] m_filterStateList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGrandFactionInfo(GrandFaction faction, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreSellToggleRedPointState(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreSellToggleState(bool isValid)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipStoreSellToggleRedPointState(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipStoreSellToggleState(bool isValid)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortButtonSelectState(NpcShopMainUITask.NpcShopSortType sortType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortStateText(NpcShopMainUITask.NpcShopSortType sortType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTitleText(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSortListPanel(bool isShow, NpcShopMainUITask.NpcShopSortType type = 0, bool[] filterStateList = null)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideSystemDescriptionButton(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

