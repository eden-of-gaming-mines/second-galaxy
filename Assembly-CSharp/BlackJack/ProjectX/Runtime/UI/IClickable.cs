﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public interface IClickable
    {
        void RegisterItemClickEvent(Action<UIControllerBase> action);
        void RegisterItemDoubleClickEvent(Action<UIControllerBase> action);
        void UnregisterItemClickEvent(Action<UIControllerBase> action);
    }
}

