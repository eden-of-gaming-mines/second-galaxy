﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipCustomTemplateUIController : CommonMenuItemUIController
    {
        private const string CloseState = "Close";
        private const string ShowState = "Show";
        protected CommonItemIconUIController m_shipIconCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnShipIconClick;
        [AutoBind("./Detail/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backButton;
        [AutoBind("./Detail/ShowAllTemplateGroupToggle/Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_showAllTemplateGroupToggle;
        [AutoBind("./Detail/ShipSlotGroupsInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipSlotGroupsInfoPanel;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup m_toggleGroup;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/TemporaryMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_temporaryMenuItemGo;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/TemporaryMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_temporaryMenuItemToggle;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/TemporaryMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_temporaryMenuItemCtrl;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/TemporaryMenuItem/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_temporaryMenuItemText;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/SelfMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_selfMenuItemBtn;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/SelfMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_selfMenuItemStat;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/SelfMenuItem/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_selfMenuItemText;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/GuildMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildMenuItemBtn;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/GuildMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildMenuItemStat;
        [AutoBind("./Detail/ShipTemplateTypePanel/ScrollView/Viewport/Content/GuildMenuItem/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildMenuItemText;
        [AutoBind("./Detail/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buttonGroupGo;
        [AutoBind("./Detail/ButtonGroup/FuncButtons/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_saveButton;
        [AutoBind("./Detail/ButtonGroup/FuncButtons/ApplyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_applyButton;
        [AutoBind("./Detail/ButtonGroup/FuncButtons/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shareButton;
        [AutoBind("./Detail/ButtonGroup/FuncButtons/PurchaseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_purchaseButton;
        [AutoBind("./Detail/ButtonGroup/FuncButtons/SaveGuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_saveGuildButton;
        [AutoBind("./Detail/ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_deleteButton;
        [AutoBind("./Detail/ButtonGroup/CurrSetting", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_currSettingGo;
        [AutoBind("./Detail/ShipTemplateNameInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipTemplateNamePanelGo;
        [AutoBind("./Detail/ShipTemplateNameInfoPanel/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipIconDummyGo;
        [AutoBind("./Detail/ShipTemplateNameInfoPanel/NameGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipTemplateInfoNameText;
        [AutoBind("./Detail/ShipTemplateNameInfoPanel/ShipLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipTemplateInfoLevelText;
        [AutoBind("./Detail/ShipTemplateNameInfoPanel/NameGroup/RenameButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_renameButton;
        [AutoBind("./RenamePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_renamePanelGo;
        [AutoBind("./RenamePanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_renameBackGroundButton;
        [AutoBind("./RenamePanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_renameCancelButton;
        [AutoBind("./RenamePanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_renameConfirmButton;
        [AutoBind("./RenamePanel/RenameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_renameInpuField;
        [AutoBind("./RenamePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_renameStateCtrl;
        [AutoBind("./UnloadItemWarningPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_unloadItemWarningPanelGo;
        [AutoBind("./UnloadItemWarningPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_unloadItemWarningBackGroundButton;
        [AutoBind("./UnloadItemWarningPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_unloadItemWarningCancelButton;
        [AutoBind("./UnloadItemWarningPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_unloadItemWarningConfirmButton;
        [AutoBind("./UnloadItemWarningPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_unloadItemWarningStateCtrl;
        [AutoBind("./ShipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_shipDummy;
        [AutoBind("./AmmoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_ammondummy;
        [AutoBind("./WeaponAndEquipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_weaponAndEquipDummy;
        private static DelegateBridge __Hotfix_SetShowAllTemplateGroupToggleState;
        private static DelegateBridge __Hotfix_UpdateShipCustomTemplate;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_CreateWarningWindowShowProcess;
        private static DelegateBridge __Hotfix_CreateRenameWindowShowProcess;
        private static DelegateBridge __Hotfix_CreateWarningWindowCloseProcess;
        private static DelegateBridge __Hotfix_CreateRenameWindowCloseProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipIconClick;

        public event Action<UIControllerBase> EventOnShipIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateRenameWindowCloseProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateRenameWindowShowProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateWarningWindowCloseProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateWarningWindowShowProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos(StoreItemType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShowAllTemplateGroupToggleState(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipCustomTemplate(ShipCustomTemplateInfo templateInfo, bool isTemporary, bool isShowTemporary, bool isCurrSetting, bool isShowApply, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

