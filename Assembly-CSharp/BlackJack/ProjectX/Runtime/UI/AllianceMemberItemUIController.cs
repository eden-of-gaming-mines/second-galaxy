﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceMemberItemUIController : UIControllerBase, IScrollItem
    {
        private Action<uint> m_onClickEvent;
        private Action<uint, string> m_onClickLeaderTransferEvent;
        private Action<uint, string> m_onClickRemoveEvent;
        private Action m_onClickQuitEvent;
        private uint m_guildId;
        private uint m_allianceLeaderId;
        private string m_guildNameStr;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailBtn;
        [AutoBind("./GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildName;
        [AutoBind("./MemberCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_memberCount;
        [AutoBind("./SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_guildLogo;
        [AutoBind("./LanguageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_regionName;
        [AutoBind("./BaseLocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_baseLocationText;
        [AutoBind("./DisplayItems/LeaderIcon", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_leaderIcon;
        [AutoBind("./DisplayItems/LeadderTransferButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_leaderTransferBtn;
        [AutoBind("./DisplayItems/GuildRemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_removeBtn;
        [AutoBind("./DisplayItems/GuildQuitButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_quitBtn;
        private const string GuildNameFormat = "[{0}]{1}";
        private const string StarNameFormat = "{0}-{1} ({2})";
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetData;
        private static DelegateBridge __Hotfix_IsLeaderGuild;
        private static DelegateBridge __Hotfix_IsSelfGuild;
        private static DelegateBridge __Hotfix_OnClick;
        private static DelegateBridge __Hotfix_OnLeaderTransferClick;
        private static DelegateBridge __Hotfix_OnRemoveClick;
        private static DelegateBridge __Hotfix_OnQuitClick;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_GetPlayerCtx;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private ProjectXPlayerContext GetPlayerCtx()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsLeaderGuild()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSelfGuild()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaderTransferClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuitClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetData(AllianceMemberInfo guildInfo, AllianceInfo allianceInfo, Dictionary<string, UnityEngine.Object> resDict, Action<uint> clickEvent, Action<uint, string> clickLeaderTransferEvent = null, Action<uint, string> clickRemoveEvent = null, Action clickQuitEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

