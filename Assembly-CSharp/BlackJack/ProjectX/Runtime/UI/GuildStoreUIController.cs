﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class GuildStoreUIController : UIControllerBase
    {
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./GuildWarehouseItemCategoryToggleGroup/GuildWarehouseItemCategoryToggleGroupUIPrefab/Viewport/Content/ShipWalletMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipWalletMenuItemCommonUIStateController;
        [AutoBind("./GuildWarehouseItemCategoryToggleGroup/GuildWarehouseItemCategoryToggleGroupUIPrefab/Viewport/Content/ShipWalletMenuItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipWalletMenuItemButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildStoreUIStateCtrl;
        [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EmptyPanelUIStateCtrl;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_GetEmptyUIProcess;
        private static DelegateBridge __Hotfix_SetWalletMenuItemButtonSelect;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess GetEmptyUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetWalletMenuItemButtonSelect(bool isSelect)
        {
        }
    }
}

