﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildBuildingHudItemUIController : UIControllerBase
    {
        private bool m_isProcessing;
        private GuildBuildingInfo m_guildBuildingInfo;
        private ProGuildBuildingExInfo m_guildBuildingExInfo;
        private ProGuildBattleClientSyncInfo m_battleInfo;
        private double m_currProcessTimeTotal;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildBuildingHudItemStateCtrl;
        [AutoBind("./ProcessImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProcessImage;
        [AutoBind("./TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./TextGroup/ProcessValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProcessValueText;
        private static DelegateBridge __Hotfix_UpdateBuildingHudItem;
        private static DelegateBridge __Hotfix_CalculateDictancePowBetweenBuildings;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_get_BuildingInstanceId;

        [MethodImpl(0x8000)]
        public double CalculateDictancePowBetweenBuildings(GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingHudItem(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo)
        {
        }

        public ulong BuildingInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

