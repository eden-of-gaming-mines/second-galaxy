﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class RankingListTotalRankingUIController : UIControllerBase
    {
        private const string LogoAssetName = "LogoUIPrefab";
        private const string StatePersonal = "Personal";
        private const string StateGuild = "Guild";
        private const string StateAlliance = "Alliance";
        private const string StateNormal = "Normal";
        private const string StateNobady = "Nobody";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<RankingListType> EventOnEnterSingleRankingListButtonClick;
        private CommonCaptainIconUIController m_killCountFirstPlayrItemCtrl;
        private CommonCaptainIconUIController m_killValueFirstPlayrItemCtrl;
        private CommonCaptainIconUIController m_continueKillCountFirstPlayrItemCtrl;
        private CommonCaptainIconUIController m_characterGradeFirstPlayrItemCtrl;
        private GuildLogoController m_guildLogoController;
        private GuildLogoController m_allianceLogoController;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_totalRankStateCtrl;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillCountInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillValueInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillContinueInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CharacterGradeInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/AllianceBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllianceInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/GuildBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildInfoCtrl;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoKillCountListButton;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoKillValueListButton;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoContinueKillCountListButton;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoCharacterGradeListButton;
        [AutoBind("./ScrollView/Viewport/Content/AllianceBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoAllianceInfoListButton;
        [AutoBind("./ScrollView/Viewport/Content/GuildBgImage/Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GotoGuildInfoListButton;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillCountNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage/KillValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillValueNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage/ContinueKillCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContinueKillCountNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage/CharacterGradeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterGradeNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/AllianceBgImage/EvaluateScoreText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllianceEvaluateScoreNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/GuildBgImage/EvaluateScoreText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildEvaluateScoreNumberTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillCountNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillValueNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContinueKillCountNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterGradeNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/AllianceBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllianceNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/GuildBgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildNameTxt;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage/ArmyGroupNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillCountArmyGroupNameText;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage/ArmyGroupNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillValueArmyGroupNameText;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage/ArmyGroupNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContinueKillCountArmyGroupNameText;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage/ArmyGroupNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterGradeArmyGroupNameText;
        [AutoBind("./ScrollView/Viewport/Content/KillCountBgImage/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject KillCountCommonItemDummy;
        [AutoBind("./ScrollView/Viewport/Content/KillValueBgImage/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject KillValueCommonItemDummy;
        [AutoBind("./ScrollView/Viewport/Content/KillContinueBgImage/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ContinueKillCountCommonItemDummy;
        [AutoBind("./ScrollView/Viewport/Content/CharacterGradeBgImage/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CharacterGradeCommonItemDummy;
        [AutoBind("./ScrollView/Viewport/Content/GuildBgImage/GuildDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildItemDummy;
        [AutoBind("./ScrollView/Viewport/Content/AllianceBgImage/AllianceDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AllianceItemDummy;
        private const string GuildCodeFormat = "[{0}]";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdatePersonalTotalRankingListInfo;
        private static DelegateBridge __Hotfix_UpdateGuildTotalRankingListInfo;
        private static DelegateBridge __Hotfix_UpdateAllianceTotalRankingListInfo;
        private static DelegateBridge __Hotfix_UpdateTotalRankingInfo;
        private static DelegateBridge __Hotfix_OnEnterKillCountRankingListButtonClick;
        private static DelegateBridge __Hotfix_OnEnterKillValueRankingListButtonClick;
        private static DelegateBridge __Hotfix_OnEnterContinueKillCountRankingListButtonClick;
        private static DelegateBridge __Hotfix_OnEnterCharacterGradeListButtonClick;
        private static DelegateBridge __Hotfix_OnEnterGuildEvaluateScoreListButtonClick;
        private static DelegateBridge __Hotfix_OnEnterAllianceEvaluateScoreListButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnEnterSingleRankingListButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnEnterSingleRankingListButtonClick;

        public event Action<RankingListType> EventOnEnterSingleRankingListButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterAllianceEvaluateScoreListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterCharacterGradeListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterContinueKillCountRankingListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterGuildEvaluateScoreListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterKillCountRankingListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnterKillValueRankingListButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllianceTotalRankingListInfo(RankingTargetInfo allianceInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildTotalRankingListInfo(RankingTargetInfo guildInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePersonalTotalRankingListInfo(RankingTargetInfo killCountFirstPlayerInfo, RankingTargetInfo killValueFirstPlayerInfo, RankingTargetInfo continueKillCountFirstPlayerInfo, RankingTargetInfo characterGradeFirstPlayerInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTotalRankingInfo(RankingPlayerBasicInfo playerInfo, int score, CommonUIStateController uiStateCtrl, CommonCaptainIconUIController captainIconUICtrl, Text playerNameText, Text playerArmyGroupNameText, Text scoreText, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

