﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class MonthCardSubscriptionUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseBtn;
        [AutoBind("./BGImages/BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGBtn;
        [AutoBind("./PrivilegeDescTipPanel/ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyBtn;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/IOSDetailInfo/URLGroup/URLButton01", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UserAgreementLinkButtonIOS;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/IOSDetailInfo/URLGroup/URLButton01", AutoBindAttribute.InitState.NotInit, false)]
        public Text UserAgreementLinkTextIOS;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/IOSDetailInfo/URLGroup/URLButton02", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UserPrivacyLinkButtonIOS;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/IOSDetailInfo/URLGroup/URLButton02", AutoBindAttribute.InitState.NotInit, false)]
        public Text UserPrivacyLinkTextIOS;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/GoogleplayDetailInfo/URLGroup/URLButton01", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UserAgreementLinkButtonGP;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/GoogleplayDetailInfo/URLGroup/URLButton01", AutoBindAttribute.InitState.NotInit, false)]
        public Text UserAgreementLinkTextGP;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/GoogleplayDetailInfo/URLGroup/URLButton02", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UserPrivacyLinkButtonGP;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/GoogleplayDetailInfo/URLGroup/URLButton02", AutoBindAttribute.InitState.NotInit, false)]
        public Text UserPrivacyLinkTextGP;
        [AutoBind("./PrivilegeDescTipPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrivilegeDescTipUIStateCtrl;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/IOSDetailInfo/IOSDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text IOSDescText;
        [AutoBind("./PrivilegeDescTipPanel/Scroll View/Viewport/Content/GoogleplayDetailInfo/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GPDescText;
        [AutoBind("./PrivilegeDescTipPanel/ConfirmButtonGroup/ConfirmButton/CostGroup/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CostText;
        [AutoBind("./PrivilegeDescTipPanel/ConfirmButtonGroup/ConfirmButton/CostGroup/CostIconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CostCurrency;
        private static DelegateBridge __Hotfix_GetSubscriptionUIProcess;
        private static DelegateBridge __Hotfix_GetAgreementUrl;
        private static DelegateBridge __Hotfix_GetPrivicyUrl;
        private static DelegateBridge __Hotfix_UpdateContent;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public string GetAgreementUrl()
        {
        }

        [MethodImpl(0x8000)]
        public string GetPrivicyUrl()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSubscriptionUIProcess(bool isShow, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateContent(LBRechargeMonthlyCard cardInfo)
        {
        }
    }
}

