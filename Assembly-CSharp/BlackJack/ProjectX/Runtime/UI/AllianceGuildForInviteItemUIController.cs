﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceGuildForInviteItemUIController : UIControllerBase, IScrollItem
    {
        private Action<uint> m_onClickEvent;
        private Action<uint> m_onClickInviteEvent;
        private uint m_guildId;
        private readonly Color m_hasAllianceColor;
        private readonly Color m_isInMyAllianceColor;
        private readonly Color m_hasInvitedColor;
        private readonly Color m_noAllianceColor;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailBtn;
        [AutoBind("./GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildName;
        [AutoBind("./MemberCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_memberCount;
        [AutoBind("./SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_guildLogo;
        [AutoBind("./LanguageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_regionName;
        [AutoBind("./OfficeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_baseLocationText;
        [AutoBind("./AllianceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceName;
        [AutoBind("./InviteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_inviteBtn;
        private const string GuildNameFormat = "[{0}]{1}";
        private const string StarNameFormat = "{0}-{1} ({2})";
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetData;
        private static DelegateBridge __Hotfix_OnClick;
        private static DelegateBridge __Hotfix_OnInviteClick;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_GetPlayerCtx;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private ProjectXPlayerContext GetPlayerCtx()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInviteClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetData(GuildSimplestInfo guildInfo, AllianceInfo allianceInfo, List<uint> invitedGuildIds, Dictionary<string, UnityEngine.Object> resDict, Action<uint> clickEvent, Action<uint> clickInviteEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

