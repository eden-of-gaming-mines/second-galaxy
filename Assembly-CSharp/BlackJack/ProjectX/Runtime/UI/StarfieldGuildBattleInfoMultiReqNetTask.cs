﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarfieldGuildBattleInfoMultiReqNetTask : NetWorkTransactionTask
    {
        private int m_processedCount;
        private readonly HashSet<int> m_starFieldIdSet;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildCurrStarfieldGuildBattleInfoAck;

        [MethodImpl(0x8000)]
        public StarfieldGuildBattleInfoMultiReqNetTask(HashSet<int> starFieldIdSet, bool needBlockGlobalUI = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCurrStarfieldGuildBattleInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

