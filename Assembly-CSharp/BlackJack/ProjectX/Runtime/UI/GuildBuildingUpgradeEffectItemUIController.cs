﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildBuildingUpgradeEffectItemUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeEffectItemStateCtrl;
        [AutoBind("./NameText/NumberRightText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberRightText;
        [AutoBind("./NameText/NumberLeftText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberLeftText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        private static DelegateBridge __Hotfix_UpdateUpgradeEffectItemCompare;
        private static DelegateBridge __Hotfix_UpdateUpgradeEffectItemSingle;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUpgradeEffectItemCompare(string desc, string baseValue, string nextValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUpgradeEffectItemSingle(string desc)
        {
        }
    }
}

