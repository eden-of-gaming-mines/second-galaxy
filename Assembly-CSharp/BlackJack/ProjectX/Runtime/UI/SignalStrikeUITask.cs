﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SignalStrikeUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private CaptainSortType m_currCaptainSortType;
        public static string ParamKey_DelegateSignal;
        public static string ParamKey_MainUIIntent;
        private LBSignalDelegateBase m_delegateSignal;
        private UIIntent m_mainUIIntent;
        private SignalStrikeUIController m_mainCtrl;
        private AllowInShipGroupUIController m_allowInShipGroupUIController;
        private List<LBStaticHiredCaptain> m_captainInfoCacheList;
        private List<ulong> m_selectedCaptainInsIdList;
        private bool m_isOnlyShowSelected;
        private int m_currentSignalShipCountLimit;
        private int m_currentSelectionIndex;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTaskEnd;
        public const string TaskName = "SignalStrikeUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartSignalStrikeUITask;
        private static DelegateBridge __Hotfix_SetTaskEndAction;
        private static DelegateBridge __Hotfix_CaptainCanBeQuickForm;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnCaptainItemSelectionButtonClicked;
        private static DelegateBridge __Hotfix_OnCaptainSortTypeChanged;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnQuickFormButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnOnlyShowSelectedToggleChanged;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnTipsCancelButtonClick;
        private static DelegateBridge __Hotfix_OnTipsConfirmButtonClick;
        private static DelegateBridge __Hotfix_StartSignaleStrikeNetTask;
        private static DelegateBridge __Hotfix_OnAllowInShipGroupButtonClick;
        private static DelegateBridge __Hotfix_OnSignalTimeDetailButtonClick;
        private static DelegateBridge __Hotfix_OnHitTipWindowsBGButtonClick;
        private static DelegateBridge __Hotfix_GetParamDataFromUIIntent;
        private static DelegateBridge __Hotfix_GetCaptainInfoSortedList;
        private static DelegateBridge __Hotfix_CaptainInfoComparer;
        private static DelegateBridge __Hotfix_CaptainInfoComparerForFight;
        private static DelegateBridge __Hotfix_CaptainInfoComparerForTransport;
        private static DelegateBridge __Hotfix_CaptainInfoComparerForCollecting;
        private static DelegateBridge __Hotfix_GetSuitableCaptainListStartIndexWithSelection;
        private static DelegateBridge __Hotfix_QuickFormForDelegateFight;
        private static DelegateBridge __Hotfix_QuickFormForDelegateMineral;
        private static DelegateBridge __Hotfix_QuickFormForDelegateTransport;
        private static DelegateBridge __Hotfix_CanBeSelected;
        private static DelegateBridge __Hotfix_ClearDataCache;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_EventOnTaskEnd;
        private static DelegateBridge __Hotfix_remove_EventOnTaskEnd;

        public event Action EventOnTaskEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SignalStrikeUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private static bool CanBeSelected(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CaptainCanBeQuickForm()
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainInfoComparer(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainInfoComparerForCollecting(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainInfoComparerForFight(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainInfoComparerForTransport(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void GetCaptainInfoSortedList()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetParamDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableCaptainListStartIndexWithSelection(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipGroupButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainItemSelectionButtonClicked(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainSortTypeChanged(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHitTipWindowsBGButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOnlyShowSelectedToggleChanged(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuickFormButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSignalTimeDetailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipsCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipsConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void QuickFormForDelegateFight()
        {
        }

        [MethodImpl(0x8000)]
        private void QuickFormForDelegateMineral()
        {
        }

        [MethodImpl(0x8000)]
        private void QuickFormForDelegateTransport()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTaskEndAction(Action taskEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void StartSignaleStrikeNetTask()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartSignalStrikeUITask(LBSignalDelegateBase lbSignal, Action<bool> onPipeLineEnd, Action actionOnTaskEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum CaptainSortType
        {
            Attack,
            Collecting
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedLoadDynamicRes,
            IsResumeTask,
            IsCaptainSortTypeChanged,
            IsOnlyShowSelectedChanged,
            IsCaptainSelectionChanged
        }
    }
}

