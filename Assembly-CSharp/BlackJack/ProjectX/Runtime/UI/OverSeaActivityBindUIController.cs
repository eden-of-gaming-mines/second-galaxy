﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class OverSeaActivityBindUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnRewardItemClick;
        public const string RedeemRewardButtonStateGet = "Get";
        public const string RedeemRewardButtonStateNoGet = "NoGet";
        public const string RedeemRewardButtonStateAccomplish = "Accomplish";
        private const string PanelStateShow = "Show";
        private const string PanelStateClose = "Close";
        private const string BindRewardItem = "BindRewardItem";
        private List<FakeLBStoreItem> m_cachedRewardItems;
        private Dictionary<string, UnityEngine.Object> m_cachedDynamicDict;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect m_scrollRect;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./Scroll View/RewardItemRoot/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_rewardItemRoot;
        [AutoBind("./GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_redeemRewardButton;
        [AutoBind("./GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_redeemRewardButtonStateCtrl;
        [AutoBind("./BoundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bindButton;
        [AutoBind("./SimpleItemInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_simpleItemInfoTf;
        private static DelegateBridge __Hotfix_GetPanelProcess;
        private static DelegateBridge __Hotfix_UpdateViewForRewards;
        private static DelegateBridge __Hotfix_UpdateViewForButtons;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnItemPoolObjectFill;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;

        public event Action<UIControllerBase> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetPanelProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPoolObjectFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewForButtons(string redeemRewardButtonStateName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewForRewards(List<FakeLBStoreItem> items, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

