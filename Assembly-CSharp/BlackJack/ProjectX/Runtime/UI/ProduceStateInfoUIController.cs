﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceStateInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<List<CostInfo>> EventOnAddProduceCountButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<List<CostInfo>> EventOnReduceProduceCountButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStopProduceButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStartProduceButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGetProduceButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSpeedUpButtonClick;
        protected bool m_isSelfProduceMode;
        protected LBProductionLine m_currSelfProduceLine;
        protected GuildProductionLineInfo m_currGuildProduceLine;
        protected int m_currBlueprintIdInIdleMode;
        protected List<CostInfo> m_orignalCostList;
        protected List<CostInfo> m_currCostListInIdleMode;
        protected DateTime m_nextTickTime;
        private const string StartProduceMode = "StartProduce";
        private const string ProducingMode = "Producing";
        private const string GetProduceMode = "GetProduce";
        private const string SelfProduceMode = "Normal";
        private const string GuildProduceMode = "Guild";
        private const string GuildMoneyIcon = "Guild";
        private const string SelfMoneyIcon = "personage";
        [AutoBind("./StartProducePanel/BGImages/MoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MoneyIconStateCtrl;
        [AutoBind("./StartProducePanel/TimeReduceImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeReduceEffectImage;
        [AutoBind("./StartProducePanel/CurrencyReduceImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CurrencyReduceEffectImage;
        [AutoBind("./ProducingPanel/TimePanel/MakingProgressBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MakingProgressBGImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController uiStateCtrl;
        [AutoBind("./StartProducePanel/ReduceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ReduceButton;
        [AutoBind("./StartProducePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./StartProducePanel/MakingButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StartMakingButton;
        [AutoBind("./ProducingPanel/StopMakingButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StopMakingButton;
        [AutoBind("./CompletePanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GetProduceButton;
        [AutoBind("./StartProducePanel/MakingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MakingNumberText;
        [AutoBind("./StartProducePanel/ConsumeTimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeNumberTextForStartProduce;
        [AutoBind("./ProducingPanel/TimePanel/ConsumeTimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeNumberTextForProducing;
        [AutoBind("./StartProducePanel/ConsumeCountNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProduceItemCountText;
        [AutoBind("./StartProducePanel/ConsumeMaterialsNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ConsumeMaterialsNumberText;
        [AutoBind("./ProducingPanel/GuildButtonGroup/StopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StopGuildProduceButton;
        [AutoBind("./ProducingPanel/GuildButtonGroup/SpeedUpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildProduceSpeedUpButton;
        [AutoBind("./StartProducePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StartPanelStateCtrl;
        [AutoBind("./ProducingPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProducingPanelStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateCompleteStateInfoForSelfProduce;
        private static DelegateBridge __Hotfix_UpdateCompleteStateInfoForGuildProduce;
        private static DelegateBridge __Hotfix_SetCaptainEffectState;
        private static DelegateBridge __Hotfix_SetToNotSelectedStateForSelf;
        private static DelegateBridge __Hotfix_SetToNotSelectedStateForGuild;
        private static DelegateBridge __Hotfix_SetToNormalState;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateUIInfo4StartProduceMode_Self;
        private static DelegateBridge __Hotfix_UpdateUIInfo4ProducingMode_Self;
        private static DelegateBridge __Hotfix_UpdateUIInfo4StartProduceMode_Guild;
        private static DelegateBridge __Hotfix_UpdateUIInfo4ProducingMode_Guild;
        private static DelegateBridge __Hotfix_UpdateUIInfo4ProduceCompleteMode;
        private static DelegateBridge __Hotfix_SetProduceCountView;
        private static DelegateBridge __Hotfix_SetCurrencyTotalCostNum_0;
        private static DelegateBridge __Hotfix_SetCurrencyTotalCostNum_1;
        private static DelegateBridge __Hotfix_GetTimeCostString;
        private static DelegateBridge __Hotfix_GetCurrencyTotalCost;
        private static DelegateBridge __Hotfix_OnAddProduceCountButtonClick;
        private static DelegateBridge __Hotfix_OnReduceProduceCountButtonClick;
        private static DelegateBridge __Hotfix_OnStartProduceButtonClick;
        private static DelegateBridge __Hotfix_OnGetProduceButtonClick;
        private static DelegateBridge __Hotfix_OnStopProduceButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddProduceCountButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddProduceCountButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnReduceProduceCountButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnReduceProduceCountButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStopProduceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStopProduceButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStartProduceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStartProduceButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGetProduceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGetProduceButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<List<CostInfo>> EventOnAddProduceCountButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGetProduceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<CostInfo>> EventOnReduceProduceCountButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSpeedUpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartProduceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStopProduceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected int GetCurrencyTotalCost(List<CostInfo> costInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetTimeCostString(int costTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddProduceCountButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGetProduceButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReduceProduceCountButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSpeedUpButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStartProduceButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStopProduceButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainEffectState(ProduceLineState state, int batch, int orignalCostTime, int costTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrencyTotalCostNum(List<CostInfo> costInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrencyTotalCostNum(ulong costMoney)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetProduceCountView(int count, int blueprintId, bool isBind, bool isFreezig)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNormalState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNotSelectedStateForGuild()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNotSelectedStateForSelf()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompleteStateInfoForGuildProduce(ProduceLineState state, GuildProductionLineInfo produceLine, int selectTempleteIdInIdle = 0, int batch = 0, List<CostInfo> orignalCostInfoList = null, List<CostInfo> costInfoListInIdle = null, int orignalCostTime = 0, int costTime = 0, ulong costMoney = 0UL)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompleteStateInfoForSelfProduce(ProduceLineState state, LBProductionLine produceLine, int selectBlueprintIdInIdle = 0, bool isBind = false, bool isFreezig = false, int batch = 0, List<CostInfo> orignalCostInfoList = null, List<CostInfo> costInfoListInIdle = null, int orignalCostTime = 0, int costTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUIInfo4ProduceCompleteMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUIInfo4ProducingMode_Guild(GuildProductionLineInfo produceLine)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUIInfo4ProducingMode_Self(LBProductionLine produceLine)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUIInfo4StartProduceMode_Guild(int templeteId, int batch, int orignalCostTime, int realCostTime, ulong costMoney)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUIInfo4StartProduceMode_Self(int blueprintId, int batch, int orignalCostTime, int realCostTime, bool isBind, bool isFreezig)
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

