﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ItemStoreListUIController : UIControllerBase
    {
        private List<ILBStoreItemClient> m_cachedItemList;
        private Dictionary<string, UnityEngine.Object> m_cachedResDict;
        private GameObject m_itemTemplateGo;
        private int m_selectedIndex;
        private bool m_isShowItemCount;
        private AuctionBuyItemFilterType m_AuctionBuyItemFilterType;
        private bool m_IsShowRankTypeLocked;
        private RankType m_CurRankTypeForGuild;
        private const string m_itemStoreListUIItemPoolName = "ItemStoreListItem";
        private const string m_itemStoreListItemAssetName = "ItemStoreListItem";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ItemStoreTabType> EventOnActiveTabChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDropdownChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemDoubleClicked;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItem3DTouch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnItemListScrollViewClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, CommonItemIconUIController> EventOnItemFilled;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./ItemStoreListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ItemStoreListScrollRect;
        [AutoBind("./ItemStoreListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler ItemListScrollViewClick;
        [AutoBind("./ItemStoreListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ItemStoreListUIItemPool;
        [AutoBind("./ItemStoreListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GridLayoutGroup ContentGridLayoutGroup;
        [AutoBind("./ItemTypeToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ItemTypeToggleGroupGo;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GridLayoutGroup ToggleGridLayoutGroup;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent", AutoBindAttribute.InitState.NotInit, false)]
        public GridLayoutGroup BigToggleGridLayoutGroup;
        [AutoBind("./ItemStoreNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemStoreNameText;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/AllToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle AllToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/ShipToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ShipToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/WeaponToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle WeaponToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/AmmoToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle AmmoToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/DeviceToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle DeviceToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/ComponentToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ComponentToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/ChipToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ChipToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/MaterialToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle MaterialToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/BluePrintToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle BluePrintToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/CrackBoxToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle CrackBoxToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/FeatsBookToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle FeatsBookToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/Content/QuestItemToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle QuestItemToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/AllToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle AllBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/ShipToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ShipBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/WeaponToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle WeaponBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/AmmoToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle AmmoBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/DeviceToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle DeviceBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/ComponentToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ComponentBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/ChipToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ChipBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/MaterialToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle MaterialBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/BluePrintToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle BluePrintBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/CrackBoxToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle CrackBoxBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/FeatsBookToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle FeatsBookBigToggle;
        [AutoBind("./ItemTypeToggleGroup/Viewport/BigToggleContent/QuestItemToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle QuestItemBigToggle;
        [AutoBind("./DropDownButton", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown DropDownButton;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./UnloadAllButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnloadAllButton;
        [AutoBind("./StoreSpace", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StoreSpaceGo;
        [AutoBind("./StoreSpace/StoreSpaceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StoreSpaceText;
        [AutoBind("./StoreSpace/StoreSpaceProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image StoreSpaceProgressBar;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateItemListPool;
        private static DelegateBridge __Hotfix_InitItemListSize;
        private static DelegateBridge __Hotfix_CreateUIShowOrHideEffectInfo;
        private static DelegateBridge __Hotfix_CreateUIShowOrHideProcess;
        private static DelegateBridge __Hotfix_ShowItemTypeToggleGroup;
        private static DelegateBridge __Hotfix_HideSomeItemTypeToggle;
        private static DelegateBridge __Hotfix_ShowStoreSpaceAndDropdownRegion;
        private static DelegateBridge __Hotfix_EnableItemList;
        private static DelegateBridge __Hotfix_EnableShowItemCount;
        private static DelegateBridge __Hotfix_SetItemStoreName;
        private static DelegateBridge __Hotfix_UpdateItemStoreList;
        private static DelegateBridge __Hotfix_UpdateSingleSelectItem;
        private static DelegateBridge __Hotfix_SelectSingleItem;
        private static DelegateBridge __Hotfix_UpdateSingleItemInfo;
        private static DelegateBridge __Hotfix_UpdateDropDownMenu;
        private static DelegateBridge __Hotfix_UpdateToggleState;
        private static DelegateBridge __Hotfix_UpdateItemStoreSpace;
        private static DelegateBridge __Hotfix_GetFirstVisibleItemIndex;
        private static DelegateBridge __Hotfix_GetItemRectByConfId;
        private static DelegateBridge __Hotfix_GetFirstItemRect;
        private static DelegateBridge __Hotfix_SetRankTypeByGuildShipHangar;
        private static DelegateBridge __Hotfix_GetActiveItemByIndex;
        private static DelegateBridge __Hotfix_SetAllActiveItemUnselect;
        private static DelegateBridge __Hotfix_GetCurrentProgressGreenToRedColor;
        private static DelegateBridge __Hotfix_SetRankTypeFlagForGuildShipHangar;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemFill;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemClick;
        private static DelegateBridge __Hotfix_OnItemStoreUIItem3DTouch;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemDoubleClick;
        private static DelegateBridge __Hotfix_OnToggleAllValueChanged;
        private static DelegateBridge __Hotfix_OnToggleShipsValueChanged;
        private static DelegateBridge __Hotfix_OnToggleWeaponValueChanged;
        private static DelegateBridge __Hotfix_OnToggleAmmoValueChanged;
        private static DelegateBridge __Hotfix_OnToggleDeviceValueChanged;
        private static DelegateBridge __Hotfix_OnToggleComponentValueChanged;
        private static DelegateBridge __Hotfix_OnToggleChipValueChanged;
        private static DelegateBridge __Hotfix_OnToggleMaterialValueChanged;
        private static DelegateBridge __Hotfix_OnToggleBluePrintValueChanged;
        private static DelegateBridge __Hotfix_OnToggleCrackBoxValueChanged;
        private static DelegateBridge __Hotfix_OnToggleFeatsBookValueChanged;
        private static DelegateBridge __Hotfix_OnToggleQuestItemValueChanged;
        private static DelegateBridge __Hotfix_OnDropdownValueChanged;
        private static DelegateBridge __Hotfix_OnItemListScrollViewClick;
        private static DelegateBridge __Hotfix_GetFirstExploreTicketItem;
        private static DelegateBridge __Hotfix_GetFirstExploreTicketIndex;
        private static DelegateBridge __Hotfix_add_EventOnActiveTabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnActiveTabChanged;
        private static DelegateBridge __Hotfix_add_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_add_EventOnItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnItemDoubleClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemDoubleClicked;
        private static DelegateBridge __Hotfix_add_EventOnItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnItemListScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemListScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnItemFilled;
        private static DelegateBridge __Hotfix_remove_EventOnItemFilled;

        public event Action<ItemStoreTabType> EventOnActiveTabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDropdownChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemDoubleClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, CommonItemIconUIController> EventOnItemFilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemListScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateItemListPool(int poolSize)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateUIShowOrHideEffectInfo(bool isShow, bool useFullSceneEffect)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess CreateUIShowOrHideProcess(bool isShow, bool useFullSceneEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableItemList(bool isshow)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableShowItemCount(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController GetActiveItemByIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private Color GetCurrentProgressGreenToRedColor(float progress)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFirstExploreTicketIndex()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstExploreTicketItem()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFirstVisibleItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetItemRectByConfId(StoreItemType itemType, int confId)
        {
        }

        [MethodImpl(0x8000)]
        public void HideSomeItemTypeToggle(HashSet<string> hideToggles)
        {
        }

        [MethodImpl(0x8000)]
        public void InitItemListSize(ItemStoreListSizeDesc sizeDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownValueChanged(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItem3DTouch(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemDoubleClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemFill(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleAllValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleAmmoValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleBluePrintValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleChipValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleComponentValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleCrackBoxValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleDeviceValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleFeatsBookValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleMaterialValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleQuestItemValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleShipsValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleWeaponValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SelectSingleItem(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllActiveItemUnselect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRankTypeByGuildShipHangar(RankType rank)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRankTypeFlagForGuildShipHangar(ILBStoreItemClient item, CommonItemIconUIController storeItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowItemTypeToggleGroup(bool isshow, bool isBig = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowStoreSpaceAndDropdownRegion(bool isShowName, bool isShowStoreSpace, bool isShowDropDown, string dropdownTipText = "")
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDropDownMenu(List<string> dropDownItemList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreSpace(ulong currVal, ulong maxSpace)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleItemInfo(int index, ILBStoreItemClient itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleSelectItem(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateToggleState(ItemStoreTabType currTabType, bool isBig = false)
        {
        }

        public enum ItemStoreTabType
        {
            All,
            SpaceShip,
            Weapon,
            Ammo,
            Device,
            Component,
            Chip,
            Material,
            BluePrint,
            CrackBox,
            FeatsBook,
            Other,
            Building,
            Flagship,
            GuildOther
        }
    }
}

