﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class RescueMenuItemUIController : MenuItemUIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        private CommonUIStateController[] m_shipLimitCtrlList;
        private Transform[] m_fleetCaptainTransList;
        private Transform[] m_fleetShipsTransList;
        public LBPVPInvadeRescue m_invadeRescueInfo;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        private CommonCaptainIconUIController m_enemyCtrl;
        private CommonCaptainIconUIController m_friendCtrl;
        private const string AllowInImageUIStateAllowIn = "AllowIn";
        private const string AllowInImageUIStateNotAllowIn = "NotAllowIn";
        public const string ChangeCaptainShowingButtonUIStateCaptain = "Captain";
        public const string ChangeCaptainShowingButtonUIStateShip = "Ship";
        [AutoBind("./TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerNameText;
        [AutoBind("./TargetInfo/Info/UnionAndArmyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_armyGroupNameText;
        [AutoBind("./Detail/Reward/CaptainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_captainGroup;
        [AutoBind("./Detail/Reward/ShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipGroup;
        [AutoBind("./Detail/Reward/CaptainGroup/Item1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetCaptainTrans1;
        [AutoBind("./Detail/Reward/CaptainGroup/Item2", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetCaptainTrans2;
        [AutoBind("./Detail/Reward/CaptainGroup/Item3", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetCaptainTrans3;
        [AutoBind("./Detail/Reward/CaptainGroup/Item4", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetCaptainTrans4;
        [AutoBind("./Detail/Reward/CaptainGroup/Item5", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetCaptainTrans5;
        [AutoBind("./Detail/Reward/ShipGroup/Item1", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetShipTrans1;
        [AutoBind("./Detail/Reward/ShipGroup/Item2", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetShipTrans2;
        [AutoBind("./Detail/Reward/ShipGroup/Item3", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetShipTrans3;
        [AutoBind("./Detail/Reward/ShipGroup/Item4", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetShipTrans4;
        [AutoBind("./Detail/Reward/ShipGroup/Item5", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_fleetShipTrans5;
        [AutoBind("./TargetInfo/Info/IconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_playerIconDummy;
        [AutoBind("./Detail/Reward/TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_enemyPlayerNameText;
        [AutoBind("./Detail/Reward/TargetInfo/Info/IconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_iconDummy;
        [AutoBind("./Detail/Reward/TargetInfo/Info/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_enemyPlayerLevelText;
        [AutoBind("./Detail/Time/StartTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_invadeStartTimeText;
        [AutoBind("./Detail/Time/TimespanToNowInfo/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_invadeTimeLengthToNowText;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowIn/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        [AutoBind("./Detail/RescueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_rescueButton;
        [AutoBind("./Detail/Reward/TextTitle/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeCaptainShowingButton;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_fleetStateCtrl;
        [AutoBind("./Detail/AllowIn/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lossWarningButton;
        [AutoBind("./Detail/LossWarningButton/SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lossWarningPos;
        [AutoBind("./Detail/Reward/TargetInfo/MenuPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_menuPos;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBPVPInvadeRescue, Vector3> EventOnRescueSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetInvadeRescuePVPInfoToUI;
        private static DelegateBridge __Hotfix_SetFleetInfoMode;
        private static DelegateBridge __Hotfix_SwitchFleetInfoMode;
        private static DelegateBridge __Hotfix_GetRescueInfo;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetLossWarningButtonPos;
        private static DelegateBridge __Hotfix_GetLossWarningButtonSize;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetInvadedPlayerInfo;
        private static DelegateBridge __Hotfix_SetFleetCaptainListInfo;
        private static DelegateBridge __Hotfix_SetInvadeEnemyInfo;
        private static DelegateBridge __Hotfix_SetAllowShipType;
        private static DelegateBridge __Hotfix_OnInvadePlayerClick;
        private static DelegateBridge __Hotfix_ClearInvadePlayerClickEvent;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_get_ShipLimitCtrlList;
        private static DelegateBridge __Hotfix_get_FleetCaptainTransList;
        private static DelegateBridge __Hotfix_get_FleetShipsTransList;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRescueSignalOperationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRescueSignalOperationButtonClick;

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBPVPInvadeRescue, Vector3> EventOnRescueSignalOperationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearInvadePlayerClickEvent()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetLossWarningButtonPos()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector2 GetLossWarningButtonSize()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue GetRescueInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInvadePlayerClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllowShipType(LBPVPInvadeRescue rescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFleetCaptainListInfo(LBPVPInvadeRescue rescueInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFleetInfoMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void SetInvadedPlayerInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetInvadeEnemyInfo(LBPVPInvadeRescue rescueInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInvadeRescuePVPInfoToUI(LBPVPInvadeRescue rescueInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchFleetInfoMode()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private CommonUIStateController[] ShipLimitCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private Transform[] FleetCaptainTransList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private Transform[] FleetShipsTransList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

