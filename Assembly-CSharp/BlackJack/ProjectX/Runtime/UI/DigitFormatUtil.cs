﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class DigitFormatUtil
    {
        private static AbbreviationExecutor m_abbreviationExecutor;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_2;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_6;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_1;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_5;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_3;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_7;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_4;
        private static DelegateBridge __Hotfix_GetDigitFormatResult_0;
        private static DelegateBridge __Hotfix_get_AbbreviationExecutorInstance;

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref double sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref short sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref int sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref long sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref float sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref ushort sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref uint sourceValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDigitFormatResult(DigitFormatType type, ref ulong sourceValue)
        {
        }

        private static AbbreviationExecutor AbbreviationExecutorInstance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private class AbbreviationExecutor : DigitFormatUtil.DigitFormatExecutor
        {
            private const string m_postfixK = "K";
            private const string m_postfixM = "M";
            private const string m_postfixB = "B";
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_1;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_5;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_2;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_6;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_3;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_7;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_4;
            private static DelegateBridge __Hotfix_ExecuteDigitFormat_0;
            private static DelegateBridge __Hotfix_GetDigitFormatType;

            public AbbreviationExecutor()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override string ExecuteDigitFormat(ref double sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_0;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5569(this, ref sourceValue);
                }
                if (sourceValue < 1000.0)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 1000000.0)
                {
                    double num = (sourceValue / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                if (sourceValue < 1000000000.0)
                {
                    double num2 = (sourceValue / 1000000.0) - 0.05;
                    return $"{num2:0.#}{"M"}";
                }
                double num3 = (sourceValue / 1000000000.0) - 0.05;
                return $"{num3:0.#}{"B"}";
            }

            public override string ExecuteDigitFormat(ref short sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_1;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5564(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8)
                {
                    return sourceValue.ToString();
                }
                float num = (((float) sourceValue) / 1000f) - 0.05f;
                return $"{num:0.#}{"K"}";
            }

            public override string ExecuteDigitFormat(ref int sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_2;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5562(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 0xf4240)
                {
                    double num = (((double) sourceValue) / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                if (sourceValue < 0x3b9aca00)
                {
                    double num2 = (((double) sourceValue) / 1000000.0) - 0.05;
                    return $"{num2:0.#}{"M"}";
                }
                double num3 = (sourceValue / 0x3b9aca00) - 0.05;
                return $"{num3:0.#}{"B"}";
            }

            public override string ExecuteDigitFormat(ref long sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_3;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5566(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8L)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 0xf4240L)
                {
                    double num = (((double) sourceValue) / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                if (sourceValue < 0x3b9aca00L)
                {
                    double num2 = (((double) sourceValue) / 1000000.0) - 0.05;
                    return $"{num2:0.#}{"M"}";
                }
                double num3 = (sourceValue / 0x3b9aca00L) - 0.05;
                return $"{num3:0.#}{"B"}";
            }

            public override string ExecuteDigitFormat(ref float sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_4;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5568(this, ref sourceValue);
                }
                if (sourceValue < 1000f)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 1000000f)
                {
                    double num = (((double) sourceValue) / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                if (sourceValue < 1E+09f)
                {
                    double num2 = (((double) sourceValue) / 1000000.0) - 0.05;
                    return $"{num2:0.#}{"M"}";
                }
                double num3 = (sourceValue / 1E+09f) - 0.05;
                return $"{num3:0.#}{"B"}";
            }

            public override string ExecuteDigitFormat(ref ushort sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_5;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5565(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8)
                {
                    return sourceValue.ToString();
                }
                float num = (((float) sourceValue) / 1000f) - 0.05f;
                return $"{num:0.#}{"K"}";
            }

            public override string ExecuteDigitFormat(ref uint sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_6;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5563(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 0xf4240)
                {
                    double num = (((double) sourceValue) / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                double num2 = (((double) sourceValue) / 1000000.0) - 0.05;
                return $"{num2:0.#}{"M"}";
            }

            public override string ExecuteDigitFormat(ref ulong sourceValue)
            {
                DelegateBridge bridge = __Hotfix_ExecuteDigitFormat_7;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp5567(this, ref sourceValue);
                }
                if (sourceValue < 0x3e8L)
                {
                    return sourceValue.ToString();
                }
                if (sourceValue < 0xf4240L)
                {
                    double num = (((double) sourceValue) / 1000.0) - 0.05;
                    return $"{num:0.#}{"K"}";
                }
                if (sourceValue < 0x3b9aca00L)
                {
                    double num2 = (((double) sourceValue) / 1000000.0) - 0.05;
                    return $"{num2:0.#}{"M"}";
                }
                double num3 = ((double) (sourceValue / 0x3b9aca00L)) - 0.05;
                return $"{num3:0.#}{"B"}";
            }

            public override DigitFormatUtil.DigitFormatType GetDigitFormatType()
            {
                DelegateBridge bridge = __Hotfix_GetDigitFormatType;
                return ((bridge == null) ? DigitFormatUtil.DigitFormatType.Abbreviation : bridge.__Gen_Delegate_Imp5570(this));
            }
        }

        private abstract class DigitFormatExecutor
        {
            private static DelegateBridge _c__Hotfix_ctor;

            protected DigitFormatExecutor()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public abstract string ExecuteDigitFormat(ref double sourceValue);
            public abstract string ExecuteDigitFormat(ref short sourceValue);
            public abstract string ExecuteDigitFormat(ref int sourceValue);
            public abstract string ExecuteDigitFormat(ref long sourceValue);
            public abstract string ExecuteDigitFormat(ref float sourceValue);
            public abstract string ExecuteDigitFormat(ref ushort sourceValue);
            public abstract string ExecuteDigitFormat(ref uint sourceValue);
            public abstract string ExecuteDigitFormat(ref ulong sourceValue);
            public abstract DigitFormatUtil.DigitFormatType GetDigitFormatType();
        }

        public enum DigitFormatType
        {
            None,
            Abbreviation,
            NumberWithZeroPrecision
        }
    }
}

