﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildWalletDealGroupUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, GuildCurrencyLogFilterType, bool> EventOnToggleValueChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnCurrencyLogItemFill;
        [AutoBind("./CheckboxGroup/InformationPointCheckbox/InformationPointDonateIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx InformationPointDonateIncomeToggleEx;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ScrollViewEasyObjectPool;
        [AutoBind("./CheckboxGroup/CheckboxGroupColliderButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CheckboxGroupColliderButton;
        [AutoBind("./ProjectDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProjectDropdownCommonUIStateController;
        [AutoBind("./CheckboxGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CheckboxGroupGameObject;
        [AutoBind("./CheckboxGroup/InformationPointCheckbox/InformationPointGuildActionOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx InformationPointGuildActionOutcomeToggleEx;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollViewLoopVerticalScrollRect;
        [AutoBind("./CheckboxGroup/InformationPointCheckbox/InformationPointOccupyIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx InformationPointOccupyIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TacticalPointCheckbox/TacticalPointLearnOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TacticalPointLearnOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TacticalPointCheckbox/TacticalPointDonateIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TacticalPointDonateIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TacticalPointCheckbox/TacticalPointGuildActionIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TacticalPointGuildActionIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildProductionOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildProductionOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyWarOnSovereignOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyWarOnSovereignOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyOccupyAintenanceOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyOccupyAintenanceOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildRedeployOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildRedeployOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyTacticalPointOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyTacticalPointOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneySpeedUpOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneySpeedUpOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildTradeOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildTradeOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyPurchaseOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyPurchaseOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildGiftBagIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildGiftBagIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyDonateIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyDonateIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildActionIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildActionIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildTradeTransportSuccessedIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildReturnedIncome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildReturnedIncomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildPrepayOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildPrepayOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox/TradeMoneyGuildOutcome", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TradeMoneyGuildOutcomeToggleEx;
        [AutoBind("./CheckboxGroup/InformationPointCheckbox", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InformationPointCheckboxGameObject;
        [AutoBind("./CheckboxGroup/TacticalPointCheckbox", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TacticalPointCheckboxGameObject;
        [AutoBind("./CheckboxGroup/TradeMoneyCheckbox", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TradeMoneyCheckboxGameObject;
        [AutoBind("./ProjectDropdown/ArrowDow", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ArrowDowGameObject;
        [AutoBind("./ProjectDropdown/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FrameImageButton;
        [AutoBind("./PageBgImage/PageNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PageNumberText;
        [AutoBind("./NextPageButton", AutoBindAttribute.InitState.NotInit, false)]
        public Image NextPageButtonImage;
        [AutoBind("./PreviousPageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PreviousPageButton;
        [AutoBind("./ProjectDropdown/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ArrowUpGameObject;
        [AutoBind("./NextPageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NextPageButton;
        private static DelegateBridge __Hotfix_SetDealGroupActive;
        private static DelegateBridge __Hotfix_SetDealGroupLogTypeActive;
        private static DelegateBridge __Hotfix_SetPageNumberText;
        private static DelegateBridge __Hotfix_SetCurrencyLogInfoList;
        private static DelegateBridge __Hotfix_ResetAllToggle;
        private static DelegateBridge __Hotfix_add_EventOnToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnCurrencyLogItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnCurrencyLogItemFill;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnNewItemCreateInPool;
        private static DelegateBridge __Hotfix_OnLogUIItemFill;
        private static DelegateBridge __Hotfix_TradeMoneyGuildActionIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyDonateIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildGiftBagIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyPurchaseOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildTradeOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneySpeedUpOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyTacticalPointOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildRedeployOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyOccupyAintenanceOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyWarOnSovereignOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildProductionOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TacticalPointGuildActionIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TacticalPointDonateIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TacticalPointLearnOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_InformationPointOccupyIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_InformationPointDonateIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_InformationPointGuildActionOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildTradeTransportSuccessedIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildReturnedIncomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildPrepayOutcomeToggleValueChanged;
        private static DelegateBridge __Hotfix_TradeMoneyGuildOutcomeToggleValueChanged;

        public event Action<UIControllerBase> EventOnCurrencyLogItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, GuildCurrencyLogFilterType, bool> EventOnToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void InformationPointDonateIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void InformationPointGuildActionOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void InformationPointOccupyIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogUIItemFill(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewItemCreateInPool(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetAllToggle()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrencyLogInfoList(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDealGroupActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDealGroupLogTypeActive(GuildWalletLogUITask.LogTabType logTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPageNumberText(int index, int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        private void TacticalPointDonateIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TacticalPointGuildActionIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TacticalPointLearnOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyDonateIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildActionIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildGiftBagIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildPrepayOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildProductionOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildRedeployOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildReturnedIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildTradeOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyGuildTradeTransportSuccessedIncomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyOccupyAintenanceOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyPurchaseOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneySpeedUpOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyTacticalPointOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        private void TradeMoneyWarOnSovereignOutcomeToggleValueChanged(UIControllerBase ctrlBase, bool isSelected)
        {
        }
    }
}

