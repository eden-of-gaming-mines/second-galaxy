﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StarMapItemLocationUIController : UIControllerBase
    {
        private Vector2 m_starMapLeftBottomPos;
        private Vector2 m_starMapRightTopPos;
        private float m_arrowRotationRadius;
        private const float m_arrowTopToTargetDistance = 25f;
        public int m_solarSystemId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnLocationUIClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemCtrl;
        [AutoBind("./CenterPosRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_centerPosRoot;
        [AutoBind("./CenterPosRoot/PosArrowRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_posArrowRoot;
        [AutoBind("./CenterPosRoot/PosArrowRoot", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_locationUIButton;
        [AutoBind("./CenterPosRoot/BorderRoot/LeftBottomBorderObj", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftBottomBorderTrans;
        [AutoBind("./CenterPosRoot/BorderRoot/RightTopBorderObj", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightTopBorderTrans;
        [AutoBind("./CenterPosRoot/LeftGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftGroupTrans;
        [AutoBind("./CenterPosRoot/RightGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightGroupTrans;
        [AutoBind("./CenterPosRoot/LeftGroup/SOSSignal", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfRescueIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/SOSSignal", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfRescueIconOnRightGroup;
        [AutoBind("./CenterPosRoot/LeftGroup/SOSSignalAlly", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allianceRescueIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/SOSSignalAlly", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allianceRescueIconOnRightGroup;
        [AutoBind("./CenterPosRoot/LeftGroup/EmegencySignal", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfEmegencySignalIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/EmegencySignal", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfEmegencySignalIconOnRightGroup;
        [AutoBind("./CenterPosRoot/LeftGroup/GuildBase", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_guildBaseIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/GuildBase", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_guildBaseIconOnRightGroup;
        [AutoBind("./CenterPosRoot/LeftGroup/TradeShip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tradeShipIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/TradeShip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_tradeShipIconOnRightGroup;
        [AutoBind("./CenterPosRoot/LeftGroup/FlagShipHangar", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_flagShipHangarIconOnLeftGroup;
        [AutoBind("./CenterPosRoot/RightGroup/FlagShipHangar", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_flagShipHangarIconOnRightGroup;
        private RectTransform m_referenceTrans;
        private Vector2 m_leftBottomBorderOffsetVec;
        private Vector2 m_rightTopBorderOffsetVec;
        private LocationUIInfoType m_currMajorInfoType;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetItemIconState;
        private static DelegateBridge __Hotfix_SetLocationSolarSystemId;
        private static DelegateBridge __Hotfix_SetReferenceTransform;
        private static DelegateBridge __Hotfix_SetStarMapLeftBottomPos;
        private static DelegateBridge __Hotfix_SetStarMapRightTopPos;
        private static DelegateBridge __Hotfix_DoOrientedToStarMapPos;
        private static DelegateBridge __Hotfix_IsLocationUIAllVisible;
        private static DelegateBridge __Hotfix_IsPosStarMapUIInner;
        private static DelegateBridge __Hotfix_GetCrossPointForLineWithStarMapRect;
        private static DelegateBridge __Hotfix_SetItemIconStateByLocationUIInfoType;
        private static DelegateBridge __Hotfix_OnLocationItemUIClick;
        private static DelegateBridge __Hotfix_add_EventOnLocationUIClick;
        private static DelegateBridge __Hotfix_remove_EventOnLocationUIClick;
        private static DelegateBridge __Hotfix_Clear;

        public event Action<UIControllerBase> EventOnLocationUIClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void DoOrientedToStarMapPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetCrossPointForLineWithStarMapRect(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsLocationUIAllVisible(Vector2 uiPos)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPosStarMapUIInner(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocationItemUIClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIconState(LogicBlockStarMapInfoClient.LocationItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemIconStateByLocationUIInfoType(LogicBlockStarMapInfoClient.LocationItemInfo itemInfo, LocationUIInfoType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocationSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetReferenceTransform(RectTransform trans)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarMapLeftBottomPos(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarMapRightTopPos(Vector2 pos)
        {
        }

        private enum LocationUIInfoType
        {
            None,
            ShipOrMSLocation,
            SelfRescue,
            AllianceRescue,
            EmegencySignal,
            GuildBase,
            TradeShip,
            FlagShipHangar
        }
    }
}

