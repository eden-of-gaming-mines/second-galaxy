﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BaseRedeploySimpleInfoUIController : UIControllerBase
    {
        private MSRedeployInfo m_redeployInfo;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx RedeployInfoButton;
        [AutoBind("./Detail/RedeployPrepareGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RedeployPrepareGroup;
        [AutoBind("./Detail/ProgressBar/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBarImage;
        [AutoBind("./Detail/RedeployPrepareGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RemainingTimeText;
        [AutoBind("./Detail/RedeployReadyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RedeployReadyGroup;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowBaseRedeployInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBaseRedeployInfo(MSRedeployInfo redeployInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

