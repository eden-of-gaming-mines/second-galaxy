﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class QuestUIHelper
    {
        public const string UnknownStr = "???";
        [CompilerGenerated]
        private static Comparison<VirtualBuffDesc> <>f__mg$cache0;
        [CompilerGenerated]
        private static Comparison<VirtualBuffDesc> <>f__mg$cache1;
        [CompilerGenerated]
        private static Comparison<VirtualBuffDesc> <>f__mg$cache2;
        [CompilerGenerated]
        private static Comparison<VirtualBuffDesc> <>f__mg$cache3;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsExistUnreceivedRewardInActivity;
        private static DelegateBridge __Hotfix_GetSubStoryQuestCount;
        private static DelegateBridge __Hotfix_ShowStoryAndQuestConerTip;
        private static DelegateBridge __Hotfix_GetAcceptAndUnAccpetQuestCount;
        private static DelegateBridge __Hotfix_GetFactionAcceptAndUnAccpetQuestCount;
        private static DelegateBridge __Hotfix_IsShowInQuestList;
        private static DelegateBridge __Hotfix_GetAcceptQuestRealReward;
        private static DelegateBridge __Hotfix_GetQuestRewardInfoByQuestConf;
        private static DelegateBridge __Hotfix_GetFactionCreditValByRewadInfo;
        private static DelegateBridge __Hotfix_GetQuestRandomRewardItemDropInfoId;
        private static DelegateBridge __Hotfix_GetQuestRewardIconResPathWithInfo;
        private static DelegateBridge __Hotfix_GetBattleQuestRewardIconResPathWithInfo;
        private static DelegateBridge __Hotfix_GetFactionCreditoIconResFullPath;
        private static DelegateBridge __Hotfix_GetRewardValueWithInfo;
        private static DelegateBridge __Hotfix_GetRewardStringWithInfo;
        private static DelegateBridge __Hotfix_QuestDescParamReplaceByTypeInfo;
        private static DelegateBridge __Hotfix_GetQuestLevelByQuestConf;
        private static DelegateBridge __Hotfix_GetQuestShowLevelByQuestConf;
        private static DelegateBridge __Hotfix_GetQuestCompleteSolarSystemId;
        private static DelegateBridge __Hotfix_GetQuestCompleteNpcDNId;
        private static DelegateBridge __Hotfix_GetQuestAcceptNpcDNId;
        private static DelegateBridge __Hotfix_GetQuestRecommendShipIconResPath;
        private static DelegateBridge __Hotfix_GetFreeSignalQuestSimpleName;
        private static DelegateBridge __Hotfix_GetJumpCountForAcceptQuest;
        private static DelegateBridge __Hotfix_GetJumpCountForUnAcceptQuest;
        private static DelegateBridge __Hotfix_GetJumpCountForQuest;
        private static DelegateBridge __Hotfix_GetQuestAcceptSolarSystemId;
        private static DelegateBridge __Hotfix_GetQuestCompleteCondString;
        private static DelegateBridge __Hotfix_GetProcessingQuestCompCondString;
        private static DelegateBridge __Hotfix_GetQuestIconResPath;
        private static DelegateBridge __Hotfix_IsQuestMustCompleteInTime;
        private static DelegateBridge __Hotfix_IsEmergencySignalQuest;
        private static DelegateBridge __Hotfix_IsProcessingQuestTimeout;
        private static DelegateBridge __Hotfix_IsSignalTimeout;
        private static DelegateBridge __Hotfix_CollectVirtualBuffIconResPath;
        private static DelegateBridge __Hotfix_GetVirtualBuffSortList;
        private static DelegateBridge __Hotfix_VirtualBuffDescComparer;
        private static DelegateBridge __Hotfix_IsAutoMoveAfterQuestComeplete;
        private static DelegateBridge __Hotfix_IsPlayerLossWhenDeadInQuest;
        private static DelegateBridge __Hotfix_IsPlayerLossWhenDeadInSignal;
        private static DelegateBridge __Hotfix_IsPlayerLossWhenDeadInSpaceSignal;
        private static DelegateBridge __Hotfix_IsPlayerShipDestroyInScene;
        private static DelegateBridge __Hotfix_OnChapterQuestCutsceneStartNtf;
        private static DelegateBridge __Hotfix_PlayCharacterCutScene;
        private static DelegateBridge __Hotfix_GetFristSignalWithType;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public static List<string> CollectVirtualBuffIconResPath(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static KeyValuePair<int, int> GetAcceptAndUnAccpetQuestCount()
        {
        }

        [MethodImpl(0x8000)]
        public static List<QuestRewardInfo> GetAcceptQuestRealReward(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBattleQuestRewardIconResPathWithInfo(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFactionAcceptAndUnAccpetQuestCount()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFactionCreditoIconResFullPath(int bigFactionId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFactionCreditValByRewadInfo(QuestRewardInfo confRewardItem)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFreeSignalQuestSimpleName(ConfigDataQuestInfo confQuestInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSignalManualQuest GetFristSignalWithType(QuestType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetJumpCountForAcceptQuest(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetJumpCountForQuest(bool isPuzzleQuest, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetJumpCountForUnAcceptQuest(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetProcessingQuestCompCondString(LBProcessingQuestBase quest, List<string> criticalInfoStrList = null)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcDNId GetQuestAcceptNpcDNId(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetQuestAcceptSolarSystemId(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetQuestCompleteCondString(ConfigDataQuestInfo confQuestInfo, List<string> criticalInfoStrList = null, bool isAbbrFaction = false)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcDNId GetQuestCompleteNpcDNId(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetQuestCompleteSolarSystemId(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetQuestIconResPath(QuestType qstType, int anyFactionId = 11)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetQuestLevelByQuestConf(ConfigDataQuestInfo confQuestInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetQuestRandomRewardItemDropInfoId(ConfigDataQuestInfo confQuestInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetQuestRecommendShipIconResPath(ConfigDataQuestInfo confQuestInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetQuestRewardIconResPathWithInfo(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetQuestRewardInfoByQuestConf(ConfigDataQuestInfo confQuestInfo, out List<QuestRewardInfo> realRewardList, out List<QuestRewardInfo> configRewardList, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetQuestShowLevelByQuestConf(ConfigDataQuestInfo confQuestInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetRewardStringWithInfo(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetRewardValueWithInfo(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static KeyValuePair<bool, int> GetSubStoryQuestCount()
        {
        }

        [MethodImpl(0x8000)]
        public static Dictionary<VirtualBuffCategory, List<VirtualBuffDesc>> GetVirtualBuffSortList(List<VirtualBuffDesc> virtualBuffList)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsAutoMoveAfterQuestComeplete(ConfigDataQuestInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsEmergencySignalQuest(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsExistUnreceivedRewardInActivity()
        {
        }

        [MethodImpl(0x8000)]
        public static bool? IsPlayerLossWhenDeadInQuest(ConfigDataQuestInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        public static bool? IsPlayerLossWhenDeadInSignal(ConfigDataSignalInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        public static bool? IsPlayerLossWhenDeadInSpaceSignal(ConfigDataSpaceSignalInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        private static bool? IsPlayerShipDestroyInScene(int id)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsProcessingQuestTimeout(LBProcessingQuestBase quest, out string strLeftTime, out float leftPerc)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsQuestMustCompleteInTime(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsShowInQuestList(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSignalTimeout(LBSignalBase signal, out string strLeftTime, out float leftPerc)
        {
        }

        [MethodImpl(0x8000)]
        public static bool OnChapterQuestCutsceneStartNtf(int questId, bool blockNetTask = false)
        {
        }

        [MethodImpl(0x8000)]
        private static void PlayCharacterCutScene(int questId, ConfigDataCutInfo cutInfo, UIManager.UIActionQueueItem item)
        {
        }

        [MethodImpl(0x8000)]
        public static string QuestDescParamReplaceByTypeInfo(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int ShowStoryAndQuestConerTip(out bool showRedPint, out bool showRedCount)
        {
        }

        [MethodImpl(0x8000)]
        private static int VirtualBuffDescComparer(VirtualBuffDesc buffA, VirtualBuffDesc buffB)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnChapterQuestCutsceneStartNtf>c__AnonStorey0
        {
            internal bool blockNetTask;
            internal int questId;
            internal string lockName;
            internal ConfigDataCutInfo cutInfo;
            internal UIManager.UIActionQueueItem item;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1()
            {
            }

            private sealed class <OnChapterQuestCutsceneStartNtf>c__AnonStorey1
            {
                internal TipWindowUITask tipWindowUITask;
                internal QuestUIHelper.<OnChapterQuestCutsceneStartNtf>c__AnonStorey0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0()
                {
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PlayCharacterCutScene>c__AnonStorey2
        {
            internal int questId;
            internal UIManager.UIActionQueueItem item;
            internal ConfigDataCutInfo cutInfo;
            private static Action <>f__am$cache0;
            private static Action <>f__am$cache1;

            internal void <>m__0()
            {
                ChapterBeginUITask.StartChapterBeginUITask(this.questId, delegate {
                    this.item.OnEnd();
                    if (<>f__am$cache0 == null)
                    {
                        <>f__am$cache0 = () => SpaceStationUIHelper.EnableActiveBtnEffect(true);
                    }
                    ScreenFadeInOutUITask.StartFadeIn(true, <>f__am$cache0, true, false);
                    QuestUIHelper.PlayerCtx.SendChapterQuestCutsceneEndReq();
                });
            }

            internal void <>m__1()
            {
                CGPlayerUITask.StartPlayCG(this.cutInfo.ArtSourcePath, delegate {
                    this.item.OnEnd();
                    if (<>f__am$cache1 == null)
                    {
                        <>f__am$cache1 = () => SpaceStationUIHelper.EnableActiveBtnEffect(true);
                    }
                    ScreenFadeInOutUITask.StartFadeIn(true, <>f__am$cache1, true, false);
                    QuestUIHelper.PlayerCtx.SendChapterQuestCutsceneEndReq();
                }, (CGPlayerUITask.CGLogicType) this.cutInfo.CGChapterID);
            }

            internal void <>m__2()
            {
                this.item.OnEnd();
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = () => SpaceStationUIHelper.EnableActiveBtnEffect(true);
                }
                ScreenFadeInOutUITask.StartFadeIn(true, <>f__am$cache0, true, false);
                QuestUIHelper.PlayerCtx.SendChapterQuestCutsceneEndReq();
            }

            internal void <>m__3()
            {
                this.item.OnEnd();
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = () => SpaceStationUIHelper.EnableActiveBtnEffect(true);
                }
                ScreenFadeInOutUITask.StartFadeIn(true, <>f__am$cache1, true, false);
                QuestUIHelper.PlayerCtx.SendChapterQuestCutsceneEndReq();
            }

            private static void <>m__4()
            {
                SpaceStationUIHelper.EnableActiveBtnEffect(true);
            }

            private static void <>m__5()
            {
                SpaceStationUIHelper.EnableActiveBtnEffect(true);
            }
        }
    }
}

