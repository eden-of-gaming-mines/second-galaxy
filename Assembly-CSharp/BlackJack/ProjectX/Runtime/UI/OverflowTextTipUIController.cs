﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class OverflowTextTipUIController : UIControllerBase
    {
        private Camera m_canvasCamera;
        private float m_tipWindowDistanceToSrcText;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TipUIBGButton;
        [AutoBind("./TipWindowBG", AutoBindAttribute.InitState.NotInit, false)]
        public Image TipTextBG;
        [AutoBind("./TipWindowBG/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text TipText;
        private static DelegateBridge __Hotfix_ShowOverflowTextTipWindow;
        private static DelegateBridge __Hotfix_HideOverflowTextTipWindow;
        private static DelegateBridge __Hotfix_get_CanvasCamera;

        [MethodImpl(0x8000)]
        public void HideOverflowTextTipWindow()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOverflowTextTipWindow(Text srcText)
        {
        }

        private Camera CanvasCamera
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

