﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonBackAndCloseButtonUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BackAndCloseUIStateCtrl;
        protected string BackAndCloseUIState_PanelOpen;
        protected string BackAndCloseUIState_PanelClose;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CloseButton;
        [AutoBind("./CloseButton_Long", AutoBindAttribute.InitState.NotInit, false)]
        public Button CloseButtonLong;
        [AutoBind("./LongBarRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LongBarRoot;
        [AutoBind("./LongBarRoot/Mask/AnnouncementBarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LongAnnouncementText;
        [AutoBind("./ShortBarRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShortBarRoot;
        [AutoBind("./ShortBarRoot/Mask/AnnouncementBarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShortAnnouncementText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_StartShowBackAndCloseUIAnimation;
        private static DelegateBridge __Hotfix_StartHideBackAndCloseUIAnimation;
        private static DelegateBridge __Hotfix_SetPosition;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIMode(CommonBackAndCloseUIMode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void StartHideBackAndCloseUIAnimation(Action onHideBackAndCloseUIEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartShowBackAndCloseUIAnimation(Action onShowBackAndCloseUIEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <StartHideBackAndCloseUIAnimation>c__AnonStorey1
        {
            internal Action onHideBackAndCloseUIEnd;
            internal CommonBackAndCloseButtonUIController $this;

            internal void <>m__0(bool bo)
            {
                if (this.onHideBackAndCloseUIEnd != null)
                {
                    this.onHideBackAndCloseUIEnd();
                }
                this.$this.gameObject.SetActive(false);
            }
        }

        [CompilerGenerated]
        private sealed class <StartShowBackAndCloseUIAnimation>c__AnonStorey0
        {
            internal Action onShowBackAndCloseUIEnd;

            internal void <>m__0(bool bo)
            {
                if (this.onShowBackAndCloseUIEnd != null)
                {
                    this.onShowBackAndCloseUIEnd();
                }
            }
        }

        public enum CommonBackAndCloseUIMode
        {
            Long,
            Short
        }
    }
}

