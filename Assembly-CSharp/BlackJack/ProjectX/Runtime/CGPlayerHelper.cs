﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CGPlayerHelper
    {
        protected static IVideoPlayerCreator m_videoPlayerCreator;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegistIVideoPlayerCreator;
        private static DelegateBridge __Hotfix_CreateVideoPlayerFromGameObj;

        [MethodImpl(0x8000)]
        public static IVideoPlayer CreateVideoPlayerFromGameObj(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public static void RegistIVideoPlayerCreator(IVideoPlayerCreator creator)
        {
            DelegateBridge bridge = __Hotfix_RegistIVideoPlayerCreator;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(creator);
            }
            else
            {
                m_videoPlayerCreator = creator;
            }
        }
    }
}

