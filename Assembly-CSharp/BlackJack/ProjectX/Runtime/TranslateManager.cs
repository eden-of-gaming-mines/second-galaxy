﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime.UI;
    using BlackJack.Utils;
    using IL;
    using Newtonsoft.Json;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Text;
    using UnityEngine;

    public class TranslateManager
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStringTranslateComplete;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAudio2StringTranslateComplete;
        private readonly List<string> m_translateStringReqCacheList = new List<string>();
        private readonly Dictionary<string, TranslateResultItem> m_translateStringCachedDict = new Dictionary<string, TranslateResultItem>();
        private readonly List<TranslateAudio2StringReqCacheItem> m_audio2StringReqCacheList = new List<TranslateAudio2StringReqCacheItem>();
        private readonly Dictionary<string, TranslateResultItem> m_audio2StringCachedDict = new Dictionary<string, TranslateResultItem>();
        private WWW m_httpRequest;
        private readonly TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
        private const string TranslateStringUrlFormatStr = "{0}/LiveDataText?target={1}&sourceId=1&data={2}";
        private const string TranslateAudio2StringUrlFormatStr = "{0}/Audio/";
        private static TranslateManager m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Clear4Return2Login;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TranslateString;
        private static DelegateBridge __Hotfix_GetTranslateString;
        private static DelegateBridge __Hotfix_IsInTranslatingString;
        private static DelegateBridge __Hotfix_IsTranslateStringFailed;
        private static DelegateBridge __Hotfix_FormatToQuerryString;
        private static DelegateBridge __Hotfix_FormatToSourceString;
        private static DelegateBridge __Hotfix_ProcessStringTranslateRequest;
        private static DelegateBridge __Hotfix_GetStringTranslateUrl;
        private static DelegateBridge __Hotfix_GetCurrLanguageStandardSimpleName;
        private static DelegateBridge __Hotfix_GetStandardSimpleNameByLangType;
        private static DelegateBridge __Hotfix_TranslateAudio2String;
        private static DelegateBridge __Hotfix_GetAudio2StringResult;
        private static DelegateBridge __Hotfix_IsInAudio2String;
        private static DelegateBridge __Hotfix_IsAudio2StringFailed;
        private static DelegateBridge __Hotfix_ProcessAudio2StringTranslateRequest;
        private static DelegateBridge __Hotfix_OnAuto2StringTranslateComplete;
        private static DelegateBridge __Hotfix_ProcessAudio2StringTranslateRequestImp;
        private static DelegateBridge __Hotfix_GetAudio2StringTranslateUrl;
        private static DelegateBridge __Hotfix_GetTranslateServerUrl;
        private static DelegateBridge __Hotfix_add_EventOnStringTranslateComplete;
        private static DelegateBridge __Hotfix_remove_EventOnStringTranslateComplete;
        private static DelegateBridge __Hotfix_add_EventOnAudio2StringTranslateComplete;
        private static DelegateBridge __Hotfix_remove_EventOnAudio2StringTranslateComplete;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_set_Instance;

        public event Action EventOnAudio2StringTranslateComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStringTranslateComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public TranslateManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void Clear4Return2Login()
        {
        }

        [MethodImpl(0x8000)]
        private static string FormatToQuerryString(string srcStr)
        {
        }

        [MethodImpl(0x8000)]
        private static string FormatToSourceString(string querryStr)
        {
        }

        [MethodImpl(0x8000)]
        public string GetAudio2StringResult(string audioMd5)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetAudio2StringTranslateUrl()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCurrLanguageStandardSimpleName()
        {
        }

        [MethodImpl(0x8000)]
        private string GetStandardSimpleNameByLangType(LanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        private string GetStringTranslateUrl(string srcStr)
        {
        }

        [MethodImpl(0x8000)]
        private string GetTranslateServerUrl()
        {
        }

        [MethodImpl(0x8000)]
        public string GetTranslateString(string srcString)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAudio2StringFailed(string audioMd5)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInAudio2String(string audioMd5)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsInTranslatingString(string srcString)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsTranslateStringFailed(string srcString)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAuto2StringTranslateComplete(string md5, TranslateAudio2StringResult result)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator ProcessAudio2StringTranslateRequest(string audioMd5, byte[] audioData, int sample, LanguageType languageType)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator ProcessAudio2StringTranslateRequestImp(string audioMd5, byte[] audioData, int sample, LanguageType languageType, bool useData, Action<TranslateAudio2StringResult> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator ProcessStringTranslateRequest(string srcStr)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                try
                {
                    this.m_corutineHelper.Tick();
                    if (this.m_translateStringReqCacheList.Count > 0)
                    {
                        if (this.m_httpRequest == null)
                        {
                            string srcStr = this.m_translateStringReqCacheList[0];
                            this.m_translateStringReqCacheList.RemoveAt(0);
                            this.m_corutineHelper.StartCorutine(this.ProcessStringTranslateRequest(srcStr));
                        }
                    }
                    else if ((this.m_audio2StringReqCacheList.Count > 0) && (this.m_httpRequest == null))
                    {
                        TranslateAudio2StringReqCacheItem item = this.m_audio2StringReqCacheList[0];
                        this.m_audio2StringReqCacheList.RemoveAt(0);
                        this.m_corutineHelper.StartCorutine(this.ProcessAudio2StringTranslateRequest(item.m_audioMd5, item.m_audioData, item.m_sample, item.m_languageType));
                    }
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"TranslateManager.Tick"} : {exception}");
                }
            }
        }

        [MethodImpl(0x8000)]
        public void TranslateAudio2String(string audioMd5, byte[] audioData, int sample, LanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        public void TranslateString(string srcStr)
        {
        }

        public static TranslateManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp4182();
                }
                if (m_instance == null)
                {
                    m_instance = new TranslateManager();
                }
                return m_instance;
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <IsInAudio2String>c__AnonStorey4
        {
            internal string audioMd5;

            internal bool <>m__0(TranslateManager.TranslateAudio2StringReqCacheItem req) => 
                (req.m_audioMd5 == this.audioMd5);
        }

        [CompilerGenerated]
        private sealed class <ProcessAudio2StringTranslateRequest>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal TranslateManager.TranslateResultItem <item>__0;
            internal string audioMd5;
            internal byte[] audioData;
            internal int sample;
            internal LanguageType languageType;
            internal TranslateManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <ProcessAudio2StringTranslateRequest>c__AnonStorey5 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    {
                        this.$locvar0 = new <ProcessAudio2StringTranslateRequest>c__AnonStorey5();
                        this.$locvar0.<>f__ref$1 = this;
                        this.$locvar0.audioMd5 = this.audioMd5;
                        TranslateManager.TranslateResultItem item = new TranslateManager.TranslateResultItem {
                            m_state = TranslateManager.TranslateState.Processing,
                            m_resultStr = string.Empty
                        };
                        this.<item>__0 = item;
                        this.$this.m_audio2StringCachedDict[this.$locvar0.audioMd5] = this.<item>__0;
                        this.$locvar0.lret = TranslateManager.TranslateAudio2StringResult.None;
                        this.$current = this.$this.ProcessAudio2StringTranslateRequestImp(this.$locvar0.audioMd5, this.audioData, this.sample, this.languageType, false, new Action<TranslateManager.TranslateAudio2StringResult>(this.$locvar0.<>m__0));
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        goto TR_0001;
                    }
                    case 1:
                        if (this.$locvar0.lret != TranslateManager.TranslateAudio2StringResult.NeedAudio)
                        {
                            this.$this.OnAuto2StringTranslateComplete(this.$locvar0.audioMd5, this.$locvar0.lret);
                        }
                        else
                        {
                            this.$current = this.$this.ProcessAudio2StringTranslateRequestImp(this.$locvar0.audioMd5, this.audioData, this.sample, this.languageType, true, new Action<TranslateManager.TranslateAudio2StringResult>(this.$locvar0.<>m__1));
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                            goto TR_0001;
                        }
                        break;

                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            TR_0001:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <ProcessAudio2StringTranslateRequest>c__AnonStorey5
            {
                internal TranslateManager.TranslateAudio2StringResult lret;
                internal string audioMd5;
                internal TranslateManager.<ProcessAudio2StringTranslateRequest>c__Iterator1 <>f__ref$1;

                internal void <>m__0(TranslateManager.TranslateAudio2StringResult ret)
                {
                    this.lret = ret;
                }

                internal void <>m__1(TranslateManager.TranslateAudio2StringResult ret)
                {
                    this.lret = ret;
                    this.<>f__ref$1.$this.OnAuto2StringTranslateComplete(this.audioMd5, this.lret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ProcessAudio2StringTranslateRequestImp>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal bool useData;
            internal string <url>__0;
            internal Dictionary<string, object> <contentDict>__0;
            internal LanguageType languageType;
            internal int sample;
            internal string audioMd5;
            internal byte[] audioData;
            internal string <data>__0;
            internal Action<TranslateManager.TranslateAudio2StringResult> onEnd;
            internal string <msg>__0;
            internal JsonData <json>__0;
            internal string <status>__0;
            internal TranslateManager.TranslateAudio2StringResult <ret>__0;
            internal TranslateManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Debug.Log("ProcessAudio2StringTranslateRequestImp start useData:" + this.useData);
                        this.<url>__0 = this.$this.GetAudio2StringTranslateUrl();
                        this.<contentDict>__0 = new Dictionary<string, object>();
                        this.<contentDict>__0.Add("target", this.$this.GetStandardSimpleNameByLangType(this.languageType));
                        this.<contentDict>__0.Add("samples", this.sample);
                        this.<contentDict>__0.Add("md5", this.audioMd5);
                        if (this.useData)
                        {
                            this.<contentDict>__0.Add("data", Convert.ToBase64String(this.audioData));
                        }
                        else
                        {
                            this.<contentDict>__0.Add("data", string.Empty);
                        }
                        this.<data>__0 = JsonConvert.SerializeObject(this.<contentDict>__0);
                        this.$this.m_httpRequest = new WWW(this.<url>__0, Encoding.UTF8.GetBytes(this.<data>__0));
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (!this.$this.m_httpRequest.isDone)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (!string.IsNullOrEmpty(this.$this.m_httpRequest.error))
                {
                    Debug.LogError($"TranslateManager error: ProcessAudio2StringTranslateRequestImp url:{this.<url>__0}, errorStr:{this.$this.m_httpRequest.error}");
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_Audio2StringFailed, false, new object[0]);
                    this.$this.m_audio2StringCachedDict[this.audioMd5].m_state = TranslateManager.TranslateState.Failed;
                    if (this.onEnd != null)
                    {
                        this.onEnd(TranslateManager.TranslateAudio2StringResult.Error);
                    }
                    this.$this.m_httpRequest = null;
                }
                else
                {
                    this.<msg>__0 = this.$this.m_httpRequest.text;
                    this.<json>__0 = JsonMapper.ToObject(this.<msg>__0);
                    this.<status>__0 = (string) this.<json>__0["status"];
                    Debug.Log("ProcessAudio2StringTranslateRequestImp done ");
                    this.<ret>__0 = TranslateManager.TranslateAudio2StringResult.None;
                    if (this.<status>__0 == "ok")
                    {
                        this.<ret>__0 = TranslateManager.TranslateAudio2StringResult.Success;
                        string str = (string) this.<json>__0["translatedText"];
                        this.$this.m_audio2StringCachedDict[this.audioMd5].m_state = TranslateManager.TranslateState.Success;
                        this.$this.m_audio2StringCachedDict[this.audioMd5].m_resultStr = str;
                        Debug.Log("ProcessAudio2StringTranslateRequestImp success ");
                    }
                    else if (this.<status>__0 == "fail")
                    {
                        string str2 = (string) this.<json>__0["error"];
                        if (str2 == "need audio")
                        {
                            this.<ret>__0 = TranslateManager.TranslateAudio2StringResult.NeedAudio;
                        }
                        else
                        {
                            TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_Audio2StringFailed, false, new object[0]);
                            this.$this.m_audio2StringCachedDict[this.audioMd5].m_state = TranslateManager.TranslateState.Failed;
                            this.<ret>__0 = TranslateManager.TranslateAudio2StringResult.Error;
                        }
                        Debug.Log("ProcessAudio2StringTranslateRequestImp fail error:" + str2);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(this.<ret>__0);
                    }
                    this.$this.m_httpRequest = null;
                    this.$PC = -1;
                }
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <ProcessStringTranslateRequest>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal TranslateManager.TranslateResultItem <item>__0;
            internal string srcStr;
            internal string <url>__0;
            internal string <msg>__0;
            internal JsonData <json>__0;
            internal string <status>__0;
            internal TranslateManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    {
                        TranslateManager.TranslateResultItem item = new TranslateManager.TranslateResultItem {
                            m_state = TranslateManager.TranslateState.Processing,
                            m_resultStr = string.Empty
                        };
                        this.<item>__0 = item;
                        this.$this.m_translateStringCachedDict[this.srcStr] = this.<item>__0;
                        this.<url>__0 = this.$this.GetStringTranslateUrl(TranslateManager.FormatToQuerryString(this.srcStr));
                        this.$this.m_httpRequest = new WWW(this.<url>__0);
                        break;
                    }
                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (!this.$this.m_httpRequest.isDone)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (!string.IsNullOrEmpty(this.$this.m_httpRequest.error))
                {
                    Debug.LogError($"TranslateManager error: ProcessStringTranslateRequest url:{this.<url>__0}, errorStr:{this.$this.m_httpRequest.error}");
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_TranslateStringFailed, false, new object[0]);
                    this.<item>__0.m_state = TranslateManager.TranslateState.Failed;
                    if (this.$this.EventOnStringTranslateComplete != null)
                    {
                        this.$this.EventOnStringTranslateComplete();
                    }
                    this.$this.m_httpRequest = null;
                }
                else
                {
                    this.<msg>__0 = this.$this.m_httpRequest.text;
                    this.<json>__0 = JsonMapper.ToObject(this.<msg>__0);
                    this.<status>__0 = (string) this.<json>__0["status"];
                    if (this.<status>__0 != "ok")
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_TranslateStringFailed, false, new object[0]);
                        this.<item>__0.m_state = TranslateManager.TranslateState.Failed;
                    }
                    else
                    {
                        string str = TranslateManager.FormatToSourceString(StringFormatUtil.HtmlDecode((string) this.<json>__0["translatedText"]));
                        this.<item>__0.m_state = TranslateManager.TranslateState.Success;
                        this.<item>__0.m_resultStr = str;
                    }
                    if (this.$this.EventOnStringTranslateComplete != null)
                    {
                        this.$this.EventOnStringTranslateComplete();
                    }
                    this.$this.m_httpRequest = null;
                    this.$PC = -1;
                }
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <TranslateAudio2String>c__AnonStorey3
        {
            internal string audioMd5;

            internal bool <>m__0(TranslateManager.TranslateAudio2StringReqCacheItem item) => 
                (item.m_audioMd5 == this.audioMd5);
        }

        public class TranslateAudio2StringReqCacheItem
        {
            public string m_audioMd5;
            public byte[] m_audioData;
            public int m_sample;
            public LanguageType m_languageType;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public enum TranslateAudio2StringResult
        {
            None,
            Error,
            NeedAudio,
            Success
        }

        public class TranslateResultItem
        {
            public TranslateManager.TranslateState m_state;
            public string m_resultStr;
            private static DelegateBridge _c__Hotfix_ctor;

            public TranslateResultItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum TranslateState
        {
            Processing,
            Failed,
            Success
        }
    }
}

