﻿namespace BlackJack.ProjectX.Runtime
{
    using System;

    public interface IAudioCompressor
    {
        byte[] Compress(float[] audioData);
        float[] Decompress(byte[] compressData, int audioDatalength);
    }
}

