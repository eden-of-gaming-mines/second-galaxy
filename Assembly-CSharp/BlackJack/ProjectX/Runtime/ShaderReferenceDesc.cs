﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShaderReferenceDesc : MonoBehaviour
    {
        public List<Shader> m_shaderReferenceList;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_CreateFakeMaterials;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator CreateFakeMaterials(Action<bool> onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <CreateFakeMaterials>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <existShaderCount>__0;
            internal List<Shader>.Enumerator $locvar0;
            internal Action<bool> onEnd;
            internal Material[] <mats>__0;
            internal int <i>__0;
            internal List<Shader>.Enumerator $locvar1;
            internal Shader <shader>__1;
            internal MeshRenderer <render>__0;
            internal ShaderReferenceDesc $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

