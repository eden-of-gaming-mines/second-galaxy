﻿namespace BlackJack.ProjectX.Runtime
{
    using System;

    public enum PurchaseRechargeGoodsResult
    {
        PaymentNotReady = 1,
        CantGetRechargeGoodsInfo = 2,
        CantPurchase = 3,
        Success = 4,
        Fail = 5,
        Cancel = 6
    }
}

