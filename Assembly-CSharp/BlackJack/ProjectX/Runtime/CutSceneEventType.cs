﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;

    public class CutSceneEventType
    {
        public const string NpcChatEvent = "NPCChat";
        public const string DestroyShipEvent = "DestroyShip";
        public const string PlayAudioEvent = "Audio";
        public const string CutSceneStopEvent = "OnCutsceneStopped";
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

