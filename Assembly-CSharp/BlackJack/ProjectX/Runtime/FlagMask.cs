﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class FlagMask
    {
        private const int MaskLength = 0x40;
        private const ulong MaskNone = 0UL;
        private ulong m_mask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsHave;
        private static DelegateBridge __Hotfix_AddMask;
        private static DelegateBridge __Hotfix_RemoveMask;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_IsNone;
        private static DelegateBridge __Hotfix_Bitwise;

        [MethodImpl(0x8000)]
        public void AddMask(int index)
        {
        }

        [MethodImpl(0x8000)]
        private static ulong Bitwise(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHave(int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNone()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveMask(int index)
        {
        }
    }
}

