﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class ListUtility
    {
        private static DelegateBridge __Hotfix_IsAny;
        private static DelegateBridge __Hotfix_IsAll;
        private static DelegateBridge __Hotfix_Sort;
        private static DelegateBridge __Hotfix_InsertionSort;
        private static DelegateBridge __Hotfix_FindOne;
        private static DelegateBridge __Hotfix_IndexOf;

        [MethodImpl(0x8000)]
        public static T FindOne<T>(IList<T> list, Func<T, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public static int IndexOf<T>(IList<T> list, Func<T, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public static void InsertionSort<T>(IList<T> list, Func<T, T, int> filter)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsAll<T>(IList<T> list, Func<T, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsAny<T>(IList<T> list, Func<T, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public static void Sort<T>(List<T> list, Func<T, T, int> filter)
        {
        }

        [CompilerGenerated]
        private sealed class <Sort>c__AnonStorey0<T>
        {
            internal Func<T, T, int> filter;

            internal int <>m__0(T item1, T item2) => 
                this.filter(item1, item2);
        }
    }
}

