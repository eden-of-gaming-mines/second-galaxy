﻿namespace BlackJack.ProjectX.Runtime.SDK
{
    using IL;
    using System;

    public class GameEventID4PD
    {
        public const string LaunchGame = "1";
        public const string ShowSDKLoginView = "4";
        public const string ThirdPartyLogin = "5";
        public const string ThirdPartyLoginOk = "6";
        public const string SDKCallBackToken = "7";
        public const string LoginPdSucceed = "20";
        public const string DisplayAnnouncement = "21";
        public const string OnClickLoginButton = "25";
        public const string SkipStopCg = "26";
        public const string EnterCreatPlayerUI = "27";
        public const string OnClickBackground = "28";
        public const string OnClickHeadPortrait = "29";
        public const string OnClickRefreshPlayerName = "30";
        public const string OnClickEnterWord = "31";
        public const string OnCreateCharacter = "32";
        public const string OnClickAffirmAndEnterGame = "34";
        public const string OnClickAfreshSelect = "35";
        public const string OnClickSkipFirstCg = "36";
        public const string OnNewPlayerAnimStart = "37";
        public const string EnterFirstScene = "41";
        public const int CompleteUserGuideStep = 0x186a0;
        public const int StartUserGuideGroup = 0x2af8;
        public const int CompleteUserGuideGroup = 0x2ee0;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

