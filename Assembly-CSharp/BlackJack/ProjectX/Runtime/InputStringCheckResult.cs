﻿namespace BlackJack.ProjectX.Runtime
{
    using System;

    public enum InputStringCheckResult
    {
        Ok,
        Empty,
        LengthError,
        CharacterIllegal,
        SensitiveWord,
        InValidRegex,
        CustomFunctionCheckFail
    }
}

