﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ProjectX.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildViewHelper
    {
        private const int MaxScaleLevel = 3;
        private static readonly Dictionary<string, string> ServerIdStrDic;
        private static Color[] m_colors;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static int <XBlockCount>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static int <YBlockCount>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static int <Width>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static int <Height>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Vector2 <LeftBottom>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static int <ScaleLevel>k__BackingField;
        private const string StrMapBlockVersion = "MapBlockVersion_{0}_{1}_{2:00}_{3:00}";
        private static readonly string m_dataPath;
        private const int EffectWidth = 0x20;
        private const int SqrEffectWidth = 0x400;
        private const byte LeastPower = 100;
        private const float CalculatePower = 3f;
        public static bool m_imagesAllCalculated;
        public const int BlockWidth = 0x80;
        public const int BlockHeight = 0x80;
        public static ulong m_gotStarfieldMask;
        public static readonly Dictionary<int, ushort> m_versionDict;
        public const ushort MaxAllianceCount = 0x800;
        public const ushort MaxGuildCount = 0x2000;
        public const int SlnVersion = 2;
        private const string StrSlnVersion = "SLN_VERSION";
        [CompilerGenerated]
        private static AsyncCallback <>f__am$cache0;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ForcePosition>, ushort> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ForcePosition>, ushort> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ForcePosition>, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ForcePosition>, ushort> <>f__am$cache4;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetScaleLevel;
        private static DelegateBridge __Hotfix_TryGetUncalculatedBlock;
        private static DelegateBridge __Hotfix_VersionChanged;
        private static DelegateBridge __Hotfix_SaveVersion;
        private static DelegateBridge __Hotfix_CleanVersionData;
        private static DelegateBridge __Hotfix_TryGetImageData;
        private static DelegateBridge __Hotfix_SaveImageData;
        private static DelegateBridge __Hotfix_MapDataPath;
        private static DelegateBridge __Hotfix_SetMapBasicInfo;
        private static DelegateBridge __Hotfix_SetColorMap;
        private static DelegateBridge __Hotfix_RefreshStarFieldData;
        private static DelegateBridge __Hotfix_GetColorBySolarSystemId;
        private static DelegateBridge __Hotfix_SetMapScale;
        private static DelegateBridge __Hotfix_SetLocalTransform;
        private static DelegateBridge __Hotfix_BlockCenterPosition;
        private static DelegateBridge __Hotfix_NeedStarFieldIdMask;
        private static DelegateBridge __Hotfix_RequireDataRect;
        private static DelegateBridge __Hotfix_StarFieldIdsInRect;
        private static DelegateBridge __Hotfix_Calculate;
        private static DelegateBridge __Hotfix_ColorFromIndex;
        private static DelegateBridge __Hotfix_Merge;
        private static DelegateBridge __Hotfix_Grad;
        private static DelegateBridge __Hotfix_CalculateAreas;
        private static DelegateBridge __Hotfix_FindNear;
        private static DelegateBridge __Hotfix_SetOccupyInfo;
        private static DelegateBridge __Hotfix_SetPictureGenCompleted;
        private static DelegateBridge __Hotfix_InitData;
        private static DelegateBridge __Hotfix_GetMapBlockVersion;
        private static DelegateBridge __Hotfix_RestoreStarfieldReqMask;
        private static DelegateBridge __Hotfix_SetStarfieldReqMask;
        private static DelegateBridge __Hotfix_CreateDataPathIfNotExists;
        private static DelegateBridge __Hotfix_ServerDataPath;
        private static DelegateBridge __Hotfix_GetServerIdStr;
        private static DelegateBridge __Hotfix_get_XBlockCount;
        private static DelegateBridge __Hotfix_set_XBlockCount;
        private static DelegateBridge __Hotfix_get_YBlockCount;
        private static DelegateBridge __Hotfix_set_YBlockCount;
        private static DelegateBridge __Hotfix_get_Width;
        private static DelegateBridge __Hotfix_set_Width;
        private static DelegateBridge __Hotfix_get_Height;
        private static DelegateBridge __Hotfix_set_Height;
        private static DelegateBridge __Hotfix_get_LeftBottom;
        private static DelegateBridge __Hotfix_set_LeftBottom;
        private static DelegateBridge __Hotfix_get_ScaleLevel;
        private static DelegateBridge __Hotfix_set_ScaleLevel;
        private static DelegateBridge __Hotfix_get_TextureFormat;

        [MethodImpl(0x8000)]
        public static Vector2 BlockCenterPosition(int xIndex, int yIndex)
        {
        }

        [MethodImpl(0x8000)]
        public static void Calculate(ColorByteArrayContext context)
        {
        }

        [MethodImpl(0x8000)]
        public static List<StarArea> CalculateAreas(List<int> solarSystemIds)
        {
        }

        [MethodImpl(0x8000)]
        public static void CleanVersionData(int scaleLevel, int x, int y)
        {
        }

        [MethodImpl(0x8000)]
        public static Color ColorFromIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private static bool CreateDataPathIfNotExists()
        {
        }

        [MethodImpl(0x8000)]
        private static void FindNear(int index, List<int> fromList, List<Vector2> toList, float sqrMaxDist)
        {
        }

        [MethodImpl(0x8000)]
        public static Color GetColorBySolarSystemId(int solarSystemId, MapType mapType)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetMapBlockVersion(int scaleLevel, int x, int y)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetScaleLevel(float scale)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetServerIdStr()
        {
        }

        [MethodImpl(0x8000)]
        private static byte Grad(float distance, float reference)
        {
        }

        [MethodImpl(0x8000)]
        public static void InitData()
        {
        }

        [MethodImpl(0x8000)]
        private static string MapDataPath(int scaleLevel, int xIndex, int yIndex)
        {
        }

        [MethodImpl(0x8000)]
        private static byte Merge(byte b1, byte b2)
        {
        }

        [MethodImpl(0x8000)]
        public static ulong NeedStarFieldIdMask(int x, int y, int scaleLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static void RefreshStarFieldData(Dictionary<int, GuildAllianceId> solarSystemGuildMappingDict)
        {
        }

        [MethodImpl(0x8000)]
        public static Rect RequireDataRect(int x, int y, int scaleLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static void RestoreStarfieldReqMask()
        {
        }

        [MethodImpl(0x8000)]
        public static void SaveImageData(ColorByteArrayContext context, MapType mapType)
        {
        }

        [MethodImpl(0x8000)]
        public static void SaveVersion(int version, int scaleLevel, int x, int y)
        {
        }

        [MethodImpl(0x8000)]
        private static string ServerDataPath()
        {
        }

        [MethodImpl(0x8000)]
        public static void SetColorMap(Texture2D tex)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetLocalTransform(Transform transform, int xIndex, int yIndex)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetMapBasicInfo(Rect rect)
        {
        }

        [MethodImpl(0x8000)]
        public static bool SetMapScale(float scale)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetOccupyInfo(Dictionary<int, GuildAllianceId> solarSystemGuildMappingDict)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetPictureGenCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public static void SetStarfieldReqMask(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public static ulong StarFieldIdsInRect(int x, int y, int scaleLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetImageData(int scaleLevel, int xIndex, int yIndex, ref byte[] data, Action<byte[]> callBack = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetUncalculatedBlock(ref int scaleLevel, ref int x, ref int y, ref int version)
        {
        }

        [MethodImpl(0x8000)]
        public static bool VersionChanged(int version, int scaleLevel, int x, int y)
        {
        }

        public static int XBlockCount
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static int YBlockCount
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static int Width
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static int Height
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static Vector2 LeftBottom
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static int ScaleLevel
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public static UnityEngine.TextureFormat TextureFormat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <Calculate>c__AnonStorey1
        {
            internal Rect rect;

            internal bool <>m__0(KeyValuePair<int, ForcePosition> fp) => 
                (this.rect.Contains(fp.Value.m_pos) && ((fp.Value.m_guildForce != 0) || (fp.Value.m_allianceForce != 0)));
        }

        [CompilerGenerated]
        private sealed class <FindNear>c__AnonStorey2
        {
            internal int index;
            internal float sqrMaxDist;

            internal bool <>m__0(int i) => 
                (Vector2.SqrMagnitude(StarMapCommonData.Forces[i].m_pos - StarMapCommonData.Forces[this.index].m_pos) <= this.sqrMaxDist);
        }

        [CompilerGenerated]
        private sealed class <TryGetImageData>c__AnonStorey0
        {
            internal Action<byte[]> callBack;

            internal void <>m__0(IAsyncResult async)
            {
                this.callBack((byte[]) async.AsyncState);
            }
        }

        public enum MapType
        {
            Guild,
            Alliance
        }

        public class StarArea
        {
            public Vector2 m_centerPosition;
            public int m_starCount;
            private static DelegateBridge _c__Hotfix_ctor;

            public StarArea()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

