﻿namespace BlackJack.ProjectX.ScenePreview
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ScenePreview_Util
    {
        protected const float m_celestialResStantardRadius = 1E+07f;
        public const double m_spaceGideSize = 1000000.0;
        public const float m_meterPerUnityUnitInShipLayer = 100f;
        public const float m_meterPerUnityUnitInCelestialLayer = 1000000f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Transform2ShipLayerLocation;
        private static DelegateBridge __Hotfix_TransformShipLayerLocation2SolarSystemLocation;
        private static DelegateBridge __Hotfix_Transform2CelestialLayerLocation;
        private static DelegateBridge __Hotfix_TransformCelestialLayerLocation2SolarSystemLocation;
        private static DelegateBridge __Hotfix_GetCelestialScaleVecByRadiusValue;
        private static DelegateBridge __Hotfix_GetCelestialPlanetLikeBuildingScaleVecByRadiusValue;

        [MethodImpl(0x8000)]
        public static Vector3 GetCelestialPlanetLikeBuildingScaleVecByRadiusValue(float celestialRadius)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 GetCelestialScaleVecByRadiusValue(float celestialRadius)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 Transform2CelestialLayerLocation(Vector3D vector3D, Vector3D currCenterGridLocation)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 Transform2ShipLayerLocation(Vector3D vector3D, Vector3D currCenterGridLocation)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D TransformCelestialLayerLocation2SolarSystemLocation(Vector3 pos, Vector3D currCenterGridLocation)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D TransformShipLayerLocation2SolarSystemLocation(Vector3 pos, Vector3D currCenterGridLocation)
        {
        }
    }
}

