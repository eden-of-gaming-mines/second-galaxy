﻿namespace BlackJack.ProjectX.ScenePreview
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ScenePreview_CalcAttackLocation : MonoBehaviour
    {
        public ScenePreview_CelestialInfoDesc m_cityDesc;
        public Transform m_onHitPoint;
        public LineRenderer m_onHitLine;
        public ScenePreview_SceneInfoDesc m_sceneDesc;
        public Transform m_firePoint;
        public LineRenderer m_fireLine;
        public float m_lineWidth;
        public double m_lineLength;
        public double m_fireLineLength;
        public double m_onHitLineLength;
        protected ScenePreview_SceneManager m_previewManager;
        private static DelegateBridge __Hotfix_CalcAttackLocation;
        private static DelegateBridge __Hotfix_FillLine;

        [MethodImpl(0x8000)]
        public void CalcAttackLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void FillLine(LineRenderer line, Vector3 startPoint, Vector3 endPoint, int segmentCount)
        {
        }
    }
}

