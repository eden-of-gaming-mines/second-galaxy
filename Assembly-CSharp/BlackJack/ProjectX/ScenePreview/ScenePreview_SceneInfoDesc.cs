﻿namespace BlackJack.ProjectX.ScenePreview
{
    using Dest.Math;
    using System;
    using UnityEngine;

    public class ScenePreview_SceneInfoDesc : MonoBehaviour
    {
        [Header("场景GDB坐标")]
        public Vector3D Location;
        [Header("场景的GDB旋转(角度)")]
        public float RotateY;
    }
}

