﻿namespace BlackJack.ProjectX.ScenePreview
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ScenePreview_CelestialAutoGenSceneDesc : ScenePreview_CelestialInfoDesc
    {
        protected ScenePreview_SceneInfoDesc m_shipLayerSceneInfo;
        private static DelegateBridge __Hotfix_SetShipLayerSceneInfo;
        private static DelegateBridge __Hotfix_SyncLocationToShipLayer;
        private static DelegateBridge __Hotfix_SyncLocationFromShipLayer;

        [MethodImpl(0x8000)]
        public void SetShipLayerSceneInfo(ScenePreview_SceneInfoDesc info)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncLocationFromShipLayer()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncLocationToShipLayer()
        {
        }
    }
}

