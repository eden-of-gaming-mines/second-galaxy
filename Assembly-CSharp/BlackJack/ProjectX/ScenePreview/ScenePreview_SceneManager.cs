﻿namespace BlackJack.ProjectX.ScenePreview
{
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ScenePreview_SceneManager : MonoBehaviour
    {
        protected Vector3D m_shipCameraLocation;
        public List<ScenePreview_CelestialInfoDesc> m_celestialList;
        public List<ScenePreview_CelestialAutoGenSceneDesc> m_autoGenCelestialSceneList;
        public List<ScenePreview_SceneInfoDesc> m_sceneList;
        protected SpaceGrideIndex m_currCenterGridIndex;
        public Vector3D m_currCenterGridLocation;
        [Header("飞船层摄像机")]
        public Camera m_shipCamera;
        [Header("天体层摄像机")]
        public Camera m_celestialCamera;
        [Header("参考飞船")]
        public ScenePreview_SceneInfoDesc m_basePoint;
        [Header("天体层自动创建sceneRoot")]
        public Transform m_celestialLayerAutoCreateSceneList;
        [CompilerGenerated]
        private static Action<ScenePreview_CelestialAutoGenSceneDesc> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnGenSolarSystemButtonClick;
        private static DelegateBridge __Hotfix_OnSyncCameraButtonClick;
        private static DelegateBridge __Hotfix_OnCalcLocationFromCelesialLayerButtonClick;
        private static DelegateBridge __Hotfix_OnCalcLocationFromShipLayerButtonClick;
        private static DelegateBridge __Hotfix_CalcGrid;
        private static DelegateBridge __Hotfix_CollectCelestialList;
        private static DelegateBridge __Hotfix_CollectSceneList;
        private static DelegateBridge __Hotfix_CalcCelestialLocation;
        private static DelegateBridge __Hotfix_CalcAutoGenCelestialLocation;
        private static DelegateBridge __Hotfix_CalcCelestialGDBLocation;
        private static DelegateBridge __Hotfix_CalcAutoGenCelestialGDBLocation;
        private static DelegateBridge __Hotfix_CalcSceneLocation;
        private static DelegateBridge __Hotfix_CalcSceneGBDLocation;
        private static DelegateBridge __Hotfix_CalcShipLayerCameraLocation;
        private static DelegateBridge __Hotfix_SyncSceneCameraToShipCamera;
        private static DelegateBridge __Hotfix_SyncShipCameraToCelestialCamera;
        private static DelegateBridge __Hotfix_IsInGride;
        private static DelegateBridge __Hotfix_CalcLightDirection;

        [MethodImpl(0x8000)]
        protected void CalcAutoGenCelestialGDBLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAutoGenCelestialLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcCelestialGDBLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcCelestialLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcGrid(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcLightDirection()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcSceneGBDLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcSceneLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcShipLayerCameraLocation(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectCelestialList()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectSceneList()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsInGride(SpaceGrideIndex gridIndex, float gridSize, float bufferSize, Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCalcLocationFromCelesialLayerButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCalcLocationFromShipLayerButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGenSolarSystemButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncCameraButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void SyncSceneCameraToShipCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected void SyncShipCameraToCelestialCamera()
        {
        }
    }
}

