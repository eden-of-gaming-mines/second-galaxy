﻿namespace BlackJack.ProjectX.ScenePreview
{
    using Dest.Math;
    using System;
    using UnityEngine;

    public class ScenePreview_CelestialInfoDesc : MonoBehaviour
    {
        [Header("星球GDB坐标")]
        public Vector3D Location;
        [Header("星球的GDB旋转(角度)")]
        public float RotateY;
        [Header("星球半径(米)")]
        public float Radius;
        [Header("是否是建筑物")]
        public bool IsBuilding;
    }
}

