﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ProjectXServerSettingManager : ServerSettingManager
    {
        protected bool m_isAllUserAllowedUseGm;
        protected HashSet<string> m_gmUserNameSet = new HashSet<string>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetServerSettingExtraData;
        private static DelegateBridge __Hotfix_SetGmUserList;
        private static DelegateBridge __Hotfix_IsUserHasGmPermission;
        private static DelegateBridge __Hotfix_get_TranslateServerUrlKey;
        private static DelegateBridge __Hotfix_get_ClientUploadLogUrlkey;

        [MethodImpl(0x8000)]
        public ProjectXServerSettingManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public bool IsUserHasGmPermission(string userName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGmUserList(string gmUserList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetServerSettingExtraData(Dictionary<string, string> contentDict)
        {
            DelegateBridge bridge = __Hotfix_SetServerSettingExtraData;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, contentDict);
            }
            else
            {
                base.SetServerSettingInfoValue(this.TranslateServerUrlKey, contentDict);
                base.SetServerSettingInfoValue(this.ClientUploadLogUrlkey, contentDict);
            }
        }

        public string TranslateServerUrlKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_TranslateServerUrlKey;
                return ((bridge == null) ? "TranslateServerUrl" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public string ClientUploadLogUrlkey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_ClientUploadLogUrlkey;
                return ((bridge == null) ? "ClientUploadLogUrl" : bridge.__Gen_Delegate_Imp33(this));
            }
        }
    }
}

