﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime.SDK;
    using BlackJack.ProjectX.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ProjectXGameLauncher : MonoBehaviour
    {
        private ProjectXGameManager m_gameManager;
        private System.Type m_gmCommandtype;
        private object m_gmCommandInstance;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_StartSdkInit;
        private static DelegateBridge __Hotfix_DisableBlackCoverCamera;
        private static DelegateBridge __Hotfix_StartAndroidObbProcessing;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_LateUpdate;
        private static DelegateBridge __Hotfix_OnApplicationQuit;
        private static DelegateBridge __Hotfix_CmdProcess;
        private static DelegateBridge __Hotfix_OnNativeMessage;
        private static DelegateBridge __Hotfix_OnApplicationPause;
        private static DelegateBridge __Hotfix_OnAndroidBackKeyUp;

        [MethodImpl(0x8000)]
        public void CmdProcess(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void DisableBlackCoverCamera()
        {
            DelegateBridge bridge = __Hotfix_DisableBlackCoverCamera;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Camera component = base.GetComponent<Camera>();
                if (component != null)
                {
                    component.enabled = false;
                }
            }
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
            DelegateBridge bridge = __Hotfix_LateUpdate;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (CommonUIGlobalState.m_hasFinishAndroidBackEvent)
            {
                CommonUIGlobalState.m_hasFinishAndroidBackEvent = false;
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                this.OnAndroidBackKeyUp();
            }
        }

        [MethodImpl(0x8000)]
        private void OnAndroidBackKeyUp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationPause(bool isPause)
        {
            DelegateBridge bridge = __Hotfix_OnApplicationPause;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isPause);
            }
            else
            {
                Debug.Log($"ProjectXGameLauncher.OnApplicationPause {isPause}");
                if (this.m_gameManager == null)
                {
                    Debug.Log("ProjectXGameLauncher.OnApplicationPause m_gameManager is NULL");
                }
                else
                {
                    ProjectXPlayerContext playerContext = this.m_gameManager.PlayerContext as ProjectXPlayerContext;
                    if (isPause)
                    {
                        if (playerContext != null)
                        {
                            playerContext.SendClientAppPauseNtf();
                        }
                    }
                    else if (playerContext != null)
                    {
                        playerContext.SendClientAppResumeNtf();
                    }
                    if (playerContext != null)
                    {
                        playerContext.OnApplicationPause(isPause);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        private void OnApplicationQuit()
        {
        }

        [MethodImpl(0x8000)]
        public void OnNativeMessage(string msg)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator Start()
        {
            DelegateBridge bridge = __Hotfix_Start;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <Start>c__Iterator0 { $this = this };
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator StartAndroidObbProcessing(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartSdkInit()
        {
            DelegateBridge bridge = __Hotfix_StartSdkInit;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (!PDSDK.Instance.isInit)
            {
                PDSDK.Instance.Init();
            }
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
            DelegateBridge bridge = __Hotfix_Update;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_gameManager != null)
            {
                try
                {
                    this.m_gameManager.Tick();
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"ProjectXGameLauncher.Update"} : {exception}");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal ProjectXGameEntryUITask <entryTask>__0;
            internal ProjectXGameLauncher $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.DisableBlackCoverCamera();
                        if (Application.platform == RuntimePlatform.WindowsPlayer)
                        {
                            this.$this.StartSdkInit();
                        }
                        this.$this.m_gameManager = GameManager.CreateAndInitGameManager<ProjectXGameManager>();
                        if (this.$this.m_gameManager == null)
                        {
                            Debug.LogError("CreateAndInitGameManager start fail");
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        UIManager.Instance.RegisterUITaskWithGroup("ProjectXGameEntryUITask", new TypeDNName(typeof(ProjectXGameEntryUITask).FullName), 0, null);
                        this.<entryTask>__0 = UIManager.Instance.StartUITask(new UIIntent("ProjectXGameEntryUITask", null), false, false, null, null) as ProjectXGameEntryUITask;
                        if (this.<entryTask>__0 == null)
                        {
                            Debug.LogError("ProjectXGameEntryUITask start fail");
                        }
                        SceneManager.Instance.SetEventSystemDpi((int) Math.Round((double) (Screen.dpi * 0.05f)));
                        SDKHelper.SDKDebug("1", string.Empty);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartAndroidObbProcessing>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal IGooglePlayObbDownloader <obbDownloader>__0;
            internal string <expPath>__0;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        UnityEngine.Debug.Log("ProjectXGameLauncher:StartAndroidObbProcessing ");
                        if (GooglePlayObbDownloadManager.IsDownloaderAvailable())
                        {
                            this.<obbDownloader>__0 = GooglePlayObbDownloadManager.GetGooglePlayObbDownloader();
                            this.<expPath>__0 = this.<obbDownloader>__0.GetExpansionFilePath();
                            if (this.<expPath>__0 != null)
                            {
                                this.<obbDownloader>__0.PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj8jtTcL3Xe2fTl7QnotF9urbGyGQ48cdZAphHrTAnt7CEPGkCu/Xm7HN49sSU8/lJOxRNpF0AAPUBbjYi8z/8SZjifE5MPI9GPEvHH8nFBdnqAU2Pcq+2WACF4OseLQ6bwiNzH+MxgM/OMSysKbpBzizcrBeZHI3koFQ54+BwI1j46AkUeShQ0YKGPfCcTvwKsjhEWfo9QUp8zhhSu/twYFZrsNeZv1ijxIuhfYdbD4RA7FenWq0OKlHuajN7anfLQxiUGqpWpBH/18t7oyaWz2qWLuqTFvVh6lu0ULCF30XrwK1T9xNVFAVRCDiFRj7l2W0ddqYeZlhRxN++Q35IwIDAQAB";
                                UnityEngine.Debug.Log("Init ObbDownloader  " + Application.dataPath);
                                if (this.<obbDownloader>__0 == null)
                                {
                                    goto TR_0009;
                                }
                                else if (this.<obbDownloader>__0.GetMainOBBPath() != null)
                                {
                                    goto TR_0009;
                                }
                                else
                                {
                                    this.<obbDownloader>__0.FetchOBB();
                                }
                                break;
                            }
                            else
                            {
                                UnityEngine.Debug.LogError("External storage is not available!");
                                if (this.onEnd != null)
                                {
                                    this.onEnd(false);
                                }
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("Use GooglePlayDownloader only on Android device!");
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        goto TR_0000;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<obbDownloader>__0.GetMainOBBPath() == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                goto TR_0009;
            TR_0000:
                return false;
            TR_0009:
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

