﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SDK;
    using BlackJack.ProjectX.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using System.Threading;
    using UnityEngine;

    public class ProjectXGameEntryUITask : GameEntryUITaskBase
    {
        protected Action m_onMsgBoxConfirmButtonClick;
        private const int BytesTo2MB = 0x200000;
        private float m_refreshDelayTime;
        private const float RefreshDelayTime = 0.5f;
        private const float m_intervalProgressMin = 0.004f;
        private const float m_intervalProgressMax = 0.008f;
        protected float m_nextLoadingProcessValue;
        protected float m_loadingProcessValue;
        protected const float WaitForSDKUpdateCheckTime = 7f;
        protected const float LoadingProcess_StreamingAssetsStart = 0f;
        protected const float LoadingProcess_StreamingAssetsEnd = 0.1f;
        protected const float LoadingProcess_DownLoadBundleDataEnd = 0.2f;
        protected const float LoadingProcess_PreloadBundleEnd = 0.3f;
        protected const float LoadingProcess_LoadBundleManifestEnd = 0.4f;
        protected const float LoadingProcess_LoadDllEnd = 0.5f;
        protected const float LoadingProcess_LoadConfigDataEnd = 0.9f;
        protected const float LoadingProcess_AllEnd = 1f;
        public string GameEntryLangName;
        private long m_downloadTotalBytes;
        private Dictionary<int, string> m_gameLangTextDict;
        private string[][] m_entryUIPoemText;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private ProjectXGameEntryUIController m_mainCtrl;
        private MsgBoxUIController m_msgBoxCtrl;
        private ProjectXGameUpdateAssetUIController m_updateAssetUICtrl;
        private UIProcess m_logoProcess;
        public const string TaskName = "ProjectXGameEntryUITask";
        private bool m_isShowLogoFinished;
        private bool m_sdkForceUpdate;
        [CompilerGenerated]
        private static Action<Action> <>f__mg$cache0;
        [CompilerGenerated]
        private static Action<bool> <>f__mg$cache1;
        [CompilerGenerated]
        private static Action<bool> <>f__mg$cache2;
        [CompilerGenerated]
        private static Action<bool, bool> <>f__mg$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_EntryPipeLine;
        private static DelegateBridge __Hotfix_OnUpdateAssetBundleDownloadUrl;
        private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingStart;
        private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingEnd;
        private static DelegateBridge __Hotfix_OnBundleDataLoadingStart;
        private static DelegateBridge __Hotfix_OnBundleDataLoadingEnd;
        private static DelegateBridge __Hotfix_OnBasicVersionUnmatch;
        private static DelegateBridge __Hotfix_NotifyUserDownloadAndWait;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingRefuse;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingStart;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingEnd;
        private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingStart;
        private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingEnd;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysStart;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysEnd;
        private static DelegateBridge __Hotfix_OnStartLuaManagerStart;
        private static DelegateBridge __Hotfix_OnStartLuaManagerEnd;
        private static DelegateBridge __Hotfix_OnStartHotfixManagerStart;
        private static DelegateBridge __Hotfix_OnStartHotfixManagerEnd;
        private static DelegateBridge __Hotfix_OnLoadConfigDataStart;
        private static DelegateBridge __Hotfix_OnLoadingConfigData;
        private static DelegateBridge __Hotfix_OnLoadConfigDataEnd;
        private static DelegateBridge __Hotfix_OnStartAudioManagerStart;
        private static DelegateBridge __Hotfix_OnStartAudioManagerEnd;
        private static DelegateBridge __Hotfix_LaunchLogin;
        private static DelegateBridge __Hotfix_UpdateView4StreamingAssetsFilesProcessing;
        private static DelegateBridge __Hotfix_UpdateView4BundleDataLoading;
        private static DelegateBridge __Hotfix_UpdateView4AssetBundlePreUpdateing;
        private static DelegateBridge __Hotfix_UpdateView4AssetBundleManifestLoading;
        private static DelegateBridge __Hotfix_ReLoginBySession;
        private static DelegateBridge __Hotfix_ReturnToLoginFromRelogin;
        private static DelegateBridge __Hotfix_ReturnToLoginUI;
        private static DelegateBridge __Hotfix_ShowUIWaiting;
        private static DelegateBridge __Hotfix_OnConfirmClick;
        private static DelegateBridge __Hotfix_OnLogoBtnDoubleClick;
        private static DelegateBridge __Hotfix_StartInitSDK;
        private static DelegateBridge __Hotfix_StartPdSdkInit;
        private static DelegateBridge __Hotfix_OnPdSdkInitEnd;
        private static DelegateBridge __Hotfix_OnPdSdkForceUpdate;
        private static DelegateBridge __Hotfix_OnStartRetryExecuteNetWork;
        private static DelegateBridge __Hotfix_ShowNetworkErrorMsgBox;
        private static DelegateBridge __Hotfix_GetNetworkErrorMsg;
        private static DelegateBridge __Hotfix_UpdateStaticText;
        private static DelegateBridge __Hotfix_LoadGameEntryLang;
        private static DelegateBridge __Hotfix_ReadGameEntryLange;
        private static DelegateBridge __Hotfix_ReadEntryUIPeom;
        private static DelegateBridge __Hotfix_OnStartDownloadClick;
        private static DelegateBridge __Hotfix_OnCancelDownloadClick;
        private static DelegateBridge __Hotfix_CheckDoloadAssetWIFIEnv;
        private static DelegateBridge __Hotfix_ShowDowloadTips;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_get_GameEntryLangPath;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ProjectXGameEntryUITask(string taskName) : base(taskName)
        {
            this.GameEntryLangName = "GameEntryLang";
            this.m_gameLangTextDict = new Dictionary<int, string>();
            UITaskBase.LayerDesc desc = new UITaskBase.LayerDesc {
                m_layerName = "EntryUILayer",
                m_layerResPath = "Assets/GameProject/Resources/UI/EntryUIPrefab.prefab",
                m_isUILayer = true
            };
            this.m_layerDescArray = new UITaskBase.LayerDesc[] { desc };
            UITaskBase.UIControllerDesc desc2 = new UITaskBase.UIControllerDesc {
                m_attachLayerName = "EntryUILayer",
                m_attachPath = "EntryUIPrefab",
                m_ctrlTypeDNName = new TypeDNName("Assembly-CSharp@BlackJack.ProjectX.Static.ProjectXGameEntryUIController"),
                m_ctrlName = "ProjectXGameEntryUIController"
            };
            UITaskBase.UIControllerDesc[] descArray2 = new UITaskBase.UIControllerDesc[3];
            descArray2[0] = desc2;
            desc2 = new UITaskBase.UIControllerDesc {
                m_attachLayerName = "EntryUILayer",
                m_attachPath = "EntryUIPrefab/MsgBoxUIPrefab",
                m_ctrlTypeDNName = new TypeDNName("Assembly-CSharp@BlackJack.ProjectX.Runtime.UI.MsgBoxForEntryUIController"),
                m_ctrlName = "MsgBoxForEntryUIController"
            };
            descArray2[1] = desc2;
            desc2 = new UITaskBase.UIControllerDesc {
                m_attachLayerName = "EntryUILayer",
                m_attachPath = "EntryUIPrefab/UpdateUIPrefab",
                m_ctrlTypeDNName = new TypeDNName("Assembly-CSharp@BlackJack.ProjectX.Static.ProjectXGameUpdateAssetUIController"),
                m_ctrlName = "ProjectXGameUpdateAssetUIController"
            };
            descArray2[2] = desc2;
            this.m_uiCtrlDescArray = descArray2;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, taskName);
            }
        }

        [MethodImpl(0x8000)]
        private void CheckDoloadAssetWIFIEnv(long totalBytes)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator EntryPipeLine()
        {
            DelegateBridge bridge = __Hotfix_EntryPipeLine;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <EntryPipeLine>c__Iterator0 { $this = this };
        }

        [MethodImpl(0x8000)]
        protected override string GetNetworkErrorMsg()
        {
        }

        [MethodImpl(0x8000)]
        protected override void LaunchLogin()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerable<string> LoadGameEntryLang()
        {
            DelegateBridge bridge = __Hotfix_LoadGameEntryLang;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp4160(this);
            }
            return new <LoadGameEntryLang>c__Iterator1 { 
                $this = this,
                $PC = -2
            };
        }

        [MethodImpl(0x8000)]
        protected override void NotifyUserDownloadAndWait(long totalDownloadByte)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAssetBundleManifestLoadingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAssetBundleManifestLoadingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAssetBundlePreUpdateingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAssetBundlePreUpdateingRefuse()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAssetBundlePreUpdateingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBasicVersionUnmatch()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBundleDataLoadingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBundleDataLoadingStart()
        {
            DelegateBridge bridge = __Hotfix_OnBundleDataLoadingStart;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Debug.Log("OnBundleDataLoadingStart");
                this.m_mainCtrl.SetMsg("OnBundleDataLoadingStart");
            }
        }

        [MethodImpl(0x8000)]
        private void OnCancelDownloadClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadConfigDataEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadConfigDataStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicAssemblysEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicAssemblysStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadingConfigData(float processVal)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogoBtnDoubleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPdSdkForceUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPdSdkInitEnd(bool isSuccess)
        {
            DelegateBridge bridge = __Hotfix_OnPdSdkInitEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isSuccess);
            }
            else
            {
                Debug.Log($"ProjectXGameEntryUITask::OnPDSDKInitEnd isSuccess: {isSuccess}");
                if (SDKHelper.IsIosReview)
                {
                    GameManager.Instance.GameClientSetting.ResourcesSetting.DisableAssetBundleDownload = true;
                    GameManager.Instance.GameClientSetting.ResourcesSetting.SkipAssetBundlePreUpdateing = true;
                    GameManager.Instance.GameClientSetting.ResourcesSetting.AssetBundleDownloadUrlRoot = string.Empty;
                    ResourceManager.Instance.SetDisableAssetBundleDownload(true);
                    ResourceManager.Instance.SetSkipAssetBundlePreUpdateing(true);
                    ResourceManager.Instance.SetDownloadBundleUrl(string.Empty);
                }
                PDSDK instance = PDSDK.Instance;
                instance.m_eventInitCallback = (Action<bool>) System.Delegate.Remove(instance.m_eventInitCallback, new Action<bool>(this.OnPdSdkInitEnd));
                if (isSuccess)
                {
                    this.OnSDKInitEnd(true, false);
                }
                else
                {
                    Debug.LogError("ProjectXGameEntryUITask:::OnPdSdkInitEnd  faild, RetryExecuteNetWork StartInitSDK");
                    base.RetryExecuteNetWork(new Action(this.StartPdSdkInit));
                }
            }
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
            DelegateBridge bridge = __Hotfix_OnStart;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp43(this, intent, onPipelineEnd);
            }
            this.ReadGameEntryLange();
            return base.OnStart(intent, onPipelineEnd);
        }

        [MethodImpl(0x8000)]
        protected override void OnStartAudioManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartAudioManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStartDownloadClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartHotfixManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartHotfixManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartLuaManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartLuaManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStartRetryExecuteNetWork()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStreamingAssetsFilesProcessingEnd(bool ret)
        {
            DelegateBridge bridge = __Hotfix_OnStreamingAssetsFilesProcessingEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, ret);
            }
            else
            {
                Debug.Log($"OnStreamingAssetsFilesProcessingEnd ret:{ret}");
                if (!ret)
                {
                    this.m_mainCtrl.SetMsg("OnStreamingAssetsFilesProcessingEnd fail");
                }
                this.m_loadingProcessValue = 0.1f;
                this.m_nextLoadingProcessValue = 0.2f;
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnStreamingAssetsFilesProcessingStart()
        {
            DelegateBridge bridge = __Hotfix_OnStreamingAssetsFilesProcessingStart;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Debug.Log("OnStreamingAssetsFilesProcessingStart");
                this.m_mainCtrl.SetMsg("OnStreamingAssetsFilesProcessingStart");
                this.m_loadingProcessValue = 0f;
                this.m_nextLoadingProcessValue = 0.1f;
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
            DelegateBridge bridge = __Hotfix_OnTick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (!this.m_sdkForceUpdate)
            {
                base.OnTick();
                this.m_refreshDelayTime += Time.deltaTime;
                if ((this.m_refreshDelayTime >= 0.5f) && (this.m_mainCtrl != null))
                {
                    float nextLoadingProcessValue;
                    if (this.m_loadingProcessValue >= this.m_nextLoadingProcessValue)
                    {
                        nextLoadingProcessValue = this.m_nextLoadingProcessValue;
                    }
                    else
                    {
                        nextLoadingProcessValue = this.m_loadingProcessValue += UnityEngine.Random.Range((float) 0.004f, (float) 0.008f);
                    }
                    this.m_loadingProcessValue = nextLoadingProcessValue;
                    this.m_mainCtrl.SetProcessBar(true, this.m_loadingProcessValue);
                    this.m_refreshDelayTime = 0f;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected override bool OnUpdateAssetBundleDownloadUrl(bool isSuccess)
        {
            DelegateBridge bridge = __Hotfix_OnUpdateAssetBundleDownloadUrl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp103(this, isSuccess);
            }
            if (isSuccess)
            {
                return true;
            }
            this.m_msgBoxCtrl.OnShowMsgBoxStart(this.m_gameLangTextDict[0x13], string.Empty, this.m_gameLangTextDict[20]);
            this.m_msgBoxCtrl.m_confirmButtonText.text = this.m_gameLangTextDict[30];
            this.m_msgBoxCtrl.m_cancelButtonText.text = this.m_gameLangTextDict[0x17];
            UIPlayingEffectInfo info = this.m_msgBoxCtrl.CreateShowBothOrConfirmProcess(false);
            UIPlayingEffectInfo[] effectList = new UIPlayingEffectInfo[] { info };
            CommonUIStateEffectProcess process = UIProcessFactory.CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode.Parallel, effectList);
            base.PlayUIProcess(process, true, null, false);
            return false;
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
            DelegateBridge bridge = __Hotfix_PostOnLoadAllResCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_mainCtrl = base.m_uiCtrlArray[0] as ProjectXGameEntryUIController;
                this.m_mainCtrl.SetButtonDoubleClickListener("LogoBtn", new Action<UIControllerBase>(this.OnLogoBtnDoubleClick));
                this.m_msgBoxCtrl = base.m_uiCtrlArray[1] as MsgBoxUIController;
                this.m_updateAssetUICtrl = base.m_uiCtrlArray[2] as ProjectXGameUpdateAssetUIController;
                this.m_msgBoxCtrl.SetButtonClickListener("m_confirmButton", new Action<UIControllerBase>(this.OnConfirmClick));
                this.m_updateAssetUICtrl.SetButtonClickListener("ConfirmButton", new Action<UIControllerBase>(this.OnStartDownloadClick));
                this.m_updateAssetUICtrl.SetButtonClickListener("CancelButton", new Action<UIControllerBase>(this.OnCancelDownloadClick));
                this.m_updateAssetUICtrl.SetButtonClickListener("CancelUpdateButton", new Action<UIControllerBase>(this.OnCancelDownloadClick));
                this.m_mainCtrl.ProcessBar.gameObject.SetActive(false);
                this.m_mainCtrl.MsgPercent.gameObject.SetActive(false);
                base.PostOnLoadAllResCompleted();
            }
        }

        [MethodImpl(0x8000)]
        private void ReadEntryUIPeom()
        {
            DelegateBridge bridge = __Hotfix_ReadEntryUIPeom;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                string[] textArray1 = new string[] { this.m_gameLangTextDict[1], this.m_gameLangTextDict[2], this.m_gameLangTextDict[3], this.m_gameLangTextDict[4] };
                string[][] textArrayArray1 = new string[4][];
                textArrayArray1[0] = textArray1;
                textArrayArray1[1] = new string[] { this.m_gameLangTextDict[5], this.m_gameLangTextDict[6], this.m_gameLangTextDict[7], this.m_gameLangTextDict[8] };
                textArrayArray1[2] = new string[] { this.m_gameLangTextDict[9], this.m_gameLangTextDict[10], this.m_gameLangTextDict[11], this.m_gameLangTextDict[12], this.m_gameLangTextDict[13] };
                textArrayArray1[3] = new string[] { this.m_gameLangTextDict[14], this.m_gameLangTextDict[15], this.m_gameLangTextDict[0x10], this.m_gameLangTextDict[0x11], " " };
                this.m_entryUIPoemText = textArrayArray1;
            }
        }

        [MethodImpl(0x8000)]
        private void ReadGameEntryLange()
        {
            DelegateBridge bridge = __Hotfix_ReadGameEntryLange;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (string str in this.LoadGameEntryLang())
                {
                    string pattern = ",=";
                    string[] strArray = Regex.Split(str, pattern, RegexOptions.IgnoreCase);
                    int key = int.Parse(strArray[0]);
                    string str3 = strArray[1];
                    this.m_gameLangTextDict.Add(key, str3);
                }
                this.ReadEntryUIPeom();
            }
        }

        [MethodImpl(0x8000)]
        private static void ReLoginBySession(Action action)
        {
        }

        [MethodImpl(0x8000)]
        private static void ReturnToLoginFromRelogin(bool isDataDirty)
        {
        }

        [MethodImpl(0x8000)]
        private static void ReturnToLoginUI(bool isDataDirty, bool switchAccount)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDowloadTips(long totalBytes, bool isWifi)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowNetworkErrorMsgBox(string msg, Action onConfirm)
        {
        }

        [MethodImpl(0x8000)]
        private static void ShowUIWaiting(bool isshow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartInitSDK()
        {
            DelegateBridge bridge = __Hotfix_StartInitSDK;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Debug.Log("LoginUITask::StartInitSDK");
                base.StartInitSDK();
                this.StartPdSdkInit();
            }
        }

        [MethodImpl(0x8000)]
        protected void StartPdSdkInit()
        {
            DelegateBridge bridge = __Hotfix_StartPdSdkInit;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Debug.Log($"ProjectXGameEntryUITask::StartPDSDKInit isInit = {PDSDK.Instance.isInit}");
                if (GameManager.Instance.GameClientSetting.LoginSetting.LoginUseSDK)
                {
                    SDKHelper.SetSdkLangueType(GameManager.Instance.CurrLocalization);
                }
                if (PDSDK.Instance.isInit)
                {
                    this.OnPdSdkInitEnd(true);
                }
                else
                {
                    PDSDK instance = PDSDK.Instance;
                    instance.m_eventInitCallback = (Action<bool>) System.Delegate.Combine(instance.m_eventInitCallback, new Action<bool>(this.OnPdSdkInitEnd));
                    PDSDK.Instance.EventOnForceUpdate += new Action(this.OnPdSdkForceUpdate);
                    base.ExecuteNetWorkOnNetReachable(new Action(PDSDK.Instance.Init));
                }
            }
        }

        [MethodImpl(0x8000)]
        private void UpdateStaticText()
        {
            DelegateBridge bridge = __Hotfix_UpdateStaticText;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_updateAssetUICtrl.CancelButtonText.text = this.m_gameLangTextDict[0x17];
                this.m_updateAssetUICtrl.ConfirmButtonText.text = this.m_gameLangTextDict[0x18];
                this.m_updateAssetUICtrl.CancelUpdateButtonText.text = this.m_gameLangTextDict[0x19];
                this.m_updateAssetUICtrl.UpdateText.text = this.m_gameLangTextDict[0x1a];
                this.m_updateAssetUICtrl.TitleText.text = this.m_gameLangTextDict[0x1b];
                this.m_updateAssetUICtrl.QuitHintText.text = this.m_gameLangTextDict[0x1d];
                this.m_mainCtrl.Msg.text = this.m_gameLangTextDict[0x1c];
            }
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
            DelegateBridge bridge = __Hotfix_UpdateView;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                UIManager.Instance.GlobalUIInputEnable("ProjectXGameEntryUITask", true);
                this.m_logoProcess = this.m_mainCtrl.GetShowCompanyLogoProcess();
                base.PlayUIProcess(this.m_logoProcess, true, delegate (UIProcess p, bool ret) {
                    if (!ret)
                    {
                        this.m_mainCtrl.ForceCloseLogoPanel();
                    }
                    if (this.m_mainCtrl != null)
                    {
                        this.m_mainCtrl.UpdateEntryUIText(this.m_entryUIPoemText);
                    }
                    this.UpdateStaticText();
                    this.m_isShowLogoFinished = true;
                    this.m_logoProcess = null;
                }, true);
            }
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView4AssetBundleManifestLoading()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView4AssetBundlePreUpdateing()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView4BundleDataLoading()
        {
            DelegateBridge bridge = __Hotfix_UpdateView4BundleDataLoading;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView4StreamingAssetsFilesProcessing()
        {
        }

        public string GameEntryLangPath
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_GameEntryLangPath;
                return ((bridge == null) ? $"{"UI"}/{this.GameEntryLangName}_{GameManager.Instance.CurrLocalization}" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LayerDescArray;
                return ((bridge == null) ? this.m_layerDescArray : bridge.__Gen_Delegate_Imp321(this));
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_UICtrlDescArray;
                return ((bridge == null) ? this.m_uiCtrlDescArray : bridge.__Gen_Delegate_Imp322(this));
            }
        }

        [CompilerGenerated]
        private sealed class <EntryPipeLine>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal ProjectXGameEntryUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.$this.m_isShowLogoFinished)
                        {
                            this.$current = this.$this.<EntryPipeLine>__BaseCallProxy0();
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                        }
                        else
                        {
                            this.$current = new WaitForEndOfFrame();
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                        }
                        return true;

                    case 2:
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LoadGameEntryLang>c__Iterator1 : IEnumerable, IEnumerable<string>, IEnumerator, IDisposable, IEnumerator<string>
        {
            internal TextAsset <gameEntryText>__0;
            internal TextReader <reader>__1;
            internal string <line>__2;
            internal ProjectXGameEntryUITask $this;
            internal string $current;
            internal bool $disposing;
            internal int $PC;

            private void <>__Finally0()
            {
                if (this.<reader>__1 != null)
                {
                    this.<reader>__1.Dispose();
                }
            }

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.<>__Finally0();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<gameEntryText>__0 = Resources.Load<TextAsset>(this.$this.GameEntryLangPath);
                        if (this.<gameEntryText>__0 == null)
                        {
                            this.<gameEntryText>__0 = Resources.Load<TextAsset>($"{"UI"}/{this.$this.GameEntryLangName}_{"CN"}");
                        }
                        this.<reader>__1 = new StringReader(this.<gameEntryText>__0.text);
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            this.<line>__2 = this.<reader>__1.ReadLine();
                            if (this.<line>__2 == null)
                            {
                                break;
                            }
                            this.$current = this.<line>__2;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            flag = true;
                            return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.<>__Finally0();
                    }
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<string> IEnumerable<string>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new ProjectXGameEntryUITask.<LoadGameEntryLang>c__Iterator1 { $this = this.$this };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<string>.GetEnumerator();

            string IEnumerator<string>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

