﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    internal class ProjectXGameUpdateAssetUIController : UIControllerBase
    {
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WifiSelectState;
        [AutoBind("./ContentGroup/HintText01", AutoBindAttribute.InitState.NotInit, false)]
        public Text HintTextCapacity;
        [AutoBind("./ContentGroup/NoWifiGroup/QuitHintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuitHintText;
        [AutoBind("./ContentGroup/NoWifiGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ConfirmButton;
        [AutoBind("./ContentGroup/NoWifiGroup/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ConfirmButtonText;
        [AutoBind("./ContentGroup/NoWifiGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelButton;
        [AutoBind("./ContentGroup/NoWifiGroup/CancelButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CancelButtonText;
        [AutoBind("./ContentGroup/WifiGroup/UpdateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpdateText;
        [AutoBind("./ContentGroup/WifiGroup/CancelUpdateButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelUpdateButton;
        [AutoBind("./ContentGroup/WifiGroup/CancelUpdateButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CancelUpdateButtonText;
        [AutoBind("./ContentGroup/WifiGroup/UpdateNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpdateNumberText;
        [AutoBind("./ContentGroup/WifiGroup/UpdateBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UpdateBarImage;
        private const int BytesToMB = 0x100000;
        private const int MinBytesForUpdate = 0x28f6;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateDoladAssetGroupTipView;
        private static DelegateBridge __Hotfix_UpdateProgressBar;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnBindFiledsCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.OnBindFiledsCompleted();
                this.UpdateBarImage.fillAmount = 0f;
            }
        }

        [MethodImpl(0x8000)]
        public void UpdateDoladAssetGroupTipView(long totalBytes, bool isWifi, string tipText)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProgressBar(float val, long downloadedBytes, long totalBytes)
        {
        }
    }
}

