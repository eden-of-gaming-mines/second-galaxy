﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SDK;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProjectXGameEntryUIController : UIControllerBase
    {
        [AutoBind("EntryUIPrefab/CompanyLogo/SDKLogoPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LogoBtn;
        [AutoBind("EntryUIPrefab/Msg", AutoBindAttribute.InitState.NotInit, false)]
        public Text Msg;
        [AutoBind("EntryUIPrefab/ProcessBar", AutoBindAttribute.InitState.NotInit, false)]
        public Slider ProcessBar;
        [AutoBind("EntryUIPrefab/MsgPercent", AutoBindAttribute.InitState.NotInit, false)]
        public Text MsgPercent;
        [AutoBind("EntryUIPrefab/TextRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TextRoot;
        [AutoBind("EntryUIPrefab/TextRoot/AuthorText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AuthorText;
        [AutoBind("EntryUIPrefab/UpdateUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpdateUIStateCtr;
        [AutoBind("EntryUIPrefab/CompanyLogo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LogoStateCtrl;
        private static DelegateBridge __Hotfix_SetMsg;
        private static DelegateBridge __Hotfix_SetProcessBar;
        private static DelegateBridge __Hotfix_GetShowCompanyLogoProcess;
        private static DelegateBridge __Hotfix_ForceCloseLogoPanel;
        private static DelegateBridge __Hotfix_UpdateEntryUIText;

        [MethodImpl(0x8000)]
        public void ForceCloseLogoPanel()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetShowCompanyLogoProcess()
        {
            DelegateBridge bridge = __Hotfix_GetShowCompanyLogoProcess;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp4114(this);
            }
            UIPlayingEffectInfo info = new UIPlayingEffectInfo(this.LogoStateCtrl, "Page1", false, true);
            UIPlayingEffectInfo info2 = new UIPlayingEffectInfo(this.LogoStateCtrl, "Page2", false, true);
            UIPlayingEffectInfo info3 = new UIPlayingEffectInfo(this.LogoStateCtrl, "Page3", false, true);
            UIPlayingEffectInfo info4 = new UIPlayingEffectInfo(this.LogoStateCtrl, "Close", false, true);
            UIPlayingEffectInfo[] effectList = new UIPlayingEffectInfo[] { info, info2, info3, info4 };
            return UIProcessFactory.CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode.Serial, effectList);
        }

        [MethodImpl(0x8000)]
        public void SetMsg(string msg)
        {
            DelegateBridge bridge = __Hotfix_SetMsg;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, msg);
            }
        }

        [MethodImpl(0x8000)]
        public void SetProcessBar(bool enable, float value)
        {
            DelegateBridge bridge = __Hotfix_SetProcessBar;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp3750(this, enable, value);
            }
            else if ((!Application.isEditor && !PDSDK.Instance.isInit) || SDKHelper.IsIosReview)
            {
                if (this.ProcessBar.gameObject.activeInHierarchy)
                {
                    this.ProcessBar.gameObject.SetActive(false);
                }
                if (this.MsgPercent.gameObject.activeInHierarchy)
                {
                    this.MsgPercent.gameObject.SetActive(false);
                }
            }
            else
            {
                if (this.ProcessBar.gameObject.activeInHierarchy != enable)
                {
                    this.ProcessBar.gameObject.SetActive(enable);
                }
                if (this.MsgPercent.gameObject.activeInHierarchy != enable)
                {
                    this.MsgPercent.gameObject.SetActive(enable);
                }
                this.ProcessBar.value = value;
                this.MsgPercent.text = value.ToString("P2");
            }
        }

        [MethodImpl(0x8000)]
        public void UpdateEntryUIText(string[][] entryUIText)
        {
            DelegateBridge bridge = __Hotfix_UpdateEntryUIText;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp4158(this, entryUIText);
            }
            else
            {
                int index = UnityEngine.Random.Range(3, 3);
                string[] strArray = entryUIText[index];
                int childCount = this.TextRoot.transform.childCount;
                if (strArray.Length <= childCount)
                {
                    int num3 = 0;
                    while (true)
                    {
                        if (num3 >= (strArray.Length - 1))
                        {
                            while (true)
                            {
                                if (num3 >= (childCount - 1))
                                {
                                    this.AuthorText.text = strArray[strArray.Length - 1];
                                    break;
                                }
                                this.TextRoot.transform.GetChild(num3).gameObject.SetActive(false);
                                num3++;
                            }
                            break;
                        }
                        Text component = this.TextRoot.transform.GetChild(num3).GetComponent<Text>();
                        component.text = strArray[num3];
                        component.gameObject.SetActive(true);
                        num3++;
                    }
                }
            }
        }
    }
}

