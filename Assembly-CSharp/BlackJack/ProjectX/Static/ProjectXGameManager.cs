﻿namespace BlackJack.ProjectX.Static
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.ConfigData;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime;
    using BlackJack.SensitiveWords;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProjectXGameManager : GameManager
    {
        protected volatile bool m_isIosReviewResHandleComplete;
        protected volatile bool m_isIosReviewResLoadComplete;
        public object m_lockObj = new object();
        protected List<IosReviewResParams> m_postHandleItemList;
        protected List<UnityEngine.Object> m_unloadAssetList = new List<UnityEngine.Object>();
        protected bool m_isIosReviewResLoadStarted;
        protected GameObject m_preLoadShaderGo;
        protected DateTime m_downLoadAssetBundleUpdateServerUrlOutTime = DateTime.MinValue;
        protected const float DownLoadAssetBundleUpdateServerUrlDelayTime = 600f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_InitlizeGameSetting;
        private static DelegateBridge __Hotfix_CreateServerSettingManager;
        private static DelegateBridge __Hotfix_GetDefaultLocalization;
        private static DelegateBridge __Hotfix_StartInitGraphSetting;
        private static DelegateBridge __Hotfix_GetGraphicQualityLevelByDeviceRateLevel;
        private static DelegateBridge __Hotfix_OnApplicationQuit;
        private static DelegateBridge __Hotfix_InitGameNetworkClient;
        private static DelegateBridge __Hotfix_InitPlayerContext;
        private static DelegateBridge __Hotfix_Clear4Return2Login;
        private static DelegateBridge __Hotfix_CreateConfigDataLoader;
        private static DelegateBridge __Hotfix_GetAudioManagerInitConfigData;
        private static DelegateBridge __Hotfix_GetClientLogUploadUrl;
        private static DelegateBridge __Hotfix_GetUploadFileServerSaveName;
        private static DelegateBridge __Hotfix_OnLoadConfigDataEnd;
        private static DelegateBridge __Hotfix_InitPropertiesCalculater;
        private static DelegateBridge __Hotfix_DeserializeProtoBufMessage;
        private static DelegateBridge __Hotfix_SaveDataSection;
        private static DelegateBridge __Hotfix_Clear4Relogin;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysCompleted;
        private static DelegateBridge __Hotfix_OnGameManagerException;
        private static DelegateBridge __Hotfix_SetDataSectionDirty;
        private static DelegateBridge __Hotfix_TickDownloadAssetBundleUpdateServerUrl;
        private static DelegateBridge __Hotfix_OnDownloadAssetBundleUpdateServer;
        private static DelegateBridge __Hotfix_OnOverflowTextClick;
        private static DelegateBridge __Hotfix_IsReady;
        private static DelegateBridge __Hotfix_StartIosReviewResLoad;
        private static DelegateBridge __Hotfix_StartIosReviewResLoadImpl;
        private static DelegateBridge __Hotfix_StartConfigDataHandleThread;
        private static DelegateBridge __Hotfix_LoadSingleConfigDataRes;
        private static DelegateBridge __Hotfix_TryGetResHandleItem;
        private static DelegateBridge __Hotfix_TryAddResHandleItem;
        private static DelegateBridge __Hotfix_PostIosReviewResLoadEnd;
        private static DelegateBridge __Hotfix_get_IsIosReviewResHandleComplete;

        [MethodImpl(0x8000)]
        public ProjectXGameManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear4Relogin()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear4Return2Login(bool isCacheDataDirty)
        {
        }

        [MethodImpl(0x8000)]
        protected override ClientConfigDataLoaderBase CreateConfigDataLoader()
        {
        }

        [MethodImpl(0x8000)]
        protected override ServerSettingManager CreateServerSettingManager()
        {
            DelegateBridge bridge = __Hotfix_CreateServerSettingManager;
            return ((bridge == null) ? ((ServerSettingManager) ServerSettingManager.CreateManager<ProjectXServerSettingManager>()) : bridge.__Gen_Delegate_Imp74(this));
        }

        [MethodImpl(0x8000)]
        private object DeserializeProtoBufMessage(Stream stream, System.Type packageType, int msgId)
        {
        }

        [MethodImpl(0x8000)]
        protected override Dictionary<string, string> GetAudioManagerInitConfigData()
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetClientLogUploadUrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetDefaultLocalization()
        {
        }

        [MethodImpl(0x8000)]
        private GraphicQualityLevel GetGraphicQualityLevelByDeviceRateLevel(DeviceRatingLevel deviceRatingLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetUploadFileServerSaveName(string srcFileName)
        {
        }

        [MethodImpl(0x8000)]
        public override bool InitGameNetworkClient()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initlize()
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            bool flag = base.Initlize();
            SceneAnimationPlayerHelper.RegistSceneAnimationCreator(new ProjectXSceneAnimationPlayerCreator());
            CGPlayerHelper.RegistIVideoPlayerCreator(new CriVideoPlayerCreator());
            AudioCompressorHelper.AudioCompressor = new AudioCompressor();
            if (GameManager.Instance.GameClientSetting.AudioSetting.EnableCRI)
            {
                AudioManager4CRI.Instance.SetCRIProvider(new CRIProvider());
            }
            TextSupportStringOverflow.EventOnOverflowTextClick += new Action<Text>(this.OnOverflowTextClick);
            return flag;
        }

        [MethodImpl(0x8000)]
        protected override bool InitlizeGameSetting(string gameSettingTypeName = null)
        {
            DelegateBridge bridge = __Hotfix_InitlizeGameSetting;
            return ((bridge == null) ? base.InitlizeGameSetting(typeof(GameClientSetting).FullName) : bridge.__Gen_Delegate_Imp16(this, gameSettingTypeName));
        }

        [MethodImpl(0x8000)]
        public override bool InitPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        private void InitPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsReady()
        {
        }

        [MethodImpl(0x8000)]
        protected void LoadSingleConfigDataRes(string subPath, string configDataName)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnApplicationQuit()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDownloadAssetBundleUpdateServer()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnGameManagerException()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnLoadConfigDataEnd(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicAssemblysCompleted(bool ret, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOverflowTextClick(Text srcText)
        {
        }

        [MethodImpl(0x8000)]
        public void PostIosReviewResLoadEnd(string configDataName, string configAssetName, UnityEngine.Object asset, ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo assetInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SaveDataSection()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetDataSectionDirty()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartConfigDataHandleThread()
        {
        }

        [MethodImpl(0x8000)]
        public override IEnumerator StartInitGraphSetting(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartIosReviewResLoad()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator StartIosReviewResLoadImpl()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.Tick();
                TranslateManager.Instance.Tick();
                this.TickDownloadAssetBundleUpdateServerUrl();
            }
        }

        [MethodImpl(0x8000)]
        protected void TickDownloadAssetBundleUpdateServerUrl()
        {
            DelegateBridge bridge = __Hotfix_TickDownloadAssetBundleUpdateServerUrl;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_downLoadAssetBundleUpdateServerUrlOutTime == DateTime.MinValue)
                {
                    this.m_downLoadAssetBundleUpdateServerUrlOutTime = Timer.m_currTime.AddSeconds(600.0);
                }
                if (Timer.m_currTime > this.m_downLoadAssetBundleUpdateServerUrlOutTime)
                {
                    Debug.Log("ProjectXGameManager:TickDownloadAssetBundleUpdateServerUrl start download");
                    this.m_downLoadAssetBundleUpdateServerUrlOutTime = DateTime.MinValue;
                    base.m_corutineHelper.StartCorutine(ServerSettingManager.Instance.UpdateAssetBundleDownLoadUrl(new Action(this.OnDownloadAssetBundleUpdateServer)));
                }
            }
        }

        [MethodImpl(0x8000)]
        public void TryAddResHandleItem(IosReviewResParams action)
        {
        }

        [MethodImpl(0x8000)]
        public IosReviewResParams TryGetResHandleItem()
        {
        }

        public bool IsIosReviewResHandleComplete
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnLoadConfigDataEnd>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal ClientConfigDataLoaderBase gdbDataLoader;

            internal void <>m__0(bool llret)
            {
                if (!llret)
                {
                    Debug.LogError("StartLoadConfigData fail m_gdbDataLoader.StartInitializeFromAsset fail");
                    this.onEnd(false);
                }
                else
                {
                    GameManager.Instance.SetGameManagerParam("ClientGDBDataLoader", this.gdbDataLoader);
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartInitGraphSetting>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal ProjectXGameManager $this;

            internal void <>m__0(string path, GameObject asset)
            {
                this.$this.m_preLoadShaderGo = UnityEngine.Object.Instantiate<GameObject>(asset);
                ShaderReferenceDesc component = this.$this.m_preLoadShaderGo.GetComponent<ShaderReferenceDesc>();
                this.$this.m_corutineHelper.StartCorutine(component.CreateFakeMaterials(this.onEnd));
            }
        }

        [CompilerGenerated]
        private sealed class <StartIosReviewResLoadImpl>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal ClientConfigDataLoader <loader>__0;
            internal HashSet<KeyValuePair<string, string>>.Enumerator $locvar0;
            internal KeyValuePair<string, string> <kvPair>__1;
            internal List<UnityEngine.Object>.Enumerator $locvar1;
            internal Dictionary<int, ConfigDataSensitiveWords> <sensitiveWords>__0;
            internal Dictionary<int, ConfigDataSensitiveWords>.Enumerator $locvar2;
            internal ProjectXGameManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        Debug.Log("ProjectXGameManager::StartIosReviewResLoadImpl");
                        this.$this.StartConfigDataHandleThread();
                        this.<loader>__0 = (ClientConfigDataLoader) this.$this.ConfigDataLoader;
                        this.$locvar0 = this.<loader>__0.GetAllLazyInitLoadConfigDataAssetPath().GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    case 2:
                        goto TR_0014;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            if (!this.$locvar0.MoveNext())
                            {
                                break;
                            }
                            this.<kvPair>__1 = this.$locvar0.Current;
                            this.$this.LoadSingleConfigDataRes(this.<kvPair>__1.Key, this.<kvPair>__1.Value);
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            flag = true;
                            goto TR_0010;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$this.m_isIosReviewResLoadComplete = true;
                goto TR_0014;
            TR_0000:
                return false;
            TR_0010:
                return true;
            TR_0014:
                if (!this.$this.m_isIosReviewResHandleComplete)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                }
                else
                {
                    this.$locvar1 = this.$this.m_unloadAssetList.GetEnumerator();
                    try
                    {
                        while (this.$locvar1.MoveNext())
                        {
                            UnityEngine.Object current = this.$locvar1.Current;
                            if (ResourceManager.Instance != null)
                            {
                                ResourceManager.Instance.UnloadAsset(current);
                            }
                        }
                    }
                    finally
                    {
                        this.$locvar1.Dispose();
                    }
                    this.$this.m_unloadAssetList.Clear();
                    this.<sensitiveWords>__0 = ConfigDataHelper.ConfigDataLoader.GetAllConfigDataSensitiveWords();
                    this.$locvar2 = this.<sensitiveWords>__0.GetEnumerator();
                    try
                    {
                        while (this.$locvar2.MoveNext())
                        {
                            KeyValuePair<int, ConfigDataSensitiveWords> current = this.$locvar2.Current;
                            BlackJack.SensitiveWords.SensitiveWords.InitSensitiveWord(current.Value.Text, current.Value.WhiteListA, current.Value.WhiteListB);
                        }
                    }
                    finally
                    {
                        this.$locvar2.Dispose();
                    }
                    this.$PC = -1;
                    goto TR_0000;
                }
                goto TR_0010;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public class IosReviewResParams
        {
            public string configDataName;
            public string configAssetName;
            public UnityEngine.Object asset;
            public ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo assetInfo;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

