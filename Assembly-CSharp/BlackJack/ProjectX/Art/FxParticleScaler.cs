﻿namespace BlackJack.ProjectX.Art
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class FxParticleScaler : MonoBehaviour
    {
        protected ParticleSystem[] m_particleSystems;
        private static DelegateBridge __Hotfix_StartScale;
        private static DelegateBridge __Hotfix_ResetScale;
        private static DelegateBridge __Hotfix_GetHierarchyScale;

        [MethodImpl(0x8000)]
        private float GetHierarchyScale(Transform go)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetScale()
        {
        }

        [MethodImpl(0x8000)]
        public void StartScale()
        {
        }
    }
}

