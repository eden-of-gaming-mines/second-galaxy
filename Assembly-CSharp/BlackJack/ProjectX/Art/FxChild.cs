﻿namespace BlackJack.ProjectX.Art
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/FxChild")]
    public class FxChild : PrefabResourceContainerBase
    {
        public GameObject m_prefab;
        public bool m_visible = true;
        protected bool m_animationVisible;
        protected GameObject m_childGameObject;
        protected CRIAudioSourceHelperImpl m_audioPlayer;
        public const string PrefabName = "m_prefab";
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnDrawGizmosSelected;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_LateUpdate;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrawGizmosSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

