﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMovementModuleStopSelfShip : SimMovementModuleCruiseBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetMovementModuleType;
        private static DelegateBridge __Hotfix_CheckMoveCmd;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public SimMovementModuleStopSelfShip(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public override MovementModuleType GetMovementModuleType()
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object extraData)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }
    }
}

