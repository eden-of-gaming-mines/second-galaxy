﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMovementModuleCruiseBase : SimMovementModule
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <CurrNeedAccelerate>k__BackingField;
        private Vector3D m_angularVelocity;
        private int m_forceAccelerationState;
        private double? m_forceSpeedValue;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetMovementModuleType;
        private static DelegateBridge __Hotfix_CheckMoveCmd;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_SetAccelerationState;
        private static DelegateBridge __Hotfix_SetForceSpeedValue;
        private static DelegateBridge __Hotfix_UnsetForceSpeedValue;
        private static DelegateBridge __Hotfix_CleanAccelerateState;
        private static DelegateBridge __Hotfix_UpdateShipKinematicState;
        private static DelegateBridge __Hotfix_GetShipRotAngleValue;
        private static DelegateBridge __Hotfix_GetShipMovingSpeed;
        private static DelegateBridge __Hotfix_CalcDistanceAndTimeDecreaseToLowerSpeed;
        private static DelegateBridge __Hotfix_CalcAngularVelocity;
        private static DelegateBridge __Hotfix_CalcMaxRotatingAngleSpeedValueByRemainingAngle;
        private static DelegateBridge __Hotfix_GetRotatingAcceleratingAngleSpeedValue;
        private static DelegateBridge __Hotfix_GetMinRotatingAngleSpeed;
        private static DelegateBridge __Hotfix_GetMaxRotatingAngleSpeed;
        private static DelegateBridge __Hotfix_get_CurrNeedAccelerate;
        private static DelegateBridge __Hotfix_set_CurrNeedAccelerate;

        [MethodImpl(0x8000)]
        public SimMovementModuleCruiseBase(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        [MethodImpl(0x8000)]
        private void CalcAngularVelocity(double remainingAngle, Quaternion deltaRotation, uint deltaMilliseconds)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcDistanceAndTimeDecreaseToLowerSpeed(out double distance, out double time, double highSpeed, double lowSpeed)
        {
        }

        [MethodImpl(0x8000)]
        private void CalcMaxRotatingAngleSpeedValueByRemainingAngle(Vector3D localDirUnitVec, out double maxAngleSpeedForXAxis, out double maxAngleSpeedForYAxis)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CleanAccelerateState()
        {
        }

        [MethodImpl(0x8000)]
        private double GetMaxRotatingAngleSpeed(double remainingAngle)
        {
        }

        [MethodImpl(0x8000)]
        private double GetMinRotatingAngleSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public override MovementModuleType GetMovementModuleType()
        {
        }

        [MethodImpl(0x8000)]
        private double GetRotatingAcceleratingAngleSpeedValue()
        {
        }

        [MethodImpl(0x8000)]
        private double GetShipMovingSpeed(Vector3D globalDirUnitVec, uint deltaMilliseconds)
        {
        }

        [MethodImpl(0x8000)]
        private double GetShipRotAngleValue(Quaternion deltaRotation)
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object extraData)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetAccelerationState(int state)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetForceSpeedValue(double speedValue)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnsetForceSpeedValue()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShipKinematicState(Vector3D globalDirVec, uint deltaMilliseconds)
        {
        }

        public bool CurrNeedAccelerate
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }
    }
}

