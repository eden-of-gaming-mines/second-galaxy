﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMovementModuleHoldOffsetToTarget : SimMovementModuleCruiseBase
    {
        private uint m_targetObjId;
        private Vector3D m_targetPos;
        private Vector3D m_holdOffset;
        private Vector3D m_currMoveDirVec;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetMovementModuleType;
        private static DelegateBridge __Hotfix_CheckMoveCmd;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_HasArrivedDestPos;
        private static DelegateBridge __Hotfix_GetRelativeObjId;

        [MethodImpl(0x8000)]
        public SimMovementModuleHoldOffsetToTarget(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public override MovementModuleType GetMovementModuleType()
        {
        }

        [MethodImpl(0x8000)]
        public override uint GetRelativeObjId()
        {
        }

        [MethodImpl(0x8000)]
        private bool HasArrivedDestPos(Vector3D destPos)
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object extraData)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }
    }
}

