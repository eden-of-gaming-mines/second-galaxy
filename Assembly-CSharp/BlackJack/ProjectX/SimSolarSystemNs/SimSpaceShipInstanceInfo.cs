﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SimSpaceShipInstanceInfo
    {
        public Vector3D m_location;
        public Quaternion m_rotation;
        private static DelegateBridge _c__Hotfix_ctor_2;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public SimSpaceShipInstanceInfo(Vector3D location, Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceShipInstanceInfo(Vector3D location, Vector3D rotation)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceShipInstanceInfo(double locationX, double locationY, double locationZ, double rotationX, double rotationY, double rotationZ)
        {
        }
    }
}

