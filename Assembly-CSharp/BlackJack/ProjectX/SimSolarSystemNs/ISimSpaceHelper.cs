﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using System;

    public interface ISimSpaceHelper
    {
        uint GetCurrTickTime();
        T GetObjectByGDBId<T>(int gdbConfId) where T: SimSpaceObject;
        T GetObjectById<T>(uint id) where T: SimSpaceObject;
        Vector3D GetRandomStandaloneLocation4SpaceObject();
        uint GetTickFrameSize();
        uint GetTickSeq();
        double NextDouble();
    }
}

