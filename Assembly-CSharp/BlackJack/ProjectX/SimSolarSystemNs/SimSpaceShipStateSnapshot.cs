﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using IL;
    using System;

    [Serializable]
    public class SimSpaceShipStateSnapshot : SimMoveableSpaceObjectStateSnapshot
    {
        public SimSpaceShip.MoveCmd m_currMoveCmd;
        public SimMovementModuleArroundTargetSyncData m_arroundTargetSyncData;
        public SimMovementModuleJumpingSyncData m_jumpingSyncData;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

