﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using System;

    public interface ISimSpaceShipEventListener : ISimSpaceObjectEventListener
    {
        void OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId);
        void OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId);
        void OnEnterHighSpeedJumping();
        void OnHoldMoveStateEnd();
        void OnJumpPrepareEnd();
        void OnLeaveHighSpeedJumping();
        void OnMoveCmdComplete(SimSpaceShip.MoveCmd.CmdType cmdType, SimSpaceShip.MoveCmd moveCmd);
        void OnStartJumping(Vector3D dest);
        void OnStartJumpPrepare();
    }
}

