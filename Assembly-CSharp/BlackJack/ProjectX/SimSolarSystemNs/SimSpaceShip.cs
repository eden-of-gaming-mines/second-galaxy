﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ToolUtil;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimSpaceShip : SimMoveableSpaceObject, ISimMovementModuleEventListener
    {
        protected ConfigDataSpaceShipInfo m_confInfo;
        protected ConfigDataSpaceShip3DInfo m_conf3DInfo;
        protected Dictionary<int, SimMovementModule> m_movementModuleDict;
        public MoveCmd m_lastSetMoveCmd;
        public ISimMovemnetParamProvider m_paramProvider;
        protected SimShipFsm m_fsm;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitPoseByPoseRequest;
        private static DelegateBridge __Hotfix_SetPoseByLeaveStation;
        private static DelegateBridge __Hotfix_GetLeaveStationDurationTime;
        private static DelegateBridge __Hotfix_SetPoseByStarGateId;
        private static DelegateBridge __Hotfix_GetLeaveStarGateDurationTime;
        private static DelegateBridge __Hotfix_InitAllMovementModule;
        private static DelegateBridge __Hotfix_SwitchMovementModule;
        private static DelegateBridge __Hotfix_SetMoveCmd;
        private static DelegateBridge __Hotfix_GetLastMoveCmd;
        private static DelegateBridge __Hotfix_TakeSnapShotForViewSync;
        private static DelegateBridge __Hotfix_SyncMoveState;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsJumping;
        private static DelegateBridge __Hotfix_IsJumpPrepare;
        private static DelegateBridge __Hotfix_IsHoldMoveState;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetCurrMaxCruiseSpeed;
        private static DelegateBridge __Hotfix_GetCurrMovementModule;
        private static DelegateBridge __Hotfix_IsAccelerateCloseToTarget;
        private static DelegateBridge __Hotfix_IsAccelerateAwayFromTarget;
        private static DelegateBridge __Hotfix_IsNeedCalcSmoothLocation;
        private static DelegateBridge __Hotfix_IsNeedCalcSmoothRotation;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_ContinueJumping;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovementModuleEventListener.OnCurrMovementCompleted;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovementModuleEventListener.OnCrossNotifySpeed;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovementModuleEventListener.OnArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovementModuleEventListener.OnArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_get_ShipEventListener;

        [MethodImpl(0x8000)]
        public SimSpaceShip(uint objId, ConfigDataSpaceShipInfo confInfo, ConfigDataSpaceShip3DInfo conf3DInfo, SimSpaceShipInstanceInfo instanceInfo, ShipPoseRequest poseReq, ISimSpaceShipEventListener eventListener, ISimSpaceHelper spaceHelper, ISimMovemnetParamProvider paramProvider)
        {
        }

        [MethodImpl(0x8000)]
        void ISimMovementModuleEventListener.OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        void ISimMovementModuleEventListener.OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        void ISimMovementModuleEventListener.OnCrossNotifySpeed(bool up)
        {
        }

        [MethodImpl(0x8000)]
        void ISimMovementModuleEventListener.OnCurrMovementCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ContinueJumping(bool checkJumpPrepareEnd = true)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrMaxCruiseSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModule GetCurrMovementModule()
        {
        }

        [MethodImpl(0x8000)]
        public MoveCmd GetLastMoveCmd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual uint GetLeaveStarGateDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual uint GetLeaveStationDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitAllMovementModule()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPoseByPoseRequest(ShipPoseRequest poseReq)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAccelerateAwayFromTarget()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAccelerateCloseToTarget()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHoldMoveState()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJumping()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedCalcSmoothLocation(uint deltaMillisecond)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedCalcSmoothRotation()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMoveCmd(MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool SetPoseByLeaveStation(uint stationObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool SetPoseByStarGateId(uint starGateObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected void SwitchMovementModule(MoveCmd.CmdType type, Vector3D vectorParam, uint targetObjId, uint radiusParam, object syncData = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncMoveState(SimSpaceShipStateSnapshot snapshot, uint snapshotTime)
        {
        }

        [MethodImpl(0x8000)]
        public override SimMoveableSpaceObjectStateSnapshot TakeSnapShotForViewSync(SimMoveableSpaceObjectStateSnapshot inputSnapshot = null, bool isForEnterView = true)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        protected ISimSpaceShipEventListener ShipEventListener
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected interface ISimShipFsmEventHandler : ISimMovementModuleEventListener
        {
            bool OnSetMoveCmd(SimSpaceShip.MoveCmd cmd, object syncData = null);
            void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime);
        }

        [Serializable]
        protected class JumpContext
        {
            public Vector3D m_destation;
            public bool m_startJump;
            private static DelegateBridge _c__Hotfix_ctor;

            public JumpContext()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct MoveCmd
        {
            public CmdType m_type;
            public Vector3D m_vectorParam;
            public uint m_targetObjId;
            public uint m_radiusParam;
            public bool m_internal;
            public bool IsNoneCmdType() => 
                (this.m_type == CmdType.None);

            [MethodImpl(0x8000)]
            public bool IsSameCmd(SimSpaceShip.MoveCmd cmd)
            {
            }

            [MethodImpl(0x8000)]
            public void Clear()
            {
            }
            public enum CmdType
            {
                None,
                MoveToDirection,
                MoveToLocation,
                MoveToLocationAndStop,
                MoveCloseToTarget,
                MoveAwayFromTarget,
                MoveArroundTarget,
                MoveArroundLocation,
                HoldDistanceToTarget,
                HoldOffsetToTarget,
                MoveToEnterStation,
                MoveToUseStarGate,
                JumpToLocation,
                Stop,
                TypeMax
            }
        }

        [Serializable]
        protected class SimShipFsm : ActorFsm, SimSpaceShip.ISimShipFsmEventHandler, ISimMovementModuleEventListener
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Init;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_OnSetMoveCmd;
            private static DelegateBridge __Hotfix_OnSyncMoveState;
            private static DelegateBridge __Hotfix_OnCurrMovementCompleted;
            private static DelegateBridge __Hotfix_OnCrossNotifySpeed;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToArroundTarget;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;

            public SimShipFsm()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void Init(object param)
            {
                DelegateBridge bridge = __Hotfix_Init;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, param);
                }
                else
                {
                    SimSpaceShip owner = param as SimSpaceShip;
                    base.AddState(new SimSpaceShip.SimShipStateCruise(owner, this));
                    base.AddState(new SimSpaceShip.SimShipStateJumpPrepare(owner, this));
                    base.AddState(new SimSpaceShip.SimShipStateJumpingLowSpeed(owner, this));
                    base.AddState(new SimSpaceShip.SimShipStateJumping(owner, this));
                    base.AddState(new SimSpaceShip.SimShipStateHoldMoveState(owner, this));
                    base.GotoState(1, new object[0]);
                }
            }

            public void OnArroundTargetMovementModuleStartToArroundTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToArroundTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnArroundTargetMovementModuleStartToArroundTarget(radius, targetObjId);
                }
            }

            public void OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnArroundTargetMovementModuleStartToArroundToTarget(radius, targetObjId);
                }
            }

            public void OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnArroundTargetMovementModuleStartToCloseToTarget(radius, targetObjId);
                }
            }

            public void OnCrossNotifySpeed(bool up)
            {
                DelegateBridge bridge = __Hotfix_OnCrossNotifySpeed;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, up);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnCrossNotifySpeed(up);
                }
            }

            public void OnCurrMovementCompleted()
            {
                DelegateBridge bridge = __Hotfix_OnCurrMovementCompleted;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnCurrMovementCompleted();
                }
            }

            public bool OnSetMoveCmd(SimSpaceShip.MoveCmd cmd, object syncData = null)
            {
                DelegateBridge bridge = __Hotfix_OnSetMoveCmd;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp3734(this, cmd, syncData);
                }
                return base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnSetMoveCmd(cmd, syncData);
            }

            public void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
                else
                {
                    base.GetCurrState<SimSpaceShip.SimShipStateBase>().OnSyncMoveState(snapshot, serverSnapshotTime);
                }
            }

            public void Tick(uint deltaMillisecond)
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp197(this, deltaMillisecond);
                }
                else
                {
                    base.m_currState.OnTick(deltaMillisecond);
                }
            }
        }

        [Serializable]
        protected abstract class SimShipStateBase : ActorFsmState, SimSpaceShip.ISimShipFsmEventHandler, ISimMovementModuleEventListener
        {
            protected SimSpaceShip m_owner;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnSetMoveCmd;
            private static DelegateBridge __Hotfix_OnSyncMoveState;
            private static DelegateBridge __Hotfix_OnCurrMovementCompleted;
            private static DelegateBridge __Hotfix_OnCrossNotifySpeed;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToArroundTarget;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;

            public SimShipStateBase(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(fsm)
            {
                this.m_owner = owner;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public virtual void OnArroundTargetMovementModuleStartToArroundTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToArroundTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
            }

            public virtual void OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
            }

            public virtual void OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
            }

            public virtual void OnCrossNotifySpeed(bool up)
            {
                DelegateBridge bridge = __Hotfix_OnCrossNotifySpeed;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, up);
                }
            }

            public virtual void OnCurrMovementCompleted()
            {
                DelegateBridge bridge = __Hotfix_OnCurrMovementCompleted;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public virtual bool OnSetMoveCmd(SimSpaceShip.MoveCmd cmd, object syncData = null)
            {
                DelegateBridge bridge = __Hotfix_OnSetMoveCmd;
                return ((bridge != null) && bridge.__Gen_Delegate_Imp3734(this, cmd, syncData));
            }

            public virtual void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
            }
        }

        [Serializable]
        protected class SimShipStateCruise : SimSpaceShip.SimShipStateBase
        {
            protected SimSpaceShip.MoveCmd m_currMoveCmd;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnter;
            private static DelegateBridge __Hotfix_OnLeave;
            private static DelegateBridge __Hotfix_OnSetMoveCmd;
            private static DelegateBridge __Hotfix_OnCurrMovementCompleted;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
            private static DelegateBridge __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;
            private static DelegateBridge __Hotfix_OnSyncMoveState;

            public SimShipStateCruise(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(owner, fsm)
            {
                base.m_stateId = 1;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public override void OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToArroundToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
                else if (!this.m_currMoveCmd.m_internal)
                {
                    base.m_owner.ShipEventListener.OnArroundTargetMovementModuleStartToArroundToTarget(radius, targetObjId);
                }
            }

            public override void OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
            {
                DelegateBridge bridge = __Hotfix_OnArroundTargetMovementModuleStartToCloseToTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1487(this, radius, targetObjId);
                }
                else if (!this.m_currMoveCmd.m_internal)
                {
                    base.m_owner.ShipEventListener.OnArroundTargetMovementModuleStartToCloseToTarget(radius, targetObjId);
                }
            }

            public override void OnCurrMovementCompleted()
            {
                DelegateBridge bridge = __Hotfix_OnCurrMovementCompleted;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else if (!this.m_currMoveCmd.m_internal)
                {
                    base.m_owner.ShipEventListener.OnMoveCmdComplete(this.m_currMoveCmd.m_type, this.m_currMoveCmd);
                }
            }

            public override void OnEnter(params object[] onEnterParams)
            {
                DelegateBridge bridge = __Hotfix_OnEnter;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1871(this, onEnterParams);
                }
                else
                {
                    SimSpaceShip.MoveCmd cmd1;
                    if ((onEnterParams != null) && (onEnterParams.Length != 0))
                    {
                        cmd1 = (SimSpaceShip.MoveCmd) onEnterParams[0];
                    }
                    else
                    {
                        cmd1 = new SimSpaceShip.MoveCmd();
                    }
                    SimSpaceShip.MoveCmd cmd = cmd1;
                    if (!cmd.IsNoneCmdType())
                    {
                        base.m_owner.SetMoveCmd(cmd);
                    }
                }
            }

            public override void OnLeave()
            {
                DelegateBridge bridge = __Hotfix_OnLeave;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_currMoveCmd.Clear();
                }
            }

            public override bool OnSetMoveCmd(SimSpaceShip.MoveCmd cmd, object syncData = null)
            {
                DelegateBridge bridge = __Hotfix_OnSetMoveCmd;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp3734(this, cmd, syncData);
                }
                if (!base.m_owner.m_movementModuleDict[(int) cmd.m_type].CheckMoveCmd(cmd))
                {
                    object[] args = new object[] { base.m_owner.m_spaceObjId, cmd.m_type, cmd.m_targetObjId };
                    CommonLogHelper.LogImp.DebugLog(true, "SimShipStateCruise::OnSetMoveCmd check moveCmd erro ownerObjId={0} cmdType={1}, cmdTargetId={2}", args);
                    return false;
                }
                switch (cmd.m_type)
                {
                    case SimSpaceShip.MoveCmd.CmdType.MoveToDirection:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToLocation:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToLocationAndStop:
                    case SimSpaceShip.MoveCmd.CmdType.MoveCloseToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveAwayFromTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveArroundTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveArroundLocation:
                    case SimSpaceShip.MoveCmd.CmdType.HoldDistanceToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.HoldOffsetToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToEnterStation:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToUseStarGate:
                    case SimSpaceShip.MoveCmd.CmdType.Stop:
                        this.m_currMoveCmd = cmd;
                        base.m_owner.SwitchMovementModule(cmd.m_type, cmd.m_vectorParam, cmd.m_targetObjId, cmd.m_radiusParam, syncData);
                        return true;

                    case SimSpaceShip.MoveCmd.CmdType.JumpToLocation:
                    {
                        object[] newStateParmas = new object[] { cmd };
                        base.m_fsm.GotoState(2, newStateParmas);
                        return true;
                    }
                }
                return false;
            }

            public override void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
                else
                {
                    if (!snapshot.m_currMoveCmd.IsNoneCmdType())
                    {
                        if (snapshot.m_jumpingSyncData != null)
                        {
                            if (snapshot.m_jumpingSyncData.m_isEnterView)
                            {
                                object[] newStateParmas = new object[] { snapshot.m_currMoveCmd.m_vectorParam, snapshot.m_jumpingSyncData };
                                base.m_fsm.GotoState(3, newStateParmas);
                            }
                        }
                        else if (snapshot.m_arroundTargetSyncData != null)
                        {
                            this.OnSetMoveCmd(snapshot.m_currMoveCmd, snapshot.m_arroundTargetSyncData);
                        }
                        else
                        {
                            this.OnSetMoveCmd(snapshot.m_currMoveCmd, null);
                        }
                    }
                    base.m_owner.SetLocation(snapshot.m_location);
                    base.m_owner.SetRotation(Quaternion.Euler(snapshot.m_rotation));
                    base.m_owner.SetVelocity((Vector3D) (base.m_owner.Rotation * Vector3D.forward), (float) snapshot.m_speedValue);
                }
            }
        }

        [Serializable]
        protected class SimShipStateHoldMoveState : SimSpaceShip.SimShipStateBase
        {
            protected uint m_durationTime;
            protected uint m_tickDeltaTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnter;
            private static DelegateBridge __Hotfix_OnLeave;
            private static DelegateBridge __Hotfix_OnTick;

            public SimShipStateHoldMoveState(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(owner, fsm)
            {
                base.m_stateId = 5;
                this.m_tickDeltaTime = 0;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public override void OnEnter(params object[] onEnterParams)
            {
                DelegateBridge bridge = __Hotfix_OnEnter;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1871(this, onEnterParams);
                }
                else
                {
                    int num1;
                    if ((onEnterParams == null) || (onEnterParams.Length == 0))
                    {
                        num1 = 0;
                    }
                    else
                    {
                        num1 = (int) ((uint) onEnterParams[0]);
                    }
                    this.m_durationTime = (uint) num1;
                }
            }

            public override void OnLeave()
            {
                DelegateBridge bridge = __Hotfix_OnLeave;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.m_owner.ShipEventListener.OnHoldMoveStateEnd();
                }
            }

            public override void OnTick(uint deltaMillisecond)
            {
                DelegateBridge bridge = __Hotfix_OnTick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp197(this, deltaMillisecond);
                }
                else
                {
                    this.m_tickDeltaTime += deltaMillisecond;
                    if (this.m_tickDeltaTime >= this.m_durationTime)
                    {
                        SimSpaceShip.MoveCmd cmd = new SimSpaceShip.MoveCmd {
                            m_type = SimSpaceShip.MoveCmd.CmdType.MoveToDirection,
                            m_vectorParam = (Vector3D) base.m_owner.GetVelocity(),
                            m_internal = true
                        };
                        object[] newStateParmas = new object[] { cmd };
                        base.m_fsm.GotoState(1, newStateParmas);
                    }
                }
            }
        }

        protected enum SimShipStateId
        {
            Cruise = 1,
            JumpPrepare = 2,
            JumpingLowSpeed = 3,
            Jumping = 4,
            HoldMoveState = 5
        }

        [Serializable]
        protected class SimShipStateJumping : SimSpaceShip.SimShipStateBase
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnter;
            private static DelegateBridge __Hotfix_OnLeave;
            private static DelegateBridge __Hotfix_OnCrossNotifySpeed;
            private static DelegateBridge __Hotfix_OnSyncMoveState;

            public SimShipStateJumping(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(owner, fsm)
            {
                base.m_stateId = 4;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public override void OnCrossNotifySpeed(bool up)
            {
                DelegateBridge bridge = __Hotfix_OnCrossNotifySpeed;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, up);
                }
                else if (!up)
                {
                    base.m_fsm.GotoState(3, new object[0]);
                }
            }

            public override void OnEnter(params object[] onEnterParams)
            {
                DelegateBridge bridge = __Hotfix_OnEnter;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1871(this, onEnterParams);
                }
                else
                {
                    base.m_owner.ShipEventListener.OnEnterHighSpeedJumping();
                }
            }

            public override void OnLeave()
            {
                DelegateBridge bridge = __Hotfix_OnLeave;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.m_owner.ShipEventListener.OnLeaveHighSpeedJumping();
                }
            }

            public override void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
            }
        }

        [Serializable]
        protected class SimShipStateJumpingLowSpeed : SimSpaceShip.SimShipStateBase
        {
            protected Vector3D m_jumpDestPos;
            protected SimSpaceShip.MoveCmd m_currMoveCmd;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnter;
            private static DelegateBridge __Hotfix_OnLeave;
            private static DelegateBridge __Hotfix_OnCurrMovementCompleted;
            private static DelegateBridge __Hotfix_OnCrossNotifySpeed;
            private static DelegateBridge __Hotfix_OnSyncMoveState;

            public SimShipStateJumpingLowSpeed(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(owner, fsm)
            {
                base.m_stateId = 3;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public override void OnCrossNotifySpeed(bool up)
            {
                DelegateBridge bridge = __Hotfix_OnCrossNotifySpeed;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, up);
                }
                else if (up)
                {
                    object[] newStateParmas = new object[] { this.m_jumpDestPos };
                    base.m_fsm.GotoState(4, newStateParmas);
                }
            }

            public override void OnCurrMovementCompleted()
            {
                DelegateBridge bridge = __Hotfix_OnCurrMovementCompleted;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    SimSpaceShip.MoveCmd cmd = new SimSpaceShip.MoveCmd {
                        m_type = SimSpaceShip.MoveCmd.CmdType.MoveToDirection,
                        m_vectorParam = (Vector3D) base.m_owner.GetVelocity(),
                        m_internal = true
                    };
                    object[] newStateParmas = new object[] { cmd };
                    base.m_fsm.GotoState(1, newStateParmas);
                    base.m_owner.ShipEventListener.OnMoveCmdComplete(SimSpaceShip.MoveCmd.CmdType.JumpToLocation, this.m_currMoveCmd);
                }
            }

            public override void OnEnter(params object[] onEnterParams)
            {
                DelegateBridge bridge = __Hotfix_OnEnter;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1871(this, onEnterParams);
                }
                else
                {
                    if (onEnterParams.Length == 0)
                    {
                    }
                    if (onEnterParams.Length != 1)
                    {
                        if (onEnterParams.Length == 2)
                        {
                            this.m_jumpDestPos = (Vector3D) onEnterParams[0];
                            SimMovementModuleJumpingSyncData syncData = onEnterParams[1] as SimMovementModuleJumpingSyncData;
                            base.m_owner.SwitchMovementModule(SimSpaceShip.MoveCmd.CmdType.JumpToLocation, this.m_jumpDestPos, 0, 0, syncData);
                            base.m_owner.m_movementModule.SetNotifySpeed(1000000.0);
                        }
                    }
                    else
                    {
                        this.m_currMoveCmd = (SimSpaceShip.MoveCmd) onEnterParams[0];
                        this.m_jumpDestPos = this.m_currMoveCmd.m_vectorParam;
                        object[] args = new object[] { base.m_owner.m_spaceObjId, this.m_jumpDestPos.ToString() };
                        CommonLogHelper.LogImp.DebugLog(false, "SimShipStateJumpingLowSpeed::OnEnter objId={0} destPos={1}", args);
                        base.m_owner.SwitchMovementModule(SimSpaceShip.MoveCmd.CmdType.JumpToLocation, this.m_jumpDestPos, 0, 0, null);
                        base.m_owner.m_movementModule.SetNotifySpeed(1000000.0);
                    }
                }
            }

            public override void OnLeave()
            {
                DelegateBridge bridge = __Hotfix_OnLeave;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
            }
        }

        [Serializable]
        protected class SimShipStateJumpPrepare : SimSpaceShip.SimShipStateBase
        {
            protected SimSpaceShip.MoveCmd m_currMoveCmd;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private bool <IsJumpPrepareEnd>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnter;
            private static DelegateBridge __Hotfix_OnLeave;
            private static DelegateBridge __Hotfix_OnSetMoveCmd;
            private static DelegateBridge __Hotfix_OnCurrMovementCompleted;
            private static DelegateBridge __Hotfix_OnSyncMoveState;
            private static DelegateBridge __Hotfix_GetCurrMoveCmd;
            private static DelegateBridge __Hotfix_get_IsJumpPrepareEnd;
            private static DelegateBridge __Hotfix_set_IsJumpPrepareEnd;

            public SimShipStateJumpPrepare(SimSpaceShip owner, SimSpaceShip.SimShipFsm fsm) : base(owner, fsm)
            {
                base.m_stateId = 2;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, owner, fsm);
                }
            }

            public SimSpaceShip.MoveCmd GetCurrMoveCmd()
            {
                DelegateBridge bridge = __Hotfix_GetCurrMoveCmd;
                return ((bridge == null) ? this.m_currMoveCmd : bridge.__Gen_Delegate_Imp1506(this));
            }

            public override void OnCurrMovementCompleted()
            {
                DelegateBridge bridge = __Hotfix_OnCurrMovementCompleted;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.IsJumpPrepareEnd = true;
                    base.m_owner.ShipEventListener.OnJumpPrepareEnd();
                }
            }

            public override void OnEnter(params object[] onEnterParams)
            {
                DelegateBridge bridge = __Hotfix_OnEnter;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1871(this, onEnterParams);
                }
                else
                {
                    this.m_currMoveCmd = (SimSpaceShip.MoveCmd) onEnterParams[0];
                    Vector3D normalized = (this.m_currMoveCmd.m_vectorParam - base.m_owner.m_location).normalized;
                    base.m_owner.SwitchMovementModule(SimSpaceShip.MoveCmd.CmdType.MoveToDirection, normalized, 0, 0, true);
                    this.IsJumpPrepareEnd = false;
                    base.m_owner.ShipEventListener.OnStartJumpPrepare();
                }
            }

            public override void OnLeave()
            {
                DelegateBridge bridge = __Hotfix_OnLeave;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override bool OnSetMoveCmd(SimSpaceShip.MoveCmd cmd, object syncData = null)
            {
                DelegateBridge bridge = __Hotfix_OnSetMoveCmd;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp3734(this, cmd, syncData);
                }
                if (!base.m_owner.m_movementModuleDict[(int) cmd.m_type].CheckMoveCmd(cmd))
                {
                    object[] args = new object[] { base.m_owner.m_spaceObjId, cmd.m_type };
                    CommonLogHelper.LogImp.ErrorLog(true, "SimShipStateJumpPrepare::OnSetMoveCmd check moveCmd erro objId={0} cmdType={1}", args);
                    return false;
                }
                switch (cmd.m_type)
                {
                    case SimSpaceShip.MoveCmd.CmdType.MoveToDirection:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToLocation:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToLocationAndStop:
                    case SimSpaceShip.MoveCmd.CmdType.MoveCloseToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveAwayFromTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveArroundTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveArroundLocation:
                    case SimSpaceShip.MoveCmd.CmdType.HoldDistanceToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.HoldOffsetToTarget:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToEnterStation:
                    case SimSpaceShip.MoveCmd.CmdType.MoveToUseStarGate:
                    case SimSpaceShip.MoveCmd.CmdType.Stop:
                    {
                        object[] newStateParmas = new object[] { cmd };
                        base.m_fsm.GotoState(1, newStateParmas);
                        return true;
                    }
                }
                return false;
            }

            public override void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint serverSnapshotTime)
            {
                DelegateBridge bridge = __Hotfix_OnSyncMoveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp277(this, snapshot, serverSnapshotTime);
                }
                else
                {
                    this.OnSetMoveCmd(snapshot.m_currMoveCmd, null);
                    if (snapshot.m_jumpingSyncData != null)
                    {
                        base.m_owner.SetLocation(snapshot.m_location);
                        base.m_owner.SetRotation(Quaternion.Euler(snapshot.m_rotation));
                        base.m_owner.SetVelocity((Vector3D) (base.m_owner.Rotation * Vector3D.forward), (float) snapshot.m_speedValue);
                    }
                }
            }

            public bool IsJumpPrepareEnd
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_IsJumpPrepareEnd;
                    return ((bridge == null) ? this.<IsJumpPrepareEnd>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                protected set
                {
                    DelegateBridge bridge = __Hotfix_set_IsJumpPrepareEnd;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<IsJumpPrepareEnd>k__BackingField = value;
                    }
                }
            }
        }
    }
}

