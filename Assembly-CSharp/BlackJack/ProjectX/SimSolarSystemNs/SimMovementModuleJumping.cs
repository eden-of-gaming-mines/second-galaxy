﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMovementModuleJumping : SimMovementModule
    {
        protected uint m_jumpStartTime;
        protected Vector3D m_jumpStartShipPos;
        protected Vector3D m_jumpDestationPos;
        protected double m_lastTickSpeed;
        private double m_jumpAccelerationParam;
        private double m_jumpMaxSpeed;
        private double m_maxSpeedForCurrJumping;
        private double m_jumpModeChangedSpeed;
        private double m_jumpDistance;
        private double m_jumpDistanceFromZeroIncreaseToMax;
        private double m_timespanForKeepingMaxSpeed;
        private double m_jumpDistanceForKeepingMaxSpeed;
        private double m_timespanFromSpeedIncreaseToMaxForCurrJumping;
        private double m_jumpDistanceFromZeroToMaxForCurrJumping;
        private double m_jumpDistanceFromMaxToModeChangedSpeed;
        private double m_jumpDistanceFromMaxToModeChangedSpeedCurrJumping;
        private double m_timespanFromSpeedMaxToModeChangedSpeed;
        private double m_jumpDistanceFromZeroToModeChangedSpeed;
        private double m_jumpDistanceFromModeChangedSpeedToZero;
        private double m_timespanFromModeChangedSpeedToZero;
        private uint m_timespanForCurrJumpActionMs;
        private const double m_jumpModeCalculateParam = 1000.0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectArroundTargetSyncData;
        private static DelegateBridge __Hotfix_GetMovementModuleType;
        private static DelegateBridge __Hotfix_CheckMoveCmd;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CalcJumpingKinematicParams;
        private static DelegateBridge __Hotfix_get_m_totalTimespanForJump;

        [MethodImpl(0x8000)]
        public SimMovementModuleJumping(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcJumpingKinematicParams(double jumpTimespan, out double currDistance, out double speed, out bool jumpFinished)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModuleJumpingSyncData CollectArroundTargetSyncData()
        {
        }

        [MethodImpl(0x8000)]
        public override MovementModuleType GetMovementModuleType()
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object extraData)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        private double m_totalTimespanForJump
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

