﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using System;

    public interface ISimMovemnetParamProvider
    {
        double GetDefaultMaxCruiseSpeed();
        double GetInertiaParam();
        double GetJumpAccelerationParam();
        double GetJumpMaxSpeed();
        double GetMaxCruiseSpeed();
        bool IsShipMoveDisabled();
    }
}

