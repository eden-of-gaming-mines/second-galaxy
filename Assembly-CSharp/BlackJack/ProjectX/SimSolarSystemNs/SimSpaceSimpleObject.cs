﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SimSpaceSimpleObject : SimMoveableSpaceObject
    {
        protected int m_confId;
        private CollisionInfo m_collision;
        protected SimColliderModule m_colliderModule;
        protected float m_scale;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_Collision;
        private static DelegateBridge __Hotfix_get_ColliderModule;
        private static DelegateBridge __Hotfix_get_Scale;

        [MethodImpl(0x8000)]
        public SimSpaceSimpleObject(uint objId, CollisionInfo collision, float scale, ISimSpaceObjectEventListener eventListener, ISimSpaceHelper spaceHelper, Vector3D location, Vector3D rotation)
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CollisionInfo Collision
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimColliderModule ColliderModule
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Scale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

