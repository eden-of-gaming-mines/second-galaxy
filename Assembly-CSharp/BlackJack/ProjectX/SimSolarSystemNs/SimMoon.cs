﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SimMoon : SimSpaceObject
    {
        private int m_confId;
        private GDBMoonInfo m_info;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcJumpDestLocation;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;

        [MethodImpl(0x8000)]
        public SimMoon(uint objId, GDBMoonInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D CalcJumpDestLocation(Vector3D currLocation, int targetKeepDistance)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBMoonInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

