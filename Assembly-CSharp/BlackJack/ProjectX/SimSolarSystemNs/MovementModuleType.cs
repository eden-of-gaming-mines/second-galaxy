﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using System;

    [Serializable]
    public enum MovementModuleType
    {
        ToDirection,
        ToDirection4JumpPrepare,
        ToLocation,
        ToLocationAndStop,
        ToEnterStation,
        ToUseStarGate,
        ArroundTarget,
        ArroundLocation,
        AwayFromTarget,
        HoldDistanceToTarget,
        HoldOffsetToTarget,
        StopSelfShip,
        CloseToTarget,
        Jumping
    }
}

