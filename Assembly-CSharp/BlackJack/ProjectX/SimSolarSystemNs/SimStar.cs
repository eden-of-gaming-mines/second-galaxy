﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [Serializable]
    public class SimStar : SimSpaceObject
    {
        private int m_confId;
        [NonSerialized]
        private GDBStarInfo m_info;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnDeserialized;
        private static DelegateBridge __Hotfix_CalcJumpDestLocation;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;

        [MethodImpl(0x8000)]
        public SimStar(uint objId, GDBStarInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D CalcJumpDestLocation(Vector3D currLocation, int targetKeepDistance)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000), OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBStarInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

