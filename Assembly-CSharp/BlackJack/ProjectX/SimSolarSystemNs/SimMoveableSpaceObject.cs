﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMoveableSpaceObject : SimSpaceObject
    {
        protected SimMovementModule m_movementModule;
        public static bool IsNeedSmoothLocation = true;
        private const float m_smoothLocationDeltaTimeRatio = 0.002f;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_SetVelocity_1;
        private static DelegateBridge __Hotfix_SetVelocity_0;
        private static DelegateBridge __Hotfix_GetVelocity;
        private static DelegateBridge __Hotfix_ResetMovementModule;
        private static DelegateBridge __Hotfix_TakeSnapShotForViewSync;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateSmoothLocationAndRotation;
        private static DelegateBridge __Hotfix_IsNeedCalcSmoothLocation;
        private static DelegateBridge __Hotfix_IsNeedCalcSmoothRotation;
        private static DelegateBridge __Hotfix_get_MovementModule;
        private static DelegateBridge __Hotfix_set_MovementModule;

        [MethodImpl(0x8000)]
        public SimMoveableSpaceObject(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public SimMoveableSpaceObject(uint objId, ISimSpaceObjectEventListener eventListener, ISimSpaceHelper spaceHelper)
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D GetVelocity()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedCalcSmoothLocation(uint deltaMillisecond)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedCalcSmoothRotation()
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetMovementModule(SimMovementModule destMovementModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object syncData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVelocity(Vector4D velocity)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVelocity(Vector3D direction, float velocityOutPut)
        {
        }

        [MethodImpl(0x8000)]
        public virtual SimMoveableSpaceObjectStateSnapshot TakeSnapShotForViewSync(SimMoveableSpaceObjectStateSnapshot inputSnapshot = null, bool isForEnterView = true)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateSmoothLocationAndRotation(uint deltaMillisecond)
        {
        }

        public SimMovementModule MovementModule
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

