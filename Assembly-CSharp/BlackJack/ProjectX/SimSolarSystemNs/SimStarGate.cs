﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimStarGate : SimSpaceObject
    {
        private int m_confId;
        private GDBStargateInfo m_confInfo;
        private ConfigDataStargate3DInfo m_conf3DInfo;
        private ConfigDataStarGateInfo m_templateInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcJumpDestLocation;
        private static DelegateBridge __Hotfix_CalcShipLeaveStargateInfo;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_TemplateInfo;

        [MethodImpl(0x8000)]
        public SimStarGate(uint objId, GDBStargateInfo info, ConfigDataStargate3DInfo info3D, ConfigDataStarGateInfo templateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D CalcJumpDestLocation(Vector3D currLocation, ConfigDataStarGateInfo starGateTemplateConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcShipLeaveStargateInfo(SimSpaceShip targetShip, out Vector3D leavePos, out Quaternion leaveDirRotation)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBStargateInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataStargate3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataStarGateInfo TemplateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

