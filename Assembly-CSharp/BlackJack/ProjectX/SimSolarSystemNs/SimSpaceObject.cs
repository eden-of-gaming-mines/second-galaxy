﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class SimSpaceObject
    {
        protected uint m_spaceObjId;
        protected Vector3D m_location;
        protected Vector3D m_smoothLocation;
        protected Quaternion m_rotation;
        protected Quaternion m_smoothRotation;
        protected bool m_isStatic;
        protected uint m_tickSeq4SimStatChange;
        protected ISimSpaceObjectEventListener m_eventListener;
        protected ISimSpaceHelper m_spaceHelper;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_SetLocation;
        private static DelegateBridge __Hotfix_SetRotation;
        private static DelegateBridge __Hotfix_UpdateSimStatVersion;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_DistanceTo_0;
        private static DelegateBridge __Hotfix_DistanceTo_1;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_SpaceObjId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_get_SmoothLocation;
        private static DelegateBridge __Hotfix_get_Rotation;
        private static DelegateBridge __Hotfix_get_SmoothRotation;
        private static DelegateBridge __Hotfix_get_IsStatic;
        private static DelegateBridge __Hotfix_get_TickSeq4SimStateChange;
        private static DelegateBridge __Hotfix_get_SpaceHelper;

        [MethodImpl(0x8000)]
        public SimSpaceObject(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceObject(uint objId, ISimSpaceObjectEventListener eventListener, ISimSpaceHelper spaceHelper)
        {
        }

        [MethodImpl(0x8000)]
        public double DistanceTo(SimSpaceObject anotherObj, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public double DistanceTo(Vector3D targetPos, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocation(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRotation(Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint deltaMillisecond)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSimStatVersion()
        {
        }

        public uint SpaceObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector3D Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector3D SmoothLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Quaternion Rotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Quaternion SmoothRotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public uint TickSeq4SimStateChange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ISimSpaceHelper SpaceHelper
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

