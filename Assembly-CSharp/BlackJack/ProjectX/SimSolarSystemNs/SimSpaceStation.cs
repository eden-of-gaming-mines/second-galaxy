﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimSpaceStation : SimSpaceObject
    {
        private int m_confId;
        private GDBSpaceStationInfo m_confInfo;
        private ConfigDataSpaceStation3DInfo m_conf3DInfo;
        private ConfigDataSpaceStationResInfo m_resConfInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcJumpDestLocation;
        private static DelegateBridge __Hotfix_CalcShipLeaveStationInfo;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_TemplateConfInfo;

        [MethodImpl(0x8000)]
        public SimSpaceStation(uint objId, GDBSpaceStationInfo info, ConfigDataSpaceStation3DInfo info3D, ConfigDataSpaceStationResInfo resInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D CalcJumpDestLocation(Vector3D currLocation, ConfigDataSpaceStationResInfo spaceStationTemplateConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcShipLeaveStationInfo(SimSpaceShip targetShip, out Vector3D leavePos, out Quaternion leaveDirRotation)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBSpaceStationInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceStation3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceStationResInfo TemplateConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

