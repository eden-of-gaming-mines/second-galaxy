﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ToolUtil;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    [Serializable]
    public class SimSolarSystem : ISimSpaceHelper
    {
        protected int m_confId;
        protected GDBSolarSystemInfo m_info;
        protected ObjId2ObjMap m_objId2ObjMap;
        protected Dictionary<int, uint> m_mapGDBConfId2ObjId;
        protected uint m_currTickTime;
        protected uint m_lastTickTime;
        protected uint m_tickFrameSize;
        protected uint m_tickSeq;
        protected Random m_random;
        protected Action<SimSpaceObject> m_tickFrameOnSingleSimObj;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_AddSpaceObject_0;
        private static DelegateBridge __Hotfix_AddSpaceObject_1;
        private static DelegateBridge __Hotfix_AddSpaceStation;
        private static DelegateBridge __Hotfix_AddStarGate;
        private static DelegateBridge __Hotfix_AddSpaceShip;
        private static DelegateBridge __Hotfix_RemoveSpaceObject;
        private static DelegateBridge __Hotfix_GetObjectById;
        private static DelegateBridge __Hotfix_GetObjectByGDBId;
        private static DelegateBridge __Hotfix_ForeachSpaceObj;
        private static DelegateBridge __Hotfix_GetRandomStandaloneLocation4SpaceObject;
        private static DelegateBridge __Hotfix_CalcJumpTargetPos;
        private static DelegateBridge __Hotfix_CalcPortalJumpTargetPos;
        private static DelegateBridge __Hotfix_GetTickSeq;
        private static DelegateBridge __Hotfix_SetTickFrameSize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickOneFrame;
        private static DelegateBridge __Hotfix_TickFrameOnSingleSimObj;
        private static DelegateBridge __Hotfix_OnDeserialized;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetObjectById;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetObjectByGDBId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetTickSeq;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetCurrTickTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetTickFrameSize;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.GetRandomStandaloneLocation4SpaceObject;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceHelper.NextDouble;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;

        [MethodImpl(0x8000)]
        public bool AddSpaceObject(SimSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddSpaceObject(SimSpaceObject obj, Vector3D location, Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddSpaceShip(SimSpaceShip ship, Vector3D location, Quaternion rotation, float velocityOutPut)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddSpaceStation(int gdbConfId, SimSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddStarGate(int gdbConfId, SimSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        uint ISimSpaceHelper.GetCurrTickTime()
        {
        }

        [MethodImpl(0x8000)]
        T ISimSpaceHelper.GetObjectByGDBId<T>(int gdbConfId) where T: SimSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        T ISimSpaceHelper.GetObjectById<T>(uint id) where T: SimSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        Vector3D ISimSpaceHelper.GetRandomStandaloneLocation4SpaceObject()
        {
        }

        [MethodImpl(0x8000)]
        uint ISimSpaceHelper.GetTickFrameSize()
        {
        }

        [MethodImpl(0x8000)]
        uint ISimSpaceHelper.GetTickSeq()
        {
        }

        [MethodImpl(0x8000)]
        double ISimSpaceHelper.NextDouble()
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D CalcJumpTargetPos(Vector3D jumpTargetLocation, Vector3D currLocation, int targetKeepDistance)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D CalcPortalJumpTargetPos(Vector3D buildingTargetLocation, Vector3D portalLocation, int targetKeepDistance)
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachSpaceObj(Action<SimSpaceObject> action)
        {
        }

        [MethodImpl(0x8000)]
        public T GetObjectByGDBId<T>(int gdbConfId) where T: SimSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        public T GetObjectById<T>(uint id) where T: SimSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetRandomStandaloneLocation4SpaceObject()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetTickSeq()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize(GDBSolarSystemInfo info, int objCountMax, uint tickFrameSize = 100, uint currSolarSystemTime = 0, bool saveMemory = false)
        {
        }

        [MethodImpl(0x8000), OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSpaceObject(SimSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTickFrameSize(uint tickFrameSize)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickFrameOnSingleSimObj(SimSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickOneFrame()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBSolarSystemInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

