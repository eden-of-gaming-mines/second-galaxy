﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using System;

    public interface ISimMovementModuleEventListener
    {
        void OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId);
        void OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId);
        void OnCrossNotifySpeed(bool up);
        void OnCurrMovementCompleted();
    }
}

