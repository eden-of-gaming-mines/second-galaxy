﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class SimMovementModule : SimObjectModule
    {
        protected Vector4D m_velocity;
        protected ISimMovemnetParamProvider m_paramProvider;
        protected ISimMovementModuleEventListener m_eventListener;
        protected bool m_isMovementCompletedFired;
        protected double m_notifySpeed;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetVelocity_1;
        private static DelegateBridge __Hotfix_SetVelocity_0;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_GetRelativeObjId;
        private static DelegateBridge __Hotfix_SetNotifySpeed;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_FireMovementCompleteEvent;
        private static DelegateBridge __Hotfix_FireMovementCrossNotifySpeedEvent;
        private static DelegateBridge __Hotfix_get_Velocity;

        [MethodImpl(0x8000)]
        public SimMovementModule(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        public abstract bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd);
        [MethodImpl(0x8000)]
        protected void FireMovementCompleteEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void FireMovementCrossNotifySpeedEvent(bool up)
        {
        }

        public abstract MovementModuleType GetMovementModuleType();
        [MethodImpl(0x8000)]
        public virtual uint GetRelativeObjId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object extraData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNotifySpeed(double notifySpeed)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVelocity(Vector4D velocity)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVelocity(Vector3D direction, double speed)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint deltaMillisecond)
        {
        }

        public Vector4D Velocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

