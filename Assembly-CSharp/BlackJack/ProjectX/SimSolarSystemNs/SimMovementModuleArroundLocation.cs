﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimMovementModuleArroundLocation : SimMovementModuleCruiseBase
    {
        private Vector3D m_targetPos;
        private double m_radius;
        public bool m_clockwise;
        private Vector3D m_planeNormal;
        private uint m_planetNormalParams;
        private double m_arroundTargetDistanceParam;
        private bool m_needMoveToTargetDirectly;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetMovementModuleType;
        private static DelegateBridge __Hotfix_CollectArroundTargetSyncData;
        private static DelegateBridge __Hotfix_CheckMoveCmd;
        private static DelegateBridge __Hotfix_ResetWithMoveTarget;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetArroundNormalVector;
        private static DelegateBridge __Hotfix_GetRandomVecAroundAxisY;
        private static DelegateBridge __Hotfix_TransformFromSerializeDataToNormalVec;

        [MethodImpl(0x8000)]
        public SimMovementModuleArroundLocation(SimSpaceObject owner, ISimMovemnetParamProvider paramProvider, ISimMovementModuleEventListener eventListener = null)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckMoveCmd(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModuleArroundTargetSyncData CollectArroundTargetSyncData()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetArroundNormalVector()
        {
        }

        [MethodImpl(0x8000)]
        public override MovementModuleType GetMovementModuleType()
        {
        }

        [MethodImpl(0x8000)]
        private Vector3D GetRandomVecAroundAxisY(double angle, ref uint serializeParams)
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetWithMoveTarget(SimMovementModule preCtxModule, Vector3D vectorParam, uint targetObjId, uint radiusParam, object syncData)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        [MethodImpl(0x8000)]
        private Vector3D TransformFromSerializeDataToNormalVec()
        {
        }
    }
}

