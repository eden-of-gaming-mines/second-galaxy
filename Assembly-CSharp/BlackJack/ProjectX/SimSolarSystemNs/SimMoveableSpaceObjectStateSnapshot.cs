﻿namespace BlackJack.ProjectX.SimSolarSystemNs
{
    using Dest.Math;
    using IL;
    using System;

    [Serializable]
    public class SimMoveableSpaceObjectStateSnapshot
    {
        public Vector3D m_location;
        public Vector3D m_rotation;
        public double m_speedValue;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

