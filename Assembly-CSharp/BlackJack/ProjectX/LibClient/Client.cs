﻿namespace BlackJack.ProjectX.LibClient
{
    using BlackJack.LibClient;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Client : IClient
    {
        private LibClientStateMachine m_csm;
        private IClientEventHandler m_clientEventHandler;
        private string m_authToken;
        private string m_clientVersion;
        private string m_clientDeviceId;
        private string m_localization;
        private int m_loginChannelId;
        private int m_bornChannelId;
        private ulong? m_sessionKey;
        private string m_sessionToken;
        private Connection m_connect;
        private LibClientProtoProvider m_protoProvider;
        private bool m_isBlockProcessMsg;
        private Func<Stream, Type, int, object> m_messageDeserializeAction;
        [CompilerGenerated]
        private static MessageProc <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetClientToInit;
        private static DelegateBridge __Hotfix_BlockProcessMsg;
        private static DelegateBridge __Hotfix_LoginByAuthToken;
        private static DelegateBridge __Hotfix_LoginBySessionToken;
        private static DelegateBridge __Hotfix_Disconnect;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_SendMessage;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickAuthLogin;
        private static DelegateBridge __Hotfix_TickSessionLogin;
        private static DelegateBridge __Hotfix_TickLoginOk;
        private static DelegateBridge __Hotfix_TickDisconnecting;
        private static DelegateBridge __Hotfix_TickDisconnected;
        private static DelegateBridge __Hotfix_DebugWarning;
        private static DelegateBridge __Hotfix_ProcessMessages;
        private static DelegateBridge __Hotfix_GetEndPoint;
        private static DelegateBridge __Hotfix_RegConnectionLogEvent;

        [MethodImpl(0x8000)]
        public Client(IClientEventHandler handler, Func<Stream, Type, int, object> deserializeMessageAction = null, bool decodeMessageOnTick = true)
        {
        }

        [MethodImpl(0x8000)]
        public void BlockProcessMsg(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        public void Close()
        {
        }

        [MethodImpl(0x8000)]
        private void DebugWarning(string fun, string msg1, string msg2)
        {
        }

        [MethodImpl(0x8000)]
        public bool Disconnect()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEndPoint()
        {
        }

        [MethodImpl(0x8000)]
        public bool LoginByAuthToken(string serverAddress, int serverPort, string authToken, string clientVersion, string clientDeviceId, string localization = "", int loginChannelId = 0, int bornChannelId = 0, string serverDomain = "")
        {
        }

        [MethodImpl(0x8000)]
        public bool LoginBySessionToken(string sessionToken, string clientVersion, string localization = "", int loginChannelId = 0, int bornChannelId = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void ProcessMessages(MessageProc proc = null)
        {
        }

        [MethodImpl(0x8000)]
        public void RegConnectionLogEvent(Action<string> logAction)
        {
        }

        [MethodImpl(0x8000)]
        public bool SendMessage(object msg)
        {
        }

        [MethodImpl(0x8000)]
        public void SetClientToInit()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickAuthLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickDisconnecting()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickLoginOk()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickSessionLogin()
        {
        }
    }
}

