﻿namespace BlackJack.ProjectX.LibClient
{
    using System;

    public interface IClient
    {
        void BlockProcessMsg(bool isBlock);
        void Close();
        bool Disconnect();
        bool LoginByAuthToken(string serverAddress, int serverPort, string authToken, string clientVersion, string clientDeviceId, string localization, int loginChannelId, int bornChannelId, string serverDomain);
        bool LoginBySessionToken(string sessionToken, string clientVersion, string localization, int loginChannelId, int bornChannelId);
        void RegConnectionLogEvent(Action<string> logAction);
        bool SendMessage(object msg);
        void Tick();
    }
}

