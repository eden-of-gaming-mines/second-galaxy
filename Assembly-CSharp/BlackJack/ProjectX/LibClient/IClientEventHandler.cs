﻿namespace BlackJack.ProjectX.LibClient
{
    using System;
    using System.Runtime.InteropServices;

    public interface IClientEventHandler
    {
        void OnConnected();
        void OnDisconnected();
        void OnError(int err, string excepionInfo = null);
        void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken);
        void OnLoginBySessionTokenAck(int result);
        void OnMessage(object msg, int msgId);
    }
}

