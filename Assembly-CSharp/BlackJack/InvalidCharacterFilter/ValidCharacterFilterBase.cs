﻿namespace BlackJack.InvalidCharacterFilter
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ValidCharacterFilterBase
    {
        public static int Unicode_Character_Max;
        private static KeyValuePair<char, char>[] ValidCharArray;
        protected byte[] m_arFilterBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetValidCharacterFilter_1;
        private static DelegateBridge __Hotfix_SetValidCharacterFilter_2;
        private static DelegateBridge __Hotfix_SetValidCharacterFilter_0;
        private static DelegateBridge __Hotfix_SetInValidCharacterFilter;
        private static DelegateBridge __Hotfix_CharacterValidCheck;

        [MethodImpl(0x8000)]
        public virtual bool CharacterValidCheck(string input)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetInValidCharacterFilter(int invalidChar)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetValidCharacterFilter(int validChar)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetValidCharacterFilter(KeyValuePair<char, char>[] validArray)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetValidCharacterFilter(int validCharStart, int validCharEnd)
        {
        }
    }
}

