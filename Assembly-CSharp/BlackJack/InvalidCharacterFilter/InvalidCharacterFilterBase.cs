﻿namespace BlackJack.InvalidCharacterFilter
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class InvalidCharacterFilterBase
    {
        public static int Unicode_Character_Max;
        private static char[] InValidCharArray;
        protected byte[] m_arFilterBuf;
        protected static InvalidCharacterFilterBase m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetValidCharacterFilter_0;
        private static DelegateBridge __Hotfix_SetValidCharacterFilter_1;
        private static DelegateBridge __Hotfix_SetInValidCharacterFilter_0;
        private static DelegateBridge __Hotfix_SetInValidCharacterFilter_1;
        private static DelegateBridge __Hotfix_CharacterValidCheck;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        public virtual bool CharacterValidCheck(string input)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetInValidCharacterFilter(char[] validArray)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetInValidCharacterFilter(int invalidChar)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetValidCharacterFilter(int validChar)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetValidCharacterFilter(int validCharStart, int validCharEnd)
        {
        }

        public static InvalidCharacterFilterBase Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

