﻿namespace BlackJack.ToolUtil
{
    using System;

    public interface IIdFactory
    {
        uint AllocId();
        void FreeId(uint id);

        int MaxCount { get; }
    }
}

