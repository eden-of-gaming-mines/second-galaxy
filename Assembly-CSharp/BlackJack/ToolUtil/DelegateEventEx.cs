﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DelegateEventEx
    {
        private readonly Dictionary<object, Action> m_actionDict;
        private readonly List<ActionItem> m_actionItemQuene;
        private bool m_inInvokeLoop;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Invoke;
        private static DelegateBridge __Hotfix_Register;
        private static DelegateBridge __Hotfix_Unregister;
        private static DelegateBridge __Hotfix_IsEmpty;

        [MethodImpl(0x8000)]
        public void Invoke()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEmpty()
        {
        }

        [MethodImpl(0x8000)]
        public void Register(object key, Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void Unregister(object key)
        {
        }

        private class ActionItem
        {
            public object m_key;
            public Action m_action;
            public bool m_isRegister;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

