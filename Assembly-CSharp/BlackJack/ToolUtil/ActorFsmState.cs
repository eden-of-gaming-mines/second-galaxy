﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class ActorFsmState
    {
        protected int m_stateId;
        protected ActorFsm m_fsm;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnEnter;
        private static DelegateBridge __Hotfix_OnLeave;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_get_StateId;

        [MethodImpl(0x8000)]
        public ActorFsmState(ActorFsm fsm)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnEnter(params object[] onEnterParams)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnLeave()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnTick(uint deltaMillisecond)
        {
        }

        public int StateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

