﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class BitArrayEx
    {
        private byte[] m_array;
        private int m_indexCapacity;
        private int MaxIndex;
        [NonSerialized]
        private const int EveryUnitBitSize = 8;
        [NonSerialized]
        private const int DefaultArrayLength = 8;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_InitBitArray;
        private static DelegateBridge __Hotfix_ReInit;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetValue;
        private static DelegateBridge __Hotfix_GetValue;
        private static DelegateBridge __Hotfix_GetCapacity;
        private static DelegateBridge __Hotfix_GetMaxIndex;
        private static DelegateBridge __Hotfix_GetBitArray;
        private static DelegateBridge __Hotfix_GetBitArrayBinaryStrInfo;
        private static DelegateBridge __Hotfix_Serialize2ByteArray;
        private static DelegateBridge __Hotfix_DeserializeFromByteArray;
        private static DelegateBridge __Hotfix_EnsureCapacity;

        [MethodImpl(0x8000)]
        public BitArrayEx(int maxSize)
        {
        }

        [MethodImpl(0x8000)]
        public BitArrayEx(byte[] array, int indexCapacity, int maxSize)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public static BitArrayEx DeserializeFromByteArray(byte[] buff)
        {
        }

        [MethodImpl(0x8000)]
        private void EnsureCapacity(int newIndex)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetBitArray()
        {
        }

        [MethodImpl(0x8000)]
        public string GetBitArrayBinaryStrInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCapacity()
        {
        }

        [MethodImpl(0x8000)]
        public int GetMaxIndex()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetValue(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void InitBitArray(int maxSize)
        {
        }

        [MethodImpl(0x8000)]
        public void ReInit()
        {
        }

        [MethodImpl(0x8000)]
        public byte[] Serialize2ByteArray()
        {
        }

        [MethodImpl(0x8000)]
        public void SetValue(int index, bool value)
        {
        }
    }
}

