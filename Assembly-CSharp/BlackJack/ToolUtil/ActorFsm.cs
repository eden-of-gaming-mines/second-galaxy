﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ActorFsm
    {
        protected ActorFsmState m_currState;
        protected List<ActorFsmState> m_stateList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_AddState;
        private static DelegateBridge __Hotfix_GetCurrState;
        private static DelegateBridge __Hotfix_GetStateById;
        private static DelegateBridge __Hotfix_GotoState;

        [MethodImpl(0x8000)]
        public void AddState(ActorFsmState state)
        {
        }

        [MethodImpl(0x8000)]
        public T GetCurrState<T>() where T: ActorFsmState
        {
        }

        [MethodImpl(0x8000)]
        public T GetStateById<T>(int stateId) where T: ActorFsmState
        {
        }

        [MethodImpl(0x8000)]
        public void GotoState(int stateId, params object[] newStateParmas)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Init(object param)
        {
        }
    }
}

