﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class MapObjInstanceIdFactory : IIdFactory
    {
        private int m_maxCount;
        private int m_maxIndex;
        private int m_maxUsedIndex;
        private Queue<uint> m_freeIdPool;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AllocId;
        private static DelegateBridge __Hotfix_FreeId;
        private static DelegateBridge __Hotfix_GetIdIndex;
        private static DelegateBridge __Hotfix_GetIdSeq;
        private static DelegateBridge __Hotfix_GetFreePoolCopy;
        private static DelegateBridge __Hotfix_get_MaxCount;
        private static DelegateBridge __Hotfix_get_MaxUsedIndex;

        [MethodImpl(0x8000)]
        public MapObjInstanceIdFactory(int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public uint AllocId()
        {
        }

        [MethodImpl(0x8000)]
        public void FreeId(uint id)
        {
        }

        [MethodImpl(0x8000)]
        public void GetFreePoolCopy(ref Queue<uint> freePool)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetIdIndex(uint id)
        {
        }

        [MethodImpl(0x8000)]
        public static byte GetIdSeq(uint id)
        {
        }

        public int MaxCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int MaxUsedIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

