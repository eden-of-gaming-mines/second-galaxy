﻿namespace BlackJack.ToolUtil
{
    using System;
    using System.Collections.Generic;

    public interface IUserGuideStepInfo
    {
        int GetGroupId();
        int GetID();
        bool GetIsSavePoint();
        bool GetIsTrigerPoint();
        int GetNextStepId();
        List<KeyValuePair<int, int>> GetPageList();
    }
}

