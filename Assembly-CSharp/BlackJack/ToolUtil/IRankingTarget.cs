﻿namespace BlackJack.ToolUtil
{
    using System;

    public interface IRankingTarget
    {
        string GetKey();
    }
}

