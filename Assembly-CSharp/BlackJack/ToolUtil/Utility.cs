﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class Utility
    {
        public static class Math
        {
            private static DelegateBridge __Hotfix_NormalizeFloat2IntSpace;

            [MethodImpl(0x8000)]
            public static int NormalizeFloat2IntSpace(float p1, int maxInt)
            {
            }
        }

        public static class Misc
        {
            private static DelegateBridge __Hotfix_GetRandomInList;
            private static DelegateBridge __Hotfix_ConvertUtcTime2LocalTime;

            public static DateTime ConvertUtcTime2LocalTime(DateTime utcTime)
            {
                DelegateBridge bridge = __Hotfix_ConvertUtcTime2LocalTime;
                return ((bridge == null) ? TimeZoneInfo.ConvertTime(utcTime, TimeZoneInfo.Local) : bridge.__Gen_Delegate_Imp1881(utcTime));
            }

            public static T GetRandomInList<T>(List<T> list, Random random)
            {
                DelegateBridge bridge = __Hotfix_GetRandomInList;
                if (bridge != null)
                {
                    bridge.InvokeSessionStart();
                    bridge.InParam<List<T>>(list);
                    bridge.InParam<Random>(random);
                    bridge.Invoke(1);
                    return bridge.InvokeSessionEndWithResult<T>();
                }
                if ((list != null) && (list.Count != 0))
                {
                    return list[random.Next(list.Count)];
                }
                return default(T);
            }
        }
    }
}

