﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class ObjId2ObjMap
    {
        private int m_countMax;
        private bool m_isSaveMemory;
        private object[] m_objTable;
        private uint[] m_idTable;
        private Dictionary<uint, object> m_objDict;
        private uint m_reachedIndexMax;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Register;
        private static DelegateBridge __Hotfix_RegisterNormal;
        private static DelegateBridge __Hotfix_RegisterSaveMemory;
        private static DelegateBridge __Hotfix_Unregister;
        private static DelegateBridge __Hotfix_UnregisterNormal;
        private static DelegateBridge __Hotfix_UnregisterSaveMemory;
        private static DelegateBridge __Hotfix_GetObjectById;
        private static DelegateBridge __Hotfix_GetObjectByIdNormal;
        private static DelegateBridge __Hotfix_GetObjectByIdNormalSaveMemory;
        private static DelegateBridge __Hotfix_Foreach_0;
        private static DelegateBridge __Hotfix_ForeachNormal_0;
        private static DelegateBridge __Hotfix_ForeachSaveMemory_0;
        private static DelegateBridge __Hotfix_Foreach_1;
        private static DelegateBridge __Hotfix_ForeachNormal_1;
        private static DelegateBridge __Hotfix_ForeachSaveMemory_1;
        private static DelegateBridge __Hotfix_ForeachEx_0;
        private static DelegateBridge __Hotfix_ForeachExNormal_0;
        private static DelegateBridge __Hotfix_ForeachExSaveMemory_0;
        private static DelegateBridge __Hotfix_ForeachEx_1;
        private static DelegateBridge __Hotfix_ForeachExNormal_1;
        private static DelegateBridge __Hotfix_ForeachExSaveMemory_1;
        private static DelegateBridge __Hotfix_FindFirst;
        private static DelegateBridge __Hotfix_FindFirstNormal;
        private static DelegateBridge __Hotfix_FindFirstSaveMemory;

        [MethodImpl(0x8000)]
        public ObjId2ObjMap(int countMax, bool saveMemory = false)
        {
        }

        [MethodImpl(0x8000)]
        public T FindFirst<T>(Func<T, bool> filter) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T FindFirstNormal<T>(Func<T, bool> filter) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T FindFirstSaveMemory<T>(Func<T, bool> filter) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void Foreach<T>(Action<T> action) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void Foreach<T, TP1>(Action<T, TP1> action, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachEx<T>(Func<T, bool> func) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachEx<T, TP1>(Func<T, TP1, bool> func, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachExNormal<T>(Func<T, bool> func) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachExNormal<T, TP1>(Func<T, TP1, bool> func, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachExSaveMemory<T>(Func<T, bool> func) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachExSaveMemory<T, TP1>(Func<T, TP1, bool> func, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        private void ForeachNormal<T>(Action<T> action) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachNormal<T, TP1>(Action<T, TP1> action, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        private void ForeachSaveMemory<T>(Action<T> action) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachSaveMemory<T, TP1>(Action<T, TP1> action, TP1 p1) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T GetObjectById<T>(uint id) where T: class
        {
        }

        [MethodImpl(0x8000)]
        private T GetObjectByIdNormal<T>(uint id) where T: class
        {
        }

        [MethodImpl(0x8000)]
        private T GetObjectByIdNormalSaveMemory<T>(uint id) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public bool Register(uint id, object obj)
        {
        }

        [MethodImpl(0x8000)]
        private bool RegisterNormal(uint id, object obj)
        {
        }

        [MethodImpl(0x8000)]
        private bool RegisterSaveMemory(uint id, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public bool Unregister(uint id)
        {
        }

        [MethodImpl(0x8000)]
        private bool UnregisterNormal(uint id)
        {
        }

        [MethodImpl(0x8000)]
        private bool UnregisterSaveMemory(uint id)
        {
        }
    }
}

