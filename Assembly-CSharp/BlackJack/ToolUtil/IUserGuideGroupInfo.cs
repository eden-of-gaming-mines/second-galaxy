﻿namespace BlackJack.ToolUtil
{
    using System;

    public interface IUserGuideGroupInfo
    {
        bool CanSkip();
    }
}

