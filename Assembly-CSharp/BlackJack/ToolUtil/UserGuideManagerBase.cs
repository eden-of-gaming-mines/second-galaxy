﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class UserGuideManagerBase
    {
        protected int m_currGroup;
        protected int m_currStep;
        protected int m_nextStep;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckStep;
        private static DelegateBridge __Hotfix_OnStepStart;
        private static DelegateBridge __Hotfix_OnStepEnd;
        private static DelegateBridge __Hotfix_ClearCurrGroup;
        private static DelegateBridge __Hotfix_SkipStep;
        private static DelegateBridge __Hotfix_SetupDebugState;

        [MethodImpl(0x8000)]
        protected UserGuideManagerBase()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckStep(int stepId, out bool isGroupSaved, out int savedGroupId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearCurrGroup()
        {
        }

        public abstract IUserGuideGroupInfo GetGroupInfo(int groupId);
        public abstract IUserGuideStepInfo GetStepInfo(int stepId);
        public abstract bool IsGroupHaveSavePoint(int groupId);
        public abstract bool IsStepGroupAlreadyCompleted(int groupId);
        [MethodImpl(0x8000)]
        public void OnStepEnd(out bool groupSaved)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnStepStart(int stepId)
        {
        }

        protected abstract void SaveGroup4Completed(int currGroup);
        [MethodImpl(0x8000)]
        public virtual bool SetupDebugState(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public void SkipStep(int stepId)
        {
        }
    }
}

