﻿namespace BlackJack.ToolUtil
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DelegateEventEx<T1>
    {
        private readonly Dictionary<object, Action<T1>> m_actionDict;
        private readonly List<ActionItem<T1>> m_actionItemQuene;
        private bool m_inInvokeLoop;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Invoke;
        private static DelegateBridge __Hotfix_Register;
        private static DelegateBridge __Hotfix_Unregister;
        private static DelegateBridge __Hotfix_IsEmpty;

        [MethodImpl(0x8000)]
        public void Invoke(T1 p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEmpty()
        {
        }

        [MethodImpl(0x8000)]
        public void Register(object key, Action<T1> action)
        {
        }

        [MethodImpl(0x8000)]
        public void Unregister(object key)
        {
        }

        private class ActionItem
        {
            public object m_key;
            public Action<T1> m_action;
            public bool m_isRegister;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

