﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataConfigIDRangeInfo")]
    public class ConfigDataConfigIDRangeInfo : IExtensible
    {
        private int _ID;
        private string _ConfigObjectName;
        private readonly List<int> _ConfigIDRange;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ConfigObjectName;
        private static DelegateBridge __Hotfix_set_ConfigObjectName;
        private static DelegateBridge __Hotfix_get_ConfigIDRange;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ConfigObjectName", DataFormat=DataFormat.Default)]
        public string ConfigObjectName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ConfigIDRange", DataFormat=DataFormat.TwosComplement)]
        public List<int> ConfigIDRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

