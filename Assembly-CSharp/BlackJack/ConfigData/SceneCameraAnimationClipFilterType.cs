﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneCameraAnimationClipFilterType")]
    public enum SceneCameraAnimationClipFilterType
    {
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_None", Value=0)]
        SceneCameraAnimationClipFilterType_None = 0,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_OwnerPlayer", Value=1)]
        SceneCameraAnimationClipFilterType_OwnerPlayer = 1,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_OtherPlayer", Value=2)]
        SceneCameraAnimationClipFilterType_OtherPlayer = 2,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_NpcShip", Value=3)]
        SceneCameraAnimationClipFilterType_NpcShip = 3,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_RefNameMatch", Value=4)]
        SceneCameraAnimationClipFilterType_RefNameMatch = 4,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_SpaceStation", Value=5)]
        SceneCameraAnimationClipFilterType_SpaceStation = 5,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_StarGate", Value=6)]
        SceneCameraAnimationClipFilterType_StarGate = 6,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_SceneObject", Value=7)]
        SceneCameraAnimationClipFilterType_SceneObject = 7,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_SimpleObject", Value=8)]
        SceneCameraAnimationClipFilterType_SimpleObject = 8,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_BulletObject", Value=9)]
        SceneCameraAnimationClipFilterType_BulletObject = 9,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_HudLayer", Value=10)]
        SceneCameraAnimationClipFilterType_HudLayer = 10,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_CelestialLayer", Value=11)]
        SceneCameraAnimationClipFilterType_CelestialLayer = 11,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_ShipLayer", Value=12)]
        SceneCameraAnimationClipFilterType_ShipLayer = 12,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_UILayer", Value=13)]
        SceneCameraAnimationClipFilterType_UILayer = 13,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_AllLayer", Value=14)]
        SceneCameraAnimationClipFilterType_AllLayer = 14,
        [ProtoEnum(Name="SceneCameraAnimationClipFilterType_PlantLikeBuilding", Value=15)]
        SceneCameraAnimationClipFilterType_PlantLikeBuilding = 15
    }
}

