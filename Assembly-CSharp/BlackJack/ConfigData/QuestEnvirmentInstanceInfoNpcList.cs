﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="QuestEnvirmentInstanceInfoNpcList")]
    public class QuestEnvirmentInstanceInfoNpcList : IExtensible
    {
        private int _index;
        private int _SolarSystemId;
        private int _SpaceStationId;
        private int _NpcId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_index;
        private static DelegateBridge __Hotfix_set_index;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_SpaceStationId;
        private static DelegateBridge __Hotfix_set_SpaceStationId;
        private static DelegateBridge __Hotfix_get_NpcId;
        private static DelegateBridge __Hotfix_set_NpcId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="index", DataFormat=DataFormat.TwosComplement)]
        public int index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SpaceStationId", DataFormat=DataFormat.TwosComplement)]
        public int SpaceStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="NpcId", DataFormat=DataFormat.TwosComplement)]
        public int NpcId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

