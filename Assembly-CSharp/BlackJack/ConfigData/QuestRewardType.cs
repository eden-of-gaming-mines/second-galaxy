﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestRewardType")]
    public enum QuestRewardType
    {
        [ProtoEnum(Name="QuestRewardType_Exp", Value=1)]
        QuestRewardType_Exp = 1,
        [ProtoEnum(Name="QuestRewardType_Currency", Value=2)]
        QuestRewardType_Currency = 2,
        [ProtoEnum(Name="QuestRewardType_Item", Value=3)]
        QuestRewardType_Item = 3,
        [ProtoEnum(Name="QuestRewardType_BindItem", Value=4)]
        QuestRewardType_BindItem = 4,
        [ProtoEnum(Name="QuestRewardType_FactionCredit", Value=5)]
        QuestRewardType_FactionCredit = 5,
        [ProtoEnum(Name="QuestRewardType_Buff", Value=6)]
        QuestRewardType_Buff = 6,
        [ProtoEnum(Name="QuestRewardType_PlayerLevelExp", Value=7)]
        QuestRewardType_PlayerLevelExp = 7,
        [ProtoEnum(Name="QuestRewardType_NpcCaptain", Value=8)]
        QuestRewardType_NpcCaptain = 8,
        [ProtoEnum(Name="QuestRewardType_ScanProbe", Value=9)]
        QuestRewardType_ScanProbe = 9,
        [ProtoEnum(Name="QuestRewardType_GrandFactionItem", Value=10)]
        QuestRewardType_GrandFactionItem = 10,
        [ProtoEnum(Name="QuestRewardType_GrandFactionBindItem", Value=11)]
        QuestRewardType_GrandFactionBindItem = 11,
        [ProtoEnum(Name="QuestRewardType_GuildTradeMoney", Value=12)]
        QuestRewardType_GuildTradeMoney = 12,
        [ProtoEnum(Name="QuestRewardType_SolarSystemFlourish", Value=13)]
        QuestRewardType_SolarSystemFlourish = 13,
        [ProtoEnum(Name="QuestRewardType_SolarSystemGuildMineral", Value=14)]
        QuestRewardType_SolarSystemGuildMineral = 14,
        [ProtoEnum(Name="QuestRewardType_BattlePassExp", Value=15)]
        QuestRewardType_BattlePassExp = 15
    }
}

