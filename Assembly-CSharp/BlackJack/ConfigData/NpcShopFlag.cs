﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcShopFlag")]
    public enum NpcShopFlag
    {
        [ProtoEnum(Name="NpcShopFlag_Invalid", Value=0)]
        NpcShopFlag_Invalid = 0,
        [ProtoEnum(Name="NpcShopFlag_DefaultNpcShop", Value=1)]
        NpcShopFlag_DefaultNpcShop = 1,
        [ProtoEnum(Name="NpcShopFlag_TradeBuyNpcShop", Value=2)]
        NpcShopFlag_TradeBuyNpcShop = 2,
        [ProtoEnum(Name="NpcShopFlag_PanGalaNpcShop", Value=4)]
        NpcShopFlag_PanGalaNpcShop = 4,
        [ProtoEnum(Name="NpcShopFlag_FactionNpcShow", Value=8)]
        NpcShopFlag_FactionNpcShow = 8,
        [ProtoEnum(Name="NpcShopFlag_GuildGalaNpcShop", Value=0x10)]
        NpcShopFlag_GuildGalaNpcShop = 0x10,
        [ProtoEnum(Name="NpcShopFlag_CanNotSaleItem", Value=0x20)]
        NpcShopFlag_CanNotSaleItem = 0x20
    }
}

