﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainLevelInfo")]
    public class ConfigDataNpcCaptainLevelInfo : IExtensible
    {
        private int _ID;
        private int _LevelUpExp;
        private int _LevelTotalExp;
        private int _NeedPlayerLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LevelUpExp;
        private static DelegateBridge __Hotfix_set_LevelUpExp;
        private static DelegateBridge __Hotfix_get_LevelTotalExp;
        private static DelegateBridge __Hotfix_set_LevelTotalExp;
        private static DelegateBridge __Hotfix_get_NeedPlayerLevel;
        private static DelegateBridge __Hotfix_set_NeedPlayerLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LevelUpExp", DataFormat=DataFormat.TwosComplement)]
        public int LevelUpExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LevelTotalExp", DataFormat=DataFormat.TwosComplement)]
        public int LevelTotalExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NeedPlayerLevel", DataFormat=DataFormat.TwosComplement)]
        public int NeedPlayerLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

