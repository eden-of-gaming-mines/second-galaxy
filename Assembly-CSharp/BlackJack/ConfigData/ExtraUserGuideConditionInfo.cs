﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ExtraUserGuideConditionInfo")]
    public class ExtraUserGuideConditionInfo : IExtensible
    {
        private UserGuideConditionType _ConditionType;
        private string _Param1;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConditionType;
        private static DelegateBridge __Hotfix_set_ConditionType;
        private static DelegateBridge __Hotfix_get_Param1;
        private static DelegateBridge __Hotfix_set_Param1;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ConditionType", DataFormat=DataFormat.TwosComplement)]
        public UserGuideConditionType ConditionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Param1", DataFormat=DataFormat.Default)]
        public string Param1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

