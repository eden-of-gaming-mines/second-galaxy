﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataQuestAdditionRewardInfo")]
    public class ConfigDataQuestAdditionRewardInfo : IExtensible
    {
        private int _ID;
        private int _StartQuestLevel;
        private int _EndQuestLevel;
        private readonly List<QuestRewardInfo> _RewardList;
        private int _ItemDropInfoId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_StartQuestLevel;
        private static DelegateBridge __Hotfix_set_StartQuestLevel;
        private static DelegateBridge __Hotfix_get_EndQuestLevel;
        private static DelegateBridge __Hotfix_set_EndQuestLevel;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_ItemDropInfoId;
        private static DelegateBridge __Hotfix_set_ItemDropInfoId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StartQuestLevel", DataFormat=DataFormat.TwosComplement)]
        public int StartQuestLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EndQuestLevel", DataFormat=DataFormat.TwosComplement)]
        public int EndQuestLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ItemDropInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ItemDropInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

