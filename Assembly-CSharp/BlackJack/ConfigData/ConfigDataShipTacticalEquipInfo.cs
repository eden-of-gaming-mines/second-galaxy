﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipTacticalEquipInfo")]
    public class ConfigDataShipTacticalEquipInfo : IExtensible
    {
        private int _ID;
        private TacticalEquipFunctionType _FunctionType;
        private readonly List<ParamInfo> _EffectDescParamList;
        private float _RangMax;
        private uint _ChargeTime;
        private readonly List<int> _BufIdList;
        private readonly List<ParamInfo> _Params;
        private EquipType _GroupByEquipType;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private int _LocalPropertiesCount;
        private uint _LaunchCD;
        private string _IconResPathInStation;
        private readonly List<string> _IconResPathListInSpace;
        private string _LaunchEffect;
        private string _ChargeEffect;
        private string _ExtraEffect;
        private int _FakeBufInfoId;
        private string _NameStrKey;
        private string _EffectDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_EffectDescParamList;
        private static DelegateBridge __Hotfix_get_RangMax;
        private static DelegateBridge __Hotfix_set_RangMax;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_BufIdList;
        private static DelegateBridge __Hotfix_get_Params;
        private static DelegateBridge __Hotfix_get_GroupByEquipType;
        private static DelegateBridge __Hotfix_set_GroupByEquipType;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_set_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_get_LaunchCD;
        private static DelegateBridge __Hotfix_set_LaunchCD;
        private static DelegateBridge __Hotfix_get_IconResPathInStation;
        private static DelegateBridge __Hotfix_set_IconResPathInStation;
        private static DelegateBridge __Hotfix_get_IconResPathListInSpace;
        private static DelegateBridge __Hotfix_get_LaunchEffect;
        private static DelegateBridge __Hotfix_set_LaunchEffect;
        private static DelegateBridge __Hotfix_get_ChargeEffect;
        private static DelegateBridge __Hotfix_set_ChargeEffect;
        private static DelegateBridge __Hotfix_get_ExtraEffect;
        private static DelegateBridge __Hotfix_set_ExtraEffect;
        private static DelegateBridge __Hotfix_get_FakeBufInfoId;
        private static DelegateBridge __Hotfix_set_FakeBufInfoId;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_EffectDescStrKey;
        private static DelegateBridge __Hotfix_set_EffectDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public TacticalEquipFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="EffectDescParamList", DataFormat=DataFormat.Default)]
        public List<ParamInfo> EffectDescParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="RangMax", DataFormat=DataFormat.FixedSize)]
        public float RangMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ChargeTime", DataFormat=DataFormat.TwosComplement)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="BufIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="Params", DataFormat=DataFormat.Default)]
        public List<ParamInfo> Params
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="GroupByEquipType", DataFormat=DataFormat.TwosComplement)]
        public EquipType GroupByEquipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="LocalPropertiesCount", DataFormat=DataFormat.TwosComplement)]
        public int LocalPropertiesCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="LaunchCD", DataFormat=DataFormat.TwosComplement)]
        public uint LaunchCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="IconResPathInStation", DataFormat=DataFormat.Default)]
        public string IconResPathInStation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, Name="IconResPathListInSpace", DataFormat=DataFormat.Default)]
        public List<string> IconResPathListInSpace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="LaunchEffect", DataFormat=DataFormat.Default)]
        public string LaunchEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="ChargeEffect", DataFormat=DataFormat.Default)]
        public string ChargeEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="ExtraEffect", DataFormat=DataFormat.Default)]
        public string ExtraEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="FakeBufInfoId", DataFormat=DataFormat.TwosComplement)]
        public int FakeBufInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="EffectDescStrKey", DataFormat=DataFormat.Default)]
        public string EffectDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

