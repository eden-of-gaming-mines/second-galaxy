﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="LoginRewardInfo")]
    public class LoginRewardInfo : IExtensible
    {
        private LoginRewardType _RewardType;
        private int _P1;
        private int _P2;
        private int _P3;
        private int _P4;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_RewardType;
        private static DelegateBridge __Hotfix_set_RewardType;
        private static DelegateBridge __Hotfix_get_P1;
        private static DelegateBridge __Hotfix_set_P1;
        private static DelegateBridge __Hotfix_get_P2;
        private static DelegateBridge __Hotfix_set_P2;
        private static DelegateBridge __Hotfix_get_P3;
        private static DelegateBridge __Hotfix_set_P3;
        private static DelegateBridge __Hotfix_get_P4;
        private static DelegateBridge __Hotfix_set_P4;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="RewardType", DataFormat=DataFormat.TwosComplement)]
        public LoginRewardType RewardType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="P1", DataFormat=DataFormat.TwosComplement)]
        public int P1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="P2", DataFormat=DataFormat.TwosComplement)]
        public int P2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="P3", DataFormat=DataFormat.TwosComplement)]
        public int P3
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="P4", DataFormat=DataFormat.TwosComplement)]
        public int P4
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

