﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigParams")]
    public class ConfigParams : IExtensible
    {
        private float _Scale;
        private float _Disturb;
        private float _LifeTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Scale;
        private static DelegateBridge __Hotfix_set_Scale;
        private static DelegateBridge __Hotfix_get_Disturb;
        private static DelegateBridge __Hotfix_set_Disturb;
        private static DelegateBridge __Hotfix_get_LifeTime;
        private static DelegateBridge __Hotfix_set_LifeTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Scale", DataFormat=DataFormat.FixedSize)]
        public float Scale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Disturb", DataFormat=DataFormat.FixedSize)]
        public float Disturb
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LifeTime", DataFormat=DataFormat.FixedSize)]
        public float LifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

