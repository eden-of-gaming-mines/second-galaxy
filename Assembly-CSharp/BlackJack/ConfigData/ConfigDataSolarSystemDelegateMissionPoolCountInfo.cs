﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSolarSystemDelegateMissionPoolCountInfo")]
    public class ConfigDataSolarSystemDelegateMissionPoolCountInfo : IExtensible
    {
        private int _ID;
        private int _Difficult;
        private int _MinPoolCount;
        private int _MaxPoolCount;
        private float _FightSignalReduce;
        private float _MineralSignalReduce;
        private float _TransportSignalReduce;
        private string _IconResString;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Difficult;
        private static DelegateBridge __Hotfix_set_Difficult;
        private static DelegateBridge __Hotfix_get_MinPoolCount;
        private static DelegateBridge __Hotfix_set_MinPoolCount;
        private static DelegateBridge __Hotfix_get_MaxPoolCount;
        private static DelegateBridge __Hotfix_set_MaxPoolCount;
        private static DelegateBridge __Hotfix_get_FightSignalReduce;
        private static DelegateBridge __Hotfix_set_FightSignalReduce;
        private static DelegateBridge __Hotfix_get_MineralSignalReduce;
        private static DelegateBridge __Hotfix_set_MineralSignalReduce;
        private static DelegateBridge __Hotfix_get_TransportSignalReduce;
        private static DelegateBridge __Hotfix_set_TransportSignalReduce;
        private static DelegateBridge __Hotfix_get_IconResString;
        private static DelegateBridge __Hotfix_set_IconResString;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Difficult", DataFormat=DataFormat.TwosComplement)]
        public int Difficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="MinPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int MinPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MaxPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int MaxPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="FightSignalReduce", DataFormat=DataFormat.FixedSize)]
        public float FightSignalReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="MineralSignalReduce", DataFormat=DataFormat.FixedSize)]
        public float MineralSignalReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="TransportSignalReduce", DataFormat=DataFormat.FixedSize)]
        public float TransportSignalReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="IconResString", DataFormat=DataFormat.Default)]
        public string IconResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

