﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SubRankType")]
    public enum SubRankType
    {
        [ProtoEnum(Name="SubRankType_Invalid", Value=0)]
        SubRankType_Invalid = 0,
        [ProtoEnum(Name="SubRankType_S1", Value=1)]
        SubRankType_S1 = 1,
        [ProtoEnum(Name="SubRankType_S2", Value=2)]
        SubRankType_S2 = 2,
        [ProtoEnum(Name="SubRankType_S3", Value=3)]
        SubRankType_S3 = 3,
        [ProtoEnum(Name="SubRankType_S4", Value=4)]
        SubRankType_S4 = 4,
        [ProtoEnum(Name="SubRankType_S5", Value=5)]
        SubRankType_S5 = 5,
        [ProtoEnum(Name="SubRankType_Max", Value=6)]
        SubRankType_Max = 6
    }
}

