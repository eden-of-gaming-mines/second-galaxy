﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GEAtmosphereDensity")]
    public enum GEAtmosphereDensity
    {
        [ProtoEnum(Name="GEAtmosphereDensity_Invalid", Value=0)]
        GEAtmosphereDensity_Invalid = 0,
        [ProtoEnum(Name="GEAtmosphereDensity_ZeroDensity", Value=1)]
        GEAtmosphereDensity_ZeroDensity = 1,
        [ProtoEnum(Name="GEAtmosphereDensity_LowDensity", Value=2)]
        GEAtmosphereDensity_LowDensity = 2,
        [ProtoEnum(Name="GEAtmosphereDensity_NormalDensity", Value=3)]
        GEAtmosphereDensity_NormalDensity = 3,
        [ProtoEnum(Name="GEAtmosphereDensity_HighDensity", Value=4)]
        GEAtmosphereDensity_HighDensity = 4
    }
}

