﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="UserGuideTriggerPoint")]
    public enum UserGuideTriggerPoint
    {
        [ProtoEnum(Name="UserGuideTriggerPoint_SolarSystem", Value=1)]
        UserGuideTriggerPoint_SolarSystem = 1,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterScene", Value=2)]
        UserGuideTriggerPoint_EnterScene = 2,
        [ProtoEnum(Name="UserGuideTriggerPoint_NpcChatEnd", Value=3)]
        UserGuideTriggerPoint_NpcChatEnd = 3,
        [ProtoEnum(Name="UserGuideTriggerPoint_AcceptQuest", Value=4)]
        UserGuideTriggerPoint_AcceptQuest = 4,
        [ProtoEnum(Name="UserGuideTriggerPoint_TextNotice", Value=5)]
        UserGuideTriggerPoint_TextNotice = 5,
        [ProtoEnum(Name="UserGuideTriggerPoint_CreateCharacterEnd", Value=6)]
        UserGuideTriggerPoint_CreateCharacterEnd = 6,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStation", Value=7)]
        UserGuideTriggerPoint_EnterStation = 7,
        [ProtoEnum(Name="UserGuideTriggerPoint_OpenQuestList", Value=8)]
        UserGuideTriggerPoint_OpenQuestList = 8,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterCharacterInfoUI", Value=9)]
        UserGuideTriggerPoint_EnterCharacterInfoUI = 9,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterDrivingLicenseUI", Value=10)]
        UserGuideTriggerPoint_EnterDrivingLicenseUI = 10,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterDrivingUpgradeUI", Value=11)]
        UserGuideTriggerPoint_EnterDrivingUpgradeUI = 11,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterDrivingLVFullUI", Value=12)]
        UserGuideTriggerPoint_EnterDrivingLVFullUI = 12,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterCrackUI", Value=13)]
        UserGuideTriggerPoint_EnterCrackUI = 13,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterCharacterSkillUI", Value=14)]
        UserGuideTriggerPoint_EnterCharacterSkillUI = 14,
        [ProtoEnum(Name="UserGuideTriggerPoint_QuestWaitForAccept", Value=15)]
        UserGuideTriggerPoint_QuestWaitForAccept = 15,
        [ProtoEnum(Name="UserGuideTriggerPoint_OpenQuestAcceptDialog", Value=0x10)]
        UserGuideTriggerPoint_OpenQuestAcceptDialog = 0x10,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterShipHangarUI", Value=0x11)]
        UserGuideTriggerPoint_EnterShipHangarUI = 0x11,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterShipHangarConfigUI", Value=0x12)]
        UserGuideTriggerPoint_EnterShipHangarConfigUI = 0x12,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterSkillDetailInfoUI", Value=0x13)]
        UserGuideTriggerPoint_EnterSkillDetailInfoUI = 0x13,
        [ProtoEnum(Name="UserGuideTriggerPoint_SimpleDialogEnd", Value=20)]
        UserGuideTriggerPoint_SimpleDialogEnd = 20,
        [ProtoEnum(Name="UserGuideTriggerPoint_QuestFinished", Value=0x15)]
        UserGuideTriggerPoint_QuestFinished = 0x15,
        [ProtoEnum(Name="UserGuideTriggerPoint_MotherShipUIIsOpen", Value=0x16)]
        UserGuideTriggerPoint_MotherShipUIIsOpen = 0x16,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterDrivingCanNotLearnUI", Value=0x17)]
        UserGuideTriggerPoint_EnterDrivingCanNotLearnUI = 0x17,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainTrainingUI", Value=0x18)]
        UserGuideTriggerPoint_CaptainTrainingUI = 0x18,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainListUI", Value=0x19)]
        UserGuideTriggerPoint_CaptainListUI = 0x19,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainUI", Value=0x1a)]
        UserGuideTriggerPoint_CaptainUI = 0x1a,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainShipUI", Value=0x1b)]
        UserGuideTriggerPoint_CaptainShipUI = 0x1b,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterProduceUI", Value=0x1c)]
        UserGuideTriggerPoint_EnterProduceUI = 0x1c,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterProduceBlueprintItemStore", Value=0x1d)]
        UserGuideTriggerPoint_EnterProduceBlueprintItemStore = 0x1d,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStarMapForSolarSytemUI", Value=30)]
        UserGuideTriggerPoint_EnterStarMapForSolarSytemUI = 30,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStarMapForStarFieldUI", Value=0x1f)]
        UserGuideTriggerPoint_EnterStarMapForStarFieldUI = 0x1f,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStarMapUI", Value=0x20)]
        UserGuideTriggerPoint_EnterStarMapUI = 0x20,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterActivityUI", Value=0x21)]
        UserGuideTriggerPoint_EnterActivityUI = 0x21,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterSignalStrikeUI", Value=0x22)]
        UserGuideTriggerPoint_EnterSignalStrikeUI = 0x22,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTechUI", Value=0x23)]
        UserGuideTriggerPoint_EnterTechUI = 0x23,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTechUpgradeUI", Value=0x24)]
        UserGuideTriggerPoint_EnterTechUpgradeUI = 0x24,
        [ProtoEnum(Name="UserGuideTriggerPoint_ChapterQuestCutsceneEnd", Value=0x25)]
        UserGuideTriggerPoint_ChapterQuestCutsceneEnd = 0x25,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterWeaponEquipSetUI", Value=0x26)]
        UserGuideTriggerPoint_EnterWeaponEquipSetUI = 0x26,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterDrivingLicenseCheckUI", Value=0x27)]
        UserGuideTriggerPoint_EnterDrivingLicenseCheckUI = 0x27,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStrikeUI", Value=40)]
        UserGuideTriggerPoint_EnterStrikeUI = 40,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterShipAssignUI", Value=0x29)]
        UserGuideTriggerPoint_EnterShipAssignUI = 0x29,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterActivityInfoUI", Value=0x2a)]
        UserGuideTriggerPoint_EnterActivityInfoUI = 0x2a,
        [ProtoEnum(Name="UserGuideTriggerPoint_QuestAcceptDialogLastDialog", Value=0x2b)]
        UserGuideTriggerPoint_QuestAcceptDialogLastDialog = 0x2b,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterNpcListDialogUI", Value=0x2c)]
        UserGuideTriggerPoint_EnterNpcListDialogUI = 0x2c,
        [ProtoEnum(Name="UserGuideTriggerPoint_OnNpcDialogEnd", Value=0x2d)]
        UserGuideTriggerPoint_OnNpcDialogEnd = 0x2d,
        [ProtoEnum(Name="UserGuideTriggerPoint_FreeQuestFinished", Value=0x2e)]
        UserGuideTriggerPoint_FreeQuestFinished = 0x2e,
        [ProtoEnum(Name="UserGuideTriggerPoint_QuestWaitForComplete", Value=0x2f)]
        UserGuideTriggerPoint_QuestWaitForComplete = 0x2f,
        [ProtoEnum(Name="UserGuideTriggerPoint_OnEmergencySingalAdd", Value=0x30)]
        UserGuideTriggerPoint_OnEmergencySingalAdd = 0x30,
        [ProtoEnum(Name="UserGuideTriggerPoint_OnFuntionOpenAnimationEnd", Value=0x31)]
        UserGuideTriggerPoint_OnFuntionOpenAnimationEnd = 0x31,
        [ProtoEnum(Name="UserGuideTriggerPoint_SolarSystemTick", Value=50)]
        UserGuideTriggerPoint_SolarSystemTick = 50,
        [ProtoEnum(Name="UserGuideTriggerPoint_OnTargetDeath", Value=0x33)]
        UserGuideTriggerPoint_OnTargetDeath = 0x33,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterItemStore", Value=0x34)]
        UserGuideTriggerPoint_EnterItemStore = 0x34,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterWormholeTeleportConfirmInfoUI", Value=0x35)]
        UserGuideTriggerPoint_EnterWormholeTeleportConfirmInfoUI = 0x35,
        [ProtoEnum(Name="UserGuideTriggerPoint_OnPuzzleMatchComplete", Value=0x36)]
        UserGuideTriggerPoint_OnPuzzleMatchComplete = 0x36,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterAuction", Value=0x37)]
        UserGuideTriggerPoint_EnterAuction = 0x37,
        [ProtoEnum(Name="UserGuideTriggerPoint_UseHighSlotWeapon", Value=0x38)]
        UserGuideTriggerPoint_UseHighSlotWeapon = 0x38,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainRetireUI", Value=0x39)]
        UserGuideTriggerPoint_CaptainRetireUI = 0x39,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainLearningUI", Value=0x3a)]
        UserGuideTriggerPoint_CaptainLearningUI = 0x3a,
        [ProtoEnum(Name="UserGuideTriggerPoint_AuctionBuyUI", Value=0x3b)]
        UserGuideTriggerPoint_AuctionBuyUI = 0x3b,
        [ProtoEnum(Name="UserGuideTriggerPoint_AuctionSaleUI", Value=60)]
        UserGuideTriggerPoint_AuctionSaleUI = 60,
        [ProtoEnum(Name="UserGuideTriggerPoint_CaptainSetWingManUI", Value=0x3d)]
        UserGuideTriggerPoint_CaptainSetWingManUI = 0x3d,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterShipAmmoSetUI", Value=0x3e)]
        UserGuideTriggerPoint_EnterShipAmmoSetUI = 0x3e,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterCharacterAddPointUI", Value=0x3f)]
        UserGuideTriggerPoint_EnterCharacterAddPointUI = 0x3f,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildPurchaseUI", Value=0x40)]
        UserGuideTriggerPoint_EnterGuildPurchaseUI = 0x40,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildSolarSystemUI", Value=0x41)]
        UserGuideTriggerPoint_EnterGuildSolarSystemUI = 0x41,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildProductionUI", Value=0x42)]
        UserGuideTriggerPoint_EnterGuildProductionUI = 0x42,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterNPCShopUI", Value=0x43)]
        UserGuideTriggerPoint_EnterNPCShopUI = 0x43,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildInfoUI", Value=0x44)]
        UserGuideTriggerPoint_EnterGuildInfoUI = 0x44,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTechUISelected", Value=0x45)]
        UserGuideTriggerPoint_EnterTechUISelected = 0x45,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildFlagShipHangarUI", Value=70)]
        UserGuideTriggerPoint_EnterGuildFlagShipHangarUI = 70,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildFleetManagerUI", Value=0x47)]
        UserGuideTriggerPoint_EnterGuildFleetManagerUI = 0x47,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildFleetDetailUI", Value=0x48)]
        UserGuideTriggerPoint_EnterGuildFleetDetailUI = 0x48,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterStrikeUI_Captain", Value=0x49)]
        UserGuideTriggerPoint_EnterStrikeUI_Captain = 0x49,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTradeUI_PersonalBuy", Value=0x4a)]
        UserGuideTriggerPoint_EnterTradeUI_PersonalBuy = 0x4a,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTradeUI_PersonalSell", Value=0x4b)]
        UserGuideTriggerPoint_EnterTradeUI_PersonalSell = 0x4b,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTradeUI_GuildSell", Value=0x4c)]
        UserGuideTriggerPoint_EnterTradeUI_GuildSell = 0x4c,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterTradeUI_GuildPort", Value=0x4d)]
        UserGuideTriggerPoint_EnterTradeUI_GuildPort = 0x4d,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterFactionUI", Value=0x4e)]
        UserGuideTriggerPoint_EnterFactionUI = 0x4e,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterFactionNpcUI", Value=0x4f)]
        UserGuideTriggerPoint_EnterFactionNpcUI = 0x4f,
        [ProtoEnum(Name="UserGuideTriggerPoint_BranchStoryPuzzleOpen", Value=80)]
        UserGuideTriggerPoint_BranchStoryPuzzleOpen = 80,
        [ProtoEnum(Name="UserGuideTriggerPoint_BranchStoryPuzzleProcessing", Value=0x51)]
        UserGuideTriggerPoint_BranchStoryPuzzleProcessing = 0x51,
        [ProtoEnum(Name="UserGuideTriggerPoint_BranchStoryPuzzleRealProcessing", Value=0x52)]
        UserGuideTriggerPoint_BranchStoryPuzzleRealProcessing = 0x52,
        [ProtoEnum(Name="UserGuideTriggerPoint_DevelopmentOpen", Value=0x53)]
        UserGuideTriggerPoint_DevelopmentOpen = 0x53,
        [ProtoEnum(Name="UserGuideTriggerPoint_DevelopmentTransformationPanel", Value=0x54)]
        UserGuideTriggerPoint_DevelopmentTransformationPanel = 0x54,
        [ProtoEnum(Name="UserGuideTriggerPoint_EnterGuildBenefitsUI", Value=0x55)]
        UserGuideTriggerPoint_EnterGuildBenefitsUI = 0x55
    }
}

