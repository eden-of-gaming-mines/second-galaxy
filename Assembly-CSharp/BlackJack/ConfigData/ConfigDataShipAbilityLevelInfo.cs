﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipAbilityLevelInfo")]
    public class ConfigDataShipAbilityLevelInfo : IExtensible
    {
        private int _ID;
        private int _Attack;
        private int _Range;
        private int _Defence;
        private int _Speed;
        private int _Energy;
        private int _Collect;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Attack;
        private static DelegateBridge __Hotfix_set_Attack;
        private static DelegateBridge __Hotfix_get_Range;
        private static DelegateBridge __Hotfix_set_Range;
        private static DelegateBridge __Hotfix_get_Defence;
        private static DelegateBridge __Hotfix_set_Defence;
        private static DelegateBridge __Hotfix_get_Speed;
        private static DelegateBridge __Hotfix_set_Speed;
        private static DelegateBridge __Hotfix_get_Energy;
        private static DelegateBridge __Hotfix_set_Energy;
        private static DelegateBridge __Hotfix_get_Collect;
        private static DelegateBridge __Hotfix_set_Collect;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Attack", DataFormat=DataFormat.TwosComplement)]
        public int Attack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Range", DataFormat=DataFormat.TwosComplement)]
        public int Range
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Defence", DataFormat=DataFormat.TwosComplement)]
        public int Defence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Speed", DataFormat=DataFormat.TwosComplement)]
        public int Speed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Energy", DataFormat=DataFormat.TwosComplement)]
        public int Energy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Collect", DataFormat=DataFormat.TwosComplement)]
        public int Collect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

