﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SystemMessageId")]
    public enum SystemMessageId
    {
        [ProtoEnum(Name="SystemMessageId_OpenInfectFinalBattle", Value=1)]
        SystemMessageId_OpenInfectFinalBattle = 1,
        [ProtoEnum(Name="SystemMessageId_InfectSelfHealingAlter", Value=2)]
        SystemMessageId_InfectSelfHealingAlter = 2,
        [ProtoEnum(Name="SystemMessageId_CurrSolarSystemInfectFinalBattleStart", Value=3)]
        SystemMessageId_CurrSolarSystemInfectFinalBattleStart = 3,
        [ProtoEnum(Name="SystemMessageId_CurrSolarSystemInfectFinalBattleShieldLeft", Value=4)]
        SystemMessageId_CurrSolarSystemInfectFinalBattleShieldLeft = 4,
        [ProtoEnum(Name="SystemMessageId_CurrSolarSystemInfectFinalBattleArmorLeft", Value=5)]
        SystemMessageId_CurrSolarSystemInfectFinalBattleArmorLeft = 5,
        [ProtoEnum(Name="SystemMessageId_CurrSolarSystemInfectFinalBattleEnd", Value=6)]
        SystemMessageId_CurrSolarSystemInfectFinalBattleEnd = 6,
        [ProtoEnum(Name="SystemMessageId_SepcailWormholeSceneCreate", Value=7)]
        SystemMessageId_SepcailWormholeSceneCreate = 7,
        [ProtoEnum(Name="SystemMessageId_WormholePlayerKillByOtherPlayer", Value=8)]
        SystemMessageId_WormholePlayerKillByOtherPlayer = 8,
        [ProtoEnum(Name="SystemMessageId_NormalSolarSystemPlayerKillByOtherPlayer", Value=9)]
        SystemMessageId_NormalSolarSystemPlayerKillByOtherPlayer = 9,
        [ProtoEnum(Name="SystemMessageId_ContinousCriminalSet", Value=10)]
        SystemMessageId_ContinousCriminalSet = 10,
        [ProtoEnum(Name="SystemMessageId_HighQunityShipProduce", Value=11)]
        SystemMessageId_HighQunityShipProduce = 11,
        [ProtoEnum(Name="SystemMessageId_HighQunityWeaponEquipProduce", Value=12)]
        SystemMessageId_HighQunityWeaponEquipProduce = 12,
        [ProtoEnum(Name="SystemMessageId_WormholeSolarSystemPlayerEnter", Value=13)]
        SystemMessageId_WormholeSolarSystemPlayerEnter = 13,
        [ProtoEnum(Name="SystemMessageId_WormholeSolarSystemPlayerTeleport", Value=14)]
        SystemMessageId_WormholeSolarSystemPlayerTeleport = 14,
        [ProtoEnum(Name="SystemMessageId_WormholeSolarSystemPlayerLeave", Value=15)]
        SystemMessageId_WormholeSolarSystemPlayerLeave = 15,
        [ProtoEnum(Name="SystemMessageId_GMServerPushAnnouncement", Value=0x10)]
        SystemMessageId_GMServerPushAnnouncement = 0x10,
        [ProtoEnum(Name="SystemMessageId_GuildNameChange", Value=0x11)]
        SystemMessageId_GuildNameChange = 0x11,
        [ProtoEnum(Name="SystemMessageId_GuildCodeChange", Value=0x12)]
        SystemMessageId_GuildCodeChange = 0x12,
        [ProtoEnum(Name="SystemMessageId_GuildMemberJoinConfirm", Value=0x13)]
        SystemMessageId_GuildMemberJoinConfirm = 0x13,
        [ProtoEnum(Name="SystemMessageId_GuildMemberAutoJoin", Value=20)]
        SystemMessageId_GuildMemberAutoJoin = 20,
        [ProtoEnum(Name="SystemMessageId_GuildMemberLeave", Value=0x15)]
        SystemMessageId_GuildMemberLeave = 0x15,
        [ProtoEnum(Name="SystemMessageId_GuildMemberKick", Value=0x16)]
        SystemMessageId_GuildMemberKick = 0x16,
        [ProtoEnum(Name="SystemMessageId_GuildDiplomacyFriendlyAdd", Value=0x17)]
        SystemMessageId_GuildDiplomacyFriendlyAdd = 0x17,
        [ProtoEnum(Name="SystemMessageId_GuildDiplomacyRemove", Value=0x18)]
        SystemMessageId_GuildDiplomacyRemove = 0x18,
        [ProtoEnum(Name="SystemMessageId_GuildDiplomacyEnemyAdd", Value=0x19)]
        SystemMessageId_GuildDiplomacyEnemyAdd = 0x19,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogMemberJoinConfirm", Value=0x1a)]
        SystemMessageId_GuildStaffingLogMemberJoinConfirm = 0x1a,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogMemberAutoAdd", Value=0x1b)]
        SystemMessageId_GuildStaffingLogMemberAutoAdd = 0x1b,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogJobAppoint", Value=0x1c)]
        SystemMessageId_GuildStaffingLogJobAppoint = 0x1c,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogJobFire", Value=0x1d)]
        SystemMessageId_GuildStaffingLogJobFire = 0x1d,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogMemberLeave", Value=30)]
        SystemMessageId_GuildStaffingLogMemberLeave = 30,
        [ProtoEnum(Name="SystemMessageId_GuildStaffingLogMemberKick", Value=0x1f)]
        SystemMessageId_GuildStaffingLogMemberKick = 0x1f,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleStartForAttackerGuild", Value=0x20)]
        SystemMessageId_GuildBuildingBattleStartForAttackerGuild = 0x20,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleStartForDefenderGuild", Value=0x21)]
        SystemMessageId_GuildBuildingBattleStartForDefenderGuild = 0x21,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleStartForSolarSystem", Value=0x22)]
        SystemMessageId_GuildBuildingBattleStartForSolarSystem = 0x22,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleFailForAttackerGuild", Value=0x23)]
        SystemMessageId_GuildBuildingBattleFailForAttackerGuild = 0x23,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleFailForDefenderGuild", Value=0x24)]
        SystemMessageId_GuildBuildingBattleFailForDefenderGuild = 0x24,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleSuccessForAttackerGuild", Value=0x25)]
        SystemMessageId_GuildBuildingBattleSuccessForAttackerGuild = 0x25,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleSuccessForDefenderGuild", Value=0x26)]
        SystemMessageId_GuildBuildingBattleSuccessForDefenderGuild = 0x26,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEnterReinforcementForAttackerGuild", Value=0x27)]
        SystemMessageId_GuildBuildingBattleEnterReinforcementForAttackerGuild = 0x27,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEnterReinforcementForDefenderGuild", Value=40)]
        SystemMessageId_GuildBuildingBattleEnterReinforcementForDefenderGuild = 40,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForAttackerGuild", Value=0x29)]
        SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForAttackerGuild = 0x29,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForDefenderGuild", Value=0x2a)]
        SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForDefenderGuild = 0x2a,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForAttackerGuild", Value=0x2b)]
        SystemMessageId_GuildSovereignBattleStartForAttackerGuild = 0x2b,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForDefenderGuild", Value=0x2c)]
        SystemMessageId_GuildSovereignBattleStartForDefenderGuild = 0x2c,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForStarField", Value=0x2d)]
        SystemMessageId_GuildSovereignBattleStartForStarField = 0x2d,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForSolarSystem", Value=0x2e)]
        SystemMessageId_GuildSovereignBattleStartForSolarSystem = 0x2e,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePrepareEndOneMinuts", Value=0x2f)]
        SystemMessageId_GuildSovereignBattlePrepareEndOneMinuts = 0x2f,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePrepareEnd30Second", Value=0x30)]
        SystemMessageId_GuildSovereignBattlePrepareEnd30Second = 0x30,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAttackBuildingShieldLow", Value=0x31)]
        SystemMessageId_GuildSovereignBattleAttackBuildingShieldLow = 0x31,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAttackBuildingArmorLow", Value=50)]
        SystemMessageId_GuildSovereignBattleAttackBuildingArmorLow = 50,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAttackBuildingDestory", Value=0x33)]
        SystemMessageId_GuildSovereignBattleAttackBuildingDestory = 0x33,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleFail", Value=0x34)]
        SystemMessageId_GuildSovereignBattleFail = 0x34,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleDefenderBuildingShieldLow", Value=0x35)]
        SystemMessageId_GuildSovereignBattleDefenderBuildingShieldLow = 0x35,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleDefenderBuildingArmorLow", Value=0x36)]
        SystemMessageId_GuildSovereignBattleDefenderBuildingArmorLow = 0x36,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleDefenderBuildingDestory", Value=0x37)]
        SystemMessageId_GuildSovereignBattleDefenderBuildingDestory = 0x37,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterHalfReinforcement", Value=0x38)]
        SystemMessageId_GuildSovereignBattleEnterHalfReinforcement = 0x38,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterReinforcementForAttackerGuild", Value=0x39)]
        SystemMessageId_GuildSovereignBattleEnterReinforcementForAttackerGuild = 0x39,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterReinforcementForDefenderGuild", Value=0x3a)]
        SystemMessageId_GuildSovereignBattleEnterReinforcementForDefenderGuild = 0x3a,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePlayerDeclaration", Value=0x3b)]
        SystemMessageId_GuildSovereignBattlePlayerDeclaration = 0x3b,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAutoDeclaration", Value=60)]
        SystemMessageId_GuildSovereignBattleAutoDeclaration = 60,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceStart", Value=0x3d)]
        SystemMessageId_GuildMiningBalanceStart = 0x3d,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceEnd", Value=0x3e)]
        SystemMessageId_GuildMiningBalanceEnd = 0x3e,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceShipBeAttacked", Value=0x3f)]
        SystemMessageId_GuildMiningBalanceShipBeAttacked = 0x3f,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceShipMiddleShield", Value=0x40)]
        SystemMessageId_GuildMiningBalanceShipMiddleShield = 0x40,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceShipLowShield", Value=0x41)]
        SystemMessageId_GuildMiningBalanceShipLowShield = 0x41,
        [ProtoEnum(Name="SystemMessageId_GuildActionStart", Value=0x42)]
        SystemMessageId_GuildActionStart = 0x42,
        [ProtoEnum(Name="SystemMessageId_GuildActionComplete", Value=0x43)]
        SystemMessageId_GuildActionComplete = 0x43,
        [ProtoEnum(Name="SystemMessageId_GuildBattleKickForPlayerLimitCountDown", Value=0x44)]
        SystemMessageId_GuildBattleKickForPlayerLimitCountDown = 0x44,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterReinforcementForSolarSystem", Value=0x45)]
        SystemMessageId_GuildSovereignBattleEnterReinforcementForSolarSystem = 0x45,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterDeclarationForSolarSystem", Value=70)]
        SystemMessageId_GuildSovereignBattleEnterDeclarationForSolarSystem = 70,
        [ProtoEnum(Name="SystemMessageId_GuildMiningBalanceShipBeDestroied", Value=0x47)]
        SystemMessageId_GuildMiningBalanceShipBeDestroied = 0x47,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePlayerDeclarationForSolarSystem", Value=0x48)]
        SystemMessageId_GuildSovereignBattlePlayerDeclarationForSolarSystem = 0x48,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAutoDeclarationForSolarSystem", Value=0x49)]
        SystemMessageId_GuildSovereignBattleAutoDeclarationForSolarSystem = 0x49,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleFailForSolarSystem", Value=0x4a)]
        SystemMessageId_GuildSovereignBattleFailForSolarSystem = 0x4a,
        [ProtoEnum(Name="SystemMessageId_GuildActionStartSimpleVersion", Value=0x4b)]
        SystemMessageId_GuildActionStartSimpleVersion = 0x4b,
        [ProtoEnum(Name="SystemMessageId_GuildActionCompleteSimpleVersion", Value=0x4c)]
        SystemMessageId_GuildActionCompleteSimpleVersion = 0x4c,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleStartForAttackerGuildSimpleVersion", Value=0x4d)]
        SystemMessageId_GuildBuildingBattleStartForAttackerGuildSimpleVersion = 0x4d,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleStartForDefenderGuildSimpleVersion", Value=0x4e)]
        SystemMessageId_GuildBuildingBattleStartForDefenderGuildSimpleVersion = 0x4e,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForAttackerGuildSimpleVersion", Value=0x4f)]
        SystemMessageId_GuildSovereignBattleStartForAttackerGuildSimpleVersion = 0x4f,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleStartForDefenderGuildSimpleVersion", Value=80)]
        SystemMessageId_GuildSovereignBattleStartForDefenderGuildSimpleVersion = 80,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterReinforcementForDefenderGuildSimpleVersion", Value=0x51)]
        SystemMessageId_GuildSovereignBattleEnterReinforcementForDefenderGuildSimpleVersion = 0x51,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleFailForAttackerGuildSimpleVersion", Value=0x52)]
        SystemMessageId_GuildBuildingBattleFailForAttackerGuildSimpleVersion = 0x52,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleFailForDefenderGuildSimpleVersion", Value=0x53)]
        SystemMessageId_GuildBuildingBattleFailForDefenderGuildSimpleVersion = 0x53,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleFailSimpleVersion", Value=0x54)]
        SystemMessageId_GuildSovereignBattleFailSimpleVersion = 0x54,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleSuccessForAttackerGuildSimpleVersion", Value=0x55)]
        SystemMessageId_GuildBuildingBattleSuccessForAttackerGuildSimpleVersion = 0x55,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleSuccessForDefenderGuildSimpleVersion", Value=0x56)]
        SystemMessageId_GuildBuildingBattleSuccessForDefenderGuildSimpleVersion = 0x56,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForAttackerGuildSimpleVersion", Value=0x57)]
        SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForAttackerGuildSimpleVersion = 0x57,
        [ProtoEnum(Name="SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForDefenderGuildSimpleVersion", Value=0x58)]
        SystemMessageId_GuildBuildingBattleEndBecauseSovereignChangeForDefenderGuildSimpleVersion = 0x58,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePlayerDeclarationSimpleVersion", Value=0x59)]
        SystemMessageId_GuildSovereignBattlePlayerDeclarationSimpleVersion = 0x59,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAutoDeclarationSimpleVersion", Value=90)]
        SystemMessageId_GuildSovereignBattleAutoDeclarationSimpleVersion = 90,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePlayerDeclarationStart", Value=0x5b)]
        SystemMessageId_GuildSovereignBattlePlayerDeclarationStart = 0x5b,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleEnterReinforcementForStarField", Value=0x5c)]
        SystemMessageId_GuildSovereignBattleEnterReinforcementForStarField = 0x5c,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattlePlayerDeclarationForStarField", Value=0x5d)]
        SystemMessageId_GuildSovereignBattlePlayerDeclarationForStarField = 0x5d,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleAutoDeclarationForStarField", Value=0x5e)]
        SystemMessageId_GuildSovereignBattleAutoDeclarationForStarField = 0x5e,
        [ProtoEnum(Name="SystemMessageId_GuildSovereignBattleFailForStarField", Value=0x5f)]
        SystemMessageId_GuildSovereignBattleFailForStarField = 0x5f,
        [ProtoEnum(Name="SystemMessageId_GuildFlagShipTeleportEnd", Value=0x60)]
        SystemMessageId_GuildFlagShipTeleportEnd = 0x60,
        [ProtoEnum(Name="SystemMessageId_GuildTradeShipTeleportToSolarSystem", Value=0x61)]
        SystemMessageId_GuildTradeShipTeleportToSolarSystem = 0x61,
        [ProtoEnum(Name="SystemMessageId_SolarSystemReloadAnnouncement", Value=0x62)]
        SystemMessageId_SolarSystemReloadAnnouncement = 0x62
    }
}

