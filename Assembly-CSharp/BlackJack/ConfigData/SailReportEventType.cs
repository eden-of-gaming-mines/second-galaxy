﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SailReportEventType")]
    public enum SailReportEventType
    {
        [ProtoEnum(Name="SailReportEventType_PvpFight", Value=1)]
        SailReportEventType_PvpFight = 1,
        [ProtoEnum(Name="SailReportEventType_StoryQuest", Value=2)]
        SailReportEventType_StoryQuest = 2,
        [ProtoEnum(Name="SailReportEventType_FreeSignalQuest", Value=3)]
        SailReportEventType_FreeSignalQuest = 3,
        [ProtoEnum(Name="SailReportEventType_EnterWormHole", Value=4)]
        SailReportEventType_EnterWormHole = 4,
        [ProtoEnum(Name="SailReportEventType_InfectQuest", Value=5)]
        SailReportEventType_InfectQuest = 5,
        [ProtoEnum(Name="SailReportEventType_InfectFinalBattle", Value=6)]
        SailReportEventType_InfectFinalBattle = 6,
        [ProtoEnum(Name="SailReportEventType_ExploreQuest", Value=7)]
        SailReportEventType_ExploreQuest = 7,
        [ProtoEnum(Name="SailReportEventType_Death", Value=8)]
        SailReportEventType_Death = 8,
        [ProtoEnum(Name="SailReportEventType_DrivingLicense", Value=9)]
        SailReportEventType_DrivingLicense = 9,
        [ProtoEnum(Name="SailReportEventType_Invade", Value=10)]
        SailReportEventType_Invade = 10,
        [ProtoEnum(Name="SailReportEventType_GuildAction", Value=11)]
        SailReportEventType_GuildAction = 11
    }
}

