﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GrandFaction")]
    public enum GrandFaction
    {
        [ProtoEnum(Name="GrandFaction_Invalid", Value=0)]
        GrandFaction_Invalid = 0,
        [ProtoEnum(Name="GrandFaction_ECD", Value=1)]
        GrandFaction_ECD = 1,
        [ProtoEnum(Name="GrandFaction_NEF", Value=2)]
        GrandFaction_NEF = 2,
        [ProtoEnum(Name="GrandFaction_RS", Value=3)]
        GrandFaction_RS = 3,
        [ProtoEnum(Name="GrandFaction_OE", Value=4)]
        GrandFaction_OE = 4,
        [ProtoEnum(Name="GrandFaction_USG", Value=5)]
        GrandFaction_USG = 5,
        [ProtoEnum(Name="GrandFaction_CIV", Value=6)]
        GrandFaction_CIV = 6,
        [ProtoEnum(Name="GrandFaction_TT", Value=7)]
        GrandFaction_TT = 7,
        [ProtoEnum(Name="GrandFaction_Max", Value=8)]
        GrandFaction_Max = 8
    }
}

