﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="PreDefineNpcCaptainFlag")]
    public enum PreDefineNpcCaptainFlag
    {
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_None", Value=0)]
        PreDefineNpcCaptainFlag_None = 0,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomSubRank", Value=1)]
        PreDefineNpcCaptainFlag_RandomSubRank = 1,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomName", Value=2)]
        PreDefineNpcCaptainFlag_RandomName = 2,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomAge", Value=4)]
        PreDefineNpcCaptainFlag_RandomAge = 4,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomBirthDay", Value=8)]
        PreDefineNpcCaptainFlag_RandomBirthDay = 8,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomShipGrowthLine", Value=0x10)]
        PreDefineNpcCaptainFlag_RandomShipGrowthLine = 0x10,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomInitFeats", Value=0x20)]
        PreDefineNpcCaptainFlag_RandomInitFeats = 0x20,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomPersionality", Value=0x40)]
        PreDefineNpcCaptainFlag_RandomPersionality = 0x40,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomGrandFaction", Value=0x80)]
        PreDefineNpcCaptainFlag_RandomGrandFaction = 0x80,
        [ProtoEnum(Name="PreDefineNpcCaptainFlag_RandomProfession", Value=0x100)]
        PreDefineNpcCaptainFlag_RandomProfession = 0x100
    }
}

