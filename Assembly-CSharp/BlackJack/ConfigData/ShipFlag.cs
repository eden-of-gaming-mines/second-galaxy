﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipFlag")]
    public enum ShipFlag
    {
        [ProtoEnum(Name="ShipFlag_StrikeWithoutDrivingLicense", Value=1)]
        ShipFlag_StrikeWithoutDrivingLicense = 1
    }
}

