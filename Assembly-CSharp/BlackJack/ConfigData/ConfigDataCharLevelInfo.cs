﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCharLevelInfo")]
    public class ConfigDataCharLevelInfo : IExtensible
    {
        private int _ID;
        private int _LevelUpExp;
        private int _CharacterScore;
        private int _EmergencySiganlDifficult;
        private int _DailyExpLimit;
        private int _AFKExpLimit;
        private float _ServerLevelFactor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LevelUpExp;
        private static DelegateBridge __Hotfix_set_LevelUpExp;
        private static DelegateBridge __Hotfix_get_CharacterScore;
        private static DelegateBridge __Hotfix_set_CharacterScore;
        private static DelegateBridge __Hotfix_get_EmergencySiganlDifficult;
        private static DelegateBridge __Hotfix_set_EmergencySiganlDifficult;
        private static DelegateBridge __Hotfix_get_DailyExpLimit;
        private static DelegateBridge __Hotfix_set_DailyExpLimit;
        private static DelegateBridge __Hotfix_get_AFKExpLimit;
        private static DelegateBridge __Hotfix_set_AFKExpLimit;
        private static DelegateBridge __Hotfix_get_ServerLevelFactor;
        private static DelegateBridge __Hotfix_set_ServerLevelFactor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LevelUpExp", DataFormat=DataFormat.TwosComplement)]
        public int LevelUpExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CharacterScore", DataFormat=DataFormat.TwosComplement)]
        public int CharacterScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EmergencySiganlDifficult", DataFormat=DataFormat.TwosComplement)]
        public int EmergencySiganlDifficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="DailyExpLimit", DataFormat=DataFormat.TwosComplement)]
        public int DailyExpLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="AFKExpLimit", DataFormat=DataFormat.TwosComplement)]
        public int AFKExpLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ServerLevelFactor", DataFormat=DataFormat.FixedSize)]
        public float ServerLevelFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

