﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainGrowthInfo")]
    public class ConfigDataNpcCaptainGrowthInfo : IExtensible
    {
        private int _ID;
        private ProfessionType _Profession;
        private SubRankType _SubRank;
        private float _InitPropertyBasicAttack;
        private float _InitPropertyBasicDefence;
        private float _InitPropertyBasicElectron;
        private float _InitPropertyBasicDrive;
        private float _BasicAttack4LevelUp;
        private float _BasicDefence4LevelUp;
        private float _BasicElectron4LevelUp;
        private float _BasicDrive4LevelUp;
        private float _RandomProperty4LevelUp;
        private float _WeightAttack;
        private float _WeightDefence;
        private float _WeightElectron;
        private float _WeightDrive;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_InitPropertyBasicAttack;
        private static DelegateBridge __Hotfix_set_InitPropertyBasicAttack;
        private static DelegateBridge __Hotfix_get_InitPropertyBasicDefence;
        private static DelegateBridge __Hotfix_set_InitPropertyBasicDefence;
        private static DelegateBridge __Hotfix_get_InitPropertyBasicElectron;
        private static DelegateBridge __Hotfix_set_InitPropertyBasicElectron;
        private static DelegateBridge __Hotfix_get_InitPropertyBasicDrive;
        private static DelegateBridge __Hotfix_set_InitPropertyBasicDrive;
        private static DelegateBridge __Hotfix_get_BasicAttack4LevelUp;
        private static DelegateBridge __Hotfix_set_BasicAttack4LevelUp;
        private static DelegateBridge __Hotfix_get_BasicDefence4LevelUp;
        private static DelegateBridge __Hotfix_set_BasicDefence4LevelUp;
        private static DelegateBridge __Hotfix_get_BasicElectron4LevelUp;
        private static DelegateBridge __Hotfix_set_BasicElectron4LevelUp;
        private static DelegateBridge __Hotfix_get_BasicDrive4LevelUp;
        private static DelegateBridge __Hotfix_set_BasicDrive4LevelUp;
        private static DelegateBridge __Hotfix_get_RandomProperty4LevelUp;
        private static DelegateBridge __Hotfix_set_RandomProperty4LevelUp;
        private static DelegateBridge __Hotfix_get_WeightAttack;
        private static DelegateBridge __Hotfix_set_WeightAttack;
        private static DelegateBridge __Hotfix_get_WeightDefence;
        private static DelegateBridge __Hotfix_set_WeightDefence;
        private static DelegateBridge __Hotfix_get_WeightElectron;
        private static DelegateBridge __Hotfix_set_WeightElectron;
        private static DelegateBridge __Hotfix_get_WeightDrive;
        private static DelegateBridge __Hotfix_set_WeightDrive;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public ProfessionType Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="InitPropertyBasicAttack", DataFormat=DataFormat.FixedSize)]
        public float InitPropertyBasicAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="InitPropertyBasicDefence", DataFormat=DataFormat.FixedSize)]
        public float InitPropertyBasicDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="InitPropertyBasicElectron", DataFormat=DataFormat.FixedSize)]
        public float InitPropertyBasicElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="InitPropertyBasicDrive", DataFormat=DataFormat.FixedSize)]
        public float InitPropertyBasicDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BasicAttack4LevelUp", DataFormat=DataFormat.FixedSize)]
        public float BasicAttack4LevelUp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="BasicDefence4LevelUp", DataFormat=DataFormat.FixedSize)]
        public float BasicDefence4LevelUp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="BasicElectron4LevelUp", DataFormat=DataFormat.FixedSize)]
        public float BasicElectron4LevelUp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="BasicDrive4LevelUp", DataFormat=DataFormat.FixedSize)]
        public float BasicDrive4LevelUp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="RandomProperty4LevelUp", DataFormat=DataFormat.FixedSize)]
        public float RandomProperty4LevelUp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="WeightAttack", DataFormat=DataFormat.FixedSize)]
        public float WeightAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="WeightDefence", DataFormat=DataFormat.FixedSize)]
        public float WeightDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="WeightElectron", DataFormat=DataFormat.FixedSize)]
        public float WeightElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="WeightDrive", DataFormat=DataFormat.FixedSize)]
        public float WeightDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

