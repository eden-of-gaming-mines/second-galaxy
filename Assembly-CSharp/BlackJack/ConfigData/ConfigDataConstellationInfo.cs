﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataConstellationInfo")]
    public class ConfigDataConstellationInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.ConstellationType _ConstellationType;
        private string _Icon;
        private int _StartMonth;
        private int _StartDate;
        private int _EndMonth;
        private int _EndDate;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ConstellationType;
        private static DelegateBridge __Hotfix_set_ConstellationType;
        private static DelegateBridge __Hotfix_get_Icon;
        private static DelegateBridge __Hotfix_set_Icon;
        private static DelegateBridge __Hotfix_get_StartMonth;
        private static DelegateBridge __Hotfix_set_StartMonth;
        private static DelegateBridge __Hotfix_get_StartDate;
        private static DelegateBridge __Hotfix_set_StartDate;
        private static DelegateBridge __Hotfix_get_EndMonth;
        private static DelegateBridge __Hotfix_set_EndMonth;
        private static DelegateBridge __Hotfix_get_EndDate;
        private static DelegateBridge __Hotfix_set_EndDate;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ConstellationType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.ConstellationType ConstellationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Icon", DataFormat=DataFormat.Default)]
        public string Icon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="StartMonth", DataFormat=DataFormat.TwosComplement)]
        public int StartMonth
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="StartDate", DataFormat=DataFormat.TwosComplement)]
        public int StartDate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="EndMonth", DataFormat=DataFormat.TwosComplement)]
        public int EndMonth
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="EndDate", DataFormat=DataFormat.TwosComplement)]
        public int EndDate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

