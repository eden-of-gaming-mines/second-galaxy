﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipEquipSlotType")]
    public enum ShipEquipSlotType
    {
        [ProtoEnum(Name="ShipEquipSlotType_High", Value=1)]
        ShipEquipSlotType_High = 1,
        [ProtoEnum(Name="ShipEquipSlotType_Middle", Value=2)]
        ShipEquipSlotType_Middle = 2,
        [ProtoEnum(Name="ShipEquipSlotType_Low", Value=3)]
        ShipEquipSlotType_Low = 3,
        [ProtoEnum(Name="ShipEquipSlotType_NormalAttack", Value=4)]
        ShipEquipSlotType_NormalAttack = 4,
        [ProtoEnum(Name="ShipEquipSlotType_DefaultBooster", Value=5)]
        ShipEquipSlotType_DefaultBooster = 5,
        [ProtoEnum(Name="ShipEquipSlotType_DefaultRepairer", Value=6)]
        ShipEquipSlotType_DefaultRepairer = 6,
        [ProtoEnum(Name="ShipEquipSlotType_Super", Value=7)]
        ShipEquipSlotType_Super = 7,
        [ProtoEnum(Name="ShipEquipSlotType_DefaultTactical", Value=8)]
        ShipEquipSlotType_DefaultTactical = 8
    }
}

