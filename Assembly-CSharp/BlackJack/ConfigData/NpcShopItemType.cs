﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcShopItemType")]
    public enum NpcShopItemType
    {
        [ProtoEnum(Name="NpcShopItemType_Invalid", Value=0)]
        NpcShopItemType_Invalid = 0,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_Frigate", Value=1)]
        NpcShopItemType_ShipBlueprint_Frigate = 1,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_Destroyer", Value=2)]
        NpcShopItemType_ShipBlueprint_Destroyer = 2,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_Cruiser", Value=3)]
        NpcShopItemType_ShipBlueprint_Cruiser = 3,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_BattleCruiser", Value=4)]
        NpcShopItemType_ShipBlueprint_BattleCruiser = 4,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_BattleShip", Value=5)]
        NpcShopItemType_ShipBlueprint_BattleShip = 5,
        [ProtoEnum(Name="NpcShopItemType_ShipBlueprint_Industry", Value=6)]
        NpcShopItemType_ShipBlueprint_Industry = 6,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Railgun", Value=7)]
        NpcShopItemType_WeaponBlueprint_Weapon_Railgun = 7,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Plasma", Value=8)]
        NpcShopItemType_WeaponBlueprint_Weapon_Plasma = 8,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Cannon", Value=9)]
        NpcShopItemType_WeaponBlueprint_Weapon_Cannon = 9,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Laser", Value=10)]
        NpcShopItemType_WeaponBlueprint_Weapon_Laser = 10,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Drone", Value=11)]
        NpcShopItemType_WeaponBlueprint_Weapon_Drone = 11,
        [ProtoEnum(Name="NpcShopItemType_WeaponBlueprint_Weapon_Missile", Value=12)]
        NpcShopItemType_WeaponBlueprint_Weapon_Missile = 12,
        [ProtoEnum(Name="NpcShopItemType_EquipBlueprint_Equip_HighSlot", Value=13)]
        NpcShopItemType_EquipBlueprint_Equip_HighSlot = 13,
        [ProtoEnum(Name="NpcShopItemType_EquipBlueprint_Equip_MiddleSlot", Value=14)]
        NpcShopItemType_EquipBlueprint_Equip_MiddleSlot = 14,
        [ProtoEnum(Name="NpcShopItemType_EquipBlueprint_Equip_LowSlot", Value=15)]
        NpcShopItemType_EquipBlueprint_Equip_LowSlot = 15,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprint_Ammo_Railgun", Value=0x10)]
        NpcShopItemType_AmmoBlueprint_Ammo_Railgun = 0x10,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprint_Ammo_Plasma", Value=0x11)]
        NpcShopItemType_AmmoBlueprint_Ammo_Plasma = 0x11,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprint_Ammo_Cannon", Value=0x12)]
        NpcShopItemType_AmmoBlueprint_Ammo_Cannon = 0x12,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprint_Ammo_Laser", Value=0x13)]
        NpcShopItemType_AmmoBlueprint_Ammo_Laser = 0x13,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprintt_Ammo_Drone", Value=20)]
        NpcShopItemType_AmmoBlueprintt_Ammo_Drone = 20,
        [ProtoEnum(Name="NpcShopItemType_AmmoBlueprint_Ammo_Missile", Value=0x15)]
        NpcShopItemType_AmmoBlueprint_Ammo_Missile = 0x15,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_SelfEnhancer", Value=0x16)]
        NpcShopItemType_DeviceBlueprint_Equip_SelfEnhancer = 0x16,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RemoteEnhancer", Value=0x17)]
        NpcShopItemType_DeviceBlueprint_Equip_RemoteEnhancer = 0x17,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RangeEnhancer", Value=0x18)]
        NpcShopItemType_DeviceBlueprint_Equip_RangeEnhancer = 0x18,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_SelfRecover", Value=0x19)]
        NpcShopItemType_DeviceBlueprint_Equip_SelfRecover = 0x19,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RemoteRecover", Value=0x1a)]
        NpcShopItemType_DeviceBlueprint_Equip_RemoteRecover = 0x1a,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RangeRecover", Value=0x1b)]
        NpcShopItemType_DeviceBlueprint_Equip_RangeRecover = 0x1b,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RemoteDisturber", Value=0x1c)]
        NpcShopItemType_DeviceBlueprint_Equip_RemoteDisturber = 0x1c,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_RangeDisturber", Value=0x1d)]
        NpcShopItemType_DeviceBlueprint_Equip_RangeDisturber = 0x1d,
        [ProtoEnum(Name="NpcShopItemType_DeviceBlueprint_Equip_TacticalEquipment", Value=30)]
        NpcShopItemType_DeviceBlueprint_Equip_TacticalEquipment = 30,
        [ProtoEnum(Name="NpcShopItemType_ComponentBlueprint_DamageEnhancer", Value=0x1f)]
        NpcShopItemType_ComponentBlueprint_DamageEnhancer = 0x1f,
        [ProtoEnum(Name="NpcShopItemType_ComponentBlueprint_ShootRangeEnhancer", Value=0x20)]
        NpcShopItemType_ComponentBlueprint_ShootRangeEnhancer = 0x20,
        [ProtoEnum(Name="NpcShopItemType_ComponentBlueprint_ElectronicEnhancer", Value=0x21)]
        NpcShopItemType_ComponentBlueprint_ElectronicEnhancer = 0x21,
        [ProtoEnum(Name="NpcShopItemType_ComponentBlueprint_ShieldEnhancer", Value=0x22)]
        NpcShopItemType_ComponentBlueprint_ShieldEnhancer = 0x22,
        [ProtoEnum(Name="NpcShopItemType_ComponentBlueprint_DrivingEnhancer", Value=0x23)]
        NpcShopItemType_ComponentBlueprint_DrivingEnhancer = 0x23,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Weapon", Value=0x24)]
        NpcShopItemType_OtherBlueprint_Module_Weapon = 0x24,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Defence", Value=0x25)]
        NpcShopItemType_OtherBlueprint_Module_Defence = 0x25,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Accelerate", Value=0x26)]
        NpcShopItemType_OtherBlueprint_Module_Accelerate = 0x26,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Energy", Value=0x27)]
        NpcShopItemType_OtherBlueprint_Module_Energy = 0x27,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Tactics", Value=40)]
        NpcShopItemType_OtherBlueprint_Module_Tactics = 40,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Collection", Value=0x29)]
        NpcShopItemType_OtherBlueprint_Module_Collection = 0x29,
        [ProtoEnum(Name="NpcShopItemType_OtherBlueprint_Module_Warehousing", Value=0x2a)]
        NpcShopItemType_OtherBlueprint_Module_Warehousing = 0x2a,
        [ProtoEnum(Name="NpcShopItemType_Ship_Frigate", Value=0x2b)]
        NpcShopItemType_Ship_Frigate = 0x2b,
        [ProtoEnum(Name="NpcShopItemType_Ship_Destroyer", Value=0x2c)]
        NpcShopItemType_Ship_Destroyer = 0x2c,
        [ProtoEnum(Name="NpcShopItemType_Ship_Cruiser", Value=0x2d)]
        NpcShopItemType_Ship_Cruiser = 0x2d,
        [ProtoEnum(Name="NpcShopItemType_Ship_BattleCruiser", Value=0x2e)]
        NpcShopItemType_Ship_BattleCruiser = 0x2e,
        [ProtoEnum(Name="NpcShopItemType_Ship_BattleShip", Value=0x2f)]
        NpcShopItemType_Ship_BattleShip = 0x2f,
        [ProtoEnum(Name="NpcShopItemType_Ship_Industry", Value=0x30)]
        NpcShopItemType_Ship_Industry = 0x30,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Railgun", Value=0x31)]
        NpcShopItemType_Weapon_Railgun = 0x31,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Plasma", Value=50)]
        NpcShopItemType_Weapon_Plasma = 50,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Cannon", Value=0x33)]
        NpcShopItemType_Weapon_Cannon = 0x33,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Laser", Value=0x34)]
        NpcShopItemType_Weapon_Laser = 0x34,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Drone", Value=0x35)]
        NpcShopItemType_Weapon_Drone = 0x35,
        [ProtoEnum(Name="NpcShopItemType_Weapon_Missile", Value=0x36)]
        NpcShopItemType_Weapon_Missile = 0x36,
        [ProtoEnum(Name="NpcShopItemType_Equip_HighSlot", Value=0x37)]
        NpcShopItemType_Equip_HighSlot = 0x37,
        [ProtoEnum(Name="NpcShopItemType_Equip_MiddleSlot", Value=0x38)]
        NpcShopItemType_Equip_MiddleSlot = 0x38,
        [ProtoEnum(Name="NpcShopItemType_Equip_LowSlot", Value=0x39)]
        NpcShopItemType_Equip_LowSlot = 0x39,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Railgun", Value=0x3a)]
        NpcShopItemType_Ammo_Railgun = 0x3a,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Plasma", Value=0x3b)]
        NpcShopItemType_Ammo_Plasma = 0x3b,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Cannon", Value=60)]
        NpcShopItemType_Ammo_Cannon = 60,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Laser", Value=0x3d)]
        NpcShopItemType_Ammo_Laser = 0x3d,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Drone", Value=0x3e)]
        NpcShopItemType_Ammo_Drone = 0x3e,
        [ProtoEnum(Name="NpcShopItemType_Ammo_Missile", Value=0x3f)]
        NpcShopItemType_Ammo_Missile = 0x3f,
        [ProtoEnum(Name="NpcShopItemType_Device_SelfEnhancer", Value=0x40)]
        NpcShopItemType_Device_SelfEnhancer = 0x40,
        [ProtoEnum(Name="NpcShopItemType_Device_RemoteEnhancer", Value=0x41)]
        NpcShopItemType_Device_RemoteEnhancer = 0x41,
        [ProtoEnum(Name="NpcShopItemType_Device_RangeEnhancer", Value=0x42)]
        NpcShopItemType_Device_RangeEnhancer = 0x42,
        [ProtoEnum(Name="NpcShopItemType_Device_SelfRecover", Value=0x43)]
        NpcShopItemType_Device_SelfRecover = 0x43,
        [ProtoEnum(Name="NpcShopItemType_Device_RemoteRecover", Value=0x44)]
        NpcShopItemType_Device_RemoteRecover = 0x44,
        [ProtoEnum(Name="NpcShopItemType_Device_RangeRecover", Value=0x45)]
        NpcShopItemType_Device_RangeRecover = 0x45,
        [ProtoEnum(Name="NpcShopItemType_Device_RemoteDisturber", Value=70)]
        NpcShopItemType_Device_RemoteDisturber = 70,
        [ProtoEnum(Name="NpcShopItemType_Device_RangeDisturber", Value=0x47)]
        NpcShopItemType_Device_RangeDisturber = 0x47,
        [ProtoEnum(Name="NpcShopItemType_Device_TacticalEquipment", Value=0x48)]
        NpcShopItemType_Device_TacticalEquipment = 0x48,
        [ProtoEnum(Name="NpcShopItemType_Component_DamageEnhancer", Value=0x49)]
        NpcShopItemType_Component_DamageEnhancer = 0x49,
        [ProtoEnum(Name="NpcShopItemType_Component_ShootRangeEnhancer", Value=0x4a)]
        NpcShopItemType_Component_ShootRangeEnhancer = 0x4a,
        [ProtoEnum(Name="NpcShopItemType_Component_ElectronicEnhancer", Value=0x4b)]
        NpcShopItemType_Component_ElectronicEnhancer = 0x4b,
        [ProtoEnum(Name="NpcShopItemType_Component_ShieldEnhancer", Value=0x4c)]
        NpcShopItemType_Component_ShieldEnhancer = 0x4c,
        [ProtoEnum(Name="NpcShopItemType_Component_DrivingEnhancer", Value=0x4d)]
        NpcShopItemType_Component_DrivingEnhancer = 0x4d,
        [ProtoEnum(Name="NpcShopItemType_CharChip_Attack", Value=0x4e)]
        NpcShopItemType_CharChip_Attack = 0x4e,
        [ProtoEnum(Name="NpcShopItemType_CharChip_Defence", Value=0x4f)]
        NpcShopItemType_CharChip_Defence = 0x4f,
        [ProtoEnum(Name="NpcShopItemType_CharChip_Electron", Value=80)]
        NpcShopItemType_CharChip_Electron = 80,
        [ProtoEnum(Name="NpcShopItemType_CharChip_Drive", Value=0x51)]
        NpcShopItemType_CharChip_Drive = 0x51,
        [ProtoEnum(Name="NpcShopItemType_ProductionMaterial", Value=0x52)]
        NpcShopItemType_ProductionMaterial = 0x52,
        [ProtoEnum(Name="NpcShopItemType_FeatsBook_Tec", Value=0x53)]
        NpcShopItemType_FeatsBook_Tec = 0x53,
        [ProtoEnum(Name="NpcShopItemType_FeatsBook_Produce", Value=0x54)]
        NpcShopItemType_FeatsBook_Produce = 0x54,
        [ProtoEnum(Name="NpcShopItemType_FeatsBook_Quest", Value=0x55)]
        NpcShopItemType_FeatsBook_Quest = 0x55,
        [ProtoEnum(Name="NpcShopItemType_FeatsBook_Battle", Value=0x56)]
        NpcShopItemType_FeatsBook_Battle = 0x56,
        [ProtoEnum(Name="NpcShopItemType_Modules", Value=0x57)]
        NpcShopItemType_Modules = 0x57,
        [ProtoEnum(Name="NpcShopItemType_Other_Captain", Value=0x58)]
        NpcShopItemType_Other_Captain = 0x58,
        [ProtoEnum(Name="NpcShopItemType_Other_Manufacture", Value=0x59)]
        NpcShopItemType_Other_Manufacture = 0x59,
        [ProtoEnum(Name="NpcShopItemType_Other", Value=90)]
        NpcShopItemType_Other = 90,
        [ProtoEnum(Name="NpcShopItemType_TradeLegalGood_ECD", Value=0x5b)]
        NpcShopItemType_TradeLegalGood_ECD = 0x5b,
        [ProtoEnum(Name="NpcShopItemType_TradeLegalGood_NEF", Value=0x5c)]
        NpcShopItemType_TradeLegalGood_NEF = 0x5c,
        [ProtoEnum(Name="NpcShopItemType_TradeLegalGood_RS", Value=0x5d)]
        NpcShopItemType_TradeLegalGood_RS = 0x5d,
        [ProtoEnum(Name="NpcShopItemType_TradeLegalGood_OE", Value=0x5e)]
        NpcShopItemType_TradeLegalGood_OE = 0x5e,
        [ProtoEnum(Name="NpcShopItemType_TradeLegalGood_USG", Value=0x5f)]
        NpcShopItemType_TradeLegalGood_USG = 0x5f,
        [ProtoEnum(Name="NpcShopItemType_TradeIllegalGood", Value=0x60)]
        NpcShopItemType_TradeIllegalGood = 0x60,
        [ProtoEnum(Name="NpcShopItemType_NormalItemFunctionItem_Catalyst", Value=0x61)]
        NpcShopItemType_NormalItemFunctionItem_Catalyst = 0x61,
        [ProtoEnum(Name="NpcShopItemType_NormalItemFunctionItem_SpeedUp", Value=0x62)]
        NpcShopItemType_NormalItemFunctionItem_SpeedUp = 0x62,
        [ProtoEnum(Name="NpcShopItemType_NormalItemFunctionItem_HiredCaptain", Value=0x63)]
        NpcShopItemType_NormalItemFunctionItem_HiredCaptain = 0x63,
        [ProtoEnum(Name="NpcShopItemType_NormalItemExchangeItem_Personal", Value=100)]
        NpcShopItemType_NormalItemExchangeItem_Personal = 100,
        [ProtoEnum(Name="NpcShopItemType_EspecialRMBEquip_AlphaTech", Value=0x65)]
        NpcShopItemType_EspecialRMBEquip_AlphaTech = 0x65,
        [ProtoEnum(Name="NpcShopItemType_EspecialRMBShip_TT", Value=0x66)]
        NpcShopItemType_EspecialRMBShip_TT = 0x66,
        [ProtoEnum(Name="NpcShopItemType_Max", Value=0x67)]
        NpcShopItemType_Max = 0x67
    }
}

