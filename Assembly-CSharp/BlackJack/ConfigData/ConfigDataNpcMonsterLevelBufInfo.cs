﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcMonsterLevelBufInfo")]
    public class ConfigDataNpcMonsterLevelBufInfo : IExtensible
    {
        private int _ID;
        private int _StartLevel;
        private int _EndLevel;
        private readonly List<int> _BufList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_StartLevel;
        private static DelegateBridge __Hotfix_set_StartLevel;
        private static DelegateBridge __Hotfix_get_EndLevel;
        private static DelegateBridge __Hotfix_set_EndLevel;
        private static DelegateBridge __Hotfix_get_BufList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StartLevel", DataFormat=DataFormat.TwosComplement)]
        public int StartLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EndLevel", DataFormat=DataFormat.TwosComplement)]
        public int EndLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="BufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

