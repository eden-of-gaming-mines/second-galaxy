﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CustomizedParameter")]
    public enum CustomizedParameter
    {
        [ProtoEnum(Name="CustomizedParameter_Invalid", Value=0)]
        CustomizedParameter_Invalid = 0,
        [ProtoEnum(Name="CustomizedParameter_ExtraDailyWormholePlayCount", Value=1)]
        CustomizedParameter_ExtraDailyWormholePlayCount = 1,
        [ProtoEnum(Name="CustomizedParameter_ExtraDailyShipSalvageCount", Value=2)]
        CustomizedParameter_ExtraDailyShipSalvageCount = 2,
        [ProtoEnum(Name="CustomizedParameter_ExtraAuctionSlotCount", Value=3)]
        CustomizedParameter_ExtraAuctionSlotCount = 3,
        [ProtoEnum(Name="CustomizedParameter_ExtraTechUpgradeSpeedupPercentage", Value=4)]
        CustomizedParameter_ExtraTechUpgradeSpeedupPercentage = 4,
        [ProtoEnum(Name="CustomizedParameter_Max", Value=5)]
        CustomizedParameter_Max = 5
    }
}

