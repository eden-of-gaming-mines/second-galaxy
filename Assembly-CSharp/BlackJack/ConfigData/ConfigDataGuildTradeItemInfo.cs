﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildTradeItemInfo")]
    public class ConfigDataGuildTradeItemInfo : IExtensible
    {
        private int _ID;
        private StoreItemType _ItemType;
        private int _ItemId;
        private int _PurchasePrice;
        private int _MaxPurchasePrice;
        private int _MinPurchasePrice;
        private string _PlaceOfOrigin;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_ItemId;
        private static DelegateBridge __Hotfix_set_ItemId;
        private static DelegateBridge __Hotfix_get_PurchasePrice;
        private static DelegateBridge __Hotfix_set_PurchasePrice;
        private static DelegateBridge __Hotfix_get_MaxPurchasePrice;
        private static DelegateBridge __Hotfix_set_MaxPurchasePrice;
        private static DelegateBridge __Hotfix_get_MinPurchasePrice;
        private static DelegateBridge __Hotfix_set_MinPurchasePrice;
        private static DelegateBridge __Hotfix_get_PlaceOfOrigin;
        private static DelegateBridge __Hotfix_set_PlaceOfOrigin;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ItemId", DataFormat=DataFormat.TwosComplement)]
        public int ItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PurchasePrice", DataFormat=DataFormat.TwosComplement)]
        public int PurchasePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="MaxPurchasePrice", DataFormat=DataFormat.TwosComplement)]
        public int MaxPurchasePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="MinPurchasePrice", DataFormat=DataFormat.TwosComplement)]
        public int MinPurchasePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="PlaceOfOrigin", DataFormat=DataFormat.Default)]
        public string PlaceOfOrigin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

