﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GEPlanetType")]
    public enum GEPlanetType
    {
        [ProtoEnum(Name="GEPlanetType_Invalid", Value=0)]
        GEPlanetType_Invalid = 0,
        [ProtoEnum(Name="GEPlanetType_LavaPlanet", Value=1)]
        GEPlanetType_LavaPlanet = 1,
        [ProtoEnum(Name="GEPlanetType_StormPlanet", Value=2)]
        GEPlanetType_StormPlanet = 2,
        [ProtoEnum(Name="GEPlanetType_EarthPlanet", Value=3)]
        GEPlanetType_EarthPlanet = 3,
        [ProtoEnum(Name="GEPlanetType_DesertPlanet", Value=4)]
        GEPlanetType_DesertPlanet = 4,
        [ProtoEnum(Name="GEPlanetType_OceanPlanet", Value=5)]
        GEPlanetType_OceanPlanet = 5,
        [ProtoEnum(Name="GEPlanetType_GasPlanet", Value=6)]
        GEPlanetType_GasPlanet = 6,
        [ProtoEnum(Name="GEPlanetType_FrozenPlanet", Value=7)]
        GEPlanetType_FrozenPlanet = 7,
        [ProtoEnum(Name="GEPlanetType_NearSunRockPlanet", Value=8)]
        GEPlanetType_NearSunRockPlanet = 8,
        [ProtoEnum(Name="GEPlanetType_FarSunRockPlanet", Value=9)]
        GEPlanetType_FarSunRockPlanet = 9
    }
}

