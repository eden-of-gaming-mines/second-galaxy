﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSimpleProgramCamera")]
    public class ConfigDataSimpleProgramCamera : IExtensible
    {
        private int _ID;
        private float _InitRotateAngelForAxisY;
        private float _InitRotateAngelForAxisX;
        private float _InitOffsetZ;
        private bool _AutoRotation;
        private float _AutoRotationSpeed;
        private float _LifeTime;
        private BlackJack.ConfigData.CameraPlatformInitPosType _CameraPlatformInitPosType;
        private bool _UseUIMask;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_InitRotateAngelForAxisY;
        private static DelegateBridge __Hotfix_set_InitRotateAngelForAxisY;
        private static DelegateBridge __Hotfix_get_InitRotateAngelForAxisX;
        private static DelegateBridge __Hotfix_set_InitRotateAngelForAxisX;
        private static DelegateBridge __Hotfix_get_InitOffsetZ;
        private static DelegateBridge __Hotfix_set_InitOffsetZ;
        private static DelegateBridge __Hotfix_get_AutoRotation;
        private static DelegateBridge __Hotfix_set_AutoRotation;
        private static DelegateBridge __Hotfix_get_AutoRotationSpeed;
        private static DelegateBridge __Hotfix_set_AutoRotationSpeed;
        private static DelegateBridge __Hotfix_get_LifeTime;
        private static DelegateBridge __Hotfix_set_LifeTime;
        private static DelegateBridge __Hotfix_get_CameraPlatformInitPosType;
        private static DelegateBridge __Hotfix_set_CameraPlatformInitPosType;
        private static DelegateBridge __Hotfix_get_UseUIMask;
        private static DelegateBridge __Hotfix_set_UseUIMask;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="InitRotateAngelForAxisY", DataFormat=DataFormat.FixedSize)]
        public float InitRotateAngelForAxisY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="InitRotateAngelForAxisX", DataFormat=DataFormat.FixedSize)]
        public float InitRotateAngelForAxisX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="InitOffsetZ", DataFormat=DataFormat.FixedSize)]
        public float InitOffsetZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="AutoRotation", DataFormat=DataFormat.Default)]
        public bool AutoRotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="AutoRotationSpeed", DataFormat=DataFormat.FixedSize)]
        public float AutoRotationSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="LifeTime", DataFormat=DataFormat.FixedSize)]
        public float LifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="CameraPlatformInitPosType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.CameraPlatformInitPosType CameraPlatformInitPosType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="UseUIMask", DataFormat=DataFormat.Default)]
        public bool UseUIMask
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

