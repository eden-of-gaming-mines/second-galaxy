﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipCompListConfInfo")]
    public class ShipCompListConfInfo : IExtensible
    {
        private int _shipCompId;
        private int _Num;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_shipCompId;
        private static DelegateBridge __Hotfix_set_shipCompId;
        private static DelegateBridge __Hotfix_get_Num;
        private static DelegateBridge __Hotfix_set_Num;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="shipCompId", DataFormat=DataFormat.TwosComplement)]
        public int shipCompId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Num", DataFormat=DataFormat.TwosComplement)]
        public int Num
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

