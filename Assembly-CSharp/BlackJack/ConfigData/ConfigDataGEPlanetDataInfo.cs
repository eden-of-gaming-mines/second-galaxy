﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGEPlanetDataInfo")]
    public class ConfigDataGEPlanetDataInfo : IExtensible
    {
        private int _ID;
        private GEPlanetType _PlanetType;
        private readonly List<GEPlanetDataInfoOrbitRadiusRange> _OrbitRadiusRange;
        private int _StandardRadius;
        private double _StandardMass;
        private int _StandardTemperature;
        private GEAtmosphereDensity _AtmosphereDensity;
        private int _AppearProbability;
        private readonly List<int> _PlanetResPathList;
        private readonly List<int> _HaloResPathList;
        private float _DisplayOnStarMapScale;
        private readonly List<GEPlanetDataInfoDisplayOnStarMapOrbitSpace> _DisplayOnStarMapOrbitSpace;
        private string _PlanetTypeName;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_PlanetType;
        private static DelegateBridge __Hotfix_set_PlanetType;
        private static DelegateBridge __Hotfix_get_OrbitRadiusRange;
        private static DelegateBridge __Hotfix_get_StandardRadius;
        private static DelegateBridge __Hotfix_set_StandardRadius;
        private static DelegateBridge __Hotfix_get_StandardMass;
        private static DelegateBridge __Hotfix_set_StandardMass;
        private static DelegateBridge __Hotfix_get_StandardTemperature;
        private static DelegateBridge __Hotfix_set_StandardTemperature;
        private static DelegateBridge __Hotfix_get_AtmosphereDensity;
        private static DelegateBridge __Hotfix_set_AtmosphereDensity;
        private static DelegateBridge __Hotfix_get_AppearProbability;
        private static DelegateBridge __Hotfix_set_AppearProbability;
        private static DelegateBridge __Hotfix_get_PlanetResPathList;
        private static DelegateBridge __Hotfix_get_HaloResPathList;
        private static DelegateBridge __Hotfix_get_DisplayOnStarMapScale;
        private static DelegateBridge __Hotfix_set_DisplayOnStarMapScale;
        private static DelegateBridge __Hotfix_get_DisplayOnStarMapOrbitSpace;
        private static DelegateBridge __Hotfix_get_PlanetTypeName;
        private static DelegateBridge __Hotfix_set_PlanetTypeName;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PlanetType", DataFormat=DataFormat.TwosComplement)]
        public GEPlanetType PlanetType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="OrbitRadiusRange", DataFormat=DataFormat.Default)]
        public List<GEPlanetDataInfoOrbitRadiusRange> OrbitRadiusRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="StandardRadius", DataFormat=DataFormat.TwosComplement)]
        public int StandardRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="StandardMass", DataFormat=DataFormat.TwosComplement)]
        public double StandardMass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="StandardTemperature", DataFormat=DataFormat.TwosComplement)]
        public int StandardTemperature
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="AtmosphereDensity", DataFormat=DataFormat.TwosComplement)]
        public GEAtmosphereDensity AtmosphereDensity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="AppearProbability", DataFormat=DataFormat.TwosComplement)]
        public int AppearProbability
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="PlanetResPathList", DataFormat=DataFormat.TwosComplement)]
        public List<int> PlanetResPathList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, Name="HaloResPathList", DataFormat=DataFormat.TwosComplement)]
        public List<int> HaloResPathList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DisplayOnStarMapScale", DataFormat=DataFormat.FixedSize)]
        public float DisplayOnStarMapScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, Name="DisplayOnStarMapOrbitSpace", DataFormat=DataFormat.Default)]
        public List<GEPlanetDataInfoDisplayOnStarMapOrbitSpace> DisplayOnStarMapOrbitSpace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="PlanetTypeName", DataFormat=DataFormat.Default)]
        public string PlanetTypeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

