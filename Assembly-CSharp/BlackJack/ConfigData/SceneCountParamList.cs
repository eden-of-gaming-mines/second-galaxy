﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneCountParamList")]
    public class SceneCountParamList : IExtensible
    {
        private float _CurPercent;
        private float _RecoverPercent;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CurPercent;
        private static DelegateBridge __Hotfix_set_CurPercent;
        private static DelegateBridge __Hotfix_get_RecoverPercent;
        private static DelegateBridge __Hotfix_set_RecoverPercent;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CurPercent", DataFormat=DataFormat.FixedSize)]
        public float CurPercent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="RecoverPercent", DataFormat=DataFormat.FixedSize)]
        public float RecoverPercent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

