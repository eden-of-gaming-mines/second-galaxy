﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="PropertyViewType")]
    public enum PropertyViewType
    {
        [ProtoEnum(Name="PropertyViewType_Base", Value=0)]
        PropertyViewType_Base = 0,
        [ProtoEnum(Name="PropertyViewType_Percentage", Value=1)]
        PropertyViewType_Percentage = 1,
        [ProtoEnum(Name="PropertyViewType_Remain2BehindPoint", Value=2)]
        PropertyViewType_Remain2BehindPoint = 2,
        [ProtoEnum(Name="PropertyViewType_CeilToInt", Value=3)]
        PropertyViewType_CeilToInt = 3,
        [ProtoEnum(Name="PropertyViewType_FloorToInt", Value=4)]
        PropertyViewType_FloorToInt = 4,
        [ProtoEnum(Name="PropertyViewType_RoundToInt", Value=5)]
        PropertyViewType_RoundToInt = 5,
        [ProtoEnum(Name="PropertyViewType_SpeedFloat", Value=6)]
        PropertyViewType_SpeedFloat = 6,
        [ProtoEnum(Name="PropertyViewType_SpeedRound", Value=7)]
        PropertyViewType_SpeedRound = 7,
        [ProtoEnum(Name="PropertyViewType_DistanceFloat", Value=8)]
        PropertyViewType_DistanceFloat = 8,
        [ProtoEnum(Name="PropertyViewType_DistanceRound", Value=9)]
        PropertyViewType_DistanceRound = 9,
        [ProtoEnum(Name="PropertyViewType_VolumeFloat", Value=10)]
        PropertyViewType_VolumeFloat = 10,
        [ProtoEnum(Name="PropertyViewType_VolumeRound", Value=11)]
        PropertyViewType_VolumeRound = 11,
        [ProtoEnum(Name="PropertyViewType_TimeFloat", Value=12)]
        PropertyViewType_TimeFloat = 12,
        [ProtoEnum(Name="PropertyViewType_TimeRound", Value=13)]
        PropertyViewType_TimeRound = 13,
        [ProtoEnum(Name="PropertyViewType_TimeFormat", Value=14)]
        PropertyViewType_TimeFormat = 14,
        [ProtoEnum(Name="PropertyViewType_Percentage2BehindPoint", Value=15)]
        PropertyViewType_Percentage2BehindPoint = 15
    }
}

