﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDroneInfo")]
    public class ConfigDataDroneInfo : IExtensible
    {
        private int _ID;
        private DroneType _Type;
        private float _SalePrice;
        private float _BindMoneyBuyPrice;
        private ShipSizeType _SizeType;
        private RankType _Rank;
        private SubRankType _SubRank;
        private uint _FightTime;
        private uint _AttackCD;
        private float _AttackRange;
        private float _DroneSpeed;
        private int _DroneArmor;
        private int _DamageBasic;
        private readonly List<float> _DamageComposeList;
        private float _PackedSize;
        private bool _IsBindOnPick;
        private string _IconResPath;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private float _SuperWeaponEnergyOnceFire;
        private float _ChargeAndChannelBreakFactor;
        private float _TransverseVelocity;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_set_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_get_SizeType;
        private static DelegateBridge __Hotfix_set_SizeType;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_FightTime;
        private static DelegateBridge __Hotfix_set_FightTime;
        private static DelegateBridge __Hotfix_get_AttackCD;
        private static DelegateBridge __Hotfix_set_AttackCD;
        private static DelegateBridge __Hotfix_get_AttackRange;
        private static DelegateBridge __Hotfix_set_AttackRange;
        private static DelegateBridge __Hotfix_get_DroneSpeed;
        private static DelegateBridge __Hotfix_set_DroneSpeed;
        private static DelegateBridge __Hotfix_get_DroneArmor;
        private static DelegateBridge __Hotfix_set_DroneArmor;
        private static DelegateBridge __Hotfix_get_DamageBasic;
        private static DelegateBridge __Hotfix_set_DamageBasic;
        private static DelegateBridge __Hotfix_get_DamageComposeList;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_SuperWeaponEnergyOnceFire;
        private static DelegateBridge __Hotfix_set_SuperWeaponEnergyOnceFire;
        private static DelegateBridge __Hotfix_get_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_set_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_get_TransverseVelocity;
        private static DelegateBridge __Hotfix_set_TransverseVelocity;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public DroneType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="BindMoneyBuyPrice", DataFormat=DataFormat.FixedSize)]
        public float BindMoneyBuyPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SizeType", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType SizeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="FightTime", DataFormat=DataFormat.TwosComplement)]
        public uint FightTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="AttackCD", DataFormat=DataFormat.TwosComplement)]
        public uint AttackCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="AttackRange", DataFormat=DataFormat.FixedSize)]
        public float AttackRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DroneSpeed", DataFormat=DataFormat.FixedSize)]
        public float DroneSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DroneArmor", DataFormat=DataFormat.TwosComplement)]
        public int DroneArmor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="DamageBasic", DataFormat=DataFormat.TwosComplement)]
        public int DamageBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, Name="DamageComposeList", DataFormat=DataFormat.FixedSize)]
        public List<float> DamageComposeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.FixedSize)]
        public float PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="SuperWeaponEnergyOnceFire", DataFormat=DataFormat.FixedSize)]
        public float SuperWeaponEnergyOnceFire
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="ChargeAndChannelBreakFactor", DataFormat=DataFormat.FixedSize)]
        public float ChargeAndChannelBreakFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="TransverseVelocity", DataFormat=DataFormat.FixedSize)]
        public float TransverseVelocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

