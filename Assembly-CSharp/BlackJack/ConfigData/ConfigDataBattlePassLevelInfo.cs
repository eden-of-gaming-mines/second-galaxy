﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBattlePassLevelInfo")]
    public class ConfigDataBattlePassLevelInfo : IExtensible
    {
        private int _ID;
        private readonly List<QuestRewardInfo> _RewardList;
        private readonly List<QuestRewardInfo> _UpgradedRewardList;
        private readonly List<BattlePassRewardEffect> _RewardEffect;
        private int _Level;
        private int _TemplateId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_UpgradedRewardList;
        private static DelegateBridge __Hotfix_get_RewardEffect;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_TemplateId;
        private static DelegateBridge __Hotfix_set_TemplateId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="UpgradedRewardList", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> UpgradedRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="RewardEffect", DataFormat=DataFormat.Default)]
        public List<BattlePassRewardEffect> RewardEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="TemplateId", DataFormat=DataFormat.TwosComplement)]
        public int TemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

