﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSystemFunctionDescription")]
    public class ConfigDataSystemFunctionDescription : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.SystemFuncDescType _SystemFuncDescType;
        private string _CustomLayerAssetPath;
        private readonly List<string> _StaticCopyPathList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SystemFuncDescType;
        private static DelegateBridge __Hotfix_set_SystemFuncDescType;
        private static DelegateBridge __Hotfix_get_CustomLayerAssetPath;
        private static DelegateBridge __Hotfix_set_CustomLayerAssetPath;
        private static DelegateBridge __Hotfix_get_StaticCopyPathList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SystemFuncDescType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SystemFuncDescType SystemFuncDescType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CustomLayerAssetPath", DataFormat=DataFormat.Default)]
        public string CustomLayerAssetPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="StaticCopyPathList", DataFormat=DataFormat.Default)]
        public List<string> StaticCopyPathList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

