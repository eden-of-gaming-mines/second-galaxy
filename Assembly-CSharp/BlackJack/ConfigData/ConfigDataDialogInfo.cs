﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDialogInfo")]
    public class ConfigDataDialogInfo : IExtensible
    {
        private int _ID;
        private int _ReplaceNpcNDIdSolarSystemId;
        private int _ReplaceNpcNDIdNpcId;
        private readonly List<CommonFormatStringParamType> _DescFormatStringParamTypeList;
        private readonly List<FormatStringParamInfo> _DescFormatStringParamInfoList;
        private string _AudioRsePath;
        private NpcEmotionType _Emotion;
        private bool _IsRemoteNpc;
        private int _DuringTime;
        private bool _CanbeInterrupte;
        private string _ContentStrKey;
        private string _NextButtonStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ReplaceNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_set_ReplaceNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_get_ReplaceNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_set_ReplaceNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_get_DescFormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_DescFormatStringParamInfoList;
        private static DelegateBridge __Hotfix_get_AudioRsePath;
        private static DelegateBridge __Hotfix_set_AudioRsePath;
        private static DelegateBridge __Hotfix_get_Emotion;
        private static DelegateBridge __Hotfix_set_Emotion;
        private static DelegateBridge __Hotfix_get_IsRemoteNpc;
        private static DelegateBridge __Hotfix_set_IsRemoteNpc;
        private static DelegateBridge __Hotfix_get_DuringTime;
        private static DelegateBridge __Hotfix_set_DuringTime;
        private static DelegateBridge __Hotfix_get_CanbeInterrupte;
        private static DelegateBridge __Hotfix_set_CanbeInterrupte;
        private static DelegateBridge __Hotfix_get_ContentStrKey;
        private static DelegateBridge __Hotfix_set_ContentStrKey;
        private static DelegateBridge __Hotfix_get_NextButtonStrKey;
        private static DelegateBridge __Hotfix_set_NextButtonStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ReplaceNpcNDIdSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int ReplaceNpcNDIdSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ReplaceNpcNDIdNpcId", DataFormat=DataFormat.TwosComplement)]
        public int ReplaceNpcNDIdNpcId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="DescFormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> DescFormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="DescFormatStringParamInfoList", DataFormat=DataFormat.Default)]
        public List<FormatStringParamInfo> DescFormatStringParamInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="AudioRsePath", DataFormat=DataFormat.Default)]
        public string AudioRsePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Emotion", DataFormat=DataFormat.TwosComplement)]
        public NpcEmotionType Emotion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IsRemoteNpc", DataFormat=DataFormat.Default)]
        public bool IsRemoteNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DuringTime", DataFormat=DataFormat.TwosComplement)]
        public int DuringTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="CanbeInterrupte", DataFormat=DataFormat.Default)]
        public bool CanbeInterrupte
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="ContentStrKey", DataFormat=DataFormat.Default)]
        public string ContentStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="NextButtonStrKey", DataFormat=DataFormat.Default)]
        public string NextButtonStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

