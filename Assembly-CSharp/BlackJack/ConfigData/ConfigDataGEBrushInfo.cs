﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGEBrushInfo")]
    public class ConfigDataGEBrushInfo : IExtensible
    {
        private int _ID;
        private GEBrushType _BrushType;
        private string _Name;
        private string _Desc;
        private string _TextLegendInfo;
        private string _GradualLegendLeftInfo;
        private string _GradualLegendRightInfo;
        private readonly List<GEBrushInfoGradualLegendColorElementList> _GradualLegendColorElementList;
        private readonly List<GEBrushInfoBrushColorElementList> _BrushColorElementList;
        private readonly List<string> _ExtraBrushColor;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _TextLegendStrKey;
        private string _GradualLegendLeftInfoStrKey;
        private string _GradualLegendRightInfoStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BrushType;
        private static DelegateBridge __Hotfix_set_BrushType;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_TextLegendInfo;
        private static DelegateBridge __Hotfix_set_TextLegendInfo;
        private static DelegateBridge __Hotfix_get_GradualLegendLeftInfo;
        private static DelegateBridge __Hotfix_set_GradualLegendLeftInfo;
        private static DelegateBridge __Hotfix_get_GradualLegendRightInfo;
        private static DelegateBridge __Hotfix_set_GradualLegendRightInfo;
        private static DelegateBridge __Hotfix_get_GradualLegendColorElementList;
        private static DelegateBridge __Hotfix_get_BrushColorElementList;
        private static DelegateBridge __Hotfix_get_ExtraBrushColor;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_TextLegendStrKey;
        private static DelegateBridge __Hotfix_set_TextLegendStrKey;
        private static DelegateBridge __Hotfix_get_GradualLegendLeftInfoStrKey;
        private static DelegateBridge __Hotfix_set_GradualLegendLeftInfoStrKey;
        private static DelegateBridge __Hotfix_get_GradualLegendRightInfoStrKey;
        private static DelegateBridge __Hotfix_set_GradualLegendRightInfoStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BrushType", DataFormat=DataFormat.TwosComplement)]
        public GEBrushType BrushType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="TextLegendInfo", DataFormat=DataFormat.Default)]
        public string TextLegendInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="GradualLegendLeftInfo", DataFormat=DataFormat.Default)]
        public string GradualLegendLeftInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GradualLegendRightInfo", DataFormat=DataFormat.Default)]
        public string GradualLegendRightInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="GradualLegendColorElementList", DataFormat=DataFormat.Default)]
        public List<GEBrushInfoGradualLegendColorElementList> GradualLegendColorElementList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="BrushColorElementList", DataFormat=DataFormat.Default)]
        public List<GEBrushInfoBrushColorElementList> BrushColorElementList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="ExtraBrushColor", DataFormat=DataFormat.Default)]
        public List<string> ExtraBrushColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="TextLegendStrKey", DataFormat=DataFormat.Default)]
        public string TextLegendStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="GradualLegendLeftInfoStrKey", DataFormat=DataFormat.Default)]
        public string GradualLegendLeftInfoStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="GradualLegendRightInfoStrKey", DataFormat=DataFormat.Default)]
        public string GradualLegendRightInfoStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

