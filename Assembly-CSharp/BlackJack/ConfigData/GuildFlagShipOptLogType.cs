﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildFlagShipOptLogType")]
    public enum GuildFlagShipOptLogType
    {
        [ProtoEnum(Name="GuildFlagShipOptLogType_UnpackShip", Value=1)]
        GuildFlagShipOptLogType_UnpackShip = 1,
        [ProtoEnum(Name="GuildFlagShipOptLogType_PackShip", Value=2)]
        GuildFlagShipOptLogType_PackShip = 2,
        [ProtoEnum(Name="GuildFlagShipOptLogType_SupplementFuel", Value=3)]
        GuildFlagShipOptLogType_SupplementFuel = 3,
        [ProtoEnum(Name="GuildFlagShipOptLogType_CaptainRegister", Value=4)]
        GuildFlagShipOptLogType_CaptainRegister = 4,
        [ProtoEnum(Name="GuildFlagShipOptLogType_CaptainUnregisterForSelf", Value=5)]
        GuildFlagShipOptLogType_CaptainUnregisterForSelf = 5,
        [ProtoEnum(Name="GuildFlagShipOptLogType_CaptainUnregisterForOther", Value=6)]
        GuildFlagShipOptLogType_CaptainUnregisterForOther = 6,
        [ProtoEnum(Name="GuildFlagShipOptLogType_LeaveHangar", Value=7)]
        GuildFlagShipOptLogType_LeaveHangar = 7,
        [ProtoEnum(Name="GuildFlagShipOptLogType_ReturnHangar", Value=8)]
        GuildFlagShipOptLogType_ReturnHangar = 8,
        [ProtoEnum(Name="GuildFlagShipOptLogType_FlagShipDestroyed", Value=9)]
        GuildFlagShipOptLogType_FlagShipDestroyed = 9,
        [ProtoEnum(Name="GuildFlagShipOptLogType_HangarDestroyed", Value=10)]
        GuildFlagShipOptLogType_HangarDestroyed = 10
    }
}

