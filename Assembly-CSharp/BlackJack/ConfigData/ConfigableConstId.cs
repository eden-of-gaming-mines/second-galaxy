﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ConfigableConstId")]
    public enum ConfigableConstId
    {
        [ProtoEnum(Name="ConfigableConstId_ShieldRecoverPeriod", Value=1)]
        ConfigableConstId_ShieldRecoverPeriod = 1,
        [ProtoEnum(Name="ConfigableConstId_EnergyRecoverPeriod", Value=2)]
        ConfigableConstId_EnergyRecoverPeriod = 2,
        [ProtoEnum(Name="ConfigableConstId_ShieldRecoverPercent", Value=3)]
        ConfigableConstId_ShieldRecoverPercent = 3,
        [ProtoEnum(Name="ConfigableConstId_EnergyRecoverPercent", Value=4)]
        ConfigableConstId_EnergyRecoverPercent = 4,
        [ProtoEnum(Name="ConfigableConstId_ShieldFailurePeriod", Value=5)]
        ConfigableConstId_ShieldFailurePeriod = 5,
        [ProtoEnum(Name="ConfigableConstId_FightStateCheckDistance", Value=6)]
        ConfigableConstId_FightStateCheckDistance = 6,
        [ProtoEnum(Name="ConfigableConstId_BulletLifeTime", Value=7)]
        ConfigableConstId_BulletLifeTime = 7,
        [ProtoEnum(Name="ConfigableConstId_DroneBulletFlyTime", Value=8)]
        ConfigableConstId_DroneBulletFlyTime = 8,
        [ProtoEnum(Name="ConfigableConstId_DistanceToBuildingAfterJumping", Value=9)]
        ConfigableConstId_DistanceToBuildingAfterJumping = 9,
        [ProtoEnum(Name="ConfigableConstId_CelestialJumpingStopDistanceMult", Value=12)]
        ConfigableConstId_CelestialJumpingStopDistanceMult = 12,
        [ProtoEnum(Name="ConfigableConstId_BurstDamagePercentForBadCombo", Value=13)]
        ConfigableConstId_BurstDamagePercentForBadCombo = 13,
        [ProtoEnum(Name="ConfigableConstId_DefenceDroneFormupTime", Value=14)]
        ConfigableConstId_DefenceDroneFormupTime = 14,
        [ProtoEnum(Name="ConfigableConstId_AttackDroneFormupTime", Value=15)]
        ConfigableConstId_AttackDroneFormupTime = 15,
        [ProtoEnum(Name="ConfigableConstId_WeaponFireCtrlAccuracyBaseValue", Value=0x10)]
        ConfigableConstId_WeaponFireCtrlAccuracyBaseValue = 0x10,
        [ProtoEnum(Name="ConfigableConstId_CD4NextDroneAttack", Value=0x11)]
        ConfigableConstId_CD4NextDroneAttack = 0x11,
        [ProtoEnum(Name="ConfigableConstId_BurstCD4NextDroneAttack", Value=0x12)]
        ConfigableConstId_BurstCD4NextDroneAttack = 0x12,
        [ProtoEnum(Name="ConfigableConstId_ShipDamageCapabilityMulit", Value=0x13)]
        ConfigableConstId_ShipDamageCapabilityMulit = 0x13,
        [ProtoEnum(Name="ConfigableConstId_BurstGoodTimeStartRatioPercent", Value=20)]
        ConfigableConstId_BurstGoodTimeStartRatioPercent = 20,
        [ProtoEnum(Name="ConfigableConstId_BurstGoodTimeEndRatioPercent", Value=0x15)]
        ConfigableConstId_BurstGoodTimeEndRatioPercent = 0x15,
        [ProtoEnum(Name="ConfigableConstId_CanJumpDistanceMin", Value=0x16)]
        ConfigableConstId_CanJumpDistanceMin = 0x16,
        [ProtoEnum(Name="ConfigableConstId_ShipKinematicsInertiaModifyMulti", Value=0x17)]
        ConfigableConstId_ShipKinematicsInertiaModifyMulti = 0x17,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForFrigate", Value=0x18)]
        ConfigableConstId_DefaultCameraOffsetMultiForFrigate = 0x18,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForDestroyer", Value=0x19)]
        ConfigableConstId_DefaultCameraOffsetMultiForDestroyer = 0x19,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForCruiser", Value=0x1a)]
        ConfigableConstId_DefaultCameraOffsetMultiForCruiser = 0x1a,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForBattleCruiser", Value=0x1b)]
        ConfigableConstId_DefaultCameraOffsetMultiForBattleCruiser = 0x1b,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForBattleShip", Value=0x1c)]
        ConfigableConstId_DefaultCameraOffsetMultiForBattleShip = 0x1c,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForBuilding", Value=0x1d)]
        ConfigableConstId_DefaultCameraOffsetMultiForBuilding = 0x1d,
        [ProtoEnum(Name="ConfigableConstId_ShipSpeedMaxModifyMulti", Value=30)]
        ConfigableConstId_ShipSpeedMaxModifyMulti = 30,
        [ProtoEnum(Name="ConfigableConstId_DamageThreshholdForCameraShake", Value=0x1f)]
        ConfigableConstId_DamageThreshholdForCameraShake = 0x1f,
        [ProtoEnum(Name="ConfigableConstId_ArmorPercentForLowArmorWarning", Value=0x20)]
        ConfigableConstId_ArmorPercentForLowArmorWarning = 0x20,
        [ProtoEnum(Name="ConfigableConstId_PlayerInitSolarSystemId", Value=0x21)]
        ConfigableConstId_PlayerInitSolarSystemId = 0x21,
        [ProtoEnum(Name="ConfigableConstId_PlayerInitStaticSceneId", Value=0x22)]
        ConfigableConstId_PlayerInitStaticSceneId = 0x22,
        [ProtoEnum(Name="ConfigableConstId_PlayerInitQuestId", Value=0x23)]
        ConfigableConstId_PlayerInitQuestId = 0x23,
        [ProtoEnum(Name="ConfigableConstId_PlayerResetFirstStoryQuestId", Value=0x24)]
        ConfigableConstId_PlayerResetFirstStoryQuestId = 0x24,
        [ProtoEnum(Name="ConfigableConstId_UserGuildGroupId4ResetCharacterStaticInfo", Value=0x25)]
        ConfigableConstId_UserGuildGroupId4ResetCharacterStaticInfo = 0x25,
        [ProtoEnum(Name="ConfigableConstId_PlayerInitShipConfigId", Value=0x26)]
        ConfigableConstId_PlayerInitShipConfigId = 0x26,
        [ProtoEnum(Name="ConfigableConstId_ShipDPSMax", Value=0x27)]
        ConfigableConstId_ShipDPSMax = 0x27,
        [ProtoEnum(Name="ConfigableConstId_ShipFireRangeMax", Value=40)]
        ConfigableConstId_ShipFireRangeMax = 40,
        [ProtoEnum(Name="ConfigableConstId_ShipDefenseMax", Value=0x29)]
        ConfigableConstId_ShipDefenseMax = 0x29,
        [ProtoEnum(Name="ConfigableConstId_ShipSpeedMax", Value=0x2a)]
        ConfigableConstId_ShipSpeedMax = 0x2a,
        [ProtoEnum(Name="ConfigableConstId_ShipStorageMax", Value=0x2b)]
        ConfigableConstId_ShipStorageMax = 0x2b,
        [ProtoEnum(Name="ConfigableConstId_ShipCollectionAbilityMax", Value=0x2c)]
        ConfigableConstId_ShipCollectionAbilityMax = 0x2c,
        [ProtoEnum(Name="ConfigableConstId_NpcCaptainFeatsLevelUpRate", Value=0x2d)]
        ConfigableConstId_NpcCaptainFeatsLevelUpRate = 0x2d,
        [ProtoEnum(Name="ConfigableConstId_DelegateFightTimeMulti", Value=0x2e)]
        ConfigableConstId_DelegateFightTimeMulti = 0x2e,
        [ProtoEnum(Name="ConfigableConstId_DelegateFightShipRepairMulti", Value=0x2f)]
        ConfigableConstId_DelegateFightShipRepairMulti = 0x2f,
        [ProtoEnum(Name="ConfigableConstId_DelegateMineralCircleTime", Value=0x30)]
        ConfigableConstId_DelegateMineralCircleTime = 0x30,
        [ProtoEnum(Name="ConfigableConstId_StarFieldChatPropId", Value=0x31)]
        ConfigableConstId_StarFieldChatPropId = 0x31,
        [ProtoEnum(Name="ConfigableConstId_NPCTalkerChatDurationTime", Value=50)]
        ConfigableConstId_NPCTalkerChatDurationTime = 50,
        [ProtoEnum(Name="ConfigableConstId_SceneNoticeDurationTime", Value=0x33)]
        ConfigableConstId_SceneNoticeDurationTime = 0x33,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainRetireReserveFeatRate", Value=0x34)]
        ConfigableConstId_HiredCaptainRetireReserveFeatRate = 0x34,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainRetireReserveFeatItemId", Value=0x35)]
        ConfigableConstId_HiredCaptainRetireReserveFeatItemId = 0x35,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainFeatsReplaceRateCount1", Value=0x36)]
        ConfigableConstId_HiredCaptainFeatsReplaceRateCount1 = 0x36,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainFeatsReplaceRateCount2", Value=0x37)]
        ConfigableConstId_HiredCaptainFeatsReplaceRateCount2 = 0x37,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainFeatsReplaceRateCount3", Value=0x38)]
        ConfigableConstId_HiredCaptainFeatsReplaceRateCount3 = 0x38,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeCancelCostReturnPercent", Value=0x39)]
        ConfigableConstId_TechUpgradeCancelCostReturnPercent = 0x39,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeTimeExchangeRate2RealMoney", Value=0x3a)]
        ConfigableConstId_TechUpgradeTimeExchangeRate2RealMoney = 0x3a,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeCaptainMainPropertiesCostMulti", Value=0x3b)]
        ConfigableConstId_TechUpgradeCaptainMainPropertiesCostMulti = 0x3b,
        [ProtoEnum(Name="ConfigableConstId_MotherShipSingleSolarSystemSpainTime", Value=60)]
        ConfigableConstId_MotherShipSingleSolarSystemSpainTime = 60,
        [ProtoEnum(Name="ConfigableConstId_MotherShipPerRealMonyCostTime", Value=0x3d)]
        ConfigableConstId_MotherShipPerRealMonyCostTime = 0x3d,
        [ProtoEnum(Name="ConfigableConstId_ProductionLineCancelCostReturnPercent", Value=0x3e)]
        ConfigableConstId_ProductionLineCancelCostReturnPercent = 0x3e,
        [ProtoEnum(Name="ConfigableConstId_ProductionLineTimeExchangeRate2RealMoney", Value=0x3f)]
        ConfigableConstId_ProductionLineTimeExchangeRate2RealMoney = 0x3f,
        [ProtoEnum(Name="ConfigableConstId_ProductionLineMaxProduceTime", Value=0x40)]
        ConfigableConstId_ProductionLineMaxProduceTime = 0x40,
        [ProtoEnum(Name="ConfigableConstId_ProductionLineInitCount", Value=0x41)]
        ConfigableConstId_ProductionLineInitCount = 0x41,
        [ProtoEnum(Name="ConfigableConstId_ProductionLineMaxCount", Value=0x42)]
        ConfigableConstId_ProductionLineMaxCount = 0x42,
        [ProtoEnum(Name="ConfigableConstId_ProductionCaptainMainPropertiesCostMulti", Value=0x43)]
        ConfigableConstId_ProductionCaptainMainPropertiesCostMulti = 0x43,
        [ProtoEnum(Name="ConfigableConstId_CaptainExpAddItemLowId", Value=0x44)]
        ConfigableConstId_CaptainExpAddItemLowId = 0x44,
        [ProtoEnum(Name="ConfigableConstId_CaptainExpAddItemMediumId", Value=0x45)]
        ConfigableConstId_CaptainExpAddItemMediumId = 0x45,
        [ProtoEnum(Name="ConfigableConstId_CaptainExpAddItemHighId", Value=70)]
        ConfigableConstId_CaptainExpAddItemHighId = 70,
        [ProtoEnum(Name="ConfigableConstId_KillRecordAvailableKillerTime", Value=0x47)]
        ConfigableConstId_KillRecordAvailableKillerTime = 0x47,
        [ProtoEnum(Name="ConfigableConstId_BaseRedeploySpeedUpItem1", Value=0x48)]
        ConfigableConstId_BaseRedeploySpeedUpItem1 = 0x48,
        [ProtoEnum(Name="ConfigableConstId_BaseRedeploySpeedUpItem2", Value=0x49)]
        ConfigableConstId_BaseRedeploySpeedUpItem2 = 0x49,
        [ProtoEnum(Name="ConfigableConstId_BaseRedeploySpeedUpItem3", Value=0x4a)]
        ConfigableConstId_BaseRedeploySpeedUpItem3 = 0x4a,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalScanCountMax", Value=0x4b)]
        ConfigableConstId_DelegateSignalScanCountMax = 0x4b,
        [ProtoEnum(Name="ConfigableConstId_ManualQuestSignalScanCountMax", Value=0x4c)]
        ConfigableConstId_ManualQuestSignalScanCountMax = 0x4c,
        [ProtoEnum(Name="ConfigableConstId_PVPSignalScanCountMax", Value=0x4d)]
        ConfigableConstId_PVPSignalScanCountMax = 0x4d,
        [ProtoEnum(Name="ConfigableConstId_DelegateScanProbeCountMax", Value=0x4e)]
        ConfigableConstId_DelegateScanProbeCountMax = 0x4e,
        [ProtoEnum(Name="ConfigableConstId_ManualScanProbeCountMax", Value=0x4f)]
        ConfigableConstId_ManualScanProbeCountMax = 0x4f,
        [ProtoEnum(Name="ConfigableConstId_PVPScanProbeCountMax", Value=80)]
        ConfigableConstId_PVPScanProbeCountMax = 80,
        [ProtoEnum(Name="ConfigableConstId_DelegateScanProbeRechargeCD", Value=0x51)]
        ConfigableConstId_DelegateScanProbeRechargeCD = 0x51,
        [ProtoEnum(Name="ConfigableConstId_ManualScanProbeRechargeCD", Value=0x52)]
        ConfigableConstId_ManualScanProbeRechargeCD = 0x52,
        [ProtoEnum(Name="ConfigableConstId_PVPScanProbeRechargeCD", Value=0x53)]
        ConfigableConstId_PVPScanProbeRechargeCD = 0x53,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionCompleteTimeMin", Value=0x54)]
        ConfigableConstId_DelegateMissionCompleteTimeMin = 0x54,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionCompleteTimeMax", Value=0x55)]
        ConfigableConstId_DelegateMissionCompleteTimeMax = 0x55,
        [ProtoEnum(Name="ConfigableConstId_DelegateTransportOneJumpTime", Value=0x56)]
        ConfigableConstId_DelegateTransportOneJumpTime = 0x56,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionCountInit", Value=0x57)]
        ConfigableConstId_DelegateMissionCountInit = 0x57,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionCountMax", Value=0x58)]
        ConfigableConstId_DelegateMissionCountMax = 0x58,
        [ProtoEnum(Name="ConfigableConstId_FleetShipCountInit", Value=0x59)]
        ConfigableConstId_FleetShipCountInit = 0x59,
        [ProtoEnum(Name="ConfigableConstId_FleetShipCountMax", Value=90)]
        ConfigableConstId_FleetShipCountMax = 90,
        [ProtoEnum(Name="ConfigableConstId_PVPSignalScanJumpStep", Value=0x5b)]
        ConfigableConstId_PVPSignalScanJumpStep = 0x5b,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeSpeedUpItemId1", Value=0x5c)]
        ConfigableConstId_TechUpgradeSpeedUpItemId1 = 0x5c,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeSpeedUpItemId2", Value=0x5d)]
        ConfigableConstId_TechUpgradeSpeedUpItemId2 = 0x5d,
        [ProtoEnum(Name="ConfigableConstId_TechUpgradeSpeedUpItemId3", Value=0x5e)]
        ConfigableConstId_TechUpgradeSpeedUpItemId3 = 0x5e,
        [ProtoEnum(Name="ConfigableConstId_CrackTimeExchangeRate2RealMoney", Value=0x5f)]
        ConfigableConstId_CrackTimeExchangeRate2RealMoney = 0x5f,
        [ProtoEnum(Name="ConfigableConstId_CrackExternSlotOneRealMoneyCost", Value=0x60)]
        ConfigableConstId_CrackExternSlotOneRealMoneyCost = 0x60,
        [ProtoEnum(Name="ConfigableConstId_CrackExternSlotTwoRealMoneyCost", Value=0x61)]
        ConfigableConstId_CrackExternSlotTwoRealMoneyCost = 0x61,
        [ProtoEnum(Name="ConfigableConstId_ActivityPointInit", Value=0x62)]
        ConfigableConstId_ActivityPointInit = 0x62,
        [ProtoEnum(Name="ConfigableConstId_JustCrimeCountDown", Value=0x63)]
        ConfigableConstId_JustCrimeCountDown = 0x63,
        [ProtoEnum(Name="ConfigableConstId_CriminalHunterCountDown", Value=100)]
        ConfigableConstId_CriminalHunterCountDown = 100,
        [ProtoEnum(Name="ConfigableConstId_PlayerHostileBehaviorCountDown", Value=0x65)]
        ConfigableConstId_PlayerHostileBehaviorCountDown = 0x65,
        [ProtoEnum(Name="ConfigableConstId_PVPTargetPlayerLevelMin", Value=0x66)]
        ConfigableConstId_PVPTargetPlayerLevelMin = 0x66,
        [ProtoEnum(Name="ConfigableConstId_UseStarGateBetweenStarFieldPlayerLevelLimit", Value=0x67)]
        ConfigableConstId_UseStarGateBetweenStarFieldPlayerLevelLimit = 0x67,
        [ProtoEnum(Name="ConfigableConstId_DailyFreeQuestBurstRewardCompleteCount", Value=0x68)]
        ConfigableConstId_DailyFreeQuestBurstRewardCompleteCount = 0x68,
        [ProtoEnum(Name="ConfigableConstId_DailyFreeQuestBurstRewardForItem", Value=0x69)]
        ConfigableConstId_DailyFreeQuestBurstRewardForItem = 0x69,
        [ProtoEnum(Name="ConfigableConstId_DailyFreeQuestBurstRewardForExp", Value=0x6a)]
        ConfigableConstId_DailyFreeQuestBurstRewardForExp = 0x6a,
        [ProtoEnum(Name="ConfigableConstId_DailyFreeQuestBurstRewardForCurrency", Value=0x6b)]
        ConfigableConstId_DailyFreeQuestBurstRewardForCurrency = 0x6b,
        [ProtoEnum(Name="ConfigableConstId_InfectCycleTime", Value=0x6c)]
        ConfigableConstId_InfectCycleTime = 0x6c,
        [ProtoEnum(Name="ConfigableConstId_InfectSelfHealingTime", Value=0x6d)]
        ConfigableConstId_InfectSelfHealingTime = 0x6d,
        [ProtoEnum(Name="ConfigableConstId_InfectSelfHealingAlertTime", Value=110)]
        ConfigableConstId_InfectSelfHealingAlertTime = 110,
        [ProtoEnum(Name="ConfigableConstId_InfectSolarSystemSecurityLevelMax", Value=0x6f)]
        ConfigableConstId_InfectSolarSystemSecurityLevelMax = 0x6f,
        [ProtoEnum(Name="ConfigableConstId_InfectFinalBattleOpenMultiCastJumpDistance", Value=0x70)]
        ConfigableConstId_InfectFinalBattleOpenMultiCastJumpDistance = 0x70,
        [ProtoEnum(Name="ConfigableConstId_InfectStarGroupMin", Value=0x71)]
        ConfigableConstId_InfectStarGroupMin = 0x71,
        [ProtoEnum(Name="ConfigableConstId_InfectStarGroupMax", Value=0x72)]
        ConfigableConstId_InfectStarGroupMax = 0x72,
        [ProtoEnum(Name="ConfigableConstId_InfectInitProgressMin", Value=0x73)]
        ConfigableConstId_InfectInitProgressMin = 0x73,
        [ProtoEnum(Name="ConfigableConstId_InfectInitProgressMax", Value=0x74)]
        ConfigableConstId_InfectInitProgressMax = 0x74,
        [ProtoEnum(Name="ConfigableConstId_BlueDevoderConfigId", Value=0x75)]
        ConfigableConstId_BlueDevoderConfigId = 0x75,
        [ProtoEnum(Name="ConfigableConstId_PropertiesAutoAddPlayerLevel", Value=0x76)]
        ConfigableConstId_PropertiesAutoAddPlayerLevel = 0x76,
        [ProtoEnum(Name="ConfigableConstId_PropertiesClearItemConfigId", Value=0x77)]
        ConfigableConstId_PropertiesClearItemConfigId = 0x77,
        [ProtoEnum(Name="ConfigableConstId_CharactorAttackPropertyGainMultiValue", Value=120)]
        ConfigableConstId_CharactorAttackPropertyGainMultiValue = 120,
        [ProtoEnum(Name="ConfigableConstId_CharactorDefencePropertyGainMultiValue", Value=0x79)]
        ConfigableConstId_CharactorDefencePropertyGainMultiValue = 0x79,
        [ProtoEnum(Name="ConfigableConstId_CharactorElectronPropertyGainMultiValue", Value=0x7a)]
        ConfigableConstId_CharactorElectronPropertyGainMultiValue = 0x7a,
        [ProtoEnum(Name="ConfigableConstId_CharactorDrivePropertyGainMultiValue", Value=0x7b)]
        ConfigableConstId_CharactorDrivePropertyGainMultiValue = 0x7b,
        [ProtoEnum(Name="ConfigableConstId_UserGuideIdForMotherShipRedploy", Value=0x7c)]
        ConfigableConstId_UserGuideIdForMotherShipRedploy = 0x7c,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionShipCountInit", Value=0x7d)]
        ConfigableConstId_DelegateMissionShipCountInit = 0x7d,
        [ProtoEnum(Name="ConfigableConstId_WormholeSceneLifeTimeMin", Value=0x7e)]
        ConfigableConstId_WormholeSceneLifeTimeMin = 0x7e,
        [ProtoEnum(Name="ConfigableConstId_WormholeSceneLifeTimeMax", Value=0x7f)]
        ConfigableConstId_WormholeSceneLifeTimeMax = 0x7f,
        [ProtoEnum(Name="ConfigableConstId_DifficultGuideEmergencySignalReqRate", Value=0x80)]
        ConfigableConstId_DifficultGuideEmergencySignalReqRate = 0x80,
        [ProtoEnum(Name="ConfigableConstId_DifficultGuideEmergencySignalBonusMulti", Value=0x81)]
        ConfigableConstId_DifficultGuideEmergencySignalBonusMulti = 0x81,
        [ProtoEnum(Name="ConfigableConstId_ShipNameMaxLength", Value=130)]
        ConfigableConstId_ShipNameMaxLength = 130,
        [ProtoEnum(Name="ConfigableConstId_PlayerNameMaxLength", Value=0x83)]
        ConfigableConstId_PlayerNameMaxLength = 0x83,
        [ProtoEnum(Name="ConfigableConstId_ShipTemplateNameMaxLength", Value=0x84)]
        ConfigableConstId_ShipTemplateNameMaxLength = 0x84,
        [ProtoEnum(Name="ConfigableConstId_KillRecordLostBindMoneyCalcMulti", Value=0x85)]
        ConfigableConstId_KillRecordLostBindMoneyCalcMulti = 0x85,
        [ProtoEnum(Name="ConfigableConstId_ItemStoreSpaceMax", Value=0x86)]
        ConfigableConstId_ItemStoreSpaceMax = 0x86,
        [ProtoEnum(Name="ConfigableConstId_WormholeDebuffEmegencyWarningBufPower", Value=0x87)]
        ConfigableConstId_WormholeDebuffEmegencyWarningBufPower = 0x87,
        [ProtoEnum(Name="ConfigableConstId_UseTechSpeedUpItemNoTipTimeLimit", Value=0x88)]
        ConfigableConstId_UseTechSpeedUpItemNoTipTimeLimit = 0x88,
        [ProtoEnum(Name="ConfigableConstId_ShipSensitivityCalcValue", Value=0x89)]
        ConfigableConstId_ShipSensitivityCalcValue = 0x89,
        [ProtoEnum(Name="ConfigableConstId_ProduceLineNameLengthInMotherShipPanel", Value=0x8a)]
        ConfigableConstId_ProduceLineNameLengthInMotherShipPanel = 0x8a,
        [ProtoEnum(Name="ConfigableConstId_TechNameLengthInMotherShipPanel", Value=0x8b)]
        ConfigableConstId_TechNameLengthInMotherShipPanel = 0x8b,
        [ProtoEnum(Name="ConfigableConstId_QuestPoolCountReduceInSolarSystemDifficultLevelMin", Value=140)]
        ConfigableConstId_QuestPoolCountReduceInSolarSystemDifficultLevelMin = 140,
        [ProtoEnum(Name="ConfigableConstId_DelegateMissionPoolCountReduceInSolarSystemDifficultLevelMin", Value=0x8d)]
        ConfigableConstId_DelegateMissionPoolCountReduceInSolarSystemDifficultLevelMin = 0x8d,
        [ProtoEnum(Name="ConfigableConstId_PoolCountUpdateValue", Value=0x8e)]
        ConfigableConstId_PoolCountUpdateValue = 0x8e,
        [ProtoEnum(Name="ConfigableConstId_MarketPriceUpdateTradeOrderCount", Value=0x8f)]
        ConfigableConstId_MarketPriceUpdateTradeOrderCount = 0x8f,
        [ProtoEnum(Name="ConfigableConstId_MarketPriceDailyUpdateMin", Value=0x90)]
        ConfigableConstId_MarketPriceDailyUpdateMin = 0x90,
        [ProtoEnum(Name="ConfigableConstId_MarketPriceDailyUpdateMax", Value=0x91)]
        ConfigableConstId_MarketPriceDailyUpdateMax = 0x91,
        [ProtoEnum(Name="ConfigableConstId_AuctionPriceLimitMin", Value=0x92)]
        ConfigableConstId_AuctionPriceLimitMin = 0x92,
        [ProtoEnum(Name="ConfigableConstId_AuctionPriceLimitMax", Value=0x93)]
        ConfigableConstId_AuctionPriceLimitMax = 0x93,
        [ProtoEnum(Name="ConfigableConstId_AuctionTradeTax", Value=0x94)]
        ConfigableConstId_AuctionTradeTax = 0x94,
        [ProtoEnum(Name="ConfigableConstId_AuctionItemOrderBriefListMaxCount", Value=0x95)]
        ConfigableConstId_AuctionItemOrderBriefListMaxCount = 0x95,
        [ProtoEnum(Name="ConfigableConstId_PlayerAuctionItemOrderCountMax", Value=150)]
        ConfigableConstId_PlayerAuctionItemOrderCountMax = 150,
        [ProtoEnum(Name="ConfigableConstId_AuctionOnShelvePeriodShort", Value=0x97)]
        ConfigableConstId_AuctionOnShelvePeriodShort = 0x97,
        [ProtoEnum(Name="ConfigableConstId_AuctionOnShelvePeriodLong", Value=0x98)]
        ConfigableConstId_AuctionOnShelvePeriodLong = 0x98,
        [ProtoEnum(Name="ConfigableConstId_AuctionItemFreezingTime", Value=0x99)]
        ConfigableConstId_AuctionItemFreezingTime = 0x99,
        [ProtoEnum(Name="ConfigableConstId_CameraShakeScaleOnShieldBroken", Value=0x9a)]
        ConfigableConstId_CameraShakeScaleOnShieldBroken = 0x9a,
        [ProtoEnum(Name="ConfigableConstId_CameraShakeScaleOnDeath", Value=0x9b)]
        ConfigableConstId_CameraShakeScaleOnDeath = 0x9b,
        [ProtoEnum(Name="ConfigableConstId_CameraShakeScaleOnSpeedUp", Value=0x9c)]
        ConfigableConstId_CameraShakeScaleOnSpeedUp = 0x9c,
        [ProtoEnum(Name="ConfigableConstId_CameraShakeScaleOnSuperGearTrigger", Value=0x9d)]
        ConfigableConstId_CameraShakeScaleOnSuperGearTrigger = 0x9d,
        [ProtoEnum(Name="ConfigableConstId_PuzzleGameFristAssistCost", Value=0x9e)]
        ConfigableConstId_PuzzleGameFristAssistCost = 0x9e,
        [ProtoEnum(Name="ConfigableConstId_PuzzleGameSecondAssistCost", Value=0x9f)]
        ConfigableConstId_PuzzleGameSecondAssistCost = 0x9f,
        [ProtoEnum(Name="ConfigableConstId_PuzzleGamLastAssistCost", Value=160)]
        ConfigableConstId_PuzzleGamLastAssistCost = 160,
        [ProtoEnum(Name="ConfigableConstId_GuildSerachListMax", Value=0xa1)]
        ConfigableConstId_GuildSerachListMax = 0xa1,
        [ProtoEnum(Name="ConfigableConstId_GuildMemberCountMax", Value=0xa2)]
        ConfigableConstId_GuildMemberCountMax = 0xa2,
        [ProtoEnum(Name="ConfigableConstId_GuildJoinReqCountMax", Value=0xa3)]
        ConfigableConstId_GuildJoinReqCountMax = 0xa3,
        [ProtoEnum(Name="ConfigableConstId_GuildRecommendMemberMin", Value=0xa4)]
        ConfigableConstId_GuildRecommendMemberMin = 0xa4,
        [ProtoEnum(Name="ConfigableConstId_GuildRecommendMemberMax", Value=0xa5)]
        ConfigableConstId_GuildRecommendMemberMax = 0xa5,
        [ProtoEnum(Name="ConfigableConstId_GuildRandomRegionSearchGuildCount", Value=0xa6)]
        ConfigableConstId_GuildRandomRegionSearchGuildCount = 0xa6,
        [ProtoEnum(Name="ConfigableConstId_CreateGuildPlayerLevel", Value=0xa7)]
        ConfigableConstId_CreateGuildPlayerLevel = 0xa7,
        [ProtoEnum(Name="ConfigableConstId_CreateGuildTradeMoneyCost", Value=0xa8)]
        ConfigableConstId_CreateGuildTradeMoneyCost = 0xa8,
        [ProtoEnum(Name="ConfigableConstId_ChangeGuildNameAndCodeRealMoneyCost", Value=0xa9)]
        ConfigableConstId_ChangeGuildNameAndCodeRealMoneyCost = 0xa9,
        [ProtoEnum(Name="ConfigableConstId_GuildDismissWaitingTime", Value=170)]
        ConfigableConstId_GuildDismissWaitingTime = 170,
        [ProtoEnum(Name="ConfigableConstId_GuildJoinPlayerLevel", Value=0xab)]
        ConfigableConstId_GuildJoinPlayerLevel = 0xab,
        [ProtoEnum(Name="ConfigableConstId_GuildJoinApplyTimeout", Value=0xac)]
        ConfigableConstId_GuildJoinApplyTimeout = 0xac,
        [ProtoEnum(Name="ConfigableConstId_GuildLeaveRejoinCD", Value=0xad)]
        ConfigableConstId_GuildLeaveRejoinCD = 0xad,
        [ProtoEnum(Name="ConfigableConstId_AuctionMaxBuyCount", Value=0xae)]
        ConfigableConstId_AuctionMaxBuyCount = 0xae,
        [ProtoEnum(Name="ConfigableConstId_SolarSystemUIAutoHideTime", Value=0xaf)]
        ConfigableConstId_SolarSystemUIAutoHideTime = 0xaf,
        [ProtoEnum(Name="ConfigableConstId_SolarSystemUICameraOffsetWhenAutoHide", Value=0xb0)]
        ConfigableConstId_SolarSystemUICameraOffsetWhenAutoHide = 0xb0,
        [ProtoEnum(Name="ConfigableConstId_ManualSignalLevelMultiMin", Value=0xb1)]
        ConfigableConstId_ManualSignalLevelMultiMin = 0xb1,
        [ProtoEnum(Name="ConfigableConstId_ManualSignalSpanFactor", Value=0xb2)]
        ConfigableConstId_ManualSignalSpanFactor = 0xb2,
        [ProtoEnum(Name="ConfigableConstId_ManualSignalSpanMax", Value=0xb3)]
        ConfigableConstId_ManualSignalSpanMax = 0xb3,
        [ProtoEnum(Name="ConfigableConstId_ManualSignalShiftFactor", Value=180)]
        ConfigableConstId_ManualSignalShiftFactor = 180,
        [ProtoEnum(Name="ConfigableConstId_ManualSignalShiftMax", Value=0xb5)]
        ConfigableConstId_ManualSignalShiftMax = 0xb5,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalLevelMultiMin", Value=0xb6)]
        ConfigableConstId_DelegateSignalLevelMultiMin = 0xb6,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalSpanFactor", Value=0xb7)]
        ConfigableConstId_DelegateSignalSpanFactor = 0xb7,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalSpanMax", Value=0xb8)]
        ConfigableConstId_DelegateSignalSpanMax = 0xb8,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalShiftFactor", Value=0xb9)]
        ConfigableConstId_DelegateSignalShiftFactor = 0xb9,
        [ProtoEnum(Name="ConfigableConstId_DelegateSignalShiftMax", Value=0xba)]
        ConfigableConstId_DelegateSignalShiftMax = 0xba,
        [ProtoEnum(Name="ConfigableConstId_Navagation2PlayerRoundRaduis", Value=0xbb)]
        ConfigableConstId_Navagation2PlayerRoundRaduis = 0xbb,
        [ProtoEnum(Name="ConfigableConstId_InvestigateUnlockPlayerLevel", Value=0xbc)]
        ConfigableConstId_InvestigateUnlockPlayerLevel = 0xbc,
        [ProtoEnum(Name="ConfigableConstId_WeaponEquipFireRangeCheckRatio", Value=0xbd)]
        ConfigableConstId_WeaponEquipFireRangeCheckRatio = 0xbd,
        [ProtoEnum(Name="ConfigableConstId_PlayerHostileBehaviorCountDownForLeaveSolarSystem", Value=190)]
        ConfigableConstId_PlayerHostileBehaviorCountDownForLeaveSolarSystem = 190,
        [ProtoEnum(Name="ConfigableConstId_ChangeGuildLogoRealMoneyCost", Value=0xbf)]
        ConfigableConstId_ChangeGuildLogoRealMoneyCost = 0xbf,
        [ProtoEnum(Name="ConfigableConstId_GuildLeaderTransferWaitingTime", Value=0xc0)]
        ConfigableConstId_GuildLeaderTransferWaitingTime = 0xc0,
        [ProtoEnum(Name="ConfigableConstId_GuildLeaderAutoTransferDay", Value=0xc1)]
        ConfigableConstId_GuildLeaderAutoTransferDay = 0xc1,
        [ProtoEnum(Name="ConfigableConstId_GuildStaffingLogExpireDay", Value=0xc2)]
        ConfigableConstId_GuildStaffingLogExpireDay = 0xc2,
        [ProtoEnum(Name="ConfigableConstId_ChipSchemeUnlockTradeMoneyCost2", Value=0xc3)]
        ConfigableConstId_ChipSchemeUnlockTradeMoneyCost2 = 0xc3,
        [ProtoEnum(Name="ConfigableConstId_ChipSchemeUnlockTradeMoneyCost3", Value=0xc4)]
        ConfigableConstId_ChipSchemeUnlockTradeMoneyCost3 = 0xc4,
        [ProtoEnum(Name="ConfigableConstId_MaxGuildActionJumpDistance", Value=0xc5)]
        ConfigableConstId_MaxGuildActionJumpDistance = 0xc5,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionCancelCostReturnPercent", Value=0xc6)]
        ConfigableConstId_GuildProductionCancelCostReturnPercent = 0xc6,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionLowLevelSpeedUpCost", Value=0xc7)]
        ConfigableConstId_GuildProductionLowLevelSpeedUpCost = 0xc7,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionMidLevelSpeedUpCost", Value=200)]
        ConfigableConstId_GuildProductionMidLevelSpeedUpCost = 200,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionHighLevelSpeedUpCost", Value=0xc9)]
        ConfigableConstId_GuildProductionHighLevelSpeedUpCost = 0xc9,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionLowLevelSpeedUpReduceTime", Value=0xca)]
        ConfigableConstId_GuildProductionLowLevelSpeedUpReduceTime = 0xca,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionMidLevelSpeedUpReduceTime", Value=0xcb)]
        ConfigableConstId_GuildProductionMidLevelSpeedUpReduceTime = 0xcb,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionHighLevelSpeedUpReduceTime", Value=0xcc)]
        ConfigableConstId_GuildProductionHighLevelSpeedUpReduceTime = 0xcc,
        [ProtoEnum(Name="ConfigableConstId_GuildDonateTradeMoneySumLimitOneDay", Value=0xcd)]
        ConfigableConstId_GuildDonateTradeMoneySumLimitOneDay = 0xcd,
        [ProtoEnum(Name="ConfigableConstId_GuildEditorAnnouncementCountdownTime", Value=0xce)]
        ConfigableConstId_GuildEditorAnnouncementCountdownTime = 0xce,
        [ProtoEnum(Name="ConfigableConstId_GuildPurchaseItemCountLimit", Value=0xcf)]
        ConfigableConstId_GuildPurchaseItemCountLimit = 0xcf,
        [ProtoEnum(Name="ConfigableConstId_GuildActionSolarSystemGuildMineralOutputRateMin", Value=0xd0)]
        ConfigableConstId_GuildActionSolarSystemGuildMineralOutputRateMin = 0xd0,
        [ProtoEnum(Name="ConfigableConstId_GuildActionValidTime", Value=0xd1)]
        ConfigableConstId_GuildActionValidTime = 0xd1,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationExpireDays", Value=210)]
        ConfigableConstId_GuildCompensationExpireDays = 210,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationTimeoutHours", Value=0xd3)]
        ConfigableConstId_GuildCompensationTimeoutHours = 0xd3,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationDailyDonateCountLimit", Value=0xd4)]
        ConfigableConstId_GuildCompensationDailyDonateCountLimit = 0xd4,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationDefaultAutomaticCompensation", Value=0xd5)]
        ConfigableConstId_GuildCompensationDefaultAutomaticCompensation = 0xd5,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryProbeUseCoolingDownTime", Value=0xd6)]
        ConfigableConstId_GuildSentryProbeUseCoolingDownTime = 0xd6,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryTargetKeepingTimeAfterLeaveSolarSystem", Value=0xd7)]
        ConfigableConstId_GuildSentryTargetKeepingTimeAfterLeaveSolarSystem = 0xd7,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValueEnterSolarSystem", Value=0xd8)]
        ConfigableConstId_GuildSentryThreatValueEnterSolarSystem = 0xd8,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValueJumpEnd", Value=0xd9)]
        ConfigableConstId_GuildSentryThreatValueJumpEnd = 0xd9,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValueEnterQuestScene", Value=0xda)]
        ConfigableConstId_GuildSentryThreatValueEnterQuestScene = 0xda,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValueEnterInvadeScene", Value=0xdb)]
        ConfigableConstId_GuildSentryThreatValueEnterInvadeScene = 0xdb,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValuePVPAttack", Value=220)]
        ConfigableConstId_GuildSentryThreatValuePVPAttack = 220,
        [ProtoEnum(Name="ConfigableConstId_GuildSentryThreatValuePVPKill", Value=0xdd)]
        ConfigableConstId_GuildSentryThreatValuePVPKill = 0xdd,
        [ProtoEnum(Name="ConfigableConstId_SolarSystemFlourishValueDowngradingParam", Value=0xde)]
        ConfigableConstId_SolarSystemFlourishValueDowngradingParam = 0xde,
        [ProtoEnum(Name="ConfigableConstId_GuildBuildingBattlePrepareTime", Value=0xdf)]
        ConfigableConstId_GuildBuildingBattlePrepareTime = 0xdf,
        [ProtoEnum(Name="ConfigableConstId_GuildBuildingBattleReinforcementEndRandTime", Value=0xe0)]
        ConfigableConstId_GuildBuildingBattleReinforcementEndRandTime = 0xe0,
        [ProtoEnum(Name="ConfigableConstId_GuildSovereignBattlePrepareTime", Value=0xe1)]
        ConfigableConstId_GuildSovereignBattlePrepareTime = 0xe1,
        [ProtoEnum(Name="ConfigableConstId_GuildSovereignBattleReinforcementEndRandTime", Value=0xe2)]
        ConfigableConstId_GuildSovereignBattleReinforcementEndRandTime = 0xe2,
        [ProtoEnum(Name="ConfigableConstId_GuildSovereignBattleDeclarationDuringTime", Value=0xe3)]
        ConfigableConstId_GuildSovereignBattleDeclarationDuringTime = 0xe3,
        [ProtoEnum(Name="ConfigableConstId_GuildSovereignBattleNextStartMinTime", Value=0xe4)]
        ConfigableConstId_GuildSovereignBattleNextStartMinTime = 0xe4,
        [ProtoEnum(Name="ConfigableConstId_GuildBuildingDeployUpgradeMaxCount", Value=0xe5)]
        ConfigableConstId_GuildBuildingDeployUpgradeMaxCount = 0xe5,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionDefaultCount", Value=230)]
        ConfigableConstId_GuildProductionDefaultCount = 230,
        [ProtoEnum(Name="ConfigableConstId_GuildDonateTradeMoneyJoinTimeLimit", Value=0xe7)]
        ConfigableConstId_GuildDonateTradeMoneyJoinTimeLimit = 0xe7,
        [ProtoEnum(Name="ConfigableConstId_BattleSceneTotalPlayerCountLimit", Value=0xe8)]
        ConfigableConstId_BattleSceneTotalPlayerCountLimit = 0xe8,
        [ProtoEnum(Name="ConfigableConstId_BattleSceneTriggerKickPlayerCountThreshold", Value=0xe9)]
        ConfigableConstId_BattleSceneTriggerKickPlayerCountThreshold = 0xe9,
        [ProtoEnum(Name="ConfigableConstId_BattleSceneKickPlayerCountdownSeconds", Value=0xea)]
        ConfigableConstId_BattleSceneKickPlayerCountdownSeconds = 0xea,
        [ProtoEnum(Name="ConfigableConstId_GuildCurrencyLogListPerPage", Value=0xeb)]
        ConfigableConstId_GuildCurrencyLogListPerPage = 0xeb,
        [ProtoEnum(Name="ConfigableConstId_HiredCaptainArrivedAudioDelayTime", Value=0xec)]
        ConfigableConstId_HiredCaptainArrivedAudioDelayTime = 0xec,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalancingOverTime", Value=0xed)]
        ConfigableConstId_GuildMiningBalancingOverTime = 0xed,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalanceTimeTimeSpan", Value=0xee)]
        ConfigableConstId_GuildMiningBalanceTimeTimeSpan = 0xee,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalanceTimeRandomValue", Value=0xef)]
        ConfigableConstId_GuildMiningBalanceTimeRandomValue = 0xef,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalancingTimeForOneRound", Value=240)]
        ConfigableConstId_GuildMiningBalancingTimeForOneRound = 240,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalancingRoundTimeSpan", Value=0xf1)]
        ConfigableConstId_GuildMiningBalancingRoundTimeSpan = 0xf1,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalanceShipDropRatio", Value=0xf2)]
        ConfigableConstId_GuildMiningBalanceShipDropRatio = 0xf2,
        [ProtoEnum(Name="ConfigableConstId_GuildDonateWeekRankingRefreshFrequency", Value=0xf3)]
        ConfigableConstId_GuildDonateWeekRankingRefreshFrequency = 0xf3,
        [ProtoEnum(Name="ConfigableConstId_GuildActionCompletedCloseTime", Value=0xf4)]
        ConfigableConstId_GuildActionCompletedCloseTime = 0xf4,
        [ProtoEnum(Name="ConfigableConstId_GuildBattleReinforcementEndTimeSetCD", Value=0xf5)]
        ConfigableConstId_GuildBattleReinforcementEndTimeSetCD = 0xf5,
        [ProtoEnum(Name="ConfigableConstId_GuildMiningBalanceShipStayTime", Value=0xf6)]
        ConfigableConstId_GuildMiningBalanceShipStayTime = 0xf6,
        [ProtoEnum(Name="ConfigableConstId_BattleManipulationGuideCondition_MaxLevel", Value=0xf7)]
        ConfigableConstId_BattleManipulationGuideCondition_MaxLevel = 0xf7,
        [ProtoEnum(Name="ConfigableConstId_BattleManipulationGuideCondition_CompleteQuest", Value=0xf8)]
        ConfigableConstId_BattleManipulationGuideCondition_CompleteQuest = 0xf8,
        [ProtoEnum(Name="ConfigableConstId_GuildMessageExpireDay", Value=0xf9)]
        ConfigableConstId_GuildMessageExpireDay = 0xf9,
        [ProtoEnum(Name="ConfigableConstId_GuildAnnoucementLengthLimit", Value=250)]
        ConfigableConstId_GuildAnnoucementLengthLimit = 250,
        [ProtoEnum(Name="ConfigableConstId_GuildManifestoLengthLimit", Value=0xfb)]
        ConfigableConstId_GuildManifestoLengthLimit = 0xfb,
        [ProtoEnum(Name="ConfigableConstId_GuildStoreAnnoucementLengthLimit", Value=0xfc)]
        ConfigableConstId_GuildStoreAnnoucementLengthLimit = 0xfc,
        [ProtoEnum(Name="ConfigableConstId_OperationCDTimeInSpace", Value=0xfd)]
        ConfigableConstId_OperationCDTimeInSpace = 0xfd,
        [ProtoEnum(Name="ConfigableConstId_SearchMapSubstringMaxChar", Value=0xfe)]
        ConfigableConstId_SearchMapSubstringMaxChar = 0xfe,
        [ProtoEnum(Name="ConfigableConstId_AFKExpFactor", Value=0xff)]
        ConfigableConstId_AFKExpFactor = 0xff,
        [ProtoEnum(Name="ConfigableConstId_AllianceCreateGuildTradeMoneyCost", Value=0x100)]
        ConfigableConstId_AllianceCreateGuildTradeMoneyCost = 0x100,
        [ProtoEnum(Name="ConfigableConstId_AllianceNameChangeGuildTradeMoneyCost", Value=0x101)]
        ConfigableConstId_AllianceNameChangeGuildTradeMoneyCost = 0x101,
        [ProtoEnum(Name="ConfigableConstId_AllianceLogoChangeGuildTradeMoneyCost", Value=0x102)]
        ConfigableConstId_AllianceLogoChangeGuildTradeMoneyCost = 0x102,
        [ProtoEnum(Name="ConfigableConstId_AllianceRejoinCDHours", Value=0x103)]
        ConfigableConstId_AllianceRejoinCDHours = 0x103,
        [ProtoEnum(Name="ConfigableConstId_AllianceRegionChangeCDMinutes", Value=260)]
        ConfigableConstId_AllianceRegionChangeCDMinutes = 260,
        [ProtoEnum(Name="ConfigableConstId_AllianceAnnouncementChangeCDMinutes", Value=0x105)]
        ConfigableConstId_AllianceAnnouncementChangeCDMinutes = 0x105,
        [ProtoEnum(Name="ConfigableConstId_PanGalaNpcShopSaleItemListID", Value=0x106)]
        ConfigableConstId_PanGalaNpcShopSaleItemListID = 0x106,
        [ProtoEnum(Name="ConfigableConstId_TradeLegalRefreshTimeSpanMinutes", Value=0x107)]
        ConfigableConstId_TradeLegalRefreshTimeSpanMinutes = 0x107,
        [ProtoEnum(Name="ConfigableConstId_TradeIllegalRefreshTimeSpanMinutes", Value=0x108)]
        ConfigableConstId_TradeIllegalRefreshTimeSpanMinutes = 0x108,
        [ProtoEnum(Name="ConfigableConstId_AllianceInviteExpireTimeHours", Value=0x109)]
        ConfigableConstId_AllianceInviteExpireTimeHours = 0x109,
        [ProtoEnum(Name="ConfigableConstId_LimitDamageBufResetPeriod", Value=0x10a)]
        ConfigableConstId_LimitDamageBufResetPeriod = 0x10a,
        [ProtoEnum(Name="ConfigableConstId_HangarShipSalvageRealmMoneyCost", Value=0x10b)]
        ConfigableConstId_HangarShipSalvageRealmMoneyCost = 0x10b,
        [ProtoEnum(Name="ConfigableConstId_HangarShipSalvageItemId", Value=0x10c)]
        ConfigableConstId_HangarShipSalvageItemId = 0x10c,
        [ProtoEnum(Name="ConfigableConstId_SolarSystemTransferTimeOut", Value=0x10d)]
        ConfigableConstId_SolarSystemTransferTimeOut = 0x10d,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTransportShipLeaveSceneStayTime", Value=0x10f)]
        ConfigableConstId_GuildTradeTransportShipLeaveSceneStayTime = 0x10f,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTransportShipEnterSceneStayTime", Value=0x110)]
        ConfigableConstId_GuildTradeTransportShipEnterSceneStayTime = 0x110,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTransportShipJumpingTimeWithinSolarSystem", Value=0x111)]
        ConfigableConstId_GuildTradeTransportShipJumpingTimeWithinSolarSystem = 0x111,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTransportShipJumpingTimeBetweenSolarSystem", Value=0x112)]
        ConfigableConstId_GuildTradeTransportShipJumpingTimeBetweenSolarSystem = 0x112,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTransportShipDropRatio", Value=0x113)]
        ConfigableConstId_GuildTradeTransportShipDropRatio = 0x113,
        [ProtoEnum(Name="ConfigableConstId_BattlePassLevelExp", Value=0x114)]
        ConfigableConstId_BattlePassLevelExp = 0x114,
        [ProtoEnum(Name="ConfigableConstId_BattlePassLevelPrice", Value=0x115)]
        ConfigableConstId_BattlePassLevelPrice = 0x115,
        [ProtoEnum(Name="ConfigableConstId_BattlePassUpgradePrice", Value=0x116)]
        ConfigableConstId_BattlePassUpgradePrice = 0x116,
        [ProtoEnum(Name="ConfigableConstId_BattlePassUpgradePackagePrice", Value=0x117)]
        ConfigableConstId_BattlePassUpgradePackagePrice = 0x117,
        [ProtoEnum(Name="ConfigableConstId_BattlePassUpgradeExpBonus", Value=280)]
        ConfigableConstId_BattlePassUpgradeExpBonus = 280,
        [ProtoEnum(Name="ConfigableConstId_BattlePassPeriodDuration", Value=0x119)]
        ConfigableConstId_BattlePassPeriodDuration = 0x119,
        [ProtoEnum(Name="ConfigableConstId_BattlePassChallageGroupDuration", Value=0x11a)]
        ConfigableConstId_BattlePassChallageGroupDuration = 0x11a,
        [ProtoEnum(Name="ConfigableConstId_GuildCurrencyLogListMaxCount", Value=0x11b)]
        ConfigableConstId_GuildCurrencyLogListMaxCount = 0x11b,
        [ProtoEnum(Name="ConfigableConstId_BattlePassUpgradePackageAddLevel", Value=0x11c)]
        ConfigableConstId_BattlePassUpgradePackageAddLevel = 0x11c,
        [ProtoEnum(Name="ConfigableConstId_BattlePassRefreshHour", Value=0x11d)]
        ConfigableConstId_BattlePassRefreshHour = 0x11d,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeTax", Value=0x11e)]
        ConfigableConstId_GuildTradeTax = 0x11e,
        [ProtoEnum(Name="ConfigableConstId_FactionCreditMax", Value=0x11f)]
        ConfigableConstId_FactionCreditMax = 0x11f,
        [ProtoEnum(Name="ConfigableConstId_FactionCreditMin", Value=0x120)]
        ConfigableConstId_FactionCreditMin = 0x120,
        [ProtoEnum(Name="ConfigableConstId_GuildInformationPointDonateGuildGalaFactor", Value=0x121)]
        ConfigableConstId_GuildInformationPointDonateGuildGalaFactor = 0x121,
        [ProtoEnum(Name="ConfigableConstId_GuildTradeMoneyDonateGuildGalaFactor", Value=290)]
        ConfigableConstId_GuildTradeMoneyDonateGuildGalaFactor = 290,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationDonateGuildGalaFactor", Value=0x123)]
        ConfigableConstId_GuildCompensationDonateGuildGalaFactor = 0x123,
        [ProtoEnum(Name="ConfigableConstId_RechargeRMBRealMoneyExchangeFactor", Value=0x124)]
        ConfigableConstId_RechargeRMBRealMoneyExchangeFactor = 0x124,
        [ProtoEnum(Name="ConfigableConstId_GuildFalgShipFuelItemId", Value=0x125)]
        ConfigableConstId_GuildFalgShipFuelItemId = 0x125,
        [ProtoEnum(Name="ConfigableConstId_RechargeOrderTimeoutSeconds", Value=0x126)]
        ConfigableConstId_RechargeOrderTimeoutSeconds = 0x126,
        [ProtoEnum(Name="ConfigableConstId_GuildEvaluateScoreMinValue", Value=0x127)]
        ConfigableConstId_GuildEvaluateScoreMinValue = 0x127,
        [ProtoEnum(Name="ConfigableConstId_AllianceEvaluateScoreMinValue", Value=0x128)]
        ConfigableConstId_AllianceEvaluateScoreMinValue = 0x128,
        [ProtoEnum(Name="ConfigableConstId_SpaceSignalNavigationFailedSignalConfigId", Value=0x129)]
        ConfigableConstId_SpaceSignalNavigationFailedSignalConfigId = 0x129,
        [ProtoEnum(Name="ConfigableConstId_MovementStateStartReturn2RequiredPositionBufId", Value=0x12a)]
        ConfigableConstId_MovementStateStartReturn2RequiredPositionBufId = 0x12a,
        [ProtoEnum(Name="ConfigableConstId_CheckFreeQuestLowRewardMaxLevel", Value=0x12b)]
        ConfigableConstId_CheckFreeQuestLowRewardMaxLevel = 0x12b,
        [ProtoEnum(Name="ConfigableConstId_RechargeMonthlyCardNearExpireNotificationHours", Value=300)]
        ConfigableConstId_RechargeMonthlyCardNearExpireNotificationHours = 300,
        [ProtoEnum(Name="ConfigableConstId_GuildFlagShipOptLogListMaxCount", Value=0x12d)]
        ConfigableConstId_GuildFlagShipOptLogListMaxCount = 0x12d,
        [ProtoEnum(Name="ConfigableConstId_SpaceSignalCooldownTime", Value=0x12e)]
        ConfigableConstId_SpaceSignalCooldownTime = 0x12e,
        [ProtoEnum(Name="ConfigableConstId_FlagShipInitSuperWeaponEquipOrignalEnergyPercent", Value=0x12f)]
        ConfigableConstId_FlagShipInitSuperWeaponEquipOrignalEnergyPercent = 0x12f,
        [ProtoEnum(Name="ConfigableConstId_FlagShipJumpingEndSuperWeaponEquipOrignalEnergyPercent", Value=0x130)]
        ConfigableConstId_FlagShipJumpingEndSuperWeaponEquipOrignalEnergyPercent = 0x130,
        [ProtoEnum(Name="ConfigableConstId_FactionCreditQuestrCooldownHour", Value=0x131)]
        ConfigableConstId_FactionCreditQuestrCooldownHour = 0x131,
        [ProtoEnum(Name="ConfigableConstId_FactionCreditRefreshCooldownHour", Value=0x132)]
        ConfigableConstId_FactionCreditRefreshCooldownHour = 0x132,
        [ProtoEnum(Name="ConfigableConstId_AppScoreFirstCheckQuestId", Value=0x133)]
        ConfigableConstId_AppScoreFirstCheckQuestId = 0x133,
        [ProtoEnum(Name="ConfigableConstId_AppScoreSecondCheckQuestId", Value=0x134)]
        ConfigableConstId_AppScoreSecondCheckQuestId = 0x134,
        [ProtoEnum(Name="ConfigableConstId_AllianceMailSendCDSeconds", Value=0x135)]
        ConfigableConstId_AllianceMailSendCDSeconds = 0x135,
        [ProtoEnum(Name="ConfigableConstId_GuildFlagShipOffLineAutoPackTime", Value=310)]
        ConfigableConstId_GuildFlagShipOffLineAutoPackTime = 310,
        [ProtoEnum(Name="ConfigableConstId_CheckFreeQuestLowRewardIntervalLevel", Value=0x137)]
        ConfigableConstId_CheckFreeQuestLowRewardIntervalLevel = 0x137,
        [ProtoEnum(Name="ConfigableConstId_GuildBenefitsAfterJoinHours", Value=0x138)]
        ConfigableConstId_GuildBenefitsAfterJoinHours = 0x138,
        [ProtoEnum(Name="ConfigableConstId_DefaultCameraOffsetMultiForFlagShip", Value=0x139)]
        ConfigableConstId_DefaultCameraOffsetMultiForFlagShip = 0x139,
        [ProtoEnum(Name="ConfigableConstId_CommanderAuthRewardCfgId", Value=0x13a)]
        ConfigableConstId_CommanderAuthRewardCfgId = 0x13a,
        [ProtoEnum(Name="ConfigableConstId_CMDailySignRandRewordCfgId", Value=0x13b)]
        ConfigableConstId_CMDailySignRandRewordCfgId = 0x13b,
        [ProtoEnum(Name="ConfigableConstId_CMDailySignRandRewordCycleDay", Value=0x13c)]
        ConfigableConstId_CMDailySignRandRewordCycleDay = 0x13c,
        [ProtoEnum(Name="ConfigableConstId_MaxKickCountPerTick", Value=0x13d)]
        ConfigableConstId_MaxKickCountPerTick = 0x13d,
        [ProtoEnum(Name="ConfigableConstId_BattleSceneTotalPlayerCountLimitAgainstNpc", Value=0x13e)]
        ConfigableConstId_BattleSceneTotalPlayerCountLimitAgainstNpc = 0x13e,
        [ProtoEnum(Name="ConfigableConstId_BattleSceneTriggerKickPlayerCountThresholdAgainstNpc", Value=0x13f)]
        ConfigableConstId_BattleSceneTriggerKickPlayerCountThresholdAgainstNpc = 0x13f,
        [ProtoEnum(Name="ConfigableConstId_GuildCompensationCountLimit", Value=320)]
        ConfigableConstId_GuildCompensationCountLimit = 320,
        [ProtoEnum(Name="ConfigableConstId_AllianceMemberCountLimit", Value=0x141)]
        ConfigableConstId_AllianceMemberCountLimit = 0x141,
        [ProtoEnum(Name="ConfigableConstId_RealPVPInvadeExtraRewardForDelegateFight", Value=0x142)]
        ConfigableConstId_RealPVPInvadeExtraRewardForDelegateFight = 0x142,
        [ProtoEnum(Name="ConfigableConstId_RealPVPInvadeExtraRewardForDelegateMineral", Value=0x143)]
        ConfigableConstId_RealPVPInvadeExtraRewardForDelegateMineral = 0x143,
        [ProtoEnum(Name="ConfigableConstId_LowDisplayPerformanceFpsValue", Value=0x144)]
        ConfigableConstId_LowDisplayPerformanceFpsValue = 0x144,
        [ProtoEnum(Name="ConfigableConstId_MediumDisplayPerformanceFpsValue", Value=0x145)]
        ConfigableConstId_MediumDisplayPerformanceFpsValue = 0x145,
        [ProtoEnum(Name="ConfigableConstId_SingleFrameTickEffectCreateCountLimit", Value=0x146)]
        ConfigableConstId_SingleFrameTickEffectCreateCountLimit = 0x146,
        [ProtoEnum(Name="ConfigableConstId_GooglePreRegistrationRewardCfgId", Value=0x147)]
        ConfigableConstId_GooglePreRegistrationRewardCfgId = 0x147,
        [ProtoEnum(Name="ConfigableConstId_GuildProductionLineShownMaxCountInStation", Value=0x148)]
        ConfigableConstId_GuildProductionLineShownMaxCountInStation = 0x148,
        [ProtoEnum(Name="ConfigableConstId_AuctionItemOnShelveMaxCount", Value=0x149)]
        ConfigableConstId_AuctionItemOnShelveMaxCount = 0x149
    }
}

