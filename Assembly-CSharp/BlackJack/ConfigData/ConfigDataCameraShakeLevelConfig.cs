﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCameraShakeLevelConfig")]
    public class ConfigDataCameraShakeLevelConfig : IExtensible
    {
        private int _ID;
        private float _Disturb;
        private float _LifeTime;
        private bool _IncludeZ;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Disturb;
        private static DelegateBridge __Hotfix_set_Disturb;
        private static DelegateBridge __Hotfix_get_LifeTime;
        private static DelegateBridge __Hotfix_set_LifeTime;
        private static DelegateBridge __Hotfix_get_IncludeZ;
        private static DelegateBridge __Hotfix_set_IncludeZ;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Disturb", DataFormat=DataFormat.FixedSize)]
        public float Disturb
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="LifeTime", DataFormat=DataFormat.FixedSize)]
        public float LifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="IncludeZ", DataFormat=DataFormat.Default)]
        public bool IncludeZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

