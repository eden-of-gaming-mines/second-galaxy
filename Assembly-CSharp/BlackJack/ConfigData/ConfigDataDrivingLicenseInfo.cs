﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDrivingLicenseInfo")]
    public class ConfigDataDrivingLicenseInfo : IExtensible
    {
        private int _ID;
        private ShipType _Type;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private RankType _Rank;
        private int _MaxLevel;
        private readonly List<int> _DependsOnShips;
        private readonly List<int> _MiddleSlotUnlockList;
        private readonly List<int> _LowSlotUnlockList;
        private string _NameStrKey;
        private string _ShortNameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_MaxLevel;
        private static DelegateBridge __Hotfix_set_MaxLevel;
        private static DelegateBridge __Hotfix_get_DependsOnShips;
        private static DelegateBridge __Hotfix_get_MiddleSlotUnlockList;
        private static DelegateBridge __Hotfix_get_LowSlotUnlockList;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_ShortNameStrKey;
        private static DelegateBridge __Hotfix_set_ShortNameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public ShipType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="MaxLevel", DataFormat=DataFormat.TwosComplement)]
        public int MaxLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="DependsOnShips", DataFormat=DataFormat.TwosComplement)]
        public List<int> DependsOnShips
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, Name="MiddleSlotUnlockList", DataFormat=DataFormat.TwosComplement)]
        public List<int> MiddleSlotUnlockList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="LowSlotUnlockList", DataFormat=DataFormat.TwosComplement)]
        public List<int> LowSlotUnlockList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="ShortNameStrKey", DataFormat=DataFormat.Default)]
        public string ShortNameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

