﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDevelopmentFilterInfo")]
    public class ConfigDataDevelopmentFilterInfo : IExtensible
    {
        private int _ID;
        private StoreItemType _StoreItemTypeFilter;
        private GrandFaction _GrandFactionFilter;
        private ShipType _ShipTypeFilter;
        private ShipSizeType _ShipSizeTypeFilter;
        private WeaponCategory _WeaponCategoryFilter;
        private EquipCategory _EquipCategoryFilter;
        private PropertyCategory _ChipCategoryFilter;
        private ChipType _ChipTypeFilter;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_StoreItemTypeFilter;
        private static DelegateBridge __Hotfix_set_StoreItemTypeFilter;
        private static DelegateBridge __Hotfix_get_GrandFactionFilter;
        private static DelegateBridge __Hotfix_set_GrandFactionFilter;
        private static DelegateBridge __Hotfix_get_ShipTypeFilter;
        private static DelegateBridge __Hotfix_set_ShipTypeFilter;
        private static DelegateBridge __Hotfix_get_ShipSizeTypeFilter;
        private static DelegateBridge __Hotfix_set_ShipSizeTypeFilter;
        private static DelegateBridge __Hotfix_get_WeaponCategoryFilter;
        private static DelegateBridge __Hotfix_set_WeaponCategoryFilter;
        private static DelegateBridge __Hotfix_get_EquipCategoryFilter;
        private static DelegateBridge __Hotfix_set_EquipCategoryFilter;
        private static DelegateBridge __Hotfix_get_ChipCategoryFilter;
        private static DelegateBridge __Hotfix_set_ChipCategoryFilter;
        private static DelegateBridge __Hotfix_get_ChipTypeFilter;
        private static DelegateBridge __Hotfix_set_ChipTypeFilter;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StoreItemTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType StoreItemTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GrandFactionFilter", DataFormat=DataFormat.TwosComplement)]
        public GrandFaction GrandFactionFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShipTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public ShipType ShipTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ShipSizeTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType ShipSizeTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="WeaponCategoryFilter", DataFormat=DataFormat.TwosComplement)]
        public WeaponCategory WeaponCategoryFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="EquipCategoryFilter", DataFormat=DataFormat.TwosComplement)]
        public EquipCategory EquipCategoryFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ChipCategoryFilter", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory ChipCategoryFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="ChipTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public ChipType ChipTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

