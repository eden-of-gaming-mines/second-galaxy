﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipType")]
    public enum ShipType
    {
        [ProtoEnum(Name="ShipType_Invalid", Value=0)]
        ShipType_Invalid = 0,
        [ProtoEnum(Name="ShipType_Frigate", Value=1)]
        ShipType_Frigate = 1,
        [ProtoEnum(Name="ShipType_Destroyer", Value=2)]
        ShipType_Destroyer = 2,
        [ProtoEnum(Name="ShipType_Cruiser", Value=3)]
        ShipType_Cruiser = 3,
        [ProtoEnum(Name="ShipType_BattleCruiser", Value=4)]
        ShipType_BattleCruiser = 4,
        [ProtoEnum(Name="ShipType_BattleShip", Value=5)]
        ShipType_BattleShip = 5,
        [ProtoEnum(Name="ShipType_Carrier", Value=6)]
        ShipType_Carrier = 6,
        [ProtoEnum(Name="ShipType_Flagship", Value=7)]
        ShipType_Flagship = 7,
        [ProtoEnum(Name="ShipType_Industry", Value=8)]
        ShipType_Industry = 8,
        [ProtoEnum(Name="ShipType_Building", Value=9)]
        ShipType_Building = 9
    }
}

