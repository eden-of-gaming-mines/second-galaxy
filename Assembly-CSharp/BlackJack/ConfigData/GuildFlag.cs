﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildFlag")]
    public enum GuildFlag
    {
        [ProtoEnum(Name="GuildFlag_None", Value=0)]
        GuildFlag_None = 0,
        [ProtoEnum(Name="GuildFlag_SolarSystemPlanetMined", Value=1)]
        GuildFlag_SolarSystemPlanetMined = 1,
        [ProtoEnum(Name="GuildFlag_SolarSystemFlagShipDocking", Value=2)]
        GuildFlag_SolarSystemFlagShipDocking = 2
    }
}

