﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCommonFuncNpcTalkerSelectInfo")]
    public class ConfigDataCommonFuncNpcTalkerSelectInfo : IExtensible
    {
        private int _ID;
        private GrandFaction _Race;
        private NpcFunctionType _FunctionType;
        private int _NpcTalkerTemplateId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Race;
        private static DelegateBridge __Hotfix_set_Race;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_NpcTalkerTemplateId;
        private static DelegateBridge __Hotfix_set_NpcTalkerTemplateId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Race", DataFormat=DataFormat.TwosComplement)]
        public GrandFaction Race
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public NpcFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NpcTalkerTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int NpcTalkerTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

