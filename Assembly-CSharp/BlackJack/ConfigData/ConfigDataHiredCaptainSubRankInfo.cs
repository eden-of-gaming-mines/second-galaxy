﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataHiredCaptainSubRankInfo")]
    public class ConfigDataHiredCaptainSubRankInfo : IExtensible
    {
        private int _ID;
        private SubRankType _SubRank;
        private int _InitFeats;
        private double _RetireReserveFeatProbability;
        private int _RetireExp;
        private double _RetireExpDoubleProbability;
        private int _BasicScore;
        private int _LevelScore;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_InitFeats;
        private static DelegateBridge __Hotfix_set_InitFeats;
        private static DelegateBridge __Hotfix_get_RetireReserveFeatProbability;
        private static DelegateBridge __Hotfix_set_RetireReserveFeatProbability;
        private static DelegateBridge __Hotfix_get_RetireExp;
        private static DelegateBridge __Hotfix_set_RetireExp;
        private static DelegateBridge __Hotfix_get_RetireExpDoubleProbability;
        private static DelegateBridge __Hotfix_set_RetireExpDoubleProbability;
        private static DelegateBridge __Hotfix_get_BasicScore;
        private static DelegateBridge __Hotfix_set_BasicScore;
        private static DelegateBridge __Hotfix_get_LevelScore;
        private static DelegateBridge __Hotfix_set_LevelScore;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="InitFeats", DataFormat=DataFormat.TwosComplement)]
        public int InitFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="RetireReserveFeatProbability", DataFormat=DataFormat.TwosComplement)]
        public double RetireReserveFeatProbability
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="RetireExp", DataFormat=DataFormat.TwosComplement)]
        public int RetireExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="RetireExpDoubleProbability", DataFormat=DataFormat.TwosComplement)]
        public double RetireExpDoubleProbability
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BasicScore", DataFormat=DataFormat.TwosComplement)]
        public int BasicScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="LevelScore", DataFormat=DataFormat.TwosComplement)]
        public int LevelScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

