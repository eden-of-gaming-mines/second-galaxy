﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcTalkerInfo")]
    public class ConfigDataNpcTalkerInfo : IExtensible
    {
        private int _ID;
        private string _Name;
        private NpcFunctionType _FunctionType;
        private SpaceStationRoomType _RoomType;
        private GenderType _Gender;
        private NpcPersonalityType _PersonalityType;
        private NpcTalkerScriptType _ScriptType;
        private int _ResId;
        private bool _IsStatic;
        private readonly List<int> _AcceptableQuestIdList;
        private int _ScriptParam;
        private int _HelloDialogId;
        private readonly List<HelloDialogIdByCompletedMainQuestInfo> _HelloDialogIdByCompletedMainQuest;
        private int _SortOrder;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_RoomType;
        private static DelegateBridge __Hotfix_set_RoomType;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_PersonalityType;
        private static DelegateBridge __Hotfix_set_PersonalityType;
        private static DelegateBridge __Hotfix_get_ScriptType;
        private static DelegateBridge __Hotfix_set_ScriptType;
        private static DelegateBridge __Hotfix_get_ResId;
        private static DelegateBridge __Hotfix_set_ResId;
        private static DelegateBridge __Hotfix_get_IsStatic;
        private static DelegateBridge __Hotfix_set_IsStatic;
        private static DelegateBridge __Hotfix_get_AcceptableQuestIdList;
        private static DelegateBridge __Hotfix_get_ScriptParam;
        private static DelegateBridge __Hotfix_set_ScriptParam;
        private static DelegateBridge __Hotfix_get_HelloDialogId;
        private static DelegateBridge __Hotfix_set_HelloDialogId;
        private static DelegateBridge __Hotfix_get_HelloDialogIdByCompletedMainQuest;
        private static DelegateBridge __Hotfix_get_SortOrder;
        private static DelegateBridge __Hotfix_set_SortOrder;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public NpcFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="RoomType", DataFormat=DataFormat.TwosComplement)]
        public SpaceStationRoomType RoomType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Gender", DataFormat=DataFormat.TwosComplement)]
        public GenderType Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="PersonalityType", DataFormat=DataFormat.TwosComplement)]
        public NpcPersonalityType PersonalityType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ScriptType", DataFormat=DataFormat.TwosComplement)]
        public NpcTalkerScriptType ScriptType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ResId", DataFormat=DataFormat.TwosComplement)]
        public int ResId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="IsStatic", DataFormat=DataFormat.Default)]
        public bool IsStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="AcceptableQuestIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> AcceptableQuestIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="ScriptParam", DataFormat=DataFormat.TwosComplement)]
        public int ScriptParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="HelloDialogId", DataFormat=DataFormat.TwosComplement)]
        public int HelloDialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, Name="HelloDialogIdByCompletedMainQuest", DataFormat=DataFormat.Default)]
        public List<HelloDialogIdByCompletedMainQuestInfo> HelloDialogIdByCompletedMainQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="SortOrder", DataFormat=DataFormat.TwosComplement)]
        public int SortOrder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

