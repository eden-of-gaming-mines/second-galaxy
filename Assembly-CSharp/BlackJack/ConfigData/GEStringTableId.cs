﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GEStringTableId")]
    public enum GEStringTableId
    {
        [ProtoEnum(Name="GEStringTableId_Star", Value=1)]
        GEStringTableId_Star = 1,
        [ProtoEnum(Name="GEStringTableId_Planet", Value=2)]
        GEStringTableId_Planet = 2,
        [ProtoEnum(Name="GEStringTableId_Moon", Value=3)]
        GEStringTableId_Moon = 3,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_1", Value=4)]
        GEStringTableId_RomanNumeral_1 = 4,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_2", Value=5)]
        GEStringTableId_RomanNumeral_2 = 5,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_3", Value=6)]
        GEStringTableId_RomanNumeral_3 = 6,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_4", Value=7)]
        GEStringTableId_RomanNumeral_4 = 7,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_5", Value=8)]
        GEStringTableId_RomanNumeral_5 = 8,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_6", Value=9)]
        GEStringTableId_RomanNumeral_6 = 9,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_7", Value=10)]
        GEStringTableId_RomanNumeral_7 = 10,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_8", Value=11)]
        GEStringTableId_RomanNumeral_8 = 11,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_9", Value=12)]
        GEStringTableId_RomanNumeral_9 = 12,
        [ProtoEnum(Name="GEStringTableId_RomanNumeral_10", Value=13)]
        GEStringTableId_RomanNumeral_10 = 13
    }
}

