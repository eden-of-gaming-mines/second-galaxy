﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildPropertiesId")]
    public enum GuildPropertiesId
    {
        [ProtoEnum(Name="GuildPropertiesId_None", Value=0)]
        GuildPropertiesId_None = 0,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemPlayerQuestRewordMulti", Value=1)]
        GuildPropertiesId_SolarSystemPlayerQuestRewordMulti = 1,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemPlanetMinedMulti", Value=2)]
        GuildPropertiesId_SolarSystemPlanetMinedMulti = 2,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemProductionSpeedMulti", Value=3)]
        GuildPropertiesId_SolarSystemProductionSpeedMulti = 3,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemProductionCostMulti", Value=4)]
        GuildPropertiesId_SolarSystemProductionCostMulti = 4,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemFacilityProductionSpeedMulti", Value=5)]
        GuildPropertiesId_SolarSystemFacilityProductionSpeedMulti = 5,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemFacilityProductionCostMulti", Value=6)]
        GuildPropertiesId_SolarSystemFacilityProductionCostMulti = 6,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemFlagShipProductionSpeedMulti", Value=7)]
        GuildPropertiesId_SolarSystemFlagShipProductionSpeedMulti = 7,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemFlagShipProductionCostMulti", Value=8)]
        GuildPropertiesId_SolarSystemFlagShipProductionCostMulti = 8,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemFlagShipDockingAdd", Value=9)]
        GuildPropertiesId_SolarSystemFlagShipDockingAdd = 9,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemProductionLineAdd", Value=10)]
        GuildPropertiesId_SolarSystemProductionLineAdd = 10,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemPlayerDelegateMissionRewordMulti", Value=11)]
        GuildPropertiesId_SolarSystemPlayerDelegateMissionRewordMulti = 11,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemSentryTargetTrackingDistanceMulti", Value=12)]
        GuildPropertiesId_SolarSystemSentryTargetTrackingDistanceMulti = 12,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemSentryTrackingSceneQuantityAdd", Value=13)]
        GuildPropertiesId_SolarSystemSentryTrackingSceneQuantityAdd = 13,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemTeleportTunnelEffectRange", Value=14)]
        GuildPropertiesId_SolarSystemTeleportTunnelEffectRange = 14,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemAntiTeleportEffectTimeLength", Value=15)]
        GuildPropertiesId_SolarSystemAntiTeleportEffectTimeLength = 15,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemAntiTeleportEffectCDTime", Value=0x10)]
        GuildPropertiesId_SolarSystemAntiTeleportEffectCDTime = 0x10,
        [ProtoEnum(Name="GuildPropertiesId_SolarSystemTradeTransportShipLevel", Value=0x11)]
        GuildPropertiesId_SolarSystemTradeTransportShipLevel = 0x11,
        [ProtoEnum(Name="GuildPropertiesId_Max", Value=0x12)]
        GuildPropertiesId_Max = 0x12
    }
}

