﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBattlePassChallangeQuestAccRewardInfo")]
    public class ConfigDataBattlePassChallangeQuestAccRewardInfo : IExtensible
    {
        private int _ID;
        private int _CompleteChallangeQuestCount;
        private int _RewardItemId;
        private int _UpgradedRewardItemId;
        private int _TemplateId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_CompleteChallangeQuestCount;
        private static DelegateBridge __Hotfix_set_CompleteChallangeQuestCount;
        private static DelegateBridge __Hotfix_get_RewardItemId;
        private static DelegateBridge __Hotfix_set_RewardItemId;
        private static DelegateBridge __Hotfix_get_UpgradedRewardItemId;
        private static DelegateBridge __Hotfix_set_UpgradedRewardItemId;
        private static DelegateBridge __Hotfix_get_TemplateId;
        private static DelegateBridge __Hotfix_set_TemplateId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CompleteChallangeQuestCount", DataFormat=DataFormat.TwosComplement)]
        public int CompleteChallangeQuestCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="RewardItemId", DataFormat=DataFormat.TwosComplement)]
        public int RewardItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="UpgradedRewardItemId", DataFormat=DataFormat.TwosComplement)]
        public int UpgradedRewardItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="TemplateId", DataFormat=DataFormat.TwosComplement)]
        public int TemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

