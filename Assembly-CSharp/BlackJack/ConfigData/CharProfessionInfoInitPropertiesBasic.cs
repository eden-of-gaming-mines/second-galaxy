﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharProfessionInfoInitPropertiesBasic")]
    public class CharProfessionInfoInitPropertiesBasic : IExtensible
    {
        private PropertyCategory _ProtertyId;
        private int _Point;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProtertyId;
        private static DelegateBridge __Hotfix_set_ProtertyId;
        private static DelegateBridge __Hotfix_get_Point;
        private static DelegateBridge __Hotfix_set_Point;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ProtertyId", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory ProtertyId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Point", DataFormat=DataFormat.TwosComplement)]
        public int Point
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

