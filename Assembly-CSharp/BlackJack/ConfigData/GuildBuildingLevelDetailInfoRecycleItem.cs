﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBuildingLevelDetailInfoRecycleItem")]
    public class GuildBuildingLevelDetailInfoRecycleItem : IExtensible
    {
        private int _itemId;
        private int _itemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_itemId;
        private static DelegateBridge __Hotfix_set_itemId;
        private static DelegateBridge __Hotfix_get_itemCount;
        private static DelegateBridge __Hotfix_set_itemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="itemId", DataFormat=DataFormat.TwosComplement)]
        public int itemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="itemCount", DataFormat=DataFormat.TwosComplement)]
        public int itemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

