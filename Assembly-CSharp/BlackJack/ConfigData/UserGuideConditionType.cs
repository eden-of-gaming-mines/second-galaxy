﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="UserGuideConditionType")]
    public enum UserGuideConditionType
    {
        [ProtoEnum(Name="UserGuideConditionType_LevelGreater", Value=1)]
        UserGuideConditionType_LevelGreater = 1,
        [ProtoEnum(Name="UserGuideConditionType_LevelLess", Value=2)]
        UserGuideConditionType_LevelLess = 2,
        [ProtoEnum(Name="UserGuideConditionType_QuestFinished", Value=3)]
        UserGuideConditionType_QuestFinished = 3,
        [ProtoEnum(Name="UserGuideConditionType_QuestNotFinished", Value=4)]
        UserGuideConditionType_QuestNotFinished = 4,
        [ProtoEnum(Name="UserGuideConditionType_GuideStepFinished", Value=5)]
        UserGuideConditionType_GuideStepFinished = 5,
        [ProtoEnum(Name="UserGuideConditionType_WithMotherShip", Value=6)]
        UserGuideConditionType_WithMotherShip = 6,
        [ProtoEnum(Name="UserGuideConditionType_TechFinished", Value=7)]
        UserGuideConditionType_TechFinished = 7,
        [ProtoEnum(Name="UserGuideConditionType_TechNotFinished", Value=8)]
        UserGuideConditionType_TechNotFinished = 8,
        [ProtoEnum(Name="UserGuideConditionType_LicenseFinished", Value=9)]
        UserGuideConditionType_LicenseFinished = 9,
        [ProtoEnum(Name="UserGuideConditionType_LicenseNotFinished", Value=10)]
        UserGuideConditionType_LicenseNotFinished = 10,
        [ProtoEnum(Name="UserGuideConditionType_SkillFinished", Value=11)]
        UserGuideConditionType_SkillFinished = 11,
        [ProtoEnum(Name="UserGuideConditionType_SkillNotFinished", Value=12)]
        UserGuideConditionType_SkillNotFinished = 12,
        [ProtoEnum(Name="UserGuideConditionType_StoreHasItem", Value=13)]
        UserGuideConditionType_StoreHasItem = 13,
        [ProtoEnum(Name="UserGuideConditionType_SceneId", Value=14)]
        UserGuideConditionType_SceneId = 14,
        [ProtoEnum(Name="UserGuideConditionType_AcceptQuest", Value=15)]
        UserGuideConditionType_AcceptQuest = 15,
        [ProtoEnum(Name="UserGuideConditionType_NpcChatDialogEnd", Value=0x10)]
        UserGuideConditionType_NpcChatDialogEnd = 0x10,
        [ProtoEnum(Name="UserGuideConditionType_TextNotice", Value=0x11)]
        UserGuideConditionType_TextNotice = 0x11,
        [ProtoEnum(Name="UserGuideConditionType_QuestUnaccepted", Value=0x12)]
        UserGuideConditionType_QuestUnaccepted = 0x12,
        [ProtoEnum(Name="UserGuideConditionType_LicenseUpgradeState", Value=0x13)]
        UserGuideConditionType_LicenseUpgradeState = 0x13,
        [ProtoEnum(Name="UserGuideConditionType_InSpace", Value=20)]
        UserGuideConditionType_InSpace = 20,
        [ProtoEnum(Name="UserGuideConditionType_IsQuestListUITaskStop", Value=0x15)]
        UserGuideConditionType_IsQuestListUITaskStop = 0x15,
        [ProtoEnum(Name="UserGuideConditionType_IsSolarSytemUIAvailable", Value=0x16)]
        UserGuideConditionType_IsSolarSytemUIAvailable = 0x16,
        [ProtoEnum(Name="UserGuideConditionType_IsQuestWaitForComplete", Value=0x17)]
        UserGuideConditionType_IsQuestWaitForComplete = 0x17,
        [ProtoEnum(Name="UserGuideConditionType_IsQuestAcceptOrComplete", Value=0x18)]
        UserGuideConditionType_IsQuestAcceptOrComplete = 0x18,
        [ProtoEnum(Name="UserGuideConditionType_FunctionStateOpened", Value=0x19)]
        UserGuideConditionType_FunctionStateOpened = 0x19,
        [ProtoEnum(Name="UserGuideConditionType_EqualFunctionState", Value=0x1a)]
        UserGuideConditionType_EqualFunctionState = 0x1a,
        [ProtoEnum(Name="UserGuideConditionType_QuestFixedFinished", Value=0x1b)]
        UserGuideConditionType_QuestFixedFinished = 0x1b,
        [ProtoEnum(Name="UserGuideConditionType_StoreHasAnyItem", Value=0x1c)]
        UserGuideConditionType_StoreHasAnyItem = 0x1c,
        [ProtoEnum(Name="UserGuideConditionType_IsSpaceStationUITaskShowEnd", Value=0x1d)]
        UserGuideConditionType_IsSpaceStationUITaskShowEnd = 0x1d,
        [ProtoEnum(Name="UserGuideConditionType_IsQuestWaitForCompleteOrComplete", Value=30)]
        UserGuideConditionType_IsQuestWaitForCompleteOrComplete = 30
    }
}

