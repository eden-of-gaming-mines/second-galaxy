﻿namespace BlackJack.ConfigData
{
    using BlackJack.ToolUtil;
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataUserGuideGroupInfo")]
    public class ConfigDataUserGuideGroupInfo : IExtensible, IUserGuideGroupInfo
    {
        private int _ID;
        private bool _CanSkip;
        private bool _SkipWhenStepQuit;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_CanSkip;
        private static DelegateBridge __Hotfix_set_CanSkip;
        private static DelegateBridge __Hotfix_get_SkipWhenStepQuit;
        private static DelegateBridge __Hotfix_set_SkipWhenStepQuit;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideGroupInfo.CanSkip;

        [MethodImpl(0x8000)]
        bool IUserGuideGroupInfo.CanSkip()
        {
        }

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CanSkip", DataFormat=DataFormat.Default)]
        public bool CanSkip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SkipWhenStepQuit", DataFormat=DataFormat.Default)]
        public bool SkipWhenStepQuit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

