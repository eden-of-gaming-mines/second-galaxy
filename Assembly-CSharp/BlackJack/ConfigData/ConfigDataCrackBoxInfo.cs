﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCrackBoxInfo")]
    public class ConfigDataCrackBoxInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.CrackBoxType _CrackBoxType;
        private float _PackedSize;
        private bool _IsBindOnPick;
        private float _SalePrice;
        private RankType _Rank;
        private SubRankType _SubRank;
        private ShipSizeType _SizeType;
        private int _DropId;
        private int _CrackTimeCost;
        private int _capacity;
        private string _IconResPath;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private string _ArtResource;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_CrackBoxType;
        private static DelegateBridge __Hotfix_set_CrackBoxType;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_SizeType;
        private static DelegateBridge __Hotfix_set_SizeType;
        private static DelegateBridge __Hotfix_get_DropId;
        private static DelegateBridge __Hotfix_set_DropId;
        private static DelegateBridge __Hotfix_get_CrackTimeCost;
        private static DelegateBridge __Hotfix_set_CrackTimeCost;
        private static DelegateBridge __Hotfix_get_capacity;
        private static DelegateBridge __Hotfix_set_capacity;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_ArtResource;
        private static DelegateBridge __Hotfix_set_ArtResource;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CrackBoxType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.CrackBoxType CrackBoxType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.FixedSize)]
        public float PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="SizeType", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType SizeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DropId", DataFormat=DataFormat.TwosComplement)]
        public int DropId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="CrackTimeCost", DataFormat=DataFormat.TwosComplement)]
        public int CrackTimeCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="capacity", DataFormat=DataFormat.TwosComplement)]
        public int capacity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="ArtResource", DataFormat=DataFormat.Default)]
        public string ArtResource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

