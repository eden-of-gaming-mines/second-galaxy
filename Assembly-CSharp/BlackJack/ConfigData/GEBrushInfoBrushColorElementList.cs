﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GEBrushInfoBrushColorElementList")]
    public class GEBrushInfoBrushColorElementList : IExtensible
    {
        private float _BrushValue;
        private string _BrushColorStr;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BrushValue;
        private static DelegateBridge __Hotfix_set_BrushValue;
        private static DelegateBridge __Hotfix_get_BrushColorStr;
        private static DelegateBridge __Hotfix_set_BrushColorStr;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BrushValue", DataFormat=DataFormat.FixedSize)]
        public float BrushValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BrushColorStr", DataFormat=DataFormat.Default)]
        public string BrushColorStr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

