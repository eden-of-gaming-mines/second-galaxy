﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPassiveSkillInfo")]
    public class ConfigDataPassiveSkillInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _LevelInfoIdList;
        private int _Flag;
        private int _SkillType;
        private string _IconArtResString;
        private int _SkillModifyType;
        private readonly List<int> _DrivingLicenseQuestIdList;
        private int _Grade;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _GenericNameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LevelInfoIdList;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_get_SkillType;
        private static DelegateBridge __Hotfix_set_SkillType;
        private static DelegateBridge __Hotfix_get_IconArtResString;
        private static DelegateBridge __Hotfix_set_IconArtResString;
        private static DelegateBridge __Hotfix_get_SkillModifyType;
        private static DelegateBridge __Hotfix_set_SkillModifyType;
        private static DelegateBridge __Hotfix_get_DrivingLicenseQuestIdList;
        private static DelegateBridge __Hotfix_get_Grade;
        private static DelegateBridge __Hotfix_set_Grade;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_GenericNameStrKey;
        private static DelegateBridge __Hotfix_set_GenericNameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="LevelInfoIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> LevelInfoIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public int Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SkillType", DataFormat=DataFormat.TwosComplement)]
        public int SkillType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IconArtResString", DataFormat=DataFormat.Default)]
        public string IconArtResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="SkillModifyType", DataFormat=DataFormat.TwosComplement)]
        public int SkillModifyType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="DrivingLicenseQuestIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DrivingLicenseQuestIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Grade", DataFormat=DataFormat.TwosComplement)]
        public int Grade
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="GenericNameStrKey", DataFormat=DataFormat.Default)]
        public string GenericNameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

