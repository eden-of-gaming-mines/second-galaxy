﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataTechLevelInfo")]
    public class ConfigDataTechLevelInfo : IExtensible
    {
        private int _ID;
        private uint _Level;
        private readonly List<ConfIdLevelInfo> _DependTechList;
        private int _PlayerLevelRequire;
        private readonly List<CostInfo> _CostList;
        private int _TimeCost;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private int _BufId4Captain;
        private int _CharacterScore;
        private readonly List<UnlockItemInfo> _UnlockItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_DependTechList;
        private static DelegateBridge __Hotfix_get_PlayerLevelRequire;
        private static DelegateBridge __Hotfix_set_PlayerLevelRequire;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_get_TimeCost;
        private static DelegateBridge __Hotfix_set_TimeCost;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_BufId4Captain;
        private static DelegateBridge __Hotfix_set_BufId4Captain;
        private static DelegateBridge __Hotfix_get_CharacterScore;
        private static DelegateBridge __Hotfix_set_CharacterScore;
        private static DelegateBridge __Hotfix_get_UnlockItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public uint Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DependTechList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PlayerLevelRequire", DataFormat=DataFormat.TwosComplement)]
        public int PlayerLevelRequire
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="CostList", DataFormat=DataFormat.Default)]
        public List<CostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="TimeCost", DataFormat=DataFormat.TwosComplement)]
        public int TimeCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BufId4Captain", DataFormat=DataFormat.TwosComplement)]
        public int BufId4Captain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CharacterScore", DataFormat=DataFormat.TwosComplement)]
        public int CharacterScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="UnlockItemList", DataFormat=DataFormat.Default)]
        public List<UnlockItemInfo> UnlockItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

