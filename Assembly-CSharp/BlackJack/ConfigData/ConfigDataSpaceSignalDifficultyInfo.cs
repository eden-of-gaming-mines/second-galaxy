﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSpaceSignalDifficultyInfo")]
    public class ConfigDataSpaceSignalDifficultyInfo : IExtensible
    {
        private int _ID;
        private int _PropertyValueNeed;
        private int _ExtraExp;
        private int _RecommendedGravityEquip;
        private int _RecommendedElectronicsEquip;
        private int _RecommendedRadiationEquip;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_PropertyValueNeed;
        private static DelegateBridge __Hotfix_set_PropertyValueNeed;
        private static DelegateBridge __Hotfix_get_ExtraExp;
        private static DelegateBridge __Hotfix_set_ExtraExp;
        private static DelegateBridge __Hotfix_get_RecommendedGravityEquip;
        private static DelegateBridge __Hotfix_set_RecommendedGravityEquip;
        private static DelegateBridge __Hotfix_get_RecommendedElectronicsEquip;
        private static DelegateBridge __Hotfix_set_RecommendedElectronicsEquip;
        private static DelegateBridge __Hotfix_get_RecommendedRadiationEquip;
        private static DelegateBridge __Hotfix_set_RecommendedRadiationEquip;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PropertyValueNeed", DataFormat=DataFormat.TwosComplement)]
        public int PropertyValueNeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ExtraExp", DataFormat=DataFormat.TwosComplement)]
        public int ExtraExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="RecommendedGravityEquip", DataFormat=DataFormat.TwosComplement)]
        public int RecommendedGravityEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="RecommendedElectronicsEquip", DataFormat=DataFormat.TwosComplement)]
        public int RecommendedElectronicsEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="RecommendedRadiationEquip", DataFormat=DataFormat.TwosComplement)]
        public int RecommendedRadiationEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

