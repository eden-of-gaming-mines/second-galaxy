﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BlackMarketShopPayType")]
    public enum BlackMarketShopPayType
    {
        [ProtoEnum(Name="BlackMarketShopPayType_PlayerTradeMoney", Value=1)]
        BlackMarketShopPayType_PlayerTradeMoney = 1,
        [ProtoEnum(Name="BlackMarketShopPayType_PlayerRealMoney", Value=2)]
        BlackMarketShopPayType_PlayerRealMoney = 2,
        [ProtoEnum(Name="BlackMarketShopPayType_GuildTradeMoney", Value=3)]
        BlackMarketShopPayType_GuildTradeMoney = 3,
        [ProtoEnum(Name="BlackMarketShopPayType_Max", Value=4)]
        BlackMarketShopPayType_Max = 4
    }
}

