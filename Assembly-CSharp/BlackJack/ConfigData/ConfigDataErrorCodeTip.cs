﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataErrorCodeTip")]
    public class ConfigDataErrorCodeTip : IExtensible
    {
        private int _ID;
        private bool _IsDSUnSync;
        private string _InternalError;
        private string _ValueStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_IsDSUnSync;
        private static DelegateBridge __Hotfix_set_IsDSUnSync;
        private static DelegateBridge __Hotfix_get_InternalError;
        private static DelegateBridge __Hotfix_set_InternalError;
        private static DelegateBridge __Hotfix_get_ValueStrKey;
        private static DelegateBridge __Hotfix_set_ValueStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="IsDSUnSync", DataFormat=DataFormat.Default)]
        public bool IsDSUnSync
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="InternalError", DataFormat=DataFormat.Default)]
        public string InternalError
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ValueStrKey", DataFormat=DataFormat.Default)]
        public string ValueStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

