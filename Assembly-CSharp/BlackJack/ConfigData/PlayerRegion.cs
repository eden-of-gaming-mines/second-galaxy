﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="PlayerRegion")]
    public enum PlayerRegion
    {
        [ProtoEnum(Name="PlayerRegion_None", Value=0)]
        PlayerRegion_None = 0,
        [ProtoEnum(Name="PlayerRegion_China", Value=1)]
        PlayerRegion_China = 1,
        [ProtoEnum(Name="PlayerRegion_EU", Value=2)]
        PlayerRegion_EU = 2,
        [ProtoEnum(Name="PlayerRegion_US", Value=3)]
        PlayerRegion_US = 3,
        [ProtoEnum(Name="PlayerRegion_JP", Value=4)]
        PlayerRegion_JP = 4,
        [ProtoEnum(Name="PlayerRegion_CR", Value=5)]
        PlayerRegion_CR = 5,
        [ProtoEnum(Name="PlayerRegion_ALB", Value=6)]
        PlayerRegion_ALB = 6,
        [ProtoEnum(Name="PlayerRegion_RUS", Value=7)]
        PlayerRegion_RUS = 7,
        [ProtoEnum(Name="PlayerRegion_Max", Value=8)]
        PlayerRegion_Max = 8
    }
}

