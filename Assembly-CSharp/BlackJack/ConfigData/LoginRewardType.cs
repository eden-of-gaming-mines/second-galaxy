﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="LoginRewardType")]
    public enum LoginRewardType
    {
        [ProtoEnum(Name="LoginRewardType_Item", Value=1)]
        LoginRewardType_Item = 1,
        [ProtoEnum(Name="LoginRewardType_Captain", Value=2)]
        LoginRewardType_Captain = 2
    }
}

