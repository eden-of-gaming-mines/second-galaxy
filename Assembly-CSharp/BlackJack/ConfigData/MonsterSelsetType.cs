﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MonsterSelsetType")]
    public enum MonsterSelsetType
    {
        [ProtoEnum(Name="MonsterSelsetType_MonsterTeamId", Value=0)]
        MonsterSelsetType_MonsterTeamId = 0,
        [ProtoEnum(Name="MonsterSelsetType_MonsterTeamPoolId", Value=1)]
        MonsterSelsetType_MonsterTeamPoolId = 1,
        [ProtoEnum(Name="MonsterSelsetType_EnvMonsterTeamIndex", Value=2)]
        MonsterSelsetType_EnvMonsterTeamIndex = 2,
        [ProtoEnum(Name="MonsterSelsetType_MonsterId", Value=3)]
        MonsterSelsetType_MonsterId = 3
    }
}

