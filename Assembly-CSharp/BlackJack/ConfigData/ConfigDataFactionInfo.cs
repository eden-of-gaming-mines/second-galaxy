﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFactionInfo")]
    public class ConfigDataFactionInfo : IExtensible
    {
        private int _ID;
        private int _ParentFaction;
        private int _CreditNpcShopItemListId;
        private int _CreditRelativeModifyID;
        private int _FactionCredit2PlayerDefault;
        private bool _FactionCreditStatic;
        private string _Color;
        private string _IconResPath;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ParentFaction;
        private static DelegateBridge __Hotfix_set_ParentFaction;
        private static DelegateBridge __Hotfix_get_CreditNpcShopItemListId;
        private static DelegateBridge __Hotfix_set_CreditNpcShopItemListId;
        private static DelegateBridge __Hotfix_get_CreditRelativeModifyID;
        private static DelegateBridge __Hotfix_set_CreditRelativeModifyID;
        private static DelegateBridge __Hotfix_get_FactionCredit2PlayerDefault;
        private static DelegateBridge __Hotfix_set_FactionCredit2PlayerDefault;
        private static DelegateBridge __Hotfix_get_FactionCreditStatic;
        private static DelegateBridge __Hotfix_set_FactionCreditStatic;
        private static DelegateBridge __Hotfix_get_Color;
        private static DelegateBridge __Hotfix_set_Color;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ParentFaction", DataFormat=DataFormat.TwosComplement)]
        public int ParentFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="CreditNpcShopItemListId", DataFormat=DataFormat.TwosComplement)]
        public int CreditNpcShopItemListId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="CreditRelativeModifyID", DataFormat=DataFormat.TwosComplement)]
        public int CreditRelativeModifyID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="FactionCredit2PlayerDefault", DataFormat=DataFormat.TwosComplement)]
        public int FactionCredit2PlayerDefault
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="FactionCreditStatic", DataFormat=DataFormat.Default)]
        public bool FactionCreditStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Color", DataFormat=DataFormat.Default)]
        public string Color
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

