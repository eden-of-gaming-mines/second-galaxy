﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcCaptainFeatsType")]
    public enum NpcCaptainFeatsType
    {
        [ProtoEnum(Name="NpcCaptainFeatsType_PassiveSkill", Value=1)]
        NpcCaptainFeatsType_PassiveSkill = 1,
        [ProtoEnum(Name="NpcCaptainFeatsType_Ship", Value=2)]
        NpcCaptainFeatsType_Ship = 2,
        [ProtoEnum(Name="NpcCaptainFeatsType_BuffForPlayer", Value=3)]
        NpcCaptainFeatsType_BuffForPlayer = 3
    }
}

