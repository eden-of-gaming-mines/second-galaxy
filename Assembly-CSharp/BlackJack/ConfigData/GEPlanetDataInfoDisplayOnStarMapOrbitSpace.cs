﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GEPlanetDataInfoDisplayOnStarMapOrbitSpace")]
    public class GEPlanetDataInfoDisplayOnStarMapOrbitSpace : IExtensible
    {
        private float _MinOrbitSpace;
        private float _MaxOrbitSpace;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MinOrbitSpace;
        private static DelegateBridge __Hotfix_set_MinOrbitSpace;
        private static DelegateBridge __Hotfix_get_MaxOrbitSpace;
        private static DelegateBridge __Hotfix_set_MaxOrbitSpace;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MinOrbitSpace", DataFormat=DataFormat.FixedSize)]
        public float MinOrbitSpace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="MaxOrbitSpace", DataFormat=DataFormat.FixedSize)]
        public float MaxOrbitSpace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

