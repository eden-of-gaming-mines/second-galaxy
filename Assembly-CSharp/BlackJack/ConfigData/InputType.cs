﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="InputType")]
    public enum InputType
    {
        [ProtoEnum(Name="InputType_CharacterName", Value=1)]
        InputType_CharacterName = 1,
        [ProtoEnum(Name="InputType_ShipTempleteName", Value=2)]
        InputType_ShipTempleteName = 2,
        [ProtoEnum(Name="InputType_ShipName", Value=3)]
        InputType_ShipName = 3,
        [ProtoEnum(Name="InputType_GuildFleetName", Value=4)]
        InputType_GuildFleetName = 4,
        [ProtoEnum(Name="InputType_GuildName", Value=5)]
        InputType_GuildName = 5,
        [ProtoEnum(Name="InputType_GuildCode", Value=6)]
        InputType_GuildCode = 6,
        [ProtoEnum(Name="InputType_AllianceName", Value=7)]
        InputType_AllianceName = 7,
        [ProtoEnum(Name="InputType_Invalid", Value=8)]
        InputType_Invalid = 8
    }
}

