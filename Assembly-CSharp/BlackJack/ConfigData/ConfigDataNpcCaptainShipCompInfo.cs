﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainShipCompInfo")]
    public class ConfigDataNpcCaptainShipCompInfo : IExtensible
    {
        private int _ID;
        private readonly List<ShipCompListConfInfo> _ShipCompList4Unlock;
        private readonly List<ShipCompListConfInfo> _ShipCompList4Repaire;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShipCompList4Unlock;
        private static DelegateBridge __Hotfix_get_ShipCompList4Repaire;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ShipCompList4Unlock", DataFormat=DataFormat.Default)]
        public List<ShipCompListConfInfo> ShipCompList4Unlock
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="ShipCompList4Repaire", DataFormat=DataFormat.Default)]
        public List<ShipCompListConfInfo> ShipCompList4Repaire
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

