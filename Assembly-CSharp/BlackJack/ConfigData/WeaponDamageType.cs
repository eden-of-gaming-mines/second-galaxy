﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="WeaponDamageType")]
    public enum WeaponDamageType
    {
        [ProtoEnum(Name="WeaponDamageType_Heat", Value=1)]
        WeaponDamageType_Heat = 1,
        [ProtoEnum(Name="WeaponDamageType_Kinetic", Value=2)]
        WeaponDamageType_Kinetic = 2,
        [ProtoEnum(Name="WeaponDamageType_Electric", Value=3)]
        WeaponDamageType_Electric = 3
    }
}

