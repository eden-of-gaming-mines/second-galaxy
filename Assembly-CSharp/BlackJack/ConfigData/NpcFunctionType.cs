﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcFunctionType")]
    public enum NpcFunctionType
    {
        [ProtoEnum(Name="NpcFunctionType_Custom", Value=1)]
        NpcFunctionType_Custom = 1,
        [ProtoEnum(Name="NpcFunctionType_FactionShop", Value=2)]
        NpcFunctionType_FactionShop = 2,
        [ProtoEnum(Name="NpcFunctionType_SystemShop", Value=3)]
        NpcFunctionType_SystemShop = 3,
        [ProtoEnum(Name="NpcFunctionType_Auction", Value=4)]
        NpcFunctionType_Auction = 4,
        [ProtoEnum(Name="NpcFunctionType_GuildBaseSolarSystemSet", Value=5)]
        NpcFunctionType_GuildBaseSolarSystemSet = 5,
        [ProtoEnum(Name="NpcFunctionType_BlackMarket", Value=6)]
        NpcFunctionType_BlackMarket = 6,
        [ProtoEnum(Name="NpcFunctionType_PanGalaNpcShop", Value=7)]
        NpcFunctionType_PanGalaNpcShop = 7
    }
}

