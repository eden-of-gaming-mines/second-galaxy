﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestType")]
    public enum QuestType
    {
        [ProtoEnum(Name="QuestType_None", Value=0)]
        QuestType_None = 0,
        [ProtoEnum(Name="QuestType_Story", Value=1)]
        QuestType_Story = 1,
        [ProtoEnum(Name="QuestType_Faction", Value=2)]
        QuestType_Faction = 2,
        [ProtoEnum(Name="QuestType_Captain", Value=3)]
        QuestType_Captain = 3,
        [ProtoEnum(Name="QuestType_Activity", Value=4)]
        QuestType_Activity = 4,
        [ProtoEnum(Name="QuestType_FreeSignal_Engineer", Value=5)]
        QuestType_FreeSignal_Engineer = 5,
        [ProtoEnum(Name="QuestType_FreeSignal_Scientist", Value=6)]
        QuestType_FreeSignal_Scientist = 6,
        [ProtoEnum(Name="QuestType_FreeSignal_Soldier", Value=7)]
        QuestType_FreeSignal_Soldier = 7,
        [ProtoEnum(Name="QuestType_FreeSignal_Explorer", Value=8)]
        QuestType_FreeSignal_Explorer = 8,
        [ProtoEnum(Name="QuestType_ChapterGoal", Value=9)]
        QuestType_ChapterGoal = 9,
        [ProtoEnum(Name="QuestType_DrivingLicense", Value=10)]
        QuestType_DrivingLicense = 10,
        [ProtoEnum(Name="QuestType_ChapterSubItem", Value=11)]
        QuestType_ChapterSubItem = 11,
        [ProtoEnum(Name="QuestType_ScienceExplore_PuzzleGame", Value=12)]
        QuestType_ScienceExplore_PuzzleGame = 12,
        [ProtoEnum(Name="QuestType_ScienceExplore", Value=13)]
        QuestType_ScienceExplore = 13,
        [ProtoEnum(Name="QuestType_CelestialScan_SolarSystemScan", Value=14)]
        QuestType_CelestialScan_SolarSystemScan = 14,
        [ProtoEnum(Name="QuestType_CelestialScan_StarScan", Value=15)]
        QuestType_CelestialScan_StarScan = 15,
        [ProtoEnum(Name="QuestType_CelestialScan_PlanetScan", Value=0x10)]
        QuestType_CelestialScan_PlanetScan = 0x10,
        [ProtoEnum(Name="QuestType_BranchStory", Value=0x11)]
        QuestType_BranchStory = 0x11,
        [ProtoEnum(Name="QuestType_BattlePass", Value=0x12)]
        QuestType_BattlePass = 0x12,
        [ProtoEnum(Name="QuestType_RechargeTrigger", Value=0x13)]
        QuestType_RechargeTrigger = 0x13
    }
}

