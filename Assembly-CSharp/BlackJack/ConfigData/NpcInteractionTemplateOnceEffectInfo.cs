﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NpcInteractionTemplateOnceEffectInfo")]
    public class NpcInteractionTemplateOnceEffectInfo : IExtensible
    {
        private NpcInteractionOnceEffectType _effectType;
        private float _effectParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_effectType;
        private static DelegateBridge __Hotfix_set_effectType;
        private static DelegateBridge __Hotfix_get_effectParam;
        private static DelegateBridge __Hotfix_set_effectParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="effectType", DataFormat=DataFormat.TwosComplement)]
        public NpcInteractionOnceEffectType effectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="effectParam", DataFormat=DataFormat.FixedSize)]
        public float effectParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

