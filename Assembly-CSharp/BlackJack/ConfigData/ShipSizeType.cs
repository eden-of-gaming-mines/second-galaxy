﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipSizeType")]
    public enum ShipSizeType
    {
        [ProtoEnum(Name="ShipSizeType_Invalid", Value=0)]
        ShipSizeType_Invalid = 0,
        [ProtoEnum(Name="ShipSizeType_N", Value=1)]
        ShipSizeType_N = 1,
        [ProtoEnum(Name="ShipSizeType_S", Value=2)]
        ShipSizeType_S = 2,
        [ProtoEnum(Name="ShipSizeType_M", Value=3)]
        ShipSizeType_M = 3,
        [ProtoEnum(Name="ShipSizeType_L", Value=4)]
        ShipSizeType_L = 4,
        [ProtoEnum(Name="ShipSizeType_XL", Value=5)]
        ShipSizeType_XL = 5
    }
}

