﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataLogicalSoundResTable")]
    public class ConfigDataLogicalSoundResTable : IExtensible
    {
        private int _ID;
        private LogicalSoundResTableID _AuidoId;
        private string _CueName;
        private bool _IsAudioLoop;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AuidoId;
        private static DelegateBridge __Hotfix_set_AuidoId;
        private static DelegateBridge __Hotfix_get_CueName;
        private static DelegateBridge __Hotfix_set_CueName;
        private static DelegateBridge __Hotfix_get_IsAudioLoop;
        private static DelegateBridge __Hotfix_set_IsAudioLoop;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AuidoId", DataFormat=DataFormat.TwosComplement)]
        public LogicalSoundResTableID AuidoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CueName", DataFormat=DataFormat.Default)]
        public string CueName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="IsAudioLoop", DataFormat=DataFormat.Default)]
        public bool IsAudioLoop
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

