﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildCurrencyLogType")]
    public enum GuildCurrencyLogType
    {
        [ProtoEnum(Name="GuildCurrencyLogType_Invalid", Value=0)]
        GuildCurrencyLogType_Invalid = 0,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildActionIncome", Value=1)]
        GuildCurrencyLogType_TradeMoneyGuildActionIncome = 1,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyDonateIncome", Value=2)]
        GuildCurrencyLogType_TradeMoneyDonateIncome = 2,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildGiftBagIncome", Value=3)]
        GuildCurrencyLogType_TradeMoneyGuildGiftBagIncome = 3,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyPurchaseOutcome", Value=4)]
        GuildCurrencyLogType_TradeMoneyPurchaseOutcome = 4,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildTradeOutcome", Value=5)]
        GuildCurrencyLogType_TradeMoneyGuildTradeOutcome = 5,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildProduceSpeedUpOutcome", Value=6)]
        GuildCurrencyLogType_TradeMoneyGuildProduceSpeedUpOutcome = 6,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyTacticalPointOutcome", Value=7)]
        GuildCurrencyLogType_TradeMoneyTacticalPointOutcome = 7,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildRedeployOutcome", Value=8)]
        GuildCurrencyLogType_TradeMoneyGuildRedeployOutcome = 8,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyOccupyAintenanceOutcome", Value=9)]
        GuildCurrencyLogType_TradeMoneyOccupyAintenanceOutcome = 9,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyWarOnSovereignOutcome", Value=10)]
        GuildCurrencyLogType_TradeMoneyWarOnSovereignOutcome = 10,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyProduceOutcome", Value=11)]
        GuildCurrencyLogType_TradeMoneyProduceOutcome = 11,
        [ProtoEnum(Name="GuildCurrencyLogType_TacticalPointGuildActionIncome", Value=12)]
        GuildCurrencyLogType_TacticalPointGuildActionIncome = 12,
        [ProtoEnum(Name="GuildCurrencyLogType_TacticalPointDonateIncome", Value=13)]
        GuildCurrencyLogType_TacticalPointDonateIncome = 13,
        [ProtoEnum(Name="GuildCurrencyLogType_TacticalPointLearnOutcome", Value=14)]
        GuildCurrencyLogType_TacticalPointLearnOutcome = 14,
        [ProtoEnum(Name="GuildCurrencyLogType_InformationPointOccupyIncome", Value=15)]
        GuildCurrencyLogType_InformationPointOccupyIncome = 15,
        [ProtoEnum(Name="GuildCurrencyLogType_InformationPointDonateIncome", Value=0x10)]
        GuildCurrencyLogType_InformationPointDonateIncome = 0x10,
        [ProtoEnum(Name="GuildCurrencyLogType_InformationPointGuildActionOutcome", Value=0x11)]
        GuildCurrencyLogType_InformationPointGuildActionOutcome = 0x11,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyAllianceCreateOutcome", Value=0x12)]
        GuildCurrencyLogType_TradeMoneyAllianceCreateOutcome = 0x12,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyAllianceNameChangeOutcome", Value=0x13)]
        GuildCurrencyLogType_TradeMoneyAllianceNameChangeOutcome = 0x13,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyAllianceLogoChangeOutcome", Value=20)]
        GuildCurrencyLogType_TradeMoneyAllianceLogoChangeOutcome = 20,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildCompensationOutcome", Value=0x15)]
        GuildCurrencyLogType_TradeMoneyGuildCompensationOutcome = 0x15,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildTradePurchaseOrderEditIncome", Value=0x16)]
        GuildCurrencyLogType_TradeMoneyGuildTradePurchaseOrderEditIncome = 0x16,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildTradePurchaseOrderEditOutcome", Value=0x17)]
        GuildCurrencyLogType_TradeMoneyGuildTradePurchaseOrderEditOutcome = 0x17,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildTradePurchaseTransportFailIncome", Value=0x18)]
        GuildCurrencyLogType_TradeMoneyGuildTradePurchaseTransportFailIncome = 0x18,
        [ProtoEnum(Name="GuildCurrencyLogType_TradeMoneyGuildTradeTransportSucceedIncome", Value=0x19)]
        GuildCurrencyLogType_TradeMoneyGuildTradeTransportSucceedIncome = 0x19,
        [ProtoEnum(Name="GuildCurrencyLogType_Max", Value=0x1a)]
        GuildCurrencyLogType_Max = 0x1a
    }
}

