﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBlackMarketShopItemInfo")]
    public class ConfigDataBlackMarketShopItemInfo : IExtensible
    {
        private int _ID;
        private BlackMarketShopItemType _ItemType;
        private BlackMarketShopPayType _PayType;
        private int _Price;
        private int _DefaultOrderCount;
        private ResourceShopItemCategory _Category;
        private int _MineralConfigID;
        private int _NormalItemID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_PayType;
        private static DelegateBridge __Hotfix_set_PayType;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_DefaultOrderCount;
        private static DelegateBridge __Hotfix_set_DefaultOrderCount;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_MineralConfigID;
        private static DelegateBridge __Hotfix_set_MineralConfigID;
        private static DelegateBridge __Hotfix_get_NormalItemID;
        private static DelegateBridge __Hotfix_set_NormalItemID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public BlackMarketShopItemType ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PayType", DataFormat=DataFormat.TwosComplement)]
        public BlackMarketShopPayType PayType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public int Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="DefaultOrderCount", DataFormat=DataFormat.TwosComplement)]
        public int DefaultOrderCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public ResourceShopItemCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="MineralConfigID", DataFormat=DataFormat.TwosComplement)]
        public int MineralConfigID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="NormalItemID", DataFormat=DataFormat.TwosComplement)]
        public int NormalItemID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

