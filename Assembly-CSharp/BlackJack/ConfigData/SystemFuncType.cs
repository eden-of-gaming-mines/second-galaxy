﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SystemFuncType")]
    public enum SystemFuncType
    {
        [ProtoEnum(Name="SystemFuncType_None", Value=0)]
        SystemFuncType_None = 0,
        [ProtoEnum(Name="SystemFuncType_Tech", Value=1)]
        SystemFuncType_Tech = 1,
        [ProtoEnum(Name="SystemFuncType_CrewManager", Value=2)]
        SystemFuncType_CrewManager = 2,
        [ProtoEnum(Name="SystemFuncType_WingShip", Value=3)]
        SystemFuncType_WingShip = 3,
        [ProtoEnum(Name="SystemFuncType_ItemStore", Value=4)]
        SystemFuncType_ItemStore = 4,
        [ProtoEnum(Name="SystemFuncType_Crack", Value=5)]
        SystemFuncType_Crack = 5,
        [ProtoEnum(Name="SystemFuncType_Produce", Value=6)]
        SystemFuncType_Produce = 6,
        [ProtoEnum(Name="SystemFuncType_DelegateMisson", Value=7)]
        SystemFuncType_DelegateMisson = 7,
        [ProtoEnum(Name="SystemFuncType_DelegateFight", Value=8)]
        SystemFuncType_DelegateFight = 8,
        [ProtoEnum(Name="SystemFuncType_DelegateMineral", Value=9)]
        SystemFuncType_DelegateMineral = 9,
        [ProtoEnum(Name="SystemFuncType_PVESignal", Value=10)]
        SystemFuncType_PVESignal = 10,
        [ProtoEnum(Name="SystemFuncType_PVPSignal", Value=11)]
        SystemFuncType_PVPSignal = 11,
        [ProtoEnum(Name="SystemFuncType_WormHole", Value=12)]
        SystemFuncType_WormHole = 12,
        [ProtoEnum(Name="SystemFuncType_StarMap", Value=13)]
        SystemFuncType_StarMap = 13,
        [ProtoEnum(Name="SystemFuncType_Missile", Value=14)]
        SystemFuncType_Missile = 14,
        [ProtoEnum(Name="SystemFuncType_DriveLicense", Value=15)]
        SystemFuncType_DriveLicense = 15,
        [ProtoEnum(Name="SystemFuncType_CharacterSkill", Value=0x10)]
        SystemFuncType_CharacterSkill = 0x10,
        [ProtoEnum(Name="SystemFuncType_CharacterAddProperty", Value=0x11)]
        SystemFuncType_CharacterAddProperty = 0x11,
        [ProtoEnum(Name="SystemFuncType_Team", Value=0x12)]
        SystemFuncType_Team = 0x12,
        [ProtoEnum(Name="SystemFuncType_Friend", Value=0x13)]
        SystemFuncType_Friend = 0x13,
        [ProtoEnum(Name="SystemFuncType_Chat", Value=20)]
        SystemFuncType_Chat = 20,
        [ProtoEnum(Name="SystemFuncType_Mail", Value=0x15)]
        SystemFuncType_Mail = 0x15,
        [ProtoEnum(Name="SystemFuncType_NpcShop", Value=0x16)]
        SystemFuncType_NpcShop = 0x16,
        [ProtoEnum(Name="SystemFuncType_TradeCenter", Value=0x17)]
        SystemFuncType_TradeCenter = 0x17,
        [ProtoEnum(Name="SystemFuncType_BlackMarket", Value=0x18)]
        SystemFuncType_BlackMarket = 0x18,
        [ProtoEnum(Name="SystemFuncType_Rank", Value=0x19)]
        SystemFuncType_Rank = 0x19,
        [ProtoEnum(Name="SystemFuncType_CaptainTrain", Value=0x1a)]
        SystemFuncType_CaptainTrain = 0x1a,
        [ProtoEnum(Name="SystemFuncType_CaptainRetire", Value=0x1b)]
        SystemFuncType_CaptainRetire = 0x1b,
        [ProtoEnum(Name="SystemFuncType_CaptainFeatsLearn", Value=0x1c)]
        SystemFuncType_CaptainFeatsLearn = 0x1c,
        [ProtoEnum(Name="SystemFuncType_ShipHangarAmmoReload", Value=0x1d)]
        SystemFuncType_ShipHangarAmmoReload = 0x1d,
        [ProtoEnum(Name="SystemFuncType_ShipHangarShipConfig", Value=30)]
        SystemFuncType_ShipHangarShipConfig = 30,
        [ProtoEnum(Name="SystemFuncType_ShipHangarEquipSlot", Value=0x1f)]
        SystemFuncType_ShipHangarEquipSlot = 0x1f,
        [ProtoEnum(Name="SystemFuncType_ShipHangarPack", Value=0x20)]
        SystemFuncType_ShipHangarPack = 0x20,
        [ProtoEnum(Name="SystemFuncType_InSpaceEquipSlot", Value=0x21)]
        SystemFuncType_InSpaceEquipSlot = 0x21,
        [ProtoEnum(Name="SystemFuncType_ShipHangar", Value=0x22)]
        SystemFuncType_ShipHangar = 0x22,
        [ProtoEnum(Name="SystemFuncType_CaptainSetWingMan", Value=0x23)]
        SystemFuncType_CaptainSetWingMan = 0x23,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUIQuestNotice", Value=0x24)]
        SystemFuncType_SolarSystemUIQuestNotice = 0x24,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUITargetList", Value=0x25)]
        SystemFuncType_SolarSystemUITargetList = 0x25,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUIWeaponPanel", Value=0x26)]
        SystemFuncType_SolarSystemUIWeaponPanel = 0x26,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUISelfInfoPanel", Value=0x27)]
        SystemFuncType_SolarSystemUISelfInfoPanel = 0x27,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUIDefalutBooster", Value=40)]
        SystemFuncType_SolarSystemUIDefalutBooster = 40,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUIChatPanel", Value=0x29)]
        SystemFuncType_SolarSystemUIChatPanel = 0x29,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUIFuncMenuPanel", Value=0x2a)]
        SystemFuncType_SolarSystemUIFuncMenuPanel = 0x2a,
        [ProtoEnum(Name="SystemFuncType_StarMapQuestProbe", Value=0x2b)]
        SystemFuncType_StarMapQuestProbe = 0x2b,
        [ProtoEnum(Name="SystemFuncType_BaseRedeploy", Value=0x2c)]
        SystemFuncType_BaseRedeploy = 0x2c,
        [ProtoEnum(Name="SystemFuncType_Billboard", Value=0x2d)]
        SystemFuncType_Billboard = 0x2d,
        [ProtoEnum(Name="SystemFuncType_HeadQuarter", Value=0x2e)]
        SystemFuncType_HeadQuarter = 0x2e,
        [ProtoEnum(Name="SystemFuncType_Launch", Value=0x2f)]
        SystemFuncType_Launch = 0x2f,
        [ProtoEnum(Name="SystemFuncType_Chip", Value=0x30)]
        SystemFuncType_Chip = 0x30,
        [ProtoEnum(Name="SystemFuncType_Activity", Value=0x31)]
        SystemFuncType_Activity = 0x31,
        [ProtoEnum(Name="SystemFuncType_ActionPlan", Value=50)]
        SystemFuncType_ActionPlan = 50,
        [ProtoEnum(Name="SystemFuncType_EmergencySignal4SignalDifficultGuide", Value=0x33)]
        SystemFuncType_EmergencySignal4SignalDifficultGuide = 0x33,
        [ProtoEnum(Name="SystemFuncType_Guild", Value=0x34)]
        SystemFuncType_Guild = 0x34,
        [ProtoEnum(Name="SystemFuncType_SolarSystemUITacticalEquipInfo", Value=0x35)]
        SystemFuncType_SolarSystemUITacticalEquipInfo = 0x35,
        [ProtoEnum(Name="SystemFuncType_Development", Value=0x36)]
        SystemFuncType_Development = 0x36,
        [ProtoEnum(Name="SystemFuncType_GuildStrategy", Value=0x37)]
        SystemFuncType_GuildStrategy = 0x37,
        [ProtoEnum(Name="SystemFuncType_GuildGalaxy", Value=0x38)]
        SystemFuncType_GuildGalaxy = 0x38,
        [ProtoEnum(Name="SystemFuncType_GuildProduce", Value=0x39)]
        SystemFuncType_GuildProduce = 0x39,
        [ProtoEnum(Name="SystemFuncType_GuildFlagshipManagement", Value=0x3a)]
        SystemFuncType_GuildFlagshipManagement = 0x3a,
        [ProtoEnum(Name="SystemFuncType_FactionCredit", Value=0x3b)]
        SystemFuncType_FactionCredit = 0x3b,
        [ProtoEnum(Name="SystemFuncType_BranchStory", Value=60)]
        SystemFuncType_BranchStory = 60,
        [ProtoEnum(Name="SystemFuncType_Trade", Value=0x3d)]
        SystemFuncType_Trade = 0x3d,
        [ProtoEnum(Name="SystemFuncType_NewItemHint", Value=0x3e)]
        SystemFuncType_NewItemHint = 0x3e,
        [ProtoEnum(Name="SystemFuncType_BattlePass", Value=0x3f)]
        SystemFuncType_BattlePass = 0x3f,
        [ProtoEnum(Name="SystemFuncType_LoginActivity", Value=0x40)]
        SystemFuncType_LoginActivity = 0x40,
        [ProtoEnum(Name="SystemFuncType_Max", Value=0x41)]
        SystemFuncType_Max = 0x41
    }
}

