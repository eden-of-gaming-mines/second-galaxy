﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDelegateMineralSignalDifficultInfo")]
    public class ConfigDataDelegateMineralSignalDifficultInfo : IExtensible
    {
        private int _ID;
        private int _Difficult;
        private int _MinMineralCount;
        private int _MaxMineralCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Difficult;
        private static DelegateBridge __Hotfix_set_Difficult;
        private static DelegateBridge __Hotfix_get_MinMineralCount;
        private static DelegateBridge __Hotfix_set_MinMineralCount;
        private static DelegateBridge __Hotfix_get_MaxMineralCount;
        private static DelegateBridge __Hotfix_set_MaxMineralCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Difficult", DataFormat=DataFormat.TwosComplement)]
        public int Difficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="MinMineralCount", DataFormat=DataFormat.TwosComplement)]
        public int MinMineralCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MaxMineralCount", DataFormat=DataFormat.TwosComplement)]
        public int MaxMineralCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

