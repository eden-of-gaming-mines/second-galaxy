﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcShipAIFlag")]
    public enum NpcShipAIFlag
    {
        [ProtoEnum(Name="NpcShipAIFlag_EnableAutoNormalAttack", Value=1)]
        NpcShipAIFlag_EnableAutoNormalAttack = 1,
        [ProtoEnum(Name="NpcShipAIFlag_EnableHighSlotAuto", Value=2)]
        NpcShipAIFlag_EnableHighSlotAuto = 2,
        [ProtoEnum(Name="NpcShipAIFlag_EnableBooster", Value=3)]
        NpcShipAIFlag_EnableBooster = 3,
        [ProtoEnum(Name="NpcShipAIFlag_EnableBlink", Value=4)]
        NpcShipAIFlag_EnableBlink = 4,
        [ProtoEnum(Name="NpcShipAIFlag_EnableSelfRepairer", Value=5)]
        NpcShipAIFlag_EnableSelfRepairer = 5,
        [ProtoEnum(Name="NpcShipAIFlag_EnableSelfEnergyHot", Value=6)]
        NpcShipAIFlag_EnableSelfEnergyHot = 6,
        [ProtoEnum(Name="NpcShipAIFlag_EnableSelfEnhance", Value=7)]
        NpcShipAIFlag_EnableSelfEnhance = 7,
        [ProtoEnum(Name="NpcShipAIFlag_EnableRemoteRepairer", Value=8)]
        NpcShipAIFlag_EnableRemoteRepairer = 8,
        [ProtoEnum(Name="NpcShipAIFlag_EnableEnemyDebuff", Value=9)]
        NpcShipAIFlag_EnableEnemyDebuff = 9,
        [ProtoEnum(Name="NpcShipAIFlag_EnbaleTeamEnhance", Value=10)]
        NpcShipAIFlag_EnbaleTeamEnhance = 10,
        [ProtoEnum(Name="NpcShipAIFlag_PreferNoPlayerTarget", Value=11)]
        NpcShipAIFlag_PreferNoPlayerTarget = 11,
        [ProtoEnum(Name="NpcShipAIFlag_FightBack4Player", Value=12)]
        NpcShipAIFlag_FightBack4Player = 12,
        [ProtoEnum(Name="NpcShipAIFlag_AroundTargetByMaxWeaponRange", Value=13)]
        NpcShipAIFlag_AroundTargetByMaxWeaponRange = 13,
        [ProtoEnum(Name="NpcShipAIFlag_PreferPlayerTarget", Value=14)]
        NpcShipAIFlag_PreferPlayerTarget = 14,
        [ProtoEnum(Name="NpcShipAIFlag_EnableTacticalEquip", Value=15)]
        NpcShipAIFlag_EnableTacticalEquip = 15,
        [ProtoEnum(Name="NpcShipAIFlag_PreferPlayerAndHiredCaptainTarget", Value=0x10)]
        NpcShipAIFlag_PreferPlayerAndHiredCaptainTarget = 0x10,
        [ProtoEnum(Name="NpcShipAIFlag_EnableBoosterAwayFromTarget", Value=0x11)]
        NpcShipAIFlag_EnableBoosterAwayFromTarget = 0x11,
        [ProtoEnum(Name="NpcShipAIFlag_RandPickLockTargetGuardRadius", Value=0x12)]
        NpcShipAIFlag_RandPickLockTargetGuardRadius = 0x12,
        [ProtoEnum(Name="NpcShipAIFlag_PreferLessEnemyTaret", Value=0x13)]
        NpcShipAIFlag_PreferLessEnemyTaret = 0x13,
        [ProtoEnum(Name="NpcShipAIFlag_OnlyAttackLockTarget", Value=20)]
        NpcShipAIFlag_OnlyAttackLockTarget = 20,
        [ProtoEnum(Name="NpcShipAIFlag_EnableAutoSuperWeapon", Value=0x15)]
        NpcShipAIFlag_EnableAutoSuperWeapon = 0x15
    }
}

