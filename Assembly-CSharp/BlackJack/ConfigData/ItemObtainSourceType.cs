﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ItemObtainSourceType")]
    public enum ItemObtainSourceType
    {
        [ProtoEnum(Name="ItemObtainSourceType_Production", Value=1)]
        ItemObtainSourceType_Production = 1,
        [ProtoEnum(Name="ItemObtainSourceType_Auction", Value=2)]
        ItemObtainSourceType_Auction = 2,
        [ProtoEnum(Name="ItemObtainSourceType_NPCShop", Value=3)]
        ItemObtainSourceType_NPCShop = 3,
        [ProtoEnum(Name="ItemObtainSourceType_Recharge", Value=4)]
        ItemObtainSourceType_Recharge = 4,
        [ProtoEnum(Name="ItemObtainSourceType_Mall", Value=5)]
        ItemObtainSourceType_Mall = 5,
        [ProtoEnum(Name="ItemObtainSourceType_Special1", Value=6)]
        ItemObtainSourceType_Special1 = 6,
        [ProtoEnum(Name="ItemObtainSourceType_Special2", Value=7)]
        ItemObtainSourceType_Special2 = 7,
        [ProtoEnum(Name="ItemObtainSourceType_Special3", Value=8)]
        ItemObtainSourceType_Special3 = 8,
        [ProtoEnum(Name="ItemObtainSourceType_Crack", Value=9)]
        ItemObtainSourceType_Crack = 9,
        [ProtoEnum(Name="ItemObtainSourceType_InfectMisson", Value=10)]
        ItemObtainSourceType_InfectMisson = 10,
        [ProtoEnum(Name="ItemObtainSourceType_FreeQuest", Value=11)]
        ItemObtainSourceType_FreeQuest = 11,
        [ProtoEnum(Name="ItemObtainSourceType_DelegateMisson", Value=12)]
        ItemObtainSourceType_DelegateMisson = 12,
        [ProtoEnum(Name="ItemObtainSourceType_DelegateMisson_Fight", Value=13)]
        ItemObtainSourceType_DelegateMisson_Fight = 13,
        [ProtoEnum(Name="ItemObtainSourceType_FreeSignal_Engineer", Value=14)]
        ItemObtainSourceType_FreeSignal_Engineer = 14,
        [ProtoEnum(Name="ItemObtainSourceType_FreeSignal_Scientist", Value=15)]
        ItemObtainSourceType_FreeSignal_Scientist = 15,
        [ProtoEnum(Name="ItemObtainSourceType_FreeSignal_Soldier", Value=0x10)]
        ItemObtainSourceType_FreeSignal_Soldier = 0x10,
        [ProtoEnum(Name="ItemObtainSourceType_FreeSignal_Explorer", Value=0x11)]
        ItemObtainSourceType_FreeSignal_Explorer = 0x11,
        [ProtoEnum(Name="ItemObtainSourceType_GuildProduction", Value=0x12)]
        ItemObtainSourceType_GuildProduction = 0x12,
        [ProtoEnum(Name="ItemObtainSourceType_GuildAction", Value=0x13)]
        ItemObtainSourceType_GuildAction = 0x13,
        [ProtoEnum(Name="ItemObtainSourceType_BlackMarket_Personal", Value=20)]
        ItemObtainSourceType_BlackMarket_Personal = 20,
        [ProtoEnum(Name="ItemObtainSourceType_BlackMarket_Guild", Value=0x15)]
        ItemObtainSourceType_BlackMarket_Guild = 0x15,
        [ProtoEnum(Name="ItemObtainSourceType_ScienceExplore", Value=0x16)]
        ItemObtainSourceType_ScienceExplore = 0x16,
        [ProtoEnum(Name="ItemObtainSourceType_SpaceSignal", Value=0x17)]
        ItemObtainSourceType_SpaceSignal = 0x17,
        [ProtoEnum(Name="ItemObtainSourceType_GuildGalaNpcShop", Value=0x18)]
        ItemObtainSourceType_GuildGalaNpcShop = 0x18,
        [ProtoEnum(Name="ItemObtainSourceType_PanGalaNpcShop", Value=0x19)]
        ItemObtainSourceType_PanGalaNpcShop = 0x19,
        [ProtoEnum(Name="ItemObtainSourceType_FactionNpcShop", Value=0x1a)]
        ItemObtainSourceType_FactionNpcShop = 0x1a,
        [ProtoEnum(Name="ItemObtainSourceType_GuildMiningStation", Value=0x1b)]
        ItemObtainSourceType_GuildMiningStation = 0x1b,
        [ProtoEnum(Name="ItemObtainSourceType_GuildTradePort", Value=0x1c)]
        ItemObtainSourceType_GuildTradePort = 0x1c,
        [ProtoEnum(Name="ItemObtainSourceType_BlackMarket_IrShop", Value=0x1d)]
        ItemObtainSourceType_BlackMarket_IrShop = 0x1d,
        [ProtoEnum(Name="ItemObtainSourceType_Max", Value=30)]
        ItemObtainSourceType_Max = 30
    }
}

