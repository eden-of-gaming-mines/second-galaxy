﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSignalInfo")]
    public class ConfigDataSignalInfo : IExtensible
    {
        private int _ID;
        private string _SignalPrintableId;
        private SignalType _Type;
        private string _SignalLevel;
        private int _DifficultLevel;
        private int _ExpiryTime;
        private int _SceneId;
        private DynamicSceneLocationType _SceneLocationType;
        private DynamicSceneNearCelestialType _SceneNearCelestialType;
        private int _ShipCountLimit;
        private readonly List<ShipType> _ShipTypeLimit;
        private int _ItemDropInfoId;
        private readonly List<MonsterSelectInfo> _MonsterSelectList;
        private readonly List<SignalInfoMonsterDropInfo> _MonsterDropInfo;
        private int _Exp;
        private int _DamageNeedValue;
        private int _SurvivalNeedValue;
        private int _TotalRepaireTime;
        private string _IconResPath;
        private int _ExpectBindMoneyReward;
        private string _NameStrKey;
        private string _TargetStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SignalPrintableId;
        private static DelegateBridge __Hotfix_set_SignalPrintableId;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_SignalLevel;
        private static DelegateBridge __Hotfix_set_SignalLevel;
        private static DelegateBridge __Hotfix_get_DifficultLevel;
        private static DelegateBridge __Hotfix_set_DifficultLevel;
        private static DelegateBridge __Hotfix_get_ExpiryTime;
        private static DelegateBridge __Hotfix_set_ExpiryTime;
        private static DelegateBridge __Hotfix_get_SceneId;
        private static DelegateBridge __Hotfix_set_SceneId;
        private static DelegateBridge __Hotfix_get_SceneLocationType;
        private static DelegateBridge __Hotfix_set_SceneLocationType;
        private static DelegateBridge __Hotfix_get_SceneNearCelestialType;
        private static DelegateBridge __Hotfix_set_SceneNearCelestialType;
        private static DelegateBridge __Hotfix_get_ShipCountLimit;
        private static DelegateBridge __Hotfix_set_ShipCountLimit;
        private static DelegateBridge __Hotfix_get_ShipTypeLimit;
        private static DelegateBridge __Hotfix_get_ItemDropInfoId;
        private static DelegateBridge __Hotfix_set_ItemDropInfoId;
        private static DelegateBridge __Hotfix_get_MonsterSelectList;
        private static DelegateBridge __Hotfix_get_MonsterDropInfo;
        private static DelegateBridge __Hotfix_get_Exp;
        private static DelegateBridge __Hotfix_set_Exp;
        private static DelegateBridge __Hotfix_get_DamageNeedValue;
        private static DelegateBridge __Hotfix_set_DamageNeedValue;
        private static DelegateBridge __Hotfix_get_SurvivalNeedValue;
        private static DelegateBridge __Hotfix_set_SurvivalNeedValue;
        private static DelegateBridge __Hotfix_get_TotalRepaireTime;
        private static DelegateBridge __Hotfix_set_TotalRepaireTime;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ExpectBindMoneyReward;
        private static DelegateBridge __Hotfix_set_ExpectBindMoneyReward;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_TargetStrKey;
        private static DelegateBridge __Hotfix_set_TargetStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SignalPrintableId", DataFormat=DataFormat.Default)]
        public string SignalPrintableId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public SignalType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SignalLevel", DataFormat=DataFormat.Default)]
        public string SignalLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DifficultLevel", DataFormat=DataFormat.TwosComplement)]
        public int DifficultLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ExpiryTime", DataFormat=DataFormat.TwosComplement)]
        public int ExpiryTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="SceneId", DataFormat=DataFormat.TwosComplement)]
        public int SceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SceneLocationType", DataFormat=DataFormat.TwosComplement)]
        public DynamicSceneLocationType SceneLocationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="SceneNearCelestialType", DataFormat=DataFormat.TwosComplement)]
        public DynamicSceneNearCelestialType SceneNearCelestialType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="ShipCountLimit", DataFormat=DataFormat.TwosComplement)]
        public int ShipCountLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, Name="ShipTypeLimit", DataFormat=DataFormat.TwosComplement)]
        public List<ShipType> ShipTypeLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="ItemDropInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ItemDropInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="MonsterSelectList", DataFormat=DataFormat.Default)]
        public List<MonsterSelectInfo> MonsterSelectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, Name="MonsterDropInfo", DataFormat=DataFormat.Default)]
        public List<SignalInfoMonsterDropInfo> MonsterDropInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="Exp", DataFormat=DataFormat.TwosComplement)]
        public int Exp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="DamageNeedValue", DataFormat=DataFormat.TwosComplement)]
        public int DamageNeedValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="SurvivalNeedValue", DataFormat=DataFormat.TwosComplement)]
        public int SurvivalNeedValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="TotalRepaireTime", DataFormat=DataFormat.TwosComplement)]
        public int TotalRepaireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="ExpectBindMoneyReward", DataFormat=DataFormat.TwosComplement)]
        public int ExpectBindMoneyReward
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="TargetStrKey", DataFormat=DataFormat.Default)]
        public string TargetStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

