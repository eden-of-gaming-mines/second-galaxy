﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataMonthlyCardInfo")]
    public class ConfigDataMonthlyCardInfo : IExtensible
    {
        private int _ID;
        private int _Price;
        private MonthlyCardPaymentType _PaymentType;
        private int _BonusTotalValue;
        private readonly List<QuestRewardInfo> _OnceRewardContent;
        private readonly List<QuestRewardInfo> _DailyRewardContent;
        private readonly List<MonthlyCardPriviledgeInfo> _PriviledgeList;
        private string _BgIconPath;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_PaymentType;
        private static DelegateBridge __Hotfix_set_PaymentType;
        private static DelegateBridge __Hotfix_get_BonusTotalValue;
        private static DelegateBridge __Hotfix_set_BonusTotalValue;
        private static DelegateBridge __Hotfix_get_OnceRewardContent;
        private static DelegateBridge __Hotfix_get_DailyRewardContent;
        private static DelegateBridge __Hotfix_get_PriviledgeList;
        private static DelegateBridge __Hotfix_get_BgIconPath;
        private static DelegateBridge __Hotfix_set_BgIconPath;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public int Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PaymentType", DataFormat=DataFormat.TwosComplement)]
        public MonthlyCardPaymentType PaymentType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BonusTotalValue", DataFormat=DataFormat.TwosComplement)]
        public int BonusTotalValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="OnceRewardContent", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> OnceRewardContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="DailyRewardContent", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> DailyRewardContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="PriviledgeList", DataFormat=DataFormat.Default)]
        public List<MonthlyCardPriviledgeInfo> PriviledgeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="BgIconPath", DataFormat=DataFormat.Default)]
        public string BgIconPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

