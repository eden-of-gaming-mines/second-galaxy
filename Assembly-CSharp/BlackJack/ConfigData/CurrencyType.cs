﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CurrencyType")]
    public enum CurrencyType
    {
        [ProtoEnum(Name="CurrencyType_BindMoney", Value=1)]
        CurrencyType_BindMoney = 1,
        [ProtoEnum(Name="CurrencyType_TradeMoney", Value=2)]
        CurrencyType_TradeMoney = 2,
        [ProtoEnum(Name="CurrencyType_RealMoney", Value=3)]
        CurrencyType_RealMoney = 3,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_One", Value=4)]
        CurrencyType_CreditCurrency_One = 4,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_ECD", Value=5)]
        CurrencyType_CreditCurrency_ECD = 5,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_NEF", Value=6)]
        CurrencyType_CreditCurrency_NEF = 6,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_RS", Value=7)]
        CurrencyType_CreditCurrency_RS = 7,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_OE", Value=8)]
        CurrencyType_CreditCurrency_OE = 8,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_USG", Value=9)]
        CurrencyType_CreditCurrency_USG = 9,
        [ProtoEnum(Name="CurrencyType_CreditCurrency_CIV", Value=10)]
        CurrencyType_CreditCurrency_CIV = 10,
        [ProtoEnum(Name="CurrencyType_PanGala", Value=11)]
        CurrencyType_PanGala = 11,
        [ProtoEnum(Name="CurrencyType_GuildGala", Value=12)]
        CurrencyType_GuildGala = 12,
        [ProtoEnum(Name="CurrencyType_Max", Value=13)]
        CurrencyType_Max = 13
    }
}

