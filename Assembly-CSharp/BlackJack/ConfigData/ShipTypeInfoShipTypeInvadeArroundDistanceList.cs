﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipTypeInfoShipTypeInvadeArroundDistanceList")]
    public class ShipTypeInfoShipTypeInvadeArroundDistanceList : IExtensible
    {
        private ShipType _type;
        private int _Value;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_type;
        private static DelegateBridge __Hotfix_set_type;
        private static DelegateBridge __Hotfix_get_Value;
        private static DelegateBridge __Hotfix_set_Value;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="type", DataFormat=DataFormat.TwosComplement)]
        public ShipType type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Value", DataFormat=DataFormat.TwosComplement)]
        public int Value
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

