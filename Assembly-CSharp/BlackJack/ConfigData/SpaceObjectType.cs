﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SpaceObjectType")]
    public enum SpaceObjectType
    {
        [ProtoEnum(Name="SpaceObjectType_Invalid", Value=0)]
        SpaceObjectType_Invalid = 0,
        [ProtoEnum(Name="SpaceObjectType_Star", Value=1)]
        SpaceObjectType_Star = 1,
        [ProtoEnum(Name="SpaceObjectType_Planet", Value=2)]
        SpaceObjectType_Planet = 2,
        [ProtoEnum(Name="SpaceObjectType_Moon", Value=3)]
        SpaceObjectType_Moon = 3,
        [ProtoEnum(Name="SpaceObjectType_PlanetLikeBuilding", Value=4)]
        SpaceObjectType_PlanetLikeBuilding = 4,
        [ProtoEnum(Name="SpaceObjectType_StarGate", Value=5)]
        SpaceObjectType_StarGate = 5,
        [ProtoEnum(Name="SpaceObjectType_SpaceStation", Value=6)]
        SpaceObjectType_SpaceStation = 6,
        [ProtoEnum(Name="SpaceObjectType_PlayerShip", Value=7)]
        SpaceObjectType_PlayerShip = 7,
        [ProtoEnum(Name="SpaceObjectType_NpcShip", Value=8)]
        SpaceObjectType_NpcShip = 8,
        [ProtoEnum(Name="SpaceObjectType_SimpleObject", Value=9)]
        SpaceObjectType_SimpleObject = 9,
        [ProtoEnum(Name="SpaceObjectType_SceneObject", Value=10)]
        SpaceObjectType_SceneObject = 10,
        [ProtoEnum(Name="SpaceObjectType_EmptyObject", Value=11)]
        SpaceObjectType_EmptyObject = 11,
        [ProtoEnum(Name="SpaceObjectType_Scene", Value=12)]
        SpaceObjectType_Scene = 12,
        [ProtoEnum(Name="SpaceObjectType_DropBox", Value=13)]
        SpaceObjectType_DropBox = 13,
        [ProtoEnum(Name="SpaceObjectType_HiredCaptainShip", Value=14)]
        SpaceObjectType_HiredCaptainShip = 14,
        [ProtoEnum(Name="SpaceObjectType_Trigger", Value=15)]
        SpaceObjectType_Trigger = 15
    }
}

