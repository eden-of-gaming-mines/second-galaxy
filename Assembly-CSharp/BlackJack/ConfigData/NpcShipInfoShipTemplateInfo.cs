﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NpcShipInfoShipTemplateInfo")]
    public class NpcShipInfoShipTemplateInfo : IExtensible
    {
        private int _LvMin;
        private int _LvMax;
        private int _ShipTemplateId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LvMin;
        private static DelegateBridge __Hotfix_set_LvMin;
        private static DelegateBridge __Hotfix_get_LvMax;
        private static DelegateBridge __Hotfix_set_LvMax;
        private static DelegateBridge __Hotfix_get_ShipTemplateId;
        private static DelegateBridge __Hotfix_set_ShipTemplateId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LvMin", DataFormat=DataFormat.TwosComplement)]
        public int LvMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LvMax", DataFormat=DataFormat.TwosComplement)]
        public int LvMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int ShipTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

