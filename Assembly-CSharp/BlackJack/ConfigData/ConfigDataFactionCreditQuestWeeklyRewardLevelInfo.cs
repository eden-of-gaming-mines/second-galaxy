﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFactionCreditQuestWeeklyRewardLevelInfo")]
    public class ConfigDataFactionCreditQuestWeeklyRewardLevelInfo : IExtensible
    {
        private int _ID;
        private FactionCreditLevel _Level;
        private readonly List<CreditRewardInfo> _CreditRewardList;
        private readonly List<CreditRewardItemInfo> _StoreItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_CreditRewardList;
        private static DelegateBridge __Hotfix_get_StoreItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public FactionCreditLevel Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="CreditRewardList", DataFormat=DataFormat.Default)]
        public List<CreditRewardInfo> CreditRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="StoreItemList", DataFormat=DataFormat.Default)]
        public List<CreditRewardItemInfo> StoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

