﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DynamicSceneLocationType")]
    public enum DynamicSceneLocationType
    {
        [ProtoEnum(Name="DynamicSceneLocationType_Random", Value=1)]
        DynamicSceneLocationType_Random = 1,
        [ProtoEnum(Name="DynamicSceneLocationType_CloseToSun", Value=2)]
        DynamicSceneLocationType_CloseToSun = 2,
        [ProtoEnum(Name="DynamicSceneLocationType_NearSun", Value=3)]
        DynamicSceneLocationType_NearSun = 3,
        [ProtoEnum(Name="DynamicSceneLocationType_EearthLike", Value=4)]
        DynamicSceneLocationType_EearthLike = 4,
        [ProtoEnum(Name="DynamicSceneLocationType_Gaint", Value=5)]
        DynamicSceneLocationType_Gaint = 5,
        [ProtoEnum(Name="DynamicSceneLocationType_AsteroidBelt", Value=6)]
        DynamicSceneLocationType_AsteroidBelt = 6,
        [ProtoEnum(Name="DynamicSceneLocationType_FarSun", Value=7)]
        DynamicSceneLocationType_FarSun = 7,
        [ProtoEnum(Name="DynamicSceneLocationType_Outer", Value=8)]
        DynamicSceneLocationType_Outer = 8
    }
}

