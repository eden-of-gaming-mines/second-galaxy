﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcScriptOptType")]
    public enum NpcScriptOptType
    {
        [ProtoEnum(Name="NpcScriptOptType_NexDialog", Value=1)]
        NpcScriptOptType_NexDialog = 1,
        [ProtoEnum(Name="NpcScriptOptType_Close", Value=2)]
        NpcScriptOptType_Close = 2,
        [ProtoEnum(Name="NpcScriptOptType_Confirm2Server", Value=3)]
        NpcScriptOptType_Confirm2Server = 3,
        [ProtoEnum(Name="NpcScriptOptType_StartDialog", Value=4)]
        NpcScriptOptType_StartDialog = 4,
        [ProtoEnum(Name="NpcScriptOptType_PlayerChoiceA", Value=5)]
        NpcScriptOptType_PlayerChoiceA = 5,
        [ProtoEnum(Name="NpcScriptOptType_PlayerChoiceB", Value=6)]
        NpcScriptOptType_PlayerChoiceB = 6,
        [ProtoEnum(Name="NpcScriptOptType_PlayerChoiceC", Value=7)]
        NpcScriptOptType_PlayerChoiceC = 7
    }
}

