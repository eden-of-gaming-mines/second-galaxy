﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPassiveSkillTypeInfo")]
    public class ConfigDataPassiveSkillTypeInfo : IExtensible
    {
        private int _ID;
        private PropertyCategory _Category;
        private readonly List<int> _SkillIdListForSkillTree;
        private string _SkillTreeTemplateResPath;
        private string _IconRes;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_SkillIdListForSkillTree;
        private static DelegateBridge __Hotfix_get_SkillTreeTemplateResPath;
        private static DelegateBridge __Hotfix_set_SkillTreeTemplateResPath;
        private static DelegateBridge __Hotfix_get_IconRes;
        private static DelegateBridge __Hotfix_set_IconRes;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="SkillIdListForSkillTree", DataFormat=DataFormat.TwosComplement)]
        public List<int> SkillIdListForSkillTree
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SkillTreeTemplateResPath", DataFormat=DataFormat.Default)]
        public string SkillTreeTemplateResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="IconRes", DataFormat=DataFormat.Default)]
        public string IconRes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

