﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MonthlyCardPaymentType")]
    public enum MonthlyCardPaymentType
    {
        [ProtoEnum(Name="MonthlyCardPaymentType_Initial", Value=0)]
        MonthlyCardPaymentType_Initial = 0,
        [ProtoEnum(Name="MonthlyCardPaymentType_Normal", Value=1)]
        MonthlyCardPaymentType_Normal = 1,
        [ProtoEnum(Name="MonthlyCardPaymentType_Subscribe", Value=2)]
        MonthlyCardPaymentType_Subscribe = 2,
        [ProtoEnum(Name="MonthlyCardPaymentType_Max", Value=3)]
        MonthlyCardPaymentType_Max = 3
    }
}

