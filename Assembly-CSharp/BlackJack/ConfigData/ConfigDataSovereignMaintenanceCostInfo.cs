﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSovereignMaintenanceCostInfo")]
    public class ConfigDataSovereignMaintenanceCostInfo : IExtensible
    {
        private int _ID;
        private int _MaintenanceCostLevel;
        private int _RefinePenalty;
        private int _SovereignBenefitsPenalty;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_MaintenanceCostLevel;
        private static DelegateBridge __Hotfix_set_MaintenanceCostLevel;
        private static DelegateBridge __Hotfix_get_RefinePenalty;
        private static DelegateBridge __Hotfix_set_RefinePenalty;
        private static DelegateBridge __Hotfix_get_SovereignBenefitsPenalty;
        private static DelegateBridge __Hotfix_set_SovereignBenefitsPenalty;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="MaintenanceCostLevel", DataFormat=DataFormat.TwosComplement)]
        public int MaintenanceCostLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="RefinePenalty", DataFormat=DataFormat.TwosComplement)]
        public int RefinePenalty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SovereignBenefitsPenalty", DataFormat=DataFormat.TwosComplement)]
        public int SovereignBenefitsPenalty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

