﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildMineralExtraInfo")]
    public class ConfigDataGuildMineralExtraInfo : IExtensible
    {
        private int _ID;
        private int _GuildMineralCompressId;
        private float _GuildMineralDonateGuildGalaFactor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GuildMineralCompressId;
        private static DelegateBridge __Hotfix_set_GuildMineralCompressId;
        private static DelegateBridge __Hotfix_get_GuildMineralDonateGuildGalaFactor;
        private static DelegateBridge __Hotfix_set_GuildMineralDonateGuildGalaFactor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GuildMineralCompressId", DataFormat=DataFormat.TwosComplement)]
        public int GuildMineralCompressId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GuildMineralDonateGuildGalaFactor", DataFormat=DataFormat.FixedSize)]
        public float GuildMineralDonateGuildGalaFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

