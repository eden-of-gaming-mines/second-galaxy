﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="FactionCreditLevel")]
    public enum FactionCreditLevel
    {
        [ProtoEnum(Name="FactionCreditLevel_N5", Value=1)]
        FactionCreditLevel_N5 = 1,
        [ProtoEnum(Name="FactionCreditLevel_N4", Value=2)]
        FactionCreditLevel_N4 = 2,
        [ProtoEnum(Name="FactionCreditLevel_N3", Value=3)]
        FactionCreditLevel_N3 = 3,
        [ProtoEnum(Name="FactionCreditLevel_N2", Value=4)]
        FactionCreditLevel_N2 = 4,
        [ProtoEnum(Name="FactionCreditLevel_N1", Value=5)]
        FactionCreditLevel_N1 = 5,
        [ProtoEnum(Name="FactionCreditLevel_Neutral", Value=6)]
        FactionCreditLevel_Neutral = 6,
        [ProtoEnum(Name="FactionCreditLevel_P1", Value=7)]
        FactionCreditLevel_P1 = 7,
        [ProtoEnum(Name="FactionCreditLevel_P2", Value=8)]
        FactionCreditLevel_P2 = 8,
        [ProtoEnum(Name="FactionCreditLevel_P3", Value=9)]
        FactionCreditLevel_P3 = 9,
        [ProtoEnum(Name="FactionCreditLevel_P4", Value=10)]
        FactionCreditLevel_P4 = 10,
        [ProtoEnum(Name="FactionCreditLevel_P5", Value=11)]
        FactionCreditLevel_P5 = 11
    }
}

