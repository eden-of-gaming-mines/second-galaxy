﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildActionAdditionRewardInfo")]
    public class ConfigDataGuildActionAdditionRewardInfo : IExtensible
    {
        private int _ID;
        private int _DifficultLevel;
        private readonly List<QuestRewardInfo> _RewardList;
        private readonly List<GuildActionAdditionRewardInfoRewardItemList> _RewardItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_DifficultLevel;
        private static DelegateBridge __Hotfix_set_DifficultLevel;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_RewardItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DifficultLevel", DataFormat=DataFormat.TwosComplement)]
        public int DifficultLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="RewardItemList", DataFormat=DataFormat.Default)]
        public List<GuildActionAdditionRewardInfoRewardItemList> RewardItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

