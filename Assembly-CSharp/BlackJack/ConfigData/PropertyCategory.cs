﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="PropertyCategory")]
    public enum PropertyCategory
    {
        [ProtoEnum(Name="PropertyCategory_None", Value=0)]
        PropertyCategory_None = 0,
        [ProtoEnum(Name="PropertyCategory_Attack", Value=1)]
        PropertyCategory_Attack = 1,
        [ProtoEnum(Name="PropertyCategory_Defence", Value=2)]
        PropertyCategory_Defence = 2,
        [ProtoEnum(Name="PropertyCategory_Electron", Value=3)]
        PropertyCategory_Electron = 3,
        [ProtoEnum(Name="PropertyCategory_Drive", Value=4)]
        PropertyCategory_Drive = 4
    }
}

