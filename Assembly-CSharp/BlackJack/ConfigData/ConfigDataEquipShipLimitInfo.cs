﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataEquipShipLimitInfo")]
    public class ConfigDataEquipShipLimitInfo : IExtensible
    {
        private int _ID;
        private string _Desc;
        private readonly List<int> _ShipIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_ShipIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ShipIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> ShipIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

