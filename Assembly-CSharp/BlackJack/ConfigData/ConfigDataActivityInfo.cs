﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataActivityInfo")]
    public class ConfigDataActivityInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.ActivityType _ActivityType;
        private int _ActivityPointCost;
        private int _Vitality;
        private bool _IsDaily;
        private int _PlayCountLimit;
        private int _RewardPreview;
        private string _IconResString;
        private string _PicResString;
        private int _SortPriority;
        private readonly List<ActivityRewardInfo> _RewardList;
        private SystemFuncType _RelatedSystemFuncType;
        private bool _IsValid;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _UnLockStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ActivityType;
        private static DelegateBridge __Hotfix_set_ActivityType;
        private static DelegateBridge __Hotfix_get_ActivityPointCost;
        private static DelegateBridge __Hotfix_set_ActivityPointCost;
        private static DelegateBridge __Hotfix_get_Vitality;
        private static DelegateBridge __Hotfix_set_Vitality;
        private static DelegateBridge __Hotfix_get_IsDaily;
        private static DelegateBridge __Hotfix_set_IsDaily;
        private static DelegateBridge __Hotfix_get_PlayCountLimit;
        private static DelegateBridge __Hotfix_set_PlayCountLimit;
        private static DelegateBridge __Hotfix_get_RewardPreview;
        private static DelegateBridge __Hotfix_set_RewardPreview;
        private static DelegateBridge __Hotfix_get_IconResString;
        private static DelegateBridge __Hotfix_set_IconResString;
        private static DelegateBridge __Hotfix_get_PicResString;
        private static DelegateBridge __Hotfix_set_PicResString;
        private static DelegateBridge __Hotfix_get_SortPriority;
        private static DelegateBridge __Hotfix_set_SortPriority;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_RelatedSystemFuncType;
        private static DelegateBridge __Hotfix_set_RelatedSystemFuncType;
        private static DelegateBridge __Hotfix_get_IsValid;
        private static DelegateBridge __Hotfix_set_IsValid;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_UnLockStrKey;
        private static DelegateBridge __Hotfix_set_UnLockStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ActivityType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.ActivityType ActivityType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ActivityPointCost", DataFormat=DataFormat.TwosComplement)]
        public int ActivityPointCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Vitality", DataFormat=DataFormat.TwosComplement)]
        public int Vitality
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IsDaily", DataFormat=DataFormat.Default)]
        public bool IsDaily
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="PlayCountLimit", DataFormat=DataFormat.TwosComplement)]
        public int PlayCountLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="RewardPreview", DataFormat=DataFormat.TwosComplement)]
        public int RewardPreview
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IconResString", DataFormat=DataFormat.Default)]
        public string IconResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="PicResString", DataFormat=DataFormat.Default)]
        public string PicResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="SortPriority", DataFormat=DataFormat.TwosComplement)]
        public int SortPriority
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<ActivityRewardInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="RelatedSystemFuncType", DataFormat=DataFormat.TwosComplement)]
        public SystemFuncType RelatedSystemFuncType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="IsValid", DataFormat=DataFormat.Default)]
        public bool IsValid
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="UnLockStrKey", DataFormat=DataFormat.Default)]
        public string UnLockStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

