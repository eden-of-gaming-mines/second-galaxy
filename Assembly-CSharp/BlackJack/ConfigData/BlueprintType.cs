﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BlueprintType")]
    public enum BlueprintType
    {
        [ProtoEnum(Name="BlueprintType_Invalid", Value=0)]
        BlueprintType_Invalid = 0,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_Frigate", Value=1)]
        BlueprintType_ShipBlueprint_Frigate = 1,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_Destroyer", Value=2)]
        BlueprintType_ShipBlueprint_Destroyer = 2,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_Cruiser", Value=3)]
        BlueprintType_ShipBlueprint_Cruiser = 3,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_BattleCruiser", Value=4)]
        BlueprintType_ShipBlueprint_BattleCruiser = 4,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_BattleShip", Value=5)]
        BlueprintType_ShipBlueprint_BattleShip = 5,
        [ProtoEnum(Name="BlueprintType_ShipBlueprint_Industry", Value=6)]
        BlueprintType_ShipBlueprint_Industry = 6,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Railgun", Value=7)]
        BlueprintType_WeaponBlueprint_Weapon_Railgun = 7,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Plasma", Value=8)]
        BlueprintType_WeaponBlueprint_Weapon_Plasma = 8,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Cannon", Value=9)]
        BlueprintType_WeaponBlueprint_Weapon_Cannon = 9,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Laser", Value=10)]
        BlueprintType_WeaponBlueprint_Weapon_Laser = 10,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Drone", Value=11)]
        BlueprintType_WeaponBlueprint_Weapon_Drone = 11,
        [ProtoEnum(Name="BlueprintType_WeaponBlueprint_Weapon_Missile", Value=12)]
        BlueprintType_WeaponBlueprint_Weapon_Missile = 12,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprint_Ammo_Railgun", Value=13)]
        BlueprintType_AmmoBlueprint_Ammo_Railgun = 13,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprint_Ammo_Plasma", Value=14)]
        BlueprintType_AmmoBlueprint_Ammo_Plasma = 14,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprint_Ammo_Cannon", Value=15)]
        BlueprintType_AmmoBlueprint_Ammo_Cannon = 15,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprint_Ammo_Laser", Value=0x10)]
        BlueprintType_AmmoBlueprint_Ammo_Laser = 0x10,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprintt_Ammo_Drone", Value=0x11)]
        BlueprintType_AmmoBlueprintt_Ammo_Drone = 0x11,
        [ProtoEnum(Name="BlueprintType_AmmoBlueprint_Ammo_Missile", Value=0x12)]
        BlueprintType_AmmoBlueprint_Ammo_Missile = 0x12,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_SelfEnhancer", Value=0x13)]
        BlueprintType_DeviceBlueprint_SelfEnhancer = 0x13,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RangeEnhancer", Value=20)]
        BlueprintType_DeviceBlueprint_RangeEnhancer = 20,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_SelfRecover", Value=0x15)]
        BlueprintType_DeviceBlueprint_SelfRecover = 0x15,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RemoteRecover", Value=0x16)]
        BlueprintType_DeviceBlueprint_RemoteRecover = 0x16,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RangeRecover", Value=0x17)]
        BlueprintType_DeviceBlueprint_RangeRecover = 0x17,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RemoteDisturber", Value=0x18)]
        BlueprintType_DeviceBlueprint_RemoteDisturber = 0x18,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RangeDisturber", Value=0x19)]
        BlueprintType_DeviceBlueprint_RangeDisturber = 0x19,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_TacticalEquipment", Value=0x1a)]
        BlueprintType_DeviceBlueprint_TacticalEquipment = 0x1a,
        [ProtoEnum(Name="BlueprintType_DeviceBlueprint_RemoteEnhancer", Value=0x1b)]
        BlueprintType_DeviceBlueprint_RemoteEnhancer = 0x1b,
        [ProtoEnum(Name="BlueprintType_ComponentBlueprint_DamageEnhancer", Value=0x1c)]
        BlueprintType_ComponentBlueprint_DamageEnhancer = 0x1c,
        [ProtoEnum(Name="BlueprintType_ComponentBlueprint_ShootRangeEnhancer", Value=0x1d)]
        BlueprintType_ComponentBlueprint_ShootRangeEnhancer = 0x1d,
        [ProtoEnum(Name="BlueprintType_ComponentBlueprint_ElectronicEnhancer", Value=30)]
        BlueprintType_ComponentBlueprint_ElectronicEnhancer = 30,
        [ProtoEnum(Name="BlueprintType_ComponentBlueprint_ShieldEnhancer", Value=0x1f)]
        BlueprintType_ComponentBlueprint_ShieldEnhancer = 0x1f,
        [ProtoEnum(Name="BlueprintType_ComponentBlueprint_DrivingEnhancer", Value=0x20)]
        BlueprintType_ComponentBlueprint_DrivingEnhancer = 0x20,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Weapon", Value=0x21)]
        BlueprintType_OtherBlueprint_Module_Weapon = 0x21,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Defence", Value=0x22)]
        BlueprintType_OtherBlueprint_Module_Defence = 0x22,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Accelerate", Value=0x23)]
        BlueprintType_OtherBlueprint_Module_Accelerate = 0x23,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Energy", Value=0x24)]
        BlueprintType_OtherBlueprint_Module_Energy = 0x24,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Tactics", Value=0x25)]
        BlueprintType_OtherBlueprint_Module_Tactics = 0x25,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Collection", Value=0x26)]
        BlueprintType_OtherBlueprint_Module_Collection = 0x26,
        [ProtoEnum(Name="BlueprintType_OtherBlueprint_Module_Warehousing", Value=0x27)]
        BlueprintType_OtherBlueprint_Module_Warehousing = 0x27,
        [ProtoEnum(Name="BlueprintType_Max", Value=40)]
        BlueprintType_Max = 40
    }
}

