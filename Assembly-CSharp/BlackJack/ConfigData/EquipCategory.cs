﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="EquipCategory")]
    public enum EquipCategory
    {
        [ProtoEnum(Name="EquipCategory_SelfEnhancer", Value=1)]
        EquipCategory_SelfEnhancer = 1,
        [ProtoEnum(Name="EquipCategory_RemoteEnhancer", Value=2)]
        EquipCategory_RemoteEnhancer = 2,
        [ProtoEnum(Name="EquipCategory_RangeEnhancer", Value=3)]
        EquipCategory_RangeEnhancer = 3,
        [ProtoEnum(Name="EquipCategory_SelfRecover", Value=4)]
        EquipCategory_SelfRecover = 4,
        [ProtoEnum(Name="EquipCategory_RemoteRecover", Value=5)]
        EquipCategory_RemoteRecover = 5,
        [ProtoEnum(Name="EquipCategory_RangeRecover", Value=6)]
        EquipCategory_RangeRecover = 6,
        [ProtoEnum(Name="EquipCategory_RemoteDisturber", Value=7)]
        EquipCategory_RemoteDisturber = 7,
        [ProtoEnum(Name="EquipCategory_RangeDisturber", Value=8)]
        EquipCategory_RangeDisturber = 8,
        [ProtoEnum(Name="EquipCategory_TacticalEquipment", Value=9)]
        EquipCategory_TacticalEquipment = 9,
        [ProtoEnum(Name="EquipCategory_Separator", Value=20)]
        EquipCategory_Separator = 20,
        [ProtoEnum(Name="EquipCategory_DamageEnhancer", Value=0x15)]
        EquipCategory_DamageEnhancer = 0x15,
        [ProtoEnum(Name="EquipCategory_ShootRangeEnhancer", Value=0x16)]
        EquipCategory_ShootRangeEnhancer = 0x16,
        [ProtoEnum(Name="EquipCategory_ElectronicEnhancer", Value=0x17)]
        EquipCategory_ElectronicEnhancer = 0x17,
        [ProtoEnum(Name="EquipCategory_ShieldEnhancer", Value=0x18)]
        EquipCategory_ShieldEnhancer = 0x18,
        [ProtoEnum(Name="EquipCategory_DrivingEnhancer", Value=0x19)]
        EquipCategory_DrivingEnhancer = 0x19,
        [ProtoEnum(Name="EquipCategory_Max", Value=0x1a)]
        EquipCategory_Max = 0x1a
    }
}

