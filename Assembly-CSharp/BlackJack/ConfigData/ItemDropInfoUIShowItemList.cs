﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ItemDropInfoUIShowItemList")]
    public class ItemDropInfoUIShowItemList : IExtensible
    {
        private StoreItemType _type;
        private int _configId;
        private int _itemIsBind;
        private int _isBigPR;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_type;
        private static DelegateBridge __Hotfix_set_type;
        private static DelegateBridge __Hotfix_get_configId;
        private static DelegateBridge __Hotfix_set_configId;
        private static DelegateBridge __Hotfix_get_itemIsBind;
        private static DelegateBridge __Hotfix_set_itemIsBind;
        private static DelegateBridge __Hotfix_get_isBigPR;
        private static DelegateBridge __Hotfix_set_isBigPR;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="type", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="configId", DataFormat=DataFormat.TwosComplement)]
        public int configId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="itemIsBind", DataFormat=DataFormat.TwosComplement)]
        public int itemIsBind
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="isBigPR", DataFormat=DataFormat.TwosComplement)]
        public int isBigPR
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

