﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDelegateTransportSignalDifficultInfo")]
    public class ConfigDataDelegateTransportSignalDifficultInfo : IExtensible
    {
        private int _ID;
        private int _Difficult;
        private int _MinGoodsCount;
        private int _MaxGoodsCount;
        private int _ProcessDropInfoId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Difficult;
        private static DelegateBridge __Hotfix_set_Difficult;
        private static DelegateBridge __Hotfix_get_MinGoodsCount;
        private static DelegateBridge __Hotfix_set_MinGoodsCount;
        private static DelegateBridge __Hotfix_get_MaxGoodsCount;
        private static DelegateBridge __Hotfix_set_MaxGoodsCount;
        private static DelegateBridge __Hotfix_get_ProcessDropInfoId;
        private static DelegateBridge __Hotfix_set_ProcessDropInfoId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Difficult", DataFormat=DataFormat.TwosComplement)]
        public int Difficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="MinGoodsCount", DataFormat=DataFormat.TwosComplement)]
        public int MinGoodsCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MaxGoodsCount", DataFormat=DataFormat.TwosComplement)]
        public int MaxGoodsCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ProcessDropInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ProcessDropInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

