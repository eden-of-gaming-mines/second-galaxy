﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CrackBoxType")]
    public enum CrackBoxType
    {
        [ProtoEnum(Name="CrackBoxType_Common", Value=1)]
        CrackBoxType_Common = 1,
        [ProtoEnum(Name="CrackBoxType_Mineral", Value=2)]
        CrackBoxType_Mineral = 2,
        [ProtoEnum(Name="CrackBoxType_Chip", Value=3)]
        CrackBoxType_Chip = 3,
        [ProtoEnum(Name="CrackBoxType_NpcCaptainFeats", Value=4)]
        CrackBoxType_NpcCaptainFeats = 4
    }
}

