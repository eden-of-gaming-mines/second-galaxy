﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GiftPackageBuyCountLimitType")]
    public enum GiftPackageBuyCountLimitType
    {
        [ProtoEnum(Name="GiftPackageBuyCountLimitType_Invalid", Value=0)]
        GiftPackageBuyCountLimitType_Invalid = 0,
        [ProtoEnum(Name="GiftPackageBuyCountLimitType_Permanent", Value=1)]
        GiftPackageBuyCountLimitType_Permanent = 1,
        [ProtoEnum(Name="GiftPackageBuyCountLimitType_Weekly", Value=2)]
        GiftPackageBuyCountLimitType_Weekly = 2,
        [ProtoEnum(Name="GiftPackageBuyCountLimitType_Daily", Value=3)]
        GiftPackageBuyCountLimitType_Daily = 3,
        [ProtoEnum(Name="GiftPackageBuyCountLimitType_Max", Value=4)]
        GiftPackageBuyCountLimitType_Max = 4
    }
}

