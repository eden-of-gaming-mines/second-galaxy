﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcMonsterCollectionType")]
    public enum NpcMonsterCollectionType
    {
        [ProtoEnum(Name="NpcMonsterCollectionType_monster1", Value=1)]
        NpcMonsterCollectionType_monster1 = 1,
        [ProtoEnum(Name="NpcMonsterCollectionType_monster2", Value=2)]
        NpcMonsterCollectionType_monster2 = 2,
        [ProtoEnum(Name="NpcMonsterCollectionType_monster3", Value=3)]
        NpcMonsterCollectionType_monster3 = 3,
        [ProtoEnum(Name="NpcMonsterCollectionType_monster4", Value=4)]
        NpcMonsterCollectionType_monster4 = 4,
        [ProtoEnum(Name="NpcMonsterCollectionType_monster5", Value=5)]
        NpcMonsterCollectionType_monster5 = 5,
        [ProtoEnum(Name="NpcMonsterCollectionType_InfectMonster", Value=0x3e8)]
        NpcMonsterCollectionType_InfectMonster = 0x3e8,
        [ProtoEnum(Name="NpcMonsterCollectionType_invalid", Value=0x2711)]
        NpcMonsterCollectionType_invalid = 0x2711
    }
}

