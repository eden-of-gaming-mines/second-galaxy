﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SoundInfo")]
    public class SoundInfo : IExtensible
    {
        private string _EffectName;
        private string _CueName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EffectName;
        private static DelegateBridge __Hotfix_set_EffectName;
        private static DelegateBridge __Hotfix_get_CueName;
        private static DelegateBridge __Hotfix_set_CueName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="EffectName", DataFormat=DataFormat.Default)]
        public string EffectName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CueName", DataFormat=DataFormat.Default)]
        public string CueName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

