﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPredefineGuildMomentsInfo")]
    public class ConfigDataPredefineGuildMomentsInfo : IExtensible
    {
        private int _ID;
        private readonly List<CommonFormatStringParamType> _BriefFormatStringParamTypeList;
        private readonly List<CommonFormatStringParamType> _ContentFormatStringParamTypeList;
        private string _BriefStrKey;
        private string _ContentStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BriefFormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_ContentFormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_BriefStrKey;
        private static DelegateBridge __Hotfix_set_BriefStrKey;
        private static DelegateBridge __Hotfix_get_ContentStrKey;
        private static DelegateBridge __Hotfix_set_ContentStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="BriefFormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> BriefFormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="ContentFormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> ContentFormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="BriefStrKey", DataFormat=DataFormat.Default)]
        public string BriefStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ContentStrKey", DataFormat=DataFormat.Default)]
        public string ContentStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

