﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataAmmoInfo")]
    public class ConfigDataAmmoInfo : IExtensible
    {
        private int _ID;
        private float _SalePrice;
        private float _BindMoneyBuyPrice;
        private WeaponCategory _Category;
        private ShipSizeType _SizeType;
        private RankType _Rank;
        private SubRankType _SubRank;
        private readonly List<float> _DamageComposeList;
        private float _DamageMulti;
        private float _FireRangeMaxMulti;
        private float _PackedSize;
        private bool _IsBindOnPick;
        private string _IconResString;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private string _IconResPath;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_set_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_SizeType;
        private static DelegateBridge __Hotfix_set_SizeType;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_DamageComposeList;
        private static DelegateBridge __Hotfix_get_DamageMulti;
        private static DelegateBridge __Hotfix_set_DamageMulti;
        private static DelegateBridge __Hotfix_get_FireRangeMaxMulti;
        private static DelegateBridge __Hotfix_set_FireRangeMaxMulti;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_IconResString;
        private static DelegateBridge __Hotfix_set_IconResString;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BindMoneyBuyPrice", DataFormat=DataFormat.FixedSize)]
        public float BindMoneyBuyPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public WeaponCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SizeType", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType SizeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="DamageComposeList", DataFormat=DataFormat.FixedSize)]
        public List<float> DamageComposeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DamageMulti", DataFormat=DataFormat.FixedSize)]
        public float DamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="FireRangeMaxMulti", DataFormat=DataFormat.FixedSize)]
        public float FireRangeMaxMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.FixedSize)]
        public float PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="IconResString", DataFormat=DataFormat.Default)]
        public string IconResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

