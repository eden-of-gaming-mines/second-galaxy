﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CameraPlatformInitPosType")]
    public enum CameraPlatformInitPosType
    {
        [ProtoEnum(Name="CameraPlatformInitPosType_InCenter", Value=1)]
        CameraPlatformInitPosType_InCenter = 1,
        [ProtoEnum(Name="CameraPlatformInitPosType_RoadSideByShipNear", Value=2)]
        CameraPlatformInitPosType_RoadSideByShipNear = 2,
        [ProtoEnum(Name="CameraPlatformInitPosType_RoadSideByShipMiddle", Value=3)]
        CameraPlatformInitPosType_RoadSideByShipMiddle = 3,
        [ProtoEnum(Name="CameraPlatformInitPosType_RoadSideByShipFar", Value=4)]
        CameraPlatformInitPosType_RoadSideByShipFar = 4,
        [ProtoEnum(Name="CameraPlatformInitPosType_OffsetToTarget", Value=5)]
        CameraPlatformInitPosType_OffsetToTarget = 5,
        [ProtoEnum(Name="CameraPlatformInitPosType_OffsetToLocation", Value=6)]
        CameraPlatformInitPosType_OffsetToLocation = 6
    }
}

