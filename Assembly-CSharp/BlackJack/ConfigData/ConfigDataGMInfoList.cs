﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGMInfoList")]
    public class ConfigDataGMInfoList : IExtensible
    {
        private int _ID;
        private string _GMCommand;
        private string _GMElementText;
        private string _GMDescription;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GMCommand;
        private static DelegateBridge __Hotfix_set_GMCommand;
        private static DelegateBridge __Hotfix_get_GMElementText;
        private static DelegateBridge __Hotfix_set_GMElementText;
        private static DelegateBridge __Hotfix_get_GMDescription;
        private static DelegateBridge __Hotfix_set_GMDescription;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GMCommand", DataFormat=DataFormat.Default)]
        public string GMCommand
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GMElementText", DataFormat=DataFormat.Default)]
        public string GMElementText
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="GMDescription", DataFormat=DataFormat.Default)]
        public string GMDescription
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

