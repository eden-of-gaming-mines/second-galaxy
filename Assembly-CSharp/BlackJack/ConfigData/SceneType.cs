﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneType")]
    public enum SceneType
    {
        [ProtoEnum(Name="SceneType_Invalid", Value=0)]
        SceneType_Invalid = 0,
        [ProtoEnum(Name="SceneType_Invade", Value=1)]
        SceneType_Invade = 1,
        [ProtoEnum(Name="SceneType_Quest", Value=2)]
        SceneType_Quest = 2,
        [ProtoEnum(Name="SceneType_NormalStatic", Value=3)]
        SceneType_NormalStatic = 3,
        [ProtoEnum(Name="SceneType_NormalDynamic", Value=4)]
        SceneType_NormalDynamic = 4,
        [ProtoEnum(Name="SceneType_WormholeGamePlay", Value=5)]
        SceneType_WormholeGamePlay = 5,
        [ProtoEnum(Name="SceneType_WormholeEntry", Value=6)]
        SceneType_WormholeEntry = 6,
        [ProtoEnum(Name="SceneType_WormholeGate", Value=7)]
        SceneType_WormholeGate = 7,
        [ProtoEnum(Name="SceneType_WormholeTunnel", Value=8)]
        SceneType_WormholeTunnel = 8,
        [ProtoEnum(Name="SceneType_WormholeBackPoint", Value=9)]
        SceneType_WormholeBackPoint = 9,
        [ProtoEnum(Name="SceneType_WormholeSpecial", Value=10)]
        SceneType_WormholeSpecial = 10,
        [ProtoEnum(Name="SceneType_InfectFinalBattle", Value=11)]
        SceneType_InfectFinalBattle = 11,
        [ProtoEnum(Name="SceneType_GuildBuildingScene", Value=12)]
        SceneType_GuildBuildingScene = 12,
        [ProtoEnum(Name="SceneType_StarGate", Value=13)]
        SceneType_StarGate = 13,
        [ProtoEnum(Name="SceneType_GuildActionScene", Value=14)]
        SceneType_GuildActionScene = 14,
        [ProtoEnum(Name="SceneType_SpaceSignal", Value=15)]
        SceneType_SpaceSignal = 15,
        [ProtoEnum(Name="SceneType_FlagShipTeleportFoothold", Value=0x10)]
        SceneType_FlagShipTeleportFoothold = 0x10
    }
}

