﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainShipInfo")]
    public class ConfigDataNpcCaptainShipInfo : IExtensible
    {
        private int _ID;
        private int _AvailableLevel;
        private int _NpcMonsterConfId;
        private int _ShipCompInfoId;
        private int _NpcShipInitPassiveSkillId;
        private int _NpcShipInitPassiveSkillLevel;
        private int _NpcShipScore;
        private string _ShipUseDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AvailableLevel;
        private static DelegateBridge __Hotfix_set_AvailableLevel;
        private static DelegateBridge __Hotfix_get_NpcMonsterConfId;
        private static DelegateBridge __Hotfix_set_NpcMonsterConfId;
        private static DelegateBridge __Hotfix_get_ShipCompInfoId;
        private static DelegateBridge __Hotfix_set_ShipCompInfoId;
        private static DelegateBridge __Hotfix_get_NpcShipInitPassiveSkillId;
        private static DelegateBridge __Hotfix_set_NpcShipInitPassiveSkillId;
        private static DelegateBridge __Hotfix_get_NpcShipInitPassiveSkillLevel;
        private static DelegateBridge __Hotfix_set_NpcShipInitPassiveSkillLevel;
        private static DelegateBridge __Hotfix_get_NpcShipScore;
        private static DelegateBridge __Hotfix_set_NpcShipScore;
        private static DelegateBridge __Hotfix_get_ShipUseDescStrKey;
        private static DelegateBridge __Hotfix_set_ShipUseDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AvailableLevel", DataFormat=DataFormat.TwosComplement)]
        public int AvailableLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="NpcMonsterConfId", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShipCompInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ShipCompInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NpcShipInitPassiveSkillId", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipInitPassiveSkillId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="NpcShipInitPassiveSkillLevel", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipInitPassiveSkillLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="NpcShipScore", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="ShipUseDescStrKey", DataFormat=DataFormat.Default)]
        public string ShipUseDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

