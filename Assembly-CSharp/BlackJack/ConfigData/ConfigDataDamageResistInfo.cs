﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDamageResistInfo")]
    public class ConfigDataDamageResistInfo : IExtensible
    {
        private int _ID;
        private float _Resist2Kinetic;
        private float _Resist2Heat;
        private float _Resist2Electric;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Resist2Kinetic;
        private static DelegateBridge __Hotfix_set_Resist2Kinetic;
        private static DelegateBridge __Hotfix_get_Resist2Heat;
        private static DelegateBridge __Hotfix_set_Resist2Heat;
        private static DelegateBridge __Hotfix_get_Resist2Electric;
        private static DelegateBridge __Hotfix_set_Resist2Electric;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Resist2Kinetic", DataFormat=DataFormat.FixedSize)]
        public float Resist2Kinetic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Resist2Heat", DataFormat=DataFormat.FixedSize)]
        public float Resist2Heat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Resist2Electric", DataFormat=DataFormat.FixedSize)]
        public float Resist2Electric
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

