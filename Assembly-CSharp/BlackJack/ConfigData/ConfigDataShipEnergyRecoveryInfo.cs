﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipEnergyRecoveryInfo")]
    public class ConfigDataShipEnergyRecoveryInfo : IExtensible
    {
        private int _ID;
        private float _EnergyPercent;
        private float _EnergyModifyParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_EnergyPercent;
        private static DelegateBridge __Hotfix_set_EnergyPercent;
        private static DelegateBridge __Hotfix_get_EnergyModifyParam;
        private static DelegateBridge __Hotfix_set_EnergyModifyParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="EnergyPercent", DataFormat=DataFormat.FixedSize)]
        public float EnergyPercent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EnergyModifyParam", DataFormat=DataFormat.FixedSize)]
        public float EnergyModifyParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

