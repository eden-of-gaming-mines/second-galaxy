﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFactionCreditLevelRewardInfo")]
    public class ConfigDataFactionCreditLevelRewardInfo : IExtensible
    {
        private int _ID;
        private long _CreditNeed;
        private int _Credit;
        private readonly List<CreditRewardItemInfo> _StoreItemList;
        private float _CreditFactor;
        private float _CreditCurrencyFactor;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_CreditNeed;
        private static DelegateBridge __Hotfix_set_CreditNeed;
        private static DelegateBridge __Hotfix_get_Credit;
        private static DelegateBridge __Hotfix_set_Credit;
        private static DelegateBridge __Hotfix_get_StoreItemList;
        private static DelegateBridge __Hotfix_get_CreditFactor;
        private static DelegateBridge __Hotfix_set_CreditFactor;
        private static DelegateBridge __Hotfix_get_CreditCurrencyFactor;
        private static DelegateBridge __Hotfix_set_CreditCurrencyFactor;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CreditNeed", DataFormat=DataFormat.TwosComplement)]
        public long CreditNeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Credit", DataFormat=DataFormat.TwosComplement)]
        public int Credit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="StoreItemList", DataFormat=DataFormat.Default)]
        public List<CreditRewardItemInfo> StoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="CreditFactor", DataFormat=DataFormat.FixedSize)]
        public float CreditFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="CreditCurrencyFactor", DataFormat=DataFormat.FixedSize)]
        public float CreditCurrencyFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

