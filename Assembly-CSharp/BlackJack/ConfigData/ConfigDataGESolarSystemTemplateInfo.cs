﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGESolarSystemTemplateInfo")]
    public class ConfigDataGESolarSystemTemplateInfo : IExtensible
    {
        private int _ID;
        private int _SkyBoxResID;
        private string _AmbientColor;
        private float _AmbientIntensity;
        private bool _UseAmbientColor;
        private string _BGMResName;
        private int _ReflectionProbe;
        private int _SkyBoxCubmapResID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SkyBoxResID;
        private static DelegateBridge __Hotfix_set_SkyBoxResID;
        private static DelegateBridge __Hotfix_get_AmbientColor;
        private static DelegateBridge __Hotfix_set_AmbientColor;
        private static DelegateBridge __Hotfix_get_AmbientIntensity;
        private static DelegateBridge __Hotfix_set_AmbientIntensity;
        private static DelegateBridge __Hotfix_get_UseAmbientColor;
        private static DelegateBridge __Hotfix_set_UseAmbientColor;
        private static DelegateBridge __Hotfix_get_BGMResName;
        private static DelegateBridge __Hotfix_set_BGMResName;
        private static DelegateBridge __Hotfix_get_ReflectionProbe;
        private static DelegateBridge __Hotfix_set_ReflectionProbe;
        private static DelegateBridge __Hotfix_get_SkyBoxCubmapResID;
        private static DelegateBridge __Hotfix_set_SkyBoxCubmapResID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SkyBoxResID", DataFormat=DataFormat.TwosComplement)]
        public int SkyBoxResID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AmbientColor", DataFormat=DataFormat.Default)]
        public string AmbientColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="AmbientIntensity", DataFormat=DataFormat.FixedSize)]
        public float AmbientIntensity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="UseAmbientColor", DataFormat=DataFormat.Default)]
        public bool UseAmbientColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="BGMResName", DataFormat=DataFormat.Default)]
        public string BGMResName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ReflectionProbe", DataFormat=DataFormat.TwosComplement)]
        public int ReflectionProbe
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="SkyBoxCubmapResID", DataFormat=DataFormat.TwosComplement)]
        public int SkyBoxCubmapResID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

