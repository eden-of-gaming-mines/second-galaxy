﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipEquipInfo")]
    public class ConfigDataShipEquipInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private EquipCategory _Category;
        private EquipType _Type;
        private ShipSizeType _SizeType;
        private EquipFunctionType _FunctionType;
        private float _SalePrice;
        private float _BindMoneyBuyPrice;
        private RankType _Rank;
        private SubRankType _SubRank;
        private int _PackedSize;
        private bool _IsBindOnPick;
        private ShipEquipSlotType _SlotType;
        private uint _CPUCost;
        private uint _PowerCost;
        private int _EquipLimitId;
        private int _ShipLimitId;
        private readonly List<ConfIdLevelInfo> _DependTechList;
        private uint _LaunchEnergyCost;
        private float _LaunchEnergyPercentCost;
        private uint _LaunchCD;
        private uint _ChargeTime;
        private float _RangeMax;
        private uint _Param;
        private readonly List<int> _BufIdList;
        private readonly List<int> _SelfBufIdList;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private int _LocalPropertiesCount;
        private string _IconResPath;
        private string _ChargeEffect;
        private string _LaunchEffect;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private string _DestEffectBufDesc;
        private readonly List<int> _DestEffectBufDescPropertiesList;
        private string _SelfEffectBufDesc;
        private readonly List<int> _SelfEffectBufDescPropertiesList;
        private readonly List<int> _RelativeTechList;
        private int _FlagShipEquipRelativeId;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _DestEffectBufDescStrKey;
        private string _SelfEffectBufDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_SizeType;
        private static DelegateBridge __Hotfix_set_SizeType;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_set_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_SlotType;
        private static DelegateBridge __Hotfix_set_SlotType;
        private static DelegateBridge __Hotfix_get_CPUCost;
        private static DelegateBridge __Hotfix_set_CPUCost;
        private static DelegateBridge __Hotfix_get_PowerCost;
        private static DelegateBridge __Hotfix_set_PowerCost;
        private static DelegateBridge __Hotfix_get_EquipLimitId;
        private static DelegateBridge __Hotfix_set_EquipLimitId;
        private static DelegateBridge __Hotfix_get_ShipLimitId;
        private static DelegateBridge __Hotfix_set_ShipLimitId;
        private static DelegateBridge __Hotfix_get_DependTechList;
        private static DelegateBridge __Hotfix_get_LaunchEnergyCost;
        private static DelegateBridge __Hotfix_set_LaunchEnergyCost;
        private static DelegateBridge __Hotfix_get_LaunchEnergyPercentCost;
        private static DelegateBridge __Hotfix_set_LaunchEnergyPercentCost;
        private static DelegateBridge __Hotfix_get_LaunchCD;
        private static DelegateBridge __Hotfix_set_LaunchCD;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_RangeMax;
        private static DelegateBridge __Hotfix_set_RangeMax;
        private static DelegateBridge __Hotfix_get_Param;
        private static DelegateBridge __Hotfix_set_Param;
        private static DelegateBridge __Hotfix_get_BufIdList;
        private static DelegateBridge __Hotfix_get_SelfBufIdList;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_set_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ChargeEffect;
        private static DelegateBridge __Hotfix_set_ChargeEffect;
        private static DelegateBridge __Hotfix_get_LaunchEffect;
        private static DelegateBridge __Hotfix_set_LaunchEffect;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_DestEffectBufDesc;
        private static DelegateBridge __Hotfix_set_DestEffectBufDesc;
        private static DelegateBridge __Hotfix_get_DestEffectBufDescPropertiesList;
        private static DelegateBridge __Hotfix_get_SelfEffectBufDesc;
        private static DelegateBridge __Hotfix_set_SelfEffectBufDesc;
        private static DelegateBridge __Hotfix_get_SelfEffectBufDescPropertiesList;
        private static DelegateBridge __Hotfix_get_RelativeTechList;
        private static DelegateBridge __Hotfix_get_FlagShipEquipRelativeId;
        private static DelegateBridge __Hotfix_set_FlagShipEquipRelativeId;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_DestEffectBufDescStrKey;
        private static DelegateBridge __Hotfix_set_DestEffectBufDescStrKey;
        private static DelegateBridge __Hotfix_get_SelfEffectBufDescStrKey;
        private static DelegateBridge __Hotfix_set_SelfEffectBufDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public EquipCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public EquipType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SizeType", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType SizeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public EquipFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="BindMoneyBuyPrice", DataFormat=DataFormat.FixedSize)]
        public float BindMoneyBuyPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.TwosComplement)]
        public int PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="SlotType", DataFormat=DataFormat.TwosComplement)]
        public ShipEquipSlotType SlotType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="CPUCost", DataFormat=DataFormat.TwosComplement)]
        public uint CPUCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="PowerCost", DataFormat=DataFormat.TwosComplement)]
        public uint PowerCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="EquipLimitId", DataFormat=DataFormat.TwosComplement)]
        public int EquipLimitId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="ShipLimitId", DataFormat=DataFormat.TwosComplement)]
        public int ShipLimitId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, Name="DependTechList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="LaunchEnergyCost", DataFormat=DataFormat.TwosComplement)]
        public uint LaunchEnergyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="LaunchEnergyPercentCost", DataFormat=DataFormat.FixedSize)]
        public float LaunchEnergyPercentCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="LaunchCD", DataFormat=DataFormat.TwosComplement)]
        public uint LaunchCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="ChargeTime", DataFormat=DataFormat.TwosComplement)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="RangeMax", DataFormat=DataFormat.FixedSize)]
        public float RangeMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="Param", DataFormat=DataFormat.TwosComplement)]
        public uint Param
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, Name="BufIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x1d, Name="SelfBufIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SelfBufIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(30, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="LocalPropertiesCount", DataFormat=DataFormat.TwosComplement)]
        public int LocalPropertiesCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=true, Name="ChargeEffect", DataFormat=DataFormat.Default)]
        public string ChargeEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=true, Name="LaunchEffect", DataFormat=DataFormat.Default)]
        public string LaunchEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x23, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x24, IsRequired=true, Name="DestEffectBufDesc", DataFormat=DataFormat.Default)]
        public string DestEffectBufDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x25, Name="DestEffectBufDescPropertiesList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DestEffectBufDescPropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x26, IsRequired=true, Name="SelfEffectBufDesc", DataFormat=DataFormat.Default)]
        public string SelfEffectBufDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x27, Name="SelfEffectBufDescPropertiesList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SelfEffectBufDescPropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(40, Name="RelativeTechList", DataFormat=DataFormat.TwosComplement)]
        public List<int> RelativeTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x29, IsRequired=true, Name="FlagShipEquipRelativeId", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipEquipRelativeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=true, Name="DestEffectBufDescStrKey", DataFormat=DataFormat.Default)]
        public string DestEffectBufDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2d, IsRequired=true, Name="SelfEffectBufDescStrKey", DataFormat=DataFormat.Default)]
        public string SelfEffectBufDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

