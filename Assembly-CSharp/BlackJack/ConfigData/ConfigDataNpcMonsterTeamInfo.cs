﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcMonsterTeamInfo")]
    public class ConfigDataNpcMonsterTeamInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _NpcMonsterIdList;
        private int _NpcMonsterCollectionId;
        private int _NpcMonsterLevel;
        private int _TeamFormationID;
        private readonly List<int> _TeamMemberFormationPosIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_NpcMonsterIdList;
        private static DelegateBridge __Hotfix_get_NpcMonsterCollectionId;
        private static DelegateBridge __Hotfix_set_NpcMonsterCollectionId;
        private static DelegateBridge __Hotfix_get_NpcMonsterLevel;
        private static DelegateBridge __Hotfix_set_NpcMonsterLevel;
        private static DelegateBridge __Hotfix_get_TeamFormationID;
        private static DelegateBridge __Hotfix_set_TeamFormationID;
        private static DelegateBridge __Hotfix_get_TeamMemberFormationPosIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="NpcMonsterIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> NpcMonsterIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="NpcMonsterCollectionId", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterCollectionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NpcMonsterLevel", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="TeamFormationID", DataFormat=DataFormat.TwosComplement)]
        public int TeamFormationID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="TeamMemberFormationPosIndex", DataFormat=DataFormat.TwosComplement)]
        public List<int> TeamMemberFormationPosIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

