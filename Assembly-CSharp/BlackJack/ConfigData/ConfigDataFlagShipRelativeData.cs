﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFlagShipRelativeData")]
    public class ConfigDataFlagShipRelativeData : IExtensible
    {
        private int _ID;
        private int _EnergyPackageItemId;
        private int _EnergyPackageStoreSize;
        private float _InterstellarTeleportRadius;
        private float _TeleportTunnelEffectRadius;
        private float _InterstellarTeleportCDParam;
        private readonly List<int> _HighSlotWeaponInfo;
        private readonly List<int> _MiddleSlotEquipInfo;
        private int _UnpackPrepareTime;
        private string _IconResPath;
        private readonly List<int> _TeleportFatigueBufId;
        private readonly List<int> _TeleportCDBufId;
        private int _TeleportFootholdSceneId;
        private int _FakeFlagShipNpcMonsterId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_EnergyPackageItemId;
        private static DelegateBridge __Hotfix_set_EnergyPackageItemId;
        private static DelegateBridge __Hotfix_get_EnergyPackageStoreSize;
        private static DelegateBridge __Hotfix_set_EnergyPackageStoreSize;
        private static DelegateBridge __Hotfix_get_InterstellarTeleportRadius;
        private static DelegateBridge __Hotfix_set_InterstellarTeleportRadius;
        private static DelegateBridge __Hotfix_get_TeleportTunnelEffectRadius;
        private static DelegateBridge __Hotfix_set_TeleportTunnelEffectRadius;
        private static DelegateBridge __Hotfix_get_InterstellarTeleportCDParam;
        private static DelegateBridge __Hotfix_set_InterstellarTeleportCDParam;
        private static DelegateBridge __Hotfix_get_HighSlotWeaponInfo;
        private static DelegateBridge __Hotfix_get_MiddleSlotEquipInfo;
        private static DelegateBridge __Hotfix_get_UnpackPrepareTime;
        private static DelegateBridge __Hotfix_set_UnpackPrepareTime;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_TeleportFatigueBufId;
        private static DelegateBridge __Hotfix_get_TeleportCDBufId;
        private static DelegateBridge __Hotfix_get_TeleportFootholdSceneId;
        private static DelegateBridge __Hotfix_set_TeleportFootholdSceneId;
        private static DelegateBridge __Hotfix_get_FakeFlagShipNpcMonsterId;
        private static DelegateBridge __Hotfix_set_FakeFlagShipNpcMonsterId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="EnergyPackageItemId", DataFormat=DataFormat.TwosComplement)]
        public int EnergyPackageItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EnergyPackageStoreSize", DataFormat=DataFormat.TwosComplement)]
        public int EnergyPackageStoreSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="InterstellarTeleportRadius", DataFormat=DataFormat.FixedSize)]
        public float InterstellarTeleportRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="TeleportTunnelEffectRadius", DataFormat=DataFormat.FixedSize)]
        public float TeleportTunnelEffectRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="InterstellarTeleportCDParam", DataFormat=DataFormat.FixedSize)]
        public float InterstellarTeleportCDParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="HighSlotWeaponInfo", DataFormat=DataFormat.TwosComplement)]
        public List<int> HighSlotWeaponInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="MiddleSlotEquipInfo", DataFormat=DataFormat.TwosComplement)]
        public List<int> MiddleSlotEquipInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="UnpackPrepareTime", DataFormat=DataFormat.TwosComplement)]
        public int UnpackPrepareTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="TeleportFatigueBufId", DataFormat=DataFormat.TwosComplement)]
        public List<int> TeleportFatigueBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="TeleportCDBufId", DataFormat=DataFormat.TwosComplement)]
        public List<int> TeleportCDBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="TeleportFootholdSceneId", DataFormat=DataFormat.TwosComplement)]
        public int TeleportFootholdSceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="FakeFlagShipNpcMonsterId", DataFormat=DataFormat.TwosComplement)]
        public int FakeFlagShipNpcMonsterId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

