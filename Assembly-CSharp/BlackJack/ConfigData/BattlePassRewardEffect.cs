﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassRewardEffect")]
    public class BattlePassRewardEffect : IExtensible
    {
        private int _IsUpgrade;
        private int _Index;
        private int _Visible;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsUpgrade;
        private static DelegateBridge __Hotfix_set_IsUpgrade;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_Visible;
        private static DelegateBridge __Hotfix_set_Visible;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsUpgrade", DataFormat=DataFormat.TwosComplement)]
        public int IsUpgrade
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Visible", DataFormat=DataFormat.TwosComplement)]
        public int Visible
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

