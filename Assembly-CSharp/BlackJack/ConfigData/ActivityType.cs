﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ActivityType")]
    public enum ActivityType
    {
        [ProtoEnum(Name="ActivityType_Invalid", Value=0)]
        ActivityType_Invalid = 0,
        [ProtoEnum(Name="ActivityType_DelegateFight", Value=1)]
        ActivityType_DelegateFight = 1,
        [ProtoEnum(Name="ActivityType_DelegateMineral", Value=2)]
        ActivityType_DelegateMineral = 2,
        [ProtoEnum(Name="ActivityType_DelegateTransport", Value=3)]
        ActivityType_DelegateTransport = 3,
        [ProtoEnum(Name="ActivityType_ManualQuest", Value=4)]
        ActivityType_ManualQuest = 4,
        [ProtoEnum(Name="ActivityType_DelegateInvade", Value=5)]
        ActivityType_DelegateInvade = 5,
        [ProtoEnum(Name="ActivityType_Wormhole", Value=6)]
        ActivityType_Wormhole = 6,
        [ProtoEnum(Name="ActivityType_RareWormhole", Value=7)]
        ActivityType_RareWormhole = 7,
        [ProtoEnum(Name="ActivityType_Infect", Value=8)]
        ActivityType_Infect = 8,
        [ProtoEnum(Name="ActivityType_ScienceExplore", Value=9)]
        ActivityType_ScienceExplore = 9,
        [ProtoEnum(Name="ActivityType_FactionQuest", Value=10)]
        ActivityType_FactionQuest = 10,
        [ProtoEnum(Name="ActivityType_PlayerTrade", Value=11)]
        ActivityType_PlayerTrade = 11,
        [ProtoEnum(Name="ActivityType_SpaceSignal", Value=12)]
        ActivityType_SpaceSignal = 12
    }
}

