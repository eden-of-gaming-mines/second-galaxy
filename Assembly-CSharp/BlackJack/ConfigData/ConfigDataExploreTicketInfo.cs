﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataExploreTicketInfo")]
    public class ConfigDataExploreTicketInfo : IExtensible
    {
        private int _ID;
        private int _DifficultLevelMin;
        private int _DifficultLevelMax;
        private int _Concentration;
        private int _MaxJump;
        private readonly List<IntWeightListItem> _LevelPool;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_DifficultLevelMin;
        private static DelegateBridge __Hotfix_set_DifficultLevelMin;
        private static DelegateBridge __Hotfix_get_DifficultLevelMax;
        private static DelegateBridge __Hotfix_set_DifficultLevelMax;
        private static DelegateBridge __Hotfix_get_Concentration;
        private static DelegateBridge __Hotfix_set_Concentration;
        private static DelegateBridge __Hotfix_get_MaxJump;
        private static DelegateBridge __Hotfix_set_MaxJump;
        private static DelegateBridge __Hotfix_get_LevelPool;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DifficultLevelMin", DataFormat=DataFormat.TwosComplement)]
        public int DifficultLevelMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DifficultLevelMax", DataFormat=DataFormat.TwosComplement)]
        public int DifficultLevelMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Concentration", DataFormat=DataFormat.TwosComplement)]
        public int Concentration
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="MaxJump", DataFormat=DataFormat.TwosComplement)]
        public int MaxJump
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="LevelPool", DataFormat=DataFormat.Default)]
        public List<IntWeightListItem> LevelPool
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

