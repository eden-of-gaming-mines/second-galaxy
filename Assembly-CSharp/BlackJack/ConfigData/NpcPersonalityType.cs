﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcPersonalityType")]
    public enum NpcPersonalityType
    {
        [ProtoEnum(Name="NpcPersonalityType_NONE", Value=0)]
        NpcPersonalityType_NONE = 0,
        [ProtoEnum(Name="NpcPersonalityType_Irritable", Value=1)]
        NpcPersonalityType_Irritable = 1,
        [ProtoEnum(Name="NpcPersonalityType_Modest", Value=2)]
        NpcPersonalityType_Modest = 2,
        [ProtoEnum(Name="NpcPersonalityType_Open", Value=3)]
        NpcPersonalityType_Open = 3,
        [ProtoEnum(Name="NpcPersonalityType_Serious", Value=4)]
        NpcPersonalityType_Serious = 4,
        [ProtoEnum(Name="NpcPersonalityType_Arrogant", Value=5)]
        NpcPersonalityType_Arrogant = 5
    }
}

