﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildBuildingType")]
    public enum GuildBuildingType
    {
        [ProtoEnum(Name="GuildBuildingType_None", Value=0)]
        GuildBuildingType_None = 0,
        [ProtoEnum(Name="GuildBuildingType_GuildHQ", Value=1)]
        GuildBuildingType_GuildHQ = 1,
        [ProtoEnum(Name="GuildBuildingType_GuildMiningStation", Value=2)]
        GuildBuildingType_GuildMiningStation = 2,
        [ProtoEnum(Name="GuildBuildingType_GuildFactroy", Value=3)]
        GuildBuildingType_GuildFactroy = 3,
        [ProtoEnum(Name="GuildBuildingType_GuildSentry", Value=4)]
        GuildBuildingType_GuildSentry = 4,
        [ProtoEnum(Name="GuildBuildingType_BuildingShieldDestabiliser", Value=5)]
        GuildBuildingType_BuildingShieldDestabiliser = 5,
        [ProtoEnum(Name="GuildBuildingType_ControllShieldStabiliser1", Value=6)]
        GuildBuildingType_ControllShieldStabiliser1 = 6,
        [ProtoEnum(Name="GuildBuildingType_ControllShieldStabiliser2", Value=7)]
        GuildBuildingType_ControllShieldStabiliser2 = 7,
        [ProtoEnum(Name="GuildBuildingType_ControllShieldStabiliser3", Value=8)]
        GuildBuildingType_ControllShieldStabiliser3 = 8,
        [ProtoEnum(Name="GuildBuildingType_GuildFlagShipHangar", Value=9)]
        GuildBuildingType_GuildFlagShipHangar = 9,
        [ProtoEnum(Name="GuildBuildingType_GuildTradePort", Value=10)]
        GuildBuildingType_GuildTradePort = 10,
        [ProtoEnum(Name="GuildBuildingType_GuildTeleportStation", Value=11)]
        GuildBuildingType_GuildTeleportStation = 11,
        [ProtoEnum(Name="GuildBuildingType_GuildAntiTeleportStation", Value=12)]
        GuildBuildingType_GuildAntiTeleportStation = 12,
        [ProtoEnum(Name="GuildBuildingType_Max", Value=13)]
        GuildBuildingType_Max = 13
    }
}

