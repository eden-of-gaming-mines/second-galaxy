﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CostType")]
    public enum CostType
    {
        [ProtoEnum(Name="CostType_Currency", Value=1)]
        CostType_Currency = 1,
        [ProtoEnum(Name="CostType_Item", Value=2)]
        CostType_Item = 2
    }
}

