﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DynamicSceneLocationInfoOrbitRadiusRange")]
    public class DynamicSceneLocationInfoOrbitRadiusRange : IExtensible
    {
        private ulong _MinOrbitRadius;
        private ulong _MaxOrbitRadius;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MinOrbitRadius;
        private static DelegateBridge __Hotfix_set_MinOrbitRadius;
        private static DelegateBridge __Hotfix_get_MaxOrbitRadius;
        private static DelegateBridge __Hotfix_set_MaxOrbitRadius;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MinOrbitRadius", DataFormat=DataFormat.TwosComplement)]
        public ulong MinOrbitRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="MaxOrbitRadius", DataFormat=DataFormat.TwosComplement)]
        public ulong MaxOrbitRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

