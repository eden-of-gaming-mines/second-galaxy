﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BufType")]
    public enum BufType
    {
        [ProtoEnum(Name="BufType_PropertiesModify", Value=1)]
        BufType_PropertiesModify = 1,
        [ProtoEnum(Name="BufType_DOTShield", Value=2)]
        BufType_DOTShield = 2,
        [ProtoEnum(Name="BufType_DOTArmor", Value=3)]
        BufType_DOTArmor = 3,
        [ProtoEnum(Name="BufType_DOTEnergyPercent", Value=4)]
        BufType_DOTEnergyPercent = 4,
        [ProtoEnum(Name="BufType_DOTSuperWeaponEnergy", Value=5)]
        BufType_DOTSuperWeaponEnergy = 5,
        [ProtoEnum(Name="BufType_HOTShield", Value=6)]
        BufType_HOTShield = 6,
        [ProtoEnum(Name="BufType_HOTArmor", Value=7)]
        BufType_HOTArmor = 7,
        [ProtoEnum(Name="BufType_HOTEnergy", Value=8)]
        BufType_HOTEnergy = 8,
        [ProtoEnum(Name="BufType_HOTSuperWeaponEnergy", Value=9)]
        BufType_HOTSuperWeaponEnergy = 9,
        [ProtoEnum(Name="BufType_ShieldEx", Value=10)]
        BufType_ShieldEx = 10,
        [ProtoEnum(Name="BufType_ArmorEx", Value=11)]
        BufType_ArmorEx = 11,
        [ProtoEnum(Name="BufType_ShieldProtect", Value=12)]
        BufType_ShieldProtect = 12,
        [ProtoEnum(Name="BufType_ArmorProtect", Value=13)]
        BufType_ArmorProtect = 13,
        [ProtoEnum(Name="BufType_WeaponEquipCDSpeedUpByKillTarget", Value=14)]
        BufType_WeaponEquipCDSpeedUpByKillTarget = 14,
        [ProtoEnum(Name="BufType_EndByHighSlotLaunch", Value=15)]
        BufType_EndByHighSlotLaunch = 15,
        [ProtoEnum(Name="BufType_EndByHighSlotAndSuperWeaponEquipLaunch", Value=0x10)]
        BufType_EndByHighSlotAndSuperWeaponEquipLaunch = 0x10,
        [ProtoEnum(Name="BufType_EndByHighAndMiddleSlotLaunch", Value=0x11)]
        BufType_EndByHighAndMiddleSlotLaunch = 0x11,
        [ProtoEnum(Name="BufType_DOTShieldPercent", Value=0x12)]
        BufType_DOTShieldPercent = 0x12,
        [ProtoEnum(Name="BufType_DOTArmorPercent", Value=0x13)]
        BufType_DOTArmorPercent = 0x13,
        [ProtoEnum(Name="BufType_HOTShieldPercent", Value=20)]
        BufType_HOTShieldPercent = 20,
        [ProtoEnum(Name="BufType_HOTArmorPercent", Value=0x15)]
        BufType_HOTArmorPercent = 0x15,
        [ProtoEnum(Name="BufType_HOTEnergyPercent", Value=0x16)]
        BufType_HOTEnergyPercent = 0x16,
        [ProtoEnum(Name="BufType_DOTShieldAndArmor", Value=0x17)]
        BufType_DOTShieldAndArmor = 0x17,
        [ProtoEnum(Name="BufType_DOTShieldAndArmorPercent", Value=0x18)]
        BufType_DOTShieldAndArmorPercent = 0x18,
        [ProtoEnum(Name="BufType_ShipAnimation", Value=0x19)]
        BufType_ShipAnimation = 0x19,
        [ProtoEnum(Name="BufType_LimitDamage", Value=0x1a)]
        BufType_LimitDamage = 0x1a,
        [ProtoEnum(Name="BufType_PureDisplayEffect", Value=0x1b)]
        BufType_PureDisplayEffect = 0x1b
    }
}

