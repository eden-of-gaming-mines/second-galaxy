﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSovereignMaintenanceCostLevelInfo")]
    public class ConfigDataSovereignMaintenanceCostLevelInfo : IExtensible
    {
        private int _ID;
        private string _LevelDescColor;
        private string _LevelDescStrKey;
        private string _LevelDescForTitleStrKey;
        private string _Type1DescStrKey;
        private string _Type1DetailStrKey;
        private string _Type2DescStrKey;
        private string _Type2DetailStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LevelDescColor;
        private static DelegateBridge __Hotfix_set_LevelDescColor;
        private static DelegateBridge __Hotfix_get_LevelDescStrKey;
        private static DelegateBridge __Hotfix_set_LevelDescStrKey;
        private static DelegateBridge __Hotfix_get_LevelDescForTitleStrKey;
        private static DelegateBridge __Hotfix_set_LevelDescForTitleStrKey;
        private static DelegateBridge __Hotfix_get_Type1DescStrKey;
        private static DelegateBridge __Hotfix_set_Type1DescStrKey;
        private static DelegateBridge __Hotfix_get_Type1DetailStrKey;
        private static DelegateBridge __Hotfix_set_Type1DetailStrKey;
        private static DelegateBridge __Hotfix_get_Type2DescStrKey;
        private static DelegateBridge __Hotfix_set_Type2DescStrKey;
        private static DelegateBridge __Hotfix_get_Type2DetailStrKey;
        private static DelegateBridge __Hotfix_set_Type2DetailStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LevelDescColor", DataFormat=DataFormat.Default)]
        public string LevelDescColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="LevelDescStrKey", DataFormat=DataFormat.Default)]
        public string LevelDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="LevelDescForTitleStrKey", DataFormat=DataFormat.Default)]
        public string LevelDescForTitleStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="Type1DescStrKey", DataFormat=DataFormat.Default)]
        public string Type1DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="Type1DetailStrKey", DataFormat=DataFormat.Default)]
        public string Type1DetailStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="Type2DescStrKey", DataFormat=DataFormat.Default)]
        public string Type2DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="Type2DetailStrKey", DataFormat=DataFormat.Default)]
        public string Type2DetailStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

