﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataEquipEffectVirtualBufInfo")]
    public class ConfigDataEquipEffectVirtualBufInfo : IExtensible
    {
        private int _ID;
        private readonly List<ParamInfo> _BufDescParams;
        private ShipFightBufType _BufType;
        private bool _IsEnhance;
        private string _BufNameStrKey;
        private string _BufDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BufDescParams;
        private static DelegateBridge __Hotfix_get_BufType;
        private static DelegateBridge __Hotfix_set_BufType;
        private static DelegateBridge __Hotfix_get_IsEnhance;
        private static DelegateBridge __Hotfix_set_IsEnhance;
        private static DelegateBridge __Hotfix_get_BufNameStrKey;
        private static DelegateBridge __Hotfix_set_BufNameStrKey;
        private static DelegateBridge __Hotfix_get_BufDescStrKey;
        private static DelegateBridge __Hotfix_set_BufDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="BufDescParams", DataFormat=DataFormat.Default)]
        public List<ParamInfo> BufDescParams
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BufType", DataFormat=DataFormat.TwosComplement)]
        public ShipFightBufType BufType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="IsEnhance", DataFormat=DataFormat.Default)]
        public bool IsEnhance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="BufNameStrKey", DataFormat=DataFormat.Default)]
        public string BufNameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BufDescStrKey", DataFormat=DataFormat.Default)]
        public string BufDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

