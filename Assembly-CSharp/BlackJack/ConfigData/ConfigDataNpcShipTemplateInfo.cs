﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcShipTemplateInfo")]
    public class ConfigDataNpcShipTemplateInfo : IExtensible
    {
        private int _ID;
        private int _ShipConfId;
        private readonly List<ConfHighSlotInfo> _HighSlotInfo;
        private readonly List<int> _MiddleSlotInfo;
        private readonly List<int> _LowSlot;
        private int _DefaultBoosterEquipConfId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShipConfId;
        private static DelegateBridge __Hotfix_set_ShipConfId;
        private static DelegateBridge __Hotfix_get_HighSlotInfo;
        private static DelegateBridge __Hotfix_get_MiddleSlotInfo;
        private static DelegateBridge __Hotfix_get_LowSlot;
        private static DelegateBridge __Hotfix_get_DefaultBoosterEquipConfId;
        private static DelegateBridge __Hotfix_set_DefaultBoosterEquipConfId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipConfId", DataFormat=DataFormat.TwosComplement)]
        public int ShipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="HighSlotInfo", DataFormat=DataFormat.Default)]
        public List<ConfHighSlotInfo> HighSlotInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="MiddleSlotInfo", DataFormat=DataFormat.TwosComplement)]
        public List<int> MiddleSlotInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="LowSlot", DataFormat=DataFormat.TwosComplement)]
        public List<int> LowSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="DefaultBoosterEquipConfId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultBoosterEquipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

