﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="UserGuideStepInfoPageList")]
    public class UserGuideStepInfoPageList : IExtensible
    {
        private int _NpcTalkerId;
        private int _DialogID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NpcTalkerId;
        private static DelegateBridge __Hotfix_set_NpcTalkerId;
        private static DelegateBridge __Hotfix_get_DialogID;
        private static DelegateBridge __Hotfix_set_DialogID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="NpcTalkerId", DataFormat=DataFormat.TwosComplement)]
        public int NpcTalkerId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DialogID", DataFormat=DataFormat.TwosComplement)]
        public int DialogID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

