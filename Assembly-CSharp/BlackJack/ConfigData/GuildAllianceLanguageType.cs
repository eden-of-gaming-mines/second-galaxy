﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildAllianceLanguageType")]
    public enum GuildAllianceLanguageType
    {
        [ProtoEnum(Name="GuildAllianceLanguageType_Invalid", Value=0)]
        GuildAllianceLanguageType_Invalid = 0,
        [ProtoEnum(Name="GuildAllianceLanguageType_English", Value=1)]
        GuildAllianceLanguageType_English = 1,
        [ProtoEnum(Name="GuildAllianceLanguageType_Chinese", Value=2)]
        GuildAllianceLanguageType_Chinese = 2,
        [ProtoEnum(Name="GuildAllianceLanguageType_Russian", Value=3)]
        GuildAllianceLanguageType_Russian = 3,
        [ProtoEnum(Name="GuildAllianceLanguageType_Japanese", Value=4)]
        GuildAllianceLanguageType_Japanese = 4,
        [ProtoEnum(Name="GuildAllianceLanguageType_French", Value=5)]
        GuildAllianceLanguageType_French = 5,
        [ProtoEnum(Name="GuildAllianceLanguageType_German", Value=6)]
        GuildAllianceLanguageType_German = 6,
        [ProtoEnum(Name="GuildAllianceLanguageType_Portuguese", Value=7)]
        GuildAllianceLanguageType_Portuguese = 7,
        [ProtoEnum(Name="GuildAllianceLanguageType_Spanish", Value=8)]
        GuildAllianceLanguageType_Spanish = 8,
        [ProtoEnum(Name="GuildAllianceLanguageType_TaiWan", Value=9)]
        GuildAllianceLanguageType_TaiWan = 9,
        [ProtoEnum(Name="GuildAllianceLanguageType_Max", Value=10)]
        GuildAllianceLanguageType_Max = 10
    }
}

