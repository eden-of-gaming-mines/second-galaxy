﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipAssemblyOptimizingInfo")]
    public class ConfigDataShipAssemblyOptimizingInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.ShipType _ShipType;
        private ShipSizeType _RationalWeaponSize;
        private readonly List<RationalEquipType> _RationalEquipTypeList;
        private readonly List<ShipSizeType> _RationalEquipSizeList;
        private string _WeaponStrKey;
        private string _EquipStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShipType;
        private static DelegateBridge __Hotfix_set_ShipType;
        private static DelegateBridge __Hotfix_get_RationalWeaponSize;
        private static DelegateBridge __Hotfix_set_RationalWeaponSize;
        private static DelegateBridge __Hotfix_get_RationalEquipTypeList;
        private static DelegateBridge __Hotfix_get_RationalEquipSizeList;
        private static DelegateBridge __Hotfix_get_WeaponStrKey;
        private static DelegateBridge __Hotfix_set_WeaponStrKey;
        private static DelegateBridge __Hotfix_get_EquipStrKey;
        private static DelegateBridge __Hotfix_set_EquipStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.ShipType ShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="RationalWeaponSize", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType RationalWeaponSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="RationalEquipTypeList", DataFormat=DataFormat.Default)]
        public List<RationalEquipType> RationalEquipTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="RationalEquipSizeList", DataFormat=DataFormat.TwosComplement)]
        public List<ShipSizeType> RationalEquipSizeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="WeaponStrKey", DataFormat=DataFormat.Default)]
        public string WeaponStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="EquipStrKey", DataFormat=DataFormat.Default)]
        public string EquipStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

