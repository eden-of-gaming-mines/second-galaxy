﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGiftPackageInfo")]
    public class ConfigDataGiftPackageInfo : IExtensible
    {
        private int _ID;
        private GiftPackageCategory _Category;
        private GiftPackageTheme _Theme;
        private int _UIPriority;
        private int _Discount;
        private int _Price;
        private string _OnShelfTime;
        private string _OffShelfTime;
        private GiftPackageBuyCountLimitType _BuyCountLimitType;
        private int _BuyCountLimit;
        private int _PreGiftPackageId;
        private readonly List<int> _FeatureList;
        private readonly List<int> _GiftPackageContentDesc;
        private readonly List<QuestRewardInfo> _GiftPackageContent;
        private readonly List<QuestRewardInfo> _GiftPackageContent4Display;
        private int _PackageValue;
        private string _IconResPath;
        private string _Desc;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_Theme;
        private static DelegateBridge __Hotfix_set_Theme;
        private static DelegateBridge __Hotfix_get_UIPriority;
        private static DelegateBridge __Hotfix_set_UIPriority;
        private static DelegateBridge __Hotfix_get_Discount;
        private static DelegateBridge __Hotfix_set_Discount;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_OnShelfTime;
        private static DelegateBridge __Hotfix_set_OnShelfTime;
        private static DelegateBridge __Hotfix_get_OffShelfTime;
        private static DelegateBridge __Hotfix_set_OffShelfTime;
        private static DelegateBridge __Hotfix_get_BuyCountLimitType;
        private static DelegateBridge __Hotfix_set_BuyCountLimitType;
        private static DelegateBridge __Hotfix_get_BuyCountLimit;
        private static DelegateBridge __Hotfix_set_BuyCountLimit;
        private static DelegateBridge __Hotfix_get_PreGiftPackageId;
        private static DelegateBridge __Hotfix_set_PreGiftPackageId;
        private static DelegateBridge __Hotfix_get_FeatureList;
        private static DelegateBridge __Hotfix_get_GiftPackageContentDesc;
        private static DelegateBridge __Hotfix_get_GiftPackageContent;
        private static DelegateBridge __Hotfix_get_GiftPackageContent4Display;
        private static DelegateBridge __Hotfix_get_PackageValue;
        private static DelegateBridge __Hotfix_set_PackageValue;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public GiftPackageCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Theme", DataFormat=DataFormat.TwosComplement)]
        public GiftPackageTheme Theme
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="UIPriority", DataFormat=DataFormat.TwosComplement)]
        public int UIPriority
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Discount", DataFormat=DataFormat.TwosComplement)]
        public int Discount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public int Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="OnShelfTime", DataFormat=DataFormat.Default)]
        public string OnShelfTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="OffShelfTime", DataFormat=DataFormat.Default)]
        public string OffShelfTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="BuyCountLimitType", DataFormat=DataFormat.TwosComplement)]
        public GiftPackageBuyCountLimitType BuyCountLimitType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="BuyCountLimit", DataFormat=DataFormat.TwosComplement)]
        public int BuyCountLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="PreGiftPackageId", DataFormat=DataFormat.TwosComplement)]
        public int PreGiftPackageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, Name="FeatureList", DataFormat=DataFormat.TwosComplement)]
        public List<int> FeatureList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, Name="GiftPackageContentDesc", DataFormat=DataFormat.TwosComplement)]
        public List<int> GiftPackageContentDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, Name="GiftPackageContent", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> GiftPackageContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, Name="GiftPackageContent4Display", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> GiftPackageContent4Display
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="PackageValue", DataFormat=DataFormat.TwosComplement)]
        public int PackageValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

