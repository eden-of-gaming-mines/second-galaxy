﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcMonsterLevelDropInfo")]
    public class ConfigDataNpcMonsterLevelDropInfo : IExtensible
    {
        private int _ID;
        private int _StartLevel;
        private int _EndLevel;
        private int _DropId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_StartLevel;
        private static DelegateBridge __Hotfix_set_StartLevel;
        private static DelegateBridge __Hotfix_get_EndLevel;
        private static DelegateBridge __Hotfix_set_EndLevel;
        private static DelegateBridge __Hotfix_get_DropId;
        private static DelegateBridge __Hotfix_set_DropId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StartLevel", DataFormat=DataFormat.TwosComplement)]
        public int StartLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EndLevel", DataFormat=DataFormat.TwosComplement)]
        public int EndLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="DropId", DataFormat=DataFormat.TwosComplement)]
        public int DropId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

