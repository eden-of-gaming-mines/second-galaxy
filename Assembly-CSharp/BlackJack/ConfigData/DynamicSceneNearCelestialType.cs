﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DynamicSceneNearCelestialType")]
    public enum DynamicSceneNearCelestialType
    {
        [ProtoEnum(Name="DynamicSceneNearCelestialType_None", Value=0)]
        DynamicSceneNearCelestialType_None = 0,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_CloseTo", Value=1)]
        DynamicSceneNearCelestialType_CloseTo = 1,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_Near", Value=2)]
        DynamicSceneNearCelestialType_Near = 2,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_CloseToSunSide", Value=3)]
        DynamicSceneNearCelestialType_CloseToSunSide = 3,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_CloseToDawnSide", Value=4)]
        DynamicSceneNearCelestialType_CloseToDawnSide = 4,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_CloseToShadowSide", Value=5)]
        DynamicSceneNearCelestialType_CloseToShadowSide = 5,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_NearSunSide", Value=6)]
        DynamicSceneNearCelestialType_NearSunSide = 6,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_NearDawnSide", Value=7)]
        DynamicSceneNearCelestialType_NearDawnSide = 7,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_NearShadowSide", Value=8)]
        DynamicSceneNearCelestialType_NearShadowSide = 8,
        [ProtoEnum(Name="DynamicSceneNearCelestialType_NearMoon", Value=9)]
        DynamicSceneNearCelestialType_NearMoon = 9
    }
}

