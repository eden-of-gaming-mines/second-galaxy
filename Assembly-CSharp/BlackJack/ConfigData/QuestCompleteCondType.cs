﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestCompleteCondType")]
    public enum QuestCompleteCondType
    {
        [ProtoEnum(Name="QuestCompleteCondType_KillMonster", Value=1)]
        QuestCompleteCondType_KillMonster = 1,
        [ProtoEnum(Name="QuestCompleteCondType_CommitItem", Value=2)]
        QuestCompleteCondType_CommitItem = 2,
        [ProtoEnum(Name="QuestCompleteCondType_NpcDialog", Value=3)]
        QuestCompleteCondType_NpcDialog = 3,
        [ProtoEnum(Name="QuestCompleteCondType_SceneEvent", Value=4)]
        QuestCompleteCondType_SceneEvent = 4,
        [ProtoEnum(Name="QuestCompleteCondType_Level", Value=5)]
        QuestCompleteCondType_Level = 5,
        [ProtoEnum(Name="QuestCompleteCondType_Tech", Value=6)]
        QuestCompleteCondType_Tech = 6,
        [ProtoEnum(Name="QuestCompleteCondType_ProduceItem", Value=7)]
        QuestCompleteCondType_ProduceItem = 7,
        [ProtoEnum(Name="QuestCompleteCondType_TimeOut", Value=8)]
        QuestCompleteCondType_TimeOut = 8,
        [ProtoEnum(Name="QuestCompleteCondType_TimeDelay", Value=9)]
        QuestCompleteCondType_TimeDelay = 9,
        [ProtoEnum(Name="QuestCompleteCondType_QuestComplete", Value=10)]
        QuestCompleteCondType_QuestComplete = 10,
        [ProtoEnum(Name="QuestCompleteCondType_ShipWeaponEquipSlotSet", Value=11)]
        QuestCompleteCondType_ShipWeaponEquipSlotSet = 11,
        [ProtoEnum(Name="QuestCompleteCondType_ShipAmmoSet", Value=12)]
        QuestCompleteCondType_ShipAmmoSet = 12,
        [ProtoEnum(Name="QuestCompleteCondType_UnpackShipInHangar", Value=15)]
        QuestCompleteCondType_UnpackShipInHangar = 15,
        [ProtoEnum(Name="QuestCompleteCondType_FreeSignalQuestComplete", Value=0x10)]
        QuestCompleteCondType_FreeSignalQuestComplete = 0x10,
        [ProtoEnum(Name="QuestCompleteCondType_SkillLearn", Value=0x11)]
        QuestCompleteCondType_SkillLearn = 0x11,
        [ProtoEnum(Name="QuestCompleteCondType_PropertiesPointAdd", Value=0x12)]
        QuestCompleteCondType_PropertiesPointAdd = 0x12,
        [ProtoEnum(Name="QuestCompleteCondType_DrivingLicenseUpgrade", Value=0x13)]
        QuestCompleteCondType_DrivingLicenseUpgrade = 0x13,
        [ProtoEnum(Name="QuestCompleteCondType_CaptainLevelup", Value=20)]
        QuestCompleteCondType_CaptainLevelup = 20,
        [ProtoEnum(Name="QuestCompleteCondType_PossessOfCaptain", Value=0x15)]
        QuestCompleteCondType_PossessOfCaptain = 0x15,
        [ProtoEnum(Name="QuestCompleteCondType_CaptainChallengeComplete", Value=0x16)]
        QuestCompleteCondType_CaptainChallengeComplete = 0x16,
        [ProtoEnum(Name="QuestCompleteCondType_DelegateSignalWithCaptain", Value=0x17)]
        QuestCompleteCondType_DelegateSignalWithCaptain = 0x17,
        [ProtoEnum(Name="QuestCompleteCondType_ChipSet", Value=0x18)]
        QuestCompleteCondType_ChipSet = 0x18,
        [ProtoEnum(Name="QuestCompleteCondType_CaptainShipUnlockShip", Value=0x19)]
        QuestCompleteCondType_CaptainShipUnlockShip = 0x19,
        [ProtoEnum(Name="QuestCompleteCondType_NpcShopPurchase", Value=0x1a)]
        QuestCompleteCondType_NpcShopPurchase = 0x1a,
        [ProtoEnum(Name="QuestCompleteCondType_CrackComplete", Value=0x1b)]
        QuestCompleteCondType_CrackComplete = 0x1b,
        [ProtoEnum(Name="QuestCompleteCondType_TechUpgrade", Value=0x1c)]
        QuestCompleteCondType_TechUpgrade = 0x1c,
        [ProtoEnum(Name="QuestCompleteCondType_ProduceComplete", Value=0x1d)]
        QuestCompleteCondType_ProduceComplete = 0x1d,
        [ProtoEnum(Name="QuestCompleteCondType_InfectQuestComplete", Value=30)]
        QuestCompleteCondType_InfectQuestComplete = 30,
        [ProtoEnum(Name="QuestCompleteCondType_BlueprintDevelopment", Value=0x1f)]
        QuestCompleteCondType_BlueprintDevelopment = 0x1f,
        [ProtoEnum(Name="QuestCompleteCondType_JoinGuild", Value=0x20)]
        QuestCompleteCondType_JoinGuild = 0x20,
        [ProtoEnum(Name="QuestCompleteCondType_JoinUnion", Value=0x21)]
        QuestCompleteCondType_JoinUnion = 0x21,
        [ProtoEnum(Name="QuestCompleteCondType_WormholeComplete", Value=0x22)]
        QuestCompleteCondType_WormholeComplete = 0x22,
        [ProtoEnum(Name="QuestCompleteCondType_CaptainFeatsLearn", Value=0x23)]
        QuestCompleteCondType_CaptainFeatsLearn = 0x23,
        [ProtoEnum(Name="QuestCompleteCondType_InvadeComplete", Value=0x24)]
        QuestCompleteCondType_InvadeComplete = 0x24,
        [ProtoEnum(Name="QuestCompleteCondType_PropertiesPointAddCount", Value=0x25)]
        QuestCompleteCondType_PropertiesPointAddCount = 0x25,
        [ProtoEnum(Name="QuestCompleteCondType_PuzzleGameComplete", Value=0x26)]
        QuestCompleteCondType_PuzzleGameComplete = 0x26,
        [ProtoEnum(Name="QuestCompleteCondType_ScienceExploreQuestComplete", Value=0x27)]
        QuestCompleteCondType_ScienceExploreQuestComplete = 0x27,
        [ProtoEnum(Name="QuestCompleteCondType_SolarSystemScanPlanetCount", Value=40)]
        QuestCompleteCondType_SolarSystemScanPlanetCount = 40,
        [ProtoEnum(Name="QuestCompleteCondType_SolarSystemScanIsInfected", Value=0x29)]
        QuestCompleteCondType_SolarSystemScanIsInfected = 0x29,
        [ProtoEnum(Name="QuestCompleteCondType_StarScanStarRadius", Value=0x2a)]
        QuestCompleteCondType_StarScanStarRadius = 0x2a,
        [ProtoEnum(Name="QuestCompleteCondType_StarScanStarAge", Value=0x2b)]
        QuestCompleteCondType_StarScanStarAge = 0x2b,
        [ProtoEnum(Name="QuestCompleteCondType_PlanetScanPlanetType", Value=0x2c)]
        QuestCompleteCondType_PlanetScanPlanetType = 0x2c,
        [ProtoEnum(Name="QuestCompleteCondType_PlanetScanPlanetMass", Value=0x2d)]
        QuestCompleteCondType_PlanetScanPlanetMass = 0x2d,
        [ProtoEnum(Name="QuestCompleteCondType_PlanetScanPlanetGravity", Value=0x2e)]
        QuestCompleteCondType_PlanetScanPlanetGravity = 0x2e,
        [ProtoEnum(Name="QuestCompleteCondType_OwnItemCount", Value=0x2f)]
        QuestCompleteCondType_OwnItemCount = 0x2f,
        [ProtoEnum(Name="QuestCompleteCondType_FactionCredit", Value=0x30)]
        QuestCompleteCondType_FactionCredit = 0x30,
        [ProtoEnum(Name="QuestCompleteCondType_EnterSolarSystemCount", Value=0x31)]
        QuestCompleteCondType_EnterSolarSystemCount = 0x31,
        [ProtoEnum(Name="QuestCompleteCondType_EnterWormholeSolarSystem", Value=50)]
        QuestCompleteCondType_EnterWormholeSolarSystem = 50,
        [ProtoEnum(Name="QuestCompleteCondType_QuestInListComplete", Value=0x33)]
        QuestCompleteCondType_QuestInListComplete = 0x33,
        [ProtoEnum(Name="QuestCompleteCondType_EnterSpaceStation", Value=0x34)]
        QuestCompleteCondType_EnterSpaceStation = 0x34,
        [ProtoEnum(Name="QuestCompleteCondType_QuestCompletedHistory", Value=0x35)]
        QuestCompleteCondType_QuestCompletedHistory = 0x35,
        [ProtoEnum(Name="QuestCompleteCondType_ProduceItemType", Value=0x36)]
        QuestCompleteCondType_ProduceItemType = 0x36,
        [ProtoEnum(Name="QuestCompleteCondType_CrackCompleteSubRank", Value=0x37)]
        QuestCompleteCondType_CrackCompleteSubRank = 0x37,
        [ProtoEnum(Name="QuestCompleteCondType_FreeSignalQuestCompleteByShip", Value=0x38)]
        QuestCompleteCondType_FreeSignalQuestCompleteByShip = 0x38,
        [ProtoEnum(Name="QuestCompleteCondType_ScienceExploreQuestCompleteLevel", Value=0x39)]
        QuestCompleteCondType_ScienceExploreQuestCompleteLevel = 0x39,
        [ProtoEnum(Name="QuestCompleteCondType_FactionQuestComplete", Value=0x3a)]
        QuestCompleteCondType_FactionQuestComplete = 0x3a,
        [ProtoEnum(Name="QuestCompleteCondType_WormholeCompleteByShip", Value=0x3b)]
        QuestCompleteCondType_WormholeCompleteByShip = 0x3b,
        [ProtoEnum(Name="QuestCompleteCondType_KillMonsterInWormhole", Value=60)]
        QuestCompleteCondType_KillMonsterInWormhole = 60,
        [ProtoEnum(Name="QuestCompleteCondType_KillPlayerInWormhole", Value=0x3d)]
        QuestCompleteCondType_KillPlayerInWormhole = 0x3d,
        [ProtoEnum(Name="QuestCompleteCondType_EnterWormholeDarkRoom", Value=0x3e)]
        QuestCompleteCondType_EnterWormholeDarkRoom = 0x3e,
        [ProtoEnum(Name="QuestCompleteCondType_PickCrackBoxInWormhole", Value=0x3f)]
        QuestCompleteCondType_PickCrackBoxInWormhole = 0x3f,
        [ProtoEnum(Name="QuestCompleteCondType_TradeNpcSalePrice", Value=0x40)]
        QuestCompleteCondType_TradeNpcSalePrice = 0x40,
        [ProtoEnum(Name="QuestCompleteCondType_TradeNpcSale", Value=0x41)]
        QuestCompleteCondType_TradeNpcSale = 0x41,
        [ProtoEnum(Name="QuestCompleteCondType_GuildDonate", Value=0x42)]
        QuestCompleteCondType_GuildDonate = 0x42,
        [ProtoEnum(Name="QuestCompleteCondType_GuildMessageWrite", Value=0x43)]
        QuestCompleteCondType_GuildMessageWrite = 0x43,
        [ProtoEnum(Name="QuestCompleteCondType_Navigation", Value=0x44)]
        QuestCompleteCondType_Navigation = 0x44,
        [ProtoEnum(Name="QuestCompleteCondType_AuctionSale", Value=0x45)]
        QuestCompleteCondType_AuctionSale = 0x45,
        [ProtoEnum(Name="QuestCompleteCondType_TechUpgradeSpeedUp", Value=70)]
        QuestCompleteCondType_TechUpgradeSpeedUp = 70,
        [ProtoEnum(Name="QuestCompleteCondType_Chat", Value=0x47)]
        QuestCompleteCondType_Chat = 0x47,
        [ProtoEnum(Name="QuestCompleteCondType_EnterDifferentSpaceStation", Value=0x48)]
        QuestCompleteCondType_EnterDifferentSpaceStation = 0x48,
        [ProtoEnum(Name="QuestCompleteCondType_ActiveDays", Value=0x49)]
        QuestCompleteCondType_ActiveDays = 0x49,
        [ProtoEnum(Name="QuestCompleteCondType_SceneTypeEvent", Value=0x4a)]
        QuestCompleteCondType_SceneTypeEvent = 0x4a,
        [ProtoEnum(Name="QuestCompleteCondType_TechInList", Value=0x4b)]
        QuestCompleteCondType_TechInList = 0x4b
    }
}

