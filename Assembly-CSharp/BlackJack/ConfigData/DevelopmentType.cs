﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DevelopmentType")]
    public enum DevelopmentType
    {
        [ProtoEnum(Name="DevelopmentType_Rapid", Value=1)]
        DevelopmentType_Rapid = 1,
        [ProtoEnum(Name="DevelopmentType_Stagewise", Value=2)]
        DevelopmentType_Stagewise = 2,
        [ProtoEnum(Name="DevelopmentType_Spiral", Value=3)]
        DevelopmentType_Spiral = 3,
        [ProtoEnum(Name="DevelopmentType_Fountain", Value=4)]
        DevelopmentType_Fountain = 4,
        [ProtoEnum(Name="DevelopmentType_Evolutionary", Value=5)]
        DevelopmentType_Evolutionary = 5,
        [ProtoEnum(Name="DevelopmentType_4GL", Value=6)]
        DevelopmentType_4GL = 6,
        [ProtoEnum(Name="DevelopmentType_Max", Value=7)]
        DevelopmentType_Max = 7
    }
}

