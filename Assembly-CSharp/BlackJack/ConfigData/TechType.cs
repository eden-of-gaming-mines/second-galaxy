﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="TechType")]
    public enum TechType
    {
        [ProtoEnum(Name="TechType_Ship", Value=1)]
        TechType_Ship = 1,
        [ProtoEnum(Name="TechType_Weapon", Value=2)]
        TechType_Weapon = 2,
        [ProtoEnum(Name="TechType_Engineering", Value=3)]
        TechType_Engineering = 3,
        [ProtoEnum(Name="TechType_Electronics", Value=4)]
        TechType_Electronics = 4,
        [ProtoEnum(Name="TechType_Produce", Value=5)]
        TechType_Produce = 5,
        [ProtoEnum(Name="TechType_HiredCaptain", Value=6)]
        TechType_HiredCaptain = 6
    }
}

