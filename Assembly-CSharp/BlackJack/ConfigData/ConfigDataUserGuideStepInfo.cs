﻿namespace BlackJack.ConfigData
{
    using BlackJack.ToolUtil;
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataUserGuideStepInfo")]
    public class ConfigDataUserGuideStepInfo : IExtensible, IUserGuideStepInfo
    {
        private int _ID;
        private string _UserGuideStepName;
        private bool _IsTrigerPoint;
        private readonly List<UserGuideConditionInfo> _ConditionList;
        private readonly List<ExtraUserGuideConditionInfo> _ExtraConditionList;
        private readonly List<UserGuideConditionInfo> _SkipConditionList;
        private int _GroupId;
        private bool _IsSavePoint;
        private int _NextStepId;
        private readonly List<UserGuideStepInfoPageList> _PageList;
        private bool _UseNPCChat;
        private int _NPCChatDummyIndex;
        private float _NpcChatOffsetX;
        private float _NpcChatOffsetY;
        private string _OpAreaHierarchyPath;
        private bool _ShowOpreationAreaEffect;
        private string _OpreationAreaEffectMode;
        private bool _ShowOpreationNoticeEffect;
        private bool _EndStepWithOperatorAreaClick;
        private bool _ShowMaskPanel;
        private float _DelayTime;
        private float _AutoStopTime;
        private string _CustomLayerAssetPath;
        private readonly List<string> _CustomLayerCopyPathList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_UserGuideStepName;
        private static DelegateBridge __Hotfix_set_UserGuideStepName;
        private static DelegateBridge __Hotfix_get_IsTrigerPoint;
        private static DelegateBridge __Hotfix_set_IsTrigerPoint;
        private static DelegateBridge __Hotfix_get_ConditionList;
        private static DelegateBridge __Hotfix_get_ExtraConditionList;
        private static DelegateBridge __Hotfix_get_SkipConditionList;
        private static DelegateBridge __Hotfix_get_GroupId;
        private static DelegateBridge __Hotfix_set_GroupId;
        private static DelegateBridge __Hotfix_get_IsSavePoint;
        private static DelegateBridge __Hotfix_set_IsSavePoint;
        private static DelegateBridge __Hotfix_get_NextStepId;
        private static DelegateBridge __Hotfix_set_NextStepId;
        private static DelegateBridge __Hotfix_get_PageList;
        private static DelegateBridge __Hotfix_get_UseNPCChat;
        private static DelegateBridge __Hotfix_set_UseNPCChat;
        private static DelegateBridge __Hotfix_get_NPCChatDummyIndex;
        private static DelegateBridge __Hotfix_set_NPCChatDummyIndex;
        private static DelegateBridge __Hotfix_get_NpcChatOffsetX;
        private static DelegateBridge __Hotfix_set_NpcChatOffsetX;
        private static DelegateBridge __Hotfix_get_NpcChatOffsetY;
        private static DelegateBridge __Hotfix_set_NpcChatOffsetY;
        private static DelegateBridge __Hotfix_get_OpAreaHierarchyPath;
        private static DelegateBridge __Hotfix_set_OpAreaHierarchyPath;
        private static DelegateBridge __Hotfix_get_ShowOpreationAreaEffect;
        private static DelegateBridge __Hotfix_set_ShowOpreationAreaEffect;
        private static DelegateBridge __Hotfix_get_OpreationAreaEffectMode;
        private static DelegateBridge __Hotfix_set_OpreationAreaEffectMode;
        private static DelegateBridge __Hotfix_get_ShowOpreationNoticeEffect;
        private static DelegateBridge __Hotfix_set_ShowOpreationNoticeEffect;
        private static DelegateBridge __Hotfix_get_EndStepWithOperatorAreaClick;
        private static DelegateBridge __Hotfix_set_EndStepWithOperatorAreaClick;
        private static DelegateBridge __Hotfix_get_ShowMaskPanel;
        private static DelegateBridge __Hotfix_set_ShowMaskPanel;
        private static DelegateBridge __Hotfix_get_DelayTime;
        private static DelegateBridge __Hotfix_set_DelayTime;
        private static DelegateBridge __Hotfix_get_AutoStopTime;
        private static DelegateBridge __Hotfix_set_AutoStopTime;
        private static DelegateBridge __Hotfix_get_CustomLayerAssetPath;
        private static DelegateBridge __Hotfix_set_CustomLayerAssetPath;
        private static DelegateBridge __Hotfix_get_CustomLayerCopyPathList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetID;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetIsTrigerPoint;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetGroupId;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetIsSavePoint;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetNextStepId;
        private static DelegateBridge __Hotfix_BlackJack.ToolUtil.IUserGuideStepInfo.GetPageList;

        [MethodImpl(0x8000)]
        int IUserGuideStepInfo.GetGroupId()
        {
        }

        [MethodImpl(0x8000)]
        int IUserGuideStepInfo.GetID()
        {
        }

        [MethodImpl(0x8000)]
        bool IUserGuideStepInfo.GetIsSavePoint()
        {
        }

        [MethodImpl(0x8000)]
        bool IUserGuideStepInfo.GetIsTrigerPoint()
        {
        }

        [MethodImpl(0x8000)]
        int IUserGuideStepInfo.GetNextStepId()
        {
        }

        [MethodImpl(0x8000)]
        List<KeyValuePair<int, int>> IUserGuideStepInfo.GetPageList()
        {
        }

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="UserGuideStepName", DataFormat=DataFormat.Default)]
        public string UserGuideStepName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="IsTrigerPoint", DataFormat=DataFormat.Default)]
        public bool IsTrigerPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="ConditionList", DataFormat=DataFormat.Default)]
        public List<UserGuideConditionInfo> ConditionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="ExtraConditionList", DataFormat=DataFormat.Default)]
        public List<ExtraUserGuideConditionInfo> ExtraConditionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="SkipConditionList", DataFormat=DataFormat.Default)]
        public List<UserGuideConditionInfo> SkipConditionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="GroupId", DataFormat=DataFormat.TwosComplement)]
        public int GroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="IsSavePoint", DataFormat=DataFormat.Default)]
        public bool IsSavePoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NextStepId", DataFormat=DataFormat.TwosComplement)]
        public int NextStepId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="PageList", DataFormat=DataFormat.Default)]
        public List<UserGuideStepInfoPageList> PageList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="UseNPCChat", DataFormat=DataFormat.Default)]
        public bool UseNPCChat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="NPCChatDummyIndex", DataFormat=DataFormat.TwosComplement)]
        public int NPCChatDummyIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="NpcChatOffsetX", DataFormat=DataFormat.FixedSize)]
        public float NpcChatOffsetX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="NpcChatOffsetY", DataFormat=DataFormat.FixedSize)]
        public float NpcChatOffsetY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="OpAreaHierarchyPath", DataFormat=DataFormat.Default)]
        public string OpAreaHierarchyPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="ShowOpreationAreaEffect", DataFormat=DataFormat.Default)]
        public bool ShowOpreationAreaEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="OpreationAreaEffectMode", DataFormat=DataFormat.Default)]
        public string OpreationAreaEffectMode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="ShowOpreationNoticeEffect", DataFormat=DataFormat.Default)]
        public bool ShowOpreationNoticeEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="EndStepWithOperatorAreaClick", DataFormat=DataFormat.Default)]
        public bool EndStepWithOperatorAreaClick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="ShowMaskPanel", DataFormat=DataFormat.Default)]
        public bool ShowMaskPanel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="DelayTime", DataFormat=DataFormat.FixedSize)]
        public float DelayTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="AutoStopTime", DataFormat=DataFormat.FixedSize)]
        public float AutoStopTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="CustomLayerAssetPath", DataFormat=DataFormat.Default)]
        public string CustomLayerAssetPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, Name="CustomLayerCopyPathList", DataFormat=DataFormat.Default)]
        public List<string> CustomLayerCopyPathList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

