﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="IslandStarGroupInfoIslandInfoList")]
    public class IslandStarGroupInfoIslandInfoList : IExtensible
    {
        private int _IslandTypeId;
        private int _Count;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IslandTypeId;
        private static DelegateBridge __Hotfix_set_IslandTypeId;
        private static DelegateBridge __Hotfix_get_Count;
        private static DelegateBridge __Hotfix_set_Count;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IslandTypeId", DataFormat=DataFormat.TwosComplement)]
        public int IslandTypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Count", DataFormat=DataFormat.TwosComplement)]
        public int Count
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

