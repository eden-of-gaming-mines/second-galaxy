﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataUserGuideTriggerPointInfo")]
    public class ConfigDataUserGuideTriggerPointInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.UserGuideTriggerPoint _UserGuideTriggerPoint;
        private readonly List<int> _UserGuideStepList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_UserGuideTriggerPoint;
        private static DelegateBridge __Hotfix_set_UserGuideTriggerPoint;
        private static DelegateBridge __Hotfix_get_UserGuideStepList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="UserGuideTriggerPoint", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.UserGuideTriggerPoint UserGuideTriggerPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="UserGuideStepList", DataFormat=DataFormat.TwosComplement)]
        public List<int> UserGuideStepList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

