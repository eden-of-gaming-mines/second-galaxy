﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipSuperEquipRunningInfo")]
    public class ConfigDataShipSuperEquipRunningInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _BufIdListAttachOnLaunch;
        private readonly List<int> _BufIdList;
        private readonly List<int> _SelfBufIdList;
        private readonly List<ParamInfo> _Params;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BufIdListAttachOnLaunch;
        private static DelegateBridge __Hotfix_get_BufIdList;
        private static DelegateBridge __Hotfix_get_SelfBufIdList;
        private static DelegateBridge __Hotfix_get_Params;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="BufIdListAttachOnLaunch", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufIdListAttachOnLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="BufIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="SelfBufIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SelfBufIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="Params", DataFormat=DataFormat.Default)]
        public List<ParamInfo> Params
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

