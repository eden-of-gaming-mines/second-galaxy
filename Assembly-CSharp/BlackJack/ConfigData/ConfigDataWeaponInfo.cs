﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataWeaponInfo")]
    public class ConfigDataWeaponInfo : IExtensible
    {
        private int _ID;
        private float _SalePrice;
        private float _BindMoneyBuyPrice;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private WeaponCategory _Category;
        private WeaponType _Type;
        private ShipSizeType _SizeType;
        private RankType _Rank;
        private SubRankType _SubRank;
        private int _PackedSize;
        private bool _IsBindOnPick;
        private readonly List<int> _AvailableAmmoType;
        private string _AvailableAmmoDesc;
        private uint _CPUCost;
        private uint _PowerCost;
        private uint _ChargeTime;
        private uint _TurretCount;
        private uint _UnitCount;
        private uint _WaveCountInWaveGroup;
        private int _AmmoClipSize;
        private int _AmmoClipReloadTime;
        private int _CD4NextUnit;
        private int _CD4NextTurret;
        private int _CD4NextWave;
        private int _CD4NextWaveGroup;
        private uint _LaunchCostEnergy;
        private uint _PropertyDamageBasic;
        private float _PropertyHitBasic;
        private float _PropertyCriticalBasic;
        private float _PropertyCriticalDamageMulti;
        private float _FireCtrlAccuracy;
        private float _TransverseVelocity;
        private int _LaserDamageInfoId;
        private int _FireRangeMax;
        private float _DroneFightTimeMulti;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private int _LocalPropertiesCount;
        private int _BulletFlySpeed;
        private int _DefaultAmmoId;
        private string _ArtResString;
        private string _IconResPath;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private float _ChargeAndChannelBreakFactor;
        private readonly List<ConfIdLevelInfo> _DependTechList;
        private readonly List<int> _RelativeTechList;
        private int _FlagShipWeaponRelativeId;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_set_BindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_SizeType;
        private static DelegateBridge __Hotfix_set_SizeType;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_AvailableAmmoType;
        private static DelegateBridge __Hotfix_get_AvailableAmmoDesc;
        private static DelegateBridge __Hotfix_set_AvailableAmmoDesc;
        private static DelegateBridge __Hotfix_get_CPUCost;
        private static DelegateBridge __Hotfix_set_CPUCost;
        private static DelegateBridge __Hotfix_get_PowerCost;
        private static DelegateBridge __Hotfix_set_PowerCost;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_TurretCount;
        private static DelegateBridge __Hotfix_set_TurretCount;
        private static DelegateBridge __Hotfix_get_UnitCount;
        private static DelegateBridge __Hotfix_set_UnitCount;
        private static DelegateBridge __Hotfix_get_WaveCountInWaveGroup;
        private static DelegateBridge __Hotfix_set_WaveCountInWaveGroup;
        private static DelegateBridge __Hotfix_get_AmmoClipSize;
        private static DelegateBridge __Hotfix_set_AmmoClipSize;
        private static DelegateBridge __Hotfix_get_AmmoClipReloadTime;
        private static DelegateBridge __Hotfix_set_AmmoClipReloadTime;
        private static DelegateBridge __Hotfix_get_CD4NextUnit;
        private static DelegateBridge __Hotfix_set_CD4NextUnit;
        private static DelegateBridge __Hotfix_get_CD4NextTurret;
        private static DelegateBridge __Hotfix_set_CD4NextTurret;
        private static DelegateBridge __Hotfix_get_CD4NextWave;
        private static DelegateBridge __Hotfix_set_CD4NextWave;
        private static DelegateBridge __Hotfix_get_CD4NextWaveGroup;
        private static DelegateBridge __Hotfix_set_CD4NextWaveGroup;
        private static DelegateBridge __Hotfix_get_LaunchCostEnergy;
        private static DelegateBridge __Hotfix_set_LaunchCostEnergy;
        private static DelegateBridge __Hotfix_get_PropertyDamageBasic;
        private static DelegateBridge __Hotfix_set_PropertyDamageBasic;
        private static DelegateBridge __Hotfix_get_PropertyHitBasic;
        private static DelegateBridge __Hotfix_set_PropertyHitBasic;
        private static DelegateBridge __Hotfix_get_PropertyCriticalBasic;
        private static DelegateBridge __Hotfix_set_PropertyCriticalBasic;
        private static DelegateBridge __Hotfix_get_PropertyCriticalDamageMulti;
        private static DelegateBridge __Hotfix_set_PropertyCriticalDamageMulti;
        private static DelegateBridge __Hotfix_get_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_set_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_get_TransverseVelocity;
        private static DelegateBridge __Hotfix_set_TransverseVelocity;
        private static DelegateBridge __Hotfix_get_LaserDamageInfoId;
        private static DelegateBridge __Hotfix_set_LaserDamageInfoId;
        private static DelegateBridge __Hotfix_get_FireRangeMax;
        private static DelegateBridge __Hotfix_set_FireRangeMax;
        private static DelegateBridge __Hotfix_get_DroneFightTimeMulti;
        private static DelegateBridge __Hotfix_set_DroneFightTimeMulti;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_set_LocalPropertiesCount;
        private static DelegateBridge __Hotfix_get_BulletFlySpeed;
        private static DelegateBridge __Hotfix_set_BulletFlySpeed;
        private static DelegateBridge __Hotfix_get_DefaultAmmoId;
        private static DelegateBridge __Hotfix_set_DefaultAmmoId;
        private static DelegateBridge __Hotfix_get_ArtResString;
        private static DelegateBridge __Hotfix_set_ArtResString;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_set_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_get_DependTechList;
        private static DelegateBridge __Hotfix_get_RelativeTechList;
        private static DelegateBridge __Hotfix_get_FlagShipWeaponRelativeId;
        private static DelegateBridge __Hotfix_set_FlagShipWeaponRelativeId;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BindMoneyBuyPrice", DataFormat=DataFormat.FixedSize)]
        public float BindMoneyBuyPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public WeaponCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public WeaponType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="SizeType", DataFormat=DataFormat.TwosComplement)]
        public ShipSizeType SizeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.TwosComplement)]
        public int PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="AvailableAmmoType", DataFormat=DataFormat.TwosComplement)]
        public List<int> AvailableAmmoType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="AvailableAmmoDesc", DataFormat=DataFormat.Default)]
        public string AvailableAmmoDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="CPUCost", DataFormat=DataFormat.TwosComplement)]
        public uint CPUCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="PowerCost", DataFormat=DataFormat.TwosComplement)]
        public uint PowerCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="ChargeTime", DataFormat=DataFormat.TwosComplement)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="TurretCount", DataFormat=DataFormat.TwosComplement)]
        public uint TurretCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="UnitCount", DataFormat=DataFormat.TwosComplement)]
        public uint UnitCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="WaveCountInWaveGroup", DataFormat=DataFormat.TwosComplement)]
        public uint WaveCountInWaveGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="AmmoClipSize", DataFormat=DataFormat.TwosComplement)]
        public int AmmoClipSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="AmmoClipReloadTime", DataFormat=DataFormat.TwosComplement)]
        public int AmmoClipReloadTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="CD4NextUnit", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextUnit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="CD4NextTurret", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextTurret
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="CD4NextWave", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextWave
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="CD4NextWaveGroup", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextWaveGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=true, Name="LaunchCostEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint LaunchCostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, IsRequired=true, Name="PropertyDamageBasic", DataFormat=DataFormat.TwosComplement)]
        public uint PropertyDamageBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="PropertyHitBasic", DataFormat=DataFormat.FixedSize)]
        public float PropertyHitBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=true, Name="PropertyCriticalBasic", DataFormat=DataFormat.FixedSize)]
        public float PropertyCriticalBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=true, Name="PropertyCriticalDamageMulti", DataFormat=DataFormat.FixedSize)]
        public float PropertyCriticalDamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=true, Name="FireCtrlAccuracy", DataFormat=DataFormat.FixedSize)]
        public float FireCtrlAccuracy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x23, IsRequired=true, Name="TransverseVelocity", DataFormat=DataFormat.FixedSize)]
        public float TransverseVelocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x24, IsRequired=true, Name="LaserDamageInfoId", DataFormat=DataFormat.TwosComplement)]
        public int LaserDamageInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x25, IsRequired=true, Name="FireRangeMax", DataFormat=DataFormat.TwosComplement)]
        public int FireRangeMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x27, IsRequired=true, Name="DroneFightTimeMulti", DataFormat=DataFormat.FixedSize)]
        public float DroneFightTimeMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(40, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x29, IsRequired=true, Name="LocalPropertiesCount", DataFormat=DataFormat.TwosComplement)]
        public int LocalPropertiesCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=true, Name="BulletFlySpeed", DataFormat=DataFormat.TwosComplement)]
        public int BulletFlySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, IsRequired=true, Name="DefaultAmmoId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultAmmoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=true, Name="ArtResString", DataFormat=DataFormat.Default)]
        public string ArtResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2d, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2e, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x2f, IsRequired=true, Name="ChargeAndChannelBreakFactor", DataFormat=DataFormat.FixedSize)]
        public float ChargeAndChannelBreakFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x30, Name="DependTechList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x31, Name="RelativeTechList", DataFormat=DataFormat.TwosComplement)]
        public List<int> RelativeTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(50, IsRequired=true, Name="FlagShipWeaponRelativeId", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipWeaponRelativeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x33, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x34, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

