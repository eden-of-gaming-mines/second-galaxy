﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildEvaluateScoreInfo")]
    public class ConfigDataGuildEvaluateScoreInfo : IExtensible
    {
        private int _ID;
        private int _OccupyScore;
        private readonly List<GuildEvaluateScoreInfoBuildingScoreList> _BuildingScoreList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_OccupyScore;
        private static DelegateBridge __Hotfix_set_OccupyScore;
        private static DelegateBridge __Hotfix_get_BuildingScoreList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="OccupyScore", DataFormat=DataFormat.TwosComplement)]
        public int OccupyScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="BuildingScoreList", DataFormat=DataFormat.Default)]
        public List<GuildEvaluateScoreInfoBuildingScoreList> BuildingScoreList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

