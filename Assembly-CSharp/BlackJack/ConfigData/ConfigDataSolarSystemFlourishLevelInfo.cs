﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSolarSystemFlourishLevelInfo")]
    public class ConfigDataSolarSystemFlourishLevelInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _NeedFlourishValue;
        private int _SolarSystemControllHubShieldArmorMulti;
        private readonly List<float> _SolarSystemSovereignWarCost;
        private readonly List<int> _DailyGrow;
        private readonly List<int> _DailyInformationPointGrowList;
        private readonly List<int> _DailyBindMoneyRewardList;
        private readonly List<int> _SovereignBattleWinRewardList;
        private readonly List<int> _BuildingBattleWinRewardList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_NeedFlourishValue;
        private static DelegateBridge __Hotfix_get_SolarSystemControllHubShieldArmorMulti;
        private static DelegateBridge __Hotfix_set_SolarSystemControllHubShieldArmorMulti;
        private static DelegateBridge __Hotfix_get_SolarSystemSovereignWarCost;
        private static DelegateBridge __Hotfix_get_DailyGrow;
        private static DelegateBridge __Hotfix_get_DailyInformationPointGrowList;
        private static DelegateBridge __Hotfix_get_DailyBindMoneyRewardList;
        private static DelegateBridge __Hotfix_get_SovereignBattleWinRewardList;
        private static DelegateBridge __Hotfix_get_BuildingBattleWinRewardList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="NeedFlourishValue", DataFormat=DataFormat.TwosComplement)]
        public List<int> NeedFlourishValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SolarSystemControllHubShieldArmorMulti", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemControllHubShieldArmorMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="SolarSystemSovereignWarCost", DataFormat=DataFormat.FixedSize)]
        public List<float> SolarSystemSovereignWarCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="DailyGrow", DataFormat=DataFormat.TwosComplement)]
        public List<int> DailyGrow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="DailyInformationPointGrowList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DailyInformationPointGrowList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="DailyBindMoneyRewardList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DailyBindMoneyRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="SovereignBattleWinRewardList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SovereignBattleWinRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="BuildingBattleWinRewardList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BuildingBattleWinRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

