﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcShopItemSaleCountInfo")]
    public class ConfigDataNpcShopItemSaleCountInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.NpcShopItemType _NpcShopItemType;
        private int _DefaultSaleCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_NpcShopItemType;
        private static DelegateBridge __Hotfix_set_NpcShopItemType;
        private static DelegateBridge __Hotfix_get_DefaultSaleCount;
        private static DelegateBridge __Hotfix_set_DefaultSaleCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="NpcShopItemType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.NpcShopItemType NpcShopItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DefaultSaleCount", DataFormat=DataFormat.TwosComplement)]
        public int DefaultSaleCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

