﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ActivityRewardType")]
    public enum ActivityRewardType
    {
        [ProtoEnum(Name="ActivityRewardType_Invalid", Value=0)]
        ActivityRewardType_Invalid = 0,
        [ProtoEnum(Name="ActivityRewardType_Currency", Value=1)]
        ActivityRewardType_Currency = 1,
        [ProtoEnum(Name="ActivityRewardType_Mineral", Value=2)]
        ActivityRewardType_Mineral = 2,
        [ProtoEnum(Name="ActivityRewardType_CrackBox", Value=3)]
        ActivityRewardType_CrackBox = 3,
        [ProtoEnum(Name="ActivityRewardType_Exp", Value=4)]
        ActivityRewardType_Exp = 4,
        [ProtoEnum(Name="ActivityRewardType_InformationPoint", Value=5)]
        ActivityRewardType_InformationPoint = 5,
        [ProtoEnum(Name="ActivityRewardType_TacticalPoint", Value=6)]
        ActivityRewardType_TacticalPoint = 6,
        [ProtoEnum(Name="ActivityRewardType_GuildTradeMoney", Value=7)]
        ActivityRewardType_GuildTradeMoney = 7,
        [ProtoEnum(Name="ActivityRewardType_Max", Value=8)]
        ActivityRewardType_Max = 8
    }
}

