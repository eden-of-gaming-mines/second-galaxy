﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GiftPackageCategory")]
    public enum GiftPackageCategory
    {
        [ProtoEnum(Name="GiftPackageCategory_Invalid", Value=0)]
        GiftPackageCategory_Invalid = 0,
        [ProtoEnum(Name="GiftPackageCategory_Weekly", Value=1)]
        GiftPackageCategory_Weekly = 1,
        [ProtoEnum(Name="GiftPackageCategory_Special", Value=2)]
        GiftPackageCategory_Special = 2,
        [ProtoEnum(Name="GiftPackageCategory_Daily", Value=3)]
        GiftPackageCategory_Daily = 3,
        [ProtoEnum(Name="GiftPackageCategory_Max", Value=4)]
        GiftPackageCategory_Max = 4
    }
}

