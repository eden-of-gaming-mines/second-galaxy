﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSceneInfo")]
    public class ConfigDataSceneInfo : IExtensible
    {
        private int _ID;
        private int _ResId;
        private BlackJack.ConfigData.SceneType _SceneType;
        private SceneWingShipSetting _WingShipSetting;
        private bool _PVPEnable;
        private bool _Global;
        private bool _CanbeInvade;
        private bool _PlayerShipDropItem;
        private bool _PlayerShipDestroy;
        private int _TargetListFilter;
        private int _TargetListPriority;
        private string _BGMName;
        private string _IconResPath;
        private bool _ShowRandomName;
        private bool _DisableAutoFight;
        private readonly List<ShipType> _ShipTypeLimit;
        private bool _OnlyOwnerEnter;
        private int _EnterPlayerLevelMin;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ResId;
        private static DelegateBridge __Hotfix_set_ResId;
        private static DelegateBridge __Hotfix_get_SceneType;
        private static DelegateBridge __Hotfix_set_SceneType;
        private static DelegateBridge __Hotfix_get_WingShipSetting;
        private static DelegateBridge __Hotfix_set_WingShipSetting;
        private static DelegateBridge __Hotfix_get_PVPEnable;
        private static DelegateBridge __Hotfix_set_PVPEnable;
        private static DelegateBridge __Hotfix_get_Global;
        private static DelegateBridge __Hotfix_set_Global;
        private static DelegateBridge __Hotfix_get_CanbeInvade;
        private static DelegateBridge __Hotfix_set_CanbeInvade;
        private static DelegateBridge __Hotfix_get_PlayerShipDropItem;
        private static DelegateBridge __Hotfix_set_PlayerShipDropItem;
        private static DelegateBridge __Hotfix_get_PlayerShipDestroy;
        private static DelegateBridge __Hotfix_set_PlayerShipDestroy;
        private static DelegateBridge __Hotfix_get_TargetListFilter;
        private static DelegateBridge __Hotfix_set_TargetListFilter;
        private static DelegateBridge __Hotfix_get_TargetListPriority;
        private static DelegateBridge __Hotfix_set_TargetListPriority;
        private static DelegateBridge __Hotfix_get_BGMName;
        private static DelegateBridge __Hotfix_set_BGMName;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ShowRandomName;
        private static DelegateBridge __Hotfix_set_ShowRandomName;
        private static DelegateBridge __Hotfix_get_DisableAutoFight;
        private static DelegateBridge __Hotfix_set_DisableAutoFight;
        private static DelegateBridge __Hotfix_get_ShipTypeLimit;
        private static DelegateBridge __Hotfix_get_OnlyOwnerEnter;
        private static DelegateBridge __Hotfix_set_OnlyOwnerEnter;
        private static DelegateBridge __Hotfix_get_EnterPlayerLevelMin;
        private static DelegateBridge __Hotfix_set_EnterPlayerLevelMin;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ResId", DataFormat=DataFormat.TwosComplement)]
        public int ResId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SceneType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SceneType SceneType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="WingShipSetting", DataFormat=DataFormat.TwosComplement)]
        public SceneWingShipSetting WingShipSetting
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="PVPEnable", DataFormat=DataFormat.Default)]
        public bool PVPEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Global", DataFormat=DataFormat.Default)]
        public bool Global
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CanbeInvade", DataFormat=DataFormat.Default)]
        public bool CanbeInvade
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="PlayerShipDropItem", DataFormat=DataFormat.Default)]
        public bool PlayerShipDropItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="PlayerShipDestroy", DataFormat=DataFormat.Default)]
        public bool PlayerShipDestroy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="TargetListFilter", DataFormat=DataFormat.TwosComplement)]
        public int TargetListFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="TargetListPriority", DataFormat=DataFormat.TwosComplement)]
        public int TargetListPriority
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="BGMName", DataFormat=DataFormat.Default)]
        public string BGMName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="ShowRandomName", DataFormat=DataFormat.Default)]
        public bool ShowRandomName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="DisableAutoFight", DataFormat=DataFormat.Default)]
        public bool DisableAutoFight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, Name="ShipTypeLimit", DataFormat=DataFormat.TwosComplement)]
        public List<ShipType> ShipTypeLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="OnlyOwnerEnter", DataFormat=DataFormat.Default)]
        public bool OnlyOwnerEnter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="EnterPlayerLevelMin", DataFormat=DataFormat.TwosComplement)]
        public int EnterPlayerLevelMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

