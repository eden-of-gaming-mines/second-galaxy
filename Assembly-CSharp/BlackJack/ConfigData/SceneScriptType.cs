﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneScriptType")]
    public enum SceneScriptType
    {
        [ProtoEnum(Name="SceneScriptType_None", Value=0)]
        SceneScriptType_None = 0,
        [ProtoEnum(Name="SceneScriptType_StarGateStatic", Value=1)]
        SceneScriptType_StarGateStatic = 1,
        [ProtoEnum(Name="SceneScriptType_WormholeGate", Value=2)]
        SceneScriptType_WormholeGate = 2,
        [ProtoEnum(Name="SceneScriptType_ManualFight001", Value=3)]
        SceneScriptType_ManualFight001 = 3,
        [ProtoEnum(Name="SceneScriptType_SceneScriptKillWaveOneByOne", Value=4)]
        SceneScriptType_SceneScriptKillWaveOneByOne = 4,
        [ProtoEnum(Name="SceneScriptType_SceneScriptKillLeaderOneByOne", Value=5)]
        SceneScriptType_SceneScriptKillLeaderOneByOne = 5,
        [ProtoEnum(Name="SceneScriptType_SceneScriptSummonMultiwave", Value=6)]
        SceneScriptType_SceneScriptSummonMultiwave = 6,
        [ProtoEnum(Name="SceneScriptType_SceneScriptSummonByBossArmor", Value=7)]
        SceneScriptType_SceneScriptSummonByBossArmor = 7,
        [ProtoEnum(Name="SceneScriptType_SceneScriptSummonMultiwavePlus", Value=8)]
        SceneScriptType_SceneScriptSummonMultiwavePlus = 8,
        [ProtoEnum(Name="SceneScriptType_SceneScriptSummonByBossArmorAndShield", Value=9)]
        SceneScriptType_SceneScriptSummonByBossArmorAndShield = 9,
        [ProtoEnum(Name="SceneScriptType_PrologueQuest01", Value=10)]
        SceneScriptType_PrologueQuest01 = 10,
        [ProtoEnum(Name="SceneScriptType_PrologueQuest02", Value=11)]
        SceneScriptType_PrologueQuest02 = 11,
        [ProtoEnum(Name="SceneScriptType_MainQuest1001", Value=12)]
        SceneScriptType_MainQuest1001 = 12,
        [ProtoEnum(Name="SceneScriptType_MainQuest1002", Value=13)]
        SceneScriptType_MainQuest1002 = 13,
        [ProtoEnum(Name="SceneScriptType_MainQuest1003", Value=14)]
        SceneScriptType_MainQuest1003 = 14,
        [ProtoEnum(Name="SceneScriptType_MainQuest1004", Value=15)]
        SceneScriptType_MainQuest1004 = 15,
        [ProtoEnum(Name="SceneScriptType_MainQuest1005", Value=0x10)]
        SceneScriptType_MainQuest1005 = 0x10,
        [ProtoEnum(Name="SceneScriptType_MainQuest1006", Value=0x11)]
        SceneScriptType_MainQuest1006 = 0x11,
        [ProtoEnum(Name="SceneScriptType_MainQuest1007", Value=0x12)]
        SceneScriptType_MainQuest1007 = 0x12,
        [ProtoEnum(Name="SceneScriptType_MainQuest10070", Value=0x13)]
        SceneScriptType_MainQuest10070 = 0x13,
        [ProtoEnum(Name="SceneScriptType_MainQuest1008", Value=20)]
        SceneScriptType_MainQuest1008 = 20,
        [ProtoEnum(Name="SceneScriptType_MainQuest1009", Value=0x15)]
        SceneScriptType_MainQuest1009 = 0x15,
        [ProtoEnum(Name="SceneScriptType_MainQuest1010", Value=0x16)]
        SceneScriptType_MainQuest1010 = 0x16,
        [ProtoEnum(Name="SceneScriptType_MainQuest1011", Value=0x17)]
        SceneScriptType_MainQuest1011 = 0x17,
        [ProtoEnum(Name="SceneScriptType_MainQuest1012", Value=0x18)]
        SceneScriptType_MainQuest1012 = 0x18,
        [ProtoEnum(Name="SceneScriptType_MainQuest1013", Value=0x19)]
        SceneScriptType_MainQuest1013 = 0x19,
        [ProtoEnum(Name="SceneScriptType_MainQuest1014", Value=0x1a)]
        SceneScriptType_MainQuest1014 = 0x1a,
        [ProtoEnum(Name="SceneScriptType_MainQuest1015", Value=0x1b)]
        SceneScriptType_MainQuest1015 = 0x1b,
        [ProtoEnum(Name="SceneScriptType_MainQuest1016", Value=0x1c)]
        SceneScriptType_MainQuest1016 = 0x1c,
        [ProtoEnum(Name="SceneScriptType_MainQuest1017", Value=0x1d)]
        SceneScriptType_MainQuest1017 = 0x1d,
        [ProtoEnum(Name="SceneScriptType_MainQuest1018", Value=30)]
        SceneScriptType_MainQuest1018 = 30,
        [ProtoEnum(Name="SceneScriptType_MainQuest1019", Value=0x1f)]
        SceneScriptType_MainQuest1019 = 0x1f,
        [ProtoEnum(Name="SceneScriptType_MainQuest1020", Value=0x20)]
        SceneScriptType_MainQuest1020 = 0x20,
        [ProtoEnum(Name="SceneScriptType_MainQuest1021", Value=0x21)]
        SceneScriptType_MainQuest1021 = 0x21,
        [ProtoEnum(Name="SceneScriptType_MainQuest1022", Value=0x22)]
        SceneScriptType_MainQuest1022 = 0x22,
        [ProtoEnum(Name="SceneScriptType_MainQuest1023", Value=0x23)]
        SceneScriptType_MainQuest1023 = 0x23,
        [ProtoEnum(Name="SceneScriptType_MainQuest1024", Value=0x24)]
        SceneScriptType_MainQuest1024 = 0x24,
        [ProtoEnum(Name="SceneScriptType_MainQuest1025", Value=0x25)]
        SceneScriptType_MainQuest1025 = 0x25,
        [ProtoEnum(Name="SceneScriptType_MainQuest1026", Value=0x26)]
        SceneScriptType_MainQuest1026 = 0x26,
        [ProtoEnum(Name="SceneScriptType_MainQuest1027", Value=0x27)]
        SceneScriptType_MainQuest1027 = 0x27,
        [ProtoEnum(Name="SceneScriptType_MainQuest1028", Value=40)]
        SceneScriptType_MainQuest1028 = 40,
        [ProtoEnum(Name="SceneScriptType_MainQuest1029", Value=0x29)]
        SceneScriptType_MainQuest1029 = 0x29,
        [ProtoEnum(Name="SceneScriptType_MainQuest1030", Value=0x2a)]
        SceneScriptType_MainQuest1030 = 0x2a,
        [ProtoEnum(Name="SceneScriptType_MainQuest1031", Value=0x2b)]
        SceneScriptType_MainQuest1031 = 0x2b,
        [ProtoEnum(Name="SceneScriptType_MainQuest1032", Value=0x2c)]
        SceneScriptType_MainQuest1032 = 0x2c,
        [ProtoEnum(Name="SceneScriptType_MainQuest1033", Value=0x2d)]
        SceneScriptType_MainQuest1033 = 0x2d,
        [ProtoEnum(Name="SceneScriptType_MainQuest1034", Value=0x2e)]
        SceneScriptType_MainQuest1034 = 0x2e,
        [ProtoEnum(Name="SceneScriptType_MainQuest1035", Value=0x2f)]
        SceneScriptType_MainQuest1035 = 0x2f,
        [ProtoEnum(Name="SceneScriptType_MainQuest1036", Value=0x30)]
        SceneScriptType_MainQuest1036 = 0x30,
        [ProtoEnum(Name="SceneScriptType_MainQuest1037", Value=0x31)]
        SceneScriptType_MainQuest1037 = 0x31,
        [ProtoEnum(Name="SceneScriptType_MainQuest1038", Value=50)]
        SceneScriptType_MainQuest1038 = 50,
        [ProtoEnum(Name="SceneScriptType_MainQuest1039", Value=0x33)]
        SceneScriptType_MainQuest1039 = 0x33,
        [ProtoEnum(Name="SceneScriptType_MainQuest1040", Value=0x34)]
        SceneScriptType_MainQuest1040 = 0x34,
        [ProtoEnum(Name="SceneScriptType_MainQuest1041", Value=0x35)]
        SceneScriptType_MainQuest1041 = 0x35,
        [ProtoEnum(Name="SceneScriptType_MainQuest1042", Value=0x36)]
        SceneScriptType_MainQuest1042 = 0x36,
        [ProtoEnum(Name="SceneScriptType_MainQuest1043", Value=0x37)]
        SceneScriptType_MainQuest1043 = 0x37,
        [ProtoEnum(Name="SceneScriptType_MainQuest1044", Value=0x38)]
        SceneScriptType_MainQuest1044 = 0x38,
        [ProtoEnum(Name="SceneScriptType_MainQuest1045", Value=0x39)]
        SceneScriptType_MainQuest1045 = 0x39,
        [ProtoEnum(Name="SceneScriptType_MainQuest10450", Value=0x3a)]
        SceneScriptType_MainQuest10450 = 0x3a,
        [ProtoEnum(Name="SceneScriptType_MainQuest1046", Value=0x3b)]
        SceneScriptType_MainQuest1046 = 0x3b,
        [ProtoEnum(Name="SceneScriptType_MainQuest1047", Value=60)]
        SceneScriptType_MainQuest1047 = 60,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10101", Value=0x3d)]
        SceneScriptType_WormholeGamePlay10101 = 0x3d,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10102", Value=0x3e)]
        SceneScriptType_WormholeGamePlay10102 = 0x3e,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10201", Value=0x3f)]
        SceneScriptType_WormholeGamePlay10201 = 0x3f,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10202", Value=0x40)]
        SceneScriptType_WormholeGamePlay10202 = 0x40,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10203", Value=0x41)]
        SceneScriptType_WormholeGamePlay10203 = 0x41,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10204", Value=0x42)]
        SceneScriptType_WormholeGamePlay10204 = 0x42,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10301", Value=0x43)]
        SceneScriptType_WormholeGamePlay10301 = 0x43,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10302", Value=0x44)]
        SceneScriptType_WormholeGamePlay10302 = 0x44,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10303", Value=0x45)]
        SceneScriptType_WormholeGamePlay10303 = 0x45,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10304", Value=70)]
        SceneScriptType_WormholeGamePlay10304 = 70,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10305", Value=0x47)]
        SceneScriptType_WormholeGamePlay10305 = 0x47,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10306", Value=0x48)]
        SceneScriptType_WormholeGamePlay10306 = 0x48,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10401", Value=0x49)]
        SceneScriptType_WormholeGamePlay10401 = 0x49,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10402", Value=0x4a)]
        SceneScriptType_WormholeGamePlay10402 = 0x4a,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10403", Value=0x4b)]
        SceneScriptType_WormholeGamePlay10403 = 0x4b,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10404", Value=0x4c)]
        SceneScriptType_WormholeGamePlay10404 = 0x4c,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10405", Value=0x4d)]
        SceneScriptType_WormholeGamePlay10405 = 0x4d,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10501", Value=0x4e)]
        SceneScriptType_WormholeGamePlay10501 = 0x4e,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10502", Value=0x4f)]
        SceneScriptType_WormholeGamePlay10502 = 0x4f,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10503", Value=80)]
        SceneScriptType_WormholeGamePlay10503 = 80,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10504", Value=0x51)]
        SceneScriptType_WormholeGamePlay10504 = 0x51,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10510", Value=0x52)]
        SceneScriptType_WormholeGamePlay10510 = 0x52,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10511", Value=0x53)]
        SceneScriptType_WormholeGamePlay10511 = 0x53,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10512", Value=0x54)]
        SceneScriptType_WormholeGamePlay10512 = 0x54,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10601", Value=0x55)]
        SceneScriptType_WormholeGamePlay10601 = 0x55,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10602", Value=0x56)]
        SceneScriptType_WormholeGamePlay10602 = 0x56,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10603", Value=0x57)]
        SceneScriptType_WormholeGamePlay10603 = 0x57,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10604", Value=0x58)]
        SceneScriptType_WormholeGamePlay10604 = 0x58,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10605", Value=0x59)]
        SceneScriptType_WormholeGamePlay10605 = 0x59,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10701", Value=90)]
        SceneScriptType_WormholeGamePlay10701 = 90,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10801", Value=0x5b)]
        SceneScriptType_WormholeGamePlay10801 = 0x5b,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10802", Value=0x5c)]
        SceneScriptType_WormholeGamePlay10802 = 0x5c,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10901", Value=0x5d)]
        SceneScriptType_WormholeGamePlay10901 = 0x5d,
        [ProtoEnum(Name="SceneScriptType_WormholeGamePlay10902", Value=0x5e)]
        SceneScriptType_WormholeGamePlay10902 = 0x5e,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3101", Value=0x5f)]
        SceneScriptType_RandomQuest3101 = 0x5f,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3102", Value=0x60)]
        SceneScriptType_RandomQuest3102 = 0x60,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3103", Value=0x61)]
        SceneScriptType_RandomQuest3103 = 0x61,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3104", Value=0x62)]
        SceneScriptType_RandomQuest3104 = 0x62,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3201", Value=0x63)]
        SceneScriptType_RandomQuest3201 = 0x63,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3202", Value=100)]
        SceneScriptType_RandomQuest3202 = 100,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3203", Value=0x65)]
        SceneScriptType_RandomQuest3203 = 0x65,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3204", Value=0x66)]
        SceneScriptType_RandomQuest3204 = 0x66,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3301", Value=0x67)]
        SceneScriptType_RandomQuest3301 = 0x67,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3302", Value=0x68)]
        SceneScriptType_RandomQuest3302 = 0x68,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3303", Value=0x69)]
        SceneScriptType_RandomQuest3303 = 0x69,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3304", Value=0x6a)]
        SceneScriptType_RandomQuest3304 = 0x6a,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3401", Value=0x6b)]
        SceneScriptType_RandomQuest3401 = 0x6b,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3402", Value=0x6c)]
        SceneScriptType_RandomQuest3402 = 0x6c,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3403", Value=0x6d)]
        SceneScriptType_RandomQuest3403 = 0x6d,
        [ProtoEnum(Name="SceneScriptType_RandomQuest3404", Value=110)]
        SceneScriptType_RandomQuest3404 = 110,
        [ProtoEnum(Name="SceneScriptType_WormholeTunnel", Value=0x6f)]
        SceneScriptType_WormholeTunnel = 0x6f,
        [ProtoEnum(Name="SceneScriptType_WormholeBackPoint", Value=0x70)]
        SceneScriptType_WormholeBackPoint = 0x70,
        [ProtoEnum(Name="SceneScriptType_InfectFinalBattle", Value=0x71)]
        SceneScriptType_InfectFinalBattle = 0x71,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6001", Value=0x72)]
        SceneScriptType_DrivingLicenseQuest6001 = 0x72,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6002", Value=0x73)]
        SceneScriptType_DrivingLicenseQuest6002 = 0x73,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6003", Value=0x74)]
        SceneScriptType_DrivingLicenseQuest6003 = 0x74,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6004", Value=0x75)]
        SceneScriptType_DrivingLicenseQuest6004 = 0x75,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6005", Value=0x76)]
        SceneScriptType_DrivingLicenseQuest6005 = 0x76,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6006", Value=0x77)]
        SceneScriptType_DrivingLicenseQuest6006 = 0x77,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6007", Value=120)]
        SceneScriptType_DrivingLicenseQuest6007 = 120,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6008", Value=0x79)]
        SceneScriptType_DrivingLicenseQuest6008 = 0x79,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6009", Value=0x7a)]
        SceneScriptType_DrivingLicenseQuest6009 = 0x7a,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6010", Value=0x7b)]
        SceneScriptType_DrivingLicenseQuest6010 = 0x7b,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6011", Value=0x7c)]
        SceneScriptType_DrivingLicenseQuest6011 = 0x7c,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6012", Value=0x7d)]
        SceneScriptType_DrivingLicenseQuest6012 = 0x7d,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6013", Value=0x7e)]
        SceneScriptType_DrivingLicenseQuest6013 = 0x7e,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6014", Value=0x7f)]
        SceneScriptType_DrivingLicenseQuest6014 = 0x7f,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6015", Value=0x80)]
        SceneScriptType_DrivingLicenseQuest6015 = 0x80,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6016", Value=0x81)]
        SceneScriptType_DrivingLicenseQuest6016 = 0x81,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6017", Value=130)]
        SceneScriptType_DrivingLicenseQuest6017 = 130,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6018", Value=0x83)]
        SceneScriptType_DrivingLicenseQuest6018 = 0x83,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6019", Value=0x84)]
        SceneScriptType_DrivingLicenseQuest6019 = 0x84,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6020", Value=0x85)]
        SceneScriptType_DrivingLicenseQuest6020 = 0x85,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6021", Value=0x86)]
        SceneScriptType_DrivingLicenseQuest6021 = 0x86,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6022", Value=0x87)]
        SceneScriptType_DrivingLicenseQuest6022 = 0x87,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6023", Value=0x88)]
        SceneScriptType_DrivingLicenseQuest6023 = 0x88,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6024", Value=0x89)]
        SceneScriptType_DrivingLicenseQuest6024 = 0x89,
        [ProtoEnum(Name="SceneScriptType_DrivingLicenseQuest6025", Value=0x8a)]
        SceneScriptType_DrivingLicenseQuest6025 = 0x8a,
        [ProtoEnum(Name="SceneScriptType_InfectQuest411", Value=0x8b)]
        SceneScriptType_InfectQuest411 = 0x8b,
        [ProtoEnum(Name="SceneScriptType_InfectQuest412", Value=140)]
        SceneScriptType_InfectQuest412 = 140,
        [ProtoEnum(Name="SceneScriptType_InfectQuest413", Value=0x8d)]
        SceneScriptType_InfectQuest413 = 0x8d,
        [ProtoEnum(Name="SceneScriptType_InfectQuest421", Value=0x8e)]
        SceneScriptType_InfectQuest421 = 0x8e,
        [ProtoEnum(Name="SceneScriptType_InfectQuest422", Value=0x8f)]
        SceneScriptType_InfectQuest422 = 0x8f,
        [ProtoEnum(Name="SceneScriptType_InfectQuest423", Value=0x90)]
        SceneScriptType_InfectQuest423 = 0x90,
        [ProtoEnum(Name="SceneScriptType_InfectQuest431", Value=0x91)]
        SceneScriptType_InfectQuest431 = 0x91,
        [ProtoEnum(Name="SceneScriptType_InfectQuest432", Value=0x92)]
        SceneScriptType_InfectQuest432 = 0x92,
        [ProtoEnum(Name="SceneScriptType_InfectQuest433", Value=0x93)]
        SceneScriptType_InfectQuest433 = 0x93,
        [ProtoEnum(Name="SceneScriptType_InfectQuest441", Value=0x94)]
        SceneScriptType_InfectQuest441 = 0x94,
        [ProtoEnum(Name="SceneScriptType_InfectQuest442", Value=0x95)]
        SceneScriptType_InfectQuest442 = 0x95,
        [ProtoEnum(Name="SceneScriptType_InfectQuest443", Value=150)]
        SceneScriptType_InfectQuest443 = 150,
        [ProtoEnum(Name="SceneScriptType_BattleTest", Value=0x97)]
        SceneScriptType_BattleTest = 0x97,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100001", Value=0x98)]
        SceneScriptType_RandomQuest100001 = 0x98,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100002", Value=0x99)]
        SceneScriptType_RandomQuest100002 = 0x99,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100003", Value=0x9a)]
        SceneScriptType_RandomQuest100003 = 0x9a,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100004", Value=0x9b)]
        SceneScriptType_RandomQuest100004 = 0x9b,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100005", Value=0x9c)]
        SceneScriptType_RandomQuest100005 = 0x9c,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100006", Value=0x9d)]
        SceneScriptType_RandomQuest100006 = 0x9d,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100007", Value=0x9e)]
        SceneScriptType_RandomQuest100007 = 0x9e,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100008", Value=0x9f)]
        SceneScriptType_RandomQuest100008 = 0x9f,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100009", Value=160)]
        SceneScriptType_RandomQuest100009 = 160,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100010", Value=0xa1)]
        SceneScriptType_RandomQuest100010 = 0xa1,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100011", Value=0xa2)]
        SceneScriptType_RandomQuest100011 = 0xa2,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100012", Value=0xa3)]
        SceneScriptType_RandomQuest100012 = 0xa3,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100013", Value=0xa4)]
        SceneScriptType_RandomQuest100013 = 0xa4,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100014", Value=0xa5)]
        SceneScriptType_RandomQuest100014 = 0xa5,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100015", Value=0xa6)]
        SceneScriptType_RandomQuest100015 = 0xa6,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100016", Value=0xa7)]
        SceneScriptType_RandomQuest100016 = 0xa7,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100017", Value=0xa8)]
        SceneScriptType_RandomQuest100017 = 0xa8,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100018", Value=0xa9)]
        SceneScriptType_RandomQuest100018 = 0xa9,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100019", Value=170)]
        SceneScriptType_RandomQuest100019 = 170,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100020", Value=0xab)]
        SceneScriptType_RandomQuest100020 = 0xab,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100021", Value=0xac)]
        SceneScriptType_RandomQuest100021 = 0xac,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100022", Value=0xad)]
        SceneScriptType_RandomQuest100022 = 0xad,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100023", Value=0xae)]
        SceneScriptType_RandomQuest100023 = 0xae,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100024", Value=0xaf)]
        SceneScriptType_RandomQuest100024 = 0xaf,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100025", Value=0xb0)]
        SceneScriptType_RandomQuest100025 = 0xb0,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100026", Value=0xb1)]
        SceneScriptType_RandomQuest100026 = 0xb1,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100027", Value=0xb2)]
        SceneScriptType_RandomQuest100027 = 0xb2,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100028", Value=0xb3)]
        SceneScriptType_RandomQuest100028 = 0xb3,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100029", Value=180)]
        SceneScriptType_RandomQuest100029 = 180,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100030", Value=0xb5)]
        SceneScriptType_RandomQuest100030 = 0xb5,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100031", Value=0xb6)]
        SceneScriptType_RandomQuest100031 = 0xb6,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100032", Value=0xb7)]
        SceneScriptType_RandomQuest100032 = 0xb7,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100033", Value=0xb8)]
        SceneScriptType_RandomQuest100033 = 0xb8,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100034", Value=0xb9)]
        SceneScriptType_RandomQuest100034 = 0xb9,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100035", Value=0xba)]
        SceneScriptType_RandomQuest100035 = 0xba,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100036", Value=0xbb)]
        SceneScriptType_RandomQuest100036 = 0xbb,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100041", Value=0xbc)]
        SceneScriptType_RandomQuest100041 = 0xbc,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100043", Value=0xbd)]
        SceneScriptType_RandomQuest100043 = 0xbd,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100045", Value=190)]
        SceneScriptType_RandomQuest100045 = 190,
        [ProtoEnum(Name="SceneScriptType_RandomQuest100047", Value=0xbf)]
        SceneScriptType_RandomQuest100047 = 0xbf,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600001", Value=0xc0)]
        SceneScriptType_ScienceExplore600001 = 0xc0,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600002", Value=0xc1)]
        SceneScriptType_ScienceExplore600002 = 0xc1,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600003", Value=0xc2)]
        SceneScriptType_ScienceExplore600003 = 0xc2,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600004", Value=0xc3)]
        SceneScriptType_ScienceExplore600004 = 0xc3,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600005", Value=0xc4)]
        SceneScriptType_ScienceExplore600005 = 0xc4,
        [ProtoEnum(Name="SceneScriptType_ScienceExplore600006", Value=0xc5)]
        SceneScriptType_ScienceExplore600006 = 0xc5,
        [ProtoEnum(Name="SceneScriptType_GuildAction001", Value=0xc6)]
        SceneScriptType_GuildAction001 = 0xc6,
        [ProtoEnum(Name="SceneScriptType_GuildAction002", Value=0xc7)]
        SceneScriptType_GuildAction002 = 0xc7,
        [ProtoEnum(Name="SceneScriptType_GuildAction10101", Value=200)]
        SceneScriptType_GuildAction10101 = 200,
        [ProtoEnum(Name="SceneScriptType_GuildAction10102", Value=0xc9)]
        SceneScriptType_GuildAction10102 = 0xc9,
        [ProtoEnum(Name="SceneScriptType_GuildAction10103", Value=0xca)]
        SceneScriptType_GuildAction10103 = 0xca,
        [ProtoEnum(Name="SceneScriptType_GuildAction10201", Value=0xcb)]
        SceneScriptType_GuildAction10201 = 0xcb,
        [ProtoEnum(Name="SceneScriptType_GuildAction10301", Value=0xcc)]
        SceneScriptType_GuildAction10301 = 0xcc,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingHQ", Value=0xcd)]
        SceneScriptType_GuildBuildingHQ = 0xcd,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingMiningStation", Value=0xce)]
        SceneScriptType_GuildBuildingMiningStation = 0xce,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingFactory", Value=0xcf)]
        SceneScriptType_GuildBuildingFactory = 0xcf,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingSentry", Value=0xd0)]
        SceneScriptType_GuildBuildingSentry = 0xd0,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingShieldDestabiliser", Value=0xd1)]
        SceneScriptType_GuildBuildingShieldDestabiliser = 0xd1,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser", Value=210)]
        SceneScriptType_GuildBuildingControllShieldStabiliser = 210,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingFlagShipHangar", Value=0xd3)]
        SceneScriptType_GuildBuildingFlagShipHangar = 0xd3,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700001", Value=0xd4)]
        SceneScriptType_FactionCreditQuest700001 = 0xd4,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700002", Value=0xd5)]
        SceneScriptType_FactionCreditQuest700002 = 0xd5,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700003", Value=0xd6)]
        SceneScriptType_FactionCreditQuest700003 = 0xd6,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700005", Value=0xd7)]
        SceneScriptType_FactionCreditQuest700005 = 0xd7,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700006", Value=0xd8)]
        SceneScriptType_FactionCreditQuest700006 = 0xd8,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700007", Value=0xd9)]
        SceneScriptType_FactionCreditQuest700007 = 0xd9,
        [ProtoEnum(Name="SceneScriptType_FactionCreditQuest700008", Value=0xda)]
        SceneScriptType_FactionCreditQuest700008 = 0xda,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50102", Value=0xdb)]
        SceneScriptType_BranchStoryQuest50102 = 0xdb,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50104", Value=220)]
        SceneScriptType_BranchStoryQuest50104 = 220,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50202", Value=0xdd)]
        SceneScriptType_BranchStoryQuest50202 = 0xdd,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50203", Value=0xde)]
        SceneScriptType_BranchStoryQuest50203 = 0xde,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50302", Value=0xdf)]
        SceneScriptType_BranchStoryQuest50302 = 0xdf,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50304", Value=0xe0)]
        SceneScriptType_BranchStoryQuest50304 = 0xe0,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50402", Value=0xe1)]
        SceneScriptType_BranchStoryQuest50402 = 0xe1,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50404", Value=0xe2)]
        SceneScriptType_BranchStoryQuest50404 = 0xe2,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50405", Value=0xe3)]
        SceneScriptType_BranchStoryQuest50405 = 0xe3,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50505", Value=0xe4)]
        SceneScriptType_BranchStoryQuest50505 = 0xe4,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50702", Value=0xe5)]
        SceneScriptType_BranchStoryQuest50702 = 0xe5,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50802", Value=230)]
        SceneScriptType_BranchStoryQuest50802 = 230,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50805", Value=0xe7)]
        SceneScriptType_BranchStoryQuest50805 = 0xe7,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50806", Value=0xe8)]
        SceneScriptType_BranchStoryQuest50806 = 0xe8,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50902", Value=0xe9)]
        SceneScriptType_BranchStoryQuest50902 = 0xe9,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50904", Value=0xea)]
        SceneScriptType_BranchStoryQuest50904 = 0xea,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51002", Value=0xeb)]
        SceneScriptType_BranchStoryQuest51002 = 0xeb,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51004", Value=0xec)]
        SceneScriptType_BranchStoryQuest51004 = 0xec,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51006", Value=0xed)]
        SceneScriptType_BranchStoryQuest51006 = 0xed,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51203", Value=0xee)]
        SceneScriptType_BranchStoryQuest51203 = 0xee,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51402", Value=0xef)]
        SceneScriptType_BranchStoryQuest51402 = 0xef,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51404", Value=240)]
        SceneScriptType_BranchStoryQuest51404 = 240,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51501", Value=0xf1)]
        SceneScriptType_BranchStoryQuest51501 = 0xf1,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51502", Value=0xf2)]
        SceneScriptType_BranchStoryQuest51502 = 0xf2,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51503", Value=0xf3)]
        SceneScriptType_BranchStoryQuest51503 = 0xf3,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51602", Value=0xf4)]
        SceneScriptType_BranchStoryQuest51602 = 0xf4,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51604", Value=0xf5)]
        SceneScriptType_BranchStoryQuest51604 = 0xf5,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51605", Value=0xf6)]
        SceneScriptType_BranchStoryQuest51605 = 0xf6,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51802", Value=0xf7)]
        SceneScriptType_BranchStoryQuest51802 = 0xf7,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51804", Value=0xf8)]
        SceneScriptType_BranchStoryQuest51804 = 0xf8,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51806", Value=0xf9)]
        SceneScriptType_BranchStoryQuest51806 = 0xf9,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51904", Value=250)]
        SceneScriptType_BranchStoryQuest51904 = 250,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51906", Value=0xfb)]
        SceneScriptType_BranchStoryQuest51906 = 0xfb,
        [ProtoEnum(Name="SceneScriptType_Invade", Value=0xfc)]
        SceneScriptType_Invade = 0xfc,
        [ProtoEnum(Name="SceneScriptType_SpaceSignalGravity41000", Value=0xfd)]
        SceneScriptType_SpaceSignalGravity41000 = 0xfd,
        [ProtoEnum(Name="SceneScriptType_SpaceSignalGravity41001", Value=0xfe)]
        SceneScriptType_SpaceSignalGravity41001 = 0xfe,
        [ProtoEnum(Name="SceneScriptType_SpaceSignalMicroWave41002", Value=0xff)]
        SceneScriptType_SpaceSignalMicroWave41002 = 0xff,
        [ProtoEnum(Name="SceneScriptType_SpaceSignalRadiation41003", Value=0x100)]
        SceneScriptType_SpaceSignalRadiation41003 = 0x100,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50604", Value=0x101)]
        SceneScriptType_BranchStoryQuest50604 = 0x101,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest50605", Value=0x102)]
        SceneScriptType_BranchStoryQuest50605 = 0x102,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest52002", Value=0x103)]
        SceneScriptType_BranchStoryQuest52002 = 0x103,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest52003", Value=260)]
        SceneScriptType_BranchStoryQuest52003 = 260,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest52004", Value=0x105)]
        SceneScriptType_BranchStoryQuest52004 = 0x105,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest52006", Value=0x106)]
        SceneScriptType_BranchStoryQuest52006 = 0x106,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51702", Value=0x107)]
        SceneScriptType_BranchStoryQuest51702 = 0x107,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest51704", Value=0x108)]
        SceneScriptType_BranchStoryQuest51704 = 0x108,
        [ProtoEnum(Name="SceneScriptType_BranchStoryQuest52101", Value=0x109)]
        SceneScriptType_BranchStoryQuest52101 = 0x109,
        [ProtoEnum(Name="SceneScriptType_GuildTeleportStation", Value=0x10a)]
        SceneScriptType_GuildTeleportStation = 0x10a,
        [ProtoEnum(Name="SceneScriptType_GuildAntiTeleportStation", Value=0x10b)]
        SceneScriptType_GuildAntiTeleportStation = 0x10b,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser01", Value=0x10c)]
        SceneScriptType_GuildBuildingControllShieldStabiliser01 = 0x10c,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser02", Value=0x10d)]
        SceneScriptType_GuildBuildingControllShieldStabiliser02 = 0x10d,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser03", Value=270)]
        SceneScriptType_GuildBuildingControllShieldStabiliser03 = 270,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser04", Value=0x10f)]
        SceneScriptType_GuildBuildingControllShieldStabiliser04 = 0x10f,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser05", Value=0x110)]
        SceneScriptType_GuildBuildingControllShieldStabiliser05 = 0x110,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingControllShieldStabiliser06", Value=0x111)]
        SceneScriptType_GuildBuildingControllShieldStabiliser06 = 0x111,
        [ProtoEnum(Name="SceneScriptType_GuildBuildingTradePort", Value=0x112)]
        SceneScriptType_GuildBuildingTradePort = 0x112,
        [ProtoEnum(Name="SceneScriptType_GuildFlagShipTeleportFoothold", Value=0x113)]
        SceneScriptType_GuildFlagShipTeleportFoothold = 0x113,
        [ProtoEnum(Name="SceneScriptType_MAX", Value=0x114)]
        SceneScriptType_MAX = 0x114
    }
}

