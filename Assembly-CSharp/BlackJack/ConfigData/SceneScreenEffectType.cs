﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneScreenEffectType")]
    public enum SceneScreenEffectType
    {
        [ProtoEnum(Name="SceneScreenEffectType_FireBurning", Value=1)]
        SceneScreenEffectType_FireBurning = 1,
        [ProtoEnum(Name="SceneScreenEffectType_Max", Value=2)]
        SceneScreenEffectType_Max = 2
    }
}

