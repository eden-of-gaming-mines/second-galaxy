﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataWormholeSolarSystemInfo")]
    public class ConfigDataWormholeSolarSystemInfo : IExtensible
    {
        private int _ID;
        private int _WormholeTunnelSceneID;
        private readonly List<SceneCountParamList> _WormholeSceneCountParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_WormholeTunnelSceneID;
        private static DelegateBridge __Hotfix_set_WormholeTunnelSceneID;
        private static DelegateBridge __Hotfix_get_WormholeSceneCountParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="WormholeTunnelSceneID", DataFormat=DataFormat.TwosComplement)]
        public int WormholeTunnelSceneID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="WormholeSceneCountParamList", DataFormat=DataFormat.Default)]
        public List<SceneCountParamList> WormholeSceneCountParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

