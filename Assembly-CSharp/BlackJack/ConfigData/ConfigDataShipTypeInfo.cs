﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipTypeInfo")]
    public class ConfigDataShipTypeInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.ShipType _ShipType;
        private string _Icon;
        private float _ShipFightTime;
        private float _ShipFlexibleMulit1;
        private float _ShipFlexibleMulit2;
        private float _ShipFlexibleMulit3;
        private readonly List<ShipTypeInfoShipTypeInvadeArroundDistanceList> _ShipTypeInvadeArroundDistanceList;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShipType;
        private static DelegateBridge __Hotfix_set_ShipType;
        private static DelegateBridge __Hotfix_get_Icon;
        private static DelegateBridge __Hotfix_set_Icon;
        private static DelegateBridge __Hotfix_get_ShipFightTime;
        private static DelegateBridge __Hotfix_set_ShipFightTime;
        private static DelegateBridge __Hotfix_get_ShipFlexibleMulit1;
        private static DelegateBridge __Hotfix_set_ShipFlexibleMulit1;
        private static DelegateBridge __Hotfix_get_ShipFlexibleMulit2;
        private static DelegateBridge __Hotfix_set_ShipFlexibleMulit2;
        private static DelegateBridge __Hotfix_get_ShipFlexibleMulit3;
        private static DelegateBridge __Hotfix_set_ShipFlexibleMulit3;
        private static DelegateBridge __Hotfix_get_ShipTypeInvadeArroundDistanceList;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.ShipType ShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Icon", DataFormat=DataFormat.Default)]
        public string Icon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ShipFightTime", DataFormat=DataFormat.FixedSize)]
        public float ShipFightTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ShipFlexibleMulit1", DataFormat=DataFormat.FixedSize)]
        public float ShipFlexibleMulit1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ShipFlexibleMulit2", DataFormat=DataFormat.FixedSize)]
        public float ShipFlexibleMulit2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ShipFlexibleMulit3", DataFormat=DataFormat.FixedSize)]
        public float ShipFlexibleMulit3
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="ShipTypeInvadeArroundDistanceList", DataFormat=DataFormat.Default)]
        public List<ShipTypeInfoShipTypeInvadeArroundDistanceList> ShipTypeInvadeArroundDistanceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

