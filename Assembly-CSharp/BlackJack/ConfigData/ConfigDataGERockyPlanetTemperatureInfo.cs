﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGERockyPlanetTemperatureInfo")]
    public class ConfigDataGERockyPlanetTemperatureInfo : IExtensible
    {
        private int _ID;
        private readonly List<GERockyPlanetTemperatureInfoOrbitRadiusRange> _OrbitRadiusRange;
        private int _StandardTemperature;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_OrbitRadiusRange;
        private static DelegateBridge __Hotfix_get_StandardTemperature;
        private static DelegateBridge __Hotfix_set_StandardTemperature;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="OrbitRadiusRange", DataFormat=DataFormat.Default)]
        public List<GERockyPlanetTemperatureInfoOrbitRadiusRange> OrbitRadiusRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StandardTemperature", DataFormat=DataFormat.TwosComplement)]
        public int StandardTemperature
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

