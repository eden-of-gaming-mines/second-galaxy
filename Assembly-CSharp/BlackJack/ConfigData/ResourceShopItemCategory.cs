﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ResourceShopItemCategory")]
    public enum ResourceShopItemCategory
    {
        [ProtoEnum(Name="ResourceShopItemCategory_PersonalShop", Value=1)]
        ResourceShopItemCategory_PersonalShop = 1,
        [ProtoEnum(Name="ResourceShopItemCategory_GuildShop", Value=2)]
        ResourceShopItemCategory_GuildShop = 2,
        [ProtoEnum(Name="ResourceShopItemCategory_CatalystItem", Value=3)]
        ResourceShopItemCategory_CatalystItem = 3,
        [ProtoEnum(Name="ResourceShopItemCategory_AccelerationItem", Value=4)]
        ResourceShopItemCategory_AccelerationItem = 4,
        [ProtoEnum(Name="ResourceShopItemCategory_CaptainItem", Value=5)]
        ResourceShopItemCategory_CaptainItem = 5,
        [ProtoEnum(Name="ResourceShopItemCategory_OtherItem", Value=6)]
        ResourceShopItemCategory_OtherItem = 6,
        [ProtoEnum(Name="ResourceShopItemCategory_Max", Value=7)]
        ResourceShopItemCategory_Max = 7
    }
}

