﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBufEffectInfo")]
    public class ConfigDataBufEffectInfo : IExtensible
    {
        private int _ID;
        private string _AttachEffect;
        private string _ContinueEffect;
        private string _ChannelEffect;
        private string _DettachEffect;
        private int _GroupId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AttachEffect;
        private static DelegateBridge __Hotfix_set_AttachEffect;
        private static DelegateBridge __Hotfix_get_ContinueEffect;
        private static DelegateBridge __Hotfix_set_ContinueEffect;
        private static DelegateBridge __Hotfix_get_ChannelEffect;
        private static DelegateBridge __Hotfix_set_ChannelEffect;
        private static DelegateBridge __Hotfix_get_DettachEffect;
        private static DelegateBridge __Hotfix_set_DettachEffect;
        private static DelegateBridge __Hotfix_get_GroupId;
        private static DelegateBridge __Hotfix_set_GroupId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AttachEffect", DataFormat=DataFormat.Default)]
        public string AttachEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ContinueEffect", DataFormat=DataFormat.Default)]
        public string ContinueEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ChannelEffect", DataFormat=DataFormat.Default)]
        public string ChannelEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DettachEffect", DataFormat=DataFormat.Default)]
        public string DettachEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GroupId", DataFormat=DataFormat.TwosComplement)]
        public int GroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

