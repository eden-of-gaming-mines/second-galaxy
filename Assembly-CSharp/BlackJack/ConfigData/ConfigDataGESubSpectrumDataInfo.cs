﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGESubSpectrumDataInfo")]
    public class ConfigDataGESubSpectrumDataInfo : IExtensible
    {
        private int _ID;
        private int _BelongSpectrumID;
        private string _SubSpectrumName;
        private double _RadiusRatio;
        private double _BrightnessRatio;
        private int _StandardTemperature;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BelongSpectrumID;
        private static DelegateBridge __Hotfix_set_BelongSpectrumID;
        private static DelegateBridge __Hotfix_get_SubSpectrumName;
        private static DelegateBridge __Hotfix_set_SubSpectrumName;
        private static DelegateBridge __Hotfix_get_RadiusRatio;
        private static DelegateBridge __Hotfix_set_RadiusRatio;
        private static DelegateBridge __Hotfix_get_BrightnessRatio;
        private static DelegateBridge __Hotfix_set_BrightnessRatio;
        private static DelegateBridge __Hotfix_get_StandardTemperature;
        private static DelegateBridge __Hotfix_set_StandardTemperature;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BelongSpectrumID", DataFormat=DataFormat.TwosComplement)]
        public int BelongSpectrumID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SubSpectrumName", DataFormat=DataFormat.Default)]
        public string SubSpectrumName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="RadiusRatio", DataFormat=DataFormat.TwosComplement)]
        public double RadiusRatio
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BrightnessRatio", DataFormat=DataFormat.TwosComplement)]
        public double BrightnessRatio
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="StandardTemperature", DataFormat=DataFormat.TwosComplement)]
        public int StandardTemperature
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

