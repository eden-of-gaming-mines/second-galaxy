﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCurrencyInfo")]
    public class ConfigDataCurrencyInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.CurrencyType _CurrencyType;
        private string _Icon;
        private string _Name;
        private double _ExchangeRate;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private string _IssuerDesc;
        private string _MonetaryUsageDesc;
        private string _AcquisitionPathDesc;
        private string _NameStrKey;
        private string _IssuerDescStrKey;
        private string _MonetaryUsageDescStrKey;
        private string _AcquisitionPathDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_CurrencyType;
        private static DelegateBridge __Hotfix_set_CurrencyType;
        private static DelegateBridge __Hotfix_get_Icon;
        private static DelegateBridge __Hotfix_set_Icon;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_ExchangeRate;
        private static DelegateBridge __Hotfix_set_ExchangeRate;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_IssuerDesc;
        private static DelegateBridge __Hotfix_set_IssuerDesc;
        private static DelegateBridge __Hotfix_get_MonetaryUsageDesc;
        private static DelegateBridge __Hotfix_set_MonetaryUsageDesc;
        private static DelegateBridge __Hotfix_get_AcquisitionPathDesc;
        private static DelegateBridge __Hotfix_set_AcquisitionPathDesc;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_IssuerDescStrKey;
        private static DelegateBridge __Hotfix_set_IssuerDescStrKey;
        private static DelegateBridge __Hotfix_get_MonetaryUsageDescStrKey;
        private static DelegateBridge __Hotfix_set_MonetaryUsageDescStrKey;
        private static DelegateBridge __Hotfix_get_AcquisitionPathDescStrKey;
        private static DelegateBridge __Hotfix_set_AcquisitionPathDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CurrencyType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.CurrencyType CurrencyType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Icon", DataFormat=DataFormat.Default)]
        public string Icon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ExchangeRate", DataFormat=DataFormat.TwosComplement)]
        public double ExchangeRate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IssuerDesc", DataFormat=DataFormat.Default)]
        public string IssuerDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="MonetaryUsageDesc", DataFormat=DataFormat.Default)]
        public string MonetaryUsageDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="AcquisitionPathDesc", DataFormat=DataFormat.Default)]
        public string AcquisitionPathDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="IssuerDescStrKey", DataFormat=DataFormat.Default)]
        public string IssuerDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="MonetaryUsageDescStrKey", DataFormat=DataFormat.Default)]
        public string MonetaryUsageDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="AcquisitionPathDescStrKey", DataFormat=DataFormat.Default)]
        public string AcquisitionPathDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

