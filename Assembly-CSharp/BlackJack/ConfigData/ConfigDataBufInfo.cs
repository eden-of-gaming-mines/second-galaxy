﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBufInfo")]
    public class ConfigDataBufInfo : IExtensible
    {
        private int _ID;
        private BufType _Type;
        private ShipFightBufType _FightBufType;
        private bool _InSpaceBuf;
        private uint _Flag;
        private int _GamePlayFlag;
        private uint _BufPower;
        private uint _LifeTime;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private readonly List<int> _PropertiesDescPropertyList;
        private float _BufParam1;
        private int _BufEffectID;
        private int _DisplayPriorityLevel;
        private int _ContinueBuf;
        private bool _DetachOnLeaveScene;
        private int _GroupId;
        private int _LevelInGroup;
        private bool _NotDisplayOnBufInfoPanel;
        private string _NameStrKey;
        private string _PropertiesDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_FightBufType;
        private static DelegateBridge __Hotfix_set_FightBufType;
        private static DelegateBridge __Hotfix_get_InSpaceBuf;
        private static DelegateBridge __Hotfix_set_InSpaceBuf;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_get_GamePlayFlag;
        private static DelegateBridge __Hotfix_set_GamePlayFlag;
        private static DelegateBridge __Hotfix_get_BufPower;
        private static DelegateBridge __Hotfix_set_BufPower;
        private static DelegateBridge __Hotfix_get_LifeTime;
        private static DelegateBridge __Hotfix_set_LifeTime;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_PropertiesDescPropertyList;
        private static DelegateBridge __Hotfix_get_BufParam1;
        private static DelegateBridge __Hotfix_set_BufParam1;
        private static DelegateBridge __Hotfix_get_BufEffectID;
        private static DelegateBridge __Hotfix_set_BufEffectID;
        private static DelegateBridge __Hotfix_get_DisplayPriorityLevel;
        private static DelegateBridge __Hotfix_set_DisplayPriorityLevel;
        private static DelegateBridge __Hotfix_get_ContinueBuf;
        private static DelegateBridge __Hotfix_set_ContinueBuf;
        private static DelegateBridge __Hotfix_get_DetachOnLeaveScene;
        private static DelegateBridge __Hotfix_set_DetachOnLeaveScene;
        private static DelegateBridge __Hotfix_get_GroupId;
        private static DelegateBridge __Hotfix_set_GroupId;
        private static DelegateBridge __Hotfix_get_LevelInGroup;
        private static DelegateBridge __Hotfix_set_LevelInGroup;
        private static DelegateBridge __Hotfix_get_NotDisplayOnBufInfoPanel;
        private static DelegateBridge __Hotfix_set_NotDisplayOnBufInfoPanel;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_PropertiesDescStrKey;
        private static DelegateBridge __Hotfix_set_PropertiesDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public BufType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="FightBufType", DataFormat=DataFormat.TwosComplement)]
        public ShipFightBufType FightBufType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="InSpaceBuf", DataFormat=DataFormat.Default)]
        public bool InSpaceBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public uint Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GamePlayFlag", DataFormat=DataFormat.TwosComplement)]
        public int GamePlayFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BufPower", DataFormat=DataFormat.TwosComplement)]
        public uint BufPower
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="LifeTime", DataFormat=DataFormat.TwosComplement)]
        public uint LifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="PropertiesDescPropertyList", DataFormat=DataFormat.TwosComplement)]
        public List<int> PropertiesDescPropertyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="BufParam1", DataFormat=DataFormat.FixedSize)]
        public float BufParam1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="BufEffectID", DataFormat=DataFormat.TwosComplement)]
        public int BufEffectID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="DisplayPriorityLevel", DataFormat=DataFormat.TwosComplement)]
        public int DisplayPriorityLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="ContinueBuf", DataFormat=DataFormat.TwosComplement)]
        public int ContinueBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="DetachOnLeaveScene", DataFormat=DataFormat.Default)]
        public bool DetachOnLeaveScene
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="GroupId", DataFormat=DataFormat.TwosComplement)]
        public int GroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="LevelInGroup", DataFormat=DataFormat.TwosComplement)]
        public int LevelInGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="NotDisplayOnBufInfoPanel", DataFormat=DataFormat.Default)]
        public bool NotDisplayOnBufInfoPanel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="PropertiesDescStrKey", DataFormat=DataFormat.Default)]
        public string PropertiesDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

