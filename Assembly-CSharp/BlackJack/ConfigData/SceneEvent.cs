﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneEvent")]
    public enum SceneEvent
    {
        [ProtoEnum(Name="SceneEvent_None", Value=0)]
        SceneEvent_None = 0,
        [ProtoEnum(Name="SceneEvent_Enter", Value=1)]
        SceneEvent_Enter = 1,
        [ProtoEnum(Name="SceneEvent_leave", Value=2)]
        SceneEvent_leave = 2,
        [ProtoEnum(Name="SceneEvent_InvestigationEnd", Value=3)]
        SceneEvent_InvestigationEnd = 3,
        [ProtoEnum(Name="SceneEvent_NpcTalkerEnd", Value=4)]
        SceneEvent_NpcTalkerEnd = 4,
        [ProtoEnum(Name="SceneEvent_MonsterTeamClear", Value=5)]
        SceneEvent_MonsterTeamClear = 5,
        [ProtoEnum(Name="SceneEvent_ConveyEnd", Value=6)]
        SceneEvent_ConveyEnd = 6,
        [ProtoEnum(Name="SceneEvent_WormholeExploreComplete", Value=7)]
        SceneEvent_WormholeExploreComplete = 7,
        [ProtoEnum(Name="SceneEvent_TimeProcessEnd", Value=8)]
        SceneEvent_TimeProcessEnd = 8,
        [ProtoEnum(Name="SceneEvent_RareWormholeExploreComplete", Value=9)]
        SceneEvent_RareWormholeExploreComplete = 9
    }
}

