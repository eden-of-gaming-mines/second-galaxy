﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcTalkerResInfo")]
    public class ConfigDataNpcTalkerResInfo : IExtensible
    {
        private int _ID;
        private string _ResPath;
        private bool _CanBeHireCaptain;
        private bool _CanBeNpcTalker;
        private bool _CanBePlayer;
        private readonly List<SubRankType> _SubRankList;
        private readonly List<GrandFaction> _RaceList;
        private GenderType _Gender;
        private BlackJack.ConfigData.AgeType _AgeType;
        private readonly List<ProfessionType> _ProfessionList;
        private readonly List<NpcFunctionType> _FunctionTypeList;
        private readonly List<NpcPersonalityType> _PersonalityList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ResPath;
        private static DelegateBridge __Hotfix_set_ResPath;
        private static DelegateBridge __Hotfix_get_CanBeHireCaptain;
        private static DelegateBridge __Hotfix_set_CanBeHireCaptain;
        private static DelegateBridge __Hotfix_get_CanBeNpcTalker;
        private static DelegateBridge __Hotfix_set_CanBeNpcTalker;
        private static DelegateBridge __Hotfix_get_CanBePlayer;
        private static DelegateBridge __Hotfix_set_CanBePlayer;
        private static DelegateBridge __Hotfix_get_SubRankList;
        private static DelegateBridge __Hotfix_get_RaceList;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_AgeType;
        private static DelegateBridge __Hotfix_set_AgeType;
        private static DelegateBridge __Hotfix_get_ProfessionList;
        private static DelegateBridge __Hotfix_get_FunctionTypeList;
        private static DelegateBridge __Hotfix_get_PersonalityList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ResPath", DataFormat=DataFormat.Default)]
        public string ResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CanBeHireCaptain", DataFormat=DataFormat.Default)]
        public bool CanBeHireCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CanBeNpcTalker", DataFormat=DataFormat.Default)]
        public bool CanBeNpcTalker
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="CanBePlayer", DataFormat=DataFormat.Default)]
        public bool CanBePlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="SubRankList", DataFormat=DataFormat.TwosComplement)]
        public List<SubRankType> SubRankList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="RaceList", DataFormat=DataFormat.TwosComplement)]
        public List<GrandFaction> RaceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Gender", DataFormat=DataFormat.TwosComplement)]
        public GenderType Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="AgeType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.AgeType AgeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="ProfessionList", DataFormat=DataFormat.TwosComplement)]
        public List<ProfessionType> ProfessionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="FunctionTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<NpcFunctionType> FunctionTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="PersonalityList", DataFormat=DataFormat.TwosComplement)]
        public List<NpcPersonalityType> PersonalityList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

