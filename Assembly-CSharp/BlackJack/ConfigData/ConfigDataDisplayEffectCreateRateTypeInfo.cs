﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDisplayEffectCreateRateTypeInfo")]
    public class ConfigDataDisplayEffectCreateRateTypeInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.DisplayEffectCreateRateType _DisplayEffectCreateRateType;
        private readonly List<float> _DisplayCountLimitList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_DisplayEffectCreateRateType;
        private static DelegateBridge __Hotfix_set_DisplayEffectCreateRateType;
        private static DelegateBridge __Hotfix_get_DisplayCountLimitList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DisplayEffectCreateRateType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.DisplayEffectCreateRateType DisplayEffectCreateRateType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DisplayCountLimitList", DataFormat=DataFormat.FixedSize)]
        public List<float> DisplayCountLimitList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

