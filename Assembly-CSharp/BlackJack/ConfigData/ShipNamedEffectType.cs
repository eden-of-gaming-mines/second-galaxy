﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipNamedEffectType")]
    public enum ShipNamedEffectType
    {
        [ProtoEnum(Name="ShipNamedEffectType_None", Value=0)]
        ShipNamedEffectType_None = 0,
        [ProtoEnum(Name="ShipNamedEffectType_EngineBooster", Value=1)]
        ShipNamedEffectType_EngineBooster = 1,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkCharge", Value=2)]
        ShipNamedEffectType_BlinkCharge = 2,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkStart", Value=3)]
        ShipNamedEffectType_BlinkStart = 3,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkEnd", Value=4)]
        ShipNamedEffectType_BlinkEnd = 4,
        [ProtoEnum(Name="ShipNamedEffectType_ShieldRecover", Value=5)]
        ShipNamedEffectType_ShieldRecover = 5,
        [ProtoEnum(Name="ShipNamedEffectType_ShieldResistUp", Value=6)]
        ShipNamedEffectType_ShieldResistUp = 6,
        [ProtoEnum(Name="ShipNamedEffectType_ShieldResistDown", Value=7)]
        ShipNamedEffectType_ShieldResistDown = 7,
        [ProtoEnum(Name="ShipNamedEffectType_ArmorOnHit", Value=8)]
        ShipNamedEffectType_ArmorOnHit = 8,
        [ProtoEnum(Name="ShipNamedEffectType_ArmorRecover", Value=9)]
        ShipNamedEffectType_ArmorRecover = 9,
        [ProtoEnum(Name="ShipNamedEffectType_ArmorResistUp", Value=10)]
        ShipNamedEffectType_ArmorResistUp = 10,
        [ProtoEnum(Name="ShipNamedEffectType_ArmorResistDown", Value=11)]
        ShipNamedEffectType_ArmorResistDown = 11,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopAttackUp", Value=12)]
        ShipNamedEffectType_BuffLoopAttackUp = 12,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopAttackDown", Value=13)]
        ShipNamedEffectType_BuffLoopAttackDown = 13,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopDefenseUp", Value=14)]
        ShipNamedEffectType_BuffLoopDefenseUp = 14,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopDefenseDown", Value=15)]
        ShipNamedEffectType_BuffLoopDefenseDown = 15,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopSpeedUp", Value=0x10)]
        ShipNamedEffectType_BuffLoopSpeedUp = 0x10,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopSpeedDown", Value=0x11)]
        ShipNamedEffectType_BuffLoopSpeedDown = 0x11,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopElectronicUp", Value=0x12)]
        ShipNamedEffectType_BuffLoopElectronicUp = 0x12,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopElectronicDown", Value=0x13)]
        ShipNamedEffectType_BuffLoopElectronicDown = 0x13,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopJumpDisturber", Value=20)]
        ShipNamedEffectType_BuffLoopJumpDisturber = 20,
        [ProtoEnum(Name="ShipNamedEffectType_BuffLoopEnergyDec", Value=0x15)]
        ShipNamedEffectType_BuffLoopEnergyDec = 0x15,
        [ProtoEnum(Name="ShipNamedEffectType_ShipFakeJumpIn", Value=0x16)]
        ShipNamedEffectType_ShipFakeJumpIn = 0x16,
        [ProtoEnum(Name="ShipNamedEffectType_ShipFakeJumpOut", Value=0x17)]
        ShipNamedEffectType_ShipFakeJumpOut = 0x17,
        [ProtoEnum(Name="ShipNamedEffectType_ShipFakeFadeIn", Value=0x18)]
        ShipNamedEffectType_ShipFakeFadeIn = 0x18,
        [ProtoEnum(Name="ShipNamedEffectType_ShipFakeFadeOut", Value=0x19)]
        ShipNamedEffectType_ShipFakeFadeOut = 0x19,
        [ProtoEnum(Name="ShipNamedEffectType_ShipInvisibleStart", Value=0x1a)]
        ShipNamedEffectType_ShipInvisibleStart = 0x1a,
        [ProtoEnum(Name="ShipNamedEffectType_ShipInvisibleEnd", Value=0x1b)]
        ShipNamedEffectType_ShipInvisibleEnd = 0x1b,
        [ProtoEnum(Name="ShipNamedEffectType_ShipInvisibleCharing", Value=0x1c)]
        ShipNamedEffectType_ShipInvisibleCharing = 0x1c,
        [ProtoEnum(Name="ShipNamedEffectType_ShipInvisibleCharingInterrupt", Value=0x1d)]
        ShipNamedEffectType_ShipInvisibleCharingInterrupt = 0x1d,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkCharge_SuperEquip", Value=30)]
        ShipNamedEffectType_BlinkCharge_SuperEquip = 30,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkStart_SuperEquip", Value=0x1f)]
        ShipNamedEffectType_BlinkStart_SuperEquip = 0x1f,
        [ProtoEnum(Name="ShipNamedEffectType_BlinkEnd_SuperEquip", Value=0x20)]
        ShipNamedEffectType_BlinkEnd_SuperEquip = 0x20,
        [ProtoEnum(Name="ShipNamedEffectType_GuildBuildingStatus", Value=0x21)]
        ShipNamedEffectType_GuildBuildingStatus = 0x21,
        [ProtoEnum(Name="ShipNamedEffectType_FlagShipBlinkCharge", Value=0x22)]
        ShipNamedEffectType_FlagShipBlinkCharge = 0x22,
        [ProtoEnum(Name="ShipNamedEffectType_FlagShipBlinkStart", Value=0x23)]
        ShipNamedEffectType_FlagShipBlinkStart = 0x23,
        [ProtoEnum(Name="ShipNamedEffectType_FlagShipBlinkEnd", Value=0x24)]
        ShipNamedEffectType_FlagShipBlinkEnd = 0x24
    }
}

