﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="EquipFunctionType")]
    public enum EquipFunctionType
    {
        [ProtoEnum(Name="EquipFunctionType_Passive", Value=1)]
        EquipFunctionType_Passive = 1,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2Self", Value=2)]
        EquipFunctionType_AttachBuf2Self = 2,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2EnemyTarget", Value=3)]
        EquipFunctionType_AttachBuf2EnemyTarget = 3,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2NoenemyTarget", Value=4)]
        EquipFunctionType_AttachBuf2NoenemyTarget = 4,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2TeamMamber", Value=5)]
        EquipFunctionType_AttachBuf2TeamMamber = 5,
        [ProtoEnum(Name="EquipFunctionType_Blink", Value=6)]
        EquipFunctionType_Blink = 6,
        [ProtoEnum(Name="EquipFunctionType_ClearCD4HightSlot", Value=7)]
        EquipFunctionType_ClearCD4HightSlot = 7,
        [ProtoEnum(Name="EquipFunctionType_StartAllWeaponEquipGroupCD", Value=8)]
        EquipFunctionType_StartAllWeaponEquipGroupCD = 8,
        [ProtoEnum(Name="EquipFunctionType_Bomb", Value=9)]
        EquipFunctionType_Bomb = 9,
        [ProtoEnum(Name="EquipFunctionType_EMP4Shield", Value=10)]
        EquipFunctionType_EMP4Shield = 10,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2EnemyListAround", Value=11)]
        EquipFunctionType_AttachBuf2EnemyListAround = 11,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2AroundFriend", Value=12)]
        EquipFunctionType_AttachBuf2AroundFriend = 12,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2Around", Value=13)]
        EquipFunctionType_AttachBuf2Around = 13,
        [ProtoEnum(Name="EquipFunctionType_EMP4Energy", Value=14)]
        EquipFunctionType_EMP4Energy = 14,
        [ProtoEnum(Name="EquipFunctionType_Invisibility", Value=15)]
        EquipFunctionType_Invisibility = 15,
        [ProtoEnum(Name="EquipFunctionType_AttachBuf2EnemyListAroundAndClearInvisible", Value=0x10)]
        EquipFunctionType_AttachBuf2EnemyListAroundAndClearInvisible = 0x10,
        [ProtoEnum(Name="EquipFunctionType_Max", Value=0x11)]
        EquipFunctionType_Max = 0x11
    }
}

