﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildCurrencyLogTypeInfo")]
    public class ConfigDataGuildCurrencyLogTypeInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.GuildCurrencyLogType _GuildCurrencyLogType;
        private string _Desc;
        private bool _IsPayments;
        private BlackJack.ConfigData.GuildCurrencyLogFilterType _GuildCurrencyLogFilterType;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GuildCurrencyLogType;
        private static DelegateBridge __Hotfix_set_GuildCurrencyLogType;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_IsPayments;
        private static DelegateBridge __Hotfix_set_IsPayments;
        private static DelegateBridge __Hotfix_get_GuildCurrencyLogFilterType;
        private static DelegateBridge __Hotfix_set_GuildCurrencyLogFilterType;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GuildCurrencyLogType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GuildCurrencyLogType GuildCurrencyLogType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="IsPayments", DataFormat=DataFormat.Default)]
        public bool IsPayments
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GuildCurrencyLogFilterType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GuildCurrencyLogFilterType GuildCurrencyLogFilterType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

