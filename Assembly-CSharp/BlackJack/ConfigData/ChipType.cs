﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ChipType")]
    public enum ChipType
    {
        [ProtoEnum(Name="ChipType_MissileEffect", Value=1)]
        ChipType_MissileEffect = 1,
        [ProtoEnum(Name="ChipType_RailgunEffect", Value=2)]
        ChipType_RailgunEffect = 2,
        [ProtoEnum(Name="ChipType_PlasmaEffect", Value=3)]
        ChipType_PlasmaEffect = 3,
        [ProtoEnum(Name="ChipType_LaserEffect", Value=4)]
        ChipType_LaserEffect = 4,
        [ProtoEnum(Name="ChipType_ReduceDefenceEffect", Value=5)]
        ChipType_ReduceDefenceEffect = 5,
        [ProtoEnum(Name="ChipType_ShieldResistEffect", Value=6)]
        ChipType_ShieldResistEffect = 6,
        [ProtoEnum(Name="ChipType_ShieldMaxValueEffect", Value=7)]
        ChipType_ShieldMaxValueEffect = 7,
        [ProtoEnum(Name="ChipType_EnergyLimitEffect", Value=8)]
        ChipType_EnergyLimitEffect = 8,
        [ProtoEnum(Name="ChipType_WeaponUseCostEffect", Value=9)]
        ChipType_WeaponUseCostEffect = 9,
        [ProtoEnum(Name="ChipType_MaxSpeedEffect", Value=10)]
        ChipType_MaxSpeedEffect = 10,
        [ProtoEnum(Name="ChipType_ShipVolumeEffect", Value=11)]
        ChipType_ShipVolumeEffect = 11,
        [ProtoEnum(Name="ChipType_Max", Value=12)]
        ChipType_Max = 12
    }
}

