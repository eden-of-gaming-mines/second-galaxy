﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SystemMessageType")]
    public enum SystemMessageType
    {
        [ProtoEnum(Name="SystemMessageType_Invalid", Value=0)]
        SystemMessageType_Invalid = 0,
        [ProtoEnum(Name="SystemMessageType_ClientOnly", Value=1)]
        SystemMessageType_ClientOnly = 1,
        [ProtoEnum(Name="SystemMessageType_GameEvent", Value=2)]
        SystemMessageType_GameEvent = 2,
        [ProtoEnum(Name="SystemMessageType_ServerPush", Value=3)]
        SystemMessageType_ServerPush = 3
    }
}

