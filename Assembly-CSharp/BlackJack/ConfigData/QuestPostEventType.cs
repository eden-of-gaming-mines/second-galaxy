﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestPostEventType")]
    public enum QuestPostEventType
    {
        [ProtoEnum(Name="QuestPostEventType_PushQuest", Value=1)]
        QuestPostEventType_PushQuest = 1,
        [ProtoEnum(Name="QuestPostEventType_CloseEnv", Value=2)]
        QuestPostEventType_CloseEnv = 2,
        [ProtoEnum(Name="QuestPostEventType_PushQuestWithLevel", Value=3)]
        QuestPostEventType_PushQuestWithLevel = 3,
        [ProtoEnum(Name="QuestPostEventType_SendEmail", Value=4)]
        QuestPostEventType_SendEmail = 4
    }
}

