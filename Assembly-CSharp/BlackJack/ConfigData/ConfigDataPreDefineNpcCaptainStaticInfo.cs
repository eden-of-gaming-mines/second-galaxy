﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPreDefineNpcCaptainStaticInfo")]
    public class ConfigDataPreDefineNpcCaptainStaticInfo : IExtensible
    {
        private int _ID;
        private int _ResId;
        private SubRankType _SubRank;
        private int _LastNameId;
        private int _FirstNameId;
        private GrandFaction _GrandFactions;
        private int _Age;
        private string _BirthDay;
        private ProfessionType _Profession;
        private NpcPersonalityType _Personality;
        private readonly List<int> _ShipGrowthLineId;
        private readonly List<PreDefineNpcCaptainStaticInfoInitFeats> _InitFeats;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ResId;
        private static DelegateBridge __Hotfix_set_ResId;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_LastNameId;
        private static DelegateBridge __Hotfix_set_LastNameId;
        private static DelegateBridge __Hotfix_get_FirstNameId;
        private static DelegateBridge __Hotfix_set_FirstNameId;
        private static DelegateBridge __Hotfix_get_GrandFactions;
        private static DelegateBridge __Hotfix_set_GrandFactions;
        private static DelegateBridge __Hotfix_get_Age;
        private static DelegateBridge __Hotfix_set_Age;
        private static DelegateBridge __Hotfix_get_BirthDay;
        private static DelegateBridge __Hotfix_set_BirthDay;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_Personality;
        private static DelegateBridge __Hotfix_set_Personality;
        private static DelegateBridge __Hotfix_get_ShipGrowthLineId;
        private static DelegateBridge __Hotfix_get_InitFeats;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ResId", DataFormat=DataFormat.TwosComplement)]
        public int ResId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LastNameId", DataFormat=DataFormat.TwosComplement)]
        public int LastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="FirstNameId", DataFormat=DataFormat.TwosComplement)]
        public int FirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="GrandFactions", DataFormat=DataFormat.TwosComplement)]
        public GrandFaction GrandFactions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Age", DataFormat=DataFormat.TwosComplement)]
        public int Age
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BirthDay", DataFormat=DataFormat.Default)]
        public string BirthDay
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public ProfessionType Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Personality", DataFormat=DataFormat.TwosComplement)]
        public NpcPersonalityType Personality
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="ShipGrowthLineId", DataFormat=DataFormat.TwosComplement)]
        public List<int> ShipGrowthLineId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="InitFeats", DataFormat=DataFormat.Default)]
        public List<PreDefineNpcCaptainStaticInfoInitFeats> InitFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

