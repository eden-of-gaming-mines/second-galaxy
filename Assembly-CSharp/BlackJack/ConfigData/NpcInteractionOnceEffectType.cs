﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcInteractionOnceEffectType")]
    public enum NpcInteractionOnceEffectType
    {
        [ProtoEnum(Name="NpcInteractionOnceEffectType_Normal", Value=0)]
        NpcInteractionOnceEffectType_Normal = 0,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_EnergyChange", Value=1)]
        NpcInteractionOnceEffectType_EnergyChange = 1,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_EnergyChangePercent", Value=2)]
        NpcInteractionOnceEffectType_EnergyChangePercent = 2,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_ShieldChange", Value=3)]
        NpcInteractionOnceEffectType_ShieldChange = 3,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_ShieldChangePercent", Value=4)]
        NpcInteractionOnceEffectType_ShieldChangePercent = 4,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_ArmorChange", Value=5)]
        NpcInteractionOnceEffectType_ArmorChange = 5,
        [ProtoEnum(Name="NpcInteractionOnceEffectType_ArmorChangePercent", Value=6)]
        NpcInteractionOnceEffectType_ArmorChangePercent = 6
    }
}

