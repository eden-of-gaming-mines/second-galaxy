﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipNamedEffectTypeInfo")]
    public class ConfigDataShipNamedEffectTypeInfo : IExtensible
    {
        private int _ID;
        private ShipNamedEffectType _EffectType;
        private string _Name;
        private string _EffetAssetResPath;
        private string _EffetAudioResPath;
        private bool _IsAudioLoop;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_EffectType;
        private static DelegateBridge __Hotfix_set_EffectType;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_EffetAssetResPath;
        private static DelegateBridge __Hotfix_set_EffetAssetResPath;
        private static DelegateBridge __Hotfix_get_EffetAudioResPath;
        private static DelegateBridge __Hotfix_set_EffetAudioResPath;
        private static DelegateBridge __Hotfix_get_IsAudioLoop;
        private static DelegateBridge __Hotfix_set_IsAudioLoop;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="EffectType", DataFormat=DataFormat.TwosComplement)]
        public ShipNamedEffectType EffectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="EffetAssetResPath", DataFormat=DataFormat.Default)]
        public string EffetAssetResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="EffetAudioResPath", DataFormat=DataFormat.Default)]
        public string EffetAudioResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IsAudioLoop", DataFormat=DataFormat.Default)]
        public bool IsAudioLoop
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

