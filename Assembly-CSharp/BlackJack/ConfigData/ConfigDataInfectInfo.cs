﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataInfectInfo")]
    public class ConfigDataInfectInfo : IExtensible
    {
        private int _ID;
        private int _ShowInfectLevel;
        private int _CompleteReward;
        private int _FinalBattleRewardPreview;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShowInfectLevel;
        private static DelegateBridge __Hotfix_set_ShowInfectLevel;
        private static DelegateBridge __Hotfix_get_CompleteReward;
        private static DelegateBridge __Hotfix_set_CompleteReward;
        private static DelegateBridge __Hotfix_get_FinalBattleRewardPreview;
        private static DelegateBridge __Hotfix_set_FinalBattleRewardPreview;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShowInfectLevel", DataFormat=DataFormat.TwosComplement)]
        public int ShowInfectLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CompleteReward", DataFormat=DataFormat.TwosComplement)]
        public int CompleteReward
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="FinalBattleRewardPreview", DataFormat=DataFormat.TwosComplement)]
        public int FinalBattleRewardPreview
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

