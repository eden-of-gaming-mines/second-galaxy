﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MonthlyCardPriviledgeType")]
    public enum MonthlyCardPriviledgeType
    {
        [ProtoEnum(Name="MonthlyCardPriviledgeType_Initial", Value=0)]
        MonthlyCardPriviledgeType_Initial = 0,
        [ProtoEnum(Name="MonthlyCardPriviledgeType_ExtraDailyShipSalvageCount", Value=1)]
        MonthlyCardPriviledgeType_ExtraDailyShipSalvageCount = 1,
        [ProtoEnum(Name="MonthlyCardPriviledgeType_ExtraDailyWormholeEntryCount", Value=2)]
        MonthlyCardPriviledgeType_ExtraDailyWormholeEntryCount = 2,
        [ProtoEnum(Name="MonthlyCardPriviledgeType_ExtraAuctionSlotCount", Value=3)]
        MonthlyCardPriviledgeType_ExtraAuctionSlotCount = 3,
        [ProtoEnum(Name="MonthlyCardPriviledgeType_TechSpeedUpPercent", Value=4)]
        MonthlyCardPriviledgeType_TechSpeedUpPercent = 4,
        [ProtoEnum(Name="MonthlyCardPriviledgeType_Max", Value=5)]
        MonthlyCardPriviledgeType_Max = 5
    }
}

