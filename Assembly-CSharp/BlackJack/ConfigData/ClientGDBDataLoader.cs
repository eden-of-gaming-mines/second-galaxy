﻿namespace BlackJack.ConfigData
{
    using BlackJack.BJFramework.Runtime.ConfigData;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.ProjectX.Common;
    using IL;
    using ProtoBuf.Meta;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ClientGDBDataLoader : ClientConfigDataLoaderBase, IGDBDataLoader
    {
        private GDBGalaxyInfo m_GDBGalaxyInfo;
        private Dictionary<int, GDBStarfieldsInfo> m_GDBStarfieldsInfoData;
        private Dictionary<int, GDBSolarSystemInfo> m_GDBSolarSystemsInfoData;
        private Dictionary<int, GDBStargroupInfo> m_GDBSolarSystemId2StargroupData;
        private Dictionary<int, GDBStarfieldsInfo> m_GDBSolarSystemId2StarfieldData;
        private Dictionary<int, GDBSolarSystemSimpleInfo> m_GDBSolarSystemSimpleInfo;
        private Dictionary<int, List<int>> m_GDBSolarSystemLinkListDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartInitializeFromAsset;
        private static DelegateBridge __Hotfix_InitDeserializFunc4ConfigData;
        private static DelegateBridge __Hotfix_DeserializFunc4GDBGalaxyInfo;
        private static DelegateBridge __Hotfix_DeserializFunc4GDBStarfieldsInfo;
        private static DelegateBridge __Hotfix_GetAllInitLoadConfigDataAssetPath;
        private static DelegateBridge __Hotfix_AddSolarSystemLinkInfoRecord;
        private static DelegateBridge __Hotfix_GetGDBGalaxyInfo;
        private static DelegateBridge __Hotfix_GetGDBStarfieldsInfo;
        private static DelegateBridge __Hotfix_GetGDBSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetGDBSolarSystemSimpleInfo;
        private static DelegateBridge __Hotfix_GetGDBSpaceStationInfo;
        private static DelegateBridge __Hotfix_GetOwnerStargroupBySolarSystemId;
        private static DelegateBridge __Hotfix_GetOwnerStarfieldBySolarSystemId;
        private static DelegateBridge __Hotfix_GetAllLinkedSolarSystemIdListBySolarSystemId;
        private static DelegateBridge __Hotfix_GetAllGDBSolarSystemSimpleInfo;
        private static DelegateBridge __Hotfix_GetStargateGDBIdBetweenSolarSystem;
        private static DelegateBridge __Hotfix_GetWormholeGateBetweenSolarSystem;
        private static DelegateBridge __Hotfix_GetNpcTalkerInfoWithNpcDNId;
        private static DelegateBridge __Hotfix_GetGDBSolarSystemInfoAsync;

        [MethodImpl(0x8000)]
        protected void AddSolarSystemLinkInfoRecord(int srcId, int destId)
        {
        }

        [MethodImpl(0x8000)]
        private void DeserializFunc4GDBGalaxyInfo(BytesScriptableObjectMD5 asset, bool inThreading)
        {
        }

        [MethodImpl(0x8000)]
        private void DeserializFunc4GDBStarfieldsInfo(BytesScriptableObjectMD5 asset, bool inThreading)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, GDBSolarSystemSimpleInfo> GetAllGDBSolarSystemSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override HashSet<string> GetAllInitLoadConfigDataAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetAllLinkedSolarSystemIdListBySolarSystemId(int srcSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBGalaxyInfo GetGDBGalaxyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo GetGDBSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void GetGDBSolarSystemInfoAsync(int solarSystemId, Action<GDBSolarSystemInfo> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo GetGDBSolarSystemSimpleInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo GetGDBSpaceStationInfo(GDBSolarSystemInfo solarSystemInfo, int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarfieldsInfo GetGDBStarfieldsInfo(int id)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo GetNpcTalkerInfoWithNpcDNId(NpcDNId npcId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarfieldsInfo GetOwnerStarfieldBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargroupInfo GetOwnerStargroupBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetStargateGDBIdBetweenSolarSystem(int srcSolarsystemId, int destSolarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBWormholeGateInfo GetWormholeGateBetweenSolarSystem(int srcSolarsystemId, int destSolarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitDeserializFunc4ConfigData()
        {
        }

        [MethodImpl(0x8000)]
        public override bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
        {
        }

        [CompilerGenerated]
        private sealed class <GetGDBSolarSystemInfoAsync>c__AnonStorey0
        {
            internal string solarSystemResPath;
            internal int starFieldId;
            internal ClientGDBDataLoader.<GetGDBSolarSystemInfoAsync>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(string lpath, UnityEngine.Object lasset)
            {
                if (lasset == null)
                {
                    Debug.LogError("ClientGDBDataLoader.GetGDBSolarSystemInfoAsync loadAsset fail, path = " + lpath);
                    this.<>f__ref$1.onEnd(null);
                }
                else
                {
                    BytesScriptableObjectMD5 tmd = lasset as BytesScriptableObjectMD5;
                    if (tmd != null)
                    {
                        if (tmd.GetBytes() == null)
                        {
                            Debug.LogError("ClientGDBDataLoader.GetGDBSolarSystemInfoAsync loadAsset successed, but asset data is not correct, path= " + lpath);
                            this.<>f__ref$1.onEnd(null);
                        }
                        else
                        {
                            string str = $"{Path.GetFileNameWithoutExtension(this.solarSystemResPath)}.bin".ToLower();
                            this.<>f__ref$1.$this.m_assetMD5Dict[str] = tmd.m_MD5;
                            using (MemoryStream stream = new MemoryStream(tmd.GetBytes()))
                            {
                                foreach (GDBSolarSystemInfo info in RuntimeTypeModel.Default.Deserialize(stream, null, typeof(List<GDBSolarSystemInfo>), null) as List<GDBSolarSystemInfo>)
                                {
                                    this.<>f__ref$1.$this.m_GDBSolarSystemsInfoData[info.Id] = info;
                                    if (info.Id == this.<>f__ref$1.solarSystemId)
                                    {
                                        this.<>f__ref$1.solarSystem = info;
                                    }
                                }
                                Debug.Log("ClientGDBDataLoader.GetGDBSolarSystemInfoAsync loadAsset ok");
                            }
                            if (this.<>f__ref$1.solarSystem != null)
                            {
                                this.<>f__ref$1.onEnd(this.<>f__ref$1.solarSystem);
                            }
                            else
                            {
                                Debug.LogError($"ClientGDBDataLoader.GetGDBSolarSystemInfoAsync fail, can not find solarSystem id = {this.<>f__ref$1.solarSystemId.ToString()} on starfield id = {this.starFieldId.ToString()}");
                                this.<>f__ref$1.onEnd(null);
                            }
                        }
                    }
                    ResourceManager.Instance.UnloadAsset(lasset);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGDBSolarSystemInfoAsync>c__AnonStorey1
        {
            internal int solarSystemId;
            internal GDBSolarSystemInfo solarSystem;
            internal Action<GDBSolarSystemInfo> onEnd;
            internal ClientGDBDataLoader $this;
        }
    }
}

