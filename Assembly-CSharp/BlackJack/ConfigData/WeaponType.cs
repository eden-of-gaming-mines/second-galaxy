﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="WeaponType")]
    public enum WeaponType
    {
        [ProtoEnum(Name="WeaponType_BurstRailgun", Value=1)]
        WeaponType_BurstRailgun = 1,
        [ProtoEnum(Name="WeaponType_SniperRailgun", Value=2)]
        WeaponType_SniperRailgun = 2,
        [ProtoEnum(Name="WeaponType_MissileLauncher", Value=3)]
        WeaponType_MissileLauncher = 3,
        [ProtoEnum(Name="WeaponType_LaserPulse", Value=4)]
        WeaponType_LaserPulse = 4,
        [ProtoEnum(Name="WeaponType_LaserBunding", Value=5)]
        WeaponType_LaserBunding = 5,
        [ProtoEnum(Name="WeaponType_FreePlasma", Value=6)]
        WeaponType_FreePlasma = 6,
        [ProtoEnum(Name="WeaponType_HiNRGPlasma", Value=7)]
        WeaponType_HiNRGPlasma = 7,
        [ProtoEnum(Name="WeaponType_DroneCabin", Value=8)]
        WeaponType_DroneCabin = 8,
        [ProtoEnum(Name="WeaponType_AutoCannon", Value=9)]
        WeaponType_AutoCannon = 9,
        [ProtoEnum(Name="WeaponType_CannonHowitzer", Value=10)]
        WeaponType_CannonHowitzer = 10,
        [ProtoEnum(Name="WeaponType_Max", Value=11)]
        WeaponType_Max = 11
    }
}

