﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataIslandTypeInfo")]
    public class ConfigDataIslandTypeInfo : IExtensible
    {
        private int _ID;
        private string _Name;
        private readonly List<IslandTypeInfoSolarSystemGuildMinrealOutputInfoList> _SolarSystemGuildMinrealOutputInfoList;
        private string _ShowColor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_SolarSystemGuildMinrealOutputInfoList;
        private static DelegateBridge __Hotfix_get_ShowColor;
        private static DelegateBridge __Hotfix_set_ShowColor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="SolarSystemGuildMinrealOutputInfoList", DataFormat=DataFormat.Default)]
        public List<IslandTypeInfoSolarSystemGuildMinrealOutputInfoList> SolarSystemGuildMinrealOutputInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShowColor", DataFormat=DataFormat.Default)]
        public string ShowColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

