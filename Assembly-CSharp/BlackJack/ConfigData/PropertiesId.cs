﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="PropertiesId")]
    public enum PropertiesId
    {
        [ProtoEnum(Name="PropertiesId_RailgunCritical_Base", Value=1)]
        PropertiesId_RailgunCritical_Base = 1,
        [ProtoEnum(Name="PropertiesId_MissileCritical_Base", Value=2)]
        PropertiesId_MissileCritical_Base = 2,
        [ProtoEnum(Name="PropertiesId_LaserCritical_Base", Value=3)]
        PropertiesId_LaserCritical_Base = 3,
        [ProtoEnum(Name="PropertiesId_PlasmaCritical_Base", Value=4)]
        PropertiesId_PlasmaCritical_Base = 4,
        [ProtoEnum(Name="PropertiesId_DroneCritical_Base", Value=5)]
        PropertiesId_DroneCritical_Base = 5,
        [ProtoEnum(Name="PropertiesId_NormalAttackCritical_Base", Value=0x1be)]
        PropertiesId_NormalAttackCritical_Base = 0x1be,
        [ProtoEnum(Name="PropertiesId_CannonCritical_Base", Value=0x543)]
        PropertiesId_CannonCritical_Base = 0x543,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalAdd_ED", Value=6)]
        PropertiesId_RailgunCriticalAdd_ED = 6,
        [ProtoEnum(Name="PropertiesId_MissileCriticalAdd_ED", Value=7)]
        PropertiesId_MissileCriticalAdd_ED = 7,
        [ProtoEnum(Name="PropertiesId_LaserCriticalAdd_ED", Value=8)]
        PropertiesId_LaserCriticalAdd_ED = 8,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalAdd_ED", Value=9)]
        PropertiesId_PlasmaCriticalAdd_ED = 9,
        [ProtoEnum(Name="PropertiesId_DroneCriticalAdd_ED", Value=10)]
        PropertiesId_DroneCriticalAdd_ED = 10,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalAdd_ED", Value=0x1bf)]
        PropertiesId_NormalAttackCriticalAdd_ED = 0x1bf,
        [ProtoEnum(Name="PropertiesId_CannonCriticalAdd_ED", Value=0x544)]
        PropertiesId_CannonCriticalAdd_ED = 0x544,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalAdd_ES", Value=11)]
        PropertiesId_RailgunCriticalAdd_ES = 11,
        [ProtoEnum(Name="PropertiesId_MissileCriticalAdd_ES", Value=12)]
        PropertiesId_MissileCriticalAdd_ES = 12,
        [ProtoEnum(Name="PropertiesId_LaserCriticalAdd_ES", Value=13)]
        PropertiesId_LaserCriticalAdd_ES = 13,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalAdd_ES", Value=14)]
        PropertiesId_PlasmaCriticalAdd_ES = 14,
        [ProtoEnum(Name="PropertiesId_DroneCriticalAdd_ES", Value=15)]
        PropertiesId_DroneCriticalAdd_ES = 15,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalAdd_ES", Value=0x1c0)]
        PropertiesId_NormalAttackCriticalAdd_ES = 0x1c0,
        [ProtoEnum(Name="PropertiesId_RailgunCritical_Final", Value=0x164)]
        PropertiesId_RailgunCritical_Final = 0x164,
        [ProtoEnum(Name="PropertiesId_MissileCritical_Final", Value=0x165)]
        PropertiesId_MissileCritical_Final = 0x165,
        [ProtoEnum(Name="PropertiesId_LaserCritical_Final", Value=0x166)]
        PropertiesId_LaserCritical_Final = 0x166,
        [ProtoEnum(Name="PropertiesId_PlasmaCritical_Final", Value=0x167)]
        PropertiesId_PlasmaCritical_Final = 0x167,
        [ProtoEnum(Name="PropertiesId_DroneCritical_Final", Value=360)]
        PropertiesId_DroneCritical_Final = 360,
        [ProtoEnum(Name="PropertiesId_NormalAttackCritical_Final", Value=0x1e7)]
        PropertiesId_NormalAttackCritical_Final = 0x1e7,
        [ProtoEnum(Name="PropertiesId_CannonCritical_Final", Value=0x577)]
        PropertiesId_CannonCritical_Final = 0x577,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalDamageRatio_Base", Value=0x10)]
        PropertiesId_RailgunCriticalDamageRatio_Base = 0x10,
        [ProtoEnum(Name="PropertiesId_MissileCriticalDamageRatio_Base", Value=0x11)]
        PropertiesId_MissileCriticalDamageRatio_Base = 0x11,
        [ProtoEnum(Name="PropertiesId_LaserCriticalDamageRatio_Base", Value=0x12)]
        PropertiesId_LaserCriticalDamageRatio_Base = 0x12,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalDamageRatio_Base", Value=0x13)]
        PropertiesId_PlasmaCriticalDamageRatio_Base = 0x13,
        [ProtoEnum(Name="PropertiesId_DroneCriticalDamageRatio_Base", Value=20)]
        PropertiesId_DroneCriticalDamageRatio_Base = 20,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalDamageRatio_Base", Value=0x1c1)]
        PropertiesId_NormalAttackCriticalDamageRatio_Base = 0x1c1,
        [ProtoEnum(Name="PropertiesId_CannonCriticalDamageRatio_Base", Value=0x546)]
        PropertiesId_CannonCriticalDamageRatio_Base = 0x546,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalDamageRatioAdd_ED", Value=0x15)]
        PropertiesId_RailgunCriticalDamageRatioAdd_ED = 0x15,
        [ProtoEnum(Name="PropertiesId_MissileCriticalDamageRatioAdd_ED", Value=0x16)]
        PropertiesId_MissileCriticalDamageRatioAdd_ED = 0x16,
        [ProtoEnum(Name="PropertiesId_LaserCriticalDamageRatioAdd_ED", Value=0x17)]
        PropertiesId_LaserCriticalDamageRatioAdd_ED = 0x17,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalDamageRatioAdd_ED", Value=0x18)]
        PropertiesId_PlasmaCriticalDamageRatioAdd_ED = 0x18,
        [ProtoEnum(Name="PropertiesId_DroneCriticalDamageRatioAdd_ED", Value=0x19)]
        PropertiesId_DroneCriticalDamageRatioAdd_ED = 0x19,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalDamageRatioAdd_ED", Value=450)]
        PropertiesId_NormalAttackCriticalDamageRatioAdd_ED = 450,
        [ProtoEnum(Name="PropertiesId_CannonCriticalDamageRatioAdd_ED", Value=0x547)]
        PropertiesId_CannonCriticalDamageRatioAdd_ED = 0x547,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalDamageRatioAdd_ES", Value=0x1a)]
        PropertiesId_RailgunCriticalDamageRatioAdd_ES = 0x1a,
        [ProtoEnum(Name="PropertiesId_MissileCriticalDamageRatioAdd_ES", Value=0x1b)]
        PropertiesId_MissileCriticalDamageRatioAdd_ES = 0x1b,
        [ProtoEnum(Name="PropertiesId_LaserCriticalDamageRatioAdd_ES", Value=0x1c)]
        PropertiesId_LaserCriticalDamageRatioAdd_ES = 0x1c,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalDamageRatioAdd_ES", Value=0x1d)]
        PropertiesId_PlasmaCriticalDamageRatioAdd_ES = 0x1d,
        [ProtoEnum(Name="PropertiesId_DroneCriticalDamageRatioAdd_ES", Value=30)]
        PropertiesId_DroneCriticalDamageRatioAdd_ES = 30,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalDamageRatioAdd_ES", Value=0x69c)]
        PropertiesId_NormalAttackCriticalDamageRatioAdd_ES = 0x69c,
        [ProtoEnum(Name="PropertiesId_RailgunCriticalDamageRatio_Final", Value=0x169)]
        PropertiesId_RailgunCriticalDamageRatio_Final = 0x169,
        [ProtoEnum(Name="PropertiesId_MissileCriticalDamageRatio_Final", Value=0x16a)]
        PropertiesId_MissileCriticalDamageRatio_Final = 0x16a,
        [ProtoEnum(Name="PropertiesId_LaserCriticalDamageRatio_Final", Value=0x16b)]
        PropertiesId_LaserCriticalDamageRatio_Final = 0x16b,
        [ProtoEnum(Name="PropertiesId_PlasmaCriticalDamageRatio_Final", Value=0x16c)]
        PropertiesId_PlasmaCriticalDamageRatio_Final = 0x16c,
        [ProtoEnum(Name="PropertiesId_DroneCriticalDamageRatio_Final", Value=0x16d)]
        PropertiesId_DroneCriticalDamageRatio_Final = 0x16d,
        [ProtoEnum(Name="PropertiesId_NormalAttackCriticalDamageRatio_Final", Value=0x1e8)]
        PropertiesId_NormalAttackCriticalDamageRatio_Final = 0x1e8,
        [ProtoEnum(Name="PropertiesId_CannonCriticalDamageRatio_Final", Value=0x578)]
        PropertiesId_CannonCriticalDamageRatio_Final = 0x578,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracy_Base", Value=0x5c)]
        PropertiesId_RailgunFireCtrlAccuracy_Base = 0x5c,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracy_Base", Value=0x5d)]
        PropertiesId_MissileFireCtrlAccuracy_Base = 0x5d,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracy_Base", Value=0x5e)]
        PropertiesId_LaserFireCtrlAccuracy_Base = 0x5e,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracy_Base", Value=0x5f)]
        PropertiesId_PlasmaFireCtrlAccuracy_Base = 0x5f,
        [ProtoEnum(Name="PropertiesId_DroneFireCtrlAccuracy_Base", Value=0x60)]
        PropertiesId_DroneFireCtrlAccuracy_Base = 0x60,
        [ProtoEnum(Name="PropertiesId_NormalAttackFireCtrlAccuracy_Base", Value=0x1c8)]
        PropertiesId_NormalAttackFireCtrlAccuracy_Base = 0x1c8,
        [ProtoEnum(Name="PropertiesId_CannonFireCtrlAccuracy_Base", Value=0x54e)]
        PropertiesId_CannonFireCtrlAccuracy_Base = 0x54e,
        [ProtoEnum(Name="PropertiesId_EquipFireCtrlAccuracy_Base", Value=0x689)]
        PropertiesId_EquipFireCtrlAccuracy_Base = 0x689,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracyAdd_ES", Value=0x70)]
        PropertiesId_RailgunFireCtrlAccuracyAdd_ES = 0x70,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracyAdd_ES", Value=0x71)]
        PropertiesId_MissileFireCtrlAccuracyAdd_ES = 0x71,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracyAdd_ES", Value=0x72)]
        PropertiesId_LaserFireCtrlAccuracyAdd_ES = 0x72,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracyAdd_ES", Value=0x73)]
        PropertiesId_PlasmaFireCtrlAccuracyAdd_ES = 0x73,
        [ProtoEnum(Name="PropertiesId_DroneFireCtrlAccuracyAdd_ES", Value=0x74)]
        PropertiesId_DroneFireCtrlAccuracyAdd_ES = 0x74,
        [ProtoEnum(Name="PropertiesId_NormalAttackFireCtrlAccuracyAdd_ES", Value=460)]
        PropertiesId_NormalAttackFireCtrlAccuracyAdd_ES = 460,
        [ProtoEnum(Name="PropertiesId_CannonFireCtrlAccuracyAdd_ES", Value=0x552)]
        PropertiesId_CannonFireCtrlAccuracyAdd_ES = 0x552,
        [ProtoEnum(Name="PropertiesId_EquipFireCtrlAccuracyMulti_ES", Value=0x68a)]
        PropertiesId_EquipFireCtrlAccuracyMulti_ES = 0x68a,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracyMulti_ED", Value=0x6b6)]
        PropertiesId_RailgunFireCtrlAccuracyMulti_ED = 0x6b6,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracyMulti_ED", Value=0x6b7)]
        PropertiesId_MissileFireCtrlAccuracyMulti_ED = 0x6b7,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracyMulti_ED", Value=0x6b8)]
        PropertiesId_LaserFireCtrlAccuracyMulti_ED = 0x6b8,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracyMulti_ED", Value=0x6b9)]
        PropertiesId_PlasmaFireCtrlAccuracyMulti_ED = 0x6b9,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracyMulti_ES", Value=0x6ba)]
        PropertiesId_RailgunFireCtrlAccuracyMulti_ES = 0x6ba,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracyMulti_ES", Value=0x6bb)]
        PropertiesId_MissileFireCtrlAccuracyMulti_ES = 0x6bb,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracyMulti_ES", Value=0x6bc)]
        PropertiesId_LaserFireCtrlAccuracyMulti_ES = 0x6bc,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracyMulti_ES", Value=0x6bd)]
        PropertiesId_PlasmaFireCtrlAccuracyMulti_ES = 0x6bd,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracy_ND", Value=0x61)]
        PropertiesId_RailgunFireCtrlAccuracy_ND = 0x61,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracy_ND", Value=0x62)]
        PropertiesId_MissileFireCtrlAccuracy_ND = 0x62,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracy_ND", Value=0x63)]
        PropertiesId_LaserFireCtrlAccuracy_ND = 0x63,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracy_ND", Value=100)]
        PropertiesId_PlasmaFireCtrlAccuracy_ND = 100,
        [ProtoEnum(Name="PropertiesId_DroneFireCtrlAccuracy_ND", Value=0x65)]
        PropertiesId_DroneFireCtrlAccuracy_ND = 0x65,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracy_NS", Value=0x66)]
        PropertiesId_RailgunFireCtrlAccuracy_NS = 0x66,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracy_NS", Value=0x67)]
        PropertiesId_MissileFireCtrlAccuracy_NS = 0x67,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracy_NS", Value=0x68)]
        PropertiesId_LaserFireCtrlAccuracy_NS = 0x68,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracy_NS", Value=0x69)]
        PropertiesId_PlasmaFireCtrlAccuracy_NS = 0x69,
        [ProtoEnum(Name="PropertiesId_RailgunFireCtrlAccuracy_Final", Value=0x16e)]
        PropertiesId_RailgunFireCtrlAccuracy_Final = 0x16e,
        [ProtoEnum(Name="PropertiesId_MissileFireCtrlAccuracy_Final", Value=0x16f)]
        PropertiesId_MissileFireCtrlAccuracy_Final = 0x16f,
        [ProtoEnum(Name="PropertiesId_LaserFireCtrlAccuracy_Final", Value=0x170)]
        PropertiesId_LaserFireCtrlAccuracy_Final = 0x170,
        [ProtoEnum(Name="PropertiesId_PlasmaFireCtrlAccuracy_Final", Value=0x171)]
        PropertiesId_PlasmaFireCtrlAccuracy_Final = 0x171,
        [ProtoEnum(Name="PropertiesId_DroneFireCtrlAccuracy_Final", Value=370)]
        PropertiesId_DroneFireCtrlAccuracy_Final = 370,
        [ProtoEnum(Name="PropertiesId_NormalAttackFireCtrlAccuracy_Final", Value=0x1e9)]
        PropertiesId_NormalAttackFireCtrlAccuracy_Final = 0x1e9,
        [ProtoEnum(Name="PropertiesId_CannonFireCtrlAccuracy_Final", Value=0x579)]
        PropertiesId_CannonFireCtrlAccuracy_Final = 0x579,
        [ProtoEnum(Name="PropertiesId_EquipFireCtrlAccuracy_Final", Value=0x690)]
        PropertiesId_EquipFireCtrlAccuracy_Final = 0x690,
        [ProtoEnum(Name="PropertiesId_LaserTransverseVelocity_Base", Value=0x752)]
        PropertiesId_LaserTransverseVelocity_Base = 0x752,
        [ProtoEnum(Name="PropertiesId_RailgunTransverseVelocity_Base", Value=0x754)]
        PropertiesId_RailgunTransverseVelocity_Base = 0x754,
        [ProtoEnum(Name="PropertiesId_PlasmaTransverseVelocity_Base", Value=0x756)]
        PropertiesId_PlasmaTransverseVelocity_Base = 0x756,
        [ProtoEnum(Name="PropertiesId_MissileTransverseVelocity_Base", Value=0x758)]
        PropertiesId_MissileTransverseVelocity_Base = 0x758,
        [ProtoEnum(Name="PropertiesId_NormalAttackTransverseVelocity_Base", Value=0x75a)]
        PropertiesId_NormalAttackTransverseVelocity_Base = 0x75a,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocity_Base", Value=0x762)]
        PropertiesId_DroneTransverseVelocity_Base = 0x762,
        [ProtoEnum(Name="PropertiesId_NormalAttackTransverseVelocityMulti_ES", Value=0x4e0)]
        PropertiesId_NormalAttackTransverseVelocityMulti_ES = 0x4e0,
        [ProtoEnum(Name="PropertiesId_LaserTransverseVelocityMulti_ES", Value=0x72a)]
        PropertiesId_LaserTransverseVelocityMulti_ES = 0x72a,
        [ProtoEnum(Name="PropertiesId_RailgunTransverseVelocityMulti_ES", Value=0x730)]
        PropertiesId_RailgunTransverseVelocityMulti_ES = 0x730,
        [ProtoEnum(Name="PropertiesId_PlasmaTransverseVelocityMulti_ES", Value=0x736)]
        PropertiesId_PlasmaTransverseVelocityMulti_ES = 0x736,
        [ProtoEnum(Name="PropertiesId_MissileTransverseVelocityMulti_ES", Value=0x73c)]
        PropertiesId_MissileTransverseVelocityMulti_ES = 0x73c,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocityMulti_ES", Value=0x75c)]
        PropertiesId_DroneTransverseVelocityMulti_ES = 0x75c,
        [ProtoEnum(Name="PropertiesId_LaserTransverseVelocity_ND", Value=0x72f)]
        PropertiesId_LaserTransverseVelocity_ND = 0x72f,
        [ProtoEnum(Name="PropertiesId_RailgunTransverseVelocity_ND", Value=0x735)]
        PropertiesId_RailgunTransverseVelocity_ND = 0x735,
        [ProtoEnum(Name="PropertiesId_PlasmaTransverseVelocity_ND", Value=0x73b)]
        PropertiesId_PlasmaTransverseVelocity_ND = 0x73b,
        [ProtoEnum(Name="PropertiesId_MissileTransverseVelocity_ND", Value=0x741)]
        PropertiesId_MissileTransverseVelocity_ND = 0x741,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocity_ND", Value=0x761)]
        PropertiesId_DroneTransverseVelocity_ND = 0x761,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocityAdd_ES", Value=0x75f)]
        PropertiesId_DroneTransverseVelocityAdd_ES = 0x75f,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocityAdd_ED", Value=0x760)]
        PropertiesId_DroneTransverseVelocityAdd_ED = 0x760,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocityMulti_ED", Value=0x75d)]
        PropertiesId_DroneTransverseVelocityMulti_ED = 0x75d,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocity_NS", Value=0x75e)]
        PropertiesId_DroneTransverseVelocity_NS = 0x75e,
        [ProtoEnum(Name="PropertiesId_LaserTransverseVelocity_Final", Value=0x753)]
        PropertiesId_LaserTransverseVelocity_Final = 0x753,
        [ProtoEnum(Name="PropertiesId_RailgunTransverseVelocity_Final", Value=0x755)]
        PropertiesId_RailgunTransverseVelocity_Final = 0x755,
        [ProtoEnum(Name="PropertiesId_PlasmaTransverseVelocity_Final", Value=0x757)]
        PropertiesId_PlasmaTransverseVelocity_Final = 0x757,
        [ProtoEnum(Name="PropertiesId_MissileTransverseVelocity_Final", Value=0x759)]
        PropertiesId_MissileTransverseVelocity_Final = 0x759,
        [ProtoEnum(Name="PropertiesId_NormalAttackTransverseVelocity_Final", Value=0x75b)]
        PropertiesId_NormalAttackTransverseVelocity_Final = 0x75b,
        [ProtoEnum(Name="PropertiesId_DroneTransverseVelocity_Final", Value=0x763)]
        PropertiesId_DroneTransverseVelocity_Final = 0x763,
        [ProtoEnum(Name="PropertiesId_RailgunHit_Base", Value=0x98)]
        PropertiesId_RailgunHit_Base = 0x98,
        [ProtoEnum(Name="PropertiesId_MissileHit_Base", Value=0x99)]
        PropertiesId_MissileHit_Base = 0x99,
        [ProtoEnum(Name="PropertiesId_PlasmaHit_Base", Value=0x9a)]
        PropertiesId_PlasmaHit_Base = 0x9a,
        [ProtoEnum(Name="PropertiesId_DroneHit_Base", Value=0x9b)]
        PropertiesId_DroneHit_Base = 0x9b,
        [ProtoEnum(Name="PropertiesId_NormalAttackHit_Base", Value=0x1d2)]
        PropertiesId_NormalAttackHit_Base = 0x1d2,
        [ProtoEnum(Name="PropertiesId_CannonHit_Base", Value=0x556)]
        PropertiesId_CannonHit_Base = 0x556,
        [ProtoEnum(Name="PropertiesId_RailgunHit_Final", Value=420)]
        PropertiesId_RailgunHit_Final = 420,
        [ProtoEnum(Name="PropertiesId_MissileHit_Final", Value=0x1a5)]
        PropertiesId_MissileHit_Final = 0x1a5,
        [ProtoEnum(Name="PropertiesId_PlasmaHit_Final", Value=0x1a6)]
        PropertiesId_PlasmaHit_Final = 0x1a6,
        [ProtoEnum(Name="PropertiesId_DroneHit_Final", Value=0x1a7)]
        PropertiesId_DroneHit_Final = 0x1a7,
        [ProtoEnum(Name="PropertiesId_NormalAttackHit_Final", Value=0x1f0)]
        PropertiesId_NormalAttackHit_Final = 0x1f0,
        [ProtoEnum(Name="PropertiesId_CannonHit_Final", Value=0x582)]
        PropertiesId_CannonHit_Final = 0x582,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRange_Base", Value=0xae)]
        PropertiesId_RailgunMaxFireRange_Base = 0xae,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRange_Base", Value=0xaf)]
        PropertiesId_MissileMaxFireRange_Base = 0xaf,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRange_Base", Value=0xb0)]
        PropertiesId_LaserMaxFireRange_Base = 0xb0,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRange_Base", Value=0xb1)]
        PropertiesId_PlasmaMaxFireRange_Base = 0xb1,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRange_Base", Value=0x1d8)]
        PropertiesId_NormalAttackMaxFireRange_Base = 0x1d8,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRange_Base", Value=0x55c)]
        PropertiesId_CannonMaxFireRange_Base = 0x55c,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRangeAdd_ES", Value=190)]
        PropertiesId_RailgunMaxFireRangeAdd_ES = 190,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRangeAdd_ES", Value=0xbf)]
        PropertiesId_MissileMaxFireRangeAdd_ES = 0xbf,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRangeAdd_ES", Value=0xc0)]
        PropertiesId_LaserMaxFireRangeAdd_ES = 0xc0,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRangeAdd_ES", Value=0xc1)]
        PropertiesId_PlasmaMaxFireRangeAdd_ES = 0xc1,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRangeAdd_ES", Value=0x1de)]
        PropertiesId_NormalAttackMaxFireRangeAdd_ES = 0x1de,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRangeAdd_ES", Value=0x562)]
        PropertiesId_CannonMaxFireRangeAdd_ES = 0x562,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRangeAdd_ED", Value=0xc3)]
        PropertiesId_RailgunMaxFireRangeAdd_ED = 0xc3,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRangeAdd_ED", Value=0xc4)]
        PropertiesId_MissileMaxFireRangeAdd_ED = 0xc4,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRangeAdd_ED", Value=0xc5)]
        PropertiesId_LaserMaxFireRangeAdd_ED = 0xc5,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRangeAdd_ED", Value=0xc6)]
        PropertiesId_PlasmaMaxFireRangeAdd_ED = 0xc6,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRangeAdd_ED", Value=0x563)]
        PropertiesId_CannonMaxFireRangeAdd_ED = 0x563,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRange_NS", Value=200)]
        PropertiesId_RailgunMaxFireRange_NS = 200,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRange_NS", Value=0xc9)]
        PropertiesId_MissileMaxFireRange_NS = 0xc9,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRange_NS", Value=0xca)]
        PropertiesId_LaserMaxFireRange_NS = 0xca,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRange_NS", Value=0xcb)]
        PropertiesId_PlasmaMaxFireRange_NS = 0xcb,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRange_NS", Value=0x1df)]
        PropertiesId_NormalAttackMaxFireRange_NS = 0x1df,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRange_NS", Value=0x564)]
        PropertiesId_CannonMaxFireRange_NS = 0x564,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRange_ND", Value=0xcd)]
        PropertiesId_RailgunMaxFireRange_ND = 0xcd,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRange_ND", Value=0xce)]
        PropertiesId_MissileMaxFireRange_ND = 0xce,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRange_ND", Value=0xcf)]
        PropertiesId_LaserMaxFireRange_ND = 0xcf,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRange_ND", Value=0xd0)]
        PropertiesId_PlasmaMaxFireRange_ND = 0xd0,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRangeMulti_ES", Value=0x626)]
        PropertiesId_RailgunMaxFireRangeMulti_ES = 0x626,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRangeMulti_ES", Value=0x628)]
        PropertiesId_MissileMaxFireRangeMulti_ES = 0x628,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRangeMulti_ES", Value=0x62a)]
        PropertiesId_LaserMaxFireRangeMulti_ES = 0x62a,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRangeMulti_ES", Value=0x62c)]
        PropertiesId_PlasmaMaxFireRangeMulti_ES = 0x62c,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRangeMulti_ES", Value=0x630)]
        PropertiesId_CannonMaxFireRangeMulti_ES = 0x630,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRangeMulti_ES", Value=0x632)]
        PropertiesId_NormalAttackMaxFireRangeMulti_ES = 0x632,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRangeMulti_ED", Value=0x627)]
        PropertiesId_RailgunMaxFireRangeMulti_ED = 0x627,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRangeMulti_ED", Value=0x629)]
        PropertiesId_MissileMaxFireRangeMulti_ED = 0x629,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRangeMulti_ED", Value=0x62b)]
        PropertiesId_LaserMaxFireRangeMulti_ED = 0x62b,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRangeMulti_ED", Value=0x62d)]
        PropertiesId_PlasmaMaxFireRangeMulti_ED = 0x62d,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRangeMulti_ED", Value=0x631)]
        PropertiesId_CannonMaxFireRangeMulti_ED = 0x631,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRangeMulti_ED", Value=0x633)]
        PropertiesId_NormalAttackMaxFireRangeMulti_ED = 0x633,
        [ProtoEnum(Name="PropertiesId_RailgunMaxFireRange_Final", Value=0x176)]
        PropertiesId_RailgunMaxFireRange_Final = 0x176,
        [ProtoEnum(Name="PropertiesId_MissileMaxFireRange_Final", Value=0x177)]
        PropertiesId_MissileMaxFireRange_Final = 0x177,
        [ProtoEnum(Name="PropertiesId_LaserMaxFireRange_Final", Value=0x178)]
        PropertiesId_LaserMaxFireRange_Final = 0x178,
        [ProtoEnum(Name="PropertiesId_PlasmaMaxFireRange_Final", Value=0x179)]
        PropertiesId_PlasmaMaxFireRange_Final = 0x179,
        [ProtoEnum(Name="PropertiesId_NormalAttackMaxFireRange_Final", Value=0x1eb)]
        PropertiesId_NormalAttackMaxFireRange_Final = 0x1eb,
        [ProtoEnum(Name="PropertiesId_CannonMaxFireRange_Final", Value=0x57b)]
        PropertiesId_CannonMaxFireRange_Final = 0x57b,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoMaxFireRangeModify_Base", Value=0xba)]
        PropertiesId_RailgunAmmoMaxFireRangeModify_Base = 0xba,
        [ProtoEnum(Name="PropertiesId_LaserAmmoMaxFireRangeModify_Base", Value=0xbb)]
        PropertiesId_LaserAmmoMaxFireRangeModify_Base = 0xbb,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoMaxFireRangeModify_Base", Value=0xbc)]
        PropertiesId_PlasmaAmmoMaxFireRangeModify_Base = 0xbc,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoMaxFireRangeModify_Base", Value=0x1dd)]
        PropertiesId_NormalAttackAmmoMaxFireRangeModify_Base = 0x1dd,
        [ProtoEnum(Name="PropertiesId_CannonAmmoMaxFireRangeModify_Base", Value=0x561)]
        PropertiesId_CannonAmmoMaxFireRangeModify_Base = 0x561,
        [ProtoEnum(Name="PropertiesId_RailgunOptimalFireRange_Base", Value=0xab)]
        PropertiesId_RailgunOptimalFireRange_Base = 0xab,
        [ProtoEnum(Name="PropertiesId_PlasmaOptimalFireRange_Base", Value=0xac)]
        PropertiesId_PlasmaOptimalFireRange_Base = 0xac,
        [ProtoEnum(Name="PropertiesId_NormalAttackOptimalFireRange_Base", Value=0x1d7)]
        PropertiesId_NormalAttackOptimalFireRange_Base = 0x1d7,
        [ProtoEnum(Name="PropertiesId_CannonOptimalFireRange_Base", Value=0x55b)]
        PropertiesId_CannonOptimalFireRange_Base = 0x55b,
        [ProtoEnum(Name="PropertiesId_CannonOptimalFireRangeAdd_ES", Value=0x55d)]
        PropertiesId_CannonOptimalFireRangeAdd_ES = 0x55d,
        [ProtoEnum(Name="PropertiesId_RailgunOptimalFireRange_NS", Value=180)]
        PropertiesId_RailgunOptimalFireRange_NS = 180,
        [ProtoEnum(Name="PropertiesId_PlasmaOptimalFireRange_NS", Value=0xb5)]
        PropertiesId_PlasmaOptimalFireRange_NS = 0xb5,
        [ProtoEnum(Name="PropertiesId_NormalAttackOptimalFireRange_NS", Value=0x1da)]
        PropertiesId_NormalAttackOptimalFireRange_NS = 0x1da,
        [ProtoEnum(Name="PropertiesId_RailgunOptimalFireRange_ND", Value=0xb6)]
        PropertiesId_RailgunOptimalFireRange_ND = 0xb6,
        [ProtoEnum(Name="PropertiesId_PlasmaOptimalFireRange_ND", Value=0xb7)]
        PropertiesId_PlasmaOptimalFireRange_ND = 0xb7,
        [ProtoEnum(Name="PropertiesId_NormalAttackOptimalFireRange_ND", Value=0x1db)]
        PropertiesId_NormalAttackOptimalFireRange_ND = 0x1db,
        [ProtoEnum(Name="PropertiesId_CannonOptimalFireRange_ND", Value=0x55f)]
        PropertiesId_CannonOptimalFireRange_ND = 0x55f,
        [ProtoEnum(Name="PropertiesId_RailgunOptimalFireRange_Final", Value=0x173)]
        PropertiesId_RailgunOptimalFireRange_Final = 0x173,
        [ProtoEnum(Name="PropertiesId_PlasmaOptimalFireRange_Final", Value=0x174)]
        PropertiesId_PlasmaOptimalFireRange_Final = 0x174,
        [ProtoEnum(Name="PropertiesId_NormalAttackOptimalFireRange_Final", Value=490)]
        PropertiesId_NormalAttackOptimalFireRange_Final = 490,
        [ProtoEnum(Name="PropertiesId_CannonOptimalFireRange_Final", Value=0x57a)]
        PropertiesId_CannonOptimalFireRange_Final = 0x57a,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoOptimalFireRangeModify_Base", Value=0xb8)]
        PropertiesId_RailgunAmmoOptimalFireRangeModify_Base = 0xb8,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoOptimalFireRangeModify_Base", Value=0xb9)]
        PropertiesId_PlasmaAmmoOptimalFireRangeModify_Base = 0xb9,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoOptimalFireRangeModify_Base", Value=0x1dc)]
        PropertiesId_NormalAttackAmmoOptimalFireRangeModify_Base = 0x1dc,
        [ProtoEnum(Name="PropertiesId_CannonAmmoOptimalFireRangeModify_Base", Value=0x560)]
        PropertiesId_CannonAmmoOptimalFireRangeModify_Base = 0x560,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRange_Base", Value=0xad)]
        PropertiesId_DroneAmmoControlRange_Base = 0xad,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRangeModify_Base", Value=0xbd)]
        PropertiesId_DroneAmmoControlRangeModify_Base = 0xbd,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRangeAdd_ES", Value=0xc2)]
        PropertiesId_DroneAmmoControlRangeAdd_ES = 0xc2,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRangeMulti_ES", Value=0x62e)]
        PropertiesId_DroneAmmoControlRangeMulti_ES = 0x62e,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRangeMulti_ED", Value=0x62f)]
        PropertiesId_DroneAmmoControlRangeMulti_ED = 0x62f,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRangeAdd_ED", Value=0xc7)]
        PropertiesId_DroneAmmoControlRangeAdd_ED = 0xc7,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRange_NS", Value=0xcc)]
        PropertiesId_DroneAmmoControlRange_NS = 0xcc,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRange_ND", Value=0xd1)]
        PropertiesId_DroneAmmoControlRange_ND = 0xd1,
        [ProtoEnum(Name="PropertiesId_DroneAmmoControlRange_Final", Value=0x175)]
        PropertiesId_DroneAmmoControlRange_Final = 0x175,
        [ProtoEnum(Name="PropertiesId_DroneLifeDuration_Base", Value=210)]
        PropertiesId_DroneLifeDuration_Base = 210,
        [ProtoEnum(Name="PropertiesId_DroneLifeDurationModify_Base", Value=0xd5)]
        PropertiesId_DroneLifeDurationModify_Base = 0xd5,
        [ProtoEnum(Name="PropertiesId_DroneLifeDuration_NS", Value=0xd4)]
        PropertiesId_DroneLifeDuration_NS = 0xd4,
        [ProtoEnum(Name="PropertiesId_DroneLifeDuration_Final", Value=0x17a)]
        PropertiesId_DroneLifeDuration_Final = 0x17a,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergy_Base", Value=0x126)]
        PropertiesId_MissileLaunchCostEnergy_Base = 0x126,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergy_Base", Value=0x12a)]
        PropertiesId_PlasmaLaunchCostEnergy_Base = 0x12a,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergy_Base", Value=0x12e)]
        PropertiesId_RailgunLaunchCostEnergy_Base = 0x12e,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergy_Base", Value=0x132)]
        PropertiesId_LaserLaunchCostEnergy_Base = 0x132,
        [ProtoEnum(Name="PropertiesId_DroneLaunchCostEnergy_Base", Value=310)]
        PropertiesId_DroneLaunchCostEnergy_Base = 310,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergy_Base", Value=0x56d)]
        PropertiesId_CannonLaunchCostEnergy_Base = 0x56d,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergyMulti_ES", Value=0x127)]
        PropertiesId_MissileLaunchCostEnergyMulti_ES = 0x127,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergyMulti_ES", Value=0x12b)]
        PropertiesId_PlasmaLaunchCostEnergyMulti_ES = 0x12b,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergyMulti_ES", Value=0x12f)]
        PropertiesId_RailgunLaunchCostEnergyMulti_ES = 0x12f,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergyMulti_ES", Value=0x133)]
        PropertiesId_LaserLaunchCostEnergyMulti_ES = 0x133,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergyMulti_ES", Value=0x56e)]
        PropertiesId_CannonLaunchCostEnergyMulti_ES = 0x56e,
        [ProtoEnum(Name="PropertiesId_DroneLaunchCostEnergyMulti_ED", Value=0x696)]
        PropertiesId_DroneLaunchCostEnergyMulti_ED = 0x696,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergyMulti_ED", Value=0x697)]
        PropertiesId_LaserLaunchCostEnergyMulti_ED = 0x697,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergyMulti_ED", Value=0x698)]
        PropertiesId_MissileLaunchCostEnergyMulti_ED = 0x698,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergyMulti_ED", Value=0x699)]
        PropertiesId_PlasmaLaunchCostEnergyMulti_ED = 0x699,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergyMulti_ED", Value=0x69a)]
        PropertiesId_RailgunLaunchCostEnergyMulti_ED = 0x69a,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergyMulti_ED", Value=0x69b)]
        PropertiesId_CannonLaunchCostEnergyMulti_ED = 0x69b,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergy_NS", Value=0x128)]
        PropertiesId_MissileLaunchCostEnergy_NS = 0x128,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergy_NS", Value=300)]
        PropertiesId_PlasmaLaunchCostEnergy_NS = 300,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergy_NS", Value=0x130)]
        PropertiesId_RailgunLaunchCostEnergy_NS = 0x130,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergy_NS", Value=0x134)]
        PropertiesId_LaserLaunchCostEnergy_NS = 0x134,
        [ProtoEnum(Name="PropertiesId_DroneLaunchCostEnergy_NS", Value=0x138)]
        PropertiesId_DroneLaunchCostEnergy_NS = 0x138,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergy_NS", Value=0x56f)]
        PropertiesId_CannonLaunchCostEnergy_NS = 0x56f,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergy_ND", Value=0x129)]
        PropertiesId_MissileLaunchCostEnergy_ND = 0x129,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergy_ND", Value=0x12d)]
        PropertiesId_PlasmaLaunchCostEnergy_ND = 0x12d,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergy_ND", Value=0x131)]
        PropertiesId_RailgunLaunchCostEnergy_ND = 0x131,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergy_ND", Value=0x135)]
        PropertiesId_LaserLaunchCostEnergy_ND = 0x135,
        [ProtoEnum(Name="PropertiesId_DroneLaunchCostEnergy_ND", Value=0x139)]
        PropertiesId_DroneLaunchCostEnergy_ND = 0x139,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergy_ND", Value=0x570)]
        PropertiesId_CannonLaunchCostEnergy_ND = 0x570,
        [ProtoEnum(Name="PropertiesId_MissileLaunchCostEnergy_Final", Value=0x192)]
        PropertiesId_MissileLaunchCostEnergy_Final = 0x192,
        [ProtoEnum(Name="PropertiesId_PlasmaLaunchCostEnergy_Final", Value=0x193)]
        PropertiesId_PlasmaLaunchCostEnergy_Final = 0x193,
        [ProtoEnum(Name="PropertiesId_RailgunLaunchCostEnergy_Final", Value=0x194)]
        PropertiesId_RailgunLaunchCostEnergy_Final = 0x194,
        [ProtoEnum(Name="PropertiesId_LaserLaunchCostEnergy_Final", Value=0x195)]
        PropertiesId_LaserLaunchCostEnergy_Final = 0x195,
        [ProtoEnum(Name="PropertiesId_DroneLaunchCostEnergy_Final", Value=0x196)]
        PropertiesId_DroneLaunchCostEnergy_Final = 0x196,
        [ProtoEnum(Name="PropertiesId_NormalAttackLaunchCostEnergy_Final", Value=0x1ef)]
        PropertiesId_NormalAttackLaunchCostEnergy_Final = 0x1ef,
        [ProtoEnum(Name="PropertiesId_CannonLaunchCostEnergy_Final", Value=0x57f)]
        PropertiesId_CannonLaunchCostEnergy_Final = 0x57f,
        [ProtoEnum(Name="PropertiesId_RailgunWeaponDamage_Base", Value=0x75)]
        PropertiesId_RailgunWeaponDamage_Base = 0x75,
        [ProtoEnum(Name="PropertiesId_LaserWeaponDamage_Base", Value=0x76)]
        PropertiesId_LaserWeaponDamage_Base = 0x76,
        [ProtoEnum(Name="PropertiesId_PlasmaWeaponDamage_Base", Value=0x77)]
        PropertiesId_PlasmaWeaponDamage_Base = 0x77,
        [ProtoEnum(Name="PropertiesId_NormalAttackWeaponDamage_Base", Value=0x1cd)]
        PropertiesId_NormalAttackWeaponDamage_Base = 0x1cd,
        [ProtoEnum(Name="PropertiesId_CannonWeaponDamage_Base", Value=0x553)]
        PropertiesId_CannonWeaponDamage_Base = 0x553,
        [ProtoEnum(Name="PropertiesId_RailgunDamageMulti_ES", Value=120)]
        PropertiesId_RailgunDamageMulti_ES = 120,
        [ProtoEnum(Name="PropertiesId_MissileDamageMulti_ES", Value=0x79)]
        PropertiesId_MissileDamageMulti_ES = 0x79,
        [ProtoEnum(Name="PropertiesId_LaserDamageMulti_ES", Value=0x7a)]
        PropertiesId_LaserDamageMulti_ES = 0x7a,
        [ProtoEnum(Name="PropertiesId_PlasmaDamageMulti_ES", Value=0x7b)]
        PropertiesId_PlasmaDamageMulti_ES = 0x7b,
        [ProtoEnum(Name="PropertiesId_DroneDamageMulti_ES", Value=0x7c)]
        PropertiesId_DroneDamageMulti_ES = 0x7c,
        [ProtoEnum(Name="PropertiesId_NormalAttackDamageMulti_ES", Value=0x1cf)]
        PropertiesId_NormalAttackDamageMulti_ES = 0x1cf,
        [ProtoEnum(Name="PropertiesId_CannonDamageMulti_ES", Value=0x554)]
        PropertiesId_CannonDamageMulti_ES = 0x554,
        [ProtoEnum(Name="PropertiesId_AllDamageMulti_ES", Value=0x61e)]
        PropertiesId_AllDamageMulti_ES = 0x61e,
        [ProtoEnum(Name="PropertiesId_NormalAttackDamageMulti_ED", Value=0x1ce)]
        PropertiesId_NormalAttackDamageMulti_ED = 0x1ce,
        [ProtoEnum(Name="PropertiesId_AllDamageMulti_ED", Value=0x61f)]
        PropertiesId_AllDamageMulti_ED = 0x61f,
        [ProtoEnum(Name="PropertiesId_EquipDamageMulti_ED", Value=0x685)]
        PropertiesId_EquipDamageMulti_ED = 0x685,
        [ProtoEnum(Name="PropertiesId_RailgunDamageMulti_ED", Value=0x69d)]
        PropertiesId_RailgunDamageMulti_ED = 0x69d,
        [ProtoEnum(Name="PropertiesId_MissileDamageMulti_ED", Value=0x69e)]
        PropertiesId_MissileDamageMulti_ED = 0x69e,
        [ProtoEnum(Name="PropertiesId_LaserDamageMulti_ED", Value=0x69f)]
        PropertiesId_LaserDamageMulti_ED = 0x69f,
        [ProtoEnum(Name="PropertiesId_PlasmaDamageMulti_ED", Value=0x6a0)]
        PropertiesId_PlasmaDamageMulti_ED = 0x6a0,
        [ProtoEnum(Name="PropertiesId_DroneDamageMulti_ED", Value=0x6a1)]
        PropertiesId_DroneDamageMulti_ED = 0x6a1,
        [ProtoEnum(Name="PropertiesId_CannonDamageMulti_ED", Value=0x6a2)]
        PropertiesId_CannonDamageMulti_ED = 0x6a2,
        [ProtoEnum(Name="PropertiesId_FlagShipDamageMulti_ED", Value=0x51a)]
        PropertiesId_FlagShipDamageMulti_ED = 0x51a,
        [ProtoEnum(Name="PropertiesId_NormalAttackDamage_NS", Value=0x1d0)]
        PropertiesId_NormalAttackDamage_NS = 0x1d0,
        [ProtoEnum(Name="PropertiesId_RailgunDamage_NS", Value=0x74c)]
        PropertiesId_RailgunDamage_NS = 0x74c,
        [ProtoEnum(Name="PropertiesId_PlasmaDamage_NS", Value=0x74d)]
        PropertiesId_PlasmaDamage_NS = 0x74d,
        [ProtoEnum(Name="PropertiesId_MissileDamage_NS", Value=0x74e)]
        PropertiesId_MissileDamage_NS = 0x74e,
        [ProtoEnum(Name="PropertiesId_LaserDamage_NS", Value=0x74f)]
        PropertiesId_LaserDamage_NS = 0x74f,
        [ProtoEnum(Name="PropertiesId_NormalAttackDamage_ND", Value=0x1d1)]
        PropertiesId_NormalAttackDamage_ND = 0x1d1,
        [ProtoEnum(Name="PropertiesId_RailgunDamageMulti_ND", Value=0x6ae)]
        PropertiesId_RailgunDamageMulti_ND = 0x6ae,
        [ProtoEnum(Name="PropertiesId_MissileDamageMulti_ND", Value=0x6af)]
        PropertiesId_MissileDamageMulti_ND = 0x6af,
        [ProtoEnum(Name="PropertiesId_LaserDamageMulti_ND", Value=0x6b0)]
        PropertiesId_LaserDamageMulti_ND = 0x6b0,
        [ProtoEnum(Name="PropertiesId_PlasmaDamageMulti_ND", Value=0x6b1)]
        PropertiesId_PlasmaDamageMulti_ND = 0x6b1,
        [ProtoEnum(Name="PropertiesId_DroneDamageMulti_ND", Value=0x6b2)]
        PropertiesId_DroneDamageMulti_ND = 0x6b2,
        [ProtoEnum(Name="PropertiesId_CannonDamageMulti_ND", Value=0x6b3)]
        PropertiesId_CannonDamageMulti_ND = 0x6b3,
        [ProtoEnum(Name="PropertiesId_PlasmaDamage_Final", Value=0x1a9)]
        PropertiesId_PlasmaDamage_Final = 0x1a9,
        [ProtoEnum(Name="PropertiesId_RailgunDamage_Final", Value=0x1aa)]
        PropertiesId_RailgunDamage_Final = 0x1aa,
        [ProtoEnum(Name="PropertiesId_MissileDamage_Final", Value=0x1ab)]
        PropertiesId_MissileDamage_Final = 0x1ab,
        [ProtoEnum(Name="PropertiesId_LaserDamage_Final", Value=0x1ac)]
        PropertiesId_LaserDamage_Final = 0x1ac,
        [ProtoEnum(Name="PropertiesId_DroneDamage_Final", Value=0x1ad)]
        PropertiesId_DroneDamage_Final = 0x1ad,
        [ProtoEnum(Name="PropertiesId_NormalAttackDamage_Final", Value=0x1f1)]
        PropertiesId_NormalAttackDamage_Final = 0x1f1,
        [ProtoEnum(Name="PropertiesId_CannonDamage_Final", Value=0x583)]
        PropertiesId_CannonDamage_Final = 0x583,
        [ProtoEnum(Name="PropertiesId_EquipDamage_Final", Value=0x688)]
        PropertiesId_EquipDamage_Final = 0x688,
        [ProtoEnum(Name="PropertiesId_RailgunDamage_FinalNoCache", Value=0x666)]
        PropertiesId_RailgunDamage_FinalNoCache = 0x666,
        [ProtoEnum(Name="PropertiesId_CannonDamage_FinalNoCache", Value=0x667)]
        PropertiesId_CannonDamage_FinalNoCache = 0x667,
        [ProtoEnum(Name="PropertiesId_PlasmaDamage_FinalNoCache", Value=0x668)]
        PropertiesId_PlasmaDamage_FinalNoCache = 0x668,
        [ProtoEnum(Name="PropertiesId_MissileDamage_FinalNoCache", Value=0x669)]
        PropertiesId_MissileDamage_FinalNoCache = 0x669,
        [ProtoEnum(Name="PropertiesId_LaserDamage_FinalNoCache", Value=0x66a)]
        PropertiesId_LaserDamage_FinalNoCache = 0x66a,
        [ProtoEnum(Name="PropertiesId_DroneDamage_FinalNoCache", Value=0x66b)]
        PropertiesId_DroneDamage_FinalNoCache = 0x66b,
        [ProtoEnum(Name="PropertiesId_EquipDamage_FinalNoCache", Value=0x692)]
        PropertiesId_EquipDamage_FinalNoCache = 0x692,
        [ProtoEnum(Name="PropertiesId_RailgunDamageFinalModify_NoCache", Value=0x660)]
        PropertiesId_RailgunDamageFinalModify_NoCache = 0x660,
        [ProtoEnum(Name="PropertiesId_CannonDamageFinalModify_NoCache", Value=0x661)]
        PropertiesId_CannonDamageFinalModify_NoCache = 0x661,
        [ProtoEnum(Name="PropertiesId_PlasmaDamageFinalModify_NoCache", Value=0x662)]
        PropertiesId_PlasmaDamageFinalModify_NoCache = 0x662,
        [ProtoEnum(Name="PropertiesId_MissileDamageFinalModify_NoCache", Value=0x663)]
        PropertiesId_MissileDamageFinalModify_NoCache = 0x663,
        [ProtoEnum(Name="PropertiesId_LaserDamageFinalModify_NoCache", Value=0x664)]
        PropertiesId_LaserDamageFinalModify_NoCache = 0x664,
        [ProtoEnum(Name="PropertiesId_DroneDamageFinalModify_NoCache", Value=0x665)]
        PropertiesId_DroneDamageFinalModify_NoCache = 0x665,
        [ProtoEnum(Name="PropertiesId_EquipDamageFinalModify_NoCache", Value=0x693)]
        PropertiesId_EquipDamageFinalModify_NoCache = 0x693,
        [ProtoEnum(Name="PropertiesId_RailgunDamage_FinalFrameCache", Value=0x672)]
        PropertiesId_RailgunDamage_FinalFrameCache = 0x672,
        [ProtoEnum(Name="PropertiesId_CannonDamage_FinalFrameCache", Value=0x673)]
        PropertiesId_CannonDamage_FinalFrameCache = 0x673,
        [ProtoEnum(Name="PropertiesId_PlasmaDamage_FinalFrameCache", Value=0x674)]
        PropertiesId_PlasmaDamage_FinalFrameCache = 0x674,
        [ProtoEnum(Name="PropertiesId_MissileDamage_FinalFrameCache", Value=0x675)]
        PropertiesId_MissileDamage_FinalFrameCache = 0x675,
        [ProtoEnum(Name="PropertiesId_LaserDamage_FinalFrameCache", Value=0x676)]
        PropertiesId_LaserDamage_FinalFrameCache = 0x676,
        [ProtoEnum(Name="PropertiesId_DroneDamage_FinalFrameCache", Value=0x677)]
        PropertiesId_DroneDamage_FinalFrameCache = 0x677,
        [ProtoEnum(Name="PropertiesId_EquipDamage_FinalFrameCache", Value=0x691)]
        PropertiesId_EquipDamage_FinalFrameCache = 0x691,
        [ProtoEnum(Name="PropertiesId_RailgunDamageFinalModify_FrameCache", Value=0x66c)]
        PropertiesId_RailgunDamageFinalModify_FrameCache = 0x66c,
        [ProtoEnum(Name="PropertiesId_CannonDamageFinalModify_FrameCache", Value=0x66d)]
        PropertiesId_CannonDamageFinalModify_FrameCache = 0x66d,
        [ProtoEnum(Name="PropertiesId_PlasmaDamageFinalModify_FrameCache", Value=0x66e)]
        PropertiesId_PlasmaDamageFinalModify_FrameCache = 0x66e,
        [ProtoEnum(Name="PropertiesId_MissileDamageFinalModify_FrameCache", Value=0x66f)]
        PropertiesId_MissileDamageFinalModify_FrameCache = 0x66f,
        [ProtoEnum(Name="PropertiesId_LaserDamageFinalModify_FrameCache", Value=0x670)]
        PropertiesId_LaserDamageFinalModify_FrameCache = 0x670,
        [ProtoEnum(Name="PropertiesId_DroneDamageFinalModify_FrameCache", Value=0x671)]
        PropertiesId_DroneDamageFinalModify_FrameCache = 0x671,
        [ProtoEnum(Name="PropertiesId_EquipDamageFinalModify_FrameCache", Value=0x694)]
        PropertiesId_EquipDamageFinalModify_FrameCache = 0x694,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoDamage_Base", Value=0x21)]
        PropertiesId_RailgunAmmoDamage_Base = 0x21,
        [ProtoEnum(Name="PropertiesId_MissileAmmoDamage_Base", Value=0x22)]
        PropertiesId_MissileAmmoDamage_Base = 0x22,
        [ProtoEnum(Name="PropertiesId_LaserAmmoDamage_Base", Value=0x23)]
        PropertiesId_LaserAmmoDamage_Base = 0x23,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoDamage_Base", Value=0x26)]
        PropertiesId_PlasmaAmmoDamage_Base = 0x26,
        [ProtoEnum(Name="PropertiesId_DroneAmmoDamage_Base", Value=0x27)]
        PropertiesId_DroneAmmoDamage_Base = 0x27,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoDamage_Base", Value=0x1c3)]
        PropertiesId_NormalAttackAmmoDamage_Base = 0x1c3,
        [ProtoEnum(Name="PropertiesId_CannonAmmoDamage_Base", Value=0x549)]
        PropertiesId_CannonAmmoDamage_Base = 0x549,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoDamageModify_Base", Value=0x37)]
        PropertiesId_RailgunAmmoDamageModify_Base = 0x37,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoDamageModify_Base", Value=0x38)]
        PropertiesId_PlasmaAmmoDamageModify_Base = 0x38,
        [ProtoEnum(Name="PropertiesId_LaserAmmoDamageModify_Base", Value=0x39)]
        PropertiesId_LaserAmmoDamageModify_Base = 0x39,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoDamageModify_Base", Value=0x1c7)]
        PropertiesId_NormalAttackAmmoDamageModify_Base = 0x1c7,
        [ProtoEnum(Name="PropertiesId_CannonAmmoDamageModify_Base", Value=0x54d)]
        PropertiesId_CannonAmmoDamageModify_Base = 0x54d,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoElectricDamageOccupancy_Base", Value=0x2a)]
        PropertiesId_RailgunAmmoElectricDamageOccupancy_Base = 0x2a,
        [ProtoEnum(Name="PropertiesId_MissileAmmoElectricDamageOccupancy_Base", Value=0x2d)]
        PropertiesId_MissileAmmoElectricDamageOccupancy_Base = 0x2d,
        [ProtoEnum(Name="PropertiesId_LaserAmmoElectricDamageOccupancy_Base", Value=0x30)]
        PropertiesId_LaserAmmoElectricDamageOccupancy_Base = 0x30,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoElectricDamageOccupancy_Base", Value=0x33)]
        PropertiesId_PlasmaAmmoElectricDamageOccupancy_Base = 0x33,
        [ProtoEnum(Name="PropertiesId_DroneAmmoElectricDamageOccupancy_Base", Value=0x36)]
        PropertiesId_DroneAmmoElectricDamageOccupancy_Base = 0x36,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoElectricDamageOccupancy_Base", Value=0x1c6)]
        PropertiesId_NormalAttackAmmoElectricDamageOccupancy_Base = 0x1c6,
        [ProtoEnum(Name="PropertiesId_CannonAmmoElectricDamageOccupancy_Base", Value=0x54c)]
        PropertiesId_CannonAmmoElectricDamageOccupancy_Base = 0x54c,
        [ProtoEnum(Name="PropertiesId_EquipElectricDamageOccupancy_Base", Value=0x680)]
        PropertiesId_EquipElectricDamageOccupancy_Base = 0x680,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoHeatDamageOccupancy_Base", Value=40)]
        PropertiesId_RailgunAmmoHeatDamageOccupancy_Base = 40,
        [ProtoEnum(Name="PropertiesId_MissileAmmoHeatDamageOccupancy_Base", Value=0x2b)]
        PropertiesId_MissileAmmoHeatDamageOccupancy_Base = 0x2b,
        [ProtoEnum(Name="PropertiesId_LaserAmmoHeatDamageOccupancy_Base", Value=0x2e)]
        PropertiesId_LaserAmmoHeatDamageOccupancy_Base = 0x2e,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoHeatDamageOccupancy_Base", Value=0x31)]
        PropertiesId_PlasmaAmmoHeatDamageOccupancy_Base = 0x31,
        [ProtoEnum(Name="PropertiesId_DroneAmmoHeatDamageOccupancy_Base", Value=0x34)]
        PropertiesId_DroneAmmoHeatDamageOccupancy_Base = 0x34,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoHeatDamageOccupancy_Base", Value=0x1c4)]
        PropertiesId_NormalAttackAmmoHeatDamageOccupancy_Base = 0x1c4,
        [ProtoEnum(Name="PropertiesId_CannonAmmoHeatDamageOccupancy_Base", Value=0x54a)]
        PropertiesId_CannonAmmoHeatDamageOccupancy_Base = 0x54a,
        [ProtoEnum(Name="PropertiesId_EquipHeatDamageOccupancy_Base", Value=0x67e)]
        PropertiesId_EquipHeatDamageOccupancy_Base = 0x67e,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoKineticDamageOccupancy_Base", Value=0x29)]
        PropertiesId_RailgunAmmoKineticDamageOccupancy_Base = 0x29,
        [ProtoEnum(Name="PropertiesId_MissileAmmoKineticDamageOccupancy_Base", Value=0x2c)]
        PropertiesId_MissileAmmoKineticDamageOccupancy_Base = 0x2c,
        [ProtoEnum(Name="PropertiesId_LaserAmmoKineticDamageOccupancy_Base", Value=0x2f)]
        PropertiesId_LaserAmmoKineticDamageOccupancy_Base = 0x2f,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoKineticDamageOccupancy_Base", Value=50)]
        PropertiesId_PlasmaAmmoKineticDamageOccupancy_Base = 50,
        [ProtoEnum(Name="PropertiesId_DroneAmmoKineticDamageOccupancy_Base", Value=0x35)]
        PropertiesId_DroneAmmoKineticDamageOccupancy_Base = 0x35,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoKineticDamageOccupancy_Base", Value=0x1c5)]
        PropertiesId_NormalAttackAmmoKineticDamageOccupancy_Base = 0x1c5,
        [ProtoEnum(Name="PropertiesId_CannonAmmoKineticDamageOccupancy_Base", Value=0x54b)]
        PropertiesId_CannonAmmoKineticDamageOccupancy_Base = 0x54b,
        [ProtoEnum(Name="PropertiesId_EquipKineticDamageOccupancy_Base", Value=0x67f)]
        PropertiesId_EquipKineticDamageOccupancy_Base = 0x67f,
        [ProtoEnum(Name="PropertiesId_LaserAmmoInitDamageOccupancy_Base", Value=0x24)]
        PropertiesId_LaserAmmoInitDamageOccupancy_Base = 0x24,
        [ProtoEnum(Name="PropertiesId_LaserAmmoMaxDamageOccupancy_Base", Value=0x25)]
        PropertiesId_LaserAmmoMaxDamageOccupancy_Base = 0x25,
        [ProtoEnum(Name="PropertiesId_RealDamage_ED", Value=0x58)]
        PropertiesId_RealDamage_ED = 0x58,
        [ProtoEnum(Name="PropertiesId_RealDamageReduce_ED", Value=90)]
        PropertiesId_RealDamageReduce_ED = 90,
        [ProtoEnum(Name="PropertiesId_RealDamageReduce_ES", Value=0x5b)]
        PropertiesId_RealDamageReduce_ES = 0x5b,
        [ProtoEnum(Name="PropertiesId_RealDamageReduce_Final", Value=0x1af)]
        PropertiesId_RealDamageReduce_Final = 0x1af,
        [ProtoEnum(Name="PropertiesId_ElectricRealDamageAdd_ED", Value=0x57)]
        PropertiesId_ElectricRealDamageAdd_ED = 0x57,
        [ProtoEnum(Name="PropertiesId_ElectricRealDamage_Final", Value=0x1b7)]
        PropertiesId_ElectricRealDamage_Final = 0x1b7,
        [ProtoEnum(Name="PropertiesId_HeatRealDamageAdd_ED", Value=0x55)]
        PropertiesId_HeatRealDamageAdd_ED = 0x55,
        [ProtoEnum(Name="PropertiesId_HeatRealDamage_Final", Value=0x1b5)]
        PropertiesId_HeatRealDamage_Final = 0x1b5,
        [ProtoEnum(Name="PropertiesId_KineticRealDamageAdd_ED", Value=0x56)]
        PropertiesId_KineticRealDamageAdd_ED = 0x56,
        [ProtoEnum(Name="PropertiesId_KineticRealDamage_Final", Value=0x1b6)]
        PropertiesId_KineticRealDamage_Final = 0x1b6,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextWave_Base", Value=0x102)]
        PropertiesId_MissileCD4NextWave_Base = 0x102,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextWave_Base", Value=0x103)]
        PropertiesId_PlasmaCD4NextWave_Base = 0x103,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextWave_Base", Value=260)]
        PropertiesId_RailgunCD4NextWave_Base = 260,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextWave_Base", Value=0x105)]
        PropertiesId_LaserCD4NextWave_Base = 0x105,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextWave_Base", Value=0x106)]
        PropertiesId_DroneCD4NextWave_Base = 0x106,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextWave_Base", Value=0x1e1)]
        PropertiesId_NormalAttackCD4NextWave_Base = 0x1e1,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextWave_Base", Value=0x566)]
        PropertiesId_CannonCD4NextWave_Base = 0x566,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextWave_Final", Value=0x184)]
        PropertiesId_MissileCD4NextWave_Final = 0x184,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextWave_Final", Value=0x185)]
        PropertiesId_PlasmaCD4NextWave_Final = 0x185,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextWave_Final", Value=390)]
        PropertiesId_RailgunCD4NextWave_Final = 390,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextWave_Final", Value=0x187)]
        PropertiesId_LaserCD4NextWave_Final = 0x187,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextWave_Final", Value=0x188)]
        PropertiesId_DroneCD4NextWave_Final = 0x188,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextWave_Final", Value=0x1ec)]
        PropertiesId_NormalAttackCD4NextWave_Final = 0x1ec,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextWave_Final", Value=0x57c)]
        PropertiesId_CannonCD4NextWave_Final = 0x57c,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextGroup_Base", Value=0x107)]
        PropertiesId_MissileCD4NextGroup_Base = 0x107,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextGroup_Base", Value=0x10c)]
        PropertiesId_PlasmaCD4NextGroup_Base = 0x10c,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextGroup_Base", Value=0x111)]
        PropertiesId_RailgunCD4NextGroup_Base = 0x111,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextGroup_Base", Value=0x116)]
        PropertiesId_LaserCD4NextGroup_Base = 0x116,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextGroup_Base", Value=0x11b)]
        PropertiesId_DroneCD4NextGroup_Base = 0x11b,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextGroup_Base", Value=0x1e2)]
        PropertiesId_NormalAttackCD4NextGroup_Base = 0x1e2,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextGroup_Base", Value=0x567)]
        PropertiesId_CannonCD4NextGroup_Base = 0x567,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextGroupAdd_ES", Value=0x108)]
        PropertiesId_MissileCD4NextGroupAdd_ES = 0x108,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextGroupAdd_ES", Value=0x10d)]
        PropertiesId_PlasmaCD4NextGroupAdd_ES = 0x10d,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextGroupAdd_ES", Value=0x112)]
        PropertiesId_RailgunCD4NextGroupAdd_ES = 0x112,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextGroupAdd_ES", Value=0x117)]
        PropertiesId_LaserCD4NextGroupAdd_ES = 0x117,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextGroupAdd_ES", Value=0x11c)]
        PropertiesId_DroneCD4NextGroupAdd_ES = 0x11c,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextGroupAdd_ES", Value=0x1e3)]
        PropertiesId_NormalAttackCD4NextGroupAdd_ES = 0x1e3,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextGroupAdd_ES", Value=0x568)]
        PropertiesId_CannonCD4NextGroupAdd_ES = 0x568,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextGroupAdd_ED", Value=0x109)]
        PropertiesId_MissileCD4NextGroupAdd_ED = 0x109,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextGroupAdd_ED", Value=270)]
        PropertiesId_PlasmaCD4NextGroupAdd_ED = 270,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextGroupAdd_ED", Value=0x113)]
        PropertiesId_RailgunCD4NextGroupAdd_ED = 0x113,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextGroupAdd_ED", Value=280)]
        PropertiesId_LaserCD4NextGroupAdd_ED = 280,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextGroupAdd_ED", Value=0x11d)]
        PropertiesId_DroneCD4NextGroupAdd_ED = 0x11d,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextGroup_NS", Value=0x10a)]
        PropertiesId_MissileCD4NextGroup_NS = 0x10a,
        [ProtoEnum(Name="PropertiesId_MissileSCD4NextGroup_NS", Value=0x29b)]
        PropertiesId_MissileSCD4NextGroup_NS = 0x29b,
        [ProtoEnum(Name="PropertiesId_MissileMCD4NextGroup_NS", Value=0x29c)]
        PropertiesId_MissileMCD4NextGroup_NS = 0x29c,
        [ProtoEnum(Name="PropertiesId_MissileLCD4NextGroup_NS", Value=0x29d)]
        PropertiesId_MissileLCD4NextGroup_NS = 0x29d,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextGroup_NS", Value=0x10f)]
        PropertiesId_PlasmaCD4NextGroup_NS = 0x10f,
        [ProtoEnum(Name="PropertiesId_PlasmaSCD4NextGroup_NS", Value=0x2a3)]
        PropertiesId_PlasmaSCD4NextGroup_NS = 0x2a3,
        [ProtoEnum(Name="PropertiesId_PlasmaMCD4NextGroup_NS", Value=0x2a7)]
        PropertiesId_PlasmaMCD4NextGroup_NS = 0x2a7,
        [ProtoEnum(Name="PropertiesId_PlasmaLCD4NextGroup_NS", Value=680)]
        PropertiesId_PlasmaLCD4NextGroup_NS = 680,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextGroup_NS", Value=0x114)]
        PropertiesId_RailgunCD4NextGroup_NS = 0x114,
        [ProtoEnum(Name="PropertiesId_RailgunSCD4NextGroup_NS", Value=0x293)]
        PropertiesId_RailgunSCD4NextGroup_NS = 0x293,
        [ProtoEnum(Name="PropertiesId_RailgunMCD4NextGroup_NS", Value=0x297)]
        PropertiesId_RailgunMCD4NextGroup_NS = 0x297,
        [ProtoEnum(Name="PropertiesId_RailgunLCD4NextGroup_NS", Value=0x298)]
        PropertiesId_RailgunLCD4NextGroup_NS = 0x298,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextGroup_NS", Value=0x119)]
        PropertiesId_LaserCD4NextGroup_NS = 0x119,
        [ProtoEnum(Name="PropertiesId_LaserSCD4NextGroup_NS", Value=670)]
        PropertiesId_LaserSCD4NextGroup_NS = 670,
        [ProtoEnum(Name="PropertiesId_LaserMCD4NextGroup_NS", Value=0x2a1)]
        PropertiesId_LaserMCD4NextGroup_NS = 0x2a1,
        [ProtoEnum(Name="PropertiesId_LaserLCD4NextGroup_NS", Value=0x2a2)]
        PropertiesId_LaserLCD4NextGroup_NS = 0x2a2,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextGroup_NS", Value=0x11e)]
        PropertiesId_DroneCD4NextGroup_NS = 0x11e,
        [ProtoEnum(Name="PropertiesId_DroneSCD4NextGroup_NS", Value=0x2ab)]
        PropertiesId_DroneSCD4NextGroup_NS = 0x2ab,
        [ProtoEnum(Name="PropertiesId_DroneMCD4NextGroup_NS", Value=0x2ac)]
        PropertiesId_DroneMCD4NextGroup_NS = 0x2ac,
        [ProtoEnum(Name="PropertiesId_DroneLCD4NextGroup_NS", Value=0x2af)]
        PropertiesId_DroneLCD4NextGroup_NS = 0x2af,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextGroup_NS", Value=0x1e5)]
        PropertiesId_NormalAttackCD4NextGroup_NS = 0x1e5,
        [ProtoEnum(Name="PropertiesId_MissileCD4NextGroup_ND", Value=0x10b)]
        PropertiesId_MissileCD4NextGroup_ND = 0x10b,
        [ProtoEnum(Name="PropertiesId_PlasmaCD4NextGroup_ND", Value=0x110)]
        PropertiesId_PlasmaCD4NextGroup_ND = 0x110,
        [ProtoEnum(Name="PropertiesId_RailgunCD4NextGroup_ND", Value=0x115)]
        PropertiesId_RailgunCD4NextGroup_ND = 0x115,
        [ProtoEnum(Name="PropertiesId_LaserCD4NextGroup_ND", Value=0x11a)]
        PropertiesId_LaserCD4NextGroup_ND = 0x11a,
        [ProtoEnum(Name="PropertiesId_DroneCD4NextGroup_ND", Value=0x11f)]
        PropertiesId_DroneCD4NextGroup_ND = 0x11f,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextGroup_ND", Value=0x1e6)]
        PropertiesId_NormalAttackCD4NextGroup_ND = 0x1e6,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextGroup_ND", Value=0x56b)]
        PropertiesId_CannonCD4NextGroup_ND = 0x56b,
        [ProtoEnum(Name="PropertiesId_MissileSCD4NextGroup_Final", Value=0x189)]
        PropertiesId_MissileSCD4NextGroup_Final = 0x189,
        [ProtoEnum(Name="PropertiesId_MissileMCD4NextGroup_Final", Value=0x4cc)]
        PropertiesId_MissileMCD4NextGroup_Final = 0x4cc,
        [ProtoEnum(Name="PropertiesId_MissileLCD4NextGroup_Final", Value=0x4cd)]
        PropertiesId_MissileLCD4NextGroup_Final = 0x4cd,
        [ProtoEnum(Name="PropertiesId_PlasmaSCD4NextGroup_Final", Value=0x18a)]
        PropertiesId_PlasmaSCD4NextGroup_Final = 0x18a,
        [ProtoEnum(Name="PropertiesId_PlasmaMCD4NextGroup_Final", Value=0x4d6)]
        PropertiesId_PlasmaMCD4NextGroup_Final = 0x4d6,
        [ProtoEnum(Name="PropertiesId_PlasmaLCD4NextGroup_Final", Value=0x4d7)]
        PropertiesId_PlasmaLCD4NextGroup_Final = 0x4d7,
        [ProtoEnum(Name="PropertiesId_RailgunSCD4NextGroup_Final", Value=0x18b)]
        PropertiesId_RailgunSCD4NextGroup_Final = 0x18b,
        [ProtoEnum(Name="PropertiesId_RailgunMCD4NextGroup_Final", Value=0x4ca)]
        PropertiesId_RailgunMCD4NextGroup_Final = 0x4ca,
        [ProtoEnum(Name="PropertiesId_RailgunLCD4NextGroup_Final", Value=0x4cb)]
        PropertiesId_RailgunLCD4NextGroup_Final = 0x4cb,
        [ProtoEnum(Name="PropertiesId_LaserSCD4NextGroup_Final", Value=0x18c)]
        PropertiesId_LaserSCD4NextGroup_Final = 0x18c,
        [ProtoEnum(Name="PropertiesId_LaserMCD4NextGroup_Final", Value=0x4d2)]
        PropertiesId_LaserMCD4NextGroup_Final = 0x4d2,
        [ProtoEnum(Name="PropertiesId_LaserLCD4NextGroup_Final", Value=0x4d3)]
        PropertiesId_LaserLCD4NextGroup_Final = 0x4d3,
        [ProtoEnum(Name="PropertiesId_DroneSCD4NextGroup_Final", Value=0x18d)]
        PropertiesId_DroneSCD4NextGroup_Final = 0x18d,
        [ProtoEnum(Name="PropertiesId_DroneMCD4NextGroup_Final", Value=0x4dc)]
        PropertiesId_DroneMCD4NextGroup_Final = 0x4dc,
        [ProtoEnum(Name="PropertiesId_DroneLCD4NextGroup_Final", Value=0x4dd)]
        PropertiesId_DroneLCD4NextGroup_Final = 0x4dd,
        [ProtoEnum(Name="PropertiesId_NormalAttackCD4NextGroup_Final", Value=0x1ed)]
        PropertiesId_NormalAttackCD4NextGroup_Final = 0x1ed,
        [ProtoEnum(Name="PropertiesId_CannonCD4NextGroup_Final", Value=0x57d)]
        PropertiesId_CannonCD4NextGroup_Final = 0x57d,
        [ProtoEnum(Name="PropertiesId_RailgunCPUCost_Base", Value=0x146)]
        PropertiesId_RailgunCPUCost_Base = 0x146,
        [ProtoEnum(Name="PropertiesId_MissileCPUCost_Base", Value=0x147)]
        PropertiesId_MissileCPUCost_Base = 0x147,
        [ProtoEnum(Name="PropertiesId_LaserCPUCost_Base", Value=0x148)]
        PropertiesId_LaserCPUCost_Base = 0x148,
        [ProtoEnum(Name="PropertiesId_PlasmaCPUCost_Base", Value=0x149)]
        PropertiesId_PlasmaCPUCost_Base = 0x149,
        [ProtoEnum(Name="PropertiesId_DroneCPUCost_Base", Value=330)]
        PropertiesId_DroneCPUCost_Base = 330,
        [ProtoEnum(Name="PropertiesId_CannonCPUCost_Base", Value=0x571)]
        PropertiesId_CannonCPUCost_Base = 0x571,
        [ProtoEnum(Name="PropertiesId_RailgunCPUCost_Final", Value=410)]
        PropertiesId_RailgunCPUCost_Final = 410,
        [ProtoEnum(Name="PropertiesId_MissileCPUCost_Final", Value=0x19b)]
        PropertiesId_MissileCPUCost_Final = 0x19b,
        [ProtoEnum(Name="PropertiesId_LaserCPUCost_Final", Value=0x19c)]
        PropertiesId_LaserCPUCost_Final = 0x19c,
        [ProtoEnum(Name="PropertiesId_PlasmaCPUCost_Final", Value=0x19d)]
        PropertiesId_PlasmaCPUCost_Final = 0x19d,
        [ProtoEnum(Name="PropertiesId_DroneCPUCost_Final", Value=0x19e)]
        PropertiesId_DroneCPUCost_Final = 0x19e,
        [ProtoEnum(Name="PropertiesId_CannonCPUCost_Final", Value=0x580)]
        PropertiesId_CannonCPUCost_Final = 0x580,
        [ProtoEnum(Name="PropertiesId_RailgunPowerCost_Base", Value=0x155)]
        PropertiesId_RailgunPowerCost_Base = 0x155,
        [ProtoEnum(Name="PropertiesId_MissilePowerCost_Base", Value=0x156)]
        PropertiesId_MissilePowerCost_Base = 0x156,
        [ProtoEnum(Name="PropertiesId_LaserPowerCost_Base", Value=0x157)]
        PropertiesId_LaserPowerCost_Base = 0x157,
        [ProtoEnum(Name="PropertiesId_PlasmaPowerCost_Base", Value=0x158)]
        PropertiesId_PlasmaPowerCost_Base = 0x158,
        [ProtoEnum(Name="PropertiesId_DronePowerCost_Base", Value=0x159)]
        PropertiesId_DronePowerCost_Base = 0x159,
        [ProtoEnum(Name="PropertiesId_CannonPowerCost_Base", Value=0x574)]
        PropertiesId_CannonPowerCost_Base = 0x574,
        [ProtoEnum(Name="PropertiesId_RailgunPowerCost_Final", Value=0x19f)]
        PropertiesId_RailgunPowerCost_Final = 0x19f,
        [ProtoEnum(Name="PropertiesId_MissilePowerCost_Final", Value=0x1a0)]
        PropertiesId_MissilePowerCost_Final = 0x1a0,
        [ProtoEnum(Name="PropertiesId_LaserPowerCost_Final", Value=0x1a1)]
        PropertiesId_LaserPowerCost_Final = 0x1a1,
        [ProtoEnum(Name="PropertiesId_PlasmaPowerCost_Final", Value=0x1a2)]
        PropertiesId_PlasmaPowerCost_Final = 0x1a2,
        [ProtoEnum(Name="PropertiesId_DronePowerCost_Final", Value=0x1a3)]
        PropertiesId_DronePowerCost_Final = 0x1a3,
        [ProtoEnum(Name="PropertiesId_CannonPowerCost_Final", Value=0x581)]
        PropertiesId_CannonPowerCost_Final = 0x581,
        [ProtoEnum(Name="PropertiesId_MissileAmmoReloadTime_Base", Value=290)]
        PropertiesId_MissileAmmoReloadTime_Base = 290,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoReloadTime_Base", Value=0x123)]
        PropertiesId_PlasmaAmmoReloadTime_Base = 0x123,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoReloadTime_Base", Value=0x124)]
        PropertiesId_RailgunAmmoReloadTime_Base = 0x124,
        [ProtoEnum(Name="PropertiesId_LaserAmmoReloadTime_Base", Value=0x125)]
        PropertiesId_LaserAmmoReloadTime_Base = 0x125,
        [ProtoEnum(Name="PropertiesId_CannonAmmoReloadTime_Base", Value=0x56c)]
        PropertiesId_CannonAmmoReloadTime_Base = 0x56c,
        [ProtoEnum(Name="PropertiesId_MissileAmmoReloadTime_Final", Value=0x18e)]
        PropertiesId_MissileAmmoReloadTime_Final = 0x18e,
        [ProtoEnum(Name="PropertiesId_PlasmaAmmoReloadTime_Final", Value=0x18f)]
        PropertiesId_PlasmaAmmoReloadTime_Final = 0x18f,
        [ProtoEnum(Name="PropertiesId_LaserAmmoReloadTime_Final", Value=400)]
        PropertiesId_LaserAmmoReloadTime_Final = 400,
        [ProtoEnum(Name="PropertiesId_RailgunAmmoReloadTime_Final", Value=0x191)]
        PropertiesId_RailgunAmmoReloadTime_Final = 0x191,
        [ProtoEnum(Name="PropertiesId_NormalAttackAmmoReloadTime_Final", Value=0x1ee)]
        PropertiesId_NormalAttackAmmoReloadTime_Final = 0x1ee,
        [ProtoEnum(Name="PropertiesId_CannonAmmoReloadTime_Final", Value=0x57e)]
        PropertiesId_CannonAmmoReloadTime_Final = 0x57e,
        [ProtoEnum(Name="PropertiesId_BoosterRoundCD_Base", Value=0x1f2)]
        PropertiesId_BoosterRoundCD_Base = 0x1f2,
        [ProtoEnum(Name="PropertiesId_BoosterRoundCDMulti_ES", Value=0x1f3)]
        PropertiesId_BoosterRoundCDMulti_ES = 0x1f3,
        [ProtoEnum(Name="PropertiesId_BoosterRoundCDMulti_ED", Value=500)]
        PropertiesId_BoosterRoundCDMulti_ED = 500,
        [ProtoEnum(Name="PropertiesId_BoosterRoundCD_NS", Value=0x4e6)]
        PropertiesId_BoosterRoundCD_NS = 0x4e6,
        [ProtoEnum(Name="PropertiesId_BoosterRoundCD_Final", Value=0x1f7)]
        PropertiesId_BoosterRoundCD_Final = 0x1f7,
        [ProtoEnum(Name="PropertiesId_BoosterCostEnergy_Base", Value=0x1f8)]
        PropertiesId_BoosterCostEnergy_Base = 0x1f8,
        [ProtoEnum(Name="PropertiesId_BoosterCostEnergyMulti_ES", Value=0x1f9)]
        PropertiesId_BoosterCostEnergyMulti_ES = 0x1f9,
        [ProtoEnum(Name="PropertiesId_BoosterCostEnergy_NS", Value=0x1fb)]
        PropertiesId_BoosterCostEnergy_NS = 0x1fb,
        [ProtoEnum(Name="PropertiesId_BoosterCostEnergy_ND", Value=0x1fc)]
        PropertiesId_BoosterCostEnergy_ND = 0x1fc,
        [ProtoEnum(Name="PropertiesId_BoosterCostEnergy_Final", Value=0x1fd)]
        PropertiesId_BoosterCostEnergy_Final = 0x1fd,
        [ProtoEnum(Name="PropertiesId_BoosterCostCPU_Base", Value=510)]
        PropertiesId_BoosterCostCPU_Base = 510,
        [ProtoEnum(Name="PropertiesId_BoosterCostCPU_Final", Value=0x201)]
        PropertiesId_BoosterCostCPU_Final = 0x201,
        [ProtoEnum(Name="PropertiesId_BoosterCostPower_Base", Value=0x202)]
        PropertiesId_BoosterCostPower_Base = 0x202,
        [ProtoEnum(Name="PropertiesId_BoosterCostPower_Final", Value=0x205)]
        PropertiesId_BoosterCostPower_Final = 0x205,
        [ProtoEnum(Name="PropertiesId_BoosterMass_Base", Value=520)]
        PropertiesId_BoosterMass_Base = 520,
        [ProtoEnum(Name="PropertiesId_BoosterThurstRatio_Base", Value=0x207)]
        PropertiesId_BoosterThurstRatio_Base = 0x207,
        [ProtoEnum(Name="PropertiesId_BoosterThurstRatio_Multi_ES", Value=0x6a8)]
        PropertiesId_BoosterThurstRatio_Multi_ES = 0x6a8,
        [ProtoEnum(Name="PropertiesId_BoosterThurstStrength_Base", Value=0x206)]
        PropertiesId_BoosterThurstStrength_Base = 0x206,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterRoundCD_Base", Value=0x209)]
        PropertiesId_ExplosiveBoosterRoundCD_Base = 0x209,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterRoundCDMulti_ES", Value=0x20a)]
        PropertiesId_ExplosiveBoosterRoundCDMulti_ES = 0x20a,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterRoundCD_Final", Value=0x20e)]
        PropertiesId_ExplosiveBoosterRoundCD_Final = 0x20e,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostEnergy_Base", Value=0x20f)]
        PropertiesId_ExplosiveBoosterCostEnergy_Base = 0x20f,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostEnergyMulti_ES", Value=0x210)]
        PropertiesId_ExplosiveBoosterCostEnergyMulti_ES = 0x210,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostEnergy_NS", Value=530)]
        PropertiesId_ExplosiveBoosterCostEnergy_NS = 530,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostEnergy_ND", Value=0x213)]
        PropertiesId_ExplosiveBoosterCostEnergy_ND = 0x213,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostEnergy_Final", Value=0x214)]
        PropertiesId_ExplosiveBoosterCostEnergy_Final = 0x214,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostCPU_Base", Value=0x215)]
        PropertiesId_ExplosiveBoosterCostCPU_Base = 0x215,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostCPU_Final", Value=0x218)]
        PropertiesId_ExplosiveBoosterCostCPU_Final = 0x218,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostPower_Base", Value=0x219)]
        PropertiesId_ExplosiveBoosterCostPower_Base = 0x219,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterCostPower_Final", Value=540)]
        PropertiesId_ExplosiveBoosterCostPower_Final = 540,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterMass_Base", Value=0x21f)]
        PropertiesId_ExplosiveBoosterMass_Base = 0x21f,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterThurstRatio_Base", Value=0x21e)]
        PropertiesId_ExplosiveBoosterThurstRatio_Base = 0x21e,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterThurstRatio_Multi_ES", Value=0x6a9)]
        PropertiesId_ExplosiveBoosterThurstRatio_Multi_ES = 0x6a9,
        [ProtoEnum(Name="PropertiesId_ExplosiveBoosterThurstStrength_Base", Value=0x21d)]
        PropertiesId_ExplosiveBoosterThurstStrength_Base = 0x21d,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorRoundCD_Base", Value=0x220)]
        PropertiesId_BlinkExecutorRoundCD_Base = 0x220,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorRoundCDMulti_ES", Value=0x221)]
        PropertiesId_BlinkExecutorRoundCDMulti_ES = 0x221,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorRoundCDMulti_ED", Value=0x222)]
        PropertiesId_BlinkExecutorRoundCDMulti_ED = 0x222,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorRoundCD_NS", Value=0x223)]
        PropertiesId_BlinkExecutorRoundCD_NS = 0x223,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorRoundCD_Final", Value=0x225)]
        PropertiesId_BlinkExecutorRoundCD_Final = 0x225,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostEnergy_Base", Value=550)]
        PropertiesId_BlinkExecutorCostEnergy_Base = 550,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostEnergyMulti_ES", Value=0x227)]
        PropertiesId_BlinkExecutorCostEnergyMulti_ES = 0x227,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostEnergy_NS", Value=0x229)]
        PropertiesId_BlinkExecutorCostEnergy_NS = 0x229,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostEnergy_ND", Value=0x22a)]
        PropertiesId_BlinkExecutorCostEnergy_ND = 0x22a,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostEnergy_Final", Value=0x22b)]
        PropertiesId_BlinkExecutorCostEnergy_Final = 0x22b,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorPercentageCostEnergy_Base", Value=0x6be)]
        PropertiesId_BlinkExecutorPercentageCostEnergy_Base = 0x6be,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorPercentageCostEnergyMulti_ES", Value=0x6bf)]
        PropertiesId_BlinkExecutorPercentageCostEnergyMulti_ES = 0x6bf,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorPercentageCostEnergy_Final", Value=0x6c3)]
        PropertiesId_BlinkExecutorPercentageCostEnergy_Final = 0x6c3,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostCPU_Base", Value=0x22c)]
        PropertiesId_BlinkExecutorCostCPU_Base = 0x22c,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostCPU_Final", Value=0x22f)]
        PropertiesId_BlinkExecutorCostCPU_Final = 0x22f,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostPower_Base", Value=560)]
        PropertiesId_BlinkExecutorCostPower_Base = 560,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorCostPower_Final", Value=0x233)]
        PropertiesId_BlinkExecutorCostPower_Final = 0x233,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelay_Base", Value=0x235)]
        PropertiesId_BlinkExecutorDelay_Base = 0x235,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelayMulti_ES", Value=0x236)]
        PropertiesId_BlinkExecutorDelayMulti_ES = 0x236,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelayMulti_ED", Value=0x237)]
        PropertiesId_BlinkExecutorDelayMulti_ED = 0x237,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelay_NS", Value=0x238)]
        PropertiesId_BlinkExecutorDelay_NS = 0x238,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelay_ND", Value=0x239)]
        PropertiesId_BlinkExecutorDelay_ND = 0x239,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorDelay_Final", Value=570)]
        PropertiesId_BlinkExecutorDelay_Final = 570,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorJumpDistance_Base", Value=0x234)]
        PropertiesId_BlinkExecutorJumpDistance_Base = 0x234,
        [ProtoEnum(Name="PropertiesId_BlinkExecutorJumpDistance_Final", Value=0x6fa)]
        PropertiesId_BlinkExecutorJumpDistance_Final = 0x6fa,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageRoundCD_Base", Value=0x45d)]
        PropertiesId_ExtraEnergyPackageRoundCD_Base = 0x45d,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageRoundCDMulti_ES", Value=0x45e)]
        PropertiesId_ExtraEnergyPackageRoundCDMulti_ES = 0x45e,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageRoundCDMulti_ED", Value=0x45f)]
        PropertiesId_ExtraEnergyPackageRoundCDMulti_ED = 0x45f,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageRoundCD_NS", Value=0x460)]
        PropertiesId_ExtraEnergyPackageRoundCD_NS = 0x460,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageRoundCD_Final", Value=0x462)]
        PropertiesId_ExtraEnergyPackageRoundCD_Final = 0x462,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostEnergy_Base", Value=0x463)]
        PropertiesId_ExtraEnergyPackageCostEnergy_Base = 0x463,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostEnergyMulti_ES", Value=0x464)]
        PropertiesId_ExtraEnergyPackageCostEnergyMulti_ES = 0x464,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostEnergy_NS", Value=0x466)]
        PropertiesId_ExtraEnergyPackageCostEnergy_NS = 0x466,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostEnergy_ND", Value=0x467)]
        PropertiesId_ExtraEnergyPackageCostEnergy_ND = 0x467,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostEnergy_Final", Value=0x468)]
        PropertiesId_ExtraEnergyPackageCostEnergy_Final = 0x468,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostCPU_Base", Value=0x469)]
        PropertiesId_ExtraEnergyPackageCostCPU_Base = 0x469,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostCPU_Final", Value=0x46c)]
        PropertiesId_ExtraEnergyPackageCostCPU_Final = 0x46c,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostPower_Base", Value=0x46d)]
        PropertiesId_ExtraEnergyPackageCostPower_Base = 0x46d,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageCostPower_Final", Value=0x470)]
        PropertiesId_ExtraEnergyPackageCostPower_Final = 0x470,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageOnceRepairAmount_Base", Value=0x471)]
        PropertiesId_ExtraEnergyPackageOnceRepairAmount_Base = 0x471,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageOnceRepairAmountMulti_ES", Value=0x472)]
        PropertiesId_ExtraEnergyPackageOnceRepairAmountMulti_ES = 0x472,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageOnceRepairAmount_ND", Value=0x474)]
        PropertiesId_ExtraEnergyPackageOnceRepairAmount_ND = 0x474,
        [ProtoEnum(Name="PropertiesId_ExtraEnergyPackageOnceRepairAmount_Final", Value=0x475)]
        PropertiesId_ExtraEnergyPackageOnceRepairAmount_Final = 0x475,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCD_Base", Value=0x5a1)]
        PropertiesId_RemoteEnergyRepairerRoundCD_Base = 0x5a1,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCD_NS", Value=0x4e4)]
        PropertiesId_RemoteEnergyRepairerRoundCD_NS = 0x4e4,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCDMulti_ES", Value=0x5a2)]
        PropertiesId_RemoteEnergyRepairerRoundCDMulti_ES = 0x5a2,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCDMulti_ED", Value=0x5a3)]
        PropertiesId_RemoteEnergyRepairerRoundCDMulti_ED = 0x5a3,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCD_ND", Value=0x5a5)]
        PropertiesId_RemoteEnergyRepairerRoundCD_ND = 0x5a5,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerRoundCD_Final", Value=0x5a6)]
        PropertiesId_RemoteEnergyRepairerRoundCD_Final = 0x5a6,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostEnergy_Base", Value=0x5a7)]
        PropertiesId_RemoteEnergyRepairerCostEnergy_Base = 0x5a7,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostEnergyMulti_ES", Value=0x5a8)]
        PropertiesId_RemoteEnergyRepairerCostEnergyMulti_ES = 0x5a8,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostEnergy_NS", Value=0x5aa)]
        PropertiesId_RemoteEnergyRepairerCostEnergy_NS = 0x5aa,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostEnergy_ND", Value=0x5ab)]
        PropertiesId_RemoteEnergyRepairerCostEnergy_ND = 0x5ab,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostEnergy_Final", Value=0x5ac)]
        PropertiesId_RemoteEnergyRepairerCostEnergy_Final = 0x5ac,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostCPU_Base", Value=0x5ad)]
        PropertiesId_RemoteEnergyRepairerCostCPU_Base = 0x5ad,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostCPU_Final", Value=0x5b0)]
        PropertiesId_RemoteEnergyRepairerCostCPU_Final = 0x5b0,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostPower_Base", Value=0x5b1)]
        PropertiesId_RemoteEnergyRepairerCostPower_Base = 0x5b1,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostPower_NS", Value=0x5b3)]
        PropertiesId_RemoteEnergyRepairerCostPower_NS = 0x5b3,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerCostPower_Final", Value=0x5b4)]
        PropertiesId_RemoteEnergyRepairerCostPower_Final = 0x5b4,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerFireRange_Base", Value=0x5b5)]
        PropertiesId_RemoteEnergyRepairerFireRange_Base = 0x5b5,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerFireRangeMulti_ES", Value=0x640)]
        PropertiesId_RemoteEnergyRepairerFireRangeMulti_ES = 0x640,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerFireRange_Final", Value=0x5ba)]
        PropertiesId_RemoteEnergyRepairerFireRange_Final = 0x5ba,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerOnceRepairAmount_Base", Value=0x5bb)]
        PropertiesId_RemoteEnergyRepairerOnceRepairAmount_Base = 0x5bb,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerOnceRepairAmountMulti_ES", Value=0x5bc)]
        PropertiesId_RemoteEnergyRepairerOnceRepairAmountMulti_ES = 0x5bc,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerOnceRepairAmount_ND", Value=0x519)]
        PropertiesId_RemoteEnergyRepairerOnceRepairAmount_ND = 0x519,
        [ProtoEnum(Name="PropertiesId_RemoteEnergyRepairerOnceRepairAmount_Final", Value=0x5bf)]
        PropertiesId_RemoteEnergyRepairerOnceRepairAmount_Final = 0x5bf,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerRoundCD_Base", Value=0x24f)]
        PropertiesId_ShieldRepairerRoundCD_Base = 0x24f,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerRoundCDMulti_ES", Value=0x250)]
        PropertiesId_ShieldRepairerRoundCDMulti_ES = 0x250,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerRoundCDMulti_ED", Value=0x251)]
        PropertiesId_ShieldRepairerRoundCDMulti_ED = 0x251,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerRoundCD_NS", Value=0x252)]
        PropertiesId_ShieldRepairerRoundCD_NS = 0x252,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerRoundCD_Final", Value=0x254)]
        PropertiesId_ShieldRepairerRoundCD_Final = 0x254,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostEnergy_Base", Value=0x255)]
        PropertiesId_ShieldRepairerCostEnergy_Base = 0x255,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostEnergyMulti_ES", Value=0x256)]
        PropertiesId_ShieldRepairerCostEnergyMulti_ES = 0x256,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostEnergy_NS", Value=600)]
        PropertiesId_ShieldRepairerCostEnergy_NS = 600,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostEnergy_ND", Value=0x259)]
        PropertiesId_ShieldRepairerCostEnergy_ND = 0x259,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostEnergy_Final", Value=0x25a)]
        PropertiesId_ShieldRepairerCostEnergy_Final = 0x25a,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostCPU_Base", Value=0x25b)]
        PropertiesId_ShieldRepairerCostCPU_Base = 0x25b,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostCPU_Final", Value=0x25e)]
        PropertiesId_ShieldRepairerCostCPU_Final = 0x25e,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostPower_Base", Value=0x25f)]
        PropertiesId_ShieldRepairerCostPower_Base = 0x25f,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerCostPower_Final", Value=610)]
        PropertiesId_ShieldRepairerCostPower_Final = 610,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmount_Base", Value=0x263)]
        PropertiesId_ShieldRepairerOnceRepairAmount_Base = 0x263,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmountMulti_ES", Value=0x264)]
        PropertiesId_ShieldRepairerOnceRepairAmountMulti_ES = 0x264,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmountMulti_ED", Value=0x6ab)]
        PropertiesId_ShieldRepairerOnceRepairAmountMulti_ED = 0x6ab,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmount_NS", Value=0x265)]
        PropertiesId_ShieldRepairerOnceRepairAmount_NS = 0x265,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmount_ND", Value=0x266)]
        PropertiesId_ShieldRepairerOnceRepairAmount_ND = 0x266,
        [ProtoEnum(Name="PropertiesId_ShieldRepairerOnceRepairAmount_Final", Value=0x267)]
        PropertiesId_ShieldRepairerOnceRepairAmount_Final = 0x267,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerRoundCD_Base", Value=0x476)]
        PropertiesId_RemoteShieldRepairerRoundCD_Base = 0x476,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerRoundCDMulti_ES", Value=0x477)]
        PropertiesId_RemoteShieldRepairerRoundCDMulti_ES = 0x477,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerRoundCDMulti_ED", Value=0x478)]
        PropertiesId_RemoteShieldRepairerRoundCDMulti_ED = 0x478,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerRoundCD_NS", Value=0x479)]
        PropertiesId_RemoteShieldRepairerRoundCD_NS = 0x479,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerRoundCD_Final", Value=0x47b)]
        PropertiesId_RemoteShieldRepairerRoundCD_Final = 0x47b,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostEnergy_Base", Value=0x47c)]
        PropertiesId_RemoteShieldRepairerCostEnergy_Base = 0x47c,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostEnergyMulti_ES", Value=0x47d)]
        PropertiesId_RemoteShieldRepairerCostEnergyMulti_ES = 0x47d,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostEnergy_NS", Value=0x47f)]
        PropertiesId_RemoteShieldRepairerCostEnergy_NS = 0x47f,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostEnergy_ND", Value=0x480)]
        PropertiesId_RemoteShieldRepairerCostEnergy_ND = 0x480,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostEnergy_Final", Value=0x481)]
        PropertiesId_RemoteShieldRepairerCostEnergy_Final = 0x481,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostCPU_Base", Value=0x482)]
        PropertiesId_RemoteShieldRepairerCostCPU_Base = 0x482,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostCPU_Final", Value=0x485)]
        PropertiesId_RemoteShieldRepairerCostCPU_Final = 0x485,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostPower_Base", Value=0x486)]
        PropertiesId_RemoteShieldRepairerCostPower_Base = 0x486,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostPower_NS", Value=0x488)]
        PropertiesId_RemoteShieldRepairerCostPower_NS = 0x488,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerCostPower_Final", Value=0x489)]
        PropertiesId_RemoteShieldRepairerCostPower_Final = 0x489,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerFireRange_Base", Value=0x48a)]
        PropertiesId_RemoteShieldRepairerFireRange_Base = 0x48a,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerFireRangeAdd_ES", Value=0x48b)]
        PropertiesId_RemoteShieldRepairerFireRangeAdd_ES = 0x48b,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerFireRangeMulti_ES", Value=0x63e)]
        PropertiesId_RemoteShieldRepairerFireRangeMulti_ES = 0x63e,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerFireRange_Final", Value=0x48f)]
        PropertiesId_RemoteShieldRepairerFireRange_Final = 0x48f,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerOnceRepairAmount_Base", Value=0x490)]
        PropertiesId_RemoteShieldRepairerOnceRepairAmount_Base = 0x490,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerOnceRepairAmountMulti_ES", Value=0x491)]
        PropertiesId_RemoteShieldRepairerOnceRepairAmountMulti_ES = 0x491,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerOnceRepairAmountMulti_ED", Value=0x6ad)]
        PropertiesId_RemoteShieldRepairerOnceRepairAmountMulti_ED = 0x6ad,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerOnceRepairAmount_ND", Value=0x493)]
        PropertiesId_RemoteShieldRepairerOnceRepairAmount_ND = 0x493,
        [ProtoEnum(Name="PropertiesId_RemoteShieldRepairerOnceRepairAmount_Final", Value=0x494)]
        PropertiesId_RemoteShieldRepairerOnceRepairAmount_Final = 0x494,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorRoundCD_Base", Value=0x716)]
        PropertiesId_ShieldCreatorRoundCD_Base = 0x716,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorRoundCDMulti_ES", Value=0x717)]
        PropertiesId_ShieldCreatorRoundCDMulti_ES = 0x717,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorRoundCDMulti_ED", Value=0x718)]
        PropertiesId_ShieldCreatorRoundCDMulti_ED = 0x718,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorRoundCD_NS", Value=0x4e5)]
        PropertiesId_ShieldCreatorRoundCD_NS = 0x4e5,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorRoundCD_Final", Value=0x71b)]
        PropertiesId_ShieldCreatorRoundCD_Final = 0x71b,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostEnergy_Base", Value=0x71c)]
        PropertiesId_ShieldCreatorCostEnergy_Base = 0x71c,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostEnergyMulti_ES", Value=0x71d)]
        PropertiesId_ShieldCreatorCostEnergyMulti_ES = 0x71d,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostEnergy_Final", Value=0x721)]
        PropertiesId_ShieldCreatorCostEnergy_Final = 0x721,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostCPU_Base", Value=0x722)]
        PropertiesId_ShieldCreatorCostCPU_Base = 0x722,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostCPU_Final", Value=0x725)]
        PropertiesId_ShieldCreatorCostCPU_Final = 0x725,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostPower_Base", Value=0x726)]
        PropertiesId_ShieldCreatorCostPower_Base = 0x726,
        [ProtoEnum(Name="PropertiesId_ShieldCreatorCostPower_Final", Value=0x729)]
        PropertiesId_ShieldCreatorCostPower_Final = 0x729,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCD_Base", Value=0x5c0)]
        PropertiesId_InvisibilityRoundCD_Base = 0x5c0,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCDMulti_ES", Value=0x5c1)]
        PropertiesId_InvisibilityRoundCDMulti_ES = 0x5c1,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCDMulti_ED", Value=0x5c2)]
        PropertiesId_InvisibilityRoundCDMulti_ED = 0x5c2,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCD_Final", Value=0x5c5)]
        PropertiesId_InvisibilityRoundCD_Final = 0x5c5,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostEnergy_Base", Value=0x5c6)]
        PropertiesId_InvisibilityCostEnergy_Base = 0x5c6,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostEnergyMulti_ES", Value=0x5c7)]
        PropertiesId_InvisibilityCostEnergyMulti_ES = 0x5c7,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostEnergy_NS", Value=0x5c9)]
        PropertiesId_InvisibilityCostEnergy_NS = 0x5c9,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostEnergy_ND", Value=0x5ca)]
        PropertiesId_InvisibilityCostEnergy_ND = 0x5ca,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostEnergy_Final", Value=0x5cb)]
        PropertiesId_InvisibilityCostEnergy_Final = 0x5cb,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCostEnergy_Base", Value=0x5da)]
        PropertiesId_InvisibilityRoundCostEnergy_Base = 0x5da,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCostEnergy_NS", Value=0x5dd)]
        PropertiesId_InvisibilityRoundCostEnergy_NS = 0x5dd,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCostEnergy_ND", Value=0x5de)]
        PropertiesId_InvisibilityRoundCostEnergy_ND = 0x5de,
        [ProtoEnum(Name="PropertiesId_InvisibilityRoundCostEnergy_Final", Value=0x5df)]
        PropertiesId_InvisibilityRoundCostEnergy_Final = 0x5df,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostCPU_Base", Value=0x5cc)]
        PropertiesId_InvisibilityCostCPU_Base = 0x5cc,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostCPU_Final", Value=0x5cf)]
        PropertiesId_InvisibilityCostCPU_Final = 0x5cf,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostPower_Base", Value=0x5d0)]
        PropertiesId_InvisibilityCostPower_Base = 0x5d0,
        [ProtoEnum(Name="PropertiesId_InvisibilityCostPower_Final", Value=0x5d3)]
        PropertiesId_InvisibilityCostPower_Final = 0x5d3,
        [ProtoEnum(Name="PropertiesId_InvisibilityDelay_Base", Value=0x5d4)]
        PropertiesId_InvisibilityDelay_Base = 0x5d4,
        [ProtoEnum(Name="PropertiesId_InvisibilityDelay_NS", Value=0x5d7)]
        PropertiesId_InvisibilityDelay_NS = 0x5d7,
        [ProtoEnum(Name="PropertiesId_InvisibilityDelay_Final", Value=0x5d9)]
        PropertiesId_InvisibilityDelay_Final = 0x5d9,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstRoundCD_Base", Value=0x1f)]
        PropertiesId_SelfAtkAsstRoundCD_Base = 0x1f,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstRoundCD_NS", Value=0x20)]
        PropertiesId_SelfAtkAsstRoundCD_NS = 0x20,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstRoundCD_Final", Value=0x359)]
        PropertiesId_SelfAtkAsstRoundCD_Final = 0x359,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstPercentageCostEnergy_Base", Value=0x4f)]
        PropertiesId_SelfAtkAsstPercentageCostEnergy_Base = 0x4f,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstPercentageCostEnergy_NS", Value=80)]
        PropertiesId_SelfAtkAsstPercentageCostEnergy_NS = 80,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstPercentageCostEnergy_Final", Value=860)]
        PropertiesId_SelfAtkAsstPercentageCostEnergy_Final = 860,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstCostCPU_Base", Value=0x35d)]
        PropertiesId_SelfAtkAsstCostCPU_Base = 0x35d,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstCostCPU_Final", Value=0x360)]
        PropertiesId_SelfAtkAsstCostCPU_Final = 0x360,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstCostPower_Base", Value=0x361)]
        PropertiesId_SelfAtkAsstCostPower_Base = 0x361,
        [ProtoEnum(Name="PropertiesId_SelfAtkAsstCostPower_Final", Value=0x362)]
        PropertiesId_SelfAtkAsstCostPower_Final = 0x362,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstRoundCD_Base", Value=160)]
        PropertiesId_SelfAccuracyAsstRoundCD_Base = 160,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstRoundCD_NS", Value=0xa1)]
        PropertiesId_SelfAccuracyAsstRoundCD_NS = 0xa1,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstRoundCD_Final", Value=0x39f)]
        PropertiesId_SelfAccuracyAsstRoundCD_Final = 0x39f,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstPercentageCostEnergy_Base", Value=0xa2)]
        PropertiesId_SelfAccuracyAsstPercentageCostEnergy_Base = 0xa2,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstPercentageCostEnergy_NS", Value=0xa3)]
        PropertiesId_SelfAccuracyAsstPercentageCostEnergy_NS = 0xa3,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstPercentageCostEnergy_Final", Value=0x3a3)]
        PropertiesId_SelfAccuracyAsstPercentageCostEnergy_Final = 0x3a3,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstCostCPU_Base", Value=0x3a4)]
        PropertiesId_SelfAccuracyAsstCostCPU_Base = 0x3a4,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstCostCPU_Final", Value=0x3a5)]
        PropertiesId_SelfAccuracyAsstCostCPU_Final = 0x3a5,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstCostPower_Base", Value=0x3a9)]
        PropertiesId_SelfAccuracyAsstCostPower_Base = 0x3a9,
        [ProtoEnum(Name="PropertiesId_SelfAccuracyAsstCostPower_Final", Value=0x3ae)]
        PropertiesId_SelfAccuracyAsstCostPower_Final = 0x3ae,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstRoundCD_Base", Value=0x7d)]
        PropertiesId_SelfCritAsstRoundCD_Base = 0x7d,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstRoundCD_NS", Value=0x7e)]
        PropertiesId_SelfCritAsstRoundCD_NS = 0x7e,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstRoundCD_Final", Value=0x380)]
        PropertiesId_SelfCritAsstRoundCD_Final = 0x380,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstPercentageCostEnergy_Base", Value=0x7f)]
        PropertiesId_SelfCritAsstPercentageCostEnergy_Base = 0x7f,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstPercentageCostEnergy_NS", Value=0x80)]
        PropertiesId_SelfCritAsstPercentageCostEnergy_NS = 0x80,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstPercentageCostEnergy_Final", Value=0x381)]
        PropertiesId_SelfCritAsstPercentageCostEnergy_Final = 0x381,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstCostCPU_Base", Value=900)]
        PropertiesId_SelfCritAsstCostCPU_Base = 900,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstCostCPU_Final", Value=0x385)]
        PropertiesId_SelfCritAsstCostCPU_Final = 0x385,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstCostPower_Base", Value=0x38a)]
        PropertiesId_SelfCritAsstCostPower_Base = 0x38a,
        [ProtoEnum(Name="PropertiesId_SelfCritAsstCostPower_Final", Value=0x38b)]
        PropertiesId_SelfCritAsstCostPower_Final = 0x38b,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstRoundCD_Base", Value=0x52)]
        PropertiesId_SelfRangeAsstRoundCD_Base = 0x52,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstRoundCD_NS", Value=0x53)]
        PropertiesId_SelfRangeAsstRoundCD_NS = 0x53,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstRoundCD_Final", Value=0x363)]
        PropertiesId_SelfRangeAsstRoundCD_Final = 0x363,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstPercentageCostEnergy_Base", Value=0x54)]
        PropertiesId_SelfRangeAsstPercentageCostEnergy_Base = 0x54,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstPercentageCostEnergy_NS", Value=0x59)]
        PropertiesId_SelfRangeAsstPercentageCostEnergy_NS = 0x59,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstPercentageCostEnergy_Final", Value=870)]
        PropertiesId_SelfRangeAsstPercentageCostEnergy_Final = 870,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstCostCPU_Base", Value=0x367)]
        PropertiesId_SelfRangeAsstCostCPU_Base = 0x367,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstCostCPU_Final", Value=0x36c)]
        PropertiesId_SelfRangeAsstCostCPU_Final = 0x36c,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstCostPower_Base", Value=0x36d)]
        PropertiesId_SelfRangeAsstCostPower_Base = 0x36d,
        [ProtoEnum(Name="PropertiesId_SelfRangeAsstCostPower_Final", Value=880)]
        PropertiesId_SelfRangeAsstCostPower_Final = 880,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstRoundCD_Base", Value=170)]
        PropertiesId_SelfDefAsstRoundCD_Base = 170,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstRoundCD_NS", Value=0xb2)]
        PropertiesId_SelfDefAsstRoundCD_NS = 0xb2,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstRoundCD_Final", Value=0x3bd)]
        PropertiesId_SelfDefAsstRoundCD_Final = 0x3bd,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstPercentageCostEnergy_Base", Value=0xb3)]
        PropertiesId_SelfDefAsstPercentageCostEnergy_Base = 0xb3,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstPercentageCostEnergy_NS", Value=0xd3)]
        PropertiesId_SelfDefAsstPercentageCostEnergy_NS = 0xd3,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstPercentageCostEnergy_Final", Value=0x3be)]
        PropertiesId_SelfDefAsstPercentageCostEnergy_Final = 0x3be,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstCostCPU_Base", Value=0x3bf)]
        PropertiesId_SelfDefAsstCostCPU_Base = 0x3bf,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstCostCPU_Final", Value=0x3c3)]
        PropertiesId_SelfDefAsstCostCPU_Final = 0x3c3,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstCostPower_Base", Value=0x3c8)]
        PropertiesId_SelfDefAsstCostPower_Base = 0x3c8,
        [ProtoEnum(Name="PropertiesId_SelfDefAsstCostPower_Final", Value=0x3c9)]
        PropertiesId_SelfDefAsstCostPower_Final = 0x3c9,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleRoundCD_Base", Value=0x5e0)]
        PropertiesId_SelfWeaponModuleRoundCD_Base = 0x5e0,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleRoundCDMulti_ES", Value=0x5e1)]
        PropertiesId_SelfWeaponModuleRoundCDMulti_ES = 0x5e1,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleRoundCD_Final", Value=0x5e5)]
        PropertiesId_SelfWeaponModuleRoundCD_Final = 0x5e5,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostEnergy_Base", Value=0x5e6)]
        PropertiesId_SelfWeaponModuleCostEnergy_Base = 0x5e6,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostEnergyMulti_ES", Value=0x5e7)]
        PropertiesId_SelfWeaponModuleCostEnergyMulti_ES = 0x5e7,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostEnergy_NS", Value=0x5e9)]
        PropertiesId_SelfWeaponModuleCostEnergy_NS = 0x5e9,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostEnergy_ND", Value=0x5ea)]
        PropertiesId_SelfWeaponModuleCostEnergy_ND = 0x5ea,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostEnergy_Final", Value=0x5eb)]
        PropertiesId_SelfWeaponModuleCostEnergy_Final = 0x5eb,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModulePercentageCostEnergy_Base", Value=0x64e)]
        PropertiesId_SelfWeaponModulePercentageCostEnergy_Base = 0x64e,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModulePercentageCostEnergy_ND", Value=0x652)]
        PropertiesId_SelfWeaponModulePercentageCostEnergy_ND = 0x652,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModulePercentageCostEnergy_Final", Value=0x653)]
        PropertiesId_SelfWeaponModulePercentageCostEnergy_Final = 0x653,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostCPU_Base", Value=0x5ec)]
        PropertiesId_SelfWeaponModuleCostCPU_Base = 0x5ec,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostCPU_Final", Value=0x5ef)]
        PropertiesId_SelfWeaponModuleCostCPU_Final = 0x5ef,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostPower_Base", Value=0x5f0)]
        PropertiesId_SelfWeaponModuleCostPower_Base = 0x5f0,
        [ProtoEnum(Name="PropertiesId_SelfWeaponModuleCostPower_Final", Value=0x5f3)]
        PropertiesId_SelfWeaponModuleCostPower_Final = 0x5f3,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleRoundCD_Base", Value=0x5f4)]
        PropertiesId_SelfRangeModuleRoundCD_Base = 0x5f4,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleRoundCDMulti_ES", Value=0x5f5)]
        PropertiesId_SelfRangeModuleRoundCDMulti_ES = 0x5f5,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleRoundCD_Final", Value=0x5f9)]
        PropertiesId_SelfRangeModuleRoundCD_Final = 0x5f9,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostEnergy_Base", Value=0x5fa)]
        PropertiesId_SelfRangeModuleCostEnergy_Base = 0x5fa,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostEnergy_NS", Value=0x5fd)]
        PropertiesId_SelfRangeModuleCostEnergy_NS = 0x5fd,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostEnergy_ND", Value=0x5fe)]
        PropertiesId_SelfRangeModuleCostEnergy_ND = 0x5fe,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostEnergy_Final", Value=0x5ff)]
        PropertiesId_SelfRangeModuleCostEnergy_Final = 0x5ff,
        [ProtoEnum(Name="PropertiesId_SelfRangeModulePercentageCostEnergy_Base", Value=0x654)]
        PropertiesId_SelfRangeModulePercentageCostEnergy_Base = 0x654,
        [ProtoEnum(Name="PropertiesId_SelfRangeModulePercentageCostEnergy_ND", Value=0x658)]
        PropertiesId_SelfRangeModulePercentageCostEnergy_ND = 0x658,
        [ProtoEnum(Name="PropertiesId_SelfRangeModulePercentageCostEnergy_Final", Value=0x659)]
        PropertiesId_SelfRangeModulePercentageCostEnergy_Final = 0x659,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostCPU_Base", Value=0x600)]
        PropertiesId_SelfRangeModuleCostCPU_Base = 0x600,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostCPU_Final", Value=0x603)]
        PropertiesId_SelfRangeModuleCostCPU_Final = 0x603,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostPower_Base", Value=0x604)]
        PropertiesId_SelfRangeModuleCostPower_Base = 0x604,
        [ProtoEnum(Name="PropertiesId_SelfRangeModuleCostPower_Final", Value=0x607)]
        PropertiesId_SelfRangeModuleCostPower_Final = 0x607,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleRoundCD_Base", Value=0x608)]
        PropertiesId_SelfDefModuleRoundCD_Base = 0x608,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleRoundCDMulti_ES", Value=0x609)]
        PropertiesId_SelfDefModuleRoundCDMulti_ES = 0x609,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleRoundCD_Final", Value=0x60d)]
        PropertiesId_SelfDefModuleRoundCD_Final = 0x60d,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostEnergy_Base", Value=0x60e)]
        PropertiesId_SelfDefModuleCostEnergy_Base = 0x60e,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostEnergyMulti_ES", Value=0x60f)]
        PropertiesId_SelfDefModuleCostEnergyMulti_ES = 0x60f,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostEnergy_NS", Value=0x611)]
        PropertiesId_SelfDefModuleCostEnergy_NS = 0x611,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostEnergy_ND", Value=0x612)]
        PropertiesId_SelfDefModuleCostEnergy_ND = 0x612,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostEnergy_Final", Value=0x613)]
        PropertiesId_SelfDefModuleCostEnergy_Final = 0x613,
        [ProtoEnum(Name="PropertiesId_SelfDefModulePercentageCostEnergy_Base", Value=0x65a)]
        PropertiesId_SelfDefModulePercentageCostEnergy_Base = 0x65a,
        [ProtoEnum(Name="PropertiesId_SelfDefModulePercentageCostEnergy_ND", Value=0x65e)]
        PropertiesId_SelfDefModulePercentageCostEnergy_ND = 0x65e,
        [ProtoEnum(Name="PropertiesId_SelfDefModulePercentageCostEnergy_Final", Value=0x65f)]
        PropertiesId_SelfDefModulePercentageCostEnergy_Final = 0x65f,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostCPU_Base", Value=0x614)]
        PropertiesId_SelfDefModuleCostCPU_Base = 0x614,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostCPU_Final", Value=0x617)]
        PropertiesId_SelfDefModuleCostCPU_Final = 0x617,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostPower_Base", Value=0x618)]
        PropertiesId_SelfDefModuleCostPower_Base = 0x618,
        [ProtoEnum(Name="PropertiesId_SelfDefModuleCostPower_Final", Value=0x61b)]
        PropertiesId_SelfDefModuleCostPower_Final = 0x61b,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstRoundCD_Base", Value=0xd7)]
        PropertiesId_RemoteDefAsstRoundCD_Base = 0xd7,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstRoundCD_NS", Value=0xe2)]
        PropertiesId_RemoteDefAsstRoundCD_NS = 0xe2,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstRoundCD_Final", Value=0x3cc)]
        PropertiesId_RemoteDefAsstRoundCD_Final = 0x3cc,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstPercentageCostEnergy_Base", Value=0xe4)]
        PropertiesId_RemoteDefAsstPercentageCostEnergy_Base = 0xe4,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstPercentageCostEnergy_NS", Value=0xe9)]
        PropertiesId_RemoteDefAsstPercentageCostEnergy_NS = 0xe9,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstPercentageCostEnergy_Final", Value=0x3cd)]
        PropertiesId_RemoteDefAsstPercentageCostEnergy_Final = 0x3cd,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstFireRange_Base", Value=0xf8)]
        PropertiesId_RemoteDefAsstFireRange_Base = 0xf8,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstFireRangeMulti_ES", Value=0xf9)]
        PropertiesId_RemoteDefAsstFireRangeMulti_ES = 0xf9,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstFireRange_Final", Value=0x3d0)]
        PropertiesId_RemoteDefAsstFireRange_Final = 0x3d0,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstCostCPU_Base", Value=0x3d1)]
        PropertiesId_RemoteDefAsstCostCPU_Base = 0x3d1,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstCostCPU_Final", Value=0x3d2)]
        PropertiesId_RemoteDefAsstCostCPU_Final = 0x3d2,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstCostPower_Base", Value=0x3d3)]
        PropertiesId_RemoteDefAsstCostPower_Base = 0x3d3,
        [ProtoEnum(Name="PropertiesId_RemoteDefAsstCostPower_Final", Value=0x3d7)]
        PropertiesId_RemoteDefAsstCostPower_Final = 0x3d7,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstRoundCD_Base", Value=0xfd)]
        PropertiesId_RemoteMovAsstRoundCD_Base = 0xfd,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstRoundCD_NS", Value=0xff)]
        PropertiesId_RemoteMovAsstRoundCD_NS = 0xff,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstRoundCD_Final", Value=0x3d8)]
        PropertiesId_RemoteMovAsstRoundCD_Final = 0x3d8,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstPercentageCostEnergy_Base", Value=0x100)]
        PropertiesId_RemoteMovAsstPercentageCostEnergy_Base = 0x100,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstPercentageCostEnergy_NS", Value=0x101)]
        PropertiesId_RemoteMovAsstPercentageCostEnergy_NS = 0x101,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstPercentageCostEnergy_Final", Value=0x3d9)]
        PropertiesId_RemoteMovAsstPercentageCostEnergy_Final = 0x3d9,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstFireRange_Base", Value=0x121)]
        PropertiesId_RemoteMovAsstFireRange_Base = 0x121,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstFireRangeMulti_ES", Value=0x137)]
        PropertiesId_RemoteMovAsstFireRangeMulti_ES = 0x137,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstFireRange_Final", Value=0x3dc)]
        PropertiesId_RemoteMovAsstFireRange_Final = 0x3dc,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstCostCPU_Base", Value=0x3dd)]
        PropertiesId_RemoteMovAsstCostCPU_Base = 0x3dd,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstCostCPU_Final", Value=0x3e2)]
        PropertiesId_RemoteMovAsstCostCPU_Final = 0x3e2,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstCostPower_Base", Value=0x3e3)]
        PropertiesId_RemoteMovAsstCostPower_Base = 0x3e3,
        [ProtoEnum(Name="PropertiesId_RemoteMovAsstCostPower_Final", Value=0x3e6)]
        PropertiesId_RemoteMovAsstCostPower_Final = 0x3e6,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstRoundCD_Base", Value=0x4b5)]
        PropertiesId_TeamAtkAsstRoundCD_Base = 0x4b5,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstRoundCD_NS", Value=0x51)]
        PropertiesId_TeamAtkAsstRoundCD_NS = 0x51,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstRoundCDMulti_ES", Value=0x4b6)]
        PropertiesId_TeamAtkAsstRoundCDMulti_ES = 0x4b6,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstRoundCDMulti_ED", Value=0x4b7)]
        PropertiesId_TeamAtkAsstRoundCDMulti_ED = 0x4b7,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstRoundCD_Final", Value=0x4ba)]
        PropertiesId_TeamAtkAsstRoundCD_Final = 0x4ba,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostEnergy_Base", Value=0x4bb)]
        PropertiesId_TeamAtkAsstCostEnergy_Base = 0x4bb,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostEnergyMulti_ES", Value=0x4bc)]
        PropertiesId_TeamAtkAsstCostEnergyMulti_ES = 0x4bc,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostEnergy_NS", Value=0x4be)]
        PropertiesId_TeamAtkAsstCostEnergy_NS = 0x4be,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostEnergy_ND", Value=0x4bf)]
        PropertiesId_TeamAtkAsstCostEnergy_ND = 0x4bf,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostEnergy_Final", Value=0x4c0)]
        PropertiesId_TeamAtkAsstCostEnergy_Final = 0x4c0,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstPercentageCostEnergy_Base", Value=0x6e2)]
        PropertiesId_TeamAtkAsstPercentageCostEnergy_Base = 0x6e2,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstPercentageCostEnergyMulti_ES", Value=0x6e3)]
        PropertiesId_TeamAtkAsstPercentageCostEnergyMulti_ES = 0x6e3,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstPercentageCostEnergy_NS", Value=0x6e5)]
        PropertiesId_TeamAtkAsstPercentageCostEnergy_NS = 0x6e5,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstPercentageCostEnergy_Final", Value=0x6e7)]
        PropertiesId_TeamAtkAsstPercentageCostEnergy_Final = 0x6e7,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostCPU_Base", Value=0x4c1)]
        PropertiesId_TeamAtkAsstCostCPU_Base = 0x4c1,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostCPU_Final", Value=0x4c4)]
        PropertiesId_TeamAtkAsstCostCPU_Final = 0x4c4,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostPower_Base", Value=0x4c5)]
        PropertiesId_TeamAtkAsstCostPower_Base = 0x4c5,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstCostPower_Final", Value=0x4c8)]
        PropertiesId_TeamAtkAsstCostPower_Final = 0x4c8,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstFireRange_Base", Value=0x4c9)]
        PropertiesId_TeamAtkAsstFireRange_Base = 0x4c9,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstFireRangeMulti_ES", Value=0x642)]
        PropertiesId_TeamAtkAsstFireRangeMulti_ES = 0x642,
        [ProtoEnum(Name="PropertiesId_TeamAtkAsstFireRange_Final", Value=0x4ce)]
        PropertiesId_TeamAtkAsstFireRange_Final = 0x4ce,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstRoundCD_Base", Value=0xa4)]
        PropertiesId_TeamAccuracyAsstRoundCD_Base = 0xa4,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstRoundCD_NS", Value=0xa5)]
        PropertiesId_TeamAccuracyAsstRoundCD_NS = 0xa5,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstRoundCD_Final", Value=0x3af)]
        PropertiesId_TeamAccuracyAsstRoundCD_Final = 0x3af,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstPercentageCostEnergy_Base", Value=0xa6)]
        PropertiesId_TeamAccuracyAsstPercentageCostEnergy_Base = 0xa6,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstPercentageCostEnergy_NS", Value=0xa7)]
        PropertiesId_TeamAccuracyAsstPercentageCostEnergy_NS = 0xa7,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstPercentageCostEnergy_Final", Value=0x3b2)]
        PropertiesId_TeamAccuracyAsstPercentageCostEnergy_Final = 0x3b2,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstFireRange_Base", Value=0xa8)]
        PropertiesId_TeamAccuracyAsstFireRange_Base = 0xa8,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstFireRangeMulti_ES", Value=0xa9)]
        PropertiesId_TeamAccuracyAsstFireRangeMulti_ES = 0xa9,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstFireRange_Final", Value=0x3b3)]
        PropertiesId_TeamAccuracyAsstFireRange_Final = 0x3b3,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstCostCPU_Base", Value=950)]
        PropertiesId_TeamAccuracyAsstCostCPU_Base = 950,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstCostCPU_Final", Value=0x3b7)]
        PropertiesId_TeamAccuracyAsstCostCPU_Final = 0x3b7,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstCostPower_Base", Value=0x3b8)]
        PropertiesId_TeamAccuracyAsstCostPower_Base = 0x3b8,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyAsstCostPower_Final", Value=0x3b9)]
        PropertiesId_TeamAccuracyAsstCostPower_Final = 0x3b9,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstRoundCD_Base", Value=0x6a)]
        PropertiesId_TeamRangeAsstRoundCD_Base = 0x6a,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstRoundCD_NS", Value=0x6b)]
        PropertiesId_TeamRangeAsstRoundCD_NS = 0x6b,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstRoundCD_Final", Value=0x371)]
        PropertiesId_TeamRangeAsstRoundCD_Final = 0x371,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstPercentageCostEnergy_Base", Value=0x6c)]
        PropertiesId_TeamRangeAsstPercentageCostEnergy_Base = 0x6c,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstPercentageCostEnergy_NS", Value=0x6d)]
        PropertiesId_TeamRangeAsstPercentageCostEnergy_NS = 0x6d,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstPercentageCostEnergy_Final", Value=0x374)]
        PropertiesId_TeamRangeAsstPercentageCostEnergy_Final = 0x374,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstFireRange_Base", Value=110)]
        PropertiesId_TeamRangeAsstFireRange_Base = 110,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstFireRangeMulti_ES", Value=0x6f)]
        PropertiesId_TeamRangeAsstFireRangeMulti_ES = 0x6f,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstFireRange_Final", Value=0x375)]
        PropertiesId_TeamRangeAsstFireRange_Final = 0x375,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstCostCPU_Base", Value=0x376)]
        PropertiesId_TeamRangeAsstCostCPU_Base = 0x376,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstCostCPU_Final", Value=0x377)]
        PropertiesId_TeamRangeAsstCostCPU_Final = 0x377,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstCostPower_Base", Value=890)]
        PropertiesId_TeamRangeAsstCostPower_Base = 890,
        [ProtoEnum(Name="PropertiesId_TeamRangeAsstCostPower_Final", Value=0x37b)]
        PropertiesId_TeamRangeAsstCostPower_Final = 0x37b,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstRoundCD_Base", Value=0x81)]
        PropertiesId_TeamCritAsstRoundCD_Base = 0x81,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstRoundCD_NS", Value=150)]
        PropertiesId_TeamCritAsstRoundCD_NS = 150,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstRoundCD_Final", Value=0x38f)]
        PropertiesId_TeamCritAsstRoundCD_Final = 0x38f,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstPercentageCostEnergy_Base", Value=0x9c)]
        PropertiesId_TeamCritAsstPercentageCostEnergy_Base = 0x9c,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstPercentageCostEnergy_NS", Value=0x9d)]
        PropertiesId_TeamCritAsstPercentageCostEnergy_NS = 0x9d,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstPercentageCostEnergy_Final", Value=0x394)]
        PropertiesId_TeamCritAsstPercentageCostEnergy_Final = 0x394,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstFireRange_Base", Value=0x9e)]
        PropertiesId_TeamCritAsstFireRange_Base = 0x9e,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstFireRangeMulti_ES", Value=0x9f)]
        PropertiesId_TeamCritAsstFireRangeMulti_ES = 0x9f,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstFireRange_Final", Value=0x395)]
        PropertiesId_TeamCritAsstFireRange_Final = 0x395,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstCostCPU_Base", Value=920)]
        PropertiesId_TeamCritAsstCostCPU_Base = 920,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstCostCPU_Final", Value=0x399)]
        PropertiesId_TeamCritAsstCostCPU_Final = 0x399,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstCostPower_Base", Value=0x39d)]
        PropertiesId_TeamCritAsstCostPower_Base = 0x39d,
        [ProtoEnum(Name="PropertiesId_TeamCritAsstCostPower_Final", Value=0x39e)]
        PropertiesId_TeamCritAsstCostPower_Final = 0x39e,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstRoundCD_Base", Value=0x4cf)]
        PropertiesId_TeamDefAsstRoundCD_Base = 0x4cf,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstRoundCDMulti_ES", Value=0x4d0)]
        PropertiesId_TeamDefAsstRoundCDMulti_ES = 0x4d0,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstRoundCDMulti_ED", Value=0x4d1)]
        PropertiesId_TeamDefAsstRoundCDMulti_ED = 0x4d1,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstRoundCD_NS", Value=0xfc)]
        PropertiesId_TeamDefAsstRoundCD_NS = 0xfc,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstRoundCD_Final", Value=0x4d4)]
        PropertiesId_TeamDefAsstRoundCD_Final = 0x4d4,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostEnergy_Base", Value=0x4d5)]
        PropertiesId_TeamDefAsstCostEnergy_Base = 0x4d5,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostEnergy_NS", Value=0x4d8)]
        PropertiesId_TeamDefAsstCostEnergy_NS = 0x4d8,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostEnergy_ND", Value=0x4d9)]
        PropertiesId_TeamDefAsstCostEnergy_ND = 0x4d9,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostEnergy_Final", Value=0x4da)]
        PropertiesId_TeamDefAsstCostEnergy_Final = 0x4da,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstPercentageCostEnergy_Base", Value=0x6e8)]
        PropertiesId_TeamDefAsstPercentageCostEnergy_Base = 0x6e8,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstPercentageCostEnergyMulti_ES", Value=0x6e9)]
        PropertiesId_TeamDefAsstPercentageCostEnergyMulti_ES = 0x6e9,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstPercentageCostEnergy_NS", Value=0x6eb)]
        PropertiesId_TeamDefAsstPercentageCostEnergy_NS = 0x6eb,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstPercentageCostEnergy_Final", Value=0x6ed)]
        PropertiesId_TeamDefAsstPercentageCostEnergy_Final = 0x6ed,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostCPU_Base", Value=0x4db)]
        PropertiesId_TeamDefAsstCostCPU_Base = 0x4db,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostCPU_Final", Value=0x4de)]
        PropertiesId_TeamDefAsstCostCPU_Final = 0x4de,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostPower_Base", Value=0x4df)]
        PropertiesId_TeamDefAsstCostPower_Base = 0x4df,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstCostPower_Final", Value=0x4e2)]
        PropertiesId_TeamDefAsstCostPower_Final = 0x4e2,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstFireRange_Base", Value=0x4e3)]
        PropertiesId_TeamDefAsstFireRange_Base = 0x4e3,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstFireRangeMulti_ES", Value=0x644)]
        PropertiesId_TeamDefAsstFireRangeMulti_ES = 0x644,
        [ProtoEnum(Name="PropertiesId_TeamDefAsstFireRange_Final", Value=0x4e8)]
        PropertiesId_TeamDefAsstFireRange_Final = 0x4e8,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstRoundCD_Base", Value=0x503)]
        PropertiesId_TeamMovAsstRoundCD_Base = 0x503,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstRoundCDMulti_ES", Value=0x504)]
        PropertiesId_TeamMovAsstRoundCDMulti_ES = 0x504,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstRoundCDMulti_ED", Value=0x505)]
        PropertiesId_TeamMovAsstRoundCDMulti_ED = 0x505,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstRoundCD_NS", Value=0x506)]
        PropertiesId_TeamMovAsstRoundCD_NS = 0x506,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstRoundCD_Final", Value=0x508)]
        PropertiesId_TeamMovAsstRoundCD_Final = 0x508,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostEnergy_Base", Value=0x509)]
        PropertiesId_TeamMovAsstCostEnergy_Base = 0x509,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostEnergy_NS", Value=0x50c)]
        PropertiesId_TeamMovAsstCostEnergy_NS = 0x50c,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostEnergy_ND", Value=0x50d)]
        PropertiesId_TeamMovAsstCostEnergy_ND = 0x50d,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostEnergy_Final", Value=0x50e)]
        PropertiesId_TeamMovAsstCostEnergy_Final = 0x50e,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstPercentageCostEnergy_Base", Value=0x6f4)]
        PropertiesId_TeamMovAsstPercentageCostEnergy_Base = 0x6f4,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstPercentageCostEnergyMulti_ES", Value=0x6f5)]
        PropertiesId_TeamMovAsstPercentageCostEnergyMulti_ES = 0x6f5,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstPercentageCostEnergy_NS", Value=0x6f7)]
        PropertiesId_TeamMovAsstPercentageCostEnergy_NS = 0x6f7,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstPercentageCostEnergy_Final", Value=0x6f9)]
        PropertiesId_TeamMovAsstPercentageCostEnergy_Final = 0x6f9,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostCPU_Base", Value=0x50f)]
        PropertiesId_TeamMovAsstCostCPU_Base = 0x50f,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostCPU_Final", Value=0x512)]
        PropertiesId_TeamMovAsstCostCPU_Final = 0x512,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostPower_Base", Value=0x513)]
        PropertiesId_TeamMovAsstCostPower_Base = 0x513,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstCostPower_Final", Value=0x516)]
        PropertiesId_TeamMovAsstCostPower_Final = 0x516,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstFireRange_Base", Value=0x517)]
        PropertiesId_TeamMovAsstFireRange_Base = 0x517,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstFireRangeMulti_ES", Value=0x648)]
        PropertiesId_TeamMovAsstFireRangeMulti_ES = 0x648,
        [ProtoEnum(Name="PropertiesId_TeamMovAsstFireRange_Final", Value=0x51c)]
        PropertiesId_TeamMovAsstFireRange_Final = 0x51c,
        [ProtoEnum(Name="PropertiesId_EnergyDecRoundCD_Base", Value=0x43e)]
        PropertiesId_EnergyDecRoundCD_Base = 0x43e,
        [ProtoEnum(Name="PropertiesId_EnergyDecRoundCDMulti_ES", Value=0x43f)]
        PropertiesId_EnergyDecRoundCDMulti_ES = 0x43f,
        [ProtoEnum(Name="PropertiesId_EnergyDecRoundCDMulti_ED", Value=0x440)]
        PropertiesId_EnergyDecRoundCDMulti_ED = 0x440,
        [ProtoEnum(Name="PropertiesId_EnergyDecRoundCD_NS", Value=0x441)]
        PropertiesId_EnergyDecRoundCD_NS = 0x441,
        [ProtoEnum(Name="PropertiesId_EnergyDecRoundCD_Final", Value=0x443)]
        PropertiesId_EnergyDecRoundCD_Final = 0x443,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostEnergy_Base", Value=0x444)]
        PropertiesId_EnergyDecCostEnergy_Base = 0x444,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostEnergyMulti_ES", Value=0x445)]
        PropertiesId_EnergyDecCostEnergyMulti_ES = 0x445,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostEnergy_NS", Value=0x447)]
        PropertiesId_EnergyDecCostEnergy_NS = 0x447,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostEnergy_ND", Value=0x448)]
        PropertiesId_EnergyDecCostEnergy_ND = 0x448,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostEnergy_Final", Value=0x449)]
        PropertiesId_EnergyDecCostEnergy_Final = 0x449,
        [ProtoEnum(Name="PropertiesId_EnergyDecPercentageCostEnergy_Base", Value=0x6dc)]
        PropertiesId_EnergyDecPercentageCostEnergy_Base = 0x6dc,
        [ProtoEnum(Name="PropertiesId_EnergyDecPercentageCostEnergyMulti_ES", Value=0x6dd)]
        PropertiesId_EnergyDecPercentageCostEnergyMulti_ES = 0x6dd,
        [ProtoEnum(Name="PropertiesId_EnergyDecPercentageCostEnergy_NS", Value=0x275)]
        PropertiesId_EnergyDecPercentageCostEnergy_NS = 0x275,
        [ProtoEnum(Name="PropertiesId_EnergyDecPercentageCostEnergy_Final", Value=0x6e1)]
        PropertiesId_EnergyDecPercentageCostEnergy_Final = 0x6e1,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostCPU_Base", Value=0x44a)]
        PropertiesId_EnergyDecCostCPU_Base = 0x44a,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostCPU_Final", Value=0x44d)]
        PropertiesId_EnergyDecCostCPU_Final = 0x44d,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostPower_Base", Value=0x44e)]
        PropertiesId_EnergyDecCostPower_Base = 0x44e,
        [ProtoEnum(Name="PropertiesId_EnergyDecCostPower_Final", Value=0x451)]
        PropertiesId_EnergyDecCostPower_Final = 0x451,
        [ProtoEnum(Name="PropertiesId_EnergyDecFireRange_Base", Value=0x452)]
        PropertiesId_EnergyDecFireRange_Base = 0x452,
        [ProtoEnum(Name="PropertiesId_EnergyDecFireRangeAdd_ES", Value=0x453)]
        PropertiesId_EnergyDecFireRangeAdd_ES = 0x453,
        [ProtoEnum(Name="PropertiesId_EnergyDecFireRangeMulti_ES", Value=0x63c)]
        PropertiesId_EnergyDecFireRangeMulti_ES = 0x63c,
        [ProtoEnum(Name="PropertiesId_EnergyDecFireRange_Final", Value=0x457)]
        PropertiesId_EnergyDecFireRange_Final = 0x457,
        [ProtoEnum(Name="PropertiesId_EnergyDecOnceDamageAmount_Base", Value=0x458)]
        PropertiesId_EnergyDecOnceDamageAmount_Base = 0x458,
        [ProtoEnum(Name="PropertiesId_EnergyDecOnceDamageAmountMulti_ES", Value=0x459)]
        PropertiesId_EnergyDecOnceDamageAmountMulti_ES = 0x459,
        [ProtoEnum(Name="PropertiesId_EnergyDecOnceDamageAmount_Final", Value=0x45c)]
        PropertiesId_EnergyDecOnceDamageAmount_Final = 0x45c,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerRoundCD_Base", Value=0x6fc)]
        PropertiesId_BlinkBlockerRoundCD_Base = 0x6fc,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerRoundCDMulti_ES", Value=0x6fd)]
        PropertiesId_BlinkBlockerRoundCDMulti_ES = 0x6fd,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerRoundCDMulti_ED", Value=0x6fe)]
        PropertiesId_BlinkBlockerRoundCDMulti_ED = 0x6fe,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerRoundCD_NS", Value=0x6ff)]
        PropertiesId_BlinkBlockerRoundCD_NS = 0x6ff,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerRoundCD_Final", Value=0x701)]
        PropertiesId_BlinkBlockerRoundCD_Final = 0x701,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerPercentageCostEnergy_Base", Value=0x710)]
        PropertiesId_BlinkBlockerPercentageCostEnergy_Base = 0x710,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerPercentageCostEnergyMulti_ES", Value=0x711)]
        PropertiesId_BlinkBlockerPercentageCostEnergyMulti_ES = 0x711,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerPercentageCostEnergy_NS", Value=0x713)]
        PropertiesId_BlinkBlockerPercentageCostEnergy_NS = 0x713,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerPercentageCostEnergy_Final", Value=0x715)]
        PropertiesId_BlinkBlockerPercentageCostEnergy_Final = 0x715,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerCostCPU_Base", Value=0x702)]
        PropertiesId_BlinkBlockerCostCPU_Base = 0x702,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerCostCPU_Final", Value=0x705)]
        PropertiesId_BlinkBlockerCostCPU_Final = 0x705,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerCostPower_Base", Value=0x706)]
        PropertiesId_BlinkBlockerCostPower_Base = 0x706,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerCostPowerMulti_ES", Value=0x707)]
        PropertiesId_BlinkBlockerCostPowerMulti_ES = 0x707,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerCostPower_Final", Value=0x709)]
        PropertiesId_BlinkBlockerCostPower_Final = 0x709,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerFireRange_Base", Value=0x70a)]
        PropertiesId_BlinkBlockerFireRange_Base = 0x70a,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerFireRangeMulti_ES", Value=0x748)]
        PropertiesId_BlinkBlockerFireRangeMulti_ES = 0x748,
        [ProtoEnum(Name="PropertiesId_BlinkBlockerFireRange_Final", Value=0x70f)]
        PropertiesId_BlinkBlockerFireRange_Final = 0x70f,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberRoundCD_Base", Value=0x13f)]
        PropertiesId_RemoteAtkDisturberRoundCD_Base = 0x13f,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberRoundCD_NS", Value=0x143)]
        PropertiesId_RemoteAtkDisturberRoundCD_NS = 0x143,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberRoundCD_Final", Value=0x3e7)]
        PropertiesId_RemoteAtkDisturberRoundCD_Final = 0x3e7,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberPercentageCostEnergy_Base", Value=0x144)]
        PropertiesId_RemoteAtkDisturberPercentageCostEnergy_Base = 0x144,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberPercentageCostEnergy_NS", Value=0x145)]
        PropertiesId_RemoteAtkDisturberPercentageCostEnergy_NS = 0x145,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberPercentageCostEnergy_Final", Value=0x3ea)]
        PropertiesId_RemoteAtkDisturberPercentageCostEnergy_Final = 0x3ea,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberFireRange_Base", Value=0x14b)]
        PropertiesId_RemoteAtkDisturberFireRange_Base = 0x14b,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberFireRangeMulti_ES", Value=0x14c)]
        PropertiesId_RemoteAtkDisturberFireRangeMulti_ES = 0x14c,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberFireRange_Final", Value=0x3eb)]
        PropertiesId_RemoteAtkDisturberFireRange_Final = 0x3eb,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberCostCPU_Base", Value=0x3ec)]
        PropertiesId_RemoteAtkDisturberCostCPU_Base = 0x3ec,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberCostCPU_Final", Value=0x3ed)]
        PropertiesId_RemoteAtkDisturberCostCPU_Final = 0x3ed,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberCostPower_Base", Value=0x3f1)]
        PropertiesId_RemoteAtkDisturberCostPower_Base = 0x3f1,
        [ProtoEnum(Name="PropertiesId_RemoteAtkDisturberCostPower_Final", Value=0x3f2)]
        PropertiesId_RemoteAtkDisturberCostPower_Final = 0x3f2,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberRoundCD_Base", Value=0x15f)]
        PropertiesId_RemoteAccuracyDisturberRoundCD_Base = 0x15f,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberRoundCD_NS", Value=0x160)]
        PropertiesId_RemoteAccuracyDisturberRoundCD_NS = 0x160,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberRoundCD_Final", Value=0x417)]
        PropertiesId_RemoteAccuracyDisturberRoundCD_Final = 0x417,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_Base", Value=0x161)]
        PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_Base = 0x161,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_NS", Value=0x162)]
        PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_NS = 0x162,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_Final", Value=0x41a)]
        PropertiesId_RemoteAccuracyDisturberPercentageCostEnergy_Final = 0x41a,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberFireRange_Base", Value=0x163)]
        PropertiesId_RemoteAccuracyDisturberFireRange_Base = 0x163,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberFireRangeMulti_ES", Value=0x1c9)]
        PropertiesId_RemoteAccuracyDisturberFireRangeMulti_ES = 0x1c9,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberFireRange_Final", Value=0x41b)]
        PropertiesId_RemoteAccuracyDisturberFireRange_Final = 0x41b,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberCostCPU_Base", Value=0x41f)]
        PropertiesId_RemoteAccuracyDisturberCostCPU_Base = 0x41f,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberCostCPU_Final", Value=0x420)]
        PropertiesId_RemoteAccuracyDisturberCostCPU_Final = 0x420,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberCostPower_Base", Value=0x421)]
        PropertiesId_RemoteAccuracyDisturberCostPower_Base = 0x421,
        [ProtoEnum(Name="PropertiesId_RemoteAccuracyDisturberCostPower_Final", Value=0x426)]
        PropertiesId_RemoteAccuracyDisturberCostPower_Final = 0x426,
        [ProtoEnum(Name="PropertiesId_RangeDisturberRoundCD_Base", Value=0x387)]
        PropertiesId_RangeDisturberRoundCD_Base = 0x387,
        [ProtoEnum(Name="PropertiesId_RangeDisturberRoundCD_NS", Value=0x153)]
        PropertiesId_RangeDisturberRoundCD_NS = 0x153,
        [ProtoEnum(Name="PropertiesId_RangeDisturberRoundCDMulti_ES", Value=0x388)]
        PropertiesId_RangeDisturberRoundCDMulti_ES = 0x388,
        [ProtoEnum(Name="PropertiesId_RangeDisturberRoundCDMulti_ED", Value=0x389)]
        PropertiesId_RangeDisturberRoundCDMulti_ED = 0x389,
        [ProtoEnum(Name="PropertiesId_RangeDisturberRoundCD_Final", Value=0x38c)]
        PropertiesId_RangeDisturberRoundCD_Final = 0x38c,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostEnergy_Base", Value=0x38d)]
        PropertiesId_RangeDisturberCostEnergy_Base = 0x38d,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostEnergyMulti_ES", Value=910)]
        PropertiesId_RangeDisturberCostEnergyMulti_ES = 910,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostEnergy_NS", Value=0x390)]
        PropertiesId_RangeDisturberCostEnergy_NS = 0x390,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostEnergy_ND", Value=0x391)]
        PropertiesId_RangeDisturberCostEnergy_ND = 0x391,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostEnergy_Final", Value=0x392)]
        PropertiesId_RangeDisturberCostEnergy_Final = 0x392,
        [ProtoEnum(Name="PropertiesId_RangeDisturberPercentageCostEnergy_Base", Value=0x6ca)]
        PropertiesId_RangeDisturberPercentageCostEnergy_Base = 0x6ca,
        [ProtoEnum(Name="PropertiesId_RangeDisturberPercentageCostEnergyMulti_ES", Value=0x6cb)]
        PropertiesId_RangeDisturberPercentageCostEnergyMulti_ES = 0x6cb,
        [ProtoEnum(Name="PropertiesId_RangeDisturberPercentageCostEnergy_NS", Value=0x6cd)]
        PropertiesId_RangeDisturberPercentageCostEnergy_NS = 0x6cd,
        [ProtoEnum(Name="PropertiesId_RangeDisturberPercentageCostEnergy_Final", Value=0x6cf)]
        PropertiesId_RangeDisturberPercentageCostEnergy_Final = 0x6cf,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostCPU_Base", Value=0x393)]
        PropertiesId_RangeDisturberCostCPU_Base = 0x393,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostCPU_Final", Value=0x396)]
        PropertiesId_RangeDisturberCostCPU_Final = 0x396,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostPower_Base", Value=0x397)]
        PropertiesId_RangeDisturberCostPower_Base = 0x397,
        [ProtoEnum(Name="PropertiesId_RangeDisturberCostPower_Final", Value=0x39a)]
        PropertiesId_RangeDisturberCostPower_Final = 0x39a,
        [ProtoEnum(Name="PropertiesId_RangeDisturberFireRange_Base", Value=0x39b)]
        PropertiesId_RangeDisturberFireRange_Base = 0x39b,
        [ProtoEnum(Name="PropertiesId_RangeDisturberFireRangeAdd_ES", Value=0x39c)]
        PropertiesId_RangeDisturberFireRangeAdd_ES = 0x39c,
        [ProtoEnum(Name="PropertiesId_RangeDisturberFireRangeMulti_ES", Value=0x636)]
        PropertiesId_RangeDisturberFireRangeMulti_ES = 0x636,
        [ProtoEnum(Name="PropertiesId_RangeDisturberFireRange_Final", Value=0x3a0)]
        PropertiesId_RangeDisturberFireRange_Final = 0x3a0,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberRoundCD_Base", Value=0x20d)]
        PropertiesId_RemoteDefDisturberRoundCD_Base = 0x20d,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberRoundCD_NS", Value=0x211)]
        PropertiesId_RemoteDefDisturberRoundCD_NS = 0x211,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberRoundCD_Final", Value=0x45a)]
        PropertiesId_RemoteDefDisturberRoundCD_Final = 0x45a,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberPercentageCostEnergy_Base", Value=0x216)]
        PropertiesId_RemoteDefDisturberPercentageCostEnergy_Base = 0x216,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberPercentageCostEnergy_NS", Value=0x217)]
        PropertiesId_RemoteDefDisturberPercentageCostEnergy_NS = 0x217,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberPercentageCostEnergy_Final", Value=0x45b)]
        PropertiesId_RemoteDefDisturberPercentageCostEnergy_Final = 0x45b,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberFireRange_Base", Value=0x21a)]
        PropertiesId_RemoteDefDisturberFireRange_Base = 0x21a,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberFireRangeMulti_ES", Value=0x21b)]
        PropertiesId_RemoteDefDisturberFireRangeMulti_ES = 0x21b,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberFireRange_Final", Value=0x461)]
        PropertiesId_RemoteDefDisturberFireRange_Final = 0x461,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberCostCPU_Base", Value=0x465)]
        PropertiesId_RemoteDefDisturberCostCPU_Base = 0x465,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberCostCPU_Final", Value=0x46a)]
        PropertiesId_RemoteDefDisturberCostCPU_Final = 0x46a,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberCostPower_Base", Value=0x46b)]
        PropertiesId_RemoteDefDisturberCostPower_Base = 0x46b,
        [ProtoEnum(Name="PropertiesId_RemoteDefDisturberCostPower_Final", Value=0x46e)]
        PropertiesId_RemoteDefDisturberCostPower_Final = 0x46e,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberRoundCD_Base", Value=0x253)]
        PropertiesId_RemoteMovDisturberRoundCD_Base = 0x253,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberRoundCD_NS", Value=0x257)]
        PropertiesId_RemoteMovDisturberRoundCD_NS = 0x257,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberRoundCD_Final", Value=0x48c)]
        PropertiesId_RemoteMovDisturberRoundCD_Final = 0x48c,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberPercentageCostEnergy_Base", Value=0x25c)]
        PropertiesId_RemoteMovDisturberPercentageCostEnergy_Base = 0x25c,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberPercentageCostEnergy_NS", Value=0x25d)]
        PropertiesId_RemoteMovDisturberPercentageCostEnergy_NS = 0x25d,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberPercentageCostEnergy_Final", Value=0x48d)]
        PropertiesId_RemoteMovDisturberPercentageCostEnergy_Final = 0x48d,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberFireRange_Base", Value=0x260)]
        PropertiesId_RemoteMovDisturberFireRange_Base = 0x260,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberFireRangeMulti_ES", Value=0x261)]
        PropertiesId_RemoteMovDisturberFireRangeMulti_ES = 0x261,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberFireRange_Final", Value=0x48e)]
        PropertiesId_RemoteMovDisturberFireRange_Final = 0x48e,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberCostCPU_Base", Value=0x492)]
        PropertiesId_RemoteMovDisturberCostCPU_Base = 0x492,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberCostCPU_Final", Value=0x496)]
        PropertiesId_RemoteMovDisturberCostCPU_Final = 0x496,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberCostPower_Base", Value=0x497)]
        PropertiesId_RemoteMovDisturberCostPower_Base = 0x497,
        [ProtoEnum(Name="PropertiesId_RemoteMovDisturberCostPower_Final", Value=0x499)]
        PropertiesId_RemoteMovDisturberCostPower_Final = 0x499,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberRoundCD_Base", Value=0x1d9)]
        PropertiesId_RemoteSignalDisturberRoundCD_Base = 0x1d9,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberRoundCD_NS", Value=480)]
        PropertiesId_RemoteSignalDisturberRoundCD_NS = 480,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberRoundCD_Final", Value=0x436)]
        PropertiesId_RemoteSignalDisturberRoundCD_Final = 0x436,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberPercentageCostEnergy_Base", Value=0x1e4)]
        PropertiesId_RemoteSignalDisturberPercentageCostEnergy_Base = 0x1e4,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberPercentageCostEnergy_NS", Value=0x1f5)]
        PropertiesId_RemoteSignalDisturberPercentageCostEnergy_NS = 0x1f5,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberPercentageCostEnergy_Final", Value=0x439)]
        PropertiesId_RemoteSignalDisturberPercentageCostEnergy_Final = 0x439,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberFireRange_Base", Value=0x1f6)]
        PropertiesId_RemoteSignalDisturberFireRange_Base = 0x1f6,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberFireRangeMulti_ES", Value=0x1fa)]
        PropertiesId_RemoteSignalDisturberFireRangeMulti_ES = 0x1fa,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberFireRange_Final", Value=0x43a)]
        PropertiesId_RemoteSignalDisturberFireRange_Final = 0x43a,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberCostCPU_Base", Value=0x43b)]
        PropertiesId_RemoteSignalDisturberCostCPU_Base = 0x43b,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberCostCPU_Final", Value=0x43c)]
        PropertiesId_RemoteSignalDisturberCostCPU_Final = 0x43c,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberCostPower_Base", Value=0x442)]
        PropertiesId_RemoteSignalDisturberCostPower_Base = 0x442,
        [ProtoEnum(Name="PropertiesId_RemoteSignalDisturberCostPower_Final", Value=0x446)]
        PropertiesId_RemoteSignalDisturberCostPower_Final = 0x446,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberRoundCD_Base", Value=0x14d)]
        PropertiesId_TeamAtkDisturberRoundCD_Base = 0x14d,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberRoundCD_NS", Value=0x14e)]
        PropertiesId_TeamAtkDisturberRoundCD_NS = 0x14e,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberRoundCD_Final", Value=0x3f3)]
        PropertiesId_TeamAtkDisturberRoundCD_Final = 0x3f3,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberPercentageCostEnergy_Base", Value=0x14f)]
        PropertiesId_TeamAtkDisturberPercentageCostEnergy_Base = 0x14f,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberPercentageCostEnergy_NS", Value=0x150)]
        PropertiesId_TeamAtkDisturberPercentageCostEnergy_NS = 0x150,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberPercentageCostEnergy_Final", Value=0x3f6)]
        PropertiesId_TeamAtkDisturberPercentageCostEnergy_Final = 0x3f6,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberFireRange_Base", Value=0x151)]
        PropertiesId_TeamAtkDisturberFireRange_Base = 0x151,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberFireRangeMulti_ES", Value=0x152)]
        PropertiesId_TeamAtkDisturberFireRangeMulti_ES = 0x152,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberFireRange_Final", Value=0x3f7)]
        PropertiesId_TeamAtkDisturberFireRange_Final = 0x3f7,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberCostCPU_Base", Value=0x3fc)]
        PropertiesId_TeamAtkDisturberCostCPU_Base = 0x3fc,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberCostCPU_Final", Value=0x3fd)]
        PropertiesId_TeamAtkDisturberCostCPU_Final = 0x3fd,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberCostPower_Base", Value=0x400)]
        PropertiesId_TeamAtkDisturberCostPower_Base = 0x400,
        [ProtoEnum(Name="PropertiesId_TeamAtkDisturberCostPower_Final", Value=0x401)]
        PropertiesId_TeamAtkDisturberCostPower_Final = 0x401,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberRoundCD_Base", Value=0x1ca)]
        PropertiesId_TeamAccuracyDisturberRoundCD_Base = 0x1ca,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberRoundCD_NS", Value=0x1cb)]
        PropertiesId_TeamAccuracyDisturberRoundCD_NS = 0x1cb,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberRoundCD_Final", Value=0x427)]
        PropertiesId_TeamAccuracyDisturberRoundCD_Final = 0x427,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_Base", Value=0x1d3)]
        PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_Base = 0x1d3,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_NS", Value=0x1d4)]
        PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_NS = 0x1d4,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_Final", Value=0x428)]
        PropertiesId_TeamAccuracyDisturberPercentageCostEnergy_Final = 0x428,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberFireRange_Base", Value=0x1d5)]
        PropertiesId_TeamAccuracyDisturberFireRange_Base = 0x1d5,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberFireRangeMulti_ES", Value=470)]
        PropertiesId_TeamAccuracyDisturberFireRangeMulti_ES = 470,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberFireRange_Final", Value=0x42b)]
        PropertiesId_TeamAccuracyDisturberFireRange_Final = 0x42b,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberCostCPU_Base", Value=0x42c)]
        PropertiesId_TeamAccuracyDisturberCostCPU_Base = 0x42c,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberCostCPU_Final", Value=0x431)]
        PropertiesId_TeamAccuracyDisturberCostCPU_Final = 0x431,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberCostPower_Base", Value=0x432)]
        PropertiesId_TeamAccuracyDisturberCostPower_Base = 0x432,
        [ProtoEnum(Name="PropertiesId_TeamAccuracyDisturberCostPower_Final", Value=0x435)]
        PropertiesId_TeamAccuracyDisturberCostPower_Final = 0x435,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberRoundCD_Base", Value=340)]
        PropertiesId_TeamRangeDisturberRoundCD_Base = 340,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberRoundCD_NS", Value=0x15a)]
        PropertiesId_TeamRangeDisturberRoundCD_NS = 0x15a,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberRoundCD_Final", Value=0x404)]
        PropertiesId_TeamRangeDisturberRoundCD_Final = 0x404,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberPercentageCostEnergy_Base", Value=0x15b)]
        PropertiesId_TeamRangeDisturberPercentageCostEnergy_Base = 0x15b,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberPercentageCostEnergy_NS", Value=0x15c)]
        PropertiesId_TeamRangeDisturberPercentageCostEnergy_NS = 0x15c,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberPercentageCostEnergy_Final", Value=0x405)]
        PropertiesId_TeamRangeDisturberPercentageCostEnergy_Final = 0x405,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberFireRange_Base", Value=0x15d)]
        PropertiesId_TeamRangeDisturberFireRange_Base = 0x15d,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberFireRangeMulti_ES", Value=350)]
        PropertiesId_TeamRangeDisturberFireRangeMulti_ES = 350,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberFireRange_Final", Value=0x406)]
        PropertiesId_TeamRangeDisturberFireRange_Final = 0x406,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberCostCPU_Base", Value=0x407)]
        PropertiesId_TeamRangeDisturberCostCPU_Base = 0x407,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberCostCPU_Final", Value=0x40d)]
        PropertiesId_TeamRangeDisturberCostCPU_Final = 0x40d,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberCostPower_Base", Value=0x411)]
        PropertiesId_TeamRangeDisturberCostPower_Base = 0x411,
        [ProtoEnum(Name="PropertiesId_TeamRangeDisturberCostPower_Final", Value=0x416)]
        PropertiesId_TeamRangeDisturberCostPower_Final = 0x416,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberRoundCD_Base", Value=0x224)]
        PropertiesId_TeamDefDisturberRoundCD_Base = 0x224,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberRoundCD_NS", Value=0x228)]
        PropertiesId_TeamDefDisturberRoundCD_NS = 0x228,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberRoundCD_Final", Value=0x46f)]
        PropertiesId_TeamDefDisturberRoundCD_Final = 0x46f,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberPercentageCostEnergy_Base", Value=0x22d)]
        PropertiesId_TeamDefDisturberPercentageCostEnergy_Base = 0x22d,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberPercentageCostEnergy_NS", Value=0x22e)]
        PropertiesId_TeamDefDisturberPercentageCostEnergy_NS = 0x22e,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberPercentageCostEnergy_Final", Value=0x473)]
        PropertiesId_TeamDefDisturberPercentageCostEnergy_Final = 0x473,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberFireRange_Base", Value=0x231)]
        PropertiesId_TeamDefDisturberFireRange_Base = 0x231,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberFireRangeMulti_ES", Value=0x232)]
        PropertiesId_TeamDefDisturberFireRangeMulti_ES = 0x232,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberFireRange_Final", Value=0x47a)]
        PropertiesId_TeamDefDisturberFireRange_Final = 0x47a,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberCostCPU_Base", Value=0x47e)]
        PropertiesId_TeamDefDisturberCostCPU_Base = 0x47e,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberCostCPU_Final", Value=0x483)]
        PropertiesId_TeamDefDisturberCostCPU_Final = 0x483,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberCostPower_Base", Value=0x484)]
        PropertiesId_TeamDefDisturberCostPower_Base = 0x484,
        [ProtoEnum(Name="PropertiesId_TeamDefDisturberCostPower_Final", Value=0x487)]
        PropertiesId_TeamDefDisturberCostPower_Final = 0x487,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecRoundCD_Base", Value=630)]
        PropertiesId_TeamEnergyDecRoundCD_Base = 630,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecRoundCD_NS", Value=0x279)]
        PropertiesId_TeamEnergyDecRoundCD_NS = 0x279,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecRoundCD_Final", Value=0x4aa)]
        PropertiesId_TeamEnergyDecRoundCD_Final = 0x4aa,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecPercentageCostEnergy_Base", Value=0x27a)]
        PropertiesId_TeamEnergyDecPercentageCostEnergy_Base = 0x27a,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecPercentageCostEnergy_NS", Value=0x27d)]
        PropertiesId_TeamEnergyDecPercentageCostEnergy_NS = 0x27d,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecPercentageCostEnergy_Final", Value=0x4ab)]
        PropertiesId_TeamEnergyDecPercentageCostEnergy_Final = 0x4ab,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecFireRange_Base", Value=0x27e)]
        PropertiesId_TeamEnergyDecFireRange_Base = 0x27e,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecFireRangeMulti_ES", Value=0x27f)]
        PropertiesId_TeamEnergyDecFireRangeMulti_ES = 0x27f,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecFireRange_Final", Value=0x4ac)]
        PropertiesId_TeamEnergyDecFireRange_Final = 0x4ac,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecCostCPU_Base", Value=0x4ad)]
        PropertiesId_TeamEnergyDecCostCPU_Base = 0x4ad,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecCostCPU_Final", Value=0x4b0)]
        PropertiesId_TeamEnergyDecCostCPU_Final = 0x4b0,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecCostPower_Base", Value=0x4b1)]
        PropertiesId_TeamEnergyDecCostPower_Base = 0x4b1,
        [ProtoEnum(Name="PropertiesId_TeamEnergyDecCostPower_Final", Value=0x4b2)]
        PropertiesId_TeamEnergyDecCostPower_Final = 0x4b2,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberRoundCD_Base", Value=0x269)]
        PropertiesId_TeamMovDisturberRoundCD_Base = 0x269,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberRoundCD_NS", Value=0x26a)]
        PropertiesId_TeamMovDisturberRoundCD_NS = 0x26a,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberRoundCD_Final", Value=0x49c)]
        PropertiesId_TeamMovDisturberRoundCD_Final = 0x49c,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberPercentageCostEnergy_Base", Value=620)]
        PropertiesId_TeamMovDisturberPercentageCostEnergy_Base = 620,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberPercentageCostEnergy_NS", Value=0x26f)]
        PropertiesId_TeamMovDisturberPercentageCostEnergy_NS = 0x26f,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberPercentageCostEnergy_Final", Value=0x49d)]
        PropertiesId_TeamMovDisturberPercentageCostEnergy_Final = 0x49d,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberFireRange_Base", Value=0x270)]
        PropertiesId_TeamMovDisturberFireRange_Base = 0x270,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberFireRangeMulti_ES", Value=0x271)]
        PropertiesId_TeamMovDisturberFireRangeMulti_ES = 0x271,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberFireRange_Final", Value=0x49e)]
        PropertiesId_TeamMovDisturberFireRange_Final = 0x49e,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberCostCPU_Base", Value=0x4a2)]
        PropertiesId_TeamMovDisturberCostCPU_Base = 0x4a2,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberCostCPU_Final", Value=0x4a3)]
        PropertiesId_TeamMovDisturberCostCPU_Final = 0x4a3,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberCostPower_Base", Value=0x4a6)]
        PropertiesId_TeamMovDisturberCostPower_Base = 0x4a6,
        [ProtoEnum(Name="PropertiesId_TeamMovDisturberCostPower_Final", Value=0x4a7)]
        PropertiesId_TeamMovDisturberCostPower_Final = 0x4a7,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerRoundCD_Base", Value=0x283)]
        PropertiesId_TeamBlinkBlockerRoundCD_Base = 0x283,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerRoundCD_NS", Value=0x285)]
        PropertiesId_TeamBlinkBlockerRoundCD_NS = 0x285,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerRoundCD_Final", Value=0x4b8)]
        PropertiesId_TeamBlinkBlockerRoundCD_Final = 0x4b8,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerPercentageCostEnergy_Base", Value=0x289)]
        PropertiesId_TeamBlinkBlockerPercentageCostEnergy_Base = 0x289,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerPercentageCostEnergy_NS", Value=0x28e)]
        PropertiesId_TeamBlinkBlockerPercentageCostEnergy_NS = 0x28e,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerPercentageCostEnergy_Final", Value=0x4b9)]
        PropertiesId_TeamBlinkBlockerPercentageCostEnergy_Final = 0x4b9,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerFireRange_Base", Value=0x28f)]
        PropertiesId_TeamBlinkBlockerFireRange_Base = 0x28f,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerFireRangeMulti_ES", Value=0x292)]
        PropertiesId_TeamBlinkBlockerFireRangeMulti_ES = 0x292,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerFireRange_Final", Value=0x4bd)]
        PropertiesId_TeamBlinkBlockerFireRange_Final = 0x4bd,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerCostCPU_Base", Value=0x4c2)]
        PropertiesId_TeamBlinkBlockerCostCPU_Base = 0x4c2,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerCostCPU_Final", Value=0x4c3)]
        PropertiesId_TeamBlinkBlockerCostCPU_Final = 0x4c3,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerCostPower_Base", Value=0x4c6)]
        PropertiesId_TeamBlinkBlockerCostPower_Base = 0x4c6,
        [ProtoEnum(Name="PropertiesId_TeamBlinkBlockerCostPower_Final", Value=0x4c7)]
        PropertiesId_TeamBlinkBlockerCostPower_Final = 0x4c7,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberRoundCD_Base", Value=0x1ff)]
        PropertiesId_TeamSignalDisturberRoundCD_Base = 0x1ff,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberRoundCD_NS", Value=0x200)]
        PropertiesId_TeamSignalDisturberRoundCD_NS = 0x200,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberRoundCD_Final", Value=0x44b)]
        PropertiesId_TeamSignalDisturberRoundCD_Final = 0x44b,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberPercentageCostEnergy_Base", Value=0x203)]
        PropertiesId_TeamSignalDisturberPercentageCostEnergy_Base = 0x203,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberPercentageCostEnergy_NS", Value=0x204)]
        PropertiesId_TeamSignalDisturberPercentageCostEnergy_NS = 0x204,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberPercentageCostEnergy_Final", Value=0x44c)]
        PropertiesId_TeamSignalDisturberPercentageCostEnergy_Final = 0x44c,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberFireRange_Base", Value=0x20b)]
        PropertiesId_TeamSignalDisturberFireRange_Base = 0x20b,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberFireRangeMulti_ES", Value=0x20c)]
        PropertiesId_TeamSignalDisturberFireRangeMulti_ES = 0x20c,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberFireRange_Final", Value=0x44f)]
        PropertiesId_TeamSignalDisturberFireRange_Final = 0x44f,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberCostCPU_Base", Value=0x450)]
        PropertiesId_TeamSignalDisturberCostCPU_Base = 0x450,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberCostCPU_Final", Value=0x454)]
        PropertiesId_TeamSignalDisturberCostCPU_Final = 0x454,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberCostPower_Base", Value=0x455)]
        PropertiesId_TeamSignalDisturberCostPower_Base = 0x455,
        [ProtoEnum(Name="PropertiesId_TeamSignalDisturberCostPower_Final", Value=0x456)]
        PropertiesId_TeamSignalDisturberCostPower_Final = 0x456,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerRoundCD_Base", Value=0x765)]
        PropertiesId_SpaceSignalGravityScannerRoundCD_Base = 0x765,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerRoundCD_NS", Value=0x50b)]
        PropertiesId_SpaceSignalGravityScannerRoundCD_NS = 0x50b,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerRoundCD_Final", Value=0x766)]
        PropertiesId_SpaceSignalGravityScannerRoundCD_Final = 0x766,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerPercentageCostEnergy_Base", Value=0x767)]
        PropertiesId_SpaceSignalGravityScannerPercentageCostEnergy_Base = 0x767,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerPercentageCostEnergy_Final", Value=0x768)]
        PropertiesId_SpaceSignalGravityScannerPercentageCostEnergy_Final = 0x768,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerCostCPU_Base", Value=0x76b)]
        PropertiesId_SpaceSignalGravityScannerCostCPU_Base = 0x76b,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerCostCPU_Final", Value=0x76c)]
        PropertiesId_SpaceSignalGravityScannerCostCPU_Final = 0x76c,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerCostPower_Base", Value=0x76d)]
        PropertiesId_SpaceSignalGravityScannerCostPower_Base = 0x76d,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityScannerCostPower_Final", Value=0x76e)]
        PropertiesId_SpaceSignalGravityScannerCostPower_Final = 0x76e,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerRoundCD_Base", Value=0x643)]
        PropertiesId_SpaceSignalMicroWaveScannerRoundCD_Base = 0x643,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerRoundCD_NS", Value=0x510)]
        PropertiesId_SpaceSignalMicroWaveScannerRoundCD_NS = 0x510,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerRoundCD_Final", Value=0x645)]
        PropertiesId_SpaceSignalMicroWaveScannerRoundCD_Final = 0x645,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerPercentageCostEnergy_Base", Value=0x647)]
        PropertiesId_SpaceSignalMicroWaveScannerPercentageCostEnergy_Base = 0x647,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerPercentageCostEnergy_Final", Value=0x4ec)]
        PropertiesId_SpaceSignalMicroWaveScannerPercentageCostEnergy_Final = 0x4ec,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerCostCPU_Base", Value=0x4ed)]
        PropertiesId_SpaceSignalMicroWaveScannerCostCPU_Base = 0x4ed,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerCostCPU_Final", Value=0x4f1)]
        PropertiesId_SpaceSignalMicroWaveScannerCostCPU_Final = 0x4f1,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerCostPower_Base", Value=0x4f6)]
        PropertiesId_SpaceSignalMicroWaveScannerCostPower_Base = 0x4f6,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWaveScannerCostPower_Final", Value=0x4f7)]
        PropertiesId_SpaceSignalMicroWaveScannerCostPower_Final = 0x4f7,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerRoundCD_Base", Value=0x4fa)]
        PropertiesId_SpaceSignalRadiationScannerRoundCD_Base = 0x4fa,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerRoundCD_NS", Value=0x511)]
        PropertiesId_SpaceSignalRadiationScannerRoundCD_NS = 0x511,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerRoundCD_Final", Value=0x4fb)]
        PropertiesId_SpaceSignalRadiationScannerRoundCD_Final = 0x4fb,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerPercentageCostEnergy_Base", Value=0x4fe)]
        PropertiesId_SpaceSignalRadiationScannerPercentageCostEnergy_Base = 0x4fe,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerPercentageCostEnergy_Final", Value=0x4ff)]
        PropertiesId_SpaceSignalRadiationScannerPercentageCostEnergy_Final = 0x4ff,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerCostCPU_Base", Value=0x500)]
        PropertiesId_SpaceSignalRadiationScannerCostCPU_Base = 0x500,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerCostCPU_Final", Value=0x501)]
        PropertiesId_SpaceSignalRadiationScannerCostCPU_Final = 0x501,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerCostPower_Base", Value=0x507)]
        PropertiesId_SpaceSignalRadiationScannerCostPower_Base = 0x507,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationScannerCostPower_Final", Value=0x50a)]
        PropertiesId_SpaceSignalRadiationScannerCostPower_Final = 0x50a,
        [ProtoEnum(Name="PropertiesId_ShipCPU_Base", Value=0xf6)]
        PropertiesId_ShipCPU_Base = 0xf6,
        [ProtoEnum(Name="PropertiesId_ShipCPUMulti_ES", Value=0xf7)]
        PropertiesId_ShipCPUMulti_ES = 0xf7,
        [ProtoEnum(Name="PropertiesId_ShipCPU_Final", Value=0x181)]
        PropertiesId_ShipCPU_Final = 0x181,
        [ProtoEnum(Name="PropertiesId_ShipPower_Base", Value=250)]
        PropertiesId_ShipPower_Base = 250,
        [ProtoEnum(Name="PropertiesId_ShipPowerMulti_ES", Value=0xfb)]
        PropertiesId_ShipPowerMulti_ES = 0xfb,
        [ProtoEnum(Name="PropertiesId_ShipPower_Final", Value=0x182)]
        PropertiesId_ShipPower_Final = 0x182,
        [ProtoEnum(Name="PropertiesId_ShipEnergy_Base", Value=0xf2)]
        PropertiesId_ShipEnergy_Base = 0xf2,
        [ProtoEnum(Name="PropertiesId_ShipEnergyAdd_ES", Value=0xf4)]
        PropertiesId_ShipEnergyAdd_ES = 0xf4,
        [ProtoEnum(Name="PropertiesId_ShipEnergyMulti_ES", Value=0xf3)]
        PropertiesId_ShipEnergyMulti_ES = 0xf3,
        [ProtoEnum(Name="PropertiesId_ShipEnergy_NS", Value=0xf5)]
        PropertiesId_ShipEnergy_NS = 0xf5,
        [ProtoEnum(Name="PropertiesId_ShipEnergy_Final", Value=0x180)]
        PropertiesId_ShipEnergy_Final = 0x180,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecovery_Base", Value=0x13a)]
        PropertiesId_ShipEnergyPassiveRecovery_Base = 0x13a,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecoveryMulti_ES", Value=0x13b)]
        PropertiesId_ShipEnergyPassiveRecoveryMulti_ES = 0x13b,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecoveryMulti_ED", Value=0x13c)]
        PropertiesId_ShipEnergyPassiveRecoveryMulti_ED = 0x13c,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecovery_NS", Value=0x13d)]
        PropertiesId_ShipEnergyPassiveRecovery_NS = 0x13d,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecovery_ND", Value=0x518)]
        PropertiesId_ShipEnergyPassiveRecovery_ND = 0x518,
        [ProtoEnum(Name="PropertiesId_ShipEnergyPassiveRecovery_Final", Value=0x197)]
        PropertiesId_ShipEnergyPassiveRecovery_Final = 0x197,
        [ProtoEnum(Name="PropertiesId_ShipShield_Base", Value=0xee)]
        PropertiesId_ShipShield_Base = 0xee,
        [ProtoEnum(Name="PropertiesId_ShipShieldAdd_ES", Value=240)]
        PropertiesId_ShipShieldAdd_ES = 240,
        [ProtoEnum(Name="PropertiesId_ShipShieldMulti_ES", Value=0xef)]
        PropertiesId_ShipShieldMulti_ES = 0xef,
        [ProtoEnum(Name="PropertiesId_ShipShieldMulti_ED", Value=0x6aa)]
        PropertiesId_ShipShieldMulti_ED = 0x6aa,
        [ProtoEnum(Name="PropertiesId_ShipShield_NS", Value=0xf1)]
        PropertiesId_ShipShield_NS = 0xf1,
        [ProtoEnum(Name="PropertiesId_ShipShield_Final", Value=0x17f)]
        PropertiesId_ShipShield_Final = 0x17f,
        [ProtoEnum(Name="PropertiesId_ShipShieldPassiveRecovery_Base", Value=0x13e)]
        PropertiesId_ShipShieldPassiveRecovery_Base = 0x13e,
        [ProtoEnum(Name="PropertiesId_ShipShieldPassiveRecoveryMulti_ES", Value=0x4e1)]
        PropertiesId_ShipShieldPassiveRecoveryMulti_ES = 0x4e1,
        [ProtoEnum(Name="PropertiesId_ShipShieldPassiveRecoveryMulti_ED", Value=320)]
        PropertiesId_ShipShieldPassiveRecoveryMulti_ED = 320,
        [ProtoEnum(Name="PropertiesId_ShipShieldPassiveRecovery_NS", Value=0x141)]
        PropertiesId_ShipShieldPassiveRecovery_NS = 0x141,
        [ProtoEnum(Name="PropertiesId_ShipShieldPassiveRecovery_Final", Value=0x198)]
        PropertiesId_ShipShieldPassiveRecovery_Final = 0x198,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_Base", Value=60)]
        PropertiesId_ShipShieldElectricDamageResist_Base = 60,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_ES", Value=0x45)]
        PropertiesId_ShipShieldElectricDamageResist_ES = 0x45,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_ED", Value=0x42)]
        PropertiesId_ShipShieldElectricDamageResist_ED = 0x42,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_ND", Value=0x3f)]
        PropertiesId_ShipShieldElectricDamageResist_ND = 0x3f,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_Final", Value=0x1ba)]
        PropertiesId_ShipShieldElectricDamageResist_Final = 0x1ba,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResistModify_FrameCache", Value=0x67a)]
        PropertiesId_ShipShieldElectricDamageResistModify_FrameCache = 0x67a,
        [ProtoEnum(Name="PropertiesId_ShipShieldElectricDamageResist_FinalFrameCache", Value=0x67d)]
        PropertiesId_ShipShieldElectricDamageResist_FinalFrameCache = 0x67d,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_Base", Value=0x3a)]
        PropertiesId_ShipShieldHeatDamageResist_Base = 0x3a,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_ES", Value=0x43)]
        PropertiesId_ShipShieldHeatDamageResist_ES = 0x43,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_ED", Value=0x40)]
        PropertiesId_ShipShieldHeatDamageResist_ED = 0x40,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_ND", Value=0x3d)]
        PropertiesId_ShipShieldHeatDamageResist_ND = 0x3d,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_Final", Value=440)]
        PropertiesId_ShipShieldHeatDamageResist_Final = 440,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResistModify_FrameCache", Value=0x678)]
        PropertiesId_ShipShieldHeatDamageResistModify_FrameCache = 0x678,
        [ProtoEnum(Name="PropertiesId_ShipShieldHeatDamageResist_FinalFrameCache", Value=0x67b)]
        PropertiesId_ShipShieldHeatDamageResist_FinalFrameCache = 0x67b,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_Base", Value=0x3b)]
        PropertiesId_ShipShieldKineticDamageResist_Base = 0x3b,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_ES", Value=0x44)]
        PropertiesId_ShipShieldKineticDamageResist_ES = 0x44,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_ED", Value=0x41)]
        PropertiesId_ShipShieldKineticDamageResist_ED = 0x41,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_ND", Value=0x3e)]
        PropertiesId_ShipShieldKineticDamageResist_ND = 0x3e,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_Final", Value=0x1b9)]
        PropertiesId_ShipShieldKineticDamageResist_Final = 0x1b9,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResistModify_FrameCache", Value=0x679)]
        PropertiesId_ShipShieldKineticDamageResistModify_FrameCache = 0x679,
        [ProtoEnum(Name="PropertiesId_ShipShieldKineticDamageResist_FinalFrameCache", Value=0x67c)]
        PropertiesId_ShipShieldKineticDamageResist_FinalFrameCache = 0x67c,
        [ProtoEnum(Name="PropertiesId_ShipArmor_Base", Value=0xea)]
        PropertiesId_ShipArmor_Base = 0xea,
        [ProtoEnum(Name="PropertiesId_ShipArmorAdd_ES", Value=0xec)]
        PropertiesId_ShipArmorAdd_ES = 0xec,
        [ProtoEnum(Name="PropertiesId_ShipArmorMulti_ES", Value=0xeb)]
        PropertiesId_ShipArmorMulti_ES = 0xeb,
        [ProtoEnum(Name="PropertiesId_ShipArmor_NS", Value=0xed)]
        PropertiesId_ShipArmor_NS = 0xed,
        [ProtoEnum(Name="PropertiesId_ShipArmor_Final", Value=0x17e)]
        PropertiesId_ShipArmor_Final = 0x17e,
        [ProtoEnum(Name="PropertiesId_ShipArmorPassiveRecovery_Base", Value=0x142)]
        PropertiesId_ShipArmorPassiveRecovery_Base = 0x142,
        [ProtoEnum(Name="PropertiesId_ShipArmorPassiveRecovery_Final", Value=0x199)]
        PropertiesId_ShipArmorPassiveRecovery_Final = 0x199,
        [ProtoEnum(Name="PropertiesId_ShipArmorElectricDamageResist_Base", Value=0x48)]
        PropertiesId_ShipArmorElectricDamageResist_Base = 0x48,
        [ProtoEnum(Name="PropertiesId_ShipArmorElectricDamageResist_ED", Value=0x4e)]
        PropertiesId_ShipArmorElectricDamageResist_ED = 0x4e,
        [ProtoEnum(Name="PropertiesId_ShipArmorElectricDamageResist_ND", Value=0x4b)]
        PropertiesId_ShipArmorElectricDamageResist_ND = 0x4b,
        [ProtoEnum(Name="PropertiesId_ShipArmorElectricDamageResist_Final", Value=0x1bd)]
        PropertiesId_ShipArmorElectricDamageResist_Final = 0x1bd,
        [ProtoEnum(Name="PropertiesId_ShipArmorHeatDamageResist_Base", Value=70)]
        PropertiesId_ShipArmorHeatDamageResist_Base = 70,
        [ProtoEnum(Name="PropertiesId_ShipArmorHeatDamageResist_ED", Value=0x4c)]
        PropertiesId_ShipArmorHeatDamageResist_ED = 0x4c,
        [ProtoEnum(Name="PropertiesId_ShipArmorHeatDamageResist_ND", Value=0x49)]
        PropertiesId_ShipArmorHeatDamageResist_ND = 0x49,
        [ProtoEnum(Name="PropertiesId_ShipArmorHeatDamageResist_Final", Value=0x1bb)]
        PropertiesId_ShipArmorHeatDamageResist_Final = 0x1bb,
        [ProtoEnum(Name="PropertiesId_ShipArmorKineticDamageResist_Base", Value=0x47)]
        PropertiesId_ShipArmorKineticDamageResist_Base = 0x47,
        [ProtoEnum(Name="PropertiesId_ShipArmorKineticDamageResist_ED", Value=0x4d)]
        PropertiesId_ShipArmorKineticDamageResist_ED = 0x4d,
        [ProtoEnum(Name="PropertiesId_ShipArmorKineticDamageResist_ND", Value=0x4a)]
        PropertiesId_ShipArmorKineticDamageResist_ND = 0x4a,
        [ProtoEnum(Name="PropertiesId_ShipArmorKineticDamageResist_Final", Value=0x1bc)]
        PropertiesId_ShipArmorKineticDamageResist_Final = 0x1bc,
        [ProtoEnum(Name="PropertiesId_ShipMass_Base", Value=0xd6)]
        PropertiesId_ShipMass_Base = 0xd6,
        [ProtoEnum(Name="PropertiesId_ShipMass_Final", Value=0x17b)]
        PropertiesId_ShipMass_Final = 0x17b,
        [ProtoEnum(Name="PropertiesId_Volumn_Base", Value=0x93)]
        PropertiesId_Volumn_Base = 0x93,
        [ProtoEnum(Name="PropertiesId_VolumnAdd_ES", Value=0x97)]
        PropertiesId_VolumnAdd_ES = 0x97,
        [ProtoEnum(Name="PropertiesId_VolumnMulti_ES", Value=0x6b5)]
        PropertiesId_VolumnMulti_ES = 0x6b5,
        [ProtoEnum(Name="PropertiesId_VolumnMulti_ED", Value=0x6b4)]
        PropertiesId_VolumnMulti_ED = 0x6b4,
        [ProtoEnum(Name="PropertiesId_Volumn_NS", Value=0x95)]
        PropertiesId_Volumn_NS = 0x95,
        [ProtoEnum(Name="PropertiesId_Volumn_ND", Value=0x94)]
        PropertiesId_Volumn_ND = 0x94,
        [ProtoEnum(Name="PropertiesId_Volumn_Final", Value=430)]
        PropertiesId_Volumn_Final = 430,
        [ProtoEnum(Name="PropertiesId_ShipInertiaParam_Base", Value=0xe3)]
        PropertiesId_ShipInertiaParam_Base = 0xe3,
        [ProtoEnum(Name="PropertiesId_ShipInertiaParamMulti_ES", Value=0x751)]
        PropertiesId_ShipInertiaParamMulti_ES = 0x751,
        [ProtoEnum(Name="PropertiesId_ShipInertiaParam_NS", Value=0xe5)]
        PropertiesId_ShipInertiaParam_NS = 0xe5,
        [ProtoEnum(Name="PropertiesId_ShipInertiaParam_ND", Value=0x6a3)]
        PropertiesId_ShipInertiaParam_ND = 0x6a3,
        [ProtoEnum(Name="PropertiesId_ShipInertiaParam_Final", Value=380)]
        PropertiesId_ShipInertiaParam_Final = 380,
        [ProtoEnum(Name="PropertiesId_ShipSpeed_Base", Value=0xd8)]
        PropertiesId_ShipSpeed_Base = 0xd8,
        [ProtoEnum(Name="PropertiesId_ShipSpeedMulti_ES", Value=0xda)]
        PropertiesId_ShipSpeedMulti_ES = 0xda,
        [ProtoEnum(Name="PropertiesId_ShipSpeedMulti_ED", Value=0xd9)]
        PropertiesId_ShipSpeedMulti_ED = 0xd9,
        [ProtoEnum(Name="PropertiesId_ShipSpeed_NS", Value=220)]
        PropertiesId_ShipSpeed_NS = 220,
        [ProtoEnum(Name="PropertiesId_ShipSpeed_ND", Value=0xdb)]
        PropertiesId_ShipSpeed_ND = 0xdb,
        [ProtoEnum(Name="PropertiesId_ShipMaxSpeed_Final", Value=0xdd)]
        PropertiesId_ShipMaxSpeed_Final = 0xdd,
        [ProtoEnum(Name="PropertiesId_ShipMaxSpeedNoBooster_Final", Value=0x1b0)]
        PropertiesId_ShipMaxSpeedNoBooster_Final = 0x1b0,
        [ProtoEnum(Name="PropertiesId_ShipJumpSteady_Base", Value=0xde)]
        PropertiesId_ShipJumpSteady_Base = 0xde,
        [ProtoEnum(Name="PropertiesId_ShipJumpSteadyAdd_ES", Value=0xdf)]
        PropertiesId_ShipJumpSteadyAdd_ES = 0xdf,
        [ProtoEnum(Name="PropertiesId_ShipJumpDisturbAdd_ED", Value=0xe0)]
        PropertiesId_ShipJumpDisturbAdd_ED = 0xe0,
        [ProtoEnum(Name="PropertiesId_ShipJumpSteady_Final", Value=0x1a8)]
        PropertiesId_ShipJumpSteady_Final = 0x1a8,
        [ProtoEnum(Name="PropertiesId_ShipJumpCostEnergy_Base", Value=0xe1)]
        PropertiesId_ShipJumpCostEnergy_Base = 0xe1,
        [ProtoEnum(Name="PropertiesId_ShipJumpAccelerateParam_Base", Value=0xe7)]
        PropertiesId_ShipJumpAccelerateParam_Base = 0xe7,
        [ProtoEnum(Name="PropertiesId_ShipJumpMaxSpeed_Base", Value=230)]
        PropertiesId_ShipJumpMaxSpeed_Base = 230,
        [ProtoEnum(Name="PropertiesId_ShipJumpMaxSpeedMulti_ES", Value=0xe8)]
        PropertiesId_ShipJumpMaxSpeedMulti_ES = 0xe8,
        [ProtoEnum(Name="PropertiesId_ShipJumpMaxSpeed_Final", Value=0x17d)]
        PropertiesId_ShipJumpMaxSpeed_Final = 0x17d,
        [ProtoEnum(Name="PropertiesId_ShipItemStoreSize_Base", Value=0xfe)]
        PropertiesId_ShipItemStoreSize_Base = 0xfe,
        [ProtoEnum(Name="PropertiesId_ShipItemStoreSizeMulti_ES", Value=0x4e7)]
        PropertiesId_ShipItemStoreSizeMulti_ES = 0x4e7,
        [ProtoEnum(Name="PropertiesId_ShipItemStoreSize_Final", Value=0x183)]
        PropertiesId_ShipItemStoreSize_Final = 0x183,
        [ProtoEnum(Name="PropertiesId_SpaceSignalGravityPoint", Value=0x771)]
        PropertiesId_SpaceSignalGravityPoint = 0x771,
        [ProtoEnum(Name="PropertiesId_SpaceSignalMicroWavePoint", Value=0x772)]
        PropertiesId_SpaceSignalMicroWavePoint = 0x772,
        [ProtoEnum(Name="PropertiesId_SpaceSignalRadiationPoint", Value=0x773)]
        PropertiesId_SpaceSignalRadiationPoint = 0x773,
        [ProtoEnum(Name="PropertiesId_FlagShipTeleportRadius_Base", Value=0x77d)]
        PropertiesId_FlagShipTeleportRadius_Base = 0x77d,
        [ProtoEnum(Name="PropertiesId_FlagShipTeleportRadiusMulti_ES", Value=0x77a)]
        PropertiesId_FlagShipTeleportRadiusMulti_ES = 0x77a,
        [ProtoEnum(Name="PropertiesId_FlagShipTeleportRadius_Final", Value=0x77e)]
        PropertiesId_FlagShipTeleportRadius_Final = 0x77e,
        [ProtoEnum(Name="PropertiesId_FlagShipMoveDisableTimeAfterTeleportMulti_NS", Value=0x77b)]
        PropertiesId_FlagShipMoveDisableTimeAfterTeleportMulti_NS = 0x77b,
        [ProtoEnum(Name="PropertiesId_FlagShipTeleportDisableTimeAfterTeleportMulti_NS", Value=0x77c)]
        PropertiesId_FlagShipTeleportDisableTimeAfterTeleportMulti_NS = 0x77c,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_Base", Value=0x764)]
        PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_Base = 0x764,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_ND", Value=0x514)]
        PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_ND = 0x514,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_Final", Value=0x769)]
        PropertiesId_EnergyOnceRepairAmountByUseSuperEquip_Final = 0x769,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_Base", Value=0x76a)]
        PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_Base = 0x76a,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_ND", Value=0x515)]
        PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_ND = 0x515,
        [ProtoEnum(Name="PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_Final", Value=0x76f)]
        PropertiesId_EnergyOnceRepairAmountByUseTacticalEquip_Final = 0x76f,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryModify_S", Value=0x120)]
        PropertiesId_SuperWeaponEnergyRecoveryModify_S = 0x120,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryModify_D", Value=0x4b4)]
        PropertiesId_SuperWeaponEnergyRecoveryModify_D = 0x4b4,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryFinalModify_FrameCache", Value=0x695)]
        PropertiesId_SuperWeaponEnergyRecoveryFinalModify_FrameCache = 0x695,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x6a6)]
        PropertiesId_SuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x6a6,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryOnEnergyUseMulti_ED", Value=0x6a7)]
        PropertiesId_SuperWeaponEnergyRecoveryOnEnergyUseMulti_ED = 0x6a7,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x6a4)]
        PropertiesId_SuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x6a4,
        [ProtoEnum(Name="PropertiesId_SuperWeaponEnergyRecoveryOnHitByEnemyMulti_ED", Value=0x6a5)]
        PropertiesId_SuperWeaponEnergyRecoveryOnHitByEnemyMulti_ED = 0x6a5,
        [ProtoEnum(Name="PropertiesId_EFFSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x307)]
        PropertiesId_EFFSuperWeaponEnergyRecoveryModifyMulti_ES = 0x307,
        [ProtoEnum(Name="PropertiesId_EFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x30b)]
        PropertiesId_EFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x30b,
        [ProtoEnum(Name="PropertiesId_EFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x310)]
        PropertiesId_EFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x310,
        [ProtoEnum(Name="PropertiesId_EDDSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x311)]
        PropertiesId_EDDSuperWeaponEnergyRecoveryModifyMulti_ES = 0x311,
        [ProtoEnum(Name="PropertiesId_EDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x314)]
        PropertiesId_EDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x314,
        [ProtoEnum(Name="PropertiesId_EDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x315)]
        PropertiesId_EDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x315,
        [ProtoEnum(Name="PropertiesId_ECASuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x319)]
        PropertiesId_ECASuperWeaponEnergyRecoveryModifyMulti_ES = 0x319,
        [ProtoEnum(Name="PropertiesId_ECASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x31a)]
        PropertiesId_ECASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x31a,
        [ProtoEnum(Name="PropertiesId_ECASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x31b)]
        PropertiesId_ECASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x31b,
        [ProtoEnum(Name="PropertiesId_EBCSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x31f)]
        PropertiesId_EBCSuperWeaponEnergyRecoveryModifyMulti_ES = 0x31f,
        [ProtoEnum(Name="PropertiesId_EBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=800)]
        PropertiesId_EBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 800,
        [ProtoEnum(Name="PropertiesId_EBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x321)]
        PropertiesId_EBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x321,
        [ProtoEnum(Name="PropertiesId_EBBSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x324)]
        PropertiesId_EBBSuperWeaponEnergyRecoveryModifyMulti_ES = 0x324,
        [ProtoEnum(Name="PropertiesId_EBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x325)]
        PropertiesId_EBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x325,
        [ProtoEnum(Name="PropertiesId_EBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=810)]
        PropertiesId_EBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 810,
        [ProtoEnum(Name="PropertiesId_EFGSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x353)]
        PropertiesId_EFGSuperWeaponEnergyRecoveryModifyMulti_ES = 0x353,
        [ProtoEnum(Name="PropertiesId_NFFSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2cb)]
        PropertiesId_NFFSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2cb,
        [ProtoEnum(Name="PropertiesId_NFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2ce)]
        PropertiesId_NFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2ce,
        [ProtoEnum(Name="PropertiesId_NFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2cf)]
        PropertiesId_NFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2cf,
        [ProtoEnum(Name="PropertiesId_NDDSuperWeaponEnergyRecoveryModifyMulti_ES", Value=720)]
        PropertiesId_NDDSuperWeaponEnergyRecoveryModifyMulti_ES = 720,
        [ProtoEnum(Name="PropertiesId_NDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2d4)]
        PropertiesId_NDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2d4,
        [ProtoEnum(Name="PropertiesId_NDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2d5)]
        PropertiesId_NDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2d5,
        [ProtoEnum(Name="PropertiesId_NCASuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2d8)]
        PropertiesId_NCASuperWeaponEnergyRecoveryModifyMulti_ES = 0x2d8,
        [ProtoEnum(Name="PropertiesId_NCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2d9)]
        PropertiesId_NCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2d9,
        [ProtoEnum(Name="PropertiesId_NCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2dc)]
        PropertiesId_NCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2dc,
        [ProtoEnum(Name="PropertiesId_NBCSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2dd)]
        PropertiesId_NBCSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2dd,
        [ProtoEnum(Name="PropertiesId_NBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2de)]
        PropertiesId_NBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2de,
        [ProtoEnum(Name="PropertiesId_NBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2df)]
        PropertiesId_NBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2df,
        [ProtoEnum(Name="PropertiesId_NBBSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2e2)]
        PropertiesId_NBBSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2e2,
        [ProtoEnum(Name="PropertiesId_NBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2e3)]
        PropertiesId_NBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2e3,
        [ProtoEnum(Name="PropertiesId_NBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2e8)]
        PropertiesId_NBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2e8,
        [ProtoEnum(Name="PropertiesId_NFGSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x34e)]
        PropertiesId_NFGSuperWeaponEnergyRecoveryModifyMulti_ES = 0x34e,
        [ProtoEnum(Name="PropertiesId_RFFSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2b0)]
        PropertiesId_RFFSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2b0,
        [ProtoEnum(Name="PropertiesId_RFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2b1)]
        PropertiesId_RFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2b1,
        [ProtoEnum(Name="PropertiesId_RFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2b4)]
        PropertiesId_RFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2b4,
        [ProtoEnum(Name="PropertiesId_RDDSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2b5)]
        PropertiesId_RDDSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2b5,
        [ProtoEnum(Name="PropertiesId_RDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2b6)]
        PropertiesId_RDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2b6,
        [ProtoEnum(Name="PropertiesId_RDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2b7)]
        PropertiesId_RDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2b7,
        [ProtoEnum(Name="PropertiesId_RCASuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2ba)]
        PropertiesId_RCASuperWeaponEnergyRecoveryModifyMulti_ES = 0x2ba,
        [ProtoEnum(Name="PropertiesId_RCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2bb)]
        PropertiesId_RCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2bb,
        [ProtoEnum(Name="PropertiesId_RCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2c0)]
        PropertiesId_RCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2c0,
        [ProtoEnum(Name="PropertiesId_RBCSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2c1)]
        PropertiesId_RBCSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2c1,
        [ProtoEnum(Name="PropertiesId_RBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2c4)]
        PropertiesId_RBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2c4,
        [ProtoEnum(Name="PropertiesId_RBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2c5)]
        PropertiesId_RBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2c5,
        [ProtoEnum(Name="PropertiesId_RBBSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2c8)]
        PropertiesId_RBBSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2c8,
        [ProtoEnum(Name="PropertiesId_RBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2c9)]
        PropertiesId_RBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2c9,
        [ProtoEnum(Name="PropertiesId_RBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2ca)]
        PropertiesId_RBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2ca,
        [ProtoEnum(Name="PropertiesId_RFGSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x34d)]
        PropertiesId_RFGSuperWeaponEnergyRecoveryModifyMulti_ES = 0x34d,
        [ProtoEnum(Name="PropertiesId_OFFSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2e9)]
        PropertiesId_OFFSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2e9,
        [ProtoEnum(Name="PropertiesId_OFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2ec)]
        PropertiesId_OFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2ec,
        [ProtoEnum(Name="PropertiesId_OFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2ed)]
        PropertiesId_OFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2ed,
        [ProtoEnum(Name="PropertiesId_ODDSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2f0)]
        PropertiesId_ODDSuperWeaponEnergyRecoveryModifyMulti_ES = 0x2f0,
        [ProtoEnum(Name="PropertiesId_ODDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2f1)]
        PropertiesId_ODDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2f1,
        [ProtoEnum(Name="PropertiesId_ODDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2f2)]
        PropertiesId_ODDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2f2,
        [ProtoEnum(Name="PropertiesId_OCASuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x2f3)]
        PropertiesId_OCASuperWeaponEnergyRecoveryModifyMulti_ES = 0x2f3,
        [ProtoEnum(Name="PropertiesId_OCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2f6)]
        PropertiesId_OCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2f6,
        [ProtoEnum(Name="PropertiesId_OCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2f7)]
        PropertiesId_OCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2f7,
        [ProtoEnum(Name="PropertiesId_OBCSuperWeaponEnergyRecoveryModifyMulti_ES", Value=760)]
        PropertiesId_OBCSuperWeaponEnergyRecoveryModifyMulti_ES = 760,
        [ProtoEnum(Name="PropertiesId_OBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x2fc)]
        PropertiesId_OBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x2fc,
        [ProtoEnum(Name="PropertiesId_OBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x2fd)]
        PropertiesId_OBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x2fd,
        [ProtoEnum(Name="PropertiesId_OBBSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x300)]
        PropertiesId_OBBSuperWeaponEnergyRecoveryModifyMulti_ES = 0x300,
        [ProtoEnum(Name="PropertiesId_OBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x301)]
        PropertiesId_OBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x301,
        [ProtoEnum(Name="PropertiesId_OBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x306)]
        PropertiesId_OBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x306,
        [ProtoEnum(Name="PropertiesId_OFGSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x34f)]
        PropertiesId_OFGSuperWeaponEnergyRecoveryModifyMulti_ES = 0x34f,
        [ProtoEnum(Name="PropertiesId_UFFSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x32b)]
        PropertiesId_UFFSuperWeaponEnergyRecoveryModifyMulti_ES = 0x32b,
        [ProtoEnum(Name="PropertiesId_UFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x32e)]
        PropertiesId_UFFSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x32e,
        [ProtoEnum(Name="PropertiesId_UFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x32f)]
        PropertiesId_UFFSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x32f,
        [ProtoEnum(Name="PropertiesId_UDDSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x332)]
        PropertiesId_UDDSuperWeaponEnergyRecoveryModifyMulti_ES = 0x332,
        [ProtoEnum(Name="PropertiesId_UDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x333)]
        PropertiesId_UDDSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x333,
        [ProtoEnum(Name="PropertiesId_UDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=820)]
        PropertiesId_UDDSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 820,
        [ProtoEnum(Name="PropertiesId_UCASuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x335)]
        PropertiesId_UCASuperWeaponEnergyRecoveryModifyMulti_ES = 0x335,
        [ProtoEnum(Name="PropertiesId_UCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x339)]
        PropertiesId_UCASuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x339,
        [ProtoEnum(Name="PropertiesId_UCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x33a)]
        PropertiesId_UCASuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x33a,
        [ProtoEnum(Name="PropertiesId_UBCSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x33b)]
        PropertiesId_UBCSuperWeaponEnergyRecoveryModifyMulti_ES = 0x33b,
        [ProtoEnum(Name="PropertiesId_UBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=0x33f)]
        PropertiesId_UBCSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 0x33f,
        [ProtoEnum(Name="PropertiesId_UBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x344)]
        PropertiesId_UBCSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x344,
        [ProtoEnum(Name="PropertiesId_UBBSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x345)]
        PropertiesId_UBBSuperWeaponEnergyRecoveryModifyMulti_ES = 0x345,
        [ProtoEnum(Name="PropertiesId_UBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES", Value=840)]
        PropertiesId_UBBSuperWeaponEnergyRecoveryOnHitByEnemyMulti_ES = 840,
        [ProtoEnum(Name="PropertiesId_UBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES", Value=0x349)]
        PropertiesId_UBBSuperWeaponEnergyRecoveryOnEnergyUseMulti_ES = 0x349,
        [ProtoEnum(Name="PropertiesId_UFGSuperWeaponEnergyRecoveryModifyMulti_ES", Value=0x358)]
        PropertiesId_UFGSuperWeaponEnergyRecoveryModifyMulti_ES = 0x358,
        [ProtoEnum(Name="PropertiesId_CharactorAttack_Base", Value=0x8e)]
        PropertiesId_CharactorAttack_Base = 0x8e,
        [ProtoEnum(Name="PropertiesId_CharactorAttackGain_Final", Value=0x1b1)]
        PropertiesId_CharactorAttackGain_Final = 0x1b1,
        [ProtoEnum(Name="PropertiesId_CharactorAttackGainRatio_Base", Value=130)]
        PropertiesId_CharactorAttackGainRatio_Base = 130,
        [ProtoEnum(Name="PropertiesId_CharactorAttackGainMinPropertyValue_Base", Value=0x84)]
        PropertiesId_CharactorAttackGainMinPropertyValue_Base = 0x84,
        [ProtoEnum(Name="PropertiesId_CharactorAttackGainMaxPropertyValue_Base", Value=0x83)]
        PropertiesId_CharactorAttackGainMaxPropertyValue_Base = 0x83,
        [ProtoEnum(Name="PropertiesId_CharactorDefence_Base", Value=0x8f)]
        PropertiesId_CharactorDefence_Base = 0x8f,
        [ProtoEnum(Name="PropertiesId_CharactorDefenceGain_Final", Value=0x1b2)]
        PropertiesId_CharactorDefenceGain_Final = 0x1b2,
        [ProtoEnum(Name="PropertiesId_CharactorDefenceGainRatio_Base", Value=0x88)]
        PropertiesId_CharactorDefenceGainRatio_Base = 0x88,
        [ProtoEnum(Name="PropertiesId_CharactorDefenceGainMinPropertyValue_Base", Value=0x8a)]
        PropertiesId_CharactorDefenceGainMinPropertyValue_Base = 0x8a,
        [ProtoEnum(Name="PropertiesId_CharactorDefenceGainMaxPropertyValue_Base", Value=0x89)]
        PropertiesId_CharactorDefenceGainMaxPropertyValue_Base = 0x89,
        [ProtoEnum(Name="PropertiesId_CharactorElectron_Base", Value=0x91)]
        PropertiesId_CharactorElectron_Base = 0x91,
        [ProtoEnum(Name="PropertiesId_CharactorElectronGain_Final", Value=0x1b4)]
        PropertiesId_CharactorElectronGain_Final = 0x1b4,
        [ProtoEnum(Name="PropertiesId_CharactorElectronGainRatio_Base", Value=0x8b)]
        PropertiesId_CharactorElectronGainRatio_Base = 0x8b,
        [ProtoEnum(Name="PropertiesId_CharactorElectronGainMinPropertyValue_Base", Value=0x8d)]
        PropertiesId_CharactorElectronGainMinPropertyValue_Base = 0x8d,
        [ProtoEnum(Name="PropertiesId_CharactorElectronGainMaxPropertyValue_Base", Value=140)]
        PropertiesId_CharactorElectronGainMaxPropertyValue_Base = 140,
        [ProtoEnum(Name="PropertiesId_CharactorDrive_Base", Value=0x90)]
        PropertiesId_CharactorDrive_Base = 0x90,
        [ProtoEnum(Name="PropertiesId_CharactorDriveGain_Final", Value=0x1b3)]
        PropertiesId_CharactorDriveGain_Final = 0x1b3,
        [ProtoEnum(Name="PropertiesId_CharactorDriveGainRatio_Base", Value=0x85)]
        PropertiesId_CharactorDriveGainRatio_Base = 0x85,
        [ProtoEnum(Name="PropertiesId_CharactorDriveGainMinPropertyValue_Base", Value=0x87)]
        PropertiesId_CharactorDriveGainMinPropertyValue_Base = 0x87,
        [ProtoEnum(Name="PropertiesId_CharactorDriveGainMaxPropertyValue_Base", Value=0x86)]
        PropertiesId_CharactorDriveGainMaxPropertyValue_Base = 0x86,
        [ProtoEnum(Name="PropertiesId_TechWeaponTimeMulti_NS", Value=0x524)]
        PropertiesId_TechWeaponTimeMulti_NS = 0x524,
        [ProtoEnum(Name="PropertiesId_TechWeaponMoneyMulti_NS", Value=0x525)]
        PropertiesId_TechWeaponMoneyMulti_NS = 0x525,
        [ProtoEnum(Name="PropertiesId_TechWeaponItemMulti_NS", Value=0x526)]
        PropertiesId_TechWeaponItemMulti_NS = 0x526,
        [ProtoEnum(Name="PropertiesId_TechShipTimeMulti_NS", Value=0x52a)]
        PropertiesId_TechShipTimeMulti_NS = 0x52a,
        [ProtoEnum(Name="PropertiesId_TechShipMoneyMulti_NS", Value=0x52b)]
        PropertiesId_TechShipMoneyMulti_NS = 0x52b,
        [ProtoEnum(Name="PropertiesId_TechShipItemMulti_NS", Value=0x52c)]
        PropertiesId_TechShipItemMulti_NS = 0x52c,
        [ProtoEnum(Name="PropertiesId_TechProduceTimeMulti_NS", Value=0x52d)]
        PropertiesId_TechProduceTimeMulti_NS = 0x52d,
        [ProtoEnum(Name="PropertiesId_TechProduceMoneyMulti_NS", Value=0x52e)]
        PropertiesId_TechProduceMoneyMulti_NS = 0x52e,
        [ProtoEnum(Name="PropertiesId_TechProduceItemMulti_NS", Value=0x52f)]
        PropertiesId_TechProduceItemMulti_NS = 0x52f,
        [ProtoEnum(Name="PropertiesId_TechHiredCaptainTimeMulti_NS", Value=0x533)]
        PropertiesId_TechHiredCaptainTimeMulti_NS = 0x533,
        [ProtoEnum(Name="PropertiesId_TechHiredCaptainMoneyMulti_NS", Value=0x534)]
        PropertiesId_TechHiredCaptainMoneyMulti_NS = 0x534,
        [ProtoEnum(Name="PropertiesId_TechHiredCaptainItemMulti_NS", Value=0x535)]
        PropertiesId_TechHiredCaptainItemMulti_NS = 0x535,
        [ProtoEnum(Name="PropertiesId_TechEngineeringTimeMulti_NS", Value=0x778)]
        PropertiesId_TechEngineeringTimeMulti_NS = 0x778,
        [ProtoEnum(Name="PropertiesId_TechEngineeringMoneyMulti_NS", Value=0x774)]
        PropertiesId_TechEngineeringMoneyMulti_NS = 0x774,
        [ProtoEnum(Name="PropertiesId_TechEngineeringItemMulti_NS", Value=0x776)]
        PropertiesId_TechEngineeringItemMulti_NS = 0x776,
        [ProtoEnum(Name="PropertiesId_TechElectronicsTimeMulti_NS", Value=0x779)]
        PropertiesId_TechElectronicsTimeMulti_NS = 0x779,
        [ProtoEnum(Name="PropertiesId_TechElectronicsMoneyMulti_NS", Value=0x775)]
        PropertiesId_TechElectronicsMoneyMulti_NS = 0x775,
        [ProtoEnum(Name="PropertiesId_TechElectronicsItemMulti_NS", Value=0x777)]
        PropertiesId_TechElectronicsItemMulti_NS = 0x777,
        [ProtoEnum(Name="PropertiesId_ProductionWeaponTimeMulti_NS", Value=0x537)]
        PropertiesId_ProductionWeaponTimeMulti_NS = 0x537,
        [ProtoEnum(Name="PropertiesId_ProductionWeaponMoneyMulti_NS", Value=0x538)]
        PropertiesId_ProductionWeaponMoneyMulti_NS = 0x538,
        [ProtoEnum(Name="PropertiesId_ProductionWeaponItemMulti_NS", Value=0x539)]
        PropertiesId_ProductionWeaponItemMulti_NS = 0x539,
        [ProtoEnum(Name="PropertiesId_ProductionAmmoTimeMulti_NS", Value=0x53a)]
        PropertiesId_ProductionAmmoTimeMulti_NS = 0x53a,
        [ProtoEnum(Name="PropertiesId_ProductionAmmoMoneyMulti_NS", Value=0x53b)]
        PropertiesId_ProductionAmmoMoneyMulti_NS = 0x53b,
        [ProtoEnum(Name="PropertiesId_ProductionAmmoItemMulti_NS", Value=0x53c)]
        PropertiesId_ProductionAmmoItemMulti_NS = 0x53c,
        [ProtoEnum(Name="PropertiesId_ProductionShipTimeMulti_NS", Value=0x53d)]
        PropertiesId_ProductionShipTimeMulti_NS = 0x53d,
        [ProtoEnum(Name="PropertiesId_ProductionShipMoneyMulti_NS", Value=0x53e)]
        PropertiesId_ProductionShipMoneyMulti_NS = 0x53e,
        [ProtoEnum(Name="PropertiesId_ProductionShipItemMulti_NS", Value=0x53f)]
        PropertiesId_ProductionShipItemMulti_NS = 0x53f,
        [ProtoEnum(Name="PropertiesId_ProductionEquipTimeMulti_NS", Value=0x540)]
        PropertiesId_ProductionEquipTimeMulti_NS = 0x540,
        [ProtoEnum(Name="PropertiesId_ProductionEquipMoneyMulti_NS", Value=0x541)]
        PropertiesId_ProductionEquipMoneyMulti_NS = 0x541,
        [ProtoEnum(Name="PropertiesId_ProductionEquipItemMulti_NS", Value=0x542)]
        PropertiesId_ProductionEquipItemMulti_NS = 0x542,
        [ProtoEnum(Name="PropertiesId_DelegateMissionCountAdd_ES", Value=0x585)]
        PropertiesId_DelegateMissionCountAdd_ES = 0x585,
        [ProtoEnum(Name="PropertiesId_DelegateMissionShipCountAdd_ES", Value=0x64a)]
        PropertiesId_DelegateMissionShipCountAdd_ES = 0x64a,
        [ProtoEnum(Name="PropertiesId_DelegateFightSpeedMulti_ES", Value=0x51d)]
        PropertiesId_DelegateFightSpeedMulti_ES = 0x51d,
        [ProtoEnum(Name="PropertiesId_DelegateMineralSpeedMulti_ES", Value=0x51e)]
        PropertiesId_DelegateMineralSpeedMulti_ES = 0x51e,
        [ProtoEnum(Name="PropertiesId_DelegateTradeSpeedMulti_ES", Value=0x51f)]
        PropertiesId_DelegateTradeSpeedMulti_ES = 0x51f,
        [ProtoEnum(Name="PropertiesId_DelegateFightRewardMulti_ES", Value=0x520)]
        PropertiesId_DelegateFightRewardMulti_ES = 0x520,
        [ProtoEnum(Name="PropertiesId_DelegateMineralRewardMulti_ES", Value=0x521)]
        PropertiesId_DelegateMineralRewardMulti_ES = 0x521,
        [ProtoEnum(Name="PropertiesId_DelegateTradeRewardMulti_ES", Value=0x522)]
        PropertiesId_DelegateTradeRewardMulti_ES = 0x522,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberRoundCD_Base", Value=0x587)]
        PropertiesId_WeaponDisturberRoundCD_Base = 0x587,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberRoundCDMulti_ES", Value=0x588)]
        PropertiesId_WeaponDisturberRoundCDMulti_ES = 0x588,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberRoundCDMulti_ED", Value=0x589)]
        PropertiesId_WeaponDisturberRoundCDMulti_ED = 0x589,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberRoundCD_Final", Value=0x58c)]
        PropertiesId_WeaponDisturberRoundCD_Final = 0x58c,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostEnergy_Base", Value=0x58d)]
        PropertiesId_WeaponDisturberCostEnergy_Base = 0x58d,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostEnergy_NS", Value=0x590)]
        PropertiesId_WeaponDisturberCostEnergy_NS = 0x590,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostEnergy_ND", Value=0x591)]
        PropertiesId_WeaponDisturberCostEnergy_ND = 0x591,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostEnergy_Final", Value=0x592)]
        PropertiesId_WeaponDisturberCostEnergy_Final = 0x592,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberPercentageCostEnergy_Base", Value=0x6d0)]
        PropertiesId_WeaponDisturberPercentageCostEnergy_Base = 0x6d0,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberPercentageCostEnergyMulti_ES", Value=0x6d1)]
        PropertiesId_WeaponDisturberPercentageCostEnergyMulti_ES = 0x6d1,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberPercentageCostEnergy_NS", Value=0x6d3)]
        PropertiesId_WeaponDisturberPercentageCostEnergy_NS = 0x6d3,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberPercentageCostEnergy_Final", Value=0x6d5)]
        PropertiesId_WeaponDisturberPercentageCostEnergy_Final = 0x6d5,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostCPU_Base", Value=0x593)]
        PropertiesId_WeaponDisturberCostCPU_Base = 0x593,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostCPU_Final", Value=0x596)]
        PropertiesId_WeaponDisturberCostCPU_Final = 0x596,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostPower_Base", Value=0x597)]
        PropertiesId_WeaponDisturberCostPower_Base = 0x597,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberCostPower_Final", Value=0x59a)]
        PropertiesId_WeaponDisturberCostPower_Final = 0x59a,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberFireRange_Base", Value=0x59b)]
        PropertiesId_WeaponDisturberFireRange_Base = 0x59b,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberFireRangeMulti_ES", Value=0x638)]
        PropertiesId_WeaponDisturberFireRangeMulti_ES = 0x638,
        [ProtoEnum(Name="PropertiesId_WeaponDisturberFireRange_Final", Value=0x5a0)]
        PropertiesId_WeaponDisturberFireRange_Final = 0x5a0,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberRoundCD_Base", Value=0x303)]
        PropertiesId_ShieldDisturberRoundCD_Base = 0x303,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberRoundCDMulti_ES", Value=0x304)]
        PropertiesId_ShieldDisturberRoundCDMulti_ES = 0x304,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberRoundCDMulti_ED", Value=0x305)]
        PropertiesId_ShieldDisturberRoundCDMulti_ED = 0x305,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberRoundCD_Final", Value=0x308)]
        PropertiesId_ShieldDisturberRoundCD_Final = 0x308,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostEnergy_Base", Value=0x309)]
        PropertiesId_ShieldDisturberCostEnergy_Base = 0x309,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostEnergyMulti_ES", Value=0x30a)]
        PropertiesId_ShieldDisturberCostEnergyMulti_ES = 0x30a,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostEnergy_NS", Value=780)]
        PropertiesId_ShieldDisturberCostEnergy_NS = 780,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostEnergy_ND", Value=0x30d)]
        PropertiesId_ShieldDisturberCostEnergy_ND = 0x30d,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostEnergy_Final", Value=0x30e)]
        PropertiesId_ShieldDisturberCostEnergy_Final = 0x30e,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberPercentageCostEnergy_Base", Value=0x6c4)]
        PropertiesId_ShieldDisturberPercentageCostEnergy_Base = 0x6c4,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberPercentageCostEnergyMulti_ES", Value=0x6c5)]
        PropertiesId_ShieldDisturberPercentageCostEnergyMulti_ES = 0x6c5,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberPercentageCostEnergy_NS", Value=0x6c7)]
        PropertiesId_ShieldDisturberPercentageCostEnergy_NS = 0x6c7,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberPercentageCostEnergy_Final", Value=0x6c9)]
        PropertiesId_ShieldDisturberPercentageCostEnergy_Final = 0x6c9,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostCPU_Base", Value=0x30f)]
        PropertiesId_ShieldDisturberCostCPU_Base = 0x30f,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostCPU_Final", Value=0x312)]
        PropertiesId_ShieldDisturberCostCPU_Final = 0x312,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostPower_Base", Value=0x313)]
        PropertiesId_ShieldDisturberCostPower_Base = 0x313,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberCostPower_Final", Value=790)]
        PropertiesId_ShieldDisturberCostPower_Final = 790,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberFireRange_Base", Value=0x317)]
        PropertiesId_ShieldDisturberFireRange_Base = 0x317,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberFireRangeAdd_ES", Value=0x318)]
        PropertiesId_ShieldDisturberFireRangeAdd_ES = 0x318,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberFireRangeMulti_ES", Value=0x634)]
        PropertiesId_ShieldDisturberFireRangeMulti_ES = 0x634,
        [ProtoEnum(Name="PropertiesId_ShieldDisturberFireRange_Final", Value=0x31c)]
        PropertiesId_ShieldDisturberFireRange_Final = 0x31c,
        [ProtoEnum(Name="PropertiesId_SpeedReducerRoundCD_Base", Value=0x409)]
        PropertiesId_SpeedReducerRoundCD_Base = 0x409,
        [ProtoEnum(Name="PropertiesId_SpeedReducerRoundCDMulti_ES", Value=0x40a)]
        PropertiesId_SpeedReducerRoundCDMulti_ES = 0x40a,
        [ProtoEnum(Name="PropertiesId_SpeedReducerRoundCDMulti_ED", Value=0x40b)]
        PropertiesId_SpeedReducerRoundCDMulti_ED = 0x40b,
        [ProtoEnum(Name="PropertiesId_SpeedReducerRoundCD_NS", Value=0x40c)]
        PropertiesId_SpeedReducerRoundCD_NS = 0x40c,
        [ProtoEnum(Name="PropertiesId_SpeedReducerRoundCD_Final", Value=0x40e)]
        PropertiesId_SpeedReducerRoundCD_Final = 0x40e,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostEnergy_Base", Value=0x40f)]
        PropertiesId_SpeedReducerCostEnergy_Base = 0x40f,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostEnergyMulti_ES", Value=0x410)]
        PropertiesId_SpeedReducerCostEnergyMulti_ES = 0x410,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostEnergy_NS", Value=0x412)]
        PropertiesId_SpeedReducerCostEnergy_NS = 0x412,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostEnergy_ND", Value=0x413)]
        PropertiesId_SpeedReducerCostEnergy_ND = 0x413,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostEnergy_Final", Value=0x414)]
        PropertiesId_SpeedReducerCostEnergy_Final = 0x414,
        [ProtoEnum(Name="PropertiesId_SpeedReducerPercentageCostEnergy_Base", Value=0x6d6)]
        PropertiesId_SpeedReducerPercentageCostEnergy_Base = 0x6d6,
        [ProtoEnum(Name="PropertiesId_SpeedReducerPercentageCostEnergyMulti_ES", Value=0x6d7)]
        PropertiesId_SpeedReducerPercentageCostEnergyMulti_ES = 0x6d7,
        [ProtoEnum(Name="PropertiesId_SpeedReducerPercentageCostEnergy_NS", Value=0x6d9)]
        PropertiesId_SpeedReducerPercentageCostEnergy_NS = 0x6d9,
        [ProtoEnum(Name="PropertiesId_SpeedReducerPercentageCostEnergy_Final", Value=0x6db)]
        PropertiesId_SpeedReducerPercentageCostEnergy_Final = 0x6db,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostCPU_Base", Value=0x415)]
        PropertiesId_SpeedReducerCostCPU_Base = 0x415,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostCPU_Final", Value=0x418)]
        PropertiesId_SpeedReducerCostCPU_Final = 0x418,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostPower_Base", Value=0x419)]
        PropertiesId_SpeedReducerCostPower_Base = 0x419,
        [ProtoEnum(Name="PropertiesId_SpeedReducerCostPower_Final", Value=0x41c)]
        PropertiesId_SpeedReducerCostPower_Final = 0x41c,
        [ProtoEnum(Name="PropertiesId_SpeedReducerFireRange_Base", Value=0x41d)]
        PropertiesId_SpeedReducerFireRange_Base = 0x41d,
        [ProtoEnum(Name="PropertiesId_SpeedReducerFireRangeAdd_ES", Value=0x41e)]
        PropertiesId_SpeedReducerFireRangeAdd_ES = 0x41e,
        [ProtoEnum(Name="PropertiesId_SpeedReducerFireRangeMulti_ES", Value=0x63a)]
        PropertiesId_SpeedReducerFireRangeMulti_ES = 0x63a,
        [ProtoEnum(Name="PropertiesId_SpeedReducerFireRange_Final", Value=0x422)]
        PropertiesId_SpeedReducerFireRange_Final = 0x422,
        [ProtoEnum(Name="PropertiesId_SpeedReducerSpeedReduceParam_Base", Value=0x423)]
        PropertiesId_SpeedReducerSpeedReduceParam_Base = 0x423,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstRoundCD_Base", Value=0x4e9)]
        PropertiesId_TeamElecAsstRoundCD_Base = 0x4e9,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstRoundCDMulti_ES", Value=0x4ea)]
        PropertiesId_TeamElecAsstRoundCDMulti_ES = 0x4ea,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstRoundCDMulti_ED", Value=0x4eb)]
        PropertiesId_TeamElecAsstRoundCDMulti_ED = 0x4eb,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstRoundCD_Final", Value=0x4ee)]
        PropertiesId_TeamElecAsstRoundCD_Final = 0x4ee,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostEnergy_Base", Value=0x4ef)]
        PropertiesId_TeamElecAsstCostEnergy_Base = 0x4ef,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostEnergyMulti_ES", Value=0x4f0)]
        PropertiesId_TeamElecAsstCostEnergyMulti_ES = 0x4f0,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostEnergy_NS", Value=0x4f2)]
        PropertiesId_TeamElecAsstCostEnergy_NS = 0x4f2,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostEnergy_ND", Value=0x4f3)]
        PropertiesId_TeamElecAsstCostEnergy_ND = 0x4f3,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostEnergy_Final", Value=0x4f4)]
        PropertiesId_TeamElecAsstCostEnergy_Final = 0x4f4,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstPercentageCostEnergy_Base", Value=0x6ee)]
        PropertiesId_TeamElecAsstPercentageCostEnergy_Base = 0x6ee,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstPercentageCostEnergyMulti_ES", Value=0x6ef)]
        PropertiesId_TeamElecAsstPercentageCostEnergyMulti_ES = 0x6ef,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstPercentageCostEnergy_NS", Value=0x6f1)]
        PropertiesId_TeamElecAsstPercentageCostEnergy_NS = 0x6f1,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstPercentageCostEnergy_Final", Value=0x6f3)]
        PropertiesId_TeamElecAsstPercentageCostEnergy_Final = 0x6f3,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostCPU_Base", Value=0x4f5)]
        PropertiesId_TeamElecAsstCostCPU_Base = 0x4f5,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostCPU_Final", Value=0x4f8)]
        PropertiesId_TeamElecAsstCostCPU_Final = 0x4f8,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostPower_Base", Value=0x4f9)]
        PropertiesId_TeamElecAsstCostPower_Base = 0x4f9,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstCostPower_Final", Value=0x4fc)]
        PropertiesId_TeamElecAsstCostPower_Final = 0x4fc,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstFireRange_Base", Value=0x4fd)]
        PropertiesId_TeamElecAsstFireRange_Base = 0x4fd,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstFireRangeMulti_ES", Value=0x646)]
        PropertiesId_TeamElecAsstFireRangeMulti_ES = 0x646,
        [ProtoEnum(Name="PropertiesId_TeamElecAsstFireRange_Final", Value=0x502)]
        PropertiesId_TeamElecAsstFireRange_Final = 0x502,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerRoundCD_Base", Value=0x268)]
        PropertiesId_ArmorRepairerRoundCD_Base = 0x268,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerRoundCD_NS", Value=0x26b)]
        PropertiesId_ArmorRepairerRoundCD_NS = 0x26b,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerRoundCD_Final", Value=0x26d)]
        PropertiesId_ArmorRepairerRoundCD_Final = 0x26d,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostEnergy_Base", Value=0x26e)]
        PropertiesId_ArmorRepairerCostEnergy_Base = 0x26e,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostEnergy_ND", Value=0x272)]
        PropertiesId_ArmorRepairerCostEnergy_ND = 0x272,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostEnergy_Final", Value=0x273)]
        PropertiesId_ArmorRepairerCostEnergy_Final = 0x273,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostCPU_Base", Value=0x274)]
        PropertiesId_ArmorRepairerCostCPU_Base = 0x274,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostCPU_Final", Value=0x277)]
        PropertiesId_ArmorRepairerCostCPU_Final = 0x277,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostPower_Base", Value=0x278)]
        PropertiesId_ArmorRepairerCostPower_Base = 0x278,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerCostPower_Final", Value=0x27b)]
        PropertiesId_ArmorRepairerCostPower_Final = 0x27b,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerOnceRepairAmount_Base", Value=0x27c)]
        PropertiesId_ArmorRepairerOnceRepairAmount_Base = 0x27c,
        [ProtoEnum(Name="PropertiesId_ArmorRepairerOnceRepairAmount_Final", Value=640)]
        PropertiesId_ArmorRepairerOnceRepairAmount_Final = 640,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerRoundCD_Base", Value=0x281)]
        PropertiesId_FlashShieldRepairerRoundCD_Base = 0x281,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerRoundCDMulti_ES", Value=0x282)]
        PropertiesId_FlashShieldRepairerRoundCDMulti_ES = 0x282,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerRoundCD_NS", Value=0x284)]
        PropertiesId_FlashShieldRepairerRoundCD_NS = 0x284,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerRoundCD_Final", Value=0x286)]
        PropertiesId_FlashShieldRepairerRoundCD_Final = 0x286,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostEnergy_Base", Value=0x287)]
        PropertiesId_FlashShieldRepairerCostEnergy_Base = 0x287,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostEnergyMulti_ES", Value=0x288)]
        PropertiesId_FlashShieldRepairerCostEnergyMulti_ES = 0x288,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostEnergy_NS", Value=650)]
        PropertiesId_FlashShieldRepairerCostEnergy_NS = 650,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostEnergy_ND", Value=0x28b)]
        PropertiesId_FlashShieldRepairerCostEnergy_ND = 0x28b,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostEnergy_Final", Value=0x28c)]
        PropertiesId_FlashShieldRepairerCostEnergy_Final = 0x28c,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostCPU_Base", Value=0x28d)]
        PropertiesId_FlashShieldRepairerCostCPU_Base = 0x28d,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostCPU_Final", Value=0x290)]
        PropertiesId_FlashShieldRepairerCostCPU_Final = 0x290,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostPower_Base", Value=0x291)]
        PropertiesId_FlashShieldRepairerCostPower_Base = 0x291,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerCostPower_Final", Value=660)]
        PropertiesId_FlashShieldRepairerCostPower_Final = 660,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerOnceRepairAmount_Base", Value=0x295)]
        PropertiesId_FlashShieldRepairerOnceRepairAmount_Base = 0x295,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerOnceRepairAmountMulti_ES", Value=0x296)]
        PropertiesId_FlashShieldRepairerOnceRepairAmountMulti_ES = 0x296,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerOnceRepairAmountMulti_ED", Value=0x6ac)]
        PropertiesId_FlashShieldRepairerOnceRepairAmountMulti_ED = 0x6ac,
        [ProtoEnum(Name="PropertiesId_FlashShieldRepairerOnceRepairAmount_Final", Value=0x299)]
        PropertiesId_FlashShieldRepairerOnceRepairAmount_Final = 0x299,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerRoundCD_Base", Value=0x29a)]
        PropertiesId_FlashArmorRepairerRoundCD_Base = 0x29a,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerRoundCD_Final", Value=0x29f)]
        PropertiesId_FlashArmorRepairerRoundCD_Final = 0x29f,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostEnergy_Base", Value=0x2a0)]
        PropertiesId_FlashArmorRepairerCostEnergy_Base = 0x2a0,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostEnergy_ND", Value=0x2a4)]
        PropertiesId_FlashArmorRepairerCostEnergy_ND = 0x2a4,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostEnergy_Final", Value=0x2a5)]
        PropertiesId_FlashArmorRepairerCostEnergy_Final = 0x2a5,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostCPU_Base", Value=0x2a6)]
        PropertiesId_FlashArmorRepairerCostCPU_Base = 0x2a6,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostCPU_Final", Value=0x2a9)]
        PropertiesId_FlashArmorRepairerCostCPU_Final = 0x2a9,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostPower_Base", Value=0x2aa)]
        PropertiesId_FlashArmorRepairerCostPower_Base = 0x2aa,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerCostPower_Final", Value=0x2ad)]
        PropertiesId_FlashArmorRepairerCostPower_Final = 0x2ad,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerOnceRepairAmount_Base", Value=0x2ae)]
        PropertiesId_FlashArmorRepairerOnceRepairAmount_Base = 0x2ae,
        [ProtoEnum(Name="PropertiesId_FlashArmorRepairerOnceRepairAmount_Final", Value=690)]
        PropertiesId_FlashArmorRepairerOnceRepairAmount_Final = 690,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerRoundCD_Base", Value=0x495)]
        PropertiesId_RemoteArmorRepairerRoundCD_Base = 0x495,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerRoundCD_NS", Value=0x498)]
        PropertiesId_RemoteArmorRepairerRoundCD_NS = 0x498,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerRoundCD_Final", Value=0x49a)]
        PropertiesId_RemoteArmorRepairerRoundCD_Final = 0x49a,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostEnergy_Base", Value=0x49b)]
        PropertiesId_RemoteArmorRepairerCostEnergy_Base = 0x49b,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostEnergy_ND", Value=0x49f)]
        PropertiesId_RemoteArmorRepairerCostEnergy_ND = 0x49f,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostEnergy_Final", Value=0x4a0)]
        PropertiesId_RemoteArmorRepairerCostEnergy_Final = 0x4a0,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostCPU_Base", Value=0x4a1)]
        PropertiesId_RemoteArmorRepairerCostCPU_Base = 0x4a1,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostCPU_Final", Value=0x4a4)]
        PropertiesId_RemoteArmorRepairerCostCPU_Final = 0x4a4,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostPower_Base", Value=0x4a5)]
        PropertiesId_RemoteArmorRepairerCostPower_Base = 0x4a5,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerCostPower_Final", Value=0x4a8)]
        PropertiesId_RemoteArmorRepairerCostPower_Final = 0x4a8,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerFireRange_Base", Value=0x4a9)]
        PropertiesId_RemoteArmorRepairerFireRange_Base = 0x4a9,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerFireRange_Final", Value=0x4ae)]
        PropertiesId_RemoteArmorRepairerFireRange_Final = 0x4ae,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerOnceRepairAmount_Base", Value=0x4af)]
        PropertiesId_RemoteArmorRepairerOnceRepairAmount_Base = 0x4af,
        [ProtoEnum(Name="PropertiesId_RemoteArmorRepairerOnceRepairAmount_Final", Value=0x4b3)]
        PropertiesId_RemoteArmorRepairerOnceRepairAmount_Final = 0x4b3,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistRoundCD_Base", Value=0x2db)]
        PropertiesId_ActiveShieldResistRoundCD_Base = 0x2db,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistRoundCD_Final", Value=0x2e0)]
        PropertiesId_ActiveShieldResistRoundCD_Final = 0x2e0,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostEnergy_Base", Value=0x2e1)]
        PropertiesId_ActiveShieldResistCostEnergy_Base = 0x2e1,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostEnergy_NS", Value=740)]
        PropertiesId_ActiveShieldResistCostEnergy_NS = 740,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostEnergy_ND", Value=0x2e5)]
        PropertiesId_ActiveShieldResistCostEnergy_ND = 0x2e5,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostEnergy_Final", Value=0x2e6)]
        PropertiesId_ActiveShieldResistCostEnergy_Final = 0x2e6,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostCPU_Base", Value=0x2e7)]
        PropertiesId_ActiveShieldResistCostCPU_Base = 0x2e7,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostCPU_Final", Value=0x2ea)]
        PropertiesId_ActiveShieldResistCostCPU_Final = 0x2ea,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostPower_Base", Value=0x2eb)]
        PropertiesId_ActiveShieldResistCostPower_Base = 0x2eb,
        [ProtoEnum(Name="PropertiesId_ActiveShieldResistCostPower_Final", Value=750)]
        PropertiesId_ActiveShieldResistCostPower_Final = 750,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistRoundCD_Base", Value=0x2ef)]
        PropertiesId_ActiveArmorResistRoundCD_Base = 0x2ef,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistRoundCD_Final", Value=0x2f4)]
        PropertiesId_ActiveArmorResistRoundCD_Final = 0x2f4,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostEnergy_Base", Value=0x2f5)]
        PropertiesId_ActiveArmorResistCostEnergy_Base = 0x2f5,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostEnergy_ND", Value=0x2f9)]
        PropertiesId_ActiveArmorResistCostEnergy_ND = 0x2f9,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostEnergy_Final", Value=0x2fa)]
        PropertiesId_ActiveArmorResistCostEnergy_Final = 0x2fa,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostCPU_Base", Value=0x2fb)]
        PropertiesId_ActiveArmorResistCostCPU_Base = 0x2fb,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostCPU_Final", Value=0x2fe)]
        PropertiesId_ActiveArmorResistCostCPU_Final = 0x2fe,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostPower_Base", Value=0x2ff)]
        PropertiesId_ActiveArmorResistCostPower_Base = 0x2ff,
        [ProtoEnum(Name="PropertiesId_ActiveArmorResistCostPower_Final", Value=770)]
        PropertiesId_ActiveArmorResistCostPower_Final = 770,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageRoundCD_Base", Value=0x2b3)]
        PropertiesId_ExtraShieldPackageRoundCD_Base = 0x2b3,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageRoundCD_Final", Value=0x2b8)]
        PropertiesId_ExtraShieldPackageRoundCD_Final = 0x2b8,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostEnergy_Base", Value=0x2b9)]
        PropertiesId_ExtraShieldPackageCostEnergy_Base = 0x2b9,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostEnergy_NS", Value=700)]
        PropertiesId_ExtraShieldPackageCostEnergy_NS = 700,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostEnergy_ND", Value=0x2bd)]
        PropertiesId_ExtraShieldPackageCostEnergy_ND = 0x2bd,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostEnergy_Final", Value=0x2be)]
        PropertiesId_ExtraShieldPackageCostEnergy_Final = 0x2be,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostCPU_Base", Value=0x2bf)]
        PropertiesId_ExtraShieldPackageCostCPU_Base = 0x2bf,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostCPU_Final", Value=0x2c2)]
        PropertiesId_ExtraShieldPackageCostCPU_Final = 0x2c2,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostPower_Base", Value=0x2c3)]
        PropertiesId_ExtraShieldPackageCostPower_Base = 0x2c3,
        [ProtoEnum(Name="PropertiesId_ExtraShieldPackageCostPower_Final", Value=710)]
        PropertiesId_ExtraShieldPackageCostPower_Final = 710,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageRoundCD_Base", Value=0x2c7)]
        PropertiesId_ExtraArmorPackageRoundCD_Base = 0x2c7,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageRoundCD_Final", Value=0x2cc)]
        PropertiesId_ExtraArmorPackageRoundCD_Final = 0x2cc,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostEnergy_Base", Value=0x2cd)]
        PropertiesId_ExtraArmorPackageCostEnergy_Base = 0x2cd,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostEnergy_ND", Value=0x2d1)]
        PropertiesId_ExtraArmorPackageCostEnergy_ND = 0x2d1,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostEnergy_Final", Value=0x2d2)]
        PropertiesId_ExtraArmorPackageCostEnergy_Final = 0x2d2,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostCPU_Base", Value=0x2d3)]
        PropertiesId_ExtraArmorPackageCostCPU_Base = 0x2d3,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostCPU_Final", Value=0x2d6)]
        PropertiesId_ExtraArmorPackageCostCPU_Final = 0x2d6,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostPower_Base", Value=0x2d7)]
        PropertiesId_ExtraArmorPackageCostPower_Base = 0x2d7,
        [ProtoEnum(Name="PropertiesId_ExtraArmorPackageCostPower_Final", Value=730)]
        PropertiesId_ExtraArmorPackageCostPower_Final = 730,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterRoundCD_Base", Value=0x373)]
        PropertiesId_WeaponBoosterRoundCD_Base = 0x373,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterRoundCD_Final", Value=0x378)]
        PropertiesId_WeaponBoosterRoundCD_Final = 0x378,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostEnergy_Base", Value=0x379)]
        PropertiesId_WeaponBoosterCostEnergy_Base = 0x379,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostEnergy_NS", Value=0x37c)]
        PropertiesId_WeaponBoosterCostEnergy_NS = 0x37c,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostEnergy_ND", Value=0x37d)]
        PropertiesId_WeaponBoosterCostEnergy_ND = 0x37d,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostEnergy_Final", Value=0x37e)]
        PropertiesId_WeaponBoosterCostEnergy_Final = 0x37e,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostCPU_Base", Value=0x37f)]
        PropertiesId_WeaponBoosterCostCPU_Base = 0x37f,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostCPU_Final", Value=0x382)]
        PropertiesId_WeaponBoosterCostCPU_Final = 0x382,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostPower_Base", Value=0x383)]
        PropertiesId_WeaponBoosterCostPower_Base = 0x383,
        [ProtoEnum(Name="PropertiesId_WeaponBoosterCostPower_Final", Value=0x386)]
        PropertiesId_WeaponBoosterCostPower_Final = 0x386,
        [ProtoEnum(Name="PropertiesId_RangeExtenderRoundCD_Base", Value=0x34b)]
        PropertiesId_RangeExtenderRoundCD_Base = 0x34b,
        [ProtoEnum(Name="PropertiesId_RangeExtenderRoundCDMulti_ES", Value=0x34c)]
        PropertiesId_RangeExtenderRoundCDMulti_ES = 0x34c,
        [ProtoEnum(Name="PropertiesId_RangeExtenderRoundCD_Final", Value=0x350)]
        PropertiesId_RangeExtenderRoundCD_Final = 0x350,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostEnergy_Base", Value=0x351)]
        PropertiesId_RangeExtenderCostEnergy_Base = 0x351,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostEnergyMulti_ES", Value=850)]
        PropertiesId_RangeExtenderCostEnergyMulti_ES = 850,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostEnergy_NS", Value=0x354)]
        PropertiesId_RangeExtenderCostEnergy_NS = 0x354,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostEnergy_ND", Value=0x355)]
        PropertiesId_RangeExtenderCostEnergy_ND = 0x355,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostEnergy_Final", Value=0x356)]
        PropertiesId_RangeExtenderCostEnergy_Final = 0x356,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostCPU_Base", Value=0x357)]
        PropertiesId_RangeExtenderCostCPU_Base = 0x357,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostCPU_Final", Value=0x35a)]
        PropertiesId_RangeExtenderCostCPU_Final = 0x35a,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostPower_Base", Value=0x35b)]
        PropertiesId_RangeExtenderCostPower_Base = 0x35b,
        [ProtoEnum(Name="PropertiesId_RangeExtenderCostPower_Final", Value=0x35e)]
        PropertiesId_RangeExtenderCostPower_Final = 0x35e,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysRoundCD_Base", Value=0x35f)]
        PropertiesId_AsstFireControlSysRoundCD_Base = 0x35f,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysRoundCD_Final", Value=0x364)]
        PropertiesId_AsstFireControlSysRoundCD_Final = 0x364,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostEnergy_Base", Value=0x365)]
        PropertiesId_AsstFireControlSysCostEnergy_Base = 0x365,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostEnergy_NS", Value=0x368)]
        PropertiesId_AsstFireControlSysCostEnergy_NS = 0x368,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostEnergy_ND", Value=0x369)]
        PropertiesId_AsstFireControlSysCostEnergy_ND = 0x369,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostEnergy_Final", Value=0x36a)]
        PropertiesId_AsstFireControlSysCostEnergy_Final = 0x36a,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostCPU_Base", Value=0x36b)]
        PropertiesId_AsstFireControlSysCostCPU_Base = 0x36b,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostCPU_Final", Value=0x36e)]
        PropertiesId_AsstFireControlSysCostCPU_Final = 0x36e,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostPower_Base", Value=0x36f)]
        PropertiesId_AsstFireControlSysCostPower_Base = 0x36f,
        [ProtoEnum(Name="PropertiesId_AsstFireControlSysCostPower_Final", Value=0x372)]
        PropertiesId_AsstFireControlSysCostPower_Final = 0x372,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCD_Base", Value=0x23b)]
        PropertiesId_ForceCDSysRoundCD_Base = 0x23b,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCDMulti_ES", Value=0x23c)]
        PropertiesId_ForceCDSysRoundCDMulti_ES = 0x23c,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCDMulti_ED", Value=0x23d)]
        PropertiesId_ForceCDSysRoundCDMulti_ED = 0x23d,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCD_NS", Value=0x23e)]
        PropertiesId_ForceCDSysRoundCD_NS = 0x23e,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCD_ND", Value=0x23f)]
        PropertiesId_ForceCDSysRoundCD_ND = 0x23f,
        [ProtoEnum(Name="PropertiesId_ForceCDSysRoundCD_Final", Value=0x240)]
        PropertiesId_ForceCDSysRoundCD_Final = 0x240,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergy_Base", Value=0x241)]
        PropertiesId_ForceCDSysCostEnergy_Base = 0x241,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergyMulti_ES", Value=0x242)]
        PropertiesId_ForceCDSysCostEnergyMulti_ES = 0x242,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergyMulti_ED", Value=0x243)]
        PropertiesId_ForceCDSysCostEnergyMulti_ED = 0x243,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergy_NS", Value=580)]
        PropertiesId_ForceCDSysCostEnergy_NS = 580,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergy_ND", Value=0x245)]
        PropertiesId_ForceCDSysCostEnergy_ND = 0x245,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostEnergy_Final", Value=0x246)]
        PropertiesId_ForceCDSysCostEnergy_Final = 0x246,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostCPU_Base", Value=0x247)]
        PropertiesId_ForceCDSysCostCPU_Base = 0x247,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostCPUMulti_ES", Value=0x248)]
        PropertiesId_ForceCDSysCostCPUMulti_ES = 0x248,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostCPU_NS", Value=0x249)]
        PropertiesId_ForceCDSysCostCPU_NS = 0x249,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostCPU_Final", Value=0x24a)]
        PropertiesId_ForceCDSysCostCPU_Final = 0x24a,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostPower_Base", Value=0x24b)]
        PropertiesId_ForceCDSysCostPower_Base = 0x24b,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostPowerMulti_ES", Value=0x24c)]
        PropertiesId_ForceCDSysCostPowerMulti_ES = 0x24c,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostPower_NS", Value=0x24d)]
        PropertiesId_ForceCDSysCostPower_NS = 0x24d,
        [ProtoEnum(Name="PropertiesId_ForceCDSysCostPower_Final", Value=590)]
        PropertiesId_ForceCDSysCostPower_Final = 590,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberRoundCD_Base", Value=0x31d)]
        PropertiesId_ArmorDisturberRoundCD_Base = 0x31d,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberRoundCDMulti_ES", Value=0x31e)]
        PropertiesId_ArmorDisturberRoundCDMulti_ES = 0x31e,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberRoundCD_Final", Value=0x322)]
        PropertiesId_ArmorDisturberRoundCD_Final = 0x322,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostEnergy_Base", Value=0x323)]
        PropertiesId_ArmorDisturberCostEnergy_Base = 0x323,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostEnergy_NS", Value=0x326)]
        PropertiesId_ArmorDisturberCostEnergy_NS = 0x326,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostEnergy_ND", Value=0x327)]
        PropertiesId_ArmorDisturberCostEnergy_ND = 0x327,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostEnergy_Final", Value=0x328)]
        PropertiesId_ArmorDisturberCostEnergy_Final = 0x328,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostCPU_Base", Value=0x329)]
        PropertiesId_ArmorDisturberCostCPU_Base = 0x329,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostCPU_Final", Value=0x32c)]
        PropertiesId_ArmorDisturberCostCPU_Final = 0x32c,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostPower_Base", Value=0x32d)]
        PropertiesId_ArmorDisturberCostPower_Base = 0x32d,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberCostPower_Final", Value=0x330)]
        PropertiesId_ArmorDisturberCostPower_Final = 0x330,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberFireRange_Base", Value=0x331)]
        PropertiesId_ArmorDisturberFireRange_Base = 0x331,
        [ProtoEnum(Name="PropertiesId_ArmorDisturberFireRange_Final", Value=0x336)]
        PropertiesId_ArmorDisturberFireRange_Final = 0x336,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberRoundCD_Base", Value=0x3d5)]
        PropertiesId_DefenceDisturberRoundCD_Base = 0x3d5,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberRoundCDMulti_ES", Value=0x3d6)]
        PropertiesId_DefenceDisturberRoundCDMulti_ES = 0x3d6,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberRoundCD_Final", Value=0x3da)]
        PropertiesId_DefenceDisturberRoundCD_Final = 0x3da,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostEnergy_Base", Value=0x3db)]
        PropertiesId_DefenceDisturberCostEnergy_Base = 0x3db,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostEnergy_NS", Value=990)]
        PropertiesId_DefenceDisturberCostEnergy_NS = 990,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostEnergy_ND", Value=0x3df)]
        PropertiesId_DefenceDisturberCostEnergy_ND = 0x3df,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostEnergy_Final", Value=0x3e0)]
        PropertiesId_DefenceDisturberCostEnergy_Final = 0x3e0,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostCPU_Base", Value=0x3e1)]
        PropertiesId_DefenceDisturberCostCPU_Base = 0x3e1,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostCPU_Final", Value=0x3e4)]
        PropertiesId_DefenceDisturberCostCPU_Final = 0x3e4,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostPower_Base", Value=0x3e5)]
        PropertiesId_DefenceDisturberCostPower_Base = 0x3e5,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberCostPower_Final", Value=0x3e8)]
        PropertiesId_DefenceDisturberCostPower_Final = 0x3e8,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberFireRange_Base", Value=0x3e9)]
        PropertiesId_DefenceDisturberFireRange_Base = 0x3e9,
        [ProtoEnum(Name="PropertiesId_DefenceDisturberFireRange_Final", Value=0x3ee)]
        PropertiesId_DefenceDisturberFireRange_Final = 0x3ee,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberRoundCD_Base", Value=0x337)]
        PropertiesId_PhaseDisturberRoundCD_Base = 0x337,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberRoundCDMulti_ES", Value=0x338)]
        PropertiesId_PhaseDisturberRoundCDMulti_ES = 0x338,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberRoundCD_Final", Value=0x33c)]
        PropertiesId_PhaseDisturberRoundCD_Final = 0x33c,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostEnergy_Base", Value=0x33d)]
        PropertiesId_PhaseDisturberCostEnergy_Base = 0x33d,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostEnergyMulti_ES", Value=830)]
        PropertiesId_PhaseDisturberCostEnergyMulti_ES = 830,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostEnergy_NS", Value=0x340)]
        PropertiesId_PhaseDisturberCostEnergy_NS = 0x340,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostEnergy_ND", Value=0x341)]
        PropertiesId_PhaseDisturberCostEnergy_ND = 0x341,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostEnergy_Final", Value=0x342)]
        PropertiesId_PhaseDisturberCostEnergy_Final = 0x342,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostCPU_Base", Value=0x343)]
        PropertiesId_PhaseDisturberCostCPU_Base = 0x343,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostCPU_Final", Value=0x346)]
        PropertiesId_PhaseDisturberCostCPU_Final = 0x346,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostPower_Base", Value=0x347)]
        PropertiesId_PhaseDisturberCostPower_Base = 0x347,
        [ProtoEnum(Name="PropertiesId_PhaseDisturberCostPower_Final", Value=0x34a)]
        PropertiesId_PhaseDisturberCostPower_Final = 0x34a,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberRoundCD_Base", Value=0x3a1)]
        PropertiesId_FireControlDisturberRoundCD_Base = 0x3a1,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberRoundCDMulti_ES", Value=930)]
        PropertiesId_FireControlDisturberRoundCDMulti_ES = 930,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberRoundCD_Final", Value=0x3a6)]
        PropertiesId_FireControlDisturberRoundCD_Final = 0x3a6,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostEnergy_Base", Value=0x3a7)]
        PropertiesId_FireControlDisturberCostEnergy_Base = 0x3a7,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostEnergyMulti_ES", Value=0x3a8)]
        PropertiesId_FireControlDisturberCostEnergyMulti_ES = 0x3a8,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostEnergy_NS", Value=0x3aa)]
        PropertiesId_FireControlDisturberCostEnergy_NS = 0x3aa,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostEnergy_ND", Value=0x3ab)]
        PropertiesId_FireControlDisturberCostEnergy_ND = 0x3ab,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostEnergy_Final", Value=940)]
        PropertiesId_FireControlDisturberCostEnergy_Final = 940,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostCPU_Base", Value=0x3ad)]
        PropertiesId_FireControlDisturberCostCPU_Base = 0x3ad,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostCPU_Final", Value=0x3b0)]
        PropertiesId_FireControlDisturberCostCPU_Final = 0x3b0,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostPower_Base", Value=0x3b1)]
        PropertiesId_FireControlDisturberCostPower_Base = 0x3b1,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberCostPower_Final", Value=0x3b4)]
        PropertiesId_FireControlDisturberCostPower_Final = 0x3b4,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberFireRange_Base", Value=0x3b5)]
        PropertiesId_FireControlDisturberFireRange_Base = 0x3b5,
        [ProtoEnum(Name="PropertiesId_FireControlDisturberFireRange_Final", Value=0x3ba)]
        PropertiesId_FireControlDisturberFireRange_Final = 0x3ba,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderRoundCD_Base", Value=0x3bb)]
        PropertiesId_FireControlRetarderRoundCD_Base = 0x3bb,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderRoundCDMulti_ES", Value=0x3bc)]
        PropertiesId_FireControlRetarderRoundCDMulti_ES = 0x3bc,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderRoundCD_Final", Value=960)]
        PropertiesId_FireControlRetarderRoundCD_Final = 960,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostEnergy_Base", Value=0x3c1)]
        PropertiesId_FireControlRetarderCostEnergy_Base = 0x3c1,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostEnergyMulti_ES", Value=0x3c2)]
        PropertiesId_FireControlRetarderCostEnergyMulti_ES = 0x3c2,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostEnergy_NS", Value=0x3c4)]
        PropertiesId_FireControlRetarderCostEnergy_NS = 0x3c4,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostEnergy_ND", Value=0x3c5)]
        PropertiesId_FireControlRetarderCostEnergy_ND = 0x3c5,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostEnergy_Final", Value=0x3c6)]
        PropertiesId_FireControlRetarderCostEnergy_Final = 0x3c6,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostCPU_Base", Value=0x3c7)]
        PropertiesId_FireControlRetarderCostCPU_Base = 0x3c7,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostCPU_Final", Value=970)]
        PropertiesId_FireControlRetarderCostCPU_Final = 970,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostPower_Base", Value=0x3cb)]
        PropertiesId_FireControlRetarderCostPower_Base = 0x3cb,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderCostPower_Final", Value=0x3ce)]
        PropertiesId_FireControlRetarderCostPower_Final = 0x3ce,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderFireRange_Base", Value=0x3cf)]
        PropertiesId_FireControlRetarderFireRange_Base = 0x3cf,
        [ProtoEnum(Name="PropertiesId_FireControlRetarderFireRange_Final", Value=980)]
        PropertiesId_FireControlRetarderFireRange_Final = 980,
        [ProtoEnum(Name="PropertiesId_JumpDisturberRoundCD_Base", Value=0x424)]
        PropertiesId_JumpDisturberRoundCD_Base = 0x424,
        [ProtoEnum(Name="PropertiesId_JumpDisturberRoundCDMulti_ES", Value=0x425)]
        PropertiesId_JumpDisturberRoundCDMulti_ES = 0x425,
        [ProtoEnum(Name="PropertiesId_JumpDisturberRoundCD_Final", Value=0x429)]
        PropertiesId_JumpDisturberRoundCD_Final = 0x429,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostEnergy_Base", Value=0x42a)]
        PropertiesId_JumpDisturberCostEnergy_Base = 0x42a,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostEnergy_NS", Value=0x42d)]
        PropertiesId_JumpDisturberCostEnergy_NS = 0x42d,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostEnergy_ND", Value=0x42e)]
        PropertiesId_JumpDisturberCostEnergy_ND = 0x42e,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostEnergy_Final", Value=0x42f)]
        PropertiesId_JumpDisturberCostEnergy_Final = 0x42f,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostCPU_Base", Value=0x430)]
        PropertiesId_JumpDisturberCostCPU_Base = 0x430,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostCPU_Final", Value=0x433)]
        PropertiesId_JumpDisturberCostCPU_Final = 0x433,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostPower_Base", Value=0x434)]
        PropertiesId_JumpDisturberCostPower_Base = 0x434,
        [ProtoEnum(Name="PropertiesId_JumpDisturberCostPower_Final", Value=0x437)]
        PropertiesId_JumpDisturberCostPower_Final = 0x437,
        [ProtoEnum(Name="PropertiesId_JumpDisturberFireRange_Base", Value=0x438)]
        PropertiesId_JumpDisturberFireRange_Base = 0x438,
        [ProtoEnum(Name="PropertiesId_JumpDisturberFireRange_Final", Value=0x43d)]
        PropertiesId_JumpDisturberFireRange_Final = 0x43d,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierRoundCD_Base", Value=0x3ef)]
        PropertiesId_TargetSingleAmplifierRoundCD_Base = 0x3ef,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierRoundCDMulti_ES", Value=0x3f0)]
        PropertiesId_TargetSingleAmplifierRoundCDMulti_ES = 0x3f0,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierRoundCD_Final", Value=0x3f4)]
        PropertiesId_TargetSingleAmplifierRoundCD_Final = 0x3f4,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostEnergy_Base", Value=0x3f5)]
        PropertiesId_TargetSingleAmplifierCostEnergy_Base = 0x3f5,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostEnergy_NS", Value=0x3f8)]
        PropertiesId_TargetSingleAmplifierCostEnergy_NS = 0x3f8,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostEnergy_ND", Value=0x3f9)]
        PropertiesId_TargetSingleAmplifierCostEnergy_ND = 0x3f9,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostEnergy_Final", Value=0x3fa)]
        PropertiesId_TargetSingleAmplifierCostEnergy_Final = 0x3fa,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostCPU_Base", Value=0x3fb)]
        PropertiesId_TargetSingleAmplifierCostCPU_Base = 0x3fb,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostCPU_Final", Value=0x3fe)]
        PropertiesId_TargetSingleAmplifierCostCPU_Final = 0x3fe,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostPower_Base", Value=0x3ff)]
        PropertiesId_TargetSingleAmplifierCostPower_Base = 0x3ff,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierCostPower_Final", Value=0x402)]
        PropertiesId_TargetSingleAmplifierCostPower_Final = 0x402,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierFireRange_Base", Value=0x403)]
        PropertiesId_TargetSingleAmplifierFireRange_Base = 0x403,
        [ProtoEnum(Name="PropertiesId_TargetSingleAmplifierFireRange_Final", Value=0x408)]
        PropertiesId_TargetSingleAmplifierFireRange_Final = 0x408,
        [ProtoEnum(Name="PropertiesId_NpcAttackGainRatio_Base", Value=0x92)]
        PropertiesId_NpcAttackGainRatio_Base = 0x92,
        [ProtoEnum(Name="PropertiesId_NpcShieldGainRatio_Base", Value=0x74a)]
        PropertiesId_NpcShieldGainRatio_Base = 0x74a,
        [ProtoEnum(Name="PropertiesId_NpcArmorGainRatio_Base", Value=0x74b)]
        PropertiesId_NpcArmorGainRatio_Base = 0x74b,
        [ProtoEnum(Name="PropertiesId_ShieldExAmount", Value=0x64b)]
        PropertiesId_ShieldExAmount = 0x64b,
        [ProtoEnum(Name="PropertiesId_ArmorExAmount", Value=0x64c)]
        PropertiesId_ArmorExAmount = 0x64c,
        [ProtoEnum(Name="PropertiesId_ArmorShieldPercentAmount", Value=0x6fb)]
        PropertiesId_ArmorShieldPercentAmount = 0x6fb,
        [ProtoEnum(Name="PropertiesId_DOTShieldAmount", Value=0x621)]
        PropertiesId_DOTShieldAmount = 0x621,
        [ProtoEnum(Name="PropertiesId_DOTArmorAmount", Value=0x622)]
        PropertiesId_DOTArmorAmount = 0x622,
        [ProtoEnum(Name="PropertiesId_HOTArmorAmount", Value=0x623)]
        PropertiesId_HOTArmorAmount = 0x623,
        [ProtoEnum(Name="PropertiesId_DOTSuperWeaponEnergyAmount", Value=0x624)]
        PropertiesId_DOTSuperWeaponEnergyAmount = 0x624,
        [ProtoEnum(Name="PropertiesId_HOTSuperWeaponEnergyAmount", Value=0x625)]
        PropertiesId_HOTSuperWeaponEnergyAmount = 0x625,
        [ProtoEnum(Name="PropertiesId_WeaponEquipCDSpeedUpAmount", Value=0x64d)]
        PropertiesId_WeaponEquipCDSpeedUpAmount = 0x64d,
        [ProtoEnum(Name="PropertiesId_DebuffLifeTimeMulti_NS", Value=0x61d)]
        PropertiesId_DebuffLifeTimeMulti_NS = 0x61d,
        [ProtoEnum(Name="PropertiesId_RedployTimeMulti_ES", Value=0x523)]
        PropertiesId_RedployTimeMulti_ES = 0x523,
        [ProtoEnum(Name="PropertiesId_EquipDamage_Base", Value=0x681)]
        PropertiesId_EquipDamage_Base = 0x681,
        [ProtoEnum(Name="PropertiesId_PercentDamage", Value=0x584)]
        PropertiesId_PercentDamage = 0x584,
        [ProtoEnum(Name="PropertiesId_TargetShieldRatioDmgMulti", Value=0x61c)]
        PropertiesId_TargetShieldRatioDmgMulti = 0x61c,
        [ProtoEnum(Name="PropertiesId_AttrDesc", Value=0x620)]
        PropertiesId_AttrDesc = 0x620,
        [ProtoEnum(Name="PropertiesId_dummy421", Value=0x51b)]
        PropertiesId_dummy421 = 0x51b,
        [ProtoEnum(Name="PropertiesId_dummy517", Value=0x649)]
        PropertiesId_dummy517 = 0x649,
        [ProtoEnum(Name="PropertiesId_dummy598", Value=0x742)]
        PropertiesId_dummy598 = 0x742,
        [ProtoEnum(Name="PropertiesId_dummy599", Value=0x743)]
        PropertiesId_dummy599 = 0x743,
        [ProtoEnum(Name="PropertiesId_dummy600", Value=0x744)]
        PropertiesId_dummy600 = 0x744,
        [ProtoEnum(Name="PropertiesId_dummy601", Value=0x745)]
        PropertiesId_dummy601 = 0x745,
        [ProtoEnum(Name="PropertiesId_dummy602", Value=0x746)]
        PropertiesId_dummy602 = 0x746,
        [ProtoEnum(Name="PropertiesId_dummy603", Value=0x747)]
        PropertiesId_dummy603 = 0x747,
        [ProtoEnum(Name="PropertiesId_dummy582", Value=0x72b)]
        PropertiesId_dummy582 = 0x72b,
        [ProtoEnum(Name="PropertiesId_dummy583", Value=0x72c)]
        PropertiesId_dummy583 = 0x72c,
        [ProtoEnum(Name="PropertiesId_dummy584", Value=0x72d)]
        PropertiesId_dummy584 = 0x72d,
        [ProtoEnum(Name="PropertiesId_dummy585", Value=0x72e)]
        PropertiesId_dummy585 = 0x72e,
        [ProtoEnum(Name="PropertiesId_dummy590", Value=0x737)]
        PropertiesId_dummy590 = 0x737,
        [ProtoEnum(Name="PropertiesId_dummy591", Value=0x738)]
        PropertiesId_dummy591 = 0x738,
        [ProtoEnum(Name="PropertiesId_dummy592", Value=0x739)]
        PropertiesId_dummy592 = 0x739,
        [ProtoEnum(Name="PropertiesId_dummy593", Value=0x73a)]
        PropertiesId_dummy593 = 0x73a,
        [ProtoEnum(Name="PropertiesId_dummy586", Value=0x731)]
        PropertiesId_dummy586 = 0x731,
        [ProtoEnum(Name="PropertiesId_dummy587", Value=0x732)]
        PropertiesId_dummy587 = 0x732,
        [ProtoEnum(Name="PropertiesId_dummy588", Value=0x733)]
        PropertiesId_dummy588 = 0x733,
        [ProtoEnum(Name="PropertiesId_dummy589", Value=0x734)]
        PropertiesId_dummy589 = 0x734,
        [ProtoEnum(Name="PropertiesId_dummy594", Value=0x73d)]
        PropertiesId_dummy594 = 0x73d,
        [ProtoEnum(Name="PropertiesId_dummy595", Value=0x73e)]
        PropertiesId_dummy595 = 0x73e,
        [ProtoEnum(Name="PropertiesId_dummy596", Value=0x73f)]
        PropertiesId_dummy596 = 0x73f,
        [ProtoEnum(Name="PropertiesId_dummy597", Value=0x740)]
        PropertiesId_dummy597 = 0x740,
        [ProtoEnum(Name="PropertiesId_dummy423", Value=0x545)]
        PropertiesId_dummy423 = 0x545,
        [ProtoEnum(Name="PropertiesId_dummy424", Value=0x548)]
        PropertiesId_dummy424 = 0x548,
        [ProtoEnum(Name="PropertiesId_dummy425", Value=0x54f)]
        PropertiesId_dummy425 = 0x54f,
        [ProtoEnum(Name="PropertiesId_dummy426", Value=0x550)]
        PropertiesId_dummy426 = 0x550,
        [ProtoEnum(Name="PropertiesId_dummy427", Value=0x551)]
        PropertiesId_dummy427 = 0x551,
        [ProtoEnum(Name="PropertiesId_dummy428", Value=0x555)]
        PropertiesId_dummy428 = 0x555,
        [ProtoEnum(Name="PropertiesId_dummy429", Value=0x557)]
        PropertiesId_dummy429 = 0x557,
        [ProtoEnum(Name="PropertiesId_dummy430", Value=0x558)]
        PropertiesId_dummy430 = 0x558,
        [ProtoEnum(Name="PropertiesId_dummy431", Value=0x559)]
        PropertiesId_dummy431 = 0x559,
        [ProtoEnum(Name="PropertiesId_dummy432", Value=0x55a)]
        PropertiesId_dummy432 = 0x55a,
        [ProtoEnum(Name="PropertiesId_dummy433", Value=0x55e)]
        PropertiesId_dummy433 = 0x55e,
        [ProtoEnum(Name="PropertiesId_dummy434", Value=0x565)]
        PropertiesId_dummy434 = 0x565,
        [ProtoEnum(Name="PropertiesId_dummy435", Value=0x569)]
        PropertiesId_dummy435 = 0x569,
        [ProtoEnum(Name="PropertiesId_dummy436", Value=0x56a)]
        PropertiesId_dummy436 = 0x56a,
        [ProtoEnum(Name="PropertiesId_dummy437", Value=0x572)]
        PropertiesId_dummy437 = 0x572,
        [ProtoEnum(Name="PropertiesId_dummy438", Value=0x573)]
        PropertiesId_dummy438 = 0x573,
        [ProtoEnum(Name="PropertiesId_dummy439", Value=0x575)]
        PropertiesId_dummy439 = 0x575,
        [ProtoEnum(Name="PropertiesId_dummy440", Value=0x576)]
        PropertiesId_dummy440 = 0x576,
        [ProtoEnum(Name="PropertiesId_dummy477", Value=0x5e2)]
        PropertiesId_dummy477 = 0x5e2,
        [ProtoEnum(Name="PropertiesId_dummy478", Value=0x5e3)]
        PropertiesId_dummy478 = 0x5e3,
        [ProtoEnum(Name="PropertiesId_dummy479", Value=0x5e4)]
        PropertiesId_dummy479 = 0x5e4,
        [ProtoEnum(Name="PropertiesId_dummy480", Value=0x5e8)]
        PropertiesId_dummy480 = 0x5e8,
        [ProtoEnum(Name="PropertiesId_dummy481", Value=0x5ed)]
        PropertiesId_dummy481 = 0x5ed,
        [ProtoEnum(Name="PropertiesId_dummy482", Value=0x5ee)]
        PropertiesId_dummy482 = 0x5ee,
        [ProtoEnum(Name="PropertiesId_dummy483", Value=0x5f1)]
        PropertiesId_dummy483 = 0x5f1,
        [ProtoEnum(Name="PropertiesId_dummy484", Value=0x5f2)]
        PropertiesId_dummy484 = 0x5f2,
        [ProtoEnum(Name="PropertiesId_dummy521", Value=0x64f)]
        PropertiesId_dummy521 = 0x64f,
        [ProtoEnum(Name="PropertiesId_dummy522", Value=0x650)]
        PropertiesId_dummy522 = 0x650,
        [ProtoEnum(Name="PropertiesId_dummy523", Value=0x651)]
        PropertiesId_dummy523 = 0x651,
        [ProtoEnum(Name="PropertiesId_dummy442", Value=0x58a)]
        PropertiesId_dummy442 = 0x58a,
        [ProtoEnum(Name="PropertiesId_dummy443", Value=0x58b)]
        PropertiesId_dummy443 = 0x58b,
        [ProtoEnum(Name="PropertiesId_dummy444", Value=0x58e)]
        PropertiesId_dummy444 = 0x58e,
        [ProtoEnum(Name="PropertiesId_dummy445", Value=0x58f)]
        PropertiesId_dummy445 = 0x58f,
        [ProtoEnum(Name="PropertiesId_dummy446", Value=0x594)]
        PropertiesId_dummy446 = 0x594,
        [ProtoEnum(Name="PropertiesId_dummy447", Value=0x595)]
        PropertiesId_dummy447 = 0x595,
        [ProtoEnum(Name="PropertiesId_dummy448", Value=0x598)]
        PropertiesId_dummy448 = 0x598,
        [ProtoEnum(Name="PropertiesId_dummy449", Value=0x599)]
        PropertiesId_dummy449 = 0x599,
        [ProtoEnum(Name="PropertiesId_dummy450", Value=0x59c)]
        PropertiesId_dummy450 = 0x59c,
        [ProtoEnum(Name="PropertiesId_dummy451", Value=0x59d)]
        PropertiesId_dummy451 = 0x59d,
        [ProtoEnum(Name="PropertiesId_dummy452", Value=0x59e)]
        PropertiesId_dummy452 = 0x59e,
        [ProtoEnum(Name="PropertiesId_dummy453", Value=0x59f)]
        PropertiesId_dummy453 = 0x59f,
        [ProtoEnum(Name="PropertiesId_dummy509", Value=0x639)]
        PropertiesId_dummy509 = 0x639,
        [ProtoEnum(Name="PropertiesId_dummy547", Value=0x6d2)]
        PropertiesId_dummy547 = 0x6d2,
        [ProtoEnum(Name="PropertiesId_dummy548", Value=0x6d4)]
        PropertiesId_dummy548 = 0x6d4,
        [ProtoEnum(Name="PropertiesId_dummy605", Value=0x750)]
        PropertiesId_dummy605 = 0x750,
        [ProtoEnum(Name="PropertiesId_dummy614", Value=0x770)]
        PropertiesId_dummy614 = 0x770,
        [ProtoEnum(Name="PropertiesId_dummy454", Value=0x5a4)]
        PropertiesId_dummy454 = 0x5a4,
        [ProtoEnum(Name="PropertiesId_dummy455", Value=0x5a9)]
        PropertiesId_dummy455 = 0x5a9,
        [ProtoEnum(Name="PropertiesId_dummy456", Value=0x5ae)]
        PropertiesId_dummy456 = 0x5ae,
        [ProtoEnum(Name="PropertiesId_dummy457", Value=0x5af)]
        PropertiesId_dummy457 = 0x5af,
        [ProtoEnum(Name="PropertiesId_dummy458", Value=0x5b2)]
        PropertiesId_dummy458 = 0x5b2,
        [ProtoEnum(Name="PropertiesId_dummy459", Value=0x5b6)]
        PropertiesId_dummy459 = 0x5b6,
        [ProtoEnum(Name="PropertiesId_dummy460", Value=0x5b7)]
        PropertiesId_dummy460 = 0x5b7,
        [ProtoEnum(Name="PropertiesId_dummy461", Value=0x5b8)]
        PropertiesId_dummy461 = 0x5b8,
        [ProtoEnum(Name="PropertiesId_dummy462", Value=0x5b9)]
        PropertiesId_dummy462 = 0x5b9,
        [ProtoEnum(Name="PropertiesId_dummy463", Value=0x5bd)]
        PropertiesId_dummy463 = 0x5bd,
        [ProtoEnum(Name="PropertiesId_dummy464", Value=0x5be)]
        PropertiesId_dummy464 = 0x5be,
        [ProtoEnum(Name="PropertiesId_dummy513", Value=0x641)]
        PropertiesId_dummy513 = 0x641,
        [ProtoEnum(Name="PropertiesId_dummy511", Value=0x63d)]
        PropertiesId_dummy511 = 0x63d,
        [ProtoEnum(Name="PropertiesId_dummy551", Value=0x6de)]
        PropertiesId_dummy551 = 0x6de,
        [ProtoEnum(Name="PropertiesId_dummy552", Value=0x6df)]
        PropertiesId_dummy552 = 0x6df,
        [ProtoEnum(Name="PropertiesId_dummy553", Value=0x6e0)]
        PropertiesId_dummy553 = 0x6e0,
        [ProtoEnum(Name="PropertiesId_dummy507", Value=0x635)]
        PropertiesId_dummy507 = 0x635,
        [ProtoEnum(Name="PropertiesId_dummy573", Value=0x719)]
        PropertiesId_dummy573 = 0x719,
        [ProtoEnum(Name="PropertiesId_dummy574", Value=0x71a)]
        PropertiesId_dummy574 = 0x71a,
        [ProtoEnum(Name="PropertiesId_dummy575", Value=0x71e)]
        PropertiesId_dummy575 = 0x71e,
        [ProtoEnum(Name="PropertiesId_dummy576", Value=0x71f)]
        PropertiesId_dummy576 = 0x71f,
        [ProtoEnum(Name="PropertiesId_dummy577", Value=0x720)]
        PropertiesId_dummy577 = 0x720,
        [ProtoEnum(Name="PropertiesId_dummy578", Value=0x723)]
        PropertiesId_dummy578 = 0x723,
        [ProtoEnum(Name="PropertiesId_dummy579", Value=0x724)]
        PropertiesId_dummy579 = 0x724,
        [ProtoEnum(Name="PropertiesId_dummy580", Value=0x727)]
        PropertiesId_dummy580 = 0x727,
        [ProtoEnum(Name="PropertiesId_dummy581", Value=0x728)]
        PropertiesId_dummy581 = 0x728,
        [ProtoEnum(Name="PropertiesId_dummy512", Value=0x63f)]
        PropertiesId_dummy512 = 0x63f,
        [ProtoEnum(Name="PropertiesId_dummy543", Value=0x6c6)]
        PropertiesId_dummy543 = 0x6c6,
        [ProtoEnum(Name="PropertiesId_dummy544", Value=0x6c8)]
        PropertiesId_dummy544 = 0x6c8,
        [ProtoEnum(Name="PropertiesId_dummy485", Value=0x5f6)]
        PropertiesId_dummy485 = 0x5f6,
        [ProtoEnum(Name="PropertiesId_dummy486", Value=0x5f7)]
        PropertiesId_dummy486 = 0x5f7,
        [ProtoEnum(Name="PropertiesId_dummy487", Value=0x5f8)]
        PropertiesId_dummy487 = 0x5f8,
        [ProtoEnum(Name="PropertiesId_dummy488", Value=0x5fb)]
        PropertiesId_dummy488 = 0x5fb,
        [ProtoEnum(Name="PropertiesId_dummy489", Value=0x5fc)]
        PropertiesId_dummy489 = 0x5fc,
        [ProtoEnum(Name="PropertiesId_dummy490", Value=0x601)]
        PropertiesId_dummy490 = 0x601,
        [ProtoEnum(Name="PropertiesId_dummy491", Value=0x602)]
        PropertiesId_dummy491 = 0x602,
        [ProtoEnum(Name="PropertiesId_dummy492", Value=0x605)]
        PropertiesId_dummy492 = 0x605,
        [ProtoEnum(Name="PropertiesId_dummy493", Value=0x606)]
        PropertiesId_dummy493 = 0x606,
        [ProtoEnum(Name="PropertiesId_dummy524", Value=0x655)]
        PropertiesId_dummy524 = 0x655,
        [ProtoEnum(Name="PropertiesId_dummy525", Value=0x656)]
        PropertiesId_dummy525 = 0x656,
        [ProtoEnum(Name="PropertiesId_dummy526", Value=0x657)]
        PropertiesId_dummy526 = 0x657,
        [ProtoEnum(Name="PropertiesId_dummy508", Value=0x637)]
        PropertiesId_dummy508 = 0x637,
        [ProtoEnum(Name="PropertiesId_dummy545", Value=0x6cc)]
        PropertiesId_dummy545 = 0x6cc,
        [ProtoEnum(Name="PropertiesId_dummy546", Value=0x6ce)]
        PropertiesId_dummy546 = 0x6ce,
        [ProtoEnum(Name="PropertiesId_dummy540", Value=0x6c0)]
        PropertiesId_dummy540 = 0x6c0,
        [ProtoEnum(Name="PropertiesId_dummy541", Value=0x6c1)]
        PropertiesId_dummy541 = 0x6c1,
        [ProtoEnum(Name="PropertiesId_dummy542", Value=0x6c2)]
        PropertiesId_dummy542 = 0x6c2,
        [ProtoEnum(Name="PropertiesId_dummy563", Value=0x700)]
        PropertiesId_dummy563 = 0x700,
        [ProtoEnum(Name="PropertiesId_dummy564", Value=0x703)]
        PropertiesId_dummy564 = 0x703,
        [ProtoEnum(Name="PropertiesId_dummy565", Value=0x704)]
        PropertiesId_dummy565 = 0x704,
        [ProtoEnum(Name="PropertiesId_dummy566", Value=0x708)]
        PropertiesId_dummy566 = 0x708,
        [ProtoEnum(Name="PropertiesId_dummy567", Value=0x70b)]
        PropertiesId_dummy567 = 0x70b,
        [ProtoEnum(Name="PropertiesId_dummy568", Value=0x70c)]
        PropertiesId_dummy568 = 0x70c,
        [ProtoEnum(Name="PropertiesId_dummy604", Value=0x749)]
        PropertiesId_dummy604 = 0x749,
        [ProtoEnum(Name="PropertiesId_dummy569", Value=0x70d)]
        PropertiesId_dummy569 = 0x70d,
        [ProtoEnum(Name="PropertiesId_dummy570", Value=0x70e)]
        PropertiesId_dummy570 = 0x70e,
        [ProtoEnum(Name="PropertiesId_dummy571", Value=0x712)]
        PropertiesId_dummy571 = 0x712,
        [ProtoEnum(Name="PropertiesId_dummy572", Value=0x714)]
        PropertiesId_dummy572 = 0x714,
        [ProtoEnum(Name="PropertiesId_dummy510", Value=0x63b)]
        PropertiesId_dummy510 = 0x63b,
        [ProtoEnum(Name="PropertiesId_dummy549", Value=0x6d8)]
        PropertiesId_dummy549 = 0x6d8,
        [ProtoEnum(Name="PropertiesId_dummy550", Value=0x6da)]
        PropertiesId_dummy550 = 0x6da,
        [ProtoEnum(Name="PropertiesId_dummy465", Value=0x5c3)]
        PropertiesId_dummy465 = 0x5c3,
        [ProtoEnum(Name="PropertiesId_dummy466", Value=0x5c4)]
        PropertiesId_dummy466 = 0x5c4,
        [ProtoEnum(Name="PropertiesId_dummy467", Value=0x5c8)]
        PropertiesId_dummy467 = 0x5c8,
        [ProtoEnum(Name="PropertiesId_dummy468", Value=0x5cd)]
        PropertiesId_dummy468 = 0x5cd,
        [ProtoEnum(Name="PropertiesId_dummy469", Value=0x5ce)]
        PropertiesId_dummy469 = 0x5ce,
        [ProtoEnum(Name="PropertiesId_dummy470", Value=0x5d1)]
        PropertiesId_dummy470 = 0x5d1,
        [ProtoEnum(Name="PropertiesId_dummy471", Value=0x5d2)]
        PropertiesId_dummy471 = 0x5d2,
        [ProtoEnum(Name="PropertiesId_dummy472", Value=0x5d5)]
        PropertiesId_dummy472 = 0x5d5,
        [ProtoEnum(Name="PropertiesId_dummy473", Value=0x5d6)]
        PropertiesId_dummy473 = 0x5d6,
        [ProtoEnum(Name="PropertiesId_dummy474", Value=0x5d8)]
        PropertiesId_dummy474 = 0x5d8,
        [ProtoEnum(Name="PropertiesId_dummy475", Value=0x5db)]
        PropertiesId_dummy475 = 0x5db,
        [ProtoEnum(Name="PropertiesId_dummy476", Value=0x5dc)]
        PropertiesId_dummy476 = 0x5dc,
        [ProtoEnum(Name="PropertiesId_dummy494", Value=0x60a)]
        PropertiesId_dummy494 = 0x60a,
        [ProtoEnum(Name="PropertiesId_dummy495", Value=0x60b)]
        PropertiesId_dummy495 = 0x60b,
        [ProtoEnum(Name="PropertiesId_dummy496", Value=0x60c)]
        PropertiesId_dummy496 = 0x60c,
        [ProtoEnum(Name="PropertiesId_dummy497", Value=0x610)]
        PropertiesId_dummy497 = 0x610,
        [ProtoEnum(Name="PropertiesId_dummy498", Value=0x615)]
        PropertiesId_dummy498 = 0x615,
        [ProtoEnum(Name="PropertiesId_dummy499", Value=0x616)]
        PropertiesId_dummy499 = 0x616,
        [ProtoEnum(Name="PropertiesId_dummy500", Value=0x619)]
        PropertiesId_dummy500 = 0x619,
        [ProtoEnum(Name="PropertiesId_dummy501", Value=0x61a)]
        PropertiesId_dummy501 = 0x61a,
        [ProtoEnum(Name="PropertiesId_dummy527", Value=0x65b)]
        PropertiesId_dummy527 = 0x65b,
        [ProtoEnum(Name="PropertiesId_dummy528", Value=0x65c)]
        PropertiesId_dummy528 = 0x65c,
        [ProtoEnum(Name="PropertiesId_dummy529", Value=0x65d)]
        PropertiesId_dummy529 = 0x65d,
        [ProtoEnum(Name="PropertiesId_dummy530", Value=0x682)]
        PropertiesId_dummy530 = 0x682,
        [ProtoEnum(Name="PropertiesId_dummy531", Value=0x683)]
        PropertiesId_dummy531 = 0x683,
        [ProtoEnum(Name="PropertiesId_dummy532", Value=0x684)]
        PropertiesId_dummy532 = 0x684,
        [ProtoEnum(Name="PropertiesId_dummy533", Value=0x686)]
        PropertiesId_dummy533 = 0x686,
        [ProtoEnum(Name="PropertiesId_dummy534", Value=0x687)]
        PropertiesId_dummy534 = 0x687,
        [ProtoEnum(Name="PropertiesId_dummy535", Value=0x68b)]
        PropertiesId_dummy535 = 0x68b,
        [ProtoEnum(Name="PropertiesId_dummy536", Value=0x68c)]
        PropertiesId_dummy536 = 0x68c,
        [ProtoEnum(Name="PropertiesId_dummy537", Value=0x68d)]
        PropertiesId_dummy537 = 0x68d,
        [ProtoEnum(Name="PropertiesId_dummy538", Value=0x68e)]
        PropertiesId_dummy538 = 0x68e,
        [ProtoEnum(Name="PropertiesId_dummy539", Value=0x68f)]
        PropertiesId_dummy539 = 0x68f,
        [ProtoEnum(Name="PropertiesId_dummy554", Value=0x6e4)]
        PropertiesId_dummy554 = 0x6e4,
        [ProtoEnum(Name="PropertiesId_dummy555", Value=0x6e6)]
        PropertiesId_dummy555 = 0x6e6,
        [ProtoEnum(Name="PropertiesId_dummy556", Value=0x6ea)]
        PropertiesId_dummy556 = 0x6ea,
        [ProtoEnum(Name="PropertiesId_dummy557", Value=0x6ec)]
        PropertiesId_dummy557 = 0x6ec,
        [ProtoEnum(Name="PropertiesId_dummy558", Value=0x6f0)]
        PropertiesId_dummy558 = 0x6f0,
        [ProtoEnum(Name="PropertiesId_dummy559", Value=0x6f2)]
        PropertiesId_dummy559 = 0x6f2,
        [ProtoEnum(Name="PropertiesId_dummy560", Value=0x6f6)]
        PropertiesId_dummy560 = 0x6f6,
        [ProtoEnum(Name="PropertiesId_dummy561", Value=0x6f8)]
        PropertiesId_dummy561 = 0x6f8,
        [ProtoEnum(Name="PropertiesId_dummy422", Value=0x536)]
        PropertiesId_dummy422 = 0x536,
        [ProtoEnum(Name="PropertiesId_dummy441", Value=0x586)]
        PropertiesId_dummy441 = 0x586,
        [ProtoEnum(Name="PropertiesId_Max", Value=0x77f)]
        PropertiesId_Max = 0x77f
    }
}

