﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="LogicalSoundResTableID")]
    public enum LogicalSoundResTableID
    {
        [ProtoEnum(Name="LogicalSoundResTableID_LoginBGMResFullPath", Value=1)]
        LogicalSoundResTableID_LoginBGMResFullPath = 1,
        [ProtoEnum(Name="LogicalSoundResTableID_SpaceStationBGM_NEFResFullPath", Value=2)]
        LogicalSoundResTableID_SpaceStationBGM_NEFResFullPath = 2,
        [ProtoEnum(Name="LogicalSoundResTableID_SpaceStationBGM_RSResFullPath", Value=3)]
        LogicalSoundResTableID_SpaceStationBGM_RSResFullPath = 3,
        [ProtoEnum(Name="LogicalSoundResTableID_SpaceStationBGM_REResFullPath", Value=4)]
        LogicalSoundResTableID_SpaceStationBGM_REResFullPath = 4,
        [ProtoEnum(Name="LogicalSoundResTableID_SpaceStationBGM_ECDResFullPath", Value=5)]
        LogicalSoundResTableID_SpaceStationBGM_ECDResFullPath = 5,
        [ProtoEnum(Name="LogicalSoundResTableID_SpaceStationBGM_USGResFullPath", Value=6)]
        LogicalSoundResTableID_SpaceStationBGM_USGResFullPath = 6,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN1ResFullPath", Value=7)]
        LogicalSoundResTableID_InSpaceBGM_GEN1ResFullPath = 7,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN2ResFullPath", Value=8)]
        LogicalSoundResTableID_InSpaceBGM_GEN2ResFullPath = 8,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN3ResFullPath", Value=9)]
        LogicalSoundResTableID_InSpaceBGM_GEN3ResFullPath = 9,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN4ResFullPath", Value=10)]
        LogicalSoundResTableID_InSpaceBGM_GEN4ResFullPath = 10,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN5ResFullPath", Value=11)]
        LogicalSoundResTableID_InSpaceBGM_GEN5ResFullPath = 11,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN6ResFullPath", Value=12)]
        LogicalSoundResTableID_InSpaceBGM_GEN6ResFullPath = 12,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN7ResFullPath", Value=13)]
        LogicalSoundResTableID_InSpaceBGM_GEN7ResFullPath = 13,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN8ResFullPath", Value=14)]
        LogicalSoundResTableID_InSpaceBGM_GEN8ResFullPath = 14,
        [ProtoEnum(Name="LogicalSoundResTableID_InSpaceBGM_GEN9ResFullPath", Value=15)]
        LogicalSoundResTableID_InSpaceBGM_GEN9ResFullPath = 15,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_NEFResFullPath", Value=0x10)]
        LogicalSoundResTableID_BattleBGM_NEFResFullPath = 0x10,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_RSResFullPath", Value=0x11)]
        LogicalSoundResTableID_BattleBGM_RSResFullPath = 0x11,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_REResFullPath", Value=0x12)]
        LogicalSoundResTableID_BattleBGM_REResFullPath = 0x12,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_ECDResFullPath", Value=0x13)]
        LogicalSoundResTableID_BattleBGM_ECDResFullPath = 0x13,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_ARResFullPath", Value=20)]
        LogicalSoundResTableID_BattleBGM_ARResFullPath = 20,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_USGResFullPath", Value=0x15)]
        LogicalSoundResTableID_BattleBGM_USGResFullPath = 0x15,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_GEN1ResFullPath", Value=0x16)]
        LogicalSoundResTableID_BattleBGM_GEN1ResFullPath = 0x16,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_GEN2ResFullPath", Value=0x17)]
        LogicalSoundResTableID_BattleBGM_GEN2ResFullPath = 0x17,
        [ProtoEnum(Name="LogicalSoundResTableID_BattleBGM_GEN3ResFullPath", Value=0x18)]
        LogicalSoundResTableID_BattleBGM_GEN3ResFullPath = 0x18,
        [ProtoEnum(Name="LogicalSoundResTableID_JumpStartAudioResFullPath", Value=0x19)]
        LogicalSoundResTableID_JumpStartAudioResFullPath = 0x19,
        [ProtoEnum(Name="LogicalSoundResTableID_JumpLoopAudioResFullPath", Value=0x1a)]
        LogicalSoundResTableID_JumpLoopAudioResFullPath = 0x1a,
        [ProtoEnum(Name="LogicalSoundResTableID_JumpEndAudioResFullPath", Value=0x1b)]
        LogicalSoundResTableID_JumpEndAudioResFullPath = 0x1b,
        [ProtoEnum(Name="LogicalSoundResTableID_EngineLoopAudioResFullPath", Value=0x1c)]
        LogicalSoundResTableID_EngineLoopAudioResFullPath = 0x1c,
        [ProtoEnum(Name="LogicalSoundResTableID_QuestCompleteAudioResFullPath", Value=0x1d)]
        LogicalSoundResTableID_QuestCompleteAudioResFullPath = 0x1d,
        [ProtoEnum(Name="LogicalSoundResTableID_QuestPopAudioResFullPath", Value=30)]
        LogicalSoundResTableID_QuestPopAudioResFullPath = 30,
        [ProtoEnum(Name="LogicalSoundResTableID_NPCChatOpenAudioResFullPath", Value=0x1f)]
        LogicalSoundResTableID_NPCChatOpenAudioResFullPath = 0x1f,
        [ProtoEnum(Name="LogicalSoundResTableID_ErrorAudioResFullPath", Value=0x20)]
        LogicalSoundResTableID_ErrorAudioResFullPath = 0x20,
        [ProtoEnum(Name="LogicalSoundResTableID_NPCTalkerOpenAudioResFullPath", Value=0x21)]
        LogicalSoundResTableID_NPCTalkerOpenAudioResFullPath = 0x21,
        [ProtoEnum(Name="LogicalSoundResTableID_ChangeEquipAudioResFullPath", Value=0x22)]
        LogicalSoundResTableID_ChangeEquipAudioResFullPath = 0x22,
        [ProtoEnum(Name="LogicalSoundResTableID_UnloadEquipAudioResFullPath", Value=0x23)]
        LogicalSoundResTableID_UnloadEquipAudioResFullPath = 0x23,
        [ProtoEnum(Name="LogicalSoundResTableID_PlayerLevelUpAudioResFullPath", Value=0x24)]
        LogicalSoundResTableID_PlayerLevelUpAudioResFullPath = 0x24,
        [ProtoEnum(Name="LogicalSoundResTableID_CaptainLevelUpAudioResFullPath", Value=0x25)]
        LogicalSoundResTableID_CaptainLevelUpAudioResFullPath = 0x25,
        [ProtoEnum(Name="LogicalSoundResTableID_SolarSystemLockTarget", Value=0x26)]
        LogicalSoundResTableID_SolarSystemLockTarget = 0x26,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangar_DockLoop", Value=0x27)]
        LogicalSoundResTableID_ShipHangar_DockLoop = 0x27,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangar_PutInDock", Value=40)]
        LogicalSoundResTableID_ShipHangar_PutInDock = 40,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangar_ChangeEquip", Value=0x29)]
        LogicalSoundResTableID_ShipHangar_ChangeEquip = 0x29,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangar_UnloadEquip", Value=0x2a)]
        LogicalSoundResTableID_ShipHangar_UnloadEquip = 0x2a,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangarItemStore_DestoryItem", Value=0x2b)]
        LogicalSoundResTableID_ShipHangarItemStore_DestoryItem = 0x2b,
        [ProtoEnum(Name="LogicalSoundResTableID_ShipHangarItemStore_TransferedItem", Value=0x2c)]
        LogicalSoundResTableID_ShipHangarItemStore_TransferedItem = 0x2c,
        [ProtoEnum(Name="LogicalSoundResTableID_Shop_ItemSale", Value=0x2d)]
        LogicalSoundResTableID_Shop_ItemSale = 0x2d,
        [ProtoEnum(Name="LogicalSoundResTableID_Produce_PutInPrint", Value=0x2e)]
        LogicalSoundResTableID_Produce_PutInPrint = 0x2e,
        [ProtoEnum(Name="LogicalSoundResTableID_Produce_Producing", Value=0x2f)]
        LogicalSoundResTableID_Produce_Producing = 0x2f,
        [ProtoEnum(Name="LogicalSoundResTableID_Produce_ProducingOnEnterUI", Value=0x30)]
        LogicalSoundResTableID_Produce_ProducingOnEnterUI = 0x30,
        [ProtoEnum(Name="LogicalSoundResTableID_Chip_PutIn", Value=0x31)]
        LogicalSoundResTableID_Chip_PutIn = 0x31,
        [ProtoEnum(Name="LogicalSoundResTableID_Chip_Unload", Value=50)]
        LogicalSoundResTableID_Chip_Unload = 50,
        [ProtoEnum(Name="LogicalSoundResTableID_Char_SkillLevelUp", Value=0x33)]
        LogicalSoundResTableID_Char_SkillLevelUp = 0x33,
        [ProtoEnum(Name="LogicalSoundResTableID_Char_AddPoint", Value=0x34)]
        LogicalSoundResTableID_Char_AddPoint = 0x34,
        [ProtoEnum(Name="LogicalSoundResTableID_Chat_Relive", Value=0x35)]
        LogicalSoundResTableID_Chat_Relive = 0x35,
        [ProtoEnum(Name="LogicalSoundResTableID_Quest_NewQuestGet", Value=0x36)]
        LogicalSoundResTableID_Quest_NewQuestGet = 0x36,
        [ProtoEnum(Name="LogicalSoundResTableID_Quest_QuestComplete", Value=0x37)]
        LogicalSoundResTableID_Quest_QuestComplete = 0x37,
        [ProtoEnum(Name="LogicalSoundResTableID_Item_NewCaptainGet", Value=0x38)]
        LogicalSoundResTableID_Item_NewCaptainGet = 0x38,
        [ProtoEnum(Name="LogicalSoundResTableID_Item_NewShipGet", Value=0x39)]
        LogicalSoundResTableID_Item_NewShipGet = 0x39,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_AddToCrackList", Value=0x3a)]
        LogicalSoundResTableID_Crack_AddToCrackList = 0x3a,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_StartCraking", Value=0x3b)]
        LogicalSoundResTableID_Crack_StartCraking = 0x3b,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_Cracking", Value=60)]
        LogicalSoundResTableID_Crack_Cracking = 60,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_CrackEnd", Value=0x3d)]
        LogicalSoundResTableID_Crack_CrackEnd = 0x3d,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_BoxCracked", Value=0x3e)]
        LogicalSoundResTableID_Crack_BoxCracked = 0x3e,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_BoxOpen", Value=0x3f)]
        LogicalSoundResTableID_Crack_BoxOpen = 0x3f,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_GetItem", Value=0x40)]
        LogicalSoundResTableID_Crack_GetItem = 0x40,
        [ProtoEnum(Name="LogicalSoundResTableID_Crack_GetItemRare", Value=0x41)]
        LogicalSoundResTableID_Crack_GetItemRare = 0x41,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_LockTarget", Value=0x42)]
        LogicalSoundResTableID_AudioVoice_LockTarget = 0x42,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_JumpPrepare", Value=0x43)]
        LogicalSoundResTableID_AudioVoice_JumpPrepare = 0x43,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_StartJump", Value=0x44)]
        LogicalSoundResTableID_AudioVoice_StartJump = 0x44,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_EndJump", Value=0x45)]
        LogicalSoundResTableID_AudioVoice_EndJump = 0x45,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_ForbidJump", Value=70)]
        LogicalSoundResTableID_AudioVoice_ForbidJump = 70,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_BegionEnterSpaceStation", Value=0x47)]
        LogicalSoundResTableID_AudioVoice_BegionEnterSpaceStation = 0x47,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_EnterSpaceStation", Value=0x48)]
        LogicalSoundResTableID_AudioVoice_EnterSpaceStation = 0x48,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_MoveCloseToTarget", Value=0x49)]
        LogicalSoundResTableID_AudioVoice_MoveCloseToTarget = 0x49,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_MoveAwayFromTarget", Value=0x4a)]
        LogicalSoundResTableID_AudioVoice_MoveAwayFromTarget = 0x4a,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_MoveArroundTarget", Value=0x4b)]
        LogicalSoundResTableID_AudioVoice_MoveArroundTarget = 0x4b,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_NotEnoughEnergy", Value=0x4c)]
        LogicalSoundResTableID_AudioVoice_NotEnoughEnergy = 0x4c,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_NotEnoughAmmo", Value=0x4d)]
        LogicalSoundResTableID_AudioVoice_NotEnoughAmmo = 0x4d,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_TargetOutofRange", Value=0x4e)]
        LogicalSoundResTableID_AudioVoice_TargetOutofRange = 0x4e,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_SuperWeaponCDEnd", Value=0x4f)]
        LogicalSoundResTableID_AudioVoice_SuperWeaponCDEnd = 0x4f,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_NavigateDestSet", Value=80)]
        LogicalSoundResTableID_AudioVoice_NavigateDestSet = 80,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_BeginAutoFight", Value=0x51)]
        LogicalSoundResTableID_AudioVoice_BeginAutoFight = 0x51,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_StopAutoFight", Value=0x52)]
        LogicalSoundResTableID_AudioVoice_StopAutoFight = 0x52,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_InteractWithTarget", Value=0x53)]
        LogicalSoundResTableID_AudioVoice_InteractWithTarget = 0x53,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_SelfShipLowHP", Value=0x54)]
        LogicalSoundResTableID_AudioVoice_SelfShipLowHP = 0x54,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_SelfShipDestoryed", Value=0x55)]
        LogicalSoundResTableID_AudioVoice_SelfShipDestoryed = 0x55,
        [ProtoEnum(Name="LogicalSoundResTableID_AudioVoice_InvisibleStart", Value=0x56)]
        LogicalSoundResTableID_AudioVoice_InvisibleStart = 0x56,
        [ProtoEnum(Name="LogicalSoundResTableID_UI_ClickNormal", Value=0x57)]
        LogicalSoundResTableID_UI_ClickNormal = 0x57,
        [ProtoEnum(Name="LogicalSoundResTableID_TeleportStartAudioResFullPath", Value=0x58)]
        LogicalSoundResTableID_TeleportStartAudioResFullPath = 0x58,
        [ProtoEnum(Name="LogicalSoundResTableID_TeleportLoopAudioResFullPath", Value=0x59)]
        LogicalSoundResTableID_TeleportLoopAudioResFullPath = 0x59,
        [ProtoEnum(Name="LogicalSoundResTableID_TeleportEndAudioResFullPath", Value=90)]
        LogicalSoundResTableID_TeleportEndAudioResFullPath = 90,
        [ProtoEnum(Name="LogicalSoundResTableID_UseStarGateAudioResFullPath", Value=0x5b)]
        LogicalSoundResTableID_UseStarGateAudioResFullPath = 0x5b,
        [ProtoEnum(Name="LogicalSoundResTableID_BuyItemInNpcShopAudioResFullPath", Value=0x5c)]
        LogicalSoundResTableID_BuyItemInNpcShopAudioResFullPath = 0x5c,
        [ProtoEnum(Name="LogicalSoundResTableID_SailReportOpen", Value=0x5d)]
        LogicalSoundResTableID_SailReportOpen = 0x5d,
        [ProtoEnum(Name="LogicalSoundResTableID_StarDataRecover_Open", Value=0x5e)]
        LogicalSoundResTableID_StarDataRecover_Open = 0x5e,
        [ProtoEnum(Name="LogicalSoundResTableID_StarDataRecover_Move", Value=0x5f)]
        LogicalSoundResTableID_StarDataRecover_Move = 0x5f,
        [ProtoEnum(Name="LogicalSoundResTableID_StarDataRecover_AutoMove", Value=0x60)]
        LogicalSoundResTableID_StarDataRecover_AutoMove = 0x60,
        [ProtoEnum(Name="LogicalSoundResTableID_StarDataRecover_Done", Value=0x61)]
        LogicalSoundResTableID_StarDataRecover_Done = 0x61,
        [ProtoEnum(Name="LogicalSoundResTableID_FlagShipTeleportStartCharging", Value=0x62)]
        LogicalSoundResTableID_FlagShipTeleportStartCharging = 0x62,
        [ProtoEnum(Name="LogicalSoundResTableID_FlagShipTeleportChargingFinish", Value=0x63)]
        LogicalSoundResTableID_FlagShipTeleportChargingFinish = 0x63,
        [ProtoEnum(Name="LogicalSoundResTableID_FlagShipTeleportChargingFail", Value=100)]
        LogicalSoundResTableID_FlagShipTeleportChargingFail = 100,
        [ProtoEnum(Name="LogicalSoundResTableID_DevelopmentNormal", Value=0x65)]
        LogicalSoundResTableID_DevelopmentNormal = 0x65,
        [ProtoEnum(Name="LogicalSoundResTableID_DevelopmentInProcess", Value=0x66)]
        LogicalSoundResTableID_DevelopmentInProcess = 0x66,
        [ProtoEnum(Name="LogicalSoundResTableID_DevelopmentReady", Value=0x67)]
        LogicalSoundResTableID_DevelopmentReady = 0x67,
        [ProtoEnum(Name="LogicalSoundResTableID_DevelopmentTransformation", Value=0x68)]
        LogicalSoundResTableID_DevelopmentTransformation = 0x68
    }
}

