﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcInteractionContinuousEffectType")]
    public enum NpcInteractionContinuousEffectType
    {
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_Normal", Value=0)]
        NpcInteractionContinuousEffectType_Normal = 0,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_EnergyChangeContinually", Value=1)]
        NpcInteractionContinuousEffectType_EnergyChangeContinually = 1,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_EnergyChangePercentContinually", Value=2)]
        NpcInteractionContinuousEffectType_EnergyChangePercentContinually = 2,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_ShieldChangeContinually", Value=3)]
        NpcInteractionContinuousEffectType_ShieldChangeContinually = 3,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_ShieldChangePercentContinually", Value=4)]
        NpcInteractionContinuousEffectType_ShieldChangePercentContinually = 4,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_ArmorChangeContinually", Value=5)]
        NpcInteractionContinuousEffectType_ArmorChangeContinually = 5,
        [ProtoEnum(Name="NpcInteractionContinuousEffectType_ArmorChangePercentContinually", Value=6)]
        NpcInteractionContinuousEffectType_ArmorChangePercentContinually = 6
    }
}

