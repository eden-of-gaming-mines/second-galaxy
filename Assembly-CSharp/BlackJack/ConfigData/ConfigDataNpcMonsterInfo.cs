﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcMonsterInfo")]
    public class ConfigDataNpcMonsterInfo : IExtensible
    {
        private int _ID;
        private int _NpcShipID;
        private readonly List<int> _BufIdForEveryLevel;
        private uint _InitArmor;
        private uint _InitShield;
        private uint _InitEnergy;
        private int _AIId;
        private int _DelegateFightTime;
        private int _BasicMiningCount;
        private int _FactionID;
        private float _NpcDeadFactionCreditValue;
        private BlackJack.ConfigData.NpcShipFunctionType _NpcShipFunctionType;
        private bool _IsNotAttackable;
        private int _TargetPriority;
        private bool _HideOnTargetList;
        private BlackJack.ConfigData.HudDisplayStatus _HudDisplayStatus;
        private bool _IsNameAlwaysVisibleOnHUD;
        private int _FixMinLODLevel;
        private int _FixMaxLODLevel;
        private int _MonsterLevelDropInfoId;
        private int _MonsterLevelBufInfoId;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_NpcShipID;
        private static DelegateBridge __Hotfix_set_NpcShipID;
        private static DelegateBridge __Hotfix_get_BufIdForEveryLevel;
        private static DelegateBridge __Hotfix_get_InitArmor;
        private static DelegateBridge __Hotfix_set_InitArmor;
        private static DelegateBridge __Hotfix_get_InitShield;
        private static DelegateBridge __Hotfix_set_InitShield;
        private static DelegateBridge __Hotfix_get_InitEnergy;
        private static DelegateBridge __Hotfix_set_InitEnergy;
        private static DelegateBridge __Hotfix_get_AIId;
        private static DelegateBridge __Hotfix_set_AIId;
        private static DelegateBridge __Hotfix_get_DelegateFightTime;
        private static DelegateBridge __Hotfix_set_DelegateFightTime;
        private static DelegateBridge __Hotfix_get_BasicMiningCount;
        private static DelegateBridge __Hotfix_set_BasicMiningCount;
        private static DelegateBridge __Hotfix_get_FactionID;
        private static DelegateBridge __Hotfix_set_FactionID;
        private static DelegateBridge __Hotfix_get_NpcDeadFactionCreditValue;
        private static DelegateBridge __Hotfix_set_NpcDeadFactionCreditValue;
        private static DelegateBridge __Hotfix_get_NpcShipFunctionType;
        private static DelegateBridge __Hotfix_set_NpcShipFunctionType;
        private static DelegateBridge __Hotfix_get_IsNotAttackable;
        private static DelegateBridge __Hotfix_set_IsNotAttackable;
        private static DelegateBridge __Hotfix_get_TargetPriority;
        private static DelegateBridge __Hotfix_set_TargetPriority;
        private static DelegateBridge __Hotfix_get_HideOnTargetList;
        private static DelegateBridge __Hotfix_set_HideOnTargetList;
        private static DelegateBridge __Hotfix_get_HudDisplayStatus;
        private static DelegateBridge __Hotfix_set_HudDisplayStatus;
        private static DelegateBridge __Hotfix_get_IsNameAlwaysVisibleOnHUD;
        private static DelegateBridge __Hotfix_set_IsNameAlwaysVisibleOnHUD;
        private static DelegateBridge __Hotfix_get_FixMinLODLevel;
        private static DelegateBridge __Hotfix_set_FixMinLODLevel;
        private static DelegateBridge __Hotfix_get_FixMaxLODLevel;
        private static DelegateBridge __Hotfix_set_FixMaxLODLevel;
        private static DelegateBridge __Hotfix_get_MonsterLevelDropInfoId;
        private static DelegateBridge __Hotfix_set_MonsterLevelDropInfoId;
        private static DelegateBridge __Hotfix_get_MonsterLevelBufInfoId;
        private static DelegateBridge __Hotfix_set_MonsterLevelBufInfoId;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="NpcShipID", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="BufIdForEveryLevel", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufIdForEveryLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="InitArmor", DataFormat=DataFormat.TwosComplement)]
        public uint InitArmor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="InitShield", DataFormat=DataFormat.TwosComplement)]
        public uint InitShield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="InitEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint InitEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="AIId", DataFormat=DataFormat.TwosComplement)]
        public int AIId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="DelegateFightTime", DataFormat=DataFormat.TwosComplement)]
        public int DelegateFightTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="BasicMiningCount", DataFormat=DataFormat.TwosComplement)]
        public int BasicMiningCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="FactionID", DataFormat=DataFormat.TwosComplement)]
        public int FactionID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="NpcDeadFactionCreditValue", DataFormat=DataFormat.FixedSize)]
        public float NpcDeadFactionCreditValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="NpcShipFunctionType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.NpcShipFunctionType NpcShipFunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="IsNotAttackable", DataFormat=DataFormat.Default)]
        public bool IsNotAttackable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="TargetPriority", DataFormat=DataFormat.TwosComplement)]
        public int TargetPriority
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="HideOnTargetList", DataFormat=DataFormat.Default)]
        public bool HideOnTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="HudDisplayStatus", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.HudDisplayStatus HudDisplayStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="IsNameAlwaysVisibleOnHUD", DataFormat=DataFormat.Default)]
        public bool IsNameAlwaysVisibleOnHUD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="FixMinLODLevel", DataFormat=DataFormat.TwosComplement)]
        public int FixMinLODLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=true, Name="FixMaxLODLevel", DataFormat=DataFormat.TwosComplement)]
        public int FixMaxLODLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, IsRequired=true, Name="MonsterLevelDropInfoId", DataFormat=DataFormat.TwosComplement)]
        public int MonsterLevelDropInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="MonsterLevelBufInfoId", DataFormat=DataFormat.TwosComplement)]
        public int MonsterLevelBufInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

