﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcTalkerScriptType")]
    public enum NpcTalkerScriptType
    {
        [ProtoEnum(Name="NpcTalkerScriptType_None", Value=0)]
        NpcTalkerScriptType_None = 0,
        [ProtoEnum(Name="NpcTalkerScriptType_Test", Value=1)]
        NpcTalkerScriptType_Test = 1,
        [ProtoEnum(Name="NpcTalkerScriptType_StationFuncDefault", Value=2)]
        NpcTalkerScriptType_StationFuncDefault = 2,
        [ProtoEnum(Name="NpcTalkerScriptType_NpcTalkerScriptMainQuest1015", Value=3)]
        NpcTalkerScriptType_NpcTalkerScriptMainQuest1015 = 3,
        [ProtoEnum(Name="NpcTalkerScriptType_ChoiceMakeDefault", Value=4)]
        NpcTalkerScriptType_ChoiceMakeDefault = 4,
        [ProtoEnum(Name="NpcTalkerScriptType_StationDefault", Value=5)]
        NpcTalkerScriptType_StationDefault = 5,
        [ProtoEnum(Name="NpcTalkerScriptType_GuildBaseSolarSystemSet", Value=6)]
        NpcTalkerScriptType_GuildBaseSolarSystemSet = 6,
        [ProtoEnum(Name="NpcTalkerScriptType_FactionCredit", Value=7)]
        NpcTalkerScriptType_FactionCredit = 7,
        [ProtoEnum(Name="NpcTalkerScriptType_TradeBuyNpcShop", Value=8)]
        NpcTalkerScriptType_TradeBuyNpcShop = 8,
        [ProtoEnum(Name="NpcTalkerScriptType_TradeSaleNpcShop", Value=9)]
        NpcTalkerScriptType_TradeSaleNpcShop = 9,
        [ProtoEnum(Name="NpcTalkerScriptType_Max", Value=10)]
        NpcTalkerScriptType_Max = 10
    }
}

