﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GenderType")]
    public enum GenderType
    {
        [ProtoEnum(Name="GenderType_Male", Value=1)]
        GenderType_Male = 1,
        [ProtoEnum(Name="GenderType_Female", Value=2)]
        GenderType_Female = 2,
        [ProtoEnum(Name="GenderType_Unknown", Value=3)]
        GenderType_Unknown = 3
    }
}

