﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MailDeleteType")]
    public enum MailDeleteType
    {
        [ProtoEnum(Name="MailDeleteType_NONE", Value=0)]
        MailDeleteType_NONE = 0,
        [ProtoEnum(Name="MailDeleteType_Manual", Value=1)]
        MailDeleteType_Manual = 1,
        [ProtoEnum(Name="MailDeleteType_Auto", Value=2)]
        MailDeleteType_Auto = 2,
        [ProtoEnum(Name="MailDeleteType_OnlyByTime", Value=3)]
        MailDeleteType_OnlyByTime = 3
    }
}

