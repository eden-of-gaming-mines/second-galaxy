﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSolarSystemGuildMinrealOutputInfo")]
    public class ConfigDataSolarSystemGuildMinrealOutputInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _GuildMinrealList;
        private int _GuildMinrealTypeCount;
        private string _ShowColor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GuildMinrealList;
        private static DelegateBridge __Hotfix_get_GuildMinrealTypeCount;
        private static DelegateBridge __Hotfix_set_GuildMinrealTypeCount;
        private static DelegateBridge __Hotfix_get_ShowColor;
        private static DelegateBridge __Hotfix_set_ShowColor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="GuildMinrealList", DataFormat=DataFormat.TwosComplement)]
        public List<int> GuildMinrealList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GuildMinrealTypeCount", DataFormat=DataFormat.TwosComplement)]
        public int GuildMinrealTypeCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShowColor", DataFormat=DataFormat.Default)]
        public string ShowColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

