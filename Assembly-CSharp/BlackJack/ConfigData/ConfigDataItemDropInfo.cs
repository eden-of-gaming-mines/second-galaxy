﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataItemDropInfo")]
    public class ConfigDataItemDropInfo : IExtensible
    {
        private int _ID;
        private int _RandomCount;
        private readonly List<ItemDropInfoUIShowItemList> _UIShowItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_RandomCount;
        private static DelegateBridge __Hotfix_set_RandomCount;
        private static DelegateBridge __Hotfix_get_UIShowItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="RandomCount", DataFormat=DataFormat.TwosComplement)]
        public int RandomCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="UIShowItemList", DataFormat=DataFormat.Default)]
        public List<ItemDropInfoUIShowItemList> UIShowItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

