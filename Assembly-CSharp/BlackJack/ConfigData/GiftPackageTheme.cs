﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GiftPackageTheme")]
    public enum GiftPackageTheme
    {
        [ProtoEnum(Name="GiftPackageTheme_Default", Value=0)]
        GiftPackageTheme_Default = 0,
        [ProtoEnum(Name="GiftPackageTheme_Technology", Value=1)]
        GiftPackageTheme_Technology = 1,
        [ProtoEnum(Name="GiftPackageTheme_Chip", Value=2)]
        GiftPackageTheme_Chip = 2,
        [ProtoEnum(Name="GiftPackageTheme_Development", Value=3)]
        GiftPackageTheme_Development = 3,
        [ProtoEnum(Name="GiftPackageTheme_Guild", Value=4)]
        GiftPackageTheme_Guild = 4,
        [ProtoEnum(Name="GiftPackageTheme_Festival1", Value=5)]
        GiftPackageTheme_Festival1 = 5,
        [ProtoEnum(Name="GiftPackageTheme_Festival2", Value=6)]
        GiftPackageTheme_Festival2 = 6,
        [ProtoEnum(Name="GiftPackageTheme_Ship", Value=7)]
        GiftPackageTheme_Ship = 7,
        [ProtoEnum(Name="GiftPackageTheme_Max", Value=8)]
        GiftPackageTheme_Max = 8
    }
}

