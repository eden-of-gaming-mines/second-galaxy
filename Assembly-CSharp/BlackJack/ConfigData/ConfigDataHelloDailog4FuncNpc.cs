﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataHelloDailog4FuncNpc")]
    public class ConfigDataHelloDailog4FuncNpc : IExtensible
    {
        private int _ID;
        private NpcFunctionType _FunctionType;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private GenderType _Gender;
        private NpcPersonalityType _PersonalityType;
        private int _HelloDialogId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_PersonalityType;
        private static DelegateBridge __Hotfix_set_PersonalityType;
        private static DelegateBridge __Hotfix_get_HelloDialogId;
        private static DelegateBridge __Hotfix_set_HelloDialogId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public NpcFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Gender", DataFormat=DataFormat.TwosComplement)]
        public GenderType Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="PersonalityType", DataFormat=DataFormat.TwosComplement)]
        public NpcPersonalityType PersonalityType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="HelloDialogId", DataFormat=DataFormat.TwosComplement)]
        public int HelloDialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

