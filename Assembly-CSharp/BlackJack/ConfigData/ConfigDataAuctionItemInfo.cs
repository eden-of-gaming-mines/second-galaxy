﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataAuctionItemInfo")]
    public class ConfigDataAuctionItemInfo : IExtensible
    {
        private int _ID;
        private NpcShopItemType _AuctionItemType;
        private StoreItemType _ItemType;
        private int _ItemId;
        private int _DefaultBuyCount;
        private int _MarketPrice;
        private bool _IsGuildPurchase;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AuctionItemType;
        private static DelegateBridge __Hotfix_set_AuctionItemType;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_ItemId;
        private static DelegateBridge __Hotfix_set_ItemId;
        private static DelegateBridge __Hotfix_get_DefaultBuyCount;
        private static DelegateBridge __Hotfix_set_DefaultBuyCount;
        private static DelegateBridge __Hotfix_get_MarketPrice;
        private static DelegateBridge __Hotfix_set_MarketPrice;
        private static DelegateBridge __Hotfix_get_IsGuildPurchase;
        private static DelegateBridge __Hotfix_set_IsGuildPurchase;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AuctionItemType", DataFormat=DataFormat.TwosComplement)]
        public NpcShopItemType AuctionItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ItemId", DataFormat=DataFormat.TwosComplement)]
        public int ItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="DefaultBuyCount", DataFormat=DataFormat.TwosComplement)]
        public int DefaultBuyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="MarketPrice", DataFormat=DataFormat.TwosComplement)]
        public int MarketPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IsGuildPurchase", DataFormat=DataFormat.Default)]
        public bool IsGuildPurchase
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

