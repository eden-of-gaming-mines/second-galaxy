﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HelloDailogInfo")]
    public class HelloDailogInfo : IExtensible
    {
        private NpcPersonalityType _Personality;
        private int _HelloDialogId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Personality;
        private static DelegateBridge __Hotfix_set_Personality;
        private static DelegateBridge __Hotfix_get_HelloDialogId;
        private static DelegateBridge __Hotfix_set_HelloDialogId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Personality", DataFormat=DataFormat.TwosComplement)]
        public NpcPersonalityType Personality
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HelloDialogId", DataFormat=DataFormat.TwosComplement)]
        public int HelloDialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

