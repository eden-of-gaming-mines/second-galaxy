﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildProduceCategory")]
    public enum GuildProduceCategory
    {
        [ProtoEnum(Name="GuildProduceCategory_Invalid", Value=0)]
        GuildProduceCategory_Invalid = 0,
        [ProtoEnum(Name="GuildProduceCategory_BuildingProduceTemplete", Value=1)]
        GuildProduceCategory_BuildingProduceTemplete = 1,
        [ProtoEnum(Name="GuildProduceCategory_ShipProduceTemplete", Value=2)]
        GuildProduceCategory_ShipProduceTemplete = 2,
        [ProtoEnum(Name="GuildProduceCategory_Max", Value=3)]
        GuildProduceCategory_Max = 3
    }
}

