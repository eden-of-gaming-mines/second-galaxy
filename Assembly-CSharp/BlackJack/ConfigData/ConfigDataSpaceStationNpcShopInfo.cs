﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSpaceStationNpcShopInfo")]
    public class ConfigDataSpaceStationNpcShopInfo : IExtensible
    {
        private int _ID;
        private GrandFaction _SpaceStationGrandFaction;
        private BlackJack.ConfigData.SpaceStationType _SpaceStationType;
        private int _NPCShopItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SpaceStationGrandFaction;
        private static DelegateBridge __Hotfix_set_SpaceStationGrandFaction;
        private static DelegateBridge __Hotfix_get_SpaceStationType;
        private static DelegateBridge __Hotfix_set_SpaceStationType;
        private static DelegateBridge __Hotfix_get_NPCShopItemList;
        private static DelegateBridge __Hotfix_set_NPCShopItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SpaceStationGrandFaction", DataFormat=DataFormat.TwosComplement)]
        public GrandFaction SpaceStationGrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SpaceStationType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SpaceStationType SpaceStationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NPCShopItemList", DataFormat=DataFormat.TwosComplement)]
        public int NPCShopItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

