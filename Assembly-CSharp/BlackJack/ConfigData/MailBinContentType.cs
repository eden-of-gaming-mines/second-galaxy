﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MailBinContentType")]
    public enum MailBinContentType
    {
        [ProtoEnum(Name="MailBinContentType_None", Value=1)]
        MailBinContentType_None = 1,
        [ProtoEnum(Name="MailBinContentType_Voice", Value=2)]
        MailBinContentType_Voice = 2,
        [ProtoEnum(Name="MailBinContentType_Quest", Value=3)]
        MailBinContentType_Quest = 3,
        [ProtoEnum(Name="MailBinContentType_PlayerKilled", Value=4)]
        MailBinContentType_PlayerKilled = 4,
        [ProtoEnum(Name="MailBinContentType_CaptainKilled", Value=5)]
        MailBinContentType_CaptainKilled = 5,
        [ProtoEnum(Name="MailBinContentType_StationRedploy", Value=6)]
        MailBinContentType_StationRedploy = 6,
        [ProtoEnum(Name="MailBinContentType_DrivingLicense", Value=7)]
        MailBinContentType_DrivingLicense = 7,
        [ProtoEnum(Name="MailBinContentType_Produce", Value=8)]
        MailBinContentType_Produce = 8
    }
}

