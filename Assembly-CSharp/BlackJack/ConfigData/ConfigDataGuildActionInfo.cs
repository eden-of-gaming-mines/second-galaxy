﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildActionInfo")]
    public class ConfigDataGuildActionInfo : IExtensible
    {
        private int _ID;
        private int _BasicInformationPointCost;
        private readonly List<float> _InformationPointCostDifficultLevelMulti;
        private readonly List<float> _InformationPointCostJumpMulti;
        private int _NeedSolarSystemSovereignLevel;
        private bool _NeedSolarSystemSovereign;
        private int _ExpiryTime;
        private int _SceneId;
        private readonly List<ShipType> _ShipTypeLimit;
        private int _RewardListId;
        private string _PicResString;
        private int _DropReward;
        private string _ActionIcon;
        private int _RecommandNumberMin;
        private int _RecommandNumberMax;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_BasicInformationPointCost;
        private static DelegateBridge __Hotfix_set_BasicInformationPointCost;
        private static DelegateBridge __Hotfix_get_InformationPointCostDifficultLevelMulti;
        private static DelegateBridge __Hotfix_get_InformationPointCostJumpMulti;
        private static DelegateBridge __Hotfix_get_NeedSolarSystemSovereignLevel;
        private static DelegateBridge __Hotfix_set_NeedSolarSystemSovereignLevel;
        private static DelegateBridge __Hotfix_get_NeedSolarSystemSovereign;
        private static DelegateBridge __Hotfix_set_NeedSolarSystemSovereign;
        private static DelegateBridge __Hotfix_get_ExpiryTime;
        private static DelegateBridge __Hotfix_set_ExpiryTime;
        private static DelegateBridge __Hotfix_get_SceneId;
        private static DelegateBridge __Hotfix_set_SceneId;
        private static DelegateBridge __Hotfix_get_ShipTypeLimit;
        private static DelegateBridge __Hotfix_get_RewardListId;
        private static DelegateBridge __Hotfix_set_RewardListId;
        private static DelegateBridge __Hotfix_get_PicResString;
        private static DelegateBridge __Hotfix_set_PicResString;
        private static DelegateBridge __Hotfix_get_DropReward;
        private static DelegateBridge __Hotfix_set_DropReward;
        private static DelegateBridge __Hotfix_get_ActionIcon;
        private static DelegateBridge __Hotfix_set_ActionIcon;
        private static DelegateBridge __Hotfix_get_RecommandNumberMin;
        private static DelegateBridge __Hotfix_set_RecommandNumberMin;
        private static DelegateBridge __Hotfix_get_RecommandNumberMax;
        private static DelegateBridge __Hotfix_set_RecommandNumberMax;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="BasicInformationPointCost", DataFormat=DataFormat.TwosComplement)]
        public int BasicInformationPointCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="InformationPointCostDifficultLevelMulti", DataFormat=DataFormat.FixedSize)]
        public List<float> InformationPointCostDifficultLevelMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="InformationPointCostJumpMulti", DataFormat=DataFormat.FixedSize)]
        public List<float> InformationPointCostJumpMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="NeedSolarSystemSovereignLevel", DataFormat=DataFormat.TwosComplement)]
        public int NeedSolarSystemSovereignLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="NeedSolarSystemSovereign", DataFormat=DataFormat.Default)]
        public bool NeedSolarSystemSovereign
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="ExpiryTime", DataFormat=DataFormat.TwosComplement)]
        public int ExpiryTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="SceneId", DataFormat=DataFormat.TwosComplement)]
        public int SceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="ShipTypeLimit", DataFormat=DataFormat.TwosComplement)]
        public List<ShipType> ShipTypeLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="RewardListId", DataFormat=DataFormat.TwosComplement)]
        public int RewardListId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="PicResString", DataFormat=DataFormat.Default)]
        public string PicResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DropReward", DataFormat=DataFormat.TwosComplement)]
        public int DropReward
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="ActionIcon", DataFormat=DataFormat.Default)]
        public string ActionIcon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="RecommandNumberMin", DataFormat=DataFormat.TwosComplement)]
        public int RecommandNumberMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="RecommandNumberMax", DataFormat=DataFormat.TwosComplement)]
        public int RecommandNumberMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

