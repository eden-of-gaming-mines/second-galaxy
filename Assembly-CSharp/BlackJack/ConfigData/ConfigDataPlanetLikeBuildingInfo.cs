﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPlanetLikeBuildingInfo")]
    public class ConfigDataPlanetLikeBuildingInfo : IExtensible
    {
        private int _ID;
        private string _ArtResource;
        private int _ArroundDistance;
        private int _JumpDistance;
        private int _PVPSecurityDistance;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ArtResource;
        private static DelegateBridge __Hotfix_set_ArtResource;
        private static DelegateBridge __Hotfix_get_ArroundDistance;
        private static DelegateBridge __Hotfix_set_ArroundDistance;
        private static DelegateBridge __Hotfix_get_JumpDistance;
        private static DelegateBridge __Hotfix_set_JumpDistance;
        private static DelegateBridge __Hotfix_get_PVPSecurityDistance;
        private static DelegateBridge __Hotfix_set_PVPSecurityDistance;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ArtResource", DataFormat=DataFormat.Default)]
        public string ArtResource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ArroundDistance", DataFormat=DataFormat.TwosComplement)]
        public int ArroundDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="JumpDistance", DataFormat=DataFormat.TwosComplement)]
        public int JumpDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="PVPSecurityDistance", DataFormat=DataFormat.TwosComplement)]
        public int PVPSecurityDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

