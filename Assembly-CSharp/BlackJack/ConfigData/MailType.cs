﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MailType")]
    public enum MailType
    {
        [ProtoEnum(Name="MailType_Player2Player", Value=1)]
        MailType_Player2Player = 1,
        [ProtoEnum(Name="MailType_Guild", Value=2)]
        MailType_Guild = 2,
        [ProtoEnum(Name="MailType_SysNormal", Value=3)]
        MailType_SysNormal = 3,
        [ProtoEnum(Name="MailType_SysQuestReward", Value=4)]
        MailType_SysQuestReward = 4,
        [ProtoEnum(Name="MailType_SysInfectCompleteReward", Value=5)]
        MailType_SysInfectCompleteReward = 5,
        [ProtoEnum(Name="MailType_SysCaptainKilled", Value=6)]
        MailType_SysCaptainKilled = 6,
        [ProtoEnum(Name="MailType_SysPlayerKilled", Value=7)]
        MailType_SysPlayerKilled = 7,
        [ProtoEnum(Name="MailType_SysContinousCriminalSet", Value=8)]
        MailType_SysContinousCriminalSet = 8,
        [ProtoEnum(Name="MailType_SysContinousCriminalUnSet", Value=9)]
        MailType_SysContinousCriminalUnSet = 9,
        [ProtoEnum(Name="MailType_SysStationRedploy", Value=10)]
        MailType_SysStationRedploy = 10,
        [ProtoEnum(Name="MailType_SysDrivingLicense", Value=11)]
        MailType_SysDrivingLicense = 11,
        [ProtoEnum(Name="MailType_SysProduce", Value=12)]
        MailType_SysProduce = 12,
        [ProtoEnum(Name="MailType_Auction", Value=13)]
        MailType_Auction = 13,
        [ProtoEnum(Name="MailType_Alliance", Value=14)]
        MailType_Alliance = 14
    }
}

