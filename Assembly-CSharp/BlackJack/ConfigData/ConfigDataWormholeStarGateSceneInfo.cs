﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataWormholeStarGateSceneInfo")]
    public class ConfigDataWormholeStarGateSceneInfo : IExtensible
    {
        private int _ID;
        private int _Level;
        private int _EntrySceneID;
        private int _WormholeTunnelSceneID;
        private int _WormholeEntrySceneID;
        private int _WormholeBackPointSceneID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_EntrySceneID;
        private static DelegateBridge __Hotfix_set_EntrySceneID;
        private static DelegateBridge __Hotfix_get_WormholeTunnelSceneID;
        private static DelegateBridge __Hotfix_set_WormholeTunnelSceneID;
        private static DelegateBridge __Hotfix_get_WormholeEntrySceneID;
        private static DelegateBridge __Hotfix_set_WormholeEntrySceneID;
        private static DelegateBridge __Hotfix_get_WormholeBackPointSceneID;
        private static DelegateBridge __Hotfix_set_WormholeBackPointSceneID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EntrySceneID", DataFormat=DataFormat.TwosComplement)]
        public int EntrySceneID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="WormholeTunnelSceneID", DataFormat=DataFormat.TwosComplement)]
        public int WormholeTunnelSceneID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="WormholeEntrySceneID", DataFormat=DataFormat.TwosComplement)]
        public int WormholeEntrySceneID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="WormholeBackPointSceneID", DataFormat=DataFormat.TwosComplement)]
        public int WormholeBackPointSceneID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

