﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainFeatsLevelInfo")]
    public class ConfigDataNpcCaptainFeatsLevelInfo : IExtensible
    {
        private int _ID;
        private int _Level;
        private int _PassiveSkillLevel;
        private int _PassiveSkillId;
        private int _NpcCaptainShipInfoId;
        private int _DependIndexInGrowthLine;
        private double _RetireReserveFeatRate;
        private int _FeatsScore;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_PassiveSkillLevel;
        private static DelegateBridge __Hotfix_set_PassiveSkillLevel;
        private static DelegateBridge __Hotfix_get_PassiveSkillId;
        private static DelegateBridge __Hotfix_set_PassiveSkillId;
        private static DelegateBridge __Hotfix_get_NpcCaptainShipInfoId;
        private static DelegateBridge __Hotfix_set_NpcCaptainShipInfoId;
        private static DelegateBridge __Hotfix_get_DependIndexInGrowthLine;
        private static DelegateBridge __Hotfix_set_DependIndexInGrowthLine;
        private static DelegateBridge __Hotfix_get_RetireReserveFeatRate;
        private static DelegateBridge __Hotfix_set_RetireReserveFeatRate;
        private static DelegateBridge __Hotfix_get_FeatsScore;
        private static DelegateBridge __Hotfix_set_FeatsScore;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PassiveSkillLevel", DataFormat=DataFormat.TwosComplement)]
        public int PassiveSkillLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PassiveSkillId", DataFormat=DataFormat.TwosComplement)]
        public int PassiveSkillId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NpcCaptainShipInfoId", DataFormat=DataFormat.TwosComplement)]
        public int NpcCaptainShipInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DependIndexInGrowthLine", DataFormat=DataFormat.TwosComplement)]
        public int DependIndexInGrowthLine
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="RetireReserveFeatRate", DataFormat=DataFormat.TwosComplement)]
        public double RetireReserveFeatRate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="FeatsScore", DataFormat=DataFormat.TwosComplement)]
        public int FeatsScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

