﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GMSystemFuncType")]
    public enum GMSystemFuncType
    {
        [ProtoEnum(Name="GMSystemFuncType_None", Value=0)]
        GMSystemFuncType_None = 0,
        [ProtoEnum(Name="GMSystemFuncType_BaseRedeploy", Value=1)]
        GMSystemFuncType_BaseRedeploy = 1,
        [ProtoEnum(Name="GMSystemFuncType_BaseRedeploySpeedUp", Value=2)]
        GMSystemFuncType_BaseRedeploySpeedUp = 2,
        [ProtoEnum(Name="GMSystemFuncType_TechUpgrade", Value=3)]
        GMSystemFuncType_TechUpgrade = 3,
        [ProtoEnum(Name="GMSystemFuncType_TechUpgradeSpeedUp", Value=4)]
        GMSystemFuncType_TechUpgradeSpeedUp = 4,
        [ProtoEnum(Name="GMSystemFuncType_StoreItemUse", Value=5)]
        GMSystemFuncType_StoreItemUse = 5,
        [ProtoEnum(Name="GMSystemFuncType_CaptainWingMan", Value=6)]
        GMSystemFuncType_CaptainWingMan = 6,
        [ProtoEnum(Name="GMSystemFuncType_CaptainTrain", Value=7)]
        GMSystemFuncType_CaptainTrain = 7,
        [ProtoEnum(Name="GMSystemFuncType_CaptainRetire", Value=8)]
        GMSystemFuncType_CaptainRetire = 8,
        [ProtoEnum(Name="GMSystemFuncType_CaptainFeatsLearn", Value=9)]
        GMSystemFuncType_CaptainFeatsLearn = 9,
        [ProtoEnum(Name="GMSystemFuncType_HangarUnpackShip", Value=10)]
        GMSystemFuncType_HangarUnpackShip = 10,
        [ProtoEnum(Name="GMSystemFuncType_HangarEquipHighSlot", Value=11)]
        GMSystemFuncType_HangarEquipHighSlot = 11,
        [ProtoEnum(Name="GMSystemFuncType_HangarEquipMiddleSlot", Value=12)]
        GMSystemFuncType_HangarEquipMiddleSlot = 12,
        [ProtoEnum(Name="GMSystemFuncType_HangarEquipLowSlot", Value=13)]
        GMSystemFuncType_HangarEquipLowSlot = 13,
        [ProtoEnum(Name="GMSystemFuncType_HangarChangeAmmo", Value=14)]
        GMSystemFuncType_HangarChangeAmmo = 14,
        [ProtoEnum(Name="GMSystemFuncType_HangarBuyAmmo", Value=15)]
        GMSystemFuncType_HangarBuyAmmo = 15,
        [ProtoEnum(Name="GMSystemFuncType_HangarTemplateUpdate", Value=0x10)]
        GMSystemFuncType_HangarTemplateUpdate = 0x10,
        [ProtoEnum(Name="GMSystemFuncType_HangarTemplateApply", Value=0x11)]
        GMSystemFuncType_HangarTemplateApply = 0x11,
        [ProtoEnum(Name="GMSystemFuncType_HangarPackShip", Value=0x12)]
        GMSystemFuncType_HangarPackShip = 0x12,
        [ProtoEnum(Name="GMSystemFuncType_CrackQueueAdd", Value=0x13)]
        GMSystemFuncType_CrackQueueAdd = 0x13,
        [ProtoEnum(Name="GMSystemFuncType_CrackSpeedUp", Value=20)]
        GMSystemFuncType_CrackSpeedUp = 20,
        [ProtoEnum(Name="GMSystemFuncType_CrackedBoxGet", Value=0x15)]
        GMSystemFuncType_CrackedBoxGet = 0x15,
        [ProtoEnum(Name="GMSystemFuncType_CrackSlotExtend", Value=0x16)]
        GMSystemFuncType_CrackSlotExtend = 0x16,
        [ProtoEnum(Name="GMSystemFuncType_ProductionStart", Value=0x17)]
        GMSystemFuncType_ProductionStart = 0x17,
        [ProtoEnum(Name="GMSystemFuncType_ProductionCancel", Value=0x18)]
        GMSystemFuncType_ProductionCancel = 0x18,
        [ProtoEnum(Name="GMSystemFuncType_ProductionGet", Value=0x19)]
        GMSystemFuncType_ProductionGet = 0x19,
        [ProtoEnum(Name="GMSystemFuncType_NpcShopBuy", Value=0x1a)]
        GMSystemFuncType_NpcShopBuy = 0x1a,
        [ProtoEnum(Name="GMSystemFuncType_NpcShopSale", Value=0x1b)]
        GMSystemFuncType_NpcShopSale = 0x1b,
        [ProtoEnum(Name="GMSystemFuncType_AuctionItemBuy", Value=0x1c)]
        GMSystemFuncType_AuctionItemBuy = 0x1c,
        [ProtoEnum(Name="GMSystemFuncType_AuctionItemOnShelve", Value=0x1d)]
        GMSystemFuncType_AuctionItemOnShelve = 0x1d,
        [ProtoEnum(Name="GMSystemFuncType_AuctionItemUnShelve", Value=30)]
        GMSystemFuncType_AuctionItemUnShelve = 30,
        [ProtoEnum(Name="GMSystemFuncType_RankingListUpdate", Value=0x1f)]
        GMSystemFuncType_RankingListUpdate = 0x1f,
        [ProtoEnum(Name="GMSystemFuncType_BlackMarketExchangePersonal", Value=0x20)]
        GMSystemFuncType_BlackMarketExchangePersonal = 0x20,
        [ProtoEnum(Name="GMSystemFuncType_BlackMarketExchangeGuild", Value=0x21)]
        GMSystemFuncType_BlackMarketExchangeGuild = 0x21,
        [ProtoEnum(Name="GMSystemFuncType_Develpment", Value=0x22)]
        GMSystemFuncType_Develpment = 0x22,
        [ProtoEnum(Name="GMSystemFuncType_Billboard", Value=0x23)]
        GMSystemFuncType_Billboard = 0x23,
        [ProtoEnum(Name="GMSystemFuncType_FightUseSuper", Value=0x24)]
        GMSystemFuncType_FightUseSuper = 0x24,
        [ProtoEnum(Name="GMSystemFuncType_FightUseHighSlot", Value=0x25)]
        GMSystemFuncType_FightUseHighSlot = 0x25,
        [ProtoEnum(Name="GMSystemFuncType_FightUseMiddleSlot", Value=0x26)]
        GMSystemFuncType_FightUseMiddleSlot = 0x26,
        [ProtoEnum(Name="GMSystemFuncType_FightUseDefault", Value=0x27)]
        GMSystemFuncType_FightUseDefault = 0x27,
        [ProtoEnum(Name="GMSystemFuncType_FightCaptainWingman", Value=40)]
        GMSystemFuncType_FightCaptainWingman = 40,
        [ProtoEnum(Name="GMSystemFuncType_FightAuto", Value=0x29)]
        GMSystemFuncType_FightAuto = 0x29,
        [ProtoEnum(Name="GMSystemFuncType_TravelJump", Value=0x2b)]
        GMSystemFuncType_TravelJump = 0x2b,
        [ProtoEnum(Name="GMSystemFuncType_TravelStarGate", Value=0x2c)]
        GMSystemFuncType_TravelStarGate = 0x2c,
        [ProtoEnum(Name="GMSystemFuncType_ScanProbeManualUse", Value=0x2d)]
        GMSystemFuncType_ScanProbeManualUse = 0x2d,
        [ProtoEnum(Name="GMSystemFuncType_ScanProbeDelegateUse", Value=0x2e)]
        GMSystemFuncType_ScanProbeDelegateUse = 0x2e,
        [ProtoEnum(Name="GMSystemFuncType_ScanProbePVPUse", Value=0x2f)]
        GMSystemFuncType_ScanProbePVPUse = 0x2f,
        [ProtoEnum(Name="GMSystemFuncType_SignalNPCInvade", Value=0x30)]
        GMSystemFuncType_SignalNPCInvade = 0x30,
        [ProtoEnum(Name="GMSystemFuncType_SignalEmergency", Value=0x31)]
        GMSystemFuncType_SignalEmergency = 0x31,
        [ProtoEnum(Name="GMSystemFuncType_DelegateMission", Value=50)]
        GMSystemFuncType_DelegateMission = 50,
        [ProtoEnum(Name="GMSystemFuncType_WormholeEntryRefresh", Value=0x33)]
        GMSystemFuncType_WormholeEntryRefresh = 0x33,
        [ProtoEnum(Name="GMSystemFuncType_WormholeEnter", Value=0x34)]
        GMSystemFuncType_WormholeEnter = 0x34,
        [ProtoEnum(Name="GMSystemFuncType_InfectSolarSystemRefresh", Value=0x35)]
        GMSystemFuncType_InfectSolarSystemRefresh = 0x35,
        [ProtoEnum(Name="GMSystemFuncType_InfectFinalBattle", Value=0x36)]
        GMSystemFuncType_InfectFinalBattle = 0x36,
        [ProtoEnum(Name="GMSystemFuncType_QuestPoolCountChange", Value=0x37)]
        GMSystemFuncType_QuestPoolCountChange = 0x37,
        [ProtoEnum(Name="GMSystemFuncType_DelegatePoolCountChange", Value=0x38)]
        GMSystemFuncType_DelegatePoolCountChange = 0x38,
        [ProtoEnum(Name="GMSystemFuncType_CharChipSet", Value=0x39)]
        GMSystemFuncType_CharChipSet = 0x39,
        [ProtoEnum(Name="GMSystemFuncType_CharBasicPropertiesAdd", Value=0x3a)]
        GMSystemFuncType_CharBasicPropertiesAdd = 0x3a,
        [ProtoEnum(Name="GMSystemFuncType_CharBasicPropertiesClear", Value=0x3b)]
        GMSystemFuncType_CharBasicPropertiesClear = 0x3b,
        [ProtoEnum(Name="GMSystemFuncType_CharDrivingLicenseExamine", Value=60)]
        GMSystemFuncType_CharDrivingLicenseExamine = 60,
        [ProtoEnum(Name="GMSystemFuncType_CharSkillLearn", Value=0x3d)]
        GMSystemFuncType_CharSkillLearn = 0x3d,
        [ProtoEnum(Name="GMSystemFuncType_QuestAccept", Value=0x3e)]
        GMSystemFuncType_QuestAccept = 0x3e,
        [ProtoEnum(Name="GMSystemFuncType_QuestCancel", Value=0x3f)]
        GMSystemFuncType_QuestCancel = 0x3f,
        [ProtoEnum(Name="GMSystemFuncType_VitalityGetReward", Value=0x40)]
        GMSystemFuncType_VitalityGetReward = 0x40,
        [ProtoEnum(Name="GMSystemFuncType_GuildCreate", Value=0x41)]
        GMSystemFuncType_GuildCreate = 0x41,
        [ProtoEnum(Name="GMSystemFuncType_GuildDismiss", Value=0x42)]
        GMSystemFuncType_GuildDismiss = 0x42,
        [ProtoEnum(Name="GMSystemFuncType_GuildBaseSolarSystemSet", Value=0x43)]
        GMSystemFuncType_GuildBaseSolarSystemSet = 0x43,
        [ProtoEnum(Name="GMSystemFuncType_GuildMemberJobUpdate", Value=0x44)]
        GMSystemFuncType_GuildMemberJobUpdate = 0x44,
        [ProtoEnum(Name="GMSystemFuncType_Dummy1", Value=0x45)]
        GMSystemFuncType_Dummy1 = 0x45,
        [ProtoEnum(Name="GMSystemFuncType_GuildMessageWrite", Value=70)]
        GMSystemFuncType_GuildMessageWrite = 70,
        [ProtoEnum(Name="GMSystemFuncType_Dummy2", Value=0x47)]
        GMSystemFuncType_Dummy2 = 0x47,
        [ProtoEnum(Name="GMSystemFuncType_GuildDiplomacyUpdate", Value=0x48)]
        GMSystemFuncType_GuildDiplomacyUpdate = 0x48,
        [ProtoEnum(Name="GMSystemFuncType_GuildBasicInfoChange", Value=0x49)]
        GMSystemFuncType_GuildBasicInfoChange = 0x49,
        [ProtoEnum(Name="GMSystemFuncType_Dummy3", Value=0x4a)]
        GMSystemFuncType_Dummy3 = 0x4a,
        [ProtoEnum(Name="GMSystemFuncType_GuildMailSend", Value=0x4b)]
        GMSystemFuncType_GuildMailSend = 0x4b,
        [ProtoEnum(Name="GMSystemFuncType_GuildDelareWar", Value=0x4c)]
        GMSystemFuncType_GuildDelareWar = 0x4c,
        [ProtoEnum(Name="GMSystemFuncType_GuildActionCreate", Value=0x4d)]
        GMSystemFuncType_GuildActionCreate = 0x4d,
        [ProtoEnum(Name="GMSystemFuncType_GuildFacility", Value=0x4e)]
        GMSystemFuncType_GuildFacility = 0x4e,
        [ProtoEnum(Name="GMSystemFuncType_GuildPurchaseOrderCreate", Value=0x4f)]
        GMSystemFuncType_GuildPurchaseOrderCreate = 0x4f,
        [ProtoEnum(Name="GMSystemFuncType_GuildBattleDamage", Value=80)]
        GMSystemFuncType_GuildBattleDamage = 80,
        [ProtoEnum(Name="GMSystemFuncType_GuildProduction", Value=0x51)]
        GMSystemFuncType_GuildProduction = 0x51,
        [ProtoEnum(Name="GMSystemFuncType_GuildProductionSpeedUp", Value=0x52)]
        GMSystemFuncType_GuildProductionSpeedUp = 0x52,
        [ProtoEnum(Name="GMSystemFuncType_ChatLocal", Value=0x53)]
        GMSystemFuncType_ChatLocal = 0x53,
        [ProtoEnum(Name="GMSystemFuncType_ChatSolarSystem", Value=0x54)]
        GMSystemFuncType_ChatSolarSystem = 0x54,
        [ProtoEnum(Name="GMSystemFuncType_ChatStarField", Value=0x55)]
        GMSystemFuncType_ChatStarField = 0x55,
        [ProtoEnum(Name="GMSystemFuncType_ChatGuild", Value=0x56)]
        GMSystemFuncType_ChatGuild = 0x56,
        [ProtoEnum(Name="GMSystemFuncType_ChatTeam", Value=0x57)]
        GMSystemFuncType_ChatTeam = 0x57,
        [ProtoEnum(Name="GMSystemFuncType_ChatWisper", Value=0x58)]
        GMSystemFuncType_ChatWisper = 0x58,
        [ProtoEnum(Name="GMSystemFuncType_MailSend", Value=0x59)]
        GMSystemFuncType_MailSend = 0x59,
        [ProtoEnum(Name="GMSystemFuncType_MailGetAttachment", Value=90)]
        GMSystemFuncType_MailGetAttachment = 90,
        [ProtoEnum(Name="GMSystemFuncType_MailRemove", Value=0x5b)]
        GMSystemFuncType_MailRemove = 0x5b,
        [ProtoEnum(Name="GMSystemFuncType_TeamCreate", Value=0x5c)]
        GMSystemFuncType_TeamCreate = 0x5c,
        [ProtoEnum(Name="GMSystemFuncType_TeamInvite", Value=0x5d)]
        GMSystemFuncType_TeamInvite = 0x5d,
        [ProtoEnum(Name="GMSystemFuncType_FactionCreditQuestAccept", Value=0x5e)]
        GMSystemFuncType_FactionCreditQuestAccept = 0x5e,
        [ProtoEnum(Name="GMSystemFuncType_BranchStoryQuestListStart", Value=0x5f)]
        GMSystemFuncType_BranchStoryQuestListStart = 0x5f,
        [ProtoEnum(Name="GMSystemFuncType_RechargeStoreBuyGoods", Value=0x60)]
        GMSystemFuncType_RechargeStoreBuyGoods = 0x60,
        [ProtoEnum(Name="GMSystemFuncType_RechargeStoreSubscribe", Value=0x61)]
        GMSystemFuncType_RechargeStoreSubscribe = 0x61,
        [ProtoEnum(Name="GMSystemFuncType_GuildPanGalaShop", Value=0x62)]
        GMSystemFuncType_GuildPanGalaShop = 0x62,
        [ProtoEnum(Name="GMSystemFuncType_PanGalaShop", Value=0x63)]
        GMSystemFuncType_PanGalaShop = 0x63,
        [ProtoEnum(Name="GMSystemFuncType_FactionCreditShip", Value=100)]
        GMSystemFuncType_FactionCreditShip = 100,
        [ProtoEnum(Name="GMSystemFuncType_Battlepass", Value=0x65)]
        GMSystemFuncType_Battlepass = 0x65,
        [ProtoEnum(Name="GMSystemFuncType_PersonalTrade", Value=0x66)]
        GMSystemFuncType_PersonalTrade = 0x66,
        [ProtoEnum(Name="GMSystemFuncType_SpaceSignal", Value=0x67)]
        GMSystemFuncType_SpaceSignal = 0x67,
        [ProtoEnum(Name="GMSystemFuncType_GuildTrade", Value=0x68)]
        GMSystemFuncType_GuildTrade = 0x68,
        [ProtoEnum(Name="GMSystemFuncType_ShipSalvage", Value=0x69)]
        GMSystemFuncType_ShipSalvage = 0x69,
        [ProtoEnum(Name="GMSystemFuncType_AllianceCreate", Value=0x6a)]
        GMSystemFuncType_AllianceCreate = 0x6a,
        [ProtoEnum(Name="GMSystemFuncType_AllianceInvite", Value=0x6b)]
        GMSystemFuncType_AllianceInvite = 0x6b,
        [ProtoEnum(Name="GMSystemFuncType_GuildBenefitsReceive", Value=0x6c)]
        GMSystemFuncType_GuildBenefitsReceive = 0x6c,
        [ProtoEnum(Name="GMSystemFuncType_BlackMarketBuyNormalItem", Value=0x6d)]
        GMSystemFuncType_BlackMarketBuyNormalItem = 0x6d,
        [ProtoEnum(Name="GMSystemFuncType_FactionCreditLevelRewardReceive", Value=110)]
        GMSystemFuncType_FactionCreditLevelRewardReceive = 110,
        [ProtoEnum(Name="GMSystemFuncType_FactionCreditQuestWeeklyRewardReceive", Value=0x6f)]
        GMSystemFuncType_FactionCreditQuestWeeklyRewardReceive = 0x6f,
        [ProtoEnum(Name="GMSystemFuncType_ReplaceFlagShip", Value=0x70)]
        GMSystemFuncType_ReplaceFlagShip = 0x70,
        [ProtoEnum(Name="GMSystemFuncType_FlagShipJumping", Value=0x71)]
        GMSystemFuncType_FlagShipJumping = 0x71,
        [ProtoEnum(Name="GMSystemFuncType_GuildAntiTeleportEffectActivate", Value=0x72)]
        GMSystemFuncType_GuildAntiTeleportEffectActivate = 0x72,
        [ProtoEnum(Name="GMSystemFuncType_GuildMemberShipUseTeleportTunnel", Value=0x73)]
        GMSystemFuncType_GuildMemberShipUseTeleportTunnel = 0x73,
        [ProtoEnum(Name="GMSystemFuncType_ZLongFAQInfo", Value=0x74)]
        GMSystemFuncType_ZLongFAQInfo = 0x74,
        [ProtoEnum(Name="GMSystemFuncType_ZlongLoginActivity", Value=0x75)]
        GMSystemFuncType_ZlongLoginActivity = 0x75,
        [ProtoEnum(Name="GMSystemFuncType_LoginActivityDailyLogin", Value=0x76)]
        GMSystemFuncType_LoginActivityDailyLogin = 0x76,
        [ProtoEnum(Name="GMSystemFuncType_LoginActivityDailySign", Value=0x77)]
        GMSystemFuncType_LoginActivityDailySign = 0x77,
        [ProtoEnum(Name="GMSystemFuncType_CommanderAuth", Value=120)]
        GMSystemFuncType_CommanderAuth = 120,
        [ProtoEnum(Name="GMSystemFuncType_ZlongQuestionnaire", Value=0x79)]
        GMSystemFuncType_ZlongQuestionnaire = 0x79,
        [ProtoEnum(Name="GMSystemFuncType_Max", Value=0x7a)]
        GMSystemFuncType_Max = 0x7a
    }
}

