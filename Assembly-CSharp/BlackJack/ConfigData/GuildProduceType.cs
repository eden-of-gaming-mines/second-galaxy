﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildProduceType")]
    public enum GuildProduceType
    {
        [ProtoEnum(Name="GuildProduceType_Invalid", Value=0)]
        GuildProduceType_Invalid = 0,
        [ProtoEnum(Name="GuildProduceType_EnergyCenter", Value=1)]
        GuildProduceType_EnergyCenter = 1,
        [ProtoEnum(Name="GuildProduceType_MiningBuilding", Value=2)]
        GuildProduceType_MiningBuilding = 2,
        [ProtoEnum(Name="GuildProduceType_ProductionFactory", Value=3)]
        GuildProduceType_ProductionFactory = 3,
        [ProtoEnum(Name="GuildProduceType_GalaxyAlertTower", Value=4)]
        GuildProduceType_GalaxyAlertTower = 4,
        [ProtoEnum(Name="GuildProduceType_ShipDock", Value=5)]
        GuildProduceType_ShipDock = 5,
        [ProtoEnum(Name="GuildProduceType_TradePort", Value=6)]
        GuildProduceType_TradePort = 6,
        [ProtoEnum(Name="GuildProduceType_TeleportTunnel", Value=7)]
        GuildProduceType_TeleportTunnel = 7,
        [ProtoEnum(Name="GuildProduceType_AntiTeleport", Value=8)]
        GuildProduceType_AntiTeleport = 8,
        [ProtoEnum(Name="GuildProduceType_FlagShipEquip", Value=9)]
        GuildProduceType_FlagShipEquip = 9,
        [ProtoEnum(Name="GuildProduceType_FlagShipFuel", Value=10)]
        GuildProduceType_FlagShipFuel = 10,
        [ProtoEnum(Name="GuildProduceType_FlagShip_Tao", Value=11)]
        GuildProduceType_FlagShip_Tao = 11,
        [ProtoEnum(Name="GuildProduceType_FlagShip_Aigis", Value=12)]
        GuildProduceType_FlagShip_Aigis = 12,
        [ProtoEnum(Name="GuildProduceType_FlagShip_SolarCorona", Value=13)]
        GuildProduceType_FlagShip_SolarCorona = 13,
        [ProtoEnum(Name="GuildProduceType_FlagShip_Minaret", Value=14)]
        GuildProduceType_FlagShip_Minaret = 14,
        [ProtoEnum(Name="GuildProduceType_Max", Value=15)]
        GuildProduceType_Max = 15
    }
}

