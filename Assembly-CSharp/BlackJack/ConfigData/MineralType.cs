﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="MineralType")]
    public enum MineralType
    {
        [ProtoEnum(Name="MineralType_OxydeCrystal", Value=1)]
        MineralType_OxydeCrystal = 1,
        [ProtoEnum(Name="MineralType_HCPolymer", Value=2)]
        MineralType_HCPolymer = 2,
        [ProtoEnum(Name="MineralType_SemiconGel", Value=3)]
        MineralType_SemiconGel = 3,
        [ProtoEnum(Name="MineralType_IrTiAlloy", Value=4)]
        MineralType_IrTiAlloy = 4,
        [ProtoEnum(Name="MineralType_SCCeramic", Value=5)]
        MineralType_SCCeramic = 5,
        [ProtoEnum(Name="MineralType_Invalid", Value=6)]
        MineralType_Invalid = 6
    }
}

