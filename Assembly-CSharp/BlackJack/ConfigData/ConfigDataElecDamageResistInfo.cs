﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataElecDamageResistInfo")]
    public class ConfigDataElecDamageResistInfo : IExtensible
    {
        private int _ID;
        private float _ElecResistMove;
        private float _ElecResistWeapon;
        private float _ElecResistDefence;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ElecResistMove;
        private static DelegateBridge __Hotfix_set_ElecResistMove;
        private static DelegateBridge __Hotfix_get_ElecResistWeapon;
        private static DelegateBridge __Hotfix_set_ElecResistWeapon;
        private static DelegateBridge __Hotfix_get_ElecResistDefence;
        private static DelegateBridge __Hotfix_set_ElecResistDefence;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ElecResistMove", DataFormat=DataFormat.FixedSize)]
        public float ElecResistMove
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ElecResistWeapon", DataFormat=DataFormat.FixedSize)]
        public float ElecResistWeapon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ElecResistDefence", DataFormat=DataFormat.FixedSize)]
        public float ElecResistDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

