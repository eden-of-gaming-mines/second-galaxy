﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CameraShakeSourceType")]
    public enum CameraShakeSourceType
    {
        [ProtoEnum(Name="CameraShakeSourceType_Custom", Value=0)]
        CameraShakeSourceType_Custom = 0,
        [ProtoEnum(Name="CameraShakeSourceType_BeHit", Value=1)]
        CameraShakeSourceType_BeHit = 1,
        [ProtoEnum(Name="CameraShakeSourceType_Explore", Value=2)]
        CameraShakeSourceType_Explore = 2,
        [ProtoEnum(Name="CameraShakeSourceType_ShieldBroken", Value=3)]
        CameraShakeSourceType_ShieldBroken = 3,
        [ProtoEnum(Name="CameraShakeSourceType_SpeedUp", Value=4)]
        CameraShakeSourceType_SpeedUp = 4,
        [ProtoEnum(Name="CameraShakeSourceType_SuperGearTrigger", Value=5)]
        CameraShakeSourceType_SuperGearTrigger = 5
    }
}

