﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="FeatsBookType")]
    public enum FeatsBookType
    {
        [ProtoEnum(Name="FeatsBookType_NONE", Value=0)]
        FeatsBookType_NONE = 0,
        [ProtoEnum(Name="FeatsBookType_Fight", Value=1)]
        FeatsBookType_Fight = 1,
        [ProtoEnum(Name="FeatsBookType_Produce", Value=2)]
        FeatsBookType_Produce = 2,
        [ProtoEnum(Name="FeatsBookType_Tech", Value=3)]
        FeatsBookType_Tech = 3,
        [ProtoEnum(Name="FeatsBookType_Delegate", Value=4)]
        FeatsBookType_Delegate = 4
    }
}

