﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DroneType")]
    public enum DroneType
    {
        [ProtoEnum(Name="DroneType_Attacker", Value=1)]
        DroneType_Attacker = 1,
        [ProtoEnum(Name="DroneType_Defender", Value=2)]
        DroneType_Defender = 2,
        [ProtoEnum(Name="DroneType_Sniper", Value=3)]
        DroneType_Sniper = 3
    }
}

