﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GEConfigableConstId")]
    public enum GEConfigableConstId
    {
        [ProtoEnum(Name="GEConfigableConstId_AU", Value=1)]
        GEConfigableConstId_AU = 1,
        [ProtoEnum(Name="GEConfigableConstId_GravitationalConstant", Value=2)]
        GEConfigableConstId_GravitationalConstant = 2,
        [ProtoEnum(Name="GEConfigableConstId_MinMoonRadius", Value=3)]
        GEConfigableConstId_MinMoonRadius = 3,
        [ProtoEnum(Name="GEConfigableConstId_MaxMoonRadius", Value=4)]
        GEConfigableConstId_MaxMoonRadius = 4,
        [ProtoEnum(Name="GEConfigableConstId_MinDistanceBetweenMoonOrbit", Value=5)]
        GEConfigableConstId_MinDistanceBetweenMoonOrbit = 5,
        [ProtoEnum(Name="GEConfigableConstId_DifferenceTemperatureBetweenPlanetAndMoon", Value=6)]
        GEConfigableConstId_DifferenceTemperatureBetweenPlanetAndMoon = 6,
        [ProtoEnum(Name="GEConfigableConstId_StandardMoonOrbitRadius", Value=7)]
        GEConfigableConstId_StandardMoonOrbitRadius = 7,
        [ProtoEnum(Name="GEConfigableConstId_MoonOrbitRadiusFloatPercent", Value=8)]
        GEConfigableConstId_MoonOrbitRadiusFloatPercent = 8,
        [ProtoEnum(Name="GEConfigableConstId_MinEccentricityOfNormalPlanet", Value=9)]
        GEConfigableConstId_MinEccentricityOfNormalPlanet = 9,
        [ProtoEnum(Name="GEConfigableConstId_MaxEccentricityOfNormalPlanet", Value=10)]
        GEConfigableConstId_MaxEccentricityOfNormalPlanet = 10,
        [ProtoEnum(Name="GEConfigableConstId_MinOrbitDistanceBetweenPlanets", Value=11)]
        GEConfigableConstId_MinOrbitDistanceBetweenPlanets = 11,
        [ProtoEnum(Name="GEConfigableConstId_MinSpecialEccentricityOfOutboardPlanet", Value=12)]
        GEConfigableConstId_MinSpecialEccentricityOfOutboardPlanet = 12,
        [ProtoEnum(Name="GEConfigableConstId_MaxSpecialEccentricityOfOutboardPlanet", Value=13)]
        GEConfigableConstId_MaxSpecialEccentricityOfOutboardPlanet = 13,
        [ProtoEnum(Name="GEConfigableConstId_SpecialEccentricityAppearProbability", Value=14)]
        GEConfigableConstId_SpecialEccentricityAppearProbability = 14,
        [ProtoEnum(Name="GEConfigableConstId_PlanetRadiusFloatPercent", Value=15)]
        GEConfigableConstId_PlanetRadiusFloatPercent = 15,
        [ProtoEnum(Name="GEConfigableConstId_PlanetMassFloatPercent", Value=0x10)]
        GEConfigableConstId_PlanetMassFloatPercent = 0x10,
        [ProtoEnum(Name="GEConfigableConstId_PlanetTemperatureFloatPercent", Value=0x11)]
        GEConfigableConstId_PlanetTemperatureFloatPercent = 0x11,
        [ProtoEnum(Name="GEConfigableConstId_PlanetElevationRangeValue", Value=0x12)]
        GEConfigableConstId_PlanetElevationRangeValue = 0x12,
        [ProtoEnum(Name="GEConfigableConstId_RockyPlanetTemperatureFloatPercent", Value=0x13)]
        GEConfigableConstId_RockyPlanetTemperatureFloatPercent = 0x13,
        [ProtoEnum(Name="GEConfigableConstId_StarRaidusFloatPercent", Value=20)]
        GEConfigableConstId_StarRaidusFloatPercent = 20,
        [ProtoEnum(Name="GEConfigableConstId_StarBrightnessFloatPercent", Value=0x15)]
        GEConfigableConstId_StarBrightnessFloatPercent = 0x15,
        [ProtoEnum(Name="GEConfigableConstId_StarTemperatureFloatPercent", Value=0x16)]
        GEConfigableConstId_StarTemperatureFloatPercent = 0x16,
        [ProtoEnum(Name="GEConfigableConstId_MinStarAge", Value=0x17)]
        GEConfigableConstId_MinStarAge = 0x17,
        [ProtoEnum(Name="GEConfigableConstId_MaxStarAge", Value=0x18)]
        GEConfigableConstId_MaxStarAge = 0x18,
        [ProtoEnum(Name="GEConfigableConstId_SolarSystemAveragePlanetCount", Value=0x19)]
        GEConfigableConstId_SolarSystemAveragePlanetCount = 0x19,
        [ProtoEnum(Name="GEConfigableConstId_SolarSystemFloatPlanetCount", Value=0x1a)]
        GEConfigableConstId_SolarSystemFloatPlanetCount = 0x1a,
        [ProtoEnum(Name="GEConfigableConstId_SpaceStationOrbitRadius", Value=0x1b)]
        GEConfigableConstId_SpaceStationOrbitRadius = 0x1b,
        [ProtoEnum(Name="GEConfigableConstId_MaxSpaceStationCountForPlanet", Value=0x1c)]
        GEConfigableConstId_MaxSpaceStationCountForPlanet = 0x1c,
        [ProtoEnum(Name="GEConfigableConstId_MinDistanceBetweenSpaceStation", Value=0x1d)]
        GEConfigableConstId_MinDistanceBetweenSpaceStation = 0x1d,
        [ProtoEnum(Name="GEConfigableConstId_MinAsteroidBeltOrbitRadius", Value=30)]
        GEConfigableConstId_MinAsteroidBeltOrbitRadius = 30,
        [ProtoEnum(Name="GEConfigableConstId_MaxAsteroidBeltOrbitRadius", Value=0x1f)]
        GEConfigableConstId_MaxAsteroidBeltOrbitRadius = 0x1f,
        [ProtoEnum(Name="GEConfigableConstId_AsteroidBeltCount", Value=0x20)]
        GEConfigableConstId_AsteroidBeltCount = 0x20,
        [ProtoEnum(Name="GEConfigableConstId_MinDistanceBetweenAsteroidBelt", Value=0x21)]
        GEConfigableConstId_MinDistanceBetweenAsteroidBelt = 0x21,
        [ProtoEnum(Name="GEConfigableConstId_SpaceObjectScaleRatio", Value=0x22)]
        GEConfigableConstId_SpaceObjectScaleRatio = 0x22,
        [ProtoEnum(Name="GEConfigableConstId_SpaceObjectDistanceScaleRatio", Value=0x23)]
        GEConfigableConstId_SpaceObjectDistanceScaleRatio = 0x23,
        [ProtoEnum(Name="GEConfigableConstId_SunRadiusValue", Value=0x24)]
        GEConfigableConstId_SunRadiusValue = 0x24,
        [ProtoEnum(Name="GEConfigableConstId_SunBrightnessValue", Value=0x25)]
        GEConfigableConstId_SunBrightnessValue = 0x25,
        [ProtoEnum(Name="GEConfigableConstId_SunTemperatureValue", Value=0x26)]
        GEConfigableConstId_SunTemperatureValue = 0x26,
        [ProtoEnum(Name="GEConfigableConstId_SpaceStationFixScaleRatio", Value=0x27)]
        GEConfigableConstId_SpaceStationFixScaleRatio = 0x27,
        [ProtoEnum(Name="GEConfigableConstId_OrbUnityObjectFixRadius", Value=40)]
        GEConfigableConstId_OrbUnityObjectFixRadius = 40,
        [ProtoEnum(Name="GEConfigableConstId_SolarSystemRandomDicMin", Value=0x29)]
        GEConfigableConstId_SolarSystemRandomDicMin = 0x29,
        [ProtoEnum(Name="GEConfigableConstId_SolarSystemRandomDicMax", Value=0x2a)]
        GEConfigableConstId_SolarSystemRandomDicMax = 0x2a,
        [ProtoEnum(Name="GEConfigableConstId_StargateRandomDicMin", Value=0x2b)]
        GEConfigableConstId_StargateRandomDicMin = 0x2b,
        [ProtoEnum(Name="GEConfigableConstId_StargateRandomDicMax", Value=0x2c)]
        GEConfigableConstId_StargateRandomDicMax = 0x2c,
        [ProtoEnum(Name="GEConfigableConstId_StargroupRandomDicMin", Value=0x2d)]
        GEConfigableConstId_StargroupRandomDicMin = 0x2d,
        [ProtoEnum(Name="GEConfigableConstId_StargroupRandomDicMax", Value=0x2e)]
        GEConfigableConstId_StargroupRandomDicMax = 0x2e,
        [ProtoEnum(Name="GEConfigableConstId_StarGroupSolarSystemMaxCount", Value=0x2f)]
        GEConfigableConstId_StarGroupSolarSystemMaxCount = 0x2f,
        [ProtoEnum(Name="GEConfigableConstId_StarGroupSolarSystemMinCount", Value=0x30)]
        GEConfigableConstId_StarGroupSolarSystemMinCount = 0x30,
        [ProtoEnum(Name="GEConfigableConstId_SpaceObjectDistanceGeometricScaleRatio", Value=0x31)]
        GEConfigableConstId_SpaceObjectDistanceGeometricScaleRatio = 0x31,
        [ProtoEnum(Name="GEConfigableConstId_MinOrbitDistanceBetweenPlanetsRatio", Value=50)]
        GEConfigableConstId_MinOrbitDistanceBetweenPlanetsRatio = 50,
        [ProtoEnum(Name="GEConfigableConstId_SunOrbitSpaceForDisplayOnStarMap", Value=0x33)]
        GEConfigableConstId_SunOrbitSpaceForDisplayOnStarMap = 0x33,
        [ProtoEnum(Name="GEConfigableConstId_StarMapDrawingScaleRatio", Value=0x34)]
        GEConfigableConstId_StarMapDrawingScaleRatio = 0x34,
        [ProtoEnum(Name="GEConfigableConstId_PortalStationOrbitRadius", Value=0x35)]
        GEConfigableConstId_PortalStationOrbitRadius = 0x35
    }
}

