﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataRechargeItemInfo")]
    public class ConfigDataRechargeItemInfo : IExtensible
    {
        private int _ID;
        private int _Discount;
        private int _Price;
        private int _CurrencyCount;
        private int _FirstTimeBonus;
        private int _NormalBonus;
        private string _IconPath;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Discount;
        private static DelegateBridge __Hotfix_set_Discount;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_CurrencyCount;
        private static DelegateBridge __Hotfix_set_CurrencyCount;
        private static DelegateBridge __Hotfix_get_FirstTimeBonus;
        private static DelegateBridge __Hotfix_set_FirstTimeBonus;
        private static DelegateBridge __Hotfix_get_NormalBonus;
        private static DelegateBridge __Hotfix_set_NormalBonus;
        private static DelegateBridge __Hotfix_get_IconPath;
        private static DelegateBridge __Hotfix_set_IconPath;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Discount", DataFormat=DataFormat.TwosComplement)]
        public int Discount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public int Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="CurrencyCount", DataFormat=DataFormat.TwosComplement)]
        public int CurrencyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="FirstTimeBonus", DataFormat=DataFormat.TwosComplement)]
        public int FirstTimeBonus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="NormalBonus", DataFormat=DataFormat.TwosComplement)]
        public int NormalBonus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="IconPath", DataFormat=DataFormat.Default)]
        public string IconPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

