﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="RankType")]
    public enum RankType
    {
        [ProtoEnum(Name="RankType_Invalid", Value=0)]
        RankType_Invalid = 0,
        [ProtoEnum(Name="RankType_T1", Value=1)]
        RankType_T1 = 1,
        [ProtoEnum(Name="RankType_T2", Value=2)]
        RankType_T2 = 2,
        [ProtoEnum(Name="RankType_T3", Value=3)]
        RankType_T3 = 3
    }
}

