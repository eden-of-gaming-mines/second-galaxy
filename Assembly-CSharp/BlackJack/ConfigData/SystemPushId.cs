﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SystemPushId")]
    public enum SystemPushId
    {
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattleStart2PlayerForAttackerGuild", Value=1)]
        SystemPushId_GuildSovereignBattleStart2PlayerForAttackerGuild = 1,
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattleStartForDefenderGuild", Value=2)]
        SystemPushId_GuildSovereignBattleStartForDefenderGuild = 2,
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattlePrepareEndOneHourForAttackerGuild", Value=3)]
        SystemPushId_GuildSovereignBattlePrepareEndOneHourForAttackerGuild = 3,
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattlePrepareEndOneHourForDefenderGuild", Value=4)]
        SystemPushId_GuildSovereignBattlePrepareEndOneHourForDefenderGuild = 4,
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattleDeclarationForAttackerGuild", Value=5)]
        SystemPushId_GuildSovereignBattleDeclarationForAttackerGuild = 5,
        [ProtoEnum(Name="SystemPushId_GuildSovereignBattleDeclarationForDefenderGuild", Value=6)]
        SystemPushId_GuildSovereignBattleDeclarationForDefenderGuild = 6,
        [ProtoEnum(Name="SystemPushId_DelegateMissionComplete", Value=7)]
        SystemPushId_DelegateMissionComplete = 7,
        [ProtoEnum(Name="SystemPushId_CrackComplete", Value=8)]
        SystemPushId_CrackComplete = 8,
        [ProtoEnum(Name="SystemPushId_DelegateMissionInvadeStart", Value=9)]
        SystemPushId_DelegateMissionInvadeStart = 9,
        [ProtoEnum(Name="SystemPushId_DelegateMissionCancel", Value=10)]
        SystemPushId_DelegateMissionCancel = 10,
        [ProtoEnum(Name="SystemPushId_Max", Value=11)]
        SystemPushId_Max = 11
    }
}

