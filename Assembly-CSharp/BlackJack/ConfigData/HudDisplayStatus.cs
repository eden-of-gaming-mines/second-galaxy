﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="HudDisplayStatus")]
    public enum HudDisplayStatus
    {
        [ProtoEnum(Name="HudDisplayStatus_Hide", Value=0)]
        HudDisplayStatus_Hide = 0,
        [ProtoEnum(Name="HudDisplayStatus_Display", Value=1)]
        HudDisplayStatus_Display = 1,
        [ProtoEnum(Name="HudDisplayStatus_FollowUI", Value=2)]
        HudDisplayStatus_FollowUI = 2
    }
}

