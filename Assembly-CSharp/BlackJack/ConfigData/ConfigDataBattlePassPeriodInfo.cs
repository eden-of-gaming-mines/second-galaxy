﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataBattlePassPeriodInfo")]
    public class ConfigDataBattlePassPeriodInfo : IExtensible
    {
        private int _ID;
        private int _ChallangeGroupTemplateId;
        private int _LevelTemplateId;
        private int _ChallangeAccRewardTemplateId;
        private readonly List<int> _UpgradeRewardList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ChallangeGroupTemplateId;
        private static DelegateBridge __Hotfix_set_ChallangeGroupTemplateId;
        private static DelegateBridge __Hotfix_get_LevelTemplateId;
        private static DelegateBridge __Hotfix_set_LevelTemplateId;
        private static DelegateBridge __Hotfix_get_ChallangeAccRewardTemplateId;
        private static DelegateBridge __Hotfix_set_ChallangeAccRewardTemplateId;
        private static DelegateBridge __Hotfix_get_UpgradeRewardList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ChallangeGroupTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeGroupTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="LevelTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int LevelTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ChallangeAccRewardTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeAccRewardTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="UpgradeRewardList", DataFormat=DataFormat.TwosComplement)]
        public List<int> UpgradeRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

