﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestDescReplaceParamType")]
    public enum QuestDescReplaceParamType
    {
        [ProtoEnum(Name="QuestDescReplaceParamType_QuestAcceptStaionName", Value=1)]
        QuestDescReplaceParamType_QuestAcceptStaionName = 1,
        [ProtoEnum(Name="QuestDescReplaceParamType_QuestCompleteStationName", Value=2)]
        QuestDescReplaceParamType_QuestCompleteStationName = 2,
        [ProtoEnum(Name="QuestDescReplaceParamType_Max", Value=3)]
        QuestDescReplaceParamType_Max = 3
    }
}

