﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GESolarSystemType")]
    public enum GESolarSystemType
    {
        [ProtoEnum(Name="GESolarSystemType_Invalid", Value=0)]
        GESolarSystemType_Invalid = 0,
        [ProtoEnum(Name="GESolarSystemType_CommonSolarSytem", Value=1)]
        GESolarSystemType_CommonSolarSytem = 1,
        [ProtoEnum(Name="GESolarSystemType_WormHoleSolarSystem", Value=2)]
        GESolarSystemType_WormHoleSolarSystem = 2
    }
}

