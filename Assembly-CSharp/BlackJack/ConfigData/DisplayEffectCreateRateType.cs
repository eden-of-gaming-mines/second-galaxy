﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DisplayEffectCreateRateType")]
    public enum DisplayEffectCreateRateType
    {
        [ProtoEnum(Name="DisplayEffectCreateRateType_SelfLaunchEffect", Value=1)]
        DisplayEffectCreateRateType_SelfLaunchEffect = 1,
        [ProtoEnum(Name="DisplayEffectCreateRateType_SelfTargetEffectOnBusy", Value=2)]
        DisplayEffectCreateRateType_SelfTargetEffectOnBusy = 2,
        [ProtoEnum(Name="DisplayEffectCreateRateType_SelfTargetEffectOnNormal", Value=3)]
        DisplayEffectCreateRateType_SelfTargetEffectOnNormal = 3,
        [ProtoEnum(Name="DisplayEffectCreateRateType_DisplayShipEffect", Value=4)]
        DisplayEffectCreateRateType_DisplayShipEffect = 4,
        [ProtoEnum(Name="DisplayEffectCreateRateType_NoneDisplayShipEffect", Value=5)]
        DisplayEffectCreateRateType_NoneDisplayShipEffect = 5
    }
}

