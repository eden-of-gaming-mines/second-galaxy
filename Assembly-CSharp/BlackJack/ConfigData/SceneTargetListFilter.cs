﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneTargetListFilter")]
    public enum SceneTargetListFilter
    {
        [ProtoEnum(Name="SceneTargetListFilter_None", Value=0)]
        SceneTargetListFilter_None = 0,
        [ProtoEnum(Name="SceneTargetListFilter_SpaceStation", Value=1)]
        SceneTargetListFilter_SpaceStation = 1,
        [ProtoEnum(Name="SceneTargetListFilter_Stargate", Value=2)]
        SceneTargetListFilter_Stargate = 2,
        [ProtoEnum(Name="SceneTargetListFilter_Celestial", Value=4)]
        SceneTargetListFilter_Celestial = 4,
        [ProtoEnum(Name="SceneTargetListFilter_PlanetLikeBuilding", Value=8)]
        SceneTargetListFilter_PlanetLikeBuilding = 8,
        [ProtoEnum(Name="SceneTargetListFilter_GlobalScene", Value=0x10)]
        SceneTargetListFilter_GlobalScene = 0x10
    }
}

