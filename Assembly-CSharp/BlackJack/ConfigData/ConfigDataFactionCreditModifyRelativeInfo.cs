﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFactionCreditModifyRelativeInfo")]
    public class ConfigDataFactionCreditModifyRelativeInfo : IExtensible
    {
        private int _ID;
        private readonly List<FactionCreditModifyRelativeInfoFactionFactorList> _FactionFactorList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_FactionFactorList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="FactionFactorList", DataFormat=DataFormat.Default)]
        public List<FactionCreditModifyRelativeInfoFactionFactorList> FactionFactorList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

