﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildLogicEffectType")]
    public enum GuildLogicEffectType
    {
        [ProtoEnum(Name="GuildLogicEffectType_None", Value=0)]
        GuildLogicEffectType_None = 0,
        [ProtoEnum(Name="GuildLogicEffectType_GuildProperties", Value=1)]
        GuildLogicEffectType_GuildProperties = 1,
        [ProtoEnum(Name="GuildLogicEffectType_GuildFlag", Value=2)]
        GuildLogicEffectType_GuildFlag = 2,
        [ProtoEnum(Name="GuildLogicEffectType_SolarSystemShipBuf", Value=3)]
        GuildLogicEffectType_SolarSystemShipBuf = 3,
        [ProtoEnum(Name="GuildLogicEffectType_SolarSystemMiningShipBuf", Value=4)]
        GuildLogicEffectType_SolarSystemMiningShipBuf = 4,
        [ProtoEnum(Name="GuildLogicEffectType_SolarSystemFlagShipBuf", Value=5)]
        GuildLogicEffectType_SolarSystemFlagShipBuf = 5,
        [ProtoEnum(Name="GuildLogicEffectType_Max", Value=6)]
        GuildLogicEffectType_Max = 6
    }
}

