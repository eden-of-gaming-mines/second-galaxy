﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPassiveSkillLevelInfo")]
    public class ConfigDataPassiveSkillLevelInfo : IExtensible
    {
        private int _ID;
        private int _SkillPoint;
        private int _PlayerLevelRequire;
        private readonly List<ConfIdLevelInfo> _DependPassiveSkillList;
        private readonly List<ConfIdLevelInfo> _DependTechList;
        private PropertyCategory _DependPropertiesBasicId;
        private int _DependPropertiesBasicValue;
        private int _BufId;
        private int _BindMoneyCost;
        private int _CharacterScore;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SkillPoint;
        private static DelegateBridge __Hotfix_set_SkillPoint;
        private static DelegateBridge __Hotfix_get_PlayerLevelRequire;
        private static DelegateBridge __Hotfix_set_PlayerLevelRequire;
        private static DelegateBridge __Hotfix_get_DependPassiveSkillList;
        private static DelegateBridge __Hotfix_get_DependTechList;
        private static DelegateBridge __Hotfix_get_DependPropertiesBasicId;
        private static DelegateBridge __Hotfix_set_DependPropertiesBasicId;
        private static DelegateBridge __Hotfix_get_DependPropertiesBasicValue;
        private static DelegateBridge __Hotfix_set_DependPropertiesBasicValue;
        private static DelegateBridge __Hotfix_get_BufId;
        private static DelegateBridge __Hotfix_set_BufId;
        private static DelegateBridge __Hotfix_get_BindMoneyCost;
        private static DelegateBridge __Hotfix_set_BindMoneyCost;
        private static DelegateBridge __Hotfix_get_CharacterScore;
        private static DelegateBridge __Hotfix_set_CharacterScore;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SkillPoint", DataFormat=DataFormat.TwosComplement)]
        public int SkillPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PlayerLevelRequire", DataFormat=DataFormat.TwosComplement)]
        public int PlayerLevelRequire
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="DependPassiveSkillList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependPassiveSkillList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="DependTechList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DependPropertiesBasicId", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory DependPropertiesBasicId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="DependPropertiesBasicValue", DataFormat=DataFormat.TwosComplement)]
        public int DependPropertiesBasicValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BufId", DataFormat=DataFormat.TwosComplement)]
        public int BufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="BindMoneyCost", DataFormat=DataFormat.TwosComplement)]
        public int BindMoneyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="CharacterScore", DataFormat=DataFormat.TwosComplement)]
        public int CharacterScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

