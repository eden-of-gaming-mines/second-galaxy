﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSpaceStationResInfo")]
    public class ConfigDataSpaceStationResInfo : IExtensible
    {
        private int _ID;
        private string _ArtResource;
        private BlackJack.ConfigData.SpaceStationType _SpaceStationType;
        private int _ArroundDistance;
        private int _EnterStationDistance;
        private string _UI3DArtReource;
        private int _LeaveStationDurationTime;
        private string _LobbyArtResName;
        private string _ShipHangarArtResName;
        private string _ShipHangarBGMResName;
        private string _ProduceArtResPath;
        private string _Desc;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ArtResource;
        private static DelegateBridge __Hotfix_set_ArtResource;
        private static DelegateBridge __Hotfix_get_SpaceStationType;
        private static DelegateBridge __Hotfix_set_SpaceStationType;
        private static DelegateBridge __Hotfix_get_ArroundDistance;
        private static DelegateBridge __Hotfix_set_ArroundDistance;
        private static DelegateBridge __Hotfix_get_EnterStationDistance;
        private static DelegateBridge __Hotfix_set_EnterStationDistance;
        private static DelegateBridge __Hotfix_get_UI3DArtReource;
        private static DelegateBridge __Hotfix_set_UI3DArtReource;
        private static DelegateBridge __Hotfix_get_LeaveStationDurationTime;
        private static DelegateBridge __Hotfix_set_LeaveStationDurationTime;
        private static DelegateBridge __Hotfix_get_LobbyArtResName;
        private static DelegateBridge __Hotfix_set_LobbyArtResName;
        private static DelegateBridge __Hotfix_get_ShipHangarArtResName;
        private static DelegateBridge __Hotfix_set_ShipHangarArtResName;
        private static DelegateBridge __Hotfix_get_ShipHangarBGMResName;
        private static DelegateBridge __Hotfix_set_ShipHangarBGMResName;
        private static DelegateBridge __Hotfix_get_ProduceArtResPath;
        private static DelegateBridge __Hotfix_set_ProduceArtResPath;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ArtResource", DataFormat=DataFormat.Default)]
        public string ArtResource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SpaceStationType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SpaceStationType SpaceStationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ArroundDistance", DataFormat=DataFormat.TwosComplement)]
        public int ArroundDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="EnterStationDistance", DataFormat=DataFormat.TwosComplement)]
        public int EnterStationDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="UI3DArtReource", DataFormat=DataFormat.Default)]
        public string UI3DArtReource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="LeaveStationDurationTime", DataFormat=DataFormat.TwosComplement)]
        public int LeaveStationDurationTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="LobbyArtResName", DataFormat=DataFormat.Default)]
        public string LobbyArtResName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="ShipHangarArtResName", DataFormat=DataFormat.Default)]
        public string ShipHangarArtResName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="ShipHangarBGMResName", DataFormat=DataFormat.Default)]
        public string ShipHangarBGMResName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="ProduceArtResPath", DataFormat=DataFormat.Default)]
        public string ProduceArtResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

