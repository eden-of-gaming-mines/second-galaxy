﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSpaceShipInfo")]
    public class ConfigDataSpaceShipInfo : IExtensible
    {
        private int _ID;
        private ShipType _Type;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private float _SalePrice;
        private uint _DeadAnimTime;
        private int _Length;
        private int _Size;
        private RankType _Rank;
        private SubRankType _SubRank;
        private int _PackedSize;
        private bool _IsBindOnPick;
        private int _StoreSize;
        private string _ArtResource;
        private string _IconResPath;
        private long _Mass;
        private double _MassInertiaParam;
        private double _CruiseSpeedMax;
        private double _JumpSpeedMax;
        private float _MinAroundRadius;
        private double _JumpAccelerationParam;
        private int _JumpEnergyCost;
        private int _CPUMax;
        private int _PowerMax;
        private int _LogicWeaponSlotCount;
        private readonly List<int> _HighSlotDesc;
        private int _MiddleSlotCount;
        private int _LowSlotCount;
        private int _ShieldMax;
        private int _ShieldGrow;
        private int _ShieldDamageResistInfoId;
        private int _ArmorMax;
        private int _ArmorDamageResistInfoId;
        private int _EnergyMax;
        private int _EnergyGrow;
        private int _ElecResistInfoId;
        private int _FightSizeParam;
        private int _SuperWeaponId;
        private double _DefaultCameraOffset;
        private readonly List<CommonPropertyInfo> _PropertiesList;
        private readonly List<int> _SpecicalBufList;
        private readonly List<CommonPropertyInfo> _DrivingLicensePropertiesList;
        private float _DrivingLicensePropertiesLevelMulti;
        private int _TowSalvageRange;
        private float _JumpSteady;
        private int _NormalAttackWeaponConfId;
        private int _DefaultBoosterEquipConfId;
        private int _DefaultRepairerEquipConfId;
        private readonly List<float> _ShipGeneralInfoList;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private int _SuperEquipId;
        private int _TacticalEquipId;
        private int _RelativeFlagShipDataId;
        private readonly List<int> _RelativeTechList;
        private int _Flag;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _SimpleDescStrKey;
        private string _ShipUseDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_DeadAnimTime;
        private static DelegateBridge __Hotfix_set_DeadAnimTime;
        private static DelegateBridge __Hotfix_get_Length;
        private static DelegateBridge __Hotfix_set_Length;
        private static DelegateBridge __Hotfix_get_Size;
        private static DelegateBridge __Hotfix_set_Size;
        private static DelegateBridge __Hotfix_get_Rank;
        private static DelegateBridge __Hotfix_set_Rank;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_StoreSize;
        private static DelegateBridge __Hotfix_set_StoreSize;
        private static DelegateBridge __Hotfix_get_ArtResource;
        private static DelegateBridge __Hotfix_set_ArtResource;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_Mass;
        private static DelegateBridge __Hotfix_set_Mass;
        private static DelegateBridge __Hotfix_get_MassInertiaParam;
        private static DelegateBridge __Hotfix_set_MassInertiaParam;
        private static DelegateBridge __Hotfix_get_CruiseSpeedMax;
        private static DelegateBridge __Hotfix_set_CruiseSpeedMax;
        private static DelegateBridge __Hotfix_get_JumpSpeedMax;
        private static DelegateBridge __Hotfix_set_JumpSpeedMax;
        private static DelegateBridge __Hotfix_get_MinAroundRadius;
        private static DelegateBridge __Hotfix_set_MinAroundRadius;
        private static DelegateBridge __Hotfix_get_JumpAccelerationParam;
        private static DelegateBridge __Hotfix_set_JumpAccelerationParam;
        private static DelegateBridge __Hotfix_get_JumpEnergyCost;
        private static DelegateBridge __Hotfix_set_JumpEnergyCost;
        private static DelegateBridge __Hotfix_get_CPUMax;
        private static DelegateBridge __Hotfix_set_CPUMax;
        private static DelegateBridge __Hotfix_get_PowerMax;
        private static DelegateBridge __Hotfix_set_PowerMax;
        private static DelegateBridge __Hotfix_get_LogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_set_LogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_get_HighSlotDesc;
        private static DelegateBridge __Hotfix_get_MiddleSlotCount;
        private static DelegateBridge __Hotfix_set_MiddleSlotCount;
        private static DelegateBridge __Hotfix_get_LowSlotCount;
        private static DelegateBridge __Hotfix_set_LowSlotCount;
        private static DelegateBridge __Hotfix_get_ShieldMax;
        private static DelegateBridge __Hotfix_set_ShieldMax;
        private static DelegateBridge __Hotfix_get_ShieldGrow;
        private static DelegateBridge __Hotfix_set_ShieldGrow;
        private static DelegateBridge __Hotfix_get_ShieldDamageResistInfoId;
        private static DelegateBridge __Hotfix_set_ShieldDamageResistInfoId;
        private static DelegateBridge __Hotfix_get_ArmorMax;
        private static DelegateBridge __Hotfix_set_ArmorMax;
        private static DelegateBridge __Hotfix_get_ArmorDamageResistInfoId;
        private static DelegateBridge __Hotfix_set_ArmorDamageResistInfoId;
        private static DelegateBridge __Hotfix_get_EnergyMax;
        private static DelegateBridge __Hotfix_set_EnergyMax;
        private static DelegateBridge __Hotfix_get_EnergyGrow;
        private static DelegateBridge __Hotfix_set_EnergyGrow;
        private static DelegateBridge __Hotfix_get_ElecResistInfoId;
        private static DelegateBridge __Hotfix_set_ElecResistInfoId;
        private static DelegateBridge __Hotfix_get_FightSizeParam;
        private static DelegateBridge __Hotfix_set_FightSizeParam;
        private static DelegateBridge __Hotfix_get_SuperWeaponId;
        private static DelegateBridge __Hotfix_set_SuperWeaponId;
        private static DelegateBridge __Hotfix_get_DefaultCameraOffset;
        private static DelegateBridge __Hotfix_set_DefaultCameraOffset;
        private static DelegateBridge __Hotfix_get_PropertiesList;
        private static DelegateBridge __Hotfix_get_SpecicalBufList;
        private static DelegateBridge __Hotfix_get_DrivingLicensePropertiesList;
        private static DelegateBridge __Hotfix_get_DrivingLicensePropertiesLevelMulti;
        private static DelegateBridge __Hotfix_set_DrivingLicensePropertiesLevelMulti;
        private static DelegateBridge __Hotfix_get_TowSalvageRange;
        private static DelegateBridge __Hotfix_set_TowSalvageRange;
        private static DelegateBridge __Hotfix_get_JumpSteady;
        private static DelegateBridge __Hotfix_set_JumpSteady;
        private static DelegateBridge __Hotfix_get_NormalAttackWeaponConfId;
        private static DelegateBridge __Hotfix_set_NormalAttackWeaponConfId;
        private static DelegateBridge __Hotfix_get_DefaultBoosterEquipConfId;
        private static DelegateBridge __Hotfix_set_DefaultBoosterEquipConfId;
        private static DelegateBridge __Hotfix_get_DefaultRepairerEquipConfId;
        private static DelegateBridge __Hotfix_set_DefaultRepairerEquipConfId;
        private static DelegateBridge __Hotfix_get_ShipGeneralInfoList;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_SuperEquipId;
        private static DelegateBridge __Hotfix_set_SuperEquipId;
        private static DelegateBridge __Hotfix_get_TacticalEquipId;
        private static DelegateBridge __Hotfix_set_TacticalEquipId;
        private static DelegateBridge __Hotfix_get_RelativeFlagShipDataId;
        private static DelegateBridge __Hotfix_set_RelativeFlagShipDataId;
        private static DelegateBridge __Hotfix_get_RelativeTechList;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_SimpleDescStrKey;
        private static DelegateBridge __Hotfix_set_SimpleDescStrKey;
        private static DelegateBridge __Hotfix_get_ShipUseDescStrKey;
        private static DelegateBridge __Hotfix_set_ShipUseDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public ShipType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DeadAnimTime", DataFormat=DataFormat.TwosComplement)]
        public uint DeadAnimTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="Length", DataFormat=DataFormat.TwosComplement)]
        public int Length
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="Size", DataFormat=DataFormat.TwosComplement)]
        public int Size
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="Rank", DataFormat=DataFormat.TwosComplement)]
        public RankType Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.TwosComplement)]
        public int PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="StoreSize", DataFormat=DataFormat.TwosComplement)]
        public int StoreSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="ArtResource", DataFormat=DataFormat.Default)]
        public string ArtResource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="Mass", DataFormat=DataFormat.TwosComplement)]
        public long Mass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="MassInertiaParam", DataFormat=DataFormat.TwosComplement)]
        public double MassInertiaParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="CruiseSpeedMax", DataFormat=DataFormat.TwosComplement)]
        public double CruiseSpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="JumpSpeedMax", DataFormat=DataFormat.TwosComplement)]
        public double JumpSpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="MinAroundRadius", DataFormat=DataFormat.FixedSize)]
        public float MinAroundRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=true, Name="JumpAccelerationParam", DataFormat=DataFormat.TwosComplement)]
        public double JumpAccelerationParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, IsRequired=true, Name="JumpEnergyCost", DataFormat=DataFormat.TwosComplement)]
        public int JumpEnergyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="CPUMax", DataFormat=DataFormat.TwosComplement)]
        public int CPUMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=true, Name="PowerMax", DataFormat=DataFormat.TwosComplement)]
        public int PowerMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=true, Name="LogicWeaponSlotCount", DataFormat=DataFormat.TwosComplement)]
        public int LogicWeaponSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, Name="HighSlotDesc", DataFormat=DataFormat.TwosComplement)]
        public List<int> HighSlotDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x23, IsRequired=true, Name="MiddleSlotCount", DataFormat=DataFormat.TwosComplement)]
        public int MiddleSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x24, IsRequired=true, Name="LowSlotCount", DataFormat=DataFormat.TwosComplement)]
        public int LowSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x25, IsRequired=true, Name="ShieldMax", DataFormat=DataFormat.TwosComplement)]
        public int ShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x26, IsRequired=true, Name="ShieldGrow", DataFormat=DataFormat.TwosComplement)]
        public int ShieldGrow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x27, IsRequired=true, Name="ShieldDamageResistInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ShieldDamageResistInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(40, IsRequired=true, Name="ArmorMax", DataFormat=DataFormat.TwosComplement)]
        public int ArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x29, IsRequired=true, Name="ArmorDamageResistInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ArmorDamageResistInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=true, Name="EnergyMax", DataFormat=DataFormat.TwosComplement)]
        public int EnergyMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, IsRequired=true, Name="EnergyGrow", DataFormat=DataFormat.TwosComplement)]
        public int EnergyGrow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=true, Name="ElecResistInfoId", DataFormat=DataFormat.TwosComplement)]
        public int ElecResistInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2d, IsRequired=true, Name="FightSizeParam", DataFormat=DataFormat.TwosComplement)]
        public int FightSizeParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2e, IsRequired=true, Name="SuperWeaponId", DataFormat=DataFormat.TwosComplement)]
        public int SuperWeaponId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2f, IsRequired=true, Name="DefaultCameraOffset", DataFormat=DataFormat.TwosComplement)]
        public double DefaultCameraOffset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x31, Name="PropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> PropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(50, Name="SpecicalBufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SpecicalBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x33, Name="DrivingLicensePropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> DrivingLicensePropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x34, IsRequired=true, Name="DrivingLicensePropertiesLevelMulti", DataFormat=DataFormat.FixedSize)]
        public float DrivingLicensePropertiesLevelMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x35, IsRequired=true, Name="TowSalvageRange", DataFormat=DataFormat.TwosComplement)]
        public int TowSalvageRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x36, IsRequired=true, Name="JumpSteady", DataFormat=DataFormat.FixedSize)]
        public float JumpSteady
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x37, IsRequired=true, Name="NormalAttackWeaponConfId", DataFormat=DataFormat.TwosComplement)]
        public int NormalAttackWeaponConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x38, IsRequired=true, Name="DefaultBoosterEquipConfId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultBoosterEquipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x39, IsRequired=true, Name="DefaultRepairerEquipConfId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultRepairerEquipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x3a, Name="ShipGeneralInfoList", DataFormat=DataFormat.FixedSize)]
        public List<float> ShipGeneralInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x3b, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(60, IsRequired=true, Name="SuperEquipId", DataFormat=DataFormat.TwosComplement)]
        public int SuperEquipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x3d, IsRequired=true, Name="TacticalEquipId", DataFormat=DataFormat.TwosComplement)]
        public int TacticalEquipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x3e, IsRequired=true, Name="RelativeFlagShipDataId", DataFormat=DataFormat.TwosComplement)]
        public int RelativeFlagShipDataId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x3f, Name="RelativeTechList", DataFormat=DataFormat.TwosComplement)]
        public List<int> RelativeTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x40, IsRequired=true, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public int Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x41, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x42, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x43, IsRequired=true, Name="SimpleDescStrKey", DataFormat=DataFormat.Default)]
        public string SimpleDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x44, IsRequired=true, Name="ShipUseDescStrKey", DataFormat=DataFormat.Default)]
        public string ShipUseDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

