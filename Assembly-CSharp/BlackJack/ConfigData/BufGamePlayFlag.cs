﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BufGamePlayFlag")]
    public enum BufGamePlayFlag
    {
        [ProtoEnum(Name="BufGamePlayFlag_None", Value=0)]
        BufGamePlayFlag_None = 0,
        [ProtoEnum(Name="BufGamePlayFlag_WormholeEnv", Value=1)]
        BufGamePlayFlag_WormholeEnv = 1,
        [ProtoEnum(Name="BufGamePlayFlag_Invisible", Value=2)]
        BufGamePlayFlag_Invisible = 2,
        [ProtoEnum(Name="BufGamePlayFlag_Booster", Value=4)]
        BufGamePlayFlag_Booster = 4,
        [ProtoEnum(Name="BufGamePlayFlag_WormholeSolarEnv", Value=8)]
        BufGamePlayFlag_WormholeSolarEnv = 8,
        [ProtoEnum(Name="BufGamePlayFlag_SuperEquipFlag_AddShieldWhenEnergyCosting", Value=0x10)]
        BufGamePlayFlag_SuperEquipFlag_AddShieldWhenEnergyCosting = 0x10,
        [ProtoEnum(Name="BufGamePlayFlag_PVPProtectFlag", Value=0x20)]
        BufGamePlayFlag_PVPProtectFlag = 0x20,
        [ProtoEnum(Name="BufGamePlayFlag_ShieldRepairerDisable", Value=0x40)]
        BufGamePlayFlag_ShieldRepairerDisable = 0x40,
        [ProtoEnum(Name="BufGamePlayFlag_ShipMoveDisable", Value=0x80)]
        BufGamePlayFlag_ShipMoveDisable = 0x80,
        [ProtoEnum(Name="BufGamePlayFlag_ShipJumpDisable", Value=0x100)]
        BufGamePlayFlag_ShipJumpDisable = 0x100,
        [ProtoEnum(Name="BufGamePlayFlag_ShipBlinkDisable", Value=0x200)]
        BufGamePlayFlag_ShipBlinkDisable = 0x200,
        [ProtoEnum(Name="BufGamePlayFlag_FlagShipTeleportDisable", Value=0x400)]
        BufGamePlayFlag_FlagShipTeleportDisable = 0x400,
        [ProtoEnum(Name="BufGamePlayFlag_WeaponEquipDisable", Value=0x800)]
        BufGamePlayFlag_WeaponEquipDisable = 0x800,
        [ProtoEnum(Name="BufGamePlayFlag_SuperWeaponEquipEnergyRecoveryDisable", Value=0x1000)]
        BufGamePlayFlag_SuperWeaponEquipEnergyRecoveryDisable = 0x1000,
        [ProtoEnum(Name="BufGamePlayFlag_FlagShipAvailable", Value=0x2000)]
        BufGamePlayFlag_FlagShipAvailable = 0x2000,
        [ProtoEnum(Name="BufGamePlayFlag_WormholePlay", Value=0x4000)]
        BufGamePlayFlag_WormholePlay = 0x4000,
        [ProtoEnum(Name="BufGamePlayFlag_Max", Value=0x8000)]
        BufGamePlayFlag_Max = 0x8000
    }
}

