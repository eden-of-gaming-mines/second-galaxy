﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGEMoonCountDataInfo")]
    public class ConfigDataGEMoonCountDataInfo : IExtensible
    {
        private int _ID;
        private readonly List<GEMoonCountDataInfoOrbitRadiusRange> _OrbitRadiusRange;
        private int _MoonMaxCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_OrbitRadiusRange;
        private static DelegateBridge __Hotfix_get_MoonMaxCount;
        private static DelegateBridge __Hotfix_set_MoonMaxCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="OrbitRadiusRange", DataFormat=DataFormat.Default)]
        public List<GEMoonCountDataInfoOrbitRadiusRange> OrbitRadiusRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="MoonMaxCount", DataFormat=DataFormat.TwosComplement)]
        public int MoonMaxCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

