﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcShopItemInfo")]
    public class ConfigDataNpcShopItemInfo : IExtensible
    {
        private int _ID;
        private StoreItemType _Type;
        private int _ConfigID;
        private int _Price;
        private int _DefaultOrderCount;
        private int _SaleCountLimit;
        private BlackJack.ConfigData.CurrencyType _CurrencyType;
        private int _DependFactionId;
        private int _DependFactionCredit;
        private NpcShopItemType _ShopItemType;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_ConfigID;
        private static DelegateBridge __Hotfix_set_ConfigID;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_DefaultOrderCount;
        private static DelegateBridge __Hotfix_set_DefaultOrderCount;
        private static DelegateBridge __Hotfix_get_SaleCountLimit;
        private static DelegateBridge __Hotfix_set_SaleCountLimit;
        private static DelegateBridge __Hotfix_get_CurrencyType;
        private static DelegateBridge __Hotfix_set_CurrencyType;
        private static DelegateBridge __Hotfix_get_DependFactionId;
        private static DelegateBridge __Hotfix_set_DependFactionId;
        private static DelegateBridge __Hotfix_get_DependFactionCredit;
        private static DelegateBridge __Hotfix_set_DependFactionCredit;
        private static DelegateBridge __Hotfix_get_ShopItemType;
        private static DelegateBridge __Hotfix_set_ShopItemType;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ConfigID", DataFormat=DataFormat.TwosComplement)]
        public int ConfigID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public int Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="DefaultOrderCount", DataFormat=DataFormat.TwosComplement)]
        public int DefaultOrderCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SaleCountLimit", DataFormat=DataFormat.TwosComplement)]
        public int SaleCountLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="CurrencyType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.CurrencyType CurrencyType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="DependFactionId", DataFormat=DataFormat.TwosComplement)]
        public int DependFactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="DependFactionCredit", DataFormat=DataFormat.TwosComplement)]
        public int DependFactionCredit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="ShopItemType", DataFormat=DataFormat.TwosComplement)]
        public NpcShopItemType ShopItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

