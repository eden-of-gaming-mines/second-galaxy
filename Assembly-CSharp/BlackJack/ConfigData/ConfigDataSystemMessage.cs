﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSystemMessage")]
    public class ConfigDataSystemMessage : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.SystemMessageId _SystemMessageId;
        private SystemMessageType _Type;
        private readonly List<CommonFormatStringParamType> _FormatStringParamTypeList;
        private readonly List<FormatStringParamInfo> _FormatStringParamInfoList;
        private int _SortOrder;
        private float _Time;
        private float _Speed;
        private int _Count;
        private bool _NotShowOnScreen;
        private string _ContentStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SystemMessageId;
        private static DelegateBridge __Hotfix_set_SystemMessageId;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_FormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_FormatStringParamInfoList;
        private static DelegateBridge __Hotfix_get_SortOrder;
        private static DelegateBridge __Hotfix_set_SortOrder;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_Speed;
        private static DelegateBridge __Hotfix_set_Speed;
        private static DelegateBridge __Hotfix_get_Count;
        private static DelegateBridge __Hotfix_set_Count;
        private static DelegateBridge __Hotfix_get_NotShowOnScreen;
        private static DelegateBridge __Hotfix_set_NotShowOnScreen;
        private static DelegateBridge __Hotfix_get_ContentStrKey;
        private static DelegateBridge __Hotfix_set_ContentStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SystemMessageId", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SystemMessageId SystemMessageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public SystemMessageType Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="FormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> FormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="FormatStringParamInfoList", DataFormat=DataFormat.Default)]
        public List<FormatStringParamInfo> FormatStringParamInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SortOrder", DataFormat=DataFormat.TwosComplement)]
        public int SortOrder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Time", DataFormat=DataFormat.FixedSize)]
        public float Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Speed", DataFormat=DataFormat.FixedSize)]
        public float Speed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Count", DataFormat=DataFormat.TwosComplement)]
        public int Count
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NotShowOnScreen", DataFormat=DataFormat.Default)]
        public bool NotShowOnScreen
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="ContentStrKey", DataFormat=DataFormat.Default)]
        public string ContentStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

