﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ConstellationType")]
    public enum ConstellationType
    {
        [ProtoEnum(Name="ConstellationType_Aries", Value=1)]
        ConstellationType_Aries = 1,
        [ProtoEnum(Name="ConstellationType_Taurus", Value=2)]
        ConstellationType_Taurus = 2,
        [ProtoEnum(Name="ConstellationType_Gemini", Value=3)]
        ConstellationType_Gemini = 3,
        [ProtoEnum(Name="ConstellationType_Cancer", Value=4)]
        ConstellationType_Cancer = 4,
        [ProtoEnum(Name="ConstellationType_Leo", Value=5)]
        ConstellationType_Leo = 5,
        [ProtoEnum(Name="ConstellationType_Virgo", Value=6)]
        ConstellationType_Virgo = 6,
        [ProtoEnum(Name="ConstellationType_Libra", Value=7)]
        ConstellationType_Libra = 7,
        [ProtoEnum(Name="ConstellationType_Scorpio", Value=8)]
        ConstellationType_Scorpio = 8,
        [ProtoEnum(Name="ConstellationType_Sagittarius", Value=9)]
        ConstellationType_Sagittarius = 9,
        [ProtoEnum(Name="ConstellationType_Capricorn", Value=10)]
        ConstellationType_Capricorn = 10,
        [ProtoEnum(Name="ConstellationType_Aquarius", Value=11)]
        ConstellationType_Aquarius = 11,
        [ProtoEnum(Name="ConstellationType_Pisces", Value=12)]
        ConstellationType_Pisces = 12
    }
}

