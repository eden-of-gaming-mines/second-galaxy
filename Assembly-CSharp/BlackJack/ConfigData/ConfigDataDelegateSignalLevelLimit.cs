﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDelegateSignalLevelLimit")]
    public class ConfigDataDelegateSignalLevelLimit : IExtensible
    {
        private int _ID;
        private int _LimitQuestId;
        private readonly List<SignalType> _DelegateSignalType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LimitQuestId;
        private static DelegateBridge __Hotfix_set_LimitQuestId;
        private static DelegateBridge __Hotfix_get_DelegateSignalType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LimitQuestId", DataFormat=DataFormat.TwosComplement)]
        public int LimitQuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DelegateSignalType", DataFormat=DataFormat.TwosComplement)]
        public List<SignalType> DelegateSignalType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

