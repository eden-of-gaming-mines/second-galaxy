﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="StringTableId")]
    public enum StringTableId
    {
        [ProtoEnum(Name="StringTableId_SpaceStation", Value=1)]
        StringTableId_SpaceStation = 1,
        [ProtoEnum(Name="StringTableId_StarGate", Value=2)]
        StringTableId_StarGate = 2,
        [ProtoEnum(Name="StringTableId_NoLockedTarget", Value=3)]
        StringTableId_NoLockedTarget = 3,
        [ProtoEnum(Name="StringTableId_TargetOutOfRange", Value=4)]
        StringTableId_TargetOutOfRange = 4,
        [ProtoEnum(Name="StringTableId_WeaponReloading", Value=5)]
        StringTableId_WeaponReloading = 5,
        [ProtoEnum(Name="StringTableId_WeaponCoolDown", Value=6)]
        StringTableId_WeaponCoolDown = 6,
        [ProtoEnum(Name="StringTableId_WeaponCharging", Value=7)]
        StringTableId_WeaponCharging = 7,
        [ProtoEnum(Name="StringTableId_PreparingForJump", Value=8)]
        StringTableId_PreparingForJump = 8,
        [ProtoEnum(Name="StringTableId_PropertyGather", Value=9)]
        StringTableId_PropertyGather = 9,
        [ProtoEnum(Name="StringTableId_PropertyOutPut", Value=10)]
        StringTableId_PropertyOutPut = 10,
        [ProtoEnum(Name="StringTableId_PropertyExistence", Value=11)]
        StringTableId_PropertyExistence = 11,
        [ProtoEnum(Name="StringTableId_ItemStore_All", Value=12)]
        StringTableId_ItemStore_All = 12,
        [ProtoEnum(Name="StringTableId_Frigate", Value=13)]
        StringTableId_Frigate = 13,
        [ProtoEnum(Name="StringTableId_Destroyer", Value=14)]
        StringTableId_Destroyer = 14,
        [ProtoEnum(Name="StringTableId_Cruiser", Value=15)]
        StringTableId_Cruiser = 15,
        [ProtoEnum(Name="StringTableId_BattleCruiser", Value=0x10)]
        StringTableId_BattleCruiser = 0x10,
        [ProtoEnum(Name="StringTableId_BattleShip", Value=0x11)]
        StringTableId_BattleShip = 0x11,
        [ProtoEnum(Name="StringTableId_Carrier", Value=0x12)]
        StringTableId_Carrier = 0x12,
        [ProtoEnum(Name="StringTableId_Flagship", Value=0x13)]
        StringTableId_Flagship = 0x13,
        [ProtoEnum(Name="StringTableId_HighSlot", Value=20)]
        StringTableId_HighSlot = 20,
        [ProtoEnum(Name="StringTableId_MiddleSlot", Value=0x15)]
        StringTableId_MiddleSlot = 0x15,
        [ProtoEnum(Name="StringTableId_LowSlot", Value=0x16)]
        StringTableId_LowSlot = 0x16,
        [ProtoEnum(Name="StringTableId_ShipHangarNextUnLockLevel", Value=0x17)]
        StringTableId_ShipHangarNextUnLockLevel = 0x17,
        [ProtoEnum(Name="StringTableId_ShipCrewIconLevel", Value=0x18)]
        StringTableId_ShipCrewIconLevel = 0x18,
        [ProtoEnum(Name="StringTableId_ShipType", Value=0x19)]
        StringTableId_ShipType = 0x19,
        [ProtoEnum(Name="StringTableId_EquipType", Value=0x1a)]
        StringTableId_EquipType = 0x1a,
        [ProtoEnum(Name="StringTableId_AmmoType", Value=0x1b)]
        StringTableId_AmmoType = 0x1b,
        [ProtoEnum(Name="StringTableId_EquipType1", Value=0x1c)]
        StringTableId_EquipType1 = 0x1c,
        [ProtoEnum(Name="StringTableId_AttackerDrone", Value=0x1d)]
        StringTableId_AttackerDrone = 0x1d,
        [ProtoEnum(Name="StringTableId_DefenderDrone", Value=30)]
        StringTableId_DefenderDrone = 30,
        [ProtoEnum(Name="StringTableId_SniperDrone", Value=0x1f)]
        StringTableId_SniperDrone = 0x1f,
        [ProtoEnum(Name="StringTableId_Missile", Value=0x20)]
        StringTableId_Missile = 0x20,
        [ProtoEnum(Name="StringTableId_Laser", Value=0x21)]
        StringTableId_Laser = 0x21,
        [ProtoEnum(Name="StringTableId_Railgun", Value=0x22)]
        StringTableId_Railgun = 0x22,
        [ProtoEnum(Name="StringTableId_Plasma", Value=0x23)]
        StringTableId_Plasma = 0x23,
        [ProtoEnum(Name="StringTableId_Drone", Value=0x24)]
        StringTableId_Drone = 0x24,
        [ProtoEnum(Name="StringTableId_Cannon", Value=0x25)]
        StringTableId_Cannon = 0x25,
        [ProtoEnum(Name="StringTableId_NormalItem", Value=0x26)]
        StringTableId_NormalItem = 0x26,
        [ProtoEnum(Name="StringTableId_Shop", Value=0x27)]
        StringTableId_Shop = 0x27,
        [ProtoEnum(Name="StringTableId_HeadQuarter", Value=40)]
        StringTableId_HeadQuarter = 40,
        [ProtoEnum(Name="StringTableId_TradingCenter", Value=0x29)]
        StringTableId_TradingCenter = 0x29,
        [ProtoEnum(Name="StringTableId_Billboard", Value=0x2a)]
        StringTableId_Billboard = 0x2a,
        [ProtoEnum(Name="StringTableId_Ranking", Value=0x2b)]
        StringTableId_Ranking = 0x2b,
        [ProtoEnum(Name="StringTableId_Bar", Value=0x2c)]
        StringTableId_Bar = 0x2c,
        [ProtoEnum(Name="StringTableId_BlackMarket", Value=0x2d)]
        StringTableId_BlackMarket = 0x2d,
        [ProtoEnum(Name="StringTableId_CreateCharacterFail", Value=0x2e)]
        StringTableId_CreateCharacterFail = 0x2e,
        [ProtoEnum(Name="StringTableId_CreatingCharaterInfo", Value=0x2f)]
        StringTableId_CreatingCharaterInfo = 0x2f,
        [ProtoEnum(Name="StringTableId_CharacterCreated", Value=0x30)]
        StringTableId_CharacterCreated = 0x30,
        [ProtoEnum(Name="StringTableId_CharacterConfirming", Value=0x31)]
        StringTableId_CharacterConfirming = 0x31,
        [ProtoEnum(Name="StringTableId_CharacterConfirmed", Value=50)]
        StringTableId_CharacterConfirmed = 50,
        [ProtoEnum(Name="StringTableId_ScanProbeCountNotEnough", Value=0x33)]
        StringTableId_ScanProbeCountNotEnough = 0x33,
        [ProtoEnum(Name="StringTableId_QuestionForEnterSSAndUseScanProbe", Value=0x34)]
        StringTableId_QuestionForEnterSSAndUseScanProbe = 0x34,
        [ProtoEnum(Name="StringTableId_ScanProbeUnlockCondInfo", Value=0x35)]
        StringTableId_ScanProbeUnlockCondInfo = 0x35,
        [ProtoEnum(Name="StringTableId_ShipSizeSmall", Value=0x36)]
        StringTableId_ShipSizeSmall = 0x36,
        [ProtoEnum(Name="StringTableId_ShipSizeMiddle", Value=0x37)]
        StringTableId_ShipSizeMiddle = 0x37,
        [ProtoEnum(Name="StringTableId_ShipSizeLarge", Value=0x38)]
        StringTableId_ShipSizeLarge = 0x38,
        [ProtoEnum(Name="StringTableId_ShipSizeExtraLarge", Value=0x39)]
        StringTableId_ShipSizeExtraLarge = 0x39,
        [ProtoEnum(Name="StringTableId_Login", Value=0x3a)]
        StringTableId_Login = 0x3a,
        [ProtoEnum(Name="StringTableId_EnterWorld", Value=0x3b)]
        StringTableId_EnterWorld = 0x3b,
        [ProtoEnum(Name="StringTableId_DamageMiss", Value=60)]
        StringTableId_DamageMiss = 60,
        [ProtoEnum(Name="StringTableId_SizeType_S", Value=0x3d)]
        StringTableId_SizeType_S = 0x3d,
        [ProtoEnum(Name="StringTableId_SizeType_M", Value=0x3e)]
        StringTableId_SizeType_M = 0x3e,
        [ProtoEnum(Name="StringTableId_SizeType_L", Value=0x3f)]
        StringTableId_SizeType_L = 0x3f,
        [ProtoEnum(Name="StringTableId_SizeType_XL", Value=0x40)]
        StringTableId_SizeType_XL = 0x40,
        [ProtoEnum(Name="StringTableId_SizeType_N", Value=0x41)]
        StringTableId_SizeType_N = 0x41,
        [ProtoEnum(Name="StringTableId_ScanInSpaceNotValid", Value=0x42)]
        StringTableId_ScanInSpaceNotValid = 0x42,
        [ProtoEnum(Name="StringTableId_UpgradeOneLevel", Value=0x43)]
        StringTableId_UpgradeOneLevel = 0x43,
        [ProtoEnum(Name="StringTableId_StartConnectGameServer", Value=0x44)]
        StringTableId_StartConnectGameServer = 0x44,
        [ProtoEnum(Name="StringTableId_PlayerInfoInitialized", Value=0x45)]
        StringTableId_PlayerInfoInitialized = 0x45,
        [ProtoEnum(Name="StringTableId_InitializingPlayerInfo", Value=70)]
        StringTableId_InitializingPlayerInfo = 70,
        [ProtoEnum(Name="StringTableId_PleaseCreateCharacter", Value=0x47)]
        StringTableId_PleaseCreateCharacter = 0x47,
        [ProtoEnum(Name="StringTableId_ConnectToGameServerSuccess", Value=0x48)]
        StringTableId_ConnectToGameServerSuccess = 0x48,
        [ProtoEnum(Name="StringTableId_ConnectToGameServerFailed", Value=0x49)]
        StringTableId_ConnectToGameServerFailed = 0x49,
        [ProtoEnum(Name="StringTableId_PlayerInfoInitializeFailed", Value=0x4a)]
        StringTableId_PlayerInfoInitializeFailed = 0x4a,
        [ProtoEnum(Name="StringTableId_CharacterNameIsEmpty", Value=0x4b)]
        StringTableId_CharacterNameIsEmpty = 0x4b,
        [ProtoEnum(Name="StringTableId_UnitStereString", Value=0x4c)]
        StringTableId_UnitStereString = 0x4c,
        [ProtoEnum(Name="StringTableId_UnitMeterString", Value=0x4d)]
        StringTableId_UnitMeterString = 0x4d,
        [ProtoEnum(Name="StringTableId_UnitSecondString", Value=0x4e)]
        StringTableId_UnitSecondString = 0x4e,
        [ProtoEnum(Name="StringTableId_UnitKMSecondString", Value=0x4f)]
        StringTableId_UnitKMSecondString = 0x4f,
        [ProtoEnum(Name="StringTableId_UnitMeterSecondString", Value=80)]
        StringTableId_UnitMeterSecondString = 80,
        [ProtoEnum(Name="StringTableId_UnitKmeterString", Value=0x51)]
        StringTableId_UnitKmeterString = 0x51,
        [ProtoEnum(Name="StringTableId_WeaponType_BurstRailgun", Value=0x52)]
        StringTableId_WeaponType_BurstRailgun = 0x52,
        [ProtoEnum(Name="StringTableId_WeaponType_SniperRailgun", Value=0x53)]
        StringTableId_WeaponType_SniperRailgun = 0x53,
        [ProtoEnum(Name="StringTableId_WeaponType_MissileLauncher", Value=0x54)]
        StringTableId_WeaponType_MissileLauncher = 0x54,
        [ProtoEnum(Name="StringTableId_WeaponType_LaserPulse", Value=0x55)]
        StringTableId_WeaponType_LaserPulse = 0x55,
        [ProtoEnum(Name="StringTableId_WeaponType_LaserBunding", Value=0x56)]
        StringTableId_WeaponType_LaserBunding = 0x56,
        [ProtoEnum(Name="StringTableId_WeaponType_HiNRGPlasma", Value=0x57)]
        StringTableId_WeaponType_HiNRGPlasma = 0x57,
        [ProtoEnum(Name="StringTableId_WeaponType_DroneCabin", Value=0x58)]
        StringTableId_WeaponType_DroneCabin = 0x58,
        [ProtoEnum(Name="StringTableId_WeaponType_FreePlasma", Value=0x59)]
        StringTableId_WeaponType_FreePlasma = 0x59,
        [ProtoEnum(Name="StringTableId_CharacterNameDuplicated", Value=90)]
        StringTableId_CharacterNameDuplicated = 90,
        [ProtoEnum(Name="StringTableId_CharacterNameOverlong", Value=0x5b)]
        StringTableId_CharacterNameOverlong = 0x5b,
        [ProtoEnum(Name="StringTableId_CharacterNameInvalid", Value=0x5c)]
        StringTableId_CharacterNameInvalid = 0x5c,
        [ProtoEnum(Name="StringTableId_CharacterNameEmpty", Value=0x5d)]
        StringTableId_CharacterNameEmpty = 0x5d,
        [ProtoEnum(Name="StringTableId_IsJumpValidForDistanceTip", Value=0x5e)]
        StringTableId_IsJumpValidForDistanceTip = 0x5e,
        [ProtoEnum(Name="StringTableId_TipInfo_ErrorUnknown", Value=0x5f)]
        StringTableId_TipInfo_ErrorUnknown = 0x5f,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_ArroundTarget", Value=0x60)]
        StringTableId_ShipMoveCmd_ArroundTarget = 0x60,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_CloseTarget", Value=0x61)]
        StringTableId_ShipMoveCmd_CloseTarget = 0x61,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_AwayTarget", Value=0x62)]
        StringTableId_ShipMoveCmd_AwayTarget = 0x62,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_DistanceToTarget", Value=0x63)]
        StringTableId_ShipMoveCmd_DistanceToTarget = 0x63,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_ReadyForJump", Value=100)]
        StringTableId_ShipMoveCmd_ReadyForJump = 100,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_Jumping", Value=0x65)]
        StringTableId_ShipMoveCmd_Jumping = 0x65,
        [ProtoEnum(Name="StringTableId_QuestCompleteCond_KillMonster", Value=0x66)]
        StringTableId_QuestCompleteCond_KillMonster = 0x66,
        [ProtoEnum(Name="StringTableId_QuestSignalSource", Value=0x67)]
        StringTableId_QuestSignalSource = 0x67,
        [ProtoEnum(Name="StringTableId_ShipMoveCmd_Stop", Value=0x68)]
        StringTableId_ShipMoveCmd_Stop = 0x68,
        [ProtoEnum(Name="StringTableId_ShipIsAutoFight_Open", Value=0x69)]
        StringTableId_ShipIsAutoFight_Open = 0x69,
        [ProtoEnum(Name="StringTableId_ShipIsAutoFight_Close", Value=0x6a)]
        StringTableId_ShipIsAutoFight_Close = 0x6a,
        [ProtoEnum(Name="StringTableId_WaitingLoginMsgAckOutTime", Value=0x6b)]
        StringTableId_WaitingLoginMsgAckOutTime = 0x6b,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionNext", Value=0x6c)]
        StringTableId_NpcDialogOptionNext = 0x6c,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionClose", Value=0x6d)]
        StringTableId_NpcDialogOptionClose = 0x6d,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionConfirm", Value=110)]
        StringTableId_NpcDialogOptionConfirm = 110,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionQuest", Value=0x6f)]
        StringTableId_NpcDialogOptionQuest = 0x6f,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionQuestComplete", Value=0x70)]
        StringTableId_NpcDialogOptionQuestComplete = 0x70,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionNextPage", Value=0x71)]
        StringTableId_NpcDialogOptionNextPage = 0x71,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenShop", Value=0x72)]
        StringTableId_NpcDialogOptionOpenShop = 0x72,
        [ProtoEnum(Name="StringTableId_JumpNotValidForSameSolarSystem", Value=0x73)]
        StringTableId_JumpNotValidForSameSolarSystem = 0x73,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionQuestAccept", Value=0x74)]
        StringTableId_NpcDialogOptionQuestAccept = 0x74,
        [ProtoEnum(Name="StringTableId_DistanceUnitJump", Value=0x75)]
        StringTableId_DistanceUnitJump = 0x75,
        [ProtoEnum(Name="StringTableId_QuestRequirement", Value=0x76)]
        StringTableId_QuestRequirement = 0x76,
        [ProtoEnum(Name="StringTableId_PlayerNotBelongToAnyGuild", Value=0x77)]
        StringTableId_PlayerNotBelongToAnyGuild = 0x77,
        [ProtoEnum(Name="StringTableId_PlayerHasNoGuildDuty", Value=120)]
        StringTableId_PlayerHasNoGuildDuty = 120,
        [ProtoEnum(Name="StringTableId_ClearPropertyPointsFailed", Value=0x79)]
        StringTableId_ClearPropertyPointsFailed = 0x79,
        [ProtoEnum(Name="StringTableId_MsgTalkToNpcInCurrentStation", Value=0x7a)]
        StringTableId_MsgTalkToNpcInCurrentStation = 0x7a,
        [ProtoEnum(Name="StringTableId_AddPropertyPointsFailed", Value=0x7b)]
        StringTableId_AddPropertyPointsFailed = 0x7b,
        [ProtoEnum(Name="StringTableId_DropBoxName_Internal", Value=0x7c)]
        StringTableId_DropBoxName_Internal = 0x7c,
        [ProtoEnum(Name="StringTableId_QuestListDialog_TalkToNpc", Value=0x7d)]
        StringTableId_QuestListDialog_TalkToNpc = 0x7d,
        [ProtoEnum(Name="StringTableId_QuestListDialog_CommitItem", Value=0x7e)]
        StringTableId_QuestListDialog_CommitItem = 0x7e,
        [ProtoEnum(Name="StringTableId_PropertyCategory_Attack", Value=0x7f)]
        StringTableId_PropertyCategory_Attack = 0x7f,
        [ProtoEnum(Name="StringTableId_PropertyCategory_Drive", Value=0x80)]
        StringTableId_PropertyCategory_Drive = 0x80,
        [ProtoEnum(Name="StringTableId_PropertyCategory_Defence", Value=0x81)]
        StringTableId_PropertyCategory_Defence = 0x81,
        [ProtoEnum(Name="StringTableId_PropertyCategory_Electronic", Value=130)]
        StringTableId_PropertyCategory_Electronic = 130,
        [ProtoEnum(Name="StringTableId_EquipCategory_SelfEnhancer", Value=0x83)]
        StringTableId_EquipCategory_SelfEnhancer = 0x83,
        [ProtoEnum(Name="StringTableId_EquipCategory_RangeEnhancer", Value=0x84)]
        StringTableId_EquipCategory_RangeEnhancer = 0x84,
        [ProtoEnum(Name="StringTableId_EquipCategory_SelfRecover", Value=0x85)]
        StringTableId_EquipCategory_SelfRecover = 0x85,
        [ProtoEnum(Name="StringTableId_EquipCategory_RemoteRecover", Value=0x86)]
        StringTableId_EquipCategory_RemoteRecover = 0x86,
        [ProtoEnum(Name="StringTableId_EquipCategory_RangeRecover", Value=0x87)]
        StringTableId_EquipCategory_RangeRecover = 0x87,
        [ProtoEnum(Name="StringTableId_EquipCategory_RemoteDisturber", Value=0x88)]
        StringTableId_EquipCategory_RemoteDisturber = 0x88,
        [ProtoEnum(Name="StringTableId_EquipCategory_RangeDisturber", Value=0x89)]
        StringTableId_EquipCategory_RangeDisturber = 0x89,
        [ProtoEnum(Name="StringTableId_EquipCategory_TacticalEquipment", Value=0x8a)]
        StringTableId_EquipCategory_TacticalEquipment = 0x8a,
        [ProtoEnum(Name="StringTableId_EquipType_Booster", Value=0x8b)]
        StringTableId_EquipType_Booster = 0x8b,
        [ProtoEnum(Name="StringTableId_EquipType_BlinkExecutor", Value=140)]
        StringTableId_EquipType_BlinkExecutor = 140,
        [ProtoEnum(Name="StringTableId_EquipType_ShieldRepairer", Value=0x8d)]
        StringTableId_EquipType_ShieldRepairer = 0x8d,
        [ProtoEnum(Name="StringTableId_EquipType_ShieldDisturber", Value=0x8e)]
        StringTableId_EquipType_ShieldDisturber = 0x8e,
        [ProtoEnum(Name="StringTableId_EquipType_RangeDisturber", Value=0x8f)]
        StringTableId_EquipType_RangeDisturber = 0x8f,
        [ProtoEnum(Name="StringTableId_EquipType_WeaponDisturber", Value=0x90)]
        StringTableId_EquipType_WeaponDisturber = 0x90,
        [ProtoEnum(Name="StringTableId_EquipType_SpeedReducer", Value=0x91)]
        StringTableId_EquipType_SpeedReducer = 0x91,
        [ProtoEnum(Name="StringTableId_EquipType_EnergyDec", Value=0x92)]
        StringTableId_EquipType_EnergyDec = 0x92,
        [ProtoEnum(Name="StringTableId_EquipType_ExtraEnergyPackage", Value=0x93)]
        StringTableId_EquipType_ExtraEnergyPackage = 0x93,
        [ProtoEnum(Name="StringTableId_EquipType_RemoteShieldRepairer", Value=0x94)]
        StringTableId_EquipType_RemoteShieldRepairer = 0x94,
        [ProtoEnum(Name="StringTableId_EquipType_RemoteEnergyRepairer", Value=0x95)]
        StringTableId_EquipType_RemoteEnergyRepairer = 0x95,
        [ProtoEnum(Name="StringTableId_EquipType_TeamAtkAsst", Value=150)]
        StringTableId_EquipType_TeamAtkAsst = 150,
        [ProtoEnum(Name="StringTableId_EquipType_TeamDefAsst", Value=0x97)]
        StringTableId_EquipType_TeamDefAsst = 0x97,
        [ProtoEnum(Name="StringTableId_EquipType_TeamElecAsst", Value=0x98)]
        StringTableId_EquipType_TeamElecAsst = 0x98,
        [ProtoEnum(Name="StringTableId_EquipType_TeamMovAsst", Value=0x99)]
        StringTableId_EquipType_TeamMovAsst = 0x99,
        [ProtoEnum(Name="StringTableId_EquipType_Invisibility", Value=0x9a)]
        StringTableId_EquipType_Invisibility = 0x9a,
        [ProtoEnum(Name="StringTableId_EquipType_SelfWeaponModule", Value=0x9b)]
        StringTableId_EquipType_SelfWeaponModule = 0x9b,
        [ProtoEnum(Name="StringTableId_EquipType_SelfRangeModule", Value=0x9c)]
        StringTableId_EquipType_SelfRangeModule = 0x9c,
        [ProtoEnum(Name="StringTableId_EquipType_SelfDefModule", Value=0x9d)]
        StringTableId_EquipType_SelfDefModule = 0x9d,
        [ProtoEnum(Name="StringTableId_EquipType_BlinkBlocker", Value=0x9e)]
        StringTableId_EquipType_BlinkBlocker = 0x9e,
        [ProtoEnum(Name="StringTableId_EquipType_ShieldCreator", Value=0x9f)]
        StringTableId_EquipType_ShieldCreator = 0x9f,
        [ProtoEnum(Name="StringTableId_WeaponType", Value=160)]
        StringTableId_WeaponType = 160,
        [ProtoEnum(Name="StringTableId_NormalWeaponDesc", Value=0xa1)]
        StringTableId_NormalWeaponDesc = 0xa1,
        [ProtoEnum(Name="StringTableId_MineralType", Value=0xa2)]
        StringTableId_MineralType = 0xa2,
        [ProtoEnum(Name="StringTableId_ChipType", Value=0xa3)]
        StringTableId_ChipType = 0xa3,
        [ProtoEnum(Name="StringTableId_ActiveEquip", Value=0xa4)]
        StringTableId_ActiveEquip = 0xa4,
        [ProtoEnum(Name="StringTableId_PassiveEquip", Value=0xa5)]
        StringTableId_PassiveEquip = 0xa5,
        [ProtoEnum(Name="StringTableId_ShipHangarLockedErrorMsg", Value=0xa6)]
        StringTableId_ShipHangarLockedErrorMsg = 0xa6,
        [ProtoEnum(Name="StringTableId_HighSlotEmptyErrorMsg", Value=0xa7)]
        StringTableId_HighSlotEmptyErrorMsg = 0xa7,
        [ProtoEnum(Name="StringTableId_ChipExtraPropertyUnknown", Value=0xa8)]
        StringTableId_ChipExtraPropertyUnknown = 0xa8,
        [ProtoEnum(Name="StringTableId_ChipSuitPropertyNotExist", Value=0xa9)]
        StringTableId_ChipSuitPropertyNotExist = 0xa9,
        [ProtoEnum(Name="StringTableId_MotherShipItemStoreName", Value=170)]
        StringTableId_MotherShipItemStoreName = 170,
        [ProtoEnum(Name="StringTableId_HangarShipItemStoreName", Value=0xab)]
        StringTableId_HangarShipItemStoreName = 0xab,
        [ProtoEnum(Name="StringTableId_PvpSignalName", Value=0xac)]
        StringTableId_PvpSignalName = 0xac,
        [ProtoEnum(Name="StringTableId_NpcGender_Male", Value=0xad)]
        StringTableId_NpcGender_Male = 0xad,
        [ProtoEnum(Name="StringTableId_NpcGender_Female", Value=0xae)]
        StringTableId_NpcGender_Female = 0xae,
        [ProtoEnum(Name="StringTableId_NpcGender_Unknown", Value=0xaf)]
        StringTableId_NpcGender_Unknown = 0xaf,
        [ProtoEnum(Name="StringTableId_NpcCaptainState_OnDuty", Value=0xb0)]
        StringTableId_NpcCaptainState_OnDuty = 0xb0,
        [ProtoEnum(Name="StringTableId_NpcCaptainState_Intervene", Value=0xb1)]
        StringTableId_NpcCaptainState_Intervene = 0xb1,
        [ProtoEnum(Name="StringTableId_NpcCaptainState_Standby", Value=0xb2)]
        StringTableId_NpcCaptainState_Standby = 0xb2,
        [ProtoEnum(Name="StringTableId_CaptainFeatType_Init", Value=0xb3)]
        StringTableId_CaptainFeatType_Init = 0xb3,
        [ProtoEnum(Name="StringTableId_CaptainFeatType_Addition", Value=180)]
        StringTableId_CaptainFeatType_Addition = 180,
        [ProtoEnum(Name="StringTableId_PickDropBoxItemSuccess", Value=0xb5)]
        StringTableId_PickDropBoxItemSuccess = 0xb5,
        [ProtoEnum(Name="StringTableId_PickDropBoxItemFail", Value=0xb6)]
        StringTableId_PickDropBoxItemFail = 0xb6,
        [ProtoEnum(Name="StringTableId_CaptainShipUnlockSuccess", Value=0xb7)]
        StringTableId_CaptainShipUnlockSuccess = 0xb7,
        [ProtoEnum(Name="StringTableId_CaptainShipUnlockFail_ModuleNotEnough", Value=0xb8)]
        StringTableId_CaptainShipUnlockFail_ModuleNotEnough = 0xb8,
        [ProtoEnum(Name="StringTableId_CaptainShipUnlockFail_CaptainLvNotEnough", Value=0xb9)]
        StringTableId_CaptainShipUnlockFail_CaptainLvNotEnough = 0xb9,
        [ProtoEnum(Name="StringTableId_CaptainShipUnlockFail_DependShipNotUnlock", Value=0xba)]
        StringTableId_CaptainShipUnlockFail_DependShipNotUnlock = 0xba,
        [ProtoEnum(Name="StringTableId_CaptainShipRepairSuccess", Value=0xbb)]
        StringTableId_CaptainShipRepairSuccess = 0xbb,
        [ProtoEnum(Name="StringTableId_CaptainShipRepairFail_ModuleNotEnough", Value=0xbc)]
        StringTableId_CaptainShipRepairFail_ModuleNotEnough = 0xbc,
        [ProtoEnum(Name="StringTableId_CaptainShipSetCurrentShipSuccess", Value=0xbd)]
        StringTableId_CaptainShipSetCurrentShipSuccess = 0xbd,
        [ProtoEnum(Name="StringTableId_FunctionNotAvailable", Value=190)]
        StringTableId_FunctionNotAvailable = 190,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_FleetIsFull", Value=0xbf)]
        StringTableId_DelegateMissionStrike_FleetIsFull = 0xbf,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_TimeShort", Value=0xc0)]
        StringTableId_DelegateMissionStrike_TimeShort = 0xc0,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NeedMoreCaptain", Value=0xc1)]
        StringTableId_DelegateMissionStrike_NeedMoreCaptain = 0xc1,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NeedMoreFightAbility", Value=0xc2)]
        StringTableId_DelegateMissionStrike_NeedMoreFightAbility = 0xc2,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NeedMoreCollectAbility", Value=0xc3)]
        StringTableId_DelegateMissionStrike_NeedMoreCollectAbility = 0xc3,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NeedMoreTransportAbility", Value=0xc4)]
        StringTableId_DelegateMissionStrike_NeedMoreTransportAbility = 0xc4,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NotAbleToCollectAllMineral", Value=0xc5)]
        StringTableId_DelegateMissionStrike_NotAbleToCollectAllMineral = 0xc5,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_NoCaptainIsChosen", Value=0xc6)]
        StringTableId_DelegateMissionStrike_NoCaptainIsChosen = 0xc6,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_FleetNotFull", Value=0xc7)]
        StringTableId_DelegateMissionStrike_FleetNotFull = 0xc7,
        [ProtoEnum(Name="StringTableId_DelegateMissionJournal_InvadeStart", Value=200)]
        StringTableId_DelegateMissionJournal_InvadeStart = 200,
        [ProtoEnum(Name="StringTableId_DelegateMissionJournal_InvadeEnd", Value=0xc9)]
        StringTableId_DelegateMissionJournal_InvadeEnd = 0xc9,
        [ProtoEnum(Name="StringTableId_DelegateMissionJournal_DestroyShip", Value=0xca)]
        StringTableId_DelegateMissionJournal_DestroyShip = 0xca,
        [ProtoEnum(Name="StringTableId_TeamInfo_TeamMemberAdd", Value=0xcb)]
        StringTableId_TeamInfo_TeamMemberAdd = 0xcb,
        [ProtoEnum(Name="StringTableId_TeamInfo_TeamMemberLeave", Value=0xcc)]
        StringTableId_TeamInfo_TeamMemberLeave = 0xcc,
        [ProtoEnum(Name="StringTableId_TeamInfo_TeamInviteSendFinish", Value=0xcd)]
        StringTableId_TeamInfo_TeamInviteSendFinish = 0xcd,
        [ProtoEnum(Name="StringTableId_Chat_Alliance_Short", Value=0xce)]
        StringTableId_Chat_Alliance_Short = 0xce,
        [ProtoEnum(Name="StringTableId_Chat_Guild_Short", Value=0xcf)]
        StringTableId_Chat_Guild_Short = 0xcf,
        [ProtoEnum(Name="StringTableId_Chat_StarField_Short", Value=0xd0)]
        StringTableId_Chat_StarField_Short = 0xd0,
        [ProtoEnum(Name="StringTableId_Chat_SolarSystem_Short", Value=0xd1)]
        StringTableId_Chat_SolarSystem_Short = 0xd1,
        [ProtoEnum(Name="StringTableId_Chat_Local_Short", Value=210)]
        StringTableId_Chat_Local_Short = 210,
        [ProtoEnum(Name="StringTableId_Chat_System_Short", Value=0xd3)]
        StringTableId_Chat_System_Short = 0xd3,
        [ProtoEnum(Name="StringTableId_Chat_Wisper_Short", Value=0xd4)]
        StringTableId_Chat_Wisper_Short = 0xd4,
        [ProtoEnum(Name="StringTableId_Chat_Team_Short", Value=0xd5)]
        StringTableId_Chat_Team_Short = 0xd5,
        [ProtoEnum(Name="StringTableId_Chat_TextInputTip_Normal", Value=0xd6)]
        StringTableId_Chat_TextInputTip_Normal = 0xd6,
        [ProtoEnum(Name="StringTableId_Chat_TextInputTip_Wisper", Value=0xd7)]
        StringTableId_Chat_TextInputTip_Wisper = 0xd7,
        [ProtoEnum(Name="StringTableId_Chat_VoiceChatMsg", Value=0xd8)]
        StringTableId_Chat_VoiceChatMsg = 0xd8,
        [ProtoEnum(Name="StringTableId_Chat_LinkChatMsg", Value=0xd9)]
        StringTableId_Chat_LinkChatMsg = 0xd9,
        [ProtoEnum(Name="StringTableId_Mail_SendSuccess", Value=0xda)]
        StringTableId_Mail_SendSuccess = 0xda,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_StrikeSuccess", Value=0xdb)]
        StringTableId_DelegateMissionStrike_StrikeSuccess = 0xdb,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_SetCurrentShip_CaptainInMission", Value=220)]
        StringTableId_CaptainShipManage_SetCurrentShip_CaptainInMission = 220,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_RepairShip_ItemNeeded", Value=0xdd)]
        StringTableId_CaptainShipManage_RepairShip_ItemNeeded = 0xdd,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_RepairShip_NotStayWithMotherShip", Value=0xde)]
        StringTableId_CaptainShipManage_RepairShip_NotStayWithMotherShip = 0xde,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_UnlockShip_NotStayWithMotherShip", Value=0xdf)]
        StringTableId_CaptainShipManage_UnlockShip_NotStayWithMotherShip = 0xdf,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_UnlockShip_CaptainLevelNotEnough", Value=0xe0)]
        StringTableId_CaptainShipManage_UnlockShip_CaptainLevelNotEnough = 0xe0,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_UnlockShip_ItemNeeded", Value=0xe1)]
        StringTableId_CaptainShipManage_UnlockShip_ItemNeeded = 0xe1,
        [ProtoEnum(Name="StringTableId_Mail_TitleOrContentCantNull", Value=0xe2)]
        StringTableId_Mail_TitleOrContentCantNull = 0xe2,
        [ProtoEnum(Name="StringTableId_Mail_IsEditoringMail_IsOrNotLeave", Value=0xe3)]
        StringTableId_Mail_IsEditoringMail_IsOrNotLeave = 0xe3,
        [ProtoEnum(Name="StringTableId_Chat_SendSuccess", Value=0xe4)]
        StringTableId_Chat_SendSuccess = 0xe4,
        [ProtoEnum(Name="StringTableId_PlayerRebirthTips", Value=0xe5)]
        StringTableId_PlayerRebirthTips = 0xe5,
        [ProtoEnum(Name="StringTableId_CharacterClearPropertyPointsMainTips", Value=230)]
        StringTableId_CharacterClearPropertyPointsMainTips = 230,
        [ProtoEnum(Name="StringTableId_CharacterClearPropertyPointsSecondTips", Value=0xe7)]
        StringTableId_CharacterClearPropertyPointsSecondTips = 0xe7,
        [ProtoEnum(Name="StringTableId_QuestStrikeTip_AlreayInQuestScene", Value=0xe8)]
        StringTableId_QuestStrikeTip_AlreayInQuestScene = 0xe8,
        [ProtoEnum(Name="StringTableId_QuestStrikeTip_CurrentShipNotSuitableForQuest", Value=0xe9)]
        StringTableId_QuestStrikeTip_CurrentShipNotSuitableForQuest = 0xe9,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionQuestRefuse", Value=0xea)]
        StringTableId_NpcDialogOptionQuestRefuse = 0xea,
        [ProtoEnum(Name="StringTableId_NotAttackTarget", Value=0xeb)]
        StringTableId_NotAttackTarget = 0xeb,
        [ProtoEnum(Name="StringTableId_NpcShop_BuyOrSaleListNull", Value=0xec)]
        StringTableId_NpcShop_BuyOrSaleListNull = 0xec,
        [ProtoEnum(Name="StringTableId_Chat_CantSendSelfCharaterInfo", Value=0xed)]
        StringTableId_Chat_CantSendSelfCharaterInfo = 0xed,
        [ProtoEnum(Name="StringTableId_Mail_System", Value=0xee)]
        StringTableId_Mail_System = 0xee,
        [ProtoEnum(Name="StringTableId_Mail_SendTo", Value=0xef)]
        StringTableId_Mail_SendTo = 0xef,
        [ProtoEnum(Name="StringTableId_QuestAlreadyAccepted", Value=240)]
        StringTableId_QuestAlreadyAccepted = 240,
        [ProtoEnum(Name="StringTableId_QuestStrike_GoNow", Value=0xf1)]
        StringTableId_QuestStrike_GoNow = 0xf1,
        [ProtoEnum(Name="StringTableId_SuperWeapon", Value=0xf2)]
        StringTableId_SuperWeapon = 0xf2,
        [ProtoEnum(Name="StringTableId_QuestCompletedMsgInSpace", Value=0xf3)]
        StringTableId_QuestCompletedMsgInSpace = 0xf3,
        [ProtoEnum(Name="StringTableId_CharacterWeaponControlSkillDescAboutSkill", Value=0xf4)]
        StringTableId_CharacterWeaponControlSkillDescAboutSkill = 0xf4,
        [ProtoEnum(Name="StringTableId_CharacterWeaponControlSkillDescAboutBattle", Value=0xf5)]
        StringTableId_CharacterWeaponControlSkillDescAboutBattle = 0xf5,
        [ProtoEnum(Name="StringTableId_CharacterDefenseOperateSkillDescAboutSkill", Value=0xf6)]
        StringTableId_CharacterDefenseOperateSkillDescAboutSkill = 0xf6,
        [ProtoEnum(Name="StringTableId_CharacterDefenseOperateSkillDescAboutBattle", Value=0xf7)]
        StringTableId_CharacterDefenseOperateSkillDescAboutBattle = 0xf7,
        [ProtoEnum(Name="StringTableId_CharacterShipDriveSkillDescAboutSkill", Value=0xf8)]
        StringTableId_CharacterShipDriveSkillDescAboutSkill = 0xf8,
        [ProtoEnum(Name="StringTableId_CharacterShipDriveSkillDescAboutBattle", Value=0xf9)]
        StringTableId_CharacterShipDriveSkillDescAboutBattle = 0xf9,
        [ProtoEnum(Name="StringTableId_CharacterElectronicWarSkillDescAboutSkill", Value=250)]
        StringTableId_CharacterElectronicWarSkillDescAboutSkill = 250,
        [ProtoEnum(Name="StringTableId_CharacterElectronicWarSkillDescAboutBattle", Value=0xfb)]
        StringTableId_CharacterElectronicWarSkillDescAboutBattle = 0xfb,
        [ProtoEnum(Name="StringTableId_CharacterWeaponControlSkillSimpleDesc", Value=0xfc)]
        StringTableId_CharacterWeaponControlSkillSimpleDesc = 0xfc,
        [ProtoEnum(Name="StringTableId_CharacterDefenseOperateSkillSimpleDesc", Value=0xfd)]
        StringTableId_CharacterDefenseOperateSkillSimpleDesc = 0xfd,
        [ProtoEnum(Name="StringTableId_CharacterShipDriveSkillSimpleDesc", Value=0xfe)]
        StringTableId_CharacterShipDriveSkillSimpleDesc = 0xfe,
        [ProtoEnum(Name="StringTableId_CharacterElectronicWarSkillSimpleDesc", Value=0xff)]
        StringTableId_CharacterElectronicWarSkillSimpleDesc = 0xff,
        [ProtoEnum(Name="StringTableId_HiredCaptainTrainFail_NotEnoughItem", Value=0x100)]
        StringTableId_HiredCaptainTrainFail_NotEnoughItem = 0x100,
        [ProtoEnum(Name="StringTableId_HiredCaptainTrainFail_LevelExceedCharacter", Value=0x101)]
        StringTableId_HiredCaptainTrainFail_LevelExceedCharacter = 0x101,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetire_FeatRemainProbability", Value=0x102)]
        StringTableId_HiredCaptainRetire_FeatRemainProbability = 0x102,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetire_ShipFeatCanNotRemain", Value=0x103)]
        StringTableId_HiredCaptainRetire_ShipFeatCanNotRemain = 0x103,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_WingMan", Value=260)]
        StringTableId_HiredCaptainRetireFail_WingMan = 260,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_Working", Value=0x105)]
        StringTableId_HiredCaptainRetireFail_Working = 0x105,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_NoEmptyFeatBook", Value=0x106)]
        StringTableId_HiredCaptainRetireFail_NoEmptyFeatBook = 0x106,
        [ProtoEnum(Name="StringTableId_CurrentlyNoHiredCaptain", Value=0x107)]
        StringTableId_CurrentlyNoHiredCaptain = 0x107,
        [ProtoEnum(Name="StringTableId_QuestListUI_AbortQuest", Value=0x108)]
        StringTableId_QuestListUI_AbortQuest = 0x108,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_OccupiedByEnemy", Value=0x109)]
        StringTableId_BaseRedeploy_OccupiedByEnemy = 0x109,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_ConfirmDeploy", Value=0x10a)]
        StringTableId_BaseRedeploy_ConfirmDeploy = 0x10a,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_Destination", Value=0x10b)]
        StringTableId_BaseRedeploy_Destination = 0x10b,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_CancelRedeploy", Value=0x10c)]
        StringTableId_BaseRedeploy_CancelRedeploy = 0x10c,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_SpeedUpRedeployWithRealMoney", Value=0x10d)]
        StringTableId_BaseRedeploy_SpeedUpRedeployWithRealMoney = 0x10d,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_SpeedUpRedeployWithItem", Value=270)]
        StringTableId_BaseRedeploy_SpeedUpRedeployWithItem = 270,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_CompleteRedeploy", Value=0x10f)]
        StringTableId_BaseRedeploy_CompleteRedeploy = 0x10f,
        [ProtoEnum(Name="StringTableId_BaseRedeploy_CompleteTips", Value=0x110)]
        StringTableId_BaseRedeploy_CompleteTips = 0x110,
        [ProtoEnum(Name="StringTableId_CaptainWingMan_SetWingMan", Value=0x111)]
        StringTableId_CaptainWingMan_SetWingMan = 0x111,
        [ProtoEnum(Name="StringTableId_CaptainWingMan_CancelWingMan", Value=0x112)]
        StringTableId_CaptainWingMan_CancelWingMan = 0x112,
        [ProtoEnum(Name="StringTableId_CaptainWingMan_ReplaceWingMan", Value=0x113)]
        StringTableId_CaptainWingMan_ReplaceWingMan = 0x113,
        [ProtoEnum(Name="StringTableId_ShipCustomTemplateBuyItemFromMall", Value=0x114)]
        StringTableId_ShipCustomTemplateBuyItemFromMall = 0x114,
        [ProtoEnum(Name="StringTableId_CharacterNameIsInvaild", Value=0x115)]
        StringTableId_CharacterNameIsInvaild = 0x115,
        [ProtoEnum(Name="StringTableId_CharacterNameTooLong", Value=0x116)]
        StringTableId_CharacterNameTooLong = 0x116,
        [ProtoEnum(Name="StringTableId_ShipCustomTemplateInfoTemporaryName", Value=0x117)]
        StringTableId_ShipCustomTemplateInfoTemporaryName = 0x117,
        [ProtoEnum(Name="StringTableId_ShipCustomTemplateInfoApplySuccess", Value=280)]
        StringTableId_ShipCustomTemplateInfoApplySuccess = 280,
        [ProtoEnum(Name="StringTableId_ShipLevelDesc", Value=0x119)]
        StringTableId_ShipLevelDesc = 0x119,
        [ProtoEnum(Name="StringTableId_FirstLastNameStyle", Value=0x11a)]
        StringTableId_FirstLastNameStyle = 0x11a,
        [ProtoEnum(Name="StringTableId_TechUpgrade_Success", Value=0x11b)]
        StringTableId_TechUpgrade_Success = 0x11b,
        [ProtoEnum(Name="StringTableId_NotBelongTo", Value=0x11c)]
        StringTableId_NotBelongTo = 0x11c,
        [ProtoEnum(Name="StringTableId_CharacterDetailInfo_CannotLookUpSelf", Value=0x11d)]
        StringTableId_CharacterDetailInfo_CannotLookUpSelf = 0x11d,
        [ProtoEnum(Name="StringTableId_Produce_Stop", Value=0x11e)]
        StringTableId_Produce_Stop = 0x11e,
        [ProtoEnum(Name="StringTableId_Produce_ItemStore", Value=0x11f)]
        StringTableId_Produce_ItemStore = 0x11f,
        [ProtoEnum(Name="StringTableId_Produce_PleaseChooseBlueprintFirst", Value=0x120)]
        StringTableId_Produce_PleaseChooseBlueprintFirst = 0x120,
        [ProtoEnum(Name="StringTableId_Produce_CannotChooseSupCaptain", Value=0x121)]
        StringTableId_Produce_CannotChooseSupCaptain = 0x121,
        [ProtoEnum(Name="StringTableId_Produce_NoMoreBlueprint", Value=290)]
        StringTableId_Produce_NoMoreBlueprint = 290,
        [ProtoEnum(Name="StringTableId_Produce_GetProducationSuccess", Value=0x123)]
        StringTableId_Produce_GetProducationSuccess = 0x123,
        [ProtoEnum(Name="StringTableId_Produce_ProducationLineLocked", Value=0x124)]
        StringTableId_Produce_ProducationLineLocked = 0x124,
        [ProtoEnum(Name="StringTableId_Quest_LeaveSceneWillCancelQuest", Value=0x125)]
        StringTableId_Quest_LeaveSceneWillCancelQuest = 0x125,
        [ProtoEnum(Name="StringTableId_Quest_AcceptDialogDelayFinish", Value=0x126)]
        StringTableId_Quest_AcceptDialogDelayFinish = 0x126,
        [ProtoEnum(Name="StringTableId_SceneJumpForbiddenType_Quest", Value=0x127)]
        StringTableId_SceneJumpForbiddenType_Quest = 0x127,
        [ProtoEnum(Name="StringTableId_SceneJumpForbiddenType_Nature", Value=0x128)]
        StringTableId_SceneJumpForbiddenType_Nature = 0x128,
        [ProtoEnum(Name="StringTableId_Crack_UseMoneyToUnlockCrackSlot", Value=0x129)]
        StringTableId_Crack_UseMoneyToUnlockCrackSlot = 0x129,
        [ProtoEnum(Name="StringTableId_SceneLeaveReason_QuestFail", Value=0x12a)]
        StringTableId_SceneLeaveReason_QuestFail = 0x12a,
        [ProtoEnum(Name="StringTableId_SceneLeaveReason_SceneShutDown", Value=0x12b)]
        StringTableId_SceneLeaveReason_SceneShutDown = 0x12b,
        [ProtoEnum(Name="StringTableId_SolarSystemStationNameFormat", Value=300)]
        StringTableId_SolarSystemStationNameFormat = 300,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetire_StaticFeatCanNotRemain", Value=0x12d)]
        StringTableId_HiredCaptainRetire_StaticFeatCanNotRemain = 0x12d,
        [ProtoEnum(Name="StringTableId_FeatsBookType", Value=0x12e)]
        StringTableId_FeatsBookType = 0x12e,
        [ProtoEnum(Name="StringTableId_ProduceBlueprintType", Value=0x12f)]
        StringTableId_ProduceBlueprintType = 0x12f,
        [ProtoEnum(Name="StringTableId_CrackBoxType", Value=0x130)]
        StringTableId_CrackBoxType = 0x130,
        [ProtoEnum(Name="StringTableId_QuestItemType", Value=0x131)]
        StringTableId_QuestItemType = 0x131,
        [ProtoEnum(Name="StringTableId_StargateNamePrefix", Value=0x132)]
        StringTableId_StargateNamePrefix = 0x132,
        [ProtoEnum(Name="StringTableId_CharacterLevel", Value=0x133)]
        StringTableId_CharacterLevel = 0x133,
        [ProtoEnum(Name="StringTableId_ActivityVitalityRewardGetSuccess", Value=0x134)]
        StringTableId_ActivityVitalityRewardGetSuccess = 0x134,
        [ProtoEnum(Name="StringTableId_KillRecordDetailCantLookSelfCharacterInfo", Value=0x135)]
        StringTableId_KillRecordDetailCantLookSelfCharacterInfo = 0x135,
        [ProtoEnum(Name="StringTableId_SimpleInfoCantAddFriendWithSelf", Value=310)]
        StringTableId_SimpleInfoCantAddFriendWithSelf = 310,
        [ProtoEnum(Name="StringTableId_ChatNotJoinGuild", Value=0x137)]
        StringTableId_ChatNotJoinGuild = 0x137,
        [ProtoEnum(Name="StringTableId_ChatNotJoinAlliance", Value=0x138)]
        StringTableId_ChatNotJoinAlliance = 0x138,
        [ProtoEnum(Name="StringTableId_MailGetAttachItemSuccess", Value=0x139)]
        StringTableId_MailGetAttachItemSuccess = 0x139,
        [ProtoEnum(Name="StringTableId_LicenseUpgradComplete", Value=0x13a)]
        StringTableId_LicenseUpgradComplete = 0x13a,
        [ProtoEnum(Name="StringTableId_LicenseUpradeTimeReduceTip", Value=0x13b)]
        StringTableId_LicenseUpradeTimeReduceTip = 0x13b,
        [ProtoEnum(Name="StringTableId_LicenseGetRewardSuccess", Value=0x13c)]
        StringTableId_LicenseGetRewardSuccess = 0x13c,
        [ProtoEnum(Name="StringTableId_LicenseCancelUpgradeSuccess", Value=0x13d)]
        StringTableId_LicenseCancelUpgradeSuccess = 0x13d,
        [ProtoEnum(Name="StringTableId_LicenseStartUpgrade", Value=0x13e)]
        StringTableId_LicenseStartUpgrade = 0x13e,
        [ProtoEnum(Name="StringTableId_HiredCaptainTrainFail_CaptainMaxLevel", Value=0x13f)]
        StringTableId_HiredCaptainTrainFail_CaptainMaxLevel = 0x13f,
        [ProtoEnum(Name="StringTableId_DelegateMissionStrike_DelegateMissionFleetMax", Value=320)]
        StringTableId_DelegateMissionStrike_DelegateMissionFleetMax = 320,
        [ProtoEnum(Name="StringTableId_CaptainShipManage_CaptainLvNotEnoughToActivate", Value=0x141)]
        StringTableId_CaptainShipManage_CaptainLvNotEnoughToActivate = 0x141,
        [ProtoEnum(Name="StringTableId_ShipStrikeFail_LicenseError", Value=0x142)]
        StringTableId_ShipStrikeFail_LicenseError = 0x142,
        [ProtoEnum(Name="StringTableId_Charging", Value=0x143)]
        StringTableId_Charging = 0x143,
        [ProtoEnum(Name="StringTableId_CoolDown", Value=0x144)]
        StringTableId_CoolDown = 0x144,
        [ProtoEnum(Name="StringTableId_CrimeState_JustCrimeTitle", Value=0x145)]
        StringTableId_CrimeState_JustCrimeTitle = 0x145,
        [ProtoEnum(Name="StringTableId_CrimeState_JustCrimeDesc", Value=0x146)]
        StringTableId_CrimeState_JustCrimeDesc = 0x146,
        [ProtoEnum(Name="StringTableId_CrimeState_CrimeTitle", Value=0x147)]
        StringTableId_CrimeState_CrimeTitle = 0x147,
        [ProtoEnum(Name="StringTableId_CrimeState_CrimeDesc", Value=0x148)]
        StringTableId_CrimeState_CrimeDesc = 0x148,
        [ProtoEnum(Name="StringTableId_CrimeState_SuspectTitle", Value=0x149)]
        StringTableId_CrimeState_SuspectTitle = 0x149,
        [ProtoEnum(Name="StringTableId_CrimeState_SuspectDesc", Value=330)]
        StringTableId_CrimeState_SuspectDesc = 330,
        [ProtoEnum(Name="StringTableId_CrimeState_CrimeHunterTitle", Value=0x14b)]
        StringTableId_CrimeState_CrimeHunterTitle = 0x14b,
        [ProtoEnum(Name="StringTableId_CrimeState_CrimeHunterDesc", Value=0x14c)]
        StringTableId_CrimeState_CrimeHunterDesc = 0x14c,
        [ProtoEnum(Name="StringTableId_PVPStateTitle", Value=0x14d)]
        StringTableId_PVPStateTitle = 0x14d,
        [ProtoEnum(Name="StringTableId_PVPStateDesc", Value=0x14e)]
        StringTableId_PVPStateDesc = 0x14e,
        [ProtoEnum(Name="StringTableId_UnitAUString", Value=0x14f)]
        StringTableId_UnitAUString = 0x14f,
        [ProtoEnum(Name="StringTableId_UnitAUPerSecondString", Value=0x150)]
        StringTableId_UnitAUPerSecondString = 0x150,
        [ProtoEnum(Name="StringTableId_UnitMinuteString", Value=0x151)]
        StringTableId_UnitMinuteString = 0x151,
        [ProtoEnum(Name="StringTableId_UnitHourString", Value=0x152)]
        StringTableId_UnitHourString = 0x152,
        [ProtoEnum(Name="StringTableId_UnitDayString", Value=0x153)]
        StringTableId_UnitDayString = 0x153,
        [ProtoEnum(Name="StringTableId_UnitAEONString", Value=340)]
        StringTableId_UnitAEONString = 340,
        [ProtoEnum(Name="StringTableId_UnitTemperatureString", Value=0x155)]
        StringTableId_UnitTemperatureString = 0x155,
        [ProtoEnum(Name="StringTableId_UnitKilogramString", Value=0x156)]
        StringTableId_UnitKilogramString = 0x156,
        [ProtoEnum(Name="StringTableId_UnitGravityString", Value=0x157)]
        StringTableId_UnitGravityString = 0x157,
        [ProtoEnum(Name="StringTableId_QuestRewardAlreadyGet", Value=0x158)]
        StringTableId_QuestRewardAlreadyGet = 0x158,
        [ProtoEnum(Name="StringTableId_QuestRewardCannotGet_QuestNotComplete", Value=0x159)]
        StringTableId_QuestRewardCannotGet_QuestNotComplete = 0x159,
        [ProtoEnum(Name="StringTableId_QuestRewardCannotGet_JustInMotherShip", Value=0x15a)]
        StringTableId_QuestRewardCannotGet_JustInMotherShip = 0x15a,
        [ProtoEnum(Name="StringTableId_ExperienceValue", Value=0x15b)]
        StringTableId_ExperienceValue = 0x15b,
        [ProtoEnum(Name="StringTableId_LeaveWormHole", Value=0x15c)]
        StringTableId_LeaveWormHole = 0x15c,
        [ProtoEnum(Name="StringTableId_NavigationSolarSystemInfo", Value=0x15d)]
        StringTableId_NavigationSolarSystemInfo = 0x15d,
        [ProtoEnum(Name="StringTableId_NavigationJumpCountInfo", Value=350)]
        StringTableId_NavigationJumpCountInfo = 350,
        [ProtoEnum(Name="StringTableId_NavigationStart", Value=0x15f)]
        StringTableId_NavigationStart = 0x15f,
        [ProtoEnum(Name="StringTableId_NavigationStop", Value=0x160)]
        StringTableId_NavigationStop = 0x160,
        [ProtoEnum(Name="StringTableId_NavigationDestChanged", Value=0x161)]
        StringTableId_NavigationDestChanged = 0x161,
        [ProtoEnum(Name="StringTableId_Level", Value=0x162)]
        StringTableId_Level = 0x162,
        [ProtoEnum(Name="StringTableId_DrivingCheckCompelet", Value=0x163)]
        StringTableId_DrivingCheckCompelet = 0x163,
        [ProtoEnum(Name="StringTableId_SpeedUpItemEffect", Value=0x164)]
        StringTableId_SpeedUpItemEffect = 0x164,
        [ProtoEnum(Name="StringTableId_UnLockDrivingLicense", Value=0x165)]
        StringTableId_UnLockDrivingLicense = 0x165,
        [ProtoEnum(Name="StringTableId_EquipPropertyTitleRange", Value=0x166)]
        StringTableId_EquipPropertyTitleRange = 0x166,
        [ProtoEnum(Name="StringTableId_EquipPropertyTitleScope", Value=0x167)]
        StringTableId_EquipPropertyTitleScope = 0x167,
        [ProtoEnum(Name="StringTableId_CharacterChipSlotIsLockedSelf", Value=360)]
        StringTableId_CharacterChipSlotIsLockedSelf = 360,
        [ProtoEnum(Name="StringTableId_CharacterChipSlotIsLockedOther", Value=0x169)]
        StringTableId_CharacterChipSlotIsLockedOther = 0x169,
        [ProtoEnum(Name="StringTableId_CharacterChipSlotIsEmpty", Value=0x16a)]
        StringTableId_CharacterChipSlotIsEmpty = 0x16a,
        [ProtoEnum(Name="StringTableId_StoreItemListUIModule", Value=0x16b)]
        StringTableId_StoreItemListUIModule = 0x16b,
        [ProtoEnum(Name="StringTableId_StoreItemListUIManufactureMaterial", Value=0x16c)]
        StringTableId_StoreItemListUIManufactureMaterial = 0x16c,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintShip", Value=0x16d)]
        StringTableId_StoreItemListUIBlueprintShip = 0x16d,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintDevice", Value=0x16e)]
        StringTableId_StoreItemListUIBlueprintDevice = 0x16e,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintComponent", Value=0x16f)]
        StringTableId_StoreItemListUIBlueprintComponent = 0x16f,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintWeapon", Value=0x170)]
        StringTableId_StoreItemListUIBlueprintWeapon = 0x170,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintAmmo", Value=0x171)]
        StringTableId_StoreItemListUIBlueprintAmmo = 0x171,
        [ProtoEnum(Name="StringTableId_StoreItemListUIBlueprintOther", Value=370)]
        StringTableId_StoreItemListUIBlueprintOther = 370,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Componnet_DamageEnhancer", Value=0x173)]
        StringTableId_StoreItemListUI_Componnet_DamageEnhancer = 0x173,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Componnet_ShootRangeEnhancer", Value=0x174)]
        StringTableId_StoreItemListUI_Componnet_ShootRangeEnhancer = 0x174,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Componnet_ElectronicEnhancer", Value=0x175)]
        StringTableId_StoreItemListUI_Componnet_ElectronicEnhancer = 0x175,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Componnet_ShieldEnhancer", Value=0x176)]
        StringTableId_StoreItemListUI_Componnet_ShieldEnhancer = 0x176,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Componnet_DrivingEnhancer", Value=0x177)]
        StringTableId_StoreItemListUI_Componnet_DrivingEnhancer = 0x177,
        [ProtoEnum(Name="StringTableId_Chat_NotJoinAllianceTip", Value=0x178)]
        StringTableId_Chat_NotJoinAllianceTip = 0x178,
        [ProtoEnum(Name="StringTableId_Chat_NotFindMicrophone", Value=0x179)]
        StringTableId_Chat_NotFindMicrophone = 0x179,
        [ProtoEnum(Name="StringTableId_Chat_AllianceNameShow", Value=0x17a)]
        StringTableId_Chat_AllianceNameShow = 0x17a,
        [ProtoEnum(Name="StringTableId_Chat_TeamChannelJoinTeamTip", Value=0x17b)]
        StringTableId_Chat_TeamChannelJoinTeamTip = 0x17b,
        [ProtoEnum(Name="StringTableId_Chat_TeamChannelleaveTeamTip", Value=380)]
        StringTableId_Chat_TeamChannelleaveTeamTip = 380,
        [ProtoEnum(Name="StringTableId_Chat_VoiceOverdued", Value=0x17d)]
        StringTableId_Chat_VoiceOverdued = 0x17d,
        [ProtoEnum(Name="StringTableId_Mail_AttachementStillInMailCannotRemoveTip", Value=0x17e)]
        StringTableId_Mail_AttachementStillInMailCannotRemoveTip = 0x17e,
        [ProtoEnum(Name="StringTableId_Mail_PleaseGetAttachementFirst", Value=0x17f)]
        StringTableId_Mail_PleaseGetAttachementFirst = 0x17f,
        [ProtoEnum(Name="StringTableId_Mail_DeleteMailSuccess", Value=0x180)]
        StringTableId_Mail_DeleteMailSuccess = 0x180,
        [ProtoEnum(Name="StringTableId_Produce_ProduceLineFull", Value=0x181)]
        StringTableId_Produce_ProduceLineFull = 0x181,
        [ProtoEnum(Name="StringTableId_Produce_ProduceStart", Value=0x182)]
        StringTableId_Produce_ProduceStart = 0x182,
        [ProtoEnum(Name="StringTableId_ShipHangarAssign_TradingPlaceNotOpen", Value=0x183)]
        StringTableId_ShipHangarAssign_TradingPlaceNotOpen = 0x183,
        [ProtoEnum(Name="StringTableId_TechUpgrade_CancelReturnResShow", Value=0x184)]
        StringTableId_TechUpgrade_CancelReturnResShow = 0x184,
        [ProtoEnum(Name="StringTableId_TechUpgrade_CancelUpgradeSuccess", Value=0x185)]
        StringTableId_TechUpgrade_CancelUpgradeSuccess = 0x185,
        [ProtoEnum(Name="StringTableId_TechUpgrade_RealMoneyUpgradeTip", Value=390)]
        StringTableId_TechUpgrade_RealMoneyUpgradeTip = 390,
        [ProtoEnum(Name="StringTableId_TechUpgrade_StartUpgrade", Value=0x187)]
        StringTableId_TechUpgrade_StartUpgrade = 0x187,
        [ProtoEnum(Name="StringTableId_TechUpgrade_UpgradeSuccessNtfToQuitCurrWindow", Value=0x188)]
        StringTableId_TechUpgrade_UpgradeSuccessNtfToQuitCurrWindow = 0x188,
        [ProtoEnum(Name="StringTableId_DrivingLicense_CharacterLevelNotEnough", Value=0x189)]
        StringTableId_DrivingLicense_CharacterLevelNotEnough = 0x189,
        [ProtoEnum(Name="StringTableId_Captain_FullLevel", Value=0x18a)]
        StringTableId_Captain_FullLevel = 0x18a,
        [ProtoEnum(Name="StringTableId_QuestTypeName_FreeSingalEngineer", Value=0x18b)]
        StringTableId_QuestTypeName_FreeSingalEngineer = 0x18b,
        [ProtoEnum(Name="StringTableId_QuestTypeName_FreeSingalScientist", Value=0x18c)]
        StringTableId_QuestTypeName_FreeSingalScientist = 0x18c,
        [ProtoEnum(Name="StringTableId_QuestTypeName_FreeSingalSoldier", Value=0x18d)]
        StringTableId_QuestTypeName_FreeSingalSoldier = 0x18d,
        [ProtoEnum(Name="StringTableId_QuestTypeName_FreeSingalExplorer", Value=0x18e)]
        StringTableId_QuestTypeName_FreeSingalExplorer = 0x18e,
        [ProtoEnum(Name="StringTableId_ShipStrikeFail_CostNotEnough", Value=0x18f)]
        StringTableId_ShipStrikeFail_CostNotEnough = 0x18f,
        [ProtoEnum(Name="StringTableId_ShipStrikeFail_ShipTypeError", Value=400)]
        StringTableId_ShipStrikeFail_ShipTypeError = 400,
        [ProtoEnum(Name="StringTableId_InfectTimeCountDown", Value=0x191)]
        StringTableId_InfectTimeCountDown = 0x191,
        [ProtoEnum(Name="StringTableId_ShipHangarErrorInfo_CostNotEnough", Value=0x192)]
        StringTableId_ShipHangarErrorInfo_CostNotEnough = 0x192,
        [ProtoEnum(Name="StringTableId_ShipHangarErrorInfo_LicenseError", Value=0x193)]
        StringTableId_ShipHangarErrorInfo_LicenseError = 0x193,
        [ProtoEnum(Name="StringTableId_WormholeBufWarningInfo", Value=0x194)]
        StringTableId_WormholeBufWarningInfo = 0x194,
        [ProtoEnum(Name="StringTableId_WormholeBufContinueWarningInfo", Value=0x195)]
        StringTableId_WormholeBufContinueWarningInfo = 0x195,
        [ProtoEnum(Name="StringTableId_SpecialNavigationPointAppear", Value=0x196)]
        StringTableId_SpecialNavigationPointAppear = 0x196,
        [ProtoEnum(Name="StringTableId_JumpToWormholeBackPoint", Value=0x197)]
        StringTableId_JumpToWormholeBackPoint = 0x197,
        [ProtoEnum(Name="StringTableId_WormholeEvacuateWarning", Value=0x198)]
        StringTableId_WormholeEvacuateWarning = 0x198,
        [ProtoEnum(Name="StringTableId_HighSlotAmmo", Value=0x199)]
        StringTableId_HighSlotAmmo = 0x199,
        [ProtoEnum(Name="StringTableId_NotExistWormhole", Value=410)]
        StringTableId_NotExistWormhole = 410,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetTitle_HighSlot", Value=0x19b)]
        StringTableId_WeaponEquipSetTitle_HighSlot = 0x19b,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetTitle_HighSlotAmmo", Value=0x19c)]
        StringTableId_WeaponEquipSetTitle_HighSlotAmmo = 0x19c,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetTitle_MiddleSlot", Value=0x19d)]
        StringTableId_WeaponEquipSetTitle_MiddleSlot = 0x19d,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetTitle_LowSlot", Value=0x19e)]
        StringTableId_WeaponEquipSetTitle_LowSlot = 0x19e,
        [ProtoEnum(Name="StringTableId_TechUpgradeItemSpeedUpTimeDesc", Value=0x19f)]
        StringTableId_TechUpgradeItemSpeedUpTimeDesc = 0x19f,
        [ProtoEnum(Name="StringTableId_Mail_WhetherToDeleteMail", Value=0x1a0)]
        StringTableId_Mail_WhetherToDeleteMail = 0x1a0,
        [ProtoEnum(Name="StringTableId_SelfInvadedInfo", Value=0x1a1)]
        StringTableId_SelfInvadedInfo = 0x1a1,
        [ProtoEnum(Name="StringTableId_AllianceInvadeInfo", Value=0x1a2)]
        StringTableId_AllianceInvadeInfo = 0x1a2,
        [ProtoEnum(Name="StringTableId_UnLoadSucceed", Value=0x1a3)]
        StringTableId_UnLoadSucceed = 0x1a3,
        [ProtoEnum(Name="StringTableId_ItemStoreError_Empty", Value=420)]
        StringTableId_ItemStoreError_Empty = 420,
        [ProtoEnum(Name="StringTableId_ItemStoreError_CantAccess", Value=0x1a5)]
        StringTableId_ItemStoreError_CantAccess = 0x1a5,
        [ProtoEnum(Name="StringTableId_ItemStoreError_ShipStoreEmpty", Value=0x1a6)]
        StringTableId_ItemStoreError_ShipStoreEmpty = 0x1a6,
        [ProtoEnum(Name="StringTableId_WingShip_LeaveStation", Value=0x1a7)]
        StringTableId_WingShip_LeaveStation = 0x1a7,
        [ProtoEnum(Name="StringTableId_WingShip_Arrived", Value=0x1a8)]
        StringTableId_WingShip_Arrived = 0x1a8,
        [ProtoEnum(Name="StringTableId_WingShip_Destroy", Value=0x1a9)]
        StringTableId_WingShip_Destroy = 0x1a9,
        [ProtoEnum(Name="StringTableId_WingShip_Evacuate", Value=0x1aa)]
        StringTableId_WingShip_Evacuate = 0x1aa,
        [ProtoEnum(Name="StringTableId_Mail_PleaseChooseItems", Value=0x1ab)]
        StringTableId_Mail_PleaseChooseItems = 0x1ab,
        [ProtoEnum(Name="StringTableId_FreeQuestRefreshInfo", Value=0x1ac)]
        StringTableId_FreeQuestRefreshInfo = 0x1ac,
        [ProtoEnum(Name="StringTableId_FreeQuestRefreshExtraInfo", Value=0x1ad)]
        StringTableId_FreeQuestRefreshExtraInfo = 0x1ad,
        [ProtoEnum(Name="StringTableId_FreeQuestRefreshTitle", Value=430)]
        StringTableId_FreeQuestRefreshTitle = 430,
        [ProtoEnum(Name="StringTableId_ShipHangarErrorInfo_SlotLicenseError", Value=0x1af)]
        StringTableId_ShipHangarErrorInfo_SlotLicenseError = 0x1af,
        [ProtoEnum(Name="StringTableId_SolarSystem_QuestCompleteNotify", Value=0x1b0)]
        StringTableId_SolarSystem_QuestCompleteNotify = 0x1b0,
        [ProtoEnum(Name="StringTableId_SolarSystem_QuestFailedNotify", Value=0x1b1)]
        StringTableId_SolarSystem_QuestFailedNotify = 0x1b1,
        [ProtoEnum(Name="StringTableId_WingShip_Intervening", Value=0x1b2)]
        StringTableId_WingShip_Intervening = 0x1b2,
        [ProtoEnum(Name="StringTableId_WingShip_Standby", Value=0x1b3)]
        StringTableId_WingShip_Standby = 0x1b3,
        [ProtoEnum(Name="StringTableId_OnlyOperateInMotherShip", Value=0x1b4)]
        StringTableId_OnlyOperateInMotherShip = 0x1b4,
        [ProtoEnum(Name="StringTableId_CharacterClearItemGreaterThan", Value=0x1b5)]
        StringTableId_CharacterClearItemGreaterThan = 0x1b5,
        [ProtoEnum(Name="StringTableId_CharacterClearItemLessThan", Value=0x1b6)]
        StringTableId_CharacterClearItemLessThan = 0x1b6,
        [ProtoEnum(Name="StringTableId_DelegateMissionCancelByPlayerSuceess", Value=0x1b7)]
        StringTableId_DelegateMissionCancelByPlayerSuceess = 0x1b7,
        [ProtoEnum(Name="StringTableId_CharactorSkillDescForAttack", Value=440)]
        StringTableId_CharactorSkillDescForAttack = 440,
        [ProtoEnum(Name="StringTableId_CharactorSkillDescForDefense", Value=0x1b9)]
        StringTableId_CharactorSkillDescForDefense = 0x1b9,
        [ProtoEnum(Name="StringTableId_CharactorSkillDescForElectronic", Value=0x1ba)]
        StringTableId_CharactorSkillDescForElectronic = 0x1ba,
        [ProtoEnum(Name="StringTableId_NormalAttackTipForUnattackableTarget", Value=0x1bb)]
        StringTableId_NormalAttackTipForUnattackableTarget = 0x1bb,
        [ProtoEnum(Name="StringTableId_RankingCharacterLevelLimit", Value=0x1bc)]
        StringTableId_RankingCharacterLevelLimit = 0x1bc,
        [ProtoEnum(Name="StringTableId_RankingPercentInfo", Value=0x1bd)]
        StringTableId_RankingPercentInfo = 0x1bd,
        [ProtoEnum(Name="StringTableId_Unlock_ShipHangarSlot", Value=0x1be)]
        StringTableId_Unlock_ShipHangarSlot = 0x1be,
        [ProtoEnum(Name="StringTableId_Unlock_CharacterSlot", Value=0x1bf)]
        StringTableId_Unlock_CharacterSlot = 0x1bf,
        [ProtoEnum(Name="StringTableId_UnLock_ProduceLineSlot", Value=0x1c0)]
        StringTableId_UnLock_ProduceLineSlot = 0x1c0,
        [ProtoEnum(Name="StringTableId_FreeQuestLocked", Value=0x1c1)]
        StringTableId_FreeQuestLocked = 0x1c1,
        [ProtoEnum(Name="StringTableId_WeaponAttackRangeDependsOnAmmo", Value=450)]
        StringTableId_WeaponAttackRangeDependsOnAmmo = 450,
        [ProtoEnum(Name="StringTableId_AutoAmmoReload", Value=0x1c3)]
        StringTableId_AutoAmmoReload = 0x1c3,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_ExpressReceive", Value=0x1c4)]
        StringTableId_MsgAnnouncement_ExpressReceive = 0x1c4,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_DropItemGet", Value=0x1c5)]
        StringTableId_MsgAnnouncement_DropItemGet = 0x1c5,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_Dead", Value=0x1c6)]
        StringTableId_MsgAnnouncement_Dead = 0x1c6,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_Team_Invate", Value=0x1c7)]
        StringTableId_MsgAnnouncement_Team_Invate = 0x1c7,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_Team_LeaveSelf", Value=0x1c8)]
        StringTableId_MsgAnnouncement_Team_LeaveSelf = 0x1c8,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_Team_LeaveOther", Value=0x1c9)]
        StringTableId_MsgAnnouncement_Team_LeaveOther = 0x1c9,
        [ProtoEnum(Name="StringTableId_MsgAnnouncement_Team_Create", Value=0x1ca)]
        StringTableId_MsgAnnouncement_Team_Create = 0x1ca,
        [ProtoEnum(Name="StringTableId_SovereignNotExist", Value=0x1cb)]
        StringTableId_SovereignNotExist = 0x1cb,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1001", Value=460)]
        StringTableId_ZilongSDKErrorCode1001 = 460,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1002", Value=0x1cd)]
        StringTableId_ZilongSDKErrorCode1002 = 0x1cd,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1099", Value=0x1ce)]
        StringTableId_ZilongSDKErrorCode1099 = 0x1ce,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1101", Value=0x1cf)]
        StringTableId_ZilongSDKErrorCode1101 = 0x1cf,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1102", Value=0x1d0)]
        StringTableId_ZilongSDKErrorCode1102 = 0x1d0,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1103", Value=0x1d1)]
        StringTableId_ZilongSDKErrorCode1103 = 0x1d1,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1104", Value=0x1d2)]
        StringTableId_ZilongSDKErrorCode1104 = 0x1d2,
        [ProtoEnum(Name="StringTableId_ZilongSDKErrorCode1105", Value=0x1d3)]
        StringTableId_ZilongSDKErrorCode1105 = 0x1d3,
        [ProtoEnum(Name="StringTableId_ShipStrike_CaptainFirst", Value=0x1d4)]
        StringTableId_ShipStrike_CaptainFirst = 0x1d4,
        [ProtoEnum(Name="StringTableId_ShipStrike_CaptainAlternate", Value=0x1d5)]
        StringTableId_ShipStrike_CaptainAlternate = 0x1d5,
        [ProtoEnum(Name="StringTableId_ShipStrike_CaptainCantStrike", Value=470)]
        StringTableId_ShipStrike_CaptainCantStrike = 470,
        [ProtoEnum(Name="StringTableId_ShipStrike_CaptainCantStrikeForShipType", Value=0x1d7)]
        StringTableId_ShipStrike_CaptainCantStrikeForShipType = 0x1d7,
        [ProtoEnum(Name="StringTableId_ShipStrike_CaptainCantStrikeForShipDamage", Value=0x1d8)]
        StringTableId_ShipStrike_CaptainCantStrikeForShipDamage = 0x1d8,
        [ProtoEnum(Name="StringTableId_QuestRewardBonus", Value=0x1d9)]
        StringTableId_QuestRewardBonus = 0x1d9,
        [ProtoEnum(Name="StringTableId_DailyFreeQuestBurstReward", Value=0x1da)]
        StringTableId_DailyFreeQuestBurstReward = 0x1da,
        [ProtoEnum(Name="StringTableId_DailyFreeQuestDesc", Value=0x1db)]
        StringTableId_DailyFreeQuestDesc = 0x1db,
        [ProtoEnum(Name="StringTableId_DelegateQuest_Success", Value=0x1dc)]
        StringTableId_DelegateQuest_Success = 0x1dc,
        [ProtoEnum(Name="StringTableId_DelegateQuest_Fail", Value=0x1dd)]
        StringTableId_DelegateQuest_Fail = 0x1dd,
        [ProtoEnum(Name="StringTableId_DelegateQuest_Cancel", Value=0x1de)]
        StringTableId_DelegateQuest_Cancel = 0x1de,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_NoInvade", Value=0x1df)]
        StringTableId_DelegateSimpleItem_NoInvade = 0x1df,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_BeInvadedFailWithSingleShip", Value=480)]
        StringTableId_DelegateSimpleItem_BeInvadedFailWithSingleShip = 480,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_BeInvadedFailWithMoreShip", Value=0x1e1)]
        StringTableId_DelegateSimpleItem_BeInvadedFailWithMoreShip = 0x1e1,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_BeInvadedSuccessWithSingleShip", Value=0x1e2)]
        StringTableId_DelegateSimpleItem_BeInvadedSuccessWithSingleShip = 0x1e2,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_BeInvadedSucessWithMoreShip", Value=0x1e3)]
        StringTableId_DelegateSimpleItem_BeInvadedSucessWithMoreShip = 0x1e3,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_NoInvade_NoReward", Value=0x1e4)]
        StringTableId_DelegateSimpleItem_NoInvade_NoReward = 0x1e4,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_NoInvade_CompleteWithReward", Value=0x1e5)]
        StringTableId_DelegateSimpleItem_NoInvade_CompleteWithReward = 0x1e5,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_NoInvade_NormalReward", Value=0x1e6)]
        StringTableId_DelegateSimpleItem_NoInvade_NormalReward = 0x1e6,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_Invade_LossAndNoReward", Value=0x1e7)]
        StringTableId_DelegateSimpleItem_Invade_LossAndNoReward = 0x1e7,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_Invade_CompleteWithRewardAndNoLoss", Value=0x1e8)]
        StringTableId_DelegateSimpleItem_Invade_CompleteWithRewardAndNoLoss = 0x1e8,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_Invade_CompleteWithRewardAndLoss", Value=0x1e9)]
        StringTableId_DelegateSimpleItem_Invade_CompleteWithRewardAndLoss = 0x1e9,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_Invade_RewardAndNoLoss", Value=490)]
        StringTableId_DelegateSimpleItem_Invade_RewardAndNoLoss = 490,
        [ProtoEnum(Name="StringTableId_DelegateSimpleItem_Invade_RewardAndLoss", Value=0x1eb)]
        StringTableId_DelegateSimpleItem_Invade_RewardAndLoss = 0x1eb,
        [ProtoEnum(Name="StringTableId_DelegateSimplePanel_AtLeastOne", Value=0x1ec)]
        StringTableId_DelegateSimplePanel_AtLeastOne = 0x1ec,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadePlayerEnter", Value=0x1ed)]
        StringTableId_InvadeEvent_InvadePlayerEnter = 0x1ed,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadePlayerDead_KillByPlayer", Value=0x1ee)]
        StringTableId_InvadeEvent_InvadePlayerDead_KillByPlayer = 0x1ee,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadePlayerDead_KillByCaptain", Value=0x1ef)]
        StringTableId_InvadeEvent_InvadePlayerDead_KillByCaptain = 0x1ef,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadePlayerDead_Other", Value=0x1f0)]
        StringTableId_InvadeEvent_InvadePlayerDead_Other = 0x1f0,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadeCaptainDead", Value=0x1f1)]
        StringTableId_InvadeEvent_InvadeCaptainDead = 0x1f1,
        [ProtoEnum(Name="StringTableId_InvadeEvent_InvadePlayerLeave", Value=0x1f2)]
        StringTableId_InvadeEvent_InvadePlayerLeave = 0x1f2,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefensePlayerEnter", Value=0x1f3)]
        StringTableId_InvadeEvent_DefensePlayerEnter = 0x1f3,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefensePlayerDead_KillByPlayer", Value=500)]
        StringTableId_InvadeEvent_DefensePlayerDead_KillByPlayer = 500,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefensePlayerDead_KillByCaptain", Value=0x1f5)]
        StringTableId_InvadeEvent_DefensePlayerDead_KillByCaptain = 0x1f5,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefensePlayerDead_KillByOther", Value=0x1f6)]
        StringTableId_InvadeEvent_DefensePlayerDead_KillByOther = 0x1f6,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefenseCaptainDead", Value=0x1f7)]
        StringTableId_InvadeEvent_DefenseCaptainDead = 0x1f7,
        [ProtoEnum(Name="StringTableId_InvadeEvent_DefensePlayerLeave", Value=0x1f8)]
        StringTableId_InvadeEvent_DefensePlayerLeave = 0x1f8,
        [ProtoEnum(Name="StringTableId_InvadeEvent_LossInfo", Value=0x1f9)]
        StringTableId_InvadeEvent_LossInfo = 0x1f9,
        [ProtoEnum(Name="StringTableId_InvadeEvent_BeInvading", Value=0x1fa)]
        StringTableId_InvadeEvent_BeInvading = 0x1fa,
        [ProtoEnum(Name="StringTableId_TechUpgrade_CannotSelectCaptain", Value=0x1fb)]
        StringTableId_TechUpgrade_CannotSelectCaptain = 0x1fb,
        [ProtoEnum(Name="StringTableId_TechUpgrade_NoCaptain", Value=0x1fc)]
        StringTableId_TechUpgrade_NoCaptain = 0x1fc,
        [ProtoEnum(Name="StringTableId_TechUpgrade_FuncNotOpen", Value=0x1fd)]
        StringTableId_TechUpgrade_FuncNotOpen = 0x1fd,
        [ProtoEnum(Name="StringTableId_NpcShopSortTypeStr_All", Value=510)]
        StringTableId_NpcShopSortTypeStr_All = 510,
        [ProtoEnum(Name="StringTableId_NpcShopSortTypeStr_SubRank", Value=0x1ff)]
        StringTableId_NpcShopSortTypeStr_SubRank = 0x1ff,
        [ProtoEnum(Name="StringTableId_NpcShopSortTypeStr_Size", Value=0x200)]
        StringTableId_NpcShopSortTypeStr_Size = 0x200,
        [ProtoEnum(Name="StringTableId_NpcShopSortTypeStr_Price", Value=0x201)]
        StringTableId_NpcShopSortTypeStr_Price = 0x201,
        [ProtoEnum(Name="StringTableId_NpcShop_Title", Value=0x202)]
        StringTableId_NpcShop_Title = 0x202,
        [ProtoEnum(Name="StringTableId_QuestExpRewardBonus", Value=0x203)]
        StringTableId_QuestExpRewardBonus = 0x203,
        [ProtoEnum(Name="StringTableId_QuestBindMoneyRewardBonus", Value=0x204)]
        StringTableId_QuestBindMoneyRewardBonus = 0x204,
        [ProtoEnum(Name="StringTableId_DelegateFightMissionRewardBonus", Value=0x205)]
        StringTableId_DelegateFightMissionRewardBonus = 0x205,
        [ProtoEnum(Name="StringTableId_DelegateMineralMissionRewardBonus", Value=0x206)]
        StringTableId_DelegateMineralMissionRewardBonus = 0x206,
        [ProtoEnum(Name="StringTableId_DelegateTransportMissionRewardBonus", Value=0x207)]
        StringTableId_DelegateTransportMissionRewardBonus = 0x207,
        [ProtoEnum(Name="StringTableId_RewardBonus", Value=520)]
        StringTableId_RewardBonus = 520,
        [ProtoEnum(Name="StringTableId_QuestItemRewardBonus", Value=0x209)]
        StringTableId_QuestItemRewardBonus = 0x209,
        [ProtoEnum(Name="StringTableId_SpecialQuestReward", Value=0x20a)]
        StringTableId_SpecialQuestReward = 0x20a,
        [ProtoEnum(Name="StringTableId_SpecialQuestRewardDesc", Value=0x20b)]
        StringTableId_SpecialQuestRewardDesc = 0x20b,
        [ProtoEnum(Name="StringTableId_AmmoCannotBuyInNPCShop", Value=0x20c)]
        StringTableId_AmmoCannotBuyInNPCShop = 0x20c,
        [ProtoEnum(Name="StringTableId_LastFilterShouldRemain", Value=0x20d)]
        StringTableId_LastFilterShouldRemain = 0x20d,
        [ProtoEnum(Name="StringTableId_ConfirmCancelItemReShelve", Value=0x20e)]
        StringTableId_ConfirmCancelItemReShelve = 0x20e,
        [ProtoEnum(Name="StringTableId_ItemFreezingRemainTime", Value=0x20f)]
        StringTableId_ItemFreezingRemainTime = 0x20f,
        [ProtoEnum(Name="StringTableId_CurrItemNotSaleInAuction", Value=0x210)]
        StringTableId_CurrItemNotSaleInAuction = 0x210,
        [ProtoEnum(Name="StringTableId_AuctionItemOnShelveSuccess", Value=0x211)]
        StringTableId_AuctionItemOnShelveSuccess = 0x211,
        [ProtoEnum(Name="StringTableId_AuctionItemOnShelveNotEnoughGoldCoins", Value=530)]
        StringTableId_AuctionItemOnShelveNotEnoughGoldCoins = 530,
        [ProtoEnum(Name="StringTableId_ConfirmUnShelveAuctionItem", Value=0x213)]
        StringTableId_ConfirmUnShelveAuctionItem = 0x213,
        [ProtoEnum(Name="StringTableId_Enemy", Value=0x214)]
        StringTableId_Enemy = 0x214,
        [ProtoEnum(Name="StringTableId_Neutral", Value=0x215)]
        StringTableId_Neutral = 0x215,
        [ProtoEnum(Name="StringTableId_Friend", Value=0x216)]
        StringTableId_Friend = 0x216,
        [ProtoEnum(Name="StringTableId_SetPersonalDiplomaticRelationWithGuild", Value=0x217)]
        StringTableId_SetPersonalDiplomaticRelationWithGuild = 0x217,
        [ProtoEnum(Name="StringTableId_SetGuildDiplomaticRelationWithGuild", Value=0x218)]
        StringTableId_SetGuildDiplomaticRelationWithGuild = 0x218,
        [ProtoEnum(Name="StringTableId_GuildJoinPolicy_AutoJoin", Value=0x219)]
        StringTableId_GuildJoinPolicy_AutoJoin = 0x219,
        [ProtoEnum(Name="StringTableId_GuildJoinPolicy_NeedConfirm", Value=0x21a)]
        StringTableId_GuildJoinPolicy_NeedConfirm = 0x21a,
        [ProtoEnum(Name="StringTableId_GuildInfoChangeSucceed", Value=0x21b)]
        StringTableId_GuildInfoChangeSucceed = 0x21b,
        [ProtoEnum(Name="StringTableId_GuildInfoChangeFail", Value=540)]
        StringTableId_GuildInfoChangeFail = 540,
        [ProtoEnum(Name="StringTableId_SailReportDeathEventName", Value=0x21d)]
        StringTableId_SailReportDeathEventName = 0x21d,
        [ProtoEnum(Name="StringTableId_SailReportFinalBattleEventName", Value=0x21e)]
        StringTableId_SailReportFinalBattleEventName = 0x21e,
        [ProtoEnum(Name="StringTableId_SailReportPvpFightEventName", Value=0x21f)]
        StringTableId_SailReportPvpFightEventName = 0x21f,
        [ProtoEnum(Name="StringTableId_SearchForPlayer", Value=0x220)]
        StringTableId_SearchForPlayer = 0x220,
        [ProtoEnum(Name="StringTableId_SearchForGuildByName", Value=0x221)]
        StringTableId_SearchForGuildByName = 0x221,
        [ProtoEnum(Name="StringTableId_SearchForGuildByCode", Value=0x222)]
        StringTableId_SearchForGuildByCode = 0x222,
        [ProtoEnum(Name="StringTableId_SearchForAlliance", Value=0x223)]
        StringTableId_SearchForAlliance = 0x223,
        [ProtoEnum(Name="StringTableId_OperationNeedPermission", Value=0x224)]
        StringTableId_OperationNeedPermission = 0x224,
        [ProtoEnum(Name="StringTableId_Online", Value=0x225)]
        StringTableId_Online = 0x225,
        [ProtoEnum(Name="StringTableId_Before", Value=550)]
        StringTableId_Before = 550,
        [ProtoEnum(Name="StringTableId_GuildAllianceIsNull", Value=0x227)]
        StringTableId_GuildAllianceIsNull = 0x227,
        [ProtoEnum(Name="StringTableId_GuildOfficeIsNull", Value=0x228)]
        StringTableId_GuildOfficeIsNull = 0x228,
        [ProtoEnum(Name="StringTableId_AuctionBuyCountOverThanCanBuyCount", Value=0x229)]
        StringTableId_AuctionBuyCountOverThanCanBuyCount = 0x229,
        [ProtoEnum(Name="StringTableId_AuctionBuyCountOverThanMaxBuyCount", Value=0x22a)]
        StringTableId_AuctionBuyCountOverThanMaxBuyCount = 0x22a,
        [ProtoEnum(Name="StringTableId_AuctionBuyCountOverThanBuyTotalItemCount", Value=0x22b)]
        StringTableId_AuctionBuyCountOverThanBuyTotalItemCount = 0x22b,
        [ProtoEnum(Name="StringTableId_AuctionBuyItemSuccess", Value=0x22c)]
        StringTableId_AuctionBuyItemSuccess = 0x22c,
        [ProtoEnum(Name="StringTableId_SetDiplomacyNeutral", Value=0x22d)]
        StringTableId_SetDiplomacyNeutral = 0x22d,
        [ProtoEnum(Name="StringTableId_SetDiplomacyNeutralRepetitively", Value=0x22e)]
        StringTableId_SetDiplomacyNeutralRepetitively = 0x22e,
        [ProtoEnum(Name="StringTableId_SetDiplomacyFriend", Value=0x22f)]
        StringTableId_SetDiplomacyFriend = 0x22f,
        [ProtoEnum(Name="StringTableId_SetDiplomacyFriendRepetitively", Value=560)]
        StringTableId_SetDiplomacyFriendRepetitively = 560,
        [ProtoEnum(Name="StringTableId_SetDiplomacyEnemy", Value=0x231)]
        StringTableId_SetDiplomacyEnemy = 0x231,
        [ProtoEnum(Name="StringTableId_SetDiplomacyEnemyRepetitively", Value=0x232)]
        StringTableId_SetDiplomacyEnemyRepetitively = 0x232,
        [ProtoEnum(Name="StringTableId_ChipDisChargeConfirm", Value=0x233)]
        StringTableId_ChipDisChargeConfirm = 0x233,
        [ProtoEnum(Name="StringTableId_AuctionItemIsSaleOut", Value=0x234)]
        StringTableId_AuctionItemIsSaleOut = 0x234,
        [ProtoEnum(Name="StringTableId_GuildQuitMsg", Value=0x235)]
        StringTableId_GuildQuitMsg = 0x235,
        [ProtoEnum(Name="StringTableId_NoChange", Value=0x236)]
        StringTableId_NoChange = 0x236,
        [ProtoEnum(Name="StringTableId_TimeFormatHour", Value=0x237)]
        StringTableId_TimeFormatHour = 0x237,
        [ProtoEnum(Name="StringTableId_TimeFormatSecond", Value=0x238)]
        StringTableId_TimeFormatSecond = 0x238,
        [ProtoEnum(Name="StringTableId_TimeFormatHourMinute", Value=0x239)]
        StringTableId_TimeFormatHourMinute = 0x239,
        [ProtoEnum(Name="StringTableId_TimeFormatMinuteSecond", Value=570)]
        StringTableId_TimeFormatMinuteSecond = 570,
        [ProtoEnum(Name="StringTableId_GuildNameRule", Value=0x23b)]
        StringTableId_GuildNameRule = 0x23b,
        [ProtoEnum(Name="StringTableId_GuildNameEmpty", Value=0x23c)]
        StringTableId_GuildNameEmpty = 0x23c,
        [ProtoEnum(Name="StringTableId_GuildNameStart", Value=0x23d)]
        StringTableId_GuildNameStart = 0x23d,
        [ProtoEnum(Name="StringTableId_GuildCodeRule", Value=0x23e)]
        StringTableId_GuildCodeRule = 0x23e,
        [ProtoEnum(Name="StringTableId_GuildCodeEmpty", Value=0x23f)]
        StringTableId_GuildCodeEmpty = 0x23f,
        [ProtoEnum(Name="StringTableId_GuildCodeStart", Value=0x240)]
        StringTableId_GuildCodeStart = 0x240,
        [ProtoEnum(Name="StringTableId_ExpelGuild", Value=0x241)]
        StringTableId_ExpelGuild = 0x241,
        [ProtoEnum(Name="StringTableId_JustTime", Value=0x242)]
        StringTableId_JustTime = 0x242,
        [ProtoEnum(Name="StringTableId_GuildNameHasSensitiveWord", Value=0x243)]
        StringTableId_GuildNameHasSensitiveWord = 0x243,
        [ProtoEnum(Name="StringTableId_GuildCodeHasSensitiveWord", Value=580)]
        StringTableId_GuildCodeHasSensitiveWord = 580,
        [ProtoEnum(Name="StringTableId_SignalFightScene", Value=0x245)]
        StringTableId_SignalFightScene = 0x245,
        [ProtoEnum(Name="StringTableId_SignalMineralScene", Value=0x246)]
        StringTableId_SignalMineralScene = 0x246,
        [ProtoEnum(Name="StringTableId_SignalFreeQuest", Value=0x247)]
        StringTableId_SignalFreeQuest = 0x247,
        [ProtoEnum(Name="StringTableId_SignalExploreQuest", Value=0x248)]
        StringTableId_SignalExploreQuest = 0x248,
        [ProtoEnum(Name="StringTableId_FriendFightConfirm", Value=0x249)]
        StringTableId_FriendFightConfirm = 0x249,
        [ProtoEnum(Name="StringTableId_DoExplorerFreeQuestToGetExploreTicket", Value=0x24a)]
        StringTableId_DoExplorerFreeQuestToGetExploreTicket = 0x24a,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetFilterName_HighSlot", Value=0x24b)]
        StringTableId_WeaponEquipSetFilterName_HighSlot = 0x24b,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetFilterName_HighSlotAmmo", Value=0x24c)]
        StringTableId_WeaponEquipSetFilterName_HighSlotAmmo = 0x24c,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetFilterName_MiddleSlot", Value=0x24d)]
        StringTableId_WeaponEquipSetFilterName_MiddleSlot = 0x24d,
        [ProtoEnum(Name="StringTableId_WeaponEquipSetFilterName_LowSlot", Value=590)]
        StringTableId_WeaponEquipSetFilterName_LowSlot = 590,
        [ProtoEnum(Name="StringTableId_ShipHangarSetFilterName", Value=0x24f)]
        StringTableId_ShipHangarSetFilterName = 0x24f,
        [ProtoEnum(Name="StringTableId_GuildBaseSolarSystemSetSucceed", Value=0x250)]
        StringTableId_GuildBaseSolarSystemSetSucceed = 0x250,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenAuction", Value=0x251)]
        StringTableId_NpcDialogOptionOpenAuction = 0x251,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionGuildBaseSolarSystemSet", Value=0x252)]
        StringTableId_NpcDialogOptionGuildBaseSolarSystemSet = 0x252,
        [ProtoEnum(Name="StringTableId_ChatTag_Item", Value=0x253)]
        StringTableId_ChatTag_Item = 0x253,
        [ProtoEnum(Name="StringTableId_ChatTag_Location", Value=0x254)]
        StringTableId_ChatTag_Location = 0x254,
        [ProtoEnum(Name="StringTableId_ChatTag_Player", Value=0x255)]
        StringTableId_ChatTag_Player = 0x255,
        [ProtoEnum(Name="StringTableId_ChatTag_TeamInvite", Value=0x256)]
        StringTableId_ChatTag_TeamInvite = 0x256,
        [ProtoEnum(Name="StringTableId_ChatTag_Ship", Value=0x257)]
        StringTableId_ChatTag_Ship = 0x257,
        [ProtoEnum(Name="StringTableId_ChatTag_KillRecord", Value=600)]
        StringTableId_ChatTag_KillRecord = 600,
        [ProtoEnum(Name="StringTableId_ChatTag_DelegateMission", Value=0x259)]
        StringTableId_ChatTag_DelegateMission = 0x259,
        [ProtoEnum(Name="StringTableId_ChatTag_Guild", Value=0x25a)]
        StringTableId_ChatTag_Guild = 0x25a,
        [ProtoEnum(Name="StringTableId_ChatLinkColor", Value=0x25b)]
        StringTableId_ChatLinkColor = 0x25b,
        [ProtoEnum(Name="StringTableId_ChatBannedTip", Value=0x25c)]
        StringTableId_ChatBannedTip = 0x25c,
        [ProtoEnum(Name="StringTableId_MailNameCharacterLimit", Value=0x25d)]
        StringTableId_MailNameCharacterLimit = 0x25d,
        [ProtoEnum(Name="StringTableId_MailDetailCharacterLimit", Value=0x25e)]
        StringTableId_MailDetailCharacterLimit = 0x25e,
        [ProtoEnum(Name="StringTableId_UnLockDrivingLicenseTip", Value=0x25f)]
        StringTableId_UnLockDrivingLicenseTip = 0x25f,
        [ProtoEnum(Name="StringTableId_LogOutNormalTip", Value=0x260)]
        StringTableId_LogOutNormalTip = 0x260,
        [ProtoEnum(Name="StringTableId_LogOutInSpaceTip", Value=0x261)]
        StringTableId_LogOutInSpaceTip = 0x261,
        [ProtoEnum(Name="StringTableId_GrandFactionError", Value=610)]
        StringTableId_GrandFactionError = 610,
        [ProtoEnum(Name="StringTableId_BaseResearchHasBeenCompleted", Value=0x263)]
        StringTableId_BaseResearchHasBeenCompleted = 0x263,
        [ProtoEnum(Name="StringTableId_BaseDecipheringTaskHasBeenCompleted", Value=0x264)]
        StringTableId_BaseDecipheringTaskHasBeenCompleted = 0x264,
        [ProtoEnum(Name="StringTableId_BaseProductionTaskHasBeenCompleted", Value=0x265)]
        StringTableId_BaseProductionTaskHasBeenCompleted = 0x265,
        [ProtoEnum(Name="StringTableId_TheDispatchFleetHasReturnedToTheBase", Value=0x266)]
        StringTableId_TheDispatchFleetHasReturnedToTheBase = 0x266,
        [ProtoEnum(Name="StringTableId_TheDispatchFleetDevastatedWithdrawn", Value=0x267)]
        StringTableId_TheDispatchFleetDevastatedWithdrawn = 0x267,
        [ProtoEnum(Name="StringTableId_EnemyEnterSceneTip", Value=0x268)]
        StringTableId_EnemyEnterSceneTip = 0x268,
        [ProtoEnum(Name="StringTableId_AttemptAttackFriendTip", Value=0x269)]
        StringTableId_AttemptAttackFriendTip = 0x269,
        [ProtoEnum(Name="StringTableId_WormholeLimitEnterLevel", Value=0x26a)]
        StringTableId_WormholeLimitEnterLevel = 0x26a,
        [ProtoEnum(Name="StringTableId_WormholeRecommendEnterLevel", Value=0x26b)]
        StringTableId_WormholeRecommendEnterLevel = 0x26b,
        [ProtoEnum(Name="StringTableId_NoJoinTeamTip", Value=620)]
        StringTableId_NoJoinTeamTip = 620,
        [ProtoEnum(Name="StringTableId_CaptainShipUnlockFail_NeedUnLockFront", Value=0x26d)]
        StringTableId_CaptainShipUnlockFail_NeedUnLockFront = 0x26d,
        [ProtoEnum(Name="StringTableId_CaptainShipIsDrivingTip", Value=0x26e)]
        StringTableId_CaptainShipIsDrivingTip = 0x26e,
        [ProtoEnum(Name="StringTableId_TaskLevelIsTooHigh", Value=0x26f)]
        StringTableId_TaskLevelIsTooHigh = 0x26f,
        [ProtoEnum(Name="StringTableId_ChipCategory_Attack", Value=0x270)]
        StringTableId_ChipCategory_Attack = 0x270,
        [ProtoEnum(Name="StringTableId_ChipCategory_Drive", Value=0x271)]
        StringTableId_ChipCategory_Drive = 0x271,
        [ProtoEnum(Name="StringTableId_ChipCategory_Defence", Value=0x272)]
        StringTableId_ChipCategory_Defence = 0x272,
        [ProtoEnum(Name="StringTableId_ChipCategory_Electron", Value=0x273)]
        StringTableId_ChipCategory_Electron = 0x273,
        [ProtoEnum(Name="StringTableId_QuestLevelCondNotSatisfy", Value=0x274)]
        StringTableId_QuestLevelCondNotSatisfy = 0x274,
        [ProtoEnum(Name="StringTableId_LocalSystemMsgTitle", Value=0x275)]
        StringTableId_LocalSystemMsgTitle = 0x275,
        [ProtoEnum(Name="StringTableId_GlobalSystemMsgTitle", Value=630)]
        StringTableId_GlobalSystemMsgTitle = 630,
        [ProtoEnum(Name="StringTableId_TeamInviteConfirmSuccess", Value=0x277)]
        StringTableId_TeamInviteConfirmSuccess = 0x277,
        [ProtoEnum(Name="StringTableId_FunctionNotOpen", Value=0x278)]
        StringTableId_FunctionNotOpen = 0x278,
        [ProtoEnum(Name="StringTableId_FactionShopNpcPrefix", Value=0x279)]
        StringTableId_FactionShopNpcPrefix = 0x279,
        [ProtoEnum(Name="StringTableId_SystemShopNpcPrefix", Value=0x27a)]
        StringTableId_SystemShopNpcPrefix = 0x27a,
        [ProtoEnum(Name="StringTableId_AuctionNpcPrefix", Value=0x27b)]
        StringTableId_AuctionNpcPrefix = 0x27b,
        [ProtoEnum(Name="StringTableId_GuildBaseSolarSystemSetNpcPrefix", Value=0x27c)]
        StringTableId_GuildBaseSolarSystemSetNpcPrefix = 0x27c,
        [ProtoEnum(Name="StringTableId_YouMustBeAtTheBaseStationForScientificResearchOperations", Value=0x27d)]
        StringTableId_YouMustBeAtTheBaseStationForScientificResearchOperations = 0x27d,
        [ProtoEnum(Name="StringTableId_ScienceExplore_PuzzleGameCompleteCondDesc", Value=0x27e)]
        StringTableId_ScienceExplore_PuzzleGameCompleteCondDesc = 0x27e,
        [ProtoEnum(Name="StringTableId_NoPropertyPointWillReturnAfterClearProperties", Value=0x27f)]
        StringTableId_NoPropertyPointWillReturnAfterClearProperties = 0x27f,
        [ProtoEnum(Name="StringTableId_GuildQuitDone", Value=640)]
        StringTableId_GuildQuitDone = 640,
        [ProtoEnum(Name="StringTableId_GuildNameEdited", Value=0x281)]
        StringTableId_GuildNameEdited = 0x281,
        [ProtoEnum(Name="StringTableId_GuildManifestoEdited", Value=0x282)]
        StringTableId_GuildManifestoEdited = 0x282,
        [ProtoEnum(Name="StringTableId_GuildAnnouncementEdited", Value=0x283)]
        StringTableId_GuildAnnouncementEdited = 0x283,
        [ProtoEnum(Name="StringTableId_JumpingCantDoThis", Value=0x284)]
        StringTableId_JumpingCantDoThis = 0x284,
        [ProtoEnum(Name="StringTableId_AssistantCantStrike", Value=0x285)]
        StringTableId_AssistantCantStrike = 0x285,
        [ProtoEnum(Name="StringTableId_QuestComplteCond_CompleteQuest", Value=0x286)]
        StringTableId_QuestComplteCond_CompleteQuest = 0x286,
        [ProtoEnum(Name="StringTableId_FinishAllChapterGoalQuest", Value=0x287)]
        StringTableId_FinishAllChapterGoalQuest = 0x287,
        [ProtoEnum(Name="StringTableId_GetCharacterExpTip", Value=0x288)]
        StringTableId_GetCharacterExpTip = 0x288,
        [ProtoEnum(Name="StringTableId_NotFindCanInvadeSignal", Value=0x289)]
        StringTableId_NotFindCanInvadeSignal = 0x289,
        [ProtoEnum(Name="StringTableId_ExitGameTip", Value=650)]
        StringTableId_ExitGameTip = 650,
        [ProtoEnum(Name="StringTableId_DisconnectedBySameAccountLogin", Value=0x28b)]
        StringTableId_DisconnectedBySameAccountLogin = 0x28b,
        [ProtoEnum(Name="StringTableId_DisconnectedByServer", Value=0x28c)]
        StringTableId_DisconnectedByServer = 0x28c,
        [ProtoEnum(Name="StringTableId_NpcTalkerDialogCountdownString", Value=0x28d)]
        StringTableId_NpcTalkerDialogCountdownString = 0x28d,
        [ProtoEnum(Name="StringTableId_ExitButton", Value=0x28e)]
        StringTableId_ExitButton = 0x28e,
        [ProtoEnum(Name="StringTableId_ExitTitle", Value=0x28f)]
        StringTableId_ExitTitle = 0x28f,
        [ProtoEnum(Name="StringTableId_LogoutButton", Value=0x290)]
        StringTableId_LogoutButton = 0x290,
        [ProtoEnum(Name="StringTableId_LogoutTitle", Value=0x291)]
        StringTableId_LogoutTitle = 0x291,
        [ProtoEnum(Name="StringTableId_CancellationOfAction", Value=0x292)]
        StringTableId_CancellationOfAction = 0x292,
        [ProtoEnum(Name="StringTableId_Produce_HasNoCaptain", Value=0x293)]
        StringTableId_Produce_HasNoCaptain = 0x293,
        [ProtoEnum(Name="StringTableId_GameVersion", Value=660)]
        StringTableId_GameVersion = 660,
        [ProtoEnum(Name="StringTableId_GameServerShutdownContextReasonBan", Value=0x295)]
        StringTableId_GameServerShutdownContextReasonBan = 0x295,
        [ProtoEnum(Name="StringTableId_LoginAuthTokenFail_ErrorCode_11", Value=0x296)]
        StringTableId_LoginAuthTokenFail_ErrorCode_11 = 0x296,
        [ProtoEnum(Name="StringTableId_PlayerIDHasBeenCopy", Value=0x297)]
        StringTableId_PlayerIDHasBeenCopy = 0x297,
        [ProtoEnum(Name="StringTableId_ServerMaintenance", Value=0x298)]
        StringTableId_ServerMaintenance = 0x298,
        [ProtoEnum(Name="StringTableId_BundleVersionMisMatch", Value=0x299)]
        StringTableId_BundleVersionMisMatch = 0x299,
        [ProtoEnum(Name="StringTableId_ServerIsFull", Value=0x29a)]
        StringTableId_ServerIsFull = 0x29a,
        [ProtoEnum(Name="StringTableId_ConnectServerFailed", Value=0x29b)]
        StringTableId_ConnectServerFailed = 0x29b,
        [ProtoEnum(Name="StringTableId_AccountNameOrPasswordWrong", Value=0x29c)]
        StringTableId_AccountNameOrPasswordWrong = 0x29c,
        [ProtoEnum(Name="StringTableId_AccountBan", Value=0x29d)]
        StringTableId_AccountBan = 0x29d,
        [ProtoEnum(Name="StringTableId_InvalidClientVersion", Value=670)]
        StringTableId_InvalidClientVersion = 670,
        [ProtoEnum(Name="StringTableId_SessionTokenOutOfTime", Value=0x29f)]
        StringTableId_SessionTokenOutOfTime = 0x29f,
        [ProtoEnum(Name="StringTableId_SameAccountLogin", Value=0x2a0)]
        StringTableId_SameAccountLogin = 0x2a0,
        [ProtoEnum(Name="StringTableId_NormalWeaponOffline", Value=0x2a1)]
        StringTableId_NormalWeaponOffline = 0x2a1,
        [ProtoEnum(Name="StringTableId_SceneDoNotAllowHireCaptain", Value=0x2a2)]
        StringTableId_SceneDoNotAllowHireCaptain = 0x2a2,
        [ProtoEnum(Name="StringTableId_CannotUseProbeInWormholeSolarSystem", Value=0x2a3)]
        StringTableId_CannotUseProbeInWormholeSolarSystem = 0x2a3,
        [ProtoEnum(Name="StringTableId_NotSelectAnyChannelWhenSendLink", Value=0x2a4)]
        StringTableId_NotSelectAnyChannelWhenSendLink = 0x2a4,
        [ProtoEnum(Name="StringTableId_Mail_UnableToDeleteMessageManually", Value=0x2a5)]
        StringTableId_Mail_UnableToDeleteMessageManually = 0x2a5,
        [ProtoEnum(Name="StringTableId_RefreshTooFrequently", Value=0x2a6)]
        StringTableId_RefreshTooFrequently = 0x2a6,
        [ProtoEnum(Name="StringTableId_AuctionBuyItemListRefreshed", Value=0x2a7)]
        StringTableId_AuctionBuyItemListRefreshed = 0x2a7,
        [ProtoEnum(Name="StringTableId_HideZeroCountAuctionBuyItem", Value=680)]
        StringTableId_HideZeroCountAuctionBuyItem = 680,
        [ProtoEnum(Name="StringTableId_ShowAllAuctionBuyItem", Value=0x2a9)]
        StringTableId_ShowAllAuctionBuyItem = 0x2a9,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Missle", Value=0x2aa)]
        StringTableId_StoreItemListUI_Ammo_Missle = 0x2aa,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Laser", Value=0x2ab)]
        StringTableId_StoreItemListUI_Ammo_Laser = 0x2ab,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Railgun", Value=0x2ac)]
        StringTableId_StoreItemListUI_Ammo_Railgun = 0x2ac,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Plasma", Value=0x2ad)]
        StringTableId_StoreItemListUI_Ammo_Plasma = 0x2ad,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Drone", Value=0x2ae)]
        StringTableId_StoreItemListUI_Ammo_Drone = 0x2ae,
        [ProtoEnum(Name="StringTableId_StoreItemListUI_Ammo_Cannon", Value=0x2af)]
        StringTableId_StoreItemListUI_Ammo_Cannon = 0x2af,
        [ProtoEnum(Name="StringTableId_Chat_PlayerInfomation", Value=0x2b0)]
        StringTableId_Chat_PlayerInfomation = 0x2b0,
        [ProtoEnum(Name="StringTableId_Chat_Team", Value=0x2b1)]
        StringTableId_Chat_Team = 0x2b1,
        [ProtoEnum(Name="StringTableId_Chat_KillRecord", Value=690)]
        StringTableId_Chat_KillRecord = 690,
        [ProtoEnum(Name="StringTableId_Chat_DelegateMission", Value=0x2b3)]
        StringTableId_Chat_DelegateMission = 0x2b3,
        [ProtoEnum(Name="StringTableId_NoCaptainSetUpIsStillAttacked", Value=0x2b4)]
        StringTableId_NoCaptainSetUpIsStillAttacked = 0x2b4,
        [ProtoEnum(Name="StringTableId_TheTypeOfCaptainShipNotMeetTheRequirements", Value=0x2b5)]
        StringTableId_TheTypeOfCaptainShipNotMeetTheRequirements = 0x2b5,
        [ProtoEnum(Name="StringTableId_SceneWingShipSettingAlwaysDisable", Value=0x2b6)]
        StringTableId_SceneWingShipSettingAlwaysDisable = 0x2b6,
        [ProtoEnum(Name="StringTableId_AssistantShipSuitableToStrike", Value=0x2b7)]
        StringTableId_AssistantShipSuitableToStrike = 0x2b7,
        [ProtoEnum(Name="StringTableId_CaptainSetCurrShipWhenShipUnlocked", Value=0x2b8)]
        StringTableId_CaptainSetCurrShipWhenShipUnlocked = 0x2b8,
        [ProtoEnum(Name="StringTableId_TheCaptainIsInProcessUnableRespondTheOrder", Value=0x2b9)]
        StringTableId_TheCaptainIsInProcessUnableRespondTheOrder = 0x2b9,
        [ProtoEnum(Name="StringTableId_TheCaptainHasBeenDestroyNewOneIsReady", Value=0x2ba)]
        StringTableId_TheCaptainHasBeenDestroyNewOneIsReady = 0x2ba,
        [ProtoEnum(Name="StringTableId_ChatTag_SailReport", Value=0x2bb)]
        StringTableId_ChatTag_SailReport = 0x2bb,
        [ProtoEnum(Name="StringTableId_LackOfBindMoney", Value=700)]
        StringTableId_LackOfBindMoney = 700,
        [ProtoEnum(Name="StringTableId_StaffingLog_JobAppoint", Value=0x2bd)]
        StringTableId_StaffingLog_JobAppoint = 0x2bd,
        [ProtoEnum(Name="StringTableId_StaffingLog_Join", Value=0x2be)]
        StringTableId_StaffingLog_Join = 0x2be,
        [ProtoEnum(Name="StringTableId_StaffingLog_Leave", Value=0x2bf)]
        StringTableId_StaffingLog_Leave = 0x2bf,
        [ProtoEnum(Name="StringTableId_StaffingLog_JobFire", Value=0x2c0)]
        StringTableId_StaffingLog_JobFire = 0x2c0,
        [ProtoEnum(Name="StringTableId_NotExistSpaceStationInSolarSystem", Value=0x2c1)]
        StringTableId_NotExistSpaceStationInSolarSystem = 0x2c1,
        [ProtoEnum(Name="StringTableId_ConfirmDropItem", Value=0x2c2)]
        StringTableId_ConfirmDropItem = 0x2c2,
        [ProtoEnum(Name="StringTableId_AddShipToBuyOrSaleListWithoutDrivingLicense", Value=0x2c3)]
        StringTableId_AddShipToBuyOrSaleListWithoutDrivingLicense = 0x2c3,
        [ProtoEnum(Name="StringTableId_AddItemToBuyOrSaleList", Value=0x2c4)]
        StringTableId_AddItemToBuyOrSaleList = 0x2c4,
        [ProtoEnum(Name="StringTableId_RemoveItemFromBuyOrSaleList", Value=0x2c5)]
        StringTableId_RemoveItemFromBuyOrSaleList = 0x2c5,
        [ProtoEnum(Name="StringTableId_BuyItemSucceed", Value=710)]
        StringTableId_BuyItemSucceed = 710,
        [ProtoEnum(Name="StringTableId_SaleItemSucceed", Value=0x2c7)]
        StringTableId_SaleItemSucceed = 0x2c7,
        [ProtoEnum(Name="StringTableId_ConfirmBuyShipWithoutDrivingLicense", Value=0x2c8)]
        StringTableId_ConfirmBuyShipWithoutDrivingLicense = 0x2c8,
        [ProtoEnum(Name="StringTableId_SailReportBySomeone", Value=0x2c9)]
        StringTableId_SailReportBySomeone = 0x2c9,
        [ProtoEnum(Name="StringTableId_DeletedGuildMessage", Value=0x2ca)]
        StringTableId_DeletedGuildMessage = 0x2ca,
        [ProtoEnum(Name="StringTableId_GuildMessageCannotBeEmpty", Value=0x2cb)]
        StringTableId_GuildMessageCannotBeEmpty = 0x2cb,
        [ProtoEnum(Name="StringTableId_PlayerIsNotExist", Value=0x2cc)]
        StringTableId_PlayerIsNotExist = 0x2cc,
        [ProtoEnum(Name="StringTableId_LongLongAgo", Value=0x2cd)]
        StringTableId_LongLongAgo = 0x2cd,
        [ProtoEnum(Name="StringTableId_MessageSendTooOften", Value=0x2ce)]
        StringTableId_MessageSendTooOften = 0x2ce,
        [ProtoEnum(Name="StringTableId_PlayerIsInBanned", Value=0x2cf)]
        StringTableId_PlayerIsInBanned = 0x2cf,
        [ProtoEnum(Name="StringTableId_TransferLeaderMsgBoxTip", Value=720)]
        StringTableId_TransferLeaderMsgBoxTip = 720,
        [ProtoEnum(Name="StringTableId_TransferLeaderingTip", Value=0x2d1)]
        StringTableId_TransferLeaderingTip = 0x2d1,
        [ProtoEnum(Name="StringTableId_AppointJobMsgBoxTip", Value=0x2d2)]
        StringTableId_AppointJobMsgBoxTip = 0x2d2,
        [ProtoEnum(Name="StringTableId_AppointJob", Value=0x2d3)]
        StringTableId_AppointJob = 0x2d3,
        [ProtoEnum(Name="StringTableId_RemoveOffice", Value=0x2d4)]
        StringTableId_RemoveOffice = 0x2d4,
        [ProtoEnum(Name="StringTableId_GuildIconEditorContinueToExit", Value=0x2d5)]
        StringTableId_GuildIconEditorContinueToExit = 0x2d5,
        [ProtoEnum(Name="StringTableId_AtMostGuildJobTip", Value=0x2d6)]
        StringTableId_AtMostGuildJobTip = 0x2d6,
        [ProtoEnum(Name="StringTableId_AmmoNotEnoughWhenApplyShipTemplate", Value=0x2d7)]
        StringTableId_AmmoNotEnoughWhenApplyShipTemplate = 0x2d7,
        [ProtoEnum(Name="StringTableId_UnLockPreChipSchemeFrist", Value=0x2d8)]
        StringTableId_UnLockPreChipSchemeFrist = 0x2d8,
        [ProtoEnum(Name="StringTableId_ChipSchemeString1", Value=0x2d9)]
        StringTableId_ChipSchemeString1 = 0x2d9,
        [ProtoEnum(Name="StringTableId_ChipSchemeString2", Value=730)]
        StringTableId_ChipSchemeString2 = 730,
        [ProtoEnum(Name="StringTableId_NoPermission", Value=0x2db)]
        StringTableId_NoPermission = 0x2db,
        [ProtoEnum(Name="StringTableId_CancelTransferLeaderSucess", Value=0x2dc)]
        StringTableId_CancelTransferLeaderSucess = 0x2dc,
        [ProtoEnum(Name="StringTableId_UnableToEvacuateTheBattlefield", Value=0x2dd)]
        StringTableId_UnableToEvacuateTheBattlefield = 0x2dd,
        [ProtoEnum(Name="StringTableId_DownloadAnnouncementFailed", Value=0x2de)]
        StringTableId_DownloadAnnouncementFailed = 0x2de,
        [ProtoEnum(Name="StringTableId_PleaseInputNumber", Value=0x2df)]
        StringTableId_PleaseInputNumber = 0x2df,
        [ProtoEnum(Name="StringTableId_PleaseInputDropNumber", Value=0x2e0)]
        StringTableId_PleaseInputDropNumber = 0x2e0,
        [ProtoEnum(Name="StringTableId_InputGiftCDKey", Value=0x2e1)]
        StringTableId_InputGiftCDKey = 0x2e1,
        [ProtoEnum(Name="StringTableId_CheckYourEMail", Value=0x2e2)]
        StringTableId_CheckYourEMail = 0x2e2,
        [ProtoEnum(Name="StringTableId_NetWorkNotReachable", Value=0x2e3)]
        StringTableId_NetWorkNotReachable = 0x2e3,
        [ProtoEnum(Name="StringTableId_StayInSameStationWithTeammate", Value=740)]
        StringTableId_StayInSameStationWithTeammate = 740,
        [ProtoEnum(Name="StringTableId_NoReportRecently", Value=0x2e5)]
        StringTableId_NoReportRecently = 0x2e5,
        [ProtoEnum(Name="StringTableId_SkipCGWhileUsingEmulator", Value=0x2e6)]
        StringTableId_SkipCGWhileUsingEmulator = 0x2e6,
        [ProtoEnum(Name="StringTableId_ChipSlotAreaTop", Value=0x2e7)]
        StringTableId_ChipSlotAreaTop = 0x2e7,
        [ProtoEnum(Name="StringTableId_ChipSlotAreaBottom", Value=0x2e8)]
        StringTableId_ChipSlotAreaBottom = 0x2e8,
        [ProtoEnum(Name="StringTableId_ChipSlotAreaLeft", Value=0x2e9)]
        StringTableId_ChipSlotAreaLeft = 0x2e9,
        [ProtoEnum(Name="StringTableId_ChipSlotAreaRight", Value=0x2ea)]
        StringTableId_ChipSlotAreaRight = 0x2ea,
        [ProtoEnum(Name="StringTableId_PleaseJoinAGuildFirst", Value=0x2eb)]
        StringTableId_PleaseJoinAGuildFirst = 0x2eb,
        [ProtoEnum(Name="StringTableId_ManualScanProbe", Value=0x2ec)]
        StringTableId_ManualScanProbe = 0x2ec,
        [ProtoEnum(Name="StringTableId_DelegateScanProbe", Value=0x2ed)]
        StringTableId_DelegateScanProbe = 0x2ed,
        [ProtoEnum(Name="StringTableId_PVPScanProbe", Value=750)]
        StringTableId_PVPScanProbe = 750,
        [ProtoEnum(Name="StringTableId_StartTransferLeader", Value=0x2ef)]
        StringTableId_StartTransferLeader = 0x2ef,
        [ProtoEnum(Name="StringTableId_CancelLeaderTransfer", Value=0x2f0)]
        StringTableId_CancelLeaderTransfer = 0x2f0,
        [ProtoEnum(Name="StringTableId_NoAccessPermission", Value=0x2f1)]
        StringTableId_NoAccessPermission = 0x2f1,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_Product", Value=0x2f2)]
        StringTableId_GuildStoreItemListUI_Building_Product = 0x2f2,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_Component", Value=0x2f3)]
        StringTableId_GuildStoreItemListUI_Building_Component = 0x2f3,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Other_GuildMineral", Value=0x2f4)]
        StringTableId_GuildStoreItemListUI_Other_GuildMineral = 0x2f4,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Other_Fuel", Value=0x2f5)]
        StringTableId_GuildStoreItemListUI_Other_Fuel = 0x2f5,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building", Value=0x2f6)]
        StringTableId_GuildStoreItemListUI_Building = 0x2f6,
        [ProtoEnum(Name="StringTableId_GuildDonateSucceed", Value=0x2f7)]
        StringTableId_GuildDonateSucceed = 0x2f7,
        [ProtoEnum(Name="StringTableId_RemoveOfficeMsgBoxTip", Value=760)]
        StringTableId_RemoveOfficeMsgBoxTip = 760,
        [ProtoEnum(Name="StringTableId_GuildOccupySolarSystem", Value=0x2f9)]
        StringTableId_GuildOccupySolarSystem = 0x2f9,
        [ProtoEnum(Name="StringTableId_GuildTradeMoney", Value=0x2fa)]
        StringTableId_GuildTradeMoney = 0x2fa,
        [ProtoEnum(Name="StringTableId_GuildInformationPoint", Value=0x2fb)]
        StringTableId_GuildInformationPoint = 0x2fb,
        [ProtoEnum(Name="StringTableId_GuildTacticalPoint", Value=0x2fc)]
        StringTableId_GuildTacticalPoint = 0x2fc,
        [ProtoEnum(Name="StringTableId_GuildTradeMoneyDetail", Value=0x2fd)]
        StringTableId_GuildTradeMoneyDetail = 0x2fd,
        [ProtoEnum(Name="StringTableId_GuildInformationPointDetail", Value=0x2fe)]
        StringTableId_GuildInformationPointDetail = 0x2fe,
        [ProtoEnum(Name="StringTableId_GuildTacticalPointDetail", Value=0x2ff)]
        StringTableId_GuildTacticalPointDetail = 0x2ff,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenBlackMarket", Value=0x300)]
        StringTableId_NpcDialogOptionOpenBlackMarket = 0x300,
        [ProtoEnum(Name="StringTableId_GuildEditorAnnouncementTips", Value=0x301)]
        StringTableId_GuildEditorAnnouncementTips = 0x301,
        [ProtoEnum(Name="StringTableId_GuildProductionLineNameSuffix", Value=770)]
        StringTableId_GuildProductionLineNameSuffix = 770,
        [ProtoEnum(Name="StringTableId_GuildProductionLineDefaultName", Value=0x303)]
        StringTableId_GuildProductionLineDefaultName = 0x303,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsDeploying", Value=0x304)]
        StringTableId_GuildBuildingStatsDeploying = 0x304,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsUpgradeing", Value=0x305)]
        StringTableId_GuildBuildingStatsUpgradeing = 0x305,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsRecycling", Value=0x306)]
        StringTableId_GuildBuildingStatsRecycling = 0x306,
        [ProtoEnum(Name="StringTableId_GuildSolarSystemFlourish", Value=0x307)]
        StringTableId_GuildSolarSystemFlourish = 0x307,
        [ProtoEnum(Name="StringTableId_GuildSolarSystemFlourishLevel", Value=0x308)]
        StringTableId_GuildSolarSystemFlourishLevel = 0x308,
        [ProtoEnum(Name="StringTableId_ShipCustomConfigSelf", Value=0x309)]
        StringTableId_ShipCustomConfigSelf = 0x309,
        [ProtoEnum(Name="StringTableId_ShipCustomConfigGuild", Value=0x30a)]
        StringTableId_ShipCustomConfigGuild = 0x30a,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship", Value=0x30b)]
        StringTableId_GuildStoreItemListUI_Flagship = 0x30b,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_Product", Value=780)]
        StringTableId_GuildStoreItemListUI_Flagship_Product = 780,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_Component", Value=0x30d)]
        StringTableId_GuildStoreItemListUI_Flagship_Component = 0x30d,
        [ProtoEnum(Name="StringTableId_SwitchGuildSolarSystemError_NoOccpiedSystem", Value=0x30e)]
        StringTableId_SwitchGuildSolarSystemError_NoOccpiedSystem = 0x30e,
        [ProtoEnum(Name="StringTableId_CharactorSkillDescForDrive", Value=0x30f)]
        StringTableId_CharactorSkillDescForDrive = 0x30f,
        [ProtoEnum(Name="StringTableId_Unknown", Value=0x310)]
        StringTableId_Unknown = 0x310,
        [ProtoEnum(Name="StringTableId_NearBy", Value=0x311)]
        StringTableId_NearBy = 0x311,
        [ProtoEnum(Name="StringTableId_OffLine", Value=0x312)]
        StringTableId_OffLine = 0x312,
        [ProtoEnum(Name="StringTableId_SameSolarSystem", Value=0x313)]
        StringTableId_SameSolarSystem = 0x313,
        [ProtoEnum(Name="StringTableId_GuildFleetMemberPositionDesc", Value=0x314)]
        StringTableId_GuildFleetMemberPositionDesc = 0x314,
        [ProtoEnum(Name="StringTableId_GuildFleetMemberPositionDescOnlyForSelf", Value=0x315)]
        StringTableId_GuildFleetMemberPositionDescOnlyForSelf = 0x315,
        [ProtoEnum(Name="StringTableId_RemoveMemberFromGuildFleetDesc", Value=790)]
        StringTableId_RemoveMemberFromGuildFleetDesc = 790,
        [ProtoEnum(Name="StringTableId_RemoveMemberFromGuildFleetTitle", Value=0x317)]
        StringTableId_RemoveMemberFromGuildFleetTitle = 0x317,
        [ProtoEnum(Name="StringTableId_RetireFromGuildFleetDesc", Value=0x318)]
        StringTableId_RetireFromGuildFleetDesc = 0x318,
        [ProtoEnum(Name="StringTableId_RetireFromGuildFleetTitle", Value=0x319)]
        StringTableId_RetireFromGuildFleetTitle = 0x319,
        [ProtoEnum(Name="StringTableId_TransferPositionInGuildFleetDesc", Value=0x31a)]
        StringTableId_TransferPositionInGuildFleetDesc = 0x31a,
        [ProtoEnum(Name="StringTableId_TransferPositionInGuildFleetTitle", Value=0x31b)]
        StringTableId_TransferPositionInGuildFleetTitle = 0x31b,
        [ProtoEnum(Name="StringTableId_Substitute", Value=0x31c)]
        StringTableId_Substitute = 0x31c,
        [ProtoEnum(Name="StringTableId_GuildFleetDefaultName", Value=0x31d)]
        StringTableId_GuildFleetDefaultName = 0x31d,
        [ProtoEnum(Name="StringTableId_OperationTooOfen", Value=0x31e)]
        StringTableId_OperationTooOfen = 0x31e,
        [ProtoEnum(Name="StringTableId_SysNotice", Value=0x31f)]
        StringTableId_SysNotice = 0x31f,
        [ProtoEnum(Name="StringTableId_InGuildBattle", Value=800)]
        StringTableId_InGuildBattle = 800,
        [ProtoEnum(Name="StringTableId_SovereignSolarSystem", Value=0x321)]
        StringTableId_SovereignSolarSystem = 0x321,
        [ProtoEnum(Name="StringTableId_GalaxyFactory", Value=0x322)]
        StringTableId_GalaxyFactory = 0x322,
        [ProtoEnum(Name="StringTableId_GuildProduceSpeedUpTime", Value=0x323)]
        StringTableId_GuildProduceSpeedUpTime = 0x323,
        [ProtoEnum(Name="StringTableId_GuildLeave", Value=0x324)]
        StringTableId_GuildLeave = 0x324,
        [ProtoEnum(Name="StringTableId_GuildProduceLineUnLock", Value=0x325)]
        StringTableId_GuildProduceLineUnLock = 0x325,
        [ProtoEnum(Name="StringTableId_UnitShipString", Value=0x326)]
        StringTableId_UnitShipString = 0x326,
        [ProtoEnum(Name="StringTableId_BackToStationBeforeIssueGuildAction", Value=0x327)]
        StringTableId_BackToStationBeforeIssueGuildAction = 0x327,
        [ProtoEnum(Name="StringTableId_NoUnderwayGuildAaction", Value=0x328)]
        StringTableId_NoUnderwayGuildAaction = 0x328,
        [ProtoEnum(Name="StringTableId_TimeLimit", Value=0x329)]
        StringTableId_TimeLimit = 0x329,
        [ProtoEnum(Name="StringTableId_LeftTime", Value=810)]
        StringTableId_LeftTime = 810,
        [ProtoEnum(Name="StringTableId_OutOfDate", Value=0x32b)]
        StringTableId_OutOfDate = 0x32b,
        [ProtoEnum(Name="StringTableId_ForgetAction", Value=0x32c)]
        StringTableId_ForgetAction = 0x32c,
        [ProtoEnum(Name="StringTableId_GoNow", Value=0x32d)]
        StringTableId_GoNow = 0x32d,
        [ProtoEnum(Name="StringTableId_BattleWithNpcGuildHasNoDetailInfo", Value=0x32e)]
        StringTableId_BattleWithNpcGuildHasNoDetailInfo = 0x32e,
        [ProtoEnum(Name="StringTableId_GuildActionIssueWarning", Value=0x32f)]
        StringTableId_GuildActionIssueWarning = 0x32f,
        [ProtoEnum(Name="StringTableId_SovereignBattleSimpleInfoIsNull", Value=0x330)]
        StringTableId_SovereignBattleSimpleInfoIsNull = 0x330,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Summarize_title", Value=0x331)]
        StringTableId_SovereignBattle_Summarize_title = 0x331,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Summarize_content", Value=0x332)]
        StringTableId_SovereignBattle_Summarize_content = 0x332,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Prepare_title", Value=0x333)]
        StringTableId_SovereignBattle_Prepare_title = 0x333,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Prepare_content", Value=820)]
        StringTableId_SovereignBattle_Prepare_content = 820,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Fight_title", Value=0x335)]
        StringTableId_SovereignBattle_Fight_title = 0x335,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Fight_content", Value=0x336)]
        StringTableId_SovereignBattle_Fight_content = 0x336,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Truce_title", Value=0x337)]
        StringTableId_SovereignBattle_Truce_title = 0x337,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Truce_content", Value=0x338)]
        StringTableId_SovereignBattle_Truce_content = 0x338,
        [ProtoEnum(Name="StringTableId_SovereignBattle_DecisiveBattle_title", Value=0x339)]
        StringTableId_SovereignBattle_DecisiveBattle_title = 0x339,
        [ProtoEnum(Name="StringTableId_SovereignBattle_DecisiveBattle_content", Value=0x33a)]
        StringTableId_SovereignBattle_DecisiveBattle_content = 0x33a,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Declare_title", Value=0x33b)]
        StringTableId_SovereignBattle_Declare_title = 0x33b,
        [ProtoEnum(Name="StringTableId_SovereignBattle_Declare_content", Value=0x33c)]
        StringTableId_SovereignBattle_Declare_content = 0x33c,
        [ProtoEnum(Name="StringTableId_SovereignBattle_AvoigWar_title", Value=0x33d)]
        StringTableId_SovereignBattle_AvoigWar_title = 0x33d,
        [ProtoEnum(Name="StringTableId_SovereignBattle_AvoigWar_content", Value=830)]
        StringTableId_SovereignBattle_AvoigWar_content = 830,
        [ProtoEnum(Name="StringTableId_GuildActionCompleteTip", Value=0x33f)]
        StringTableId_GuildActionCompleteTip = 0x33f,
        [ProtoEnum(Name="StringTableId_GuildActionFailedTip", Value=0x340)]
        StringTableId_GuildActionFailedTip = 0x340,
        [ProtoEnum(Name="StringTableId_GuildCompensationIssueSucess", Value=0x341)]
        StringTableId_GuildCompensationIssueSucess = 0x341,
        [ProtoEnum(Name="StringTableId_GuildActionIssueSucess", Value=0x342)]
        StringTableId_GuildActionIssueSucess = 0x342,
        [ProtoEnum(Name="StringTableId_GuildActionForgetSucess", Value=0x343)]
        StringTableId_GuildActionForgetSucess = 0x343,
        [ProtoEnum(Name="StringTableId_GuildPurchaseLogTextCreate", Value=0x344)]
        StringTableId_GuildPurchaseLogTextCreate = 0x344,
        [ProtoEnum(Name="StringTableId_GuildPurchaseLogTextModify", Value=0x345)]
        StringTableId_GuildPurchaseLogTextModify = 0x345,
        [ProtoEnum(Name="StringTableId_GuildPurchaseLogTextMatch", Value=0x346)]
        StringTableId_GuildPurchaseLogTextMatch = 0x346,
        [ProtoEnum(Name="StringTableId_HaveNoGuildBuildingInSolarSystem", Value=0x347)]
        StringTableId_HaveNoGuildBuildingInSolarSystem = 0x347,
        [ProtoEnum(Name="StringTableId_GuildFleet_Join", Value=840)]
        StringTableId_GuildFleet_Join = 840,
        [ProtoEnum(Name="StringTableId_GuildFleet_SelfFleetDismissed", Value=0x349)]
        StringTableId_GuildFleet_SelfFleetDismissed = 0x349,
        [ProtoEnum(Name="StringTableId_GuildFleet_Leave", Value=0x34a)]
        StringTableId_GuildFleet_Leave = 0x34a,
        [ProtoEnum(Name="StringTableId_GuildFleet_BeKickedOut", Value=0x34b)]
        StringTableId_GuildFleet_BeKickedOut = 0x34b,
        [ProtoEnum(Name="StringTableId_GuildFleet_StartFollowFormation", Value=0x34c)]
        StringTableId_GuildFleet_StartFollowFormation = 0x34c,
        [ProtoEnum(Name="StringTableId_GuildFleet_LossSycnFormation", Value=0x34d)]
        StringTableId_GuildFleet_LossSycnFormation = 0x34d,
        [ProtoEnum(Name="StringTableId_GuildFleet_BeAppointed", Value=0x34e)]
        StringTableId_GuildFleet_BeAppointed = 0x34e,
        [ProtoEnum(Name="StringTableId_GuildFleet_BeRetired", Value=0x34f)]
        StringTableId_GuildFleet_BeRetired = 0x34f,
        [ProtoEnum(Name="StringTableId_GuildFleet_NoNeededPosition", Value=0x350)]
        StringTableId_GuildFleet_NoNeededPosition = 0x350,
        [ProtoEnum(Name="StringTableId_GuildNewProduceLine", Value=0x351)]
        StringTableId_GuildNewProduceLine = 0x351,
        [ProtoEnum(Name="StringTableId_GuildSovereignSolarSystemCount", Value=850)]
        StringTableId_GuildSovereignSolarSystemCount = 850,
        [ProtoEnum(Name="StringTableId_StarMapAddScanProbeItemNotEnough", Value=0x353)]
        StringTableId_StarMapAddScanProbeItemNotEnough = 0x353,
        [ProtoEnum(Name="StringTableId_StarMapAddScanProPanelTipString", Value=0x354)]
        StringTableId_StarMapAddScanProPanelTipString = 0x354,
        [ProtoEnum(Name="StringTableId_CreateFleetSensitiveWordErrorCode", Value=0x355)]
        StringTableId_CreateFleetSensitiveWordErrorCode = 0x355,
        [ProtoEnum(Name="StringTableId_UnmetNeedForGuildBuildingDeploy", Value=0x356)]
        StringTableId_UnmetNeedForGuildBuildingDeploy = 0x356,
        [ProtoEnum(Name="StringTableId_UnmetNeedForGuildBuildingUpgrade", Value=0x357)]
        StringTableId_UnmetNeedForGuildBuildingUpgrade = 0x357,
        [ProtoEnum(Name="StringTableId_GuildSovereignBattleStartTips", Value=0x358)]
        StringTableId_GuildSovereignBattleStartTips = 0x358,
        [ProtoEnum(Name="StringTableId_PlayerSovereigntyReward", Value=0x359)]
        StringTableId_PlayerSovereigntyReward = 0x359,
        [ProtoEnum(Name="StringTableId_PlayerSovereigntyRewardDesc", Value=0x35a)]
        StringTableId_PlayerSovereigntyRewardDesc = 0x35a,
        [ProtoEnum(Name="StringTableId_SolarSystemGuildBattleStatus_DisablePlayerSovereignty", Value=0x35b)]
        StringTableId_SolarSystemGuildBattleStatus_DisablePlayerSovereignty = 0x35b,
        [ProtoEnum(Name="StringTableId_SolarSystemGuildBattleStatus_EnablePlayerSovereignty", Value=860)]
        StringTableId_SolarSystemGuildBattleStatus_EnablePlayerSovereignty = 860,
        [ProtoEnum(Name="StringTableId_SolarSystemGuildBattleStatus_InBattleStatus", Value=0x35d)]
        StringTableId_SolarSystemGuildBattleStatus_InBattleStatus = 0x35d,
        [ProtoEnum(Name="StringTableId_SolarSystemGuildBattleStatus_WaitForBattle", Value=0x35e)]
        StringTableId_SolarSystemGuildBattleStatus_WaitForBattle = 0x35e,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsReinforcement", Value=0x35f)]
        StringTableId_GuildBuildingStatsReinforcement = 0x35f,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsBeAttacked", Value=0x360)]
        StringTableId_GuildBuildingStatsBeAttacked = 0x360,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsBeDisturb", Value=0x361)]
        StringTableId_GuildBuildingStatsBeDisturb = 0x361,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsOffline", Value=0x362)]
        StringTableId_GuildBuildingStatsOffline = 0x362,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsProduct", Value=0x36d)]
        StringTableId_GuildBuildingStatsProduct = 0x36d,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsGain", Value=0x36e)]
        StringTableId_GuildBuildingStatsGain = 0x36e,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsUpload", Value=0x36f)]
        StringTableId_GuildBuildingStatsUpload = 0x36f,
        [ProtoEnum(Name="StringTableId_ScanProbeAddItemMustUseInStation", Value=880)]
        StringTableId_ScanProbeAddItemMustUseInStation = 880,
        [ProtoEnum(Name="StringTableId_IssueCompensationMsg", Value=0x371)]
        StringTableId_IssueCompensationMsg = 0x371,
        [ProtoEnum(Name="StringTableId_RefuseGuildCompensationTip", Value=0x372)]
        StringTableId_RefuseGuildCompensationTip = 0x372,
        [ProtoEnum(Name="StringTableId_MinuteString", Value=0x373)]
        StringTableId_MinuteString = 0x373,
        [ProtoEnum(Name="StringTableId_MustJoinGuildBeforeIssueCompensation", Value=0x374)]
        StringTableId_MustJoinGuildBeforeIssueCompensation = 0x374,
        [ProtoEnum(Name="StringTableId_CantReissueCompensation", Value=0x375)]
        StringTableId_CantReissueCompensation = 0x375,
        [ProtoEnum(Name="StringTableId_GuildFleetMemberJoinTitle", Value=0x376)]
        StringTableId_GuildFleetMemberJoinTitle = 0x376,
        [ProtoEnum(Name="StringTableId_GuildFleetMemberJoinContent", Value=0x377)]
        StringTableId_GuildFleetMemberJoinContent = 0x377,
        [ProtoEnum(Name="StringTableId_GuildFleetLeaveTitle", Value=0x378)]
        StringTableId_GuildFleetLeaveTitle = 0x378,
        [ProtoEnum(Name="StringTableId_GuildFleetLeaveContent_Normal", Value=0x379)]
        StringTableId_GuildFleetLeaveContent_Normal = 0x379,
        [ProtoEnum(Name="StringTableId_GuildFleetLeaveContent_TeamLeader", Value=890)]
        StringTableId_GuildFleetLeaveContent_TeamLeader = 890,
        [ProtoEnum(Name="StringTableId_GuildFleetDissolveTitle", Value=0x37b)]
        StringTableId_GuildFleetDissolveTitle = 0x37b,
        [ProtoEnum(Name="StringTableId_GuildFleetDissolveContent", Value=0x37c)]
        StringTableId_GuildFleetDissolveContent = 0x37c,
        [ProtoEnum(Name="StringTableId_GuildComepsationSucess", Value=0x37d)]
        StringTableId_GuildComepsationSucess = 0x37d,
        [ProtoEnum(Name="StringTableId_PlayerCompensationSucess", Value=0x37e)]
        StringTableId_PlayerCompensationSucess = 0x37e,
        [ProtoEnum(Name="StringTableId_TargetGalaxiesCannotBeReached", Value=0x37f)]
        StringTableId_TargetGalaxiesCannotBeReached = 0x37f,
        [ProtoEnum(Name="StringTableId_GuildCompensation", Value=0x380)]
        StringTableId_GuildCompensation = 0x380,
        [ProtoEnum(Name="StringTableId_LeaveFromGuildFleetDesc", Value=0x381)]
        StringTableId_LeaveFromGuildFleetDesc = 0x381,
        [ProtoEnum(Name="StringTableId_LeaveFromGuildFleetTitle", Value=0x382)]
        StringTableId_LeaveFromGuildFleetTitle = 0x382,
        [ProtoEnum(Name="StringTableId_TransferPositionToOffLinePlayerInGuildFleetDesc", Value=0x383)]
        StringTableId_TransferPositionToOffLinePlayerInGuildFleetDesc = 0x383,
        [ProtoEnum(Name="StringTableId_ControllShieldStabiliser", Value=900)]
        StringTableId_ControllShieldStabiliser = 900,
        [ProtoEnum(Name="StringTableId_StartBuildingBattleConfirmNotfiy", Value=0x385)]
        StringTableId_StartBuildingBattleConfirmNotfiy = 0x385,
        [ProtoEnum(Name="StringTableId_StartBuildingBattleTimeDelay", Value=0x386)]
        StringTableId_StartBuildingBattleTimeDelay = 0x386,
        [ProtoEnum(Name="StringTableId_YouAlreadyHaveTheCurrentGalaxySovereignty", Value=0x387)]
        StringTableId_YouAlreadyHaveTheCurrentGalaxySovereignty = 0x387,
        [ProtoEnum(Name="StringTableId_ThisFeatureIsNotYetOpen", Value=0x388)]
        StringTableId_ThisFeatureIsNotYetOpen = 0x388,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsDeploy", Value=0x389)]
        StringTableId_GuildBuildingStatsDeploy = 0x389,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsUpgrade", Value=0x38a)]
        StringTableId_GuildBuildingStatsUpgrade = 0x38a,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsRecycl", Value=0x38b)]
        StringTableId_GuildBuildingStatsRecycl = 0x38b,
        [ProtoEnum(Name="StringTableId_GuildBuildingFlourishRequest", Value=0x38c)]
        StringTableId_GuildBuildingFlourishRequest = 0x38c,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsUnDetected", Value=0x38d)]
        StringTableId_GuildBuildingStatsUnDetected = 0x38d,
        [ProtoEnum(Name="StringTableId_WillClose", Value=910)]
        StringTableId_WillClose = 910,
        [ProtoEnum(Name="StringTableId_GuildJointSecurityServiceDepartment", Value=0x38f)]
        StringTableId_GuildJointSecurityServiceDepartment = 0x38f,
        [ProtoEnum(Name="StringTableId_GuildCurrencyIsInsufficient", Value=0x390)]
        StringTableId_GuildCurrencyIsInsufficient = 0x390,
        [ProtoEnum(Name="StringTableId_TheGalaxyIsNotAllowedToOccupy", Value=0x391)]
        StringTableId_TheGalaxyIsNotAllowedToOccupy = 0x391,
        [ProtoEnum(Name="StringTableId_NotFindGuildSentrySingal", Value=0x392)]
        StringTableId_NotFindGuildSentrySingal = 0x392,
        [ProtoEnum(Name="StringTableId_CompensationGoodsNotEnough", Value=0x393)]
        StringTableId_CompensationGoodsNotEnough = 0x393,
        [ProtoEnum(Name="StringTableId_CantDonatToSelf", Value=0x394)]
        StringTableId_CantDonatToSelf = 0x394,
        [ProtoEnum(Name="StringTableId_BattleReinforcementEndTimeSet", Value=0x395)]
        StringTableId_BattleReinforcementEndTimeSet = 0x395,
        [ProtoEnum(Name="StringTableId_MiningBalanceTimeSet", Value=0x396)]
        StringTableId_MiningBalanceTimeSet = 0x396,
        [ProtoEnum(Name="StringTableId_SuccessfullyLaunchedADeclarationOfWar", Value=0x397)]
        StringTableId_SuccessfullyLaunchedADeclarationOfWar = 0x397,
        [ProtoEnum(Name="StringTableId_GuildFleet_OpenFormation", Value=920)]
        StringTableId_GuildFleet_OpenFormation = 920,
        [ProtoEnum(Name="StringTableId_GuildFleet_CloseSycnFormation", Value=0x399)]
        StringTableId_GuildFleet_CloseSycnFormation = 0x399,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_EnergyCenter", Value=0x39a)]
        StringTableId_GuildStoreItemListUI_Building_EnergyCenter = 0x39a,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_MiningBuilding", Value=0x39b)]
        StringTableId_GuildStoreItemListUI_Building_MiningBuilding = 0x39b,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_ProductionFactory", Value=0x39c)]
        StringTableId_GuildStoreItemListUI_Building_ProductionFactory = 0x39c,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_ShipDock", Value=0x39d)]
        StringTableId_GuildStoreItemListUI_Building_ShipDock = 0x39d,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_GalaxyAlertTower", Value=0x39e)]
        StringTableId_GuildStoreItemListUI_Building_GalaxyAlertTower = 0x39e,
        [ProtoEnum(Name="StringTableId_GuildPurchaseConfirmed", Value=0x39f)]
        StringTableId_GuildPurchaseConfirmed = 0x39f,
        [ProtoEnum(Name="StringTableId_GuildPurchaseModified", Value=0x3a0)]
        StringTableId_GuildPurchaseModified = 0x3a0,
        [ProtoEnum(Name="StringTableId_GuildPurchaseCanceled", Value=0x3a1)]
        StringTableId_GuildPurchaseCanceled = 0x3a1,
        [ProtoEnum(Name="StringTableId_GuildPurchaseWillCancel", Value=930)]
        StringTableId_GuildPurchaseWillCancel = 930,
        [ProtoEnum(Name="StringTableId_SuccessfullyLaunchedADeclarationOfIntrusion", Value=0x3a3)]
        StringTableId_SuccessfullyLaunchedADeclarationOfIntrusion = 0x3a3,
        [ProtoEnum(Name="StringTableId_GuildMoneyLessThanToPay", Value=0x3a4)]
        StringTableId_GuildMoneyLessThanToPay = 0x3a4,
        [ProtoEnum(Name="StringTableId_SaleCountTooMany", Value=0x3a5)]
        StringTableId_SaleCountTooMany = 0x3a5,
        [ProtoEnum(Name="StringTableId_PlayerDoseNotHaveGuildWarPermission", Value=0x3a6)]
        StringTableId_PlayerDoseNotHaveGuildWarPermission = 0x3a6,
        [ProtoEnum(Name="StringTableId_MustBeInTheSpaceStationToCreateGuild", Value=0x3a7)]
        StringTableId_MustBeInTheSpaceStationToCreateGuild = 0x3a7,
        [ProtoEnum(Name="StringTableId_GuildProductionRestTimeLessThanSpeedUpTimeDesc", Value=0x3a8)]
        StringTableId_GuildProductionRestTimeLessThanSpeedUpTimeDesc = 0x3a8,
        [ProtoEnum(Name="StringTableId_StabilizerTowerDisturber", Value=0x3a9)]
        StringTableId_StabilizerTowerDisturber = 0x3a9,
        [ProtoEnum(Name="StringTableId_BuildingShieldDisturber", Value=0x3aa)]
        StringTableId_BuildingShieldDisturber = 0x3aa,
        [ProtoEnum(Name="StringTableId_GuildProductionLowSpeedUpDesc", Value=0x3ab)]
        StringTableId_GuildProductionLowSpeedUpDesc = 0x3ab,
        [ProtoEnum(Name="StringTableId_GuildProductionMidSpeedUpDesc", Value=940)]
        StringTableId_GuildProductionMidSpeedUpDesc = 940,
        [ProtoEnum(Name="StringTableId_GuildProductionHighSpeedUpDesc", Value=0x3ad)]
        StringTableId_GuildProductionHighSpeedUpDesc = 0x3ad,
        [ProtoEnum(Name="StringTableId_InStation", Value=0x3ae)]
        StringTableId_InStation = 0x3ae,
        [ProtoEnum(Name="StringTableId_KeepFormationWithGuildFleet", Value=0x3af)]
        StringTableId_KeepFormationWithGuildFleet = 0x3af,
        [ProtoEnum(Name="StringTableId_GuildProduceSpeedUpSuccess", Value=0x3b0)]
        StringTableId_GuildProduceSpeedUpSuccess = 0x3b0,
        [ProtoEnum(Name="StringTableId_LoginConnectServerError", Value=0x3b1)]
        StringTableId_LoginConnectServerError = 0x3b1,
        [ProtoEnum(Name="StringTableId_LoginAgentLoginDataError", Value=0x3b2)]
        StringTableId_LoginAgentLoginDataError = 0x3b2,
        [ProtoEnum(Name="StringTableId_LoginDownloadAnnouncementError", Value=0x3b3)]
        StringTableId_LoginDownloadAnnouncementError = 0x3b3,
        [ProtoEnum(Name="StringTableId_LoginEnterWorldError", Value=0x3b4)]
        StringTableId_LoginEnterWorldError = 0x3b4,
        [ProtoEnum(Name="StringTableId_UseLeaveGuildFuncWhenYouWantKickoutYourself", Value=0x3b5)]
        StringTableId_UseLeaveGuildFuncWhenYouWantKickoutYourself = 0x3b5,
        [ProtoEnum(Name="StringTableId_NoPermissionIssueGuildAction", Value=950)]
        StringTableId_NoPermissionIssueGuildAction = 950,
        [ProtoEnum(Name="StringTableId_DeleteReaded", Value=0x3b7)]
        StringTableId_DeleteReaded = 0x3b7,
        [ProtoEnum(Name="StringTableId_Person", Value=0x3b8)]
        StringTableId_Person = 0x3b8,
        [ProtoEnum(Name="StringTableId_YouAreNotInCurrGuild", Value=0x3b9)]
        StringTableId_YouAreNotInCurrGuild = 0x3b9,
        [ProtoEnum(Name="StringTableId_CurrShipCantStrike", Value=0x3ba)]
        StringTableId_CurrShipCantStrike = 0x3ba,
        [ProtoEnum(Name="StringTableId_GuildSentrySignalInSpace", Value=0x3bb)]
        StringTableId_GuildSentrySignalInSpace = 0x3bb,
        [ProtoEnum(Name="StringTableId_LoginErrorAccountAlreadyOnline", Value=0x3bc)]
        StringTableId_LoginErrorAccountAlreadyOnline = 0x3bc,
        [ProtoEnum(Name="StringTableId_InfectNotFoundNearby", Value=0x3bd)]
        StringTableId_InfectNotFoundNearby = 0x3bd,
        [ProtoEnum(Name="StringTableId_WormholeNotFoundNearby", Value=0x3be)]
        StringTableId_WormholeNotFoundNearby = 0x3be,
        [ProtoEnum(Name="StringTableId_GuildWarReportNpcGuildInfoCannotGet", Value=0x3bf)]
        StringTableId_GuildWarReportNpcGuildInfoCannotGet = 0x3bf,
        [ProtoEnum(Name="StringTableId_GuildProductionDestoryed", Value=960)]
        StringTableId_GuildProductionDestoryed = 960,
        [ProtoEnum(Name="StringTableId_ChatLink_WarReport", Value=0x3c1)]
        StringTableId_ChatLink_WarReport = 0x3c1,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_WorkingOnTech", Value=0x3c2)]
        StringTableId_HiredCaptainRetireFail_WorkingOnTech = 0x3c2,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_WorkingOnProduction", Value=0x3c3)]
        StringTableId_HiredCaptainRetireFail_WorkingOnProduction = 0x3c3,
        [ProtoEnum(Name="StringTableId_HiredCaptainRetireFail_InStationWorking", Value=0x3c4)]
        StringTableId_HiredCaptainRetireFail_InStationWorking = 0x3c4,
        [ProtoEnum(Name="StringTableId_ChooseOrderFirst", Value=0x3c5)]
        StringTableId_ChooseOrderFirst = 0x3c5,
        [ProtoEnum(Name="StringTableId_GalaxyAnnoPrefix", Value=0x3c6)]
        StringTableId_GalaxyAnnoPrefix = 0x3c6,
        [ProtoEnum(Name="StringTableId_HangarShipNotSuitableToStrike", Value=0x3c7)]
        StringTableId_HangarShipNotSuitableToStrike = 0x3c7,
        [ProtoEnum(Name="StringTableId_ModifiedTooOftenTryAgainLater", Value=0x3c8)]
        StringTableId_ModifiedTooOftenTryAgainLater = 0x3c8,
        [ProtoEnum(Name="StringTableId_GetNeededDataFailed", Value=0x3c9)]
        StringTableId_GetNeededDataFailed = 0x3c9,
        [ProtoEnum(Name="StringTableId_InputStringValidError", Value=970)]
        StringTableId_InputStringValidError = 970,
        [ProtoEnum(Name="StringTableId_InsufficientSheetMetal", Value=0x3cb)]
        StringTableId_InsufficientSheetMetal = 0x3cb,
        [ProtoEnum(Name="StringTableId_InterruptJumping", Value=0x3cc)]
        StringTableId_InterruptJumping = 0x3cc,
        [ProtoEnum(Name="StringTableId_Chat_SolarSystem_Audio", Value=0x3cd)]
        StringTableId_Chat_SolarSystem_Audio = 0x3cd,
        [ProtoEnum(Name="StringTableId_Chat_StarField_Audio", Value=0x3ce)]
        StringTableId_Chat_StarField_Audio = 0x3ce,
        [ProtoEnum(Name="StringTableId_Chat_Guild_Audio", Value=0x3cf)]
        StringTableId_Chat_Guild_Audio = 0x3cf,
        [ProtoEnum(Name="StringTableId_Chat_Team_Audio", Value=0x3d0)]
        StringTableId_Chat_Team_Audio = 0x3d0,
        [ProtoEnum(Name="StringTableId_DelegateMissionCaptainSortOrder_MinrealFrist", Value=0x3d1)]
        StringTableId_DelegateMissionCaptainSortOrder_MinrealFrist = 0x3d1,
        [ProtoEnum(Name="StringTableId_DelegateMissionCaptainSortOrder_FightFrist", Value=0x3d2)]
        StringTableId_DelegateMissionCaptainSortOrder_FightFrist = 0x3d2,
        [ProtoEnum(Name="StringTableId_GuildSearchType_GuildName", Value=0x3d3)]
        StringTableId_GuildSearchType_GuildName = 0x3d3,
        [ProtoEnum(Name="StringTableId_GuildSearchType_GuildCode", Value=980)]
        StringTableId_GuildSearchType_GuildCode = 980,
        [ProtoEnum(Name="StringTableId_EnterWorldErrorSolarSystemFull", Value=0x3d5)]
        StringTableId_EnterWorldErrorSolarSystemFull = 0x3d5,
        [ProtoEnum(Name="StringTableId_SlightPauseSign", Value=0x3d6)]
        StringTableId_SlightPauseSign = 0x3d6,
        [ProtoEnum(Name="StringTableId_GuildFleetNameEmpty", Value=0x3d7)]
        StringTableId_GuildFleetNameEmpty = 0x3d7,
        [ProtoEnum(Name="StringTableId_GuildFleetNameTooLong", Value=0x3d8)]
        StringTableId_GuildFleetNameTooLong = 0x3d8,
        [ProtoEnum(Name="StringTableId_GuildFleetNameIllegal", Value=0x3d9)]
        StringTableId_GuildFleetNameIllegal = 0x3d9,
        [ProtoEnum(Name="StringTableId_BackToStationAfterLeaveWormhole", Value=0x3da)]
        StringTableId_BackToStationAfterLeaveWormhole = 0x3da,
        [ProtoEnum(Name="StringTableId_WillBeFightByPoliceNotification", Value=0x3db)]
        StringTableId_WillBeFightByPoliceNotification = 0x3db,
        [ProtoEnum(Name="StringTableId_UseProbeConfirm_LowQuestReward", Value=0x3dc)]
        StringTableId_UseProbeConfirm_LowQuestReward = 0x3dc,
        [ProtoEnum(Name="StringTableId_UseProbeConfirm_SolarSystemPoolCount", Value=0x3dd)]
        StringTableId_UseProbeConfirm_SolarSystemPoolCount = 0x3dd,
        [ProtoEnum(Name="StringTableId_SearchMapContextIsNull", Value=990)]
        StringTableId_SearchMapContextIsNull = 990,
        [ProtoEnum(Name="StringTableId_SearchMapResultIsNull", Value=0x3df)]
        StringTableId_SearchMapResultIsNull = 0x3df,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimization_EmptyWeaponSlot", Value=0x3e0)]
        StringTableId_ShipAssemblyOptimization_EmptyWeaponSlot = 0x3e0,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimization_EmptyNoneWeaponSlot", Value=0x3e1)]
        StringTableId_ShipAssemblyOptimization_EmptyNoneWeaponSlot = 0x3e1,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimization_LowPowerUtilizationRate", Value=0x3e2)]
        StringTableId_ShipAssemblyOptimization_LowPowerUtilizationRate = 0x3e2,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimizationTitle_UnreasonableWeaponSize", Value=0x3e3)]
        StringTableId_ShipAssemblyOptimizationTitle_UnreasonableWeaponSize = 0x3e3,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimizationTitle_UnreasonableEquipSize", Value=0x3e4)]
        StringTableId_ShipAssemblyOptimizationTitle_UnreasonableEquipSize = 0x3e4,
        [ProtoEnum(Name="StringTableId_ShipAssemblyOptimizationTitle_InsufficientAssembly", Value=0x3e5)]
        StringTableId_ShipAssemblyOptimizationTitle_InsufficientAssembly = 0x3e5,
        [ProtoEnum(Name="StringTableId_SetPersonalDiplomacyNeutral", Value=0x3e6)]
        StringTableId_SetPersonalDiplomacyNeutral = 0x3e6,
        [ProtoEnum(Name="StringTableId_SetPersonalDiplomacyFriend", Value=0x3e7)]
        StringTableId_SetPersonalDiplomacyFriend = 0x3e7,
        [ProtoEnum(Name="StringTableId_SetPersonalDiplomacyEnemy", Value=0x3e8)]
        StringTableId_SetPersonalDiplomacyEnemy = 0x3e8,
        [ProtoEnum(Name="StringTableId_OnlyGuildLeaderCanCreateAlliance", Value=0x3e9)]
        StringTableId_OnlyGuildLeaderCanCreateAlliance = 0x3e9,
        [ProtoEnum(Name="StringTableId_OnlyInStationCanCreateAlliance", Value=0x3ea)]
        StringTableId_OnlyInStationCanCreateAlliance = 0x3ea,
        [ProtoEnum(Name="StringTableId_DuplicateAllianceName", Value=0x3eb)]
        StringTableId_DuplicateAllianceName = 0x3eb,
        [ProtoEnum(Name="StringTableId_IllegalAllianceName", Value=0x3ec)]
        StringTableId_IllegalAllianceName = 0x3ec,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_Trade_SaleLegal", Value=0x3ed)]
        StringTableId_NpcShop_Title_Trade_SaleLegal = 0x3ed,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_Trade_SaleIllegal", Value=0x3ee)]
        StringTableId_NpcShop_Title_Trade_SaleIllegal = 0x3ee,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_Trade_Buy", Value=0x3ef)]
        StringTableId_NpcShop_Title_Trade_Buy = 0x3ef,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_Trade_Exchange", Value=0x3f0)]
        StringTableId_NpcShop_Title_Trade_Exchange = 0x3f0,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_Faction", Value=0x3f1)]
        StringTableId_NpcShop_Title_Faction = 0x3f1,
        [ProtoEnum(Name="StringTableId_SpecialProduct", Value=0x3f2)]
        StringTableId_SpecialProduct = 0x3f2,
        [ProtoEnum(Name="StringTableId_Contraband", Value=0x3f3)]
        StringTableId_Contraband = 0x3f3,
        [ProtoEnum(Name="StringTableId_GuildInfoRecentMessageTip_NoneGuildMember", Value=0x3f4)]
        StringTableId_GuildInfoRecentMessageTip_NoneGuildMember = 0x3f4,
        [ProtoEnum(Name="StringTableId_SearchAlliance", Value=0x3f5)]
        StringTableId_SearchAlliance = 0x3f5,
        [ProtoEnum(Name="StringTableId_CreateAlliance", Value=0x3f6)]
        StringTableId_CreateAlliance = 0x3f6,
        [ProtoEnum(Name="StringTableId_AlreadyHasAlliance", Value=0x3f7)]
        StringTableId_AlreadyHasAlliance = 0x3f7,
        [ProtoEnum(Name="StringTableId_NoAlliance", Value=0x3f8)]
        StringTableId_NoAlliance = 0x3f8,
        [ProtoEnum(Name="StringTableId_JoinOrCreateAlliance", Value=0x3f9)]
        StringTableId_JoinOrCreateAlliance = 0x3f9,
        [ProtoEnum(Name="StringTableId_Distance", Value=0x3fa)]
        StringTableId_Distance = 0x3fa,
        [ProtoEnum(Name="StringTableId_Price", Value=0x3fb)]
        StringTableId_Price = 0x3fb,
        [ProtoEnum(Name="StringTableId_DeltaPrice", Value=0x3fc)]
        StringTableId_DeltaPrice = 0x3fc,
        [ProtoEnum(Name="StringTableId_CurrencyNotEnough", Value=0x3fd)]
        StringTableId_CurrencyNotEnough = 0x3fd,
        [ProtoEnum(Name="StringTableId_NoSellContrabandTips", Value=0x3fe)]
        StringTableId_NoSellContrabandTips = 0x3fe,
        [ProtoEnum(Name="StringTableId_AllianceMember", Value=0x3ff)]
        StringTableId_AllianceMember = 0x3ff,
        [ProtoEnum(Name="StringTableId_GuildInvite", Value=0x400)]
        StringTableId_GuildInvite = 0x400,
        [ProtoEnum(Name="StringTableId_SendInvitation", Value=0x401)]
        StringTableId_SendInvitation = 0x401,
        [ProtoEnum(Name="StringTableId_LeadGuild", Value=0x402)]
        StringTableId_LeadGuild = 0x402,
        [ProtoEnum(Name="StringTableId_TransLeader", Value=0x403)]
        StringTableId_TransLeader = 0x403,
        [ProtoEnum(Name="StringTableId_TerminateAlly", Value=0x404)]
        StringTableId_TerminateAlly = 0x404,
        [ProtoEnum(Name="StringTableId_Abort", Value=0x405)]
        StringTableId_Abort = 0x405,
        [ProtoEnum(Name="StringTableId_HasNotLoadTradeItemTip", Value=0x406)]
        StringTableId_HasNotLoadTradeItemTip = 0x406,
        [ProtoEnum(Name="StringTableId_TradeItemLoadNumIsZero", Value=0x407)]
        StringTableId_TradeItemLoadNumIsZero = 0x407,
        [ProtoEnum(Name="StringTableId_SelectedShipIsDetoryed", Value=0x408)]
        StringTableId_SelectedShipIsDetoryed = 0x408,
        [ProtoEnum(Name="StringTableId_SelectedStrikeShipIsDetoryed", Value=0x409)]
        StringTableId_SelectedStrikeShipIsDetoryed = 0x409,
        [ProtoEnum(Name="StringTableId_HangarShipSalvageSucceed", Value=0x40a)]
        StringTableId_HangarShipSalvageSucceed = 0x40a,
        [ProtoEnum(Name="StringTableId_AlreadyShowKillRecordDetoryedShipInHangar", Value=0x40b)]
        StringTableId_AlreadyShowKillRecordDetoryedShipInHangar = 0x40b,
        [ProtoEnum(Name="StringTableId_AbandonSalvageDetoryedShipConfirm", Value=0x40c)]
        StringTableId_AbandonSalvageDetoryedShipConfirm = 0x40c,
        [ProtoEnum(Name="StringTableId_AlreadyAbandonSalvageDetoryedShip", Value=0x40d)]
        StringTableId_AlreadyAbandonSalvageDetoryedShip = 0x40d,
        [ProtoEnum(Name="StringTableId_CannotViewMemberForNotInAMember", Value=0x40e)]
        StringTableId_CannotViewMemberForNotInAMember = 0x40e,
        [ProtoEnum(Name="StringTableId_AlreadyInYourAlliance", Value=0x40f)]
        StringTableId_AlreadyInYourAlliance = 0x40f,
        [ProtoEnum(Name="StringTableId_InvitationHasSent", Value=0x410)]
        StringTableId_InvitationHasSent = 0x410,
        [ProtoEnum(Name="StringTableId_AllianceCreated", Value=0x411)]
        StringTableId_AllianceCreated = 0x411,
        [ProtoEnum(Name="StringTableId_AllianceJoined", Value=0x412)]
        StringTableId_AllianceJoined = 0x412,
        [ProtoEnum(Name="StringTableId_GuildTradePurchaseOrderTransportStartLog", Value=0x413)]
        StringTableId_GuildTradePurchaseOrderTransportStartLog = 0x413,
        [ProtoEnum(Name="StringTableId_GuildTradePurchaseOrderTransportSucceedLog", Value=0x414)]
        StringTableId_GuildTradePurchaseOrderTransportSucceedLog = 0x414,
        [ProtoEnum(Name="StringTableId_GuildTradePurchaseOrderTransportFailLog", Value=0x415)]
        StringTableId_GuildTradePurchaseOrderTransportFailLog = 0x415,
        [ProtoEnum(Name="StringTableId_AllianceInfoEdited", Value=0x416)]
        StringTableId_AllianceInfoEdited = 0x416,
        [ProtoEnum(Name="StringTableId_AllianceMailSent", Value=0x417)]
        StringTableId_AllianceMailSent = 0x417,
        [ProtoEnum(Name="StringTableId_AllianceQuited", Value=0x418)]
        StringTableId_AllianceQuited = 0x418,
        [ProtoEnum(Name="StringTableId_AllianceDoesntExist", Value=0x419)]
        StringTableId_AllianceDoesntExist = 0x419,
        [ProtoEnum(Name="StringTableId_NoRecord", Value=0x41a)]
        StringTableId_NoRecord = 0x41a,
        [ProtoEnum(Name="StringTableId_DestoryShipDonnotHaveKillRecord", Value=0x41b)]
        StringTableId_DestoryShipDonnotHaveKillRecord = 0x41b,
        [ProtoEnum(Name="StringTableId_ConfirmLeaderTransfer", Value=0x41c)]
        StringTableId_ConfirmLeaderTransfer = 0x41c,
        [ProtoEnum(Name="StringTableId_ConfirmRemoveFromAlliance", Value=0x41d)]
        StringTableId_ConfirmRemoveFromAlliance = 0x41d,
        [ProtoEnum(Name="StringTableId_ConfirmQuitAlliance", Value=0x41e)]
        StringTableId_ConfirmQuitAlliance = 0x41e,
        [ProtoEnum(Name="StringTableId_ConfirmDismissAlliance", Value=0x41f)]
        StringTableId_ConfirmDismissAlliance = 0x41f,
        [ProtoEnum(Name="StringTableId_InviteToYourAlliance", Value=0x420)]
        StringTableId_InviteToYourAlliance = 0x420,
        [ProtoEnum(Name="StringTableId_JoinAlliance", Value=0x421)]
        StringTableId_JoinAlliance = 0x421,
        [ProtoEnum(Name="StringTableId_LessAllianceOperationPermission", Value=0x422)]
        StringTableId_LessAllianceOperationPermission = 0x422,
        [ProtoEnum(Name="StringTableId_MustTransferLeaderToOther", Value=0x423)]
        StringTableId_MustTransferLeaderToOther = 0x423,
        [ProtoEnum(Name="StringTableId_GuildBenefits_TimeLeftOnReset", Value=0x424)]
        StringTableId_GuildBenefits_TimeLeftOnReset = 0x424,
        [ProtoEnum(Name="StringTableId_GuildBenefits_NotReachingRequirementOnObtainingRewards", Value=0x425)]
        StringTableId_GuildBenefits_NotReachingRequirementOnObtainingRewards = 0x425,
        [ProtoEnum(Name="StringTableId_GuildBenefits_RewardsOnlyObtainingOncePerWeek", Value=0x426)]
        StringTableId_GuildBenefits_RewardsOnlyObtainingOncePerWeek = 0x426,
        [ProtoEnum(Name="StringTableId_GuildBenefits_RewardsOnlyObtainingOncePerDay", Value=0x427)]
        StringTableId_GuildBenefits_RewardsOnlyObtainingOncePerDay = 0x427,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarSlotUnlockCondition", Value=0x428)]
        StringTableId_GuildFlagShipHangarSlotUnlockCondition = 0x428,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarT1Slot", Value=0x429)]
        StringTableId_GuildFlagShipHangarT1Slot = 0x429,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarT2Slot", Value=0x42a)]
        StringTableId_GuildFlagShipHangarT2Slot = 0x42a,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarTitle", Value=0x42b)]
        StringTableId_GuildFlagShipHangarTitle = 0x42b,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarListIsEmpty", Value=0x42c)]
        StringTableId_GuildFlagShipHangarListIsEmpty = 0x42c,
        [ProtoEnum(Name="StringTableId_CannotRegisterCaptainWhenGuildFlagShipIsUnpacking", Value=0x42d)]
        StringTableId_CannotRegisterCaptainWhenGuildFlagShipIsUnpacking = 0x42d,
        [ProtoEnum(Name="StringTableId_CannotPackWhenGuildFlagShipHasCaptain", Value=0x42e)]
        StringTableId_CannotPackWhenGuildFlagShipHasCaptain = 0x42e,
        [ProtoEnum(Name="StringTableId_OnlyHasOneFlagShipHangarInGuild", Value=0x42f)]
        StringTableId_OnlyHasOneFlagShipHangarInGuild = 0x42f,
        [ProtoEnum(Name="StringTableId_GuildTradePortName", Value=0x430)]
        StringTableId_GuildTradePortName = 0x430,
        [ProtoEnum(Name="StringTableId_GuildBenefits_SolarSystemNumber", Value=0x431)]
        StringTableId_GuildBenefits_SolarSystemNumber = 0x431,
        [ProtoEnum(Name="StringTableId_Number", Value=0x432)]
        StringTableId_Number = 0x432,
        [ProtoEnum(Name="StringTableId_GuildMineral", Value=0x433)]
        StringTableId_GuildMineral = 0x433,
        [ProtoEnum(Name="StringTableId_GuildFlagShipNeedLockFrist", Value=0x434)]
        StringTableId_GuildFlagShipNeedLockFrist = 0x434,
        [ProtoEnum(Name="StringTableId_EffectiveTimeLeft", Value=0x435)]
        StringTableId_EffectiveTimeLeft = 0x435,
        [ProtoEnum(Name="StringTableId_NpcShop_Title_GuildGala_Exchange", Value=0x436)]
        StringTableId_NpcShop_Title_GuildGala_Exchange = 0x436,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarLockConfirm", Value=0x437)]
        StringTableId_GuildFlagShipHangarLockConfirm = 0x437,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarUnLockConfirm", Value=0x438)]
        StringTableId_GuildFlagShipHangarUnLockConfirm = 0x438,
        [ProtoEnum(Name="StringTableId_GuildFlagShipHangarFuelStoreValueFormatString", Value=0x439)]
        StringTableId_GuildFlagShipHangarFuelStoreValueFormatString = 0x439,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetCount", Value=0x43a)]
        StringTableId_FactionCreditTraceQuestPlanetCount = 0x43a,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetIsInfectedTitle", Value=0x43b)]
        StringTableId_FactionCreditTraceQuestPlanetIsInfectedTitle = 0x43b,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetIsInfectedText", Value=0x43c)]
        StringTableId_FactionCreditTraceQuestPlanetIsInfectedText = 0x43c,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestStarRadius", Value=0x43d)]
        StringTableId_FactionCreditTraceQuestStarRadius = 0x43d,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestStarAge", Value=0x43e)]
        StringTableId_FactionCreditTraceQuestStarAge = 0x43e,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetType", Value=0x43f)]
        StringTableId_FactionCreditTraceQuestPlanetType = 0x43f,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetOcean", Value=0x440)]
        StringTableId_FactionCreditTraceQuestPlanetOcean = 0x440,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetMass", Value=0x441)]
        StringTableId_FactionCreditTraceQuestPlanetMass = 0x441,
        [ProtoEnum(Name="StringTableId_FactionCreditTraceQuestPlanetGravity", Value=0x442)]
        StringTableId_FactionCreditTraceQuestPlanetGravity = 0x442,
        [ProtoEnum(Name="StringTableId_FactionCreditTracDataInSpace", Value=0x443)]
        StringTableId_FactionCreditTracDataInSpace = 0x443,
        [ProtoEnum(Name="StringTableId_FactionCreditGetDealTip", Value=0x444)]
        StringTableId_FactionCreditGetDealTip = 0x444,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelRequireTip", Value=0x445)]
        StringTableId_FactionCreditLevelRequireTip = 0x445,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelCurTip", Value=0x446)]
        StringTableId_FactionCreditLevelCurTip = 0x446,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelGetAwardHornor", Value=0x447)]
        StringTableId_FactionCreditLevelGetAwardHornor = 0x447,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelGetAwardCurrency", Value=0x448)]
        StringTableId_FactionCreditLevelGetAwardCurrency = 0x448,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelGetAwardInfoTip", Value=0x449)]
        StringTableId_FactionCreditLevelGetAwardInfoTip = 0x449,
        [ProtoEnum(Name="StringTableId_FactionCreditWeeklyGetSucess", Value=0x44a)]
        StringTableId_FactionCreditWeeklyGetSucess = 0x44a,
        [ProtoEnum(Name="StringTableId_MonthCardLeftHour", Value=0x44b)]
        StringTableId_MonthCardLeftHour = 0x44b,
        [ProtoEnum(Name="StringTableId_MonthCardLeftDay", Value=0x44c)]
        StringTableId_MonthCardLeftDay = 0x44c,
        [ProtoEnum(Name="StringTableId_RechargeItemFirstBonusDesc", Value=0x44d)]
        StringTableId_RechargeItemFirstBonusDesc = 0x44d,
        [ProtoEnum(Name="StringTableId_RechargeItemNormalBonusDesc", Value=0x44e)]
        StringTableId_RechargeItemNormalBonusDesc = 0x44e,
        [ProtoEnum(Name="StringTableId_SpaceSignalLowPrecisenessDesc", Value=0x44f)]
        StringTableId_SpaceSignalLowPrecisenessDesc = 0x44f,
        [ProtoEnum(Name="StringTableId_OnlyInStationCanOperate", Value=0x450)]
        StringTableId_OnlyInStationCanOperate = 0x450,
        [ProtoEnum(Name="StringTableId_SpaceSignalScanResultNoSignal", Value=0x451)]
        StringTableId_SpaceSignalScanResultNoSignal = 0x451,
        [ProtoEnum(Name="StringTableId_SpaceSignalScanResultUpdateSignal", Value=0x452)]
        StringTableId_SpaceSignalScanResultUpdateSignal = 0x452,
        [ProtoEnum(Name="StringTableId_SpaceSignalScanResultOldSignal", Value=0x453)]
        StringTableId_SpaceSignalScanResultOldSignal = 0x453,
        [ProtoEnum(Name="StringTableId_MonthCardBuySuccessNotifyText", Value=0x454)]
        StringTableId_MonthCardBuySuccessNotifyText = 0x454,
        [ProtoEnum(Name="StringTableId_MonthCardBuyFailedNotifyText", Value=0x455)]
        StringTableId_MonthCardBuyFailedNotifyText = 0x455,
        [ProtoEnum(Name="StringTableId_GiftPackageBuySuccessNotifyText", Value=0x456)]
        StringTableId_GiftPackageBuySuccessNotifyText = 0x456,
        [ProtoEnum(Name="StringTableId_GiftPackageBuyFailedNotifyText", Value=0x457)]
        StringTableId_GiftPackageBuyFailedNotifyText = 0x457,
        [ProtoEnum(Name="StringTableId_RechargeItemBuySuccessNotifyText", Value=0x458)]
        StringTableId_RechargeItemBuySuccessNotifyText = 0x458,
        [ProtoEnum(Name="StringTableId_RechargeIteBuyFailedNotifyText", Value=0x459)]
        StringTableId_RechargeIteBuyFailedNotifyText = 0x459,
        [ProtoEnum(Name="StringTableId_MonthCardDailyRewardAlreadyReceive", Value=0x45a)]
        StringTableId_MonthCardDailyRewardAlreadyReceive = 0x45a,
        [ProtoEnum(Name="StringTableId_GiftPackageAlreadySellOut", Value=0x45b)]
        StringTableId_GiftPackageAlreadySellOut = 0x45b,
        [ProtoEnum(Name="StringTableId_RechargeOrderTitleText", Value=0x45c)]
        StringTableId_RechargeOrderTitleText = 0x45c,
        [ProtoEnum(Name="StringTableId_AcceptQuestTip", Value=0x45d)]
        StringTableId_AcceptQuestTip = 0x45d,
        [ProtoEnum(Name="StringTableId_GuildDismissTip", Value=0x45e)]
        StringTableId_GuildDismissTip = 0x45e,
        [ProtoEnum(Name="StringTableId_BaseWarehouse", Value=0x45f)]
        StringTableId_BaseWarehouse = 0x45f,
        [ProtoEnum(Name="StringTableId_CurShipWarehouse", Value=0x460)]
        StringTableId_CurShipWarehouse = 0x460,
        [ProtoEnum(Name="StringTableId_UnableToCalcDistance", Value=0x461)]
        StringTableId_UnableToCalcDistance = 0x461,
        [ProtoEnum(Name="StringTableId_CouldProduceByGuildProduce", Value=0x462)]
        StringTableId_CouldProduceByGuildProduce = 0x462,
        [ProtoEnum(Name="StringTableId_CouldGetTheMineralBySpecificGuildActivity", Value=0x463)]
        StringTableId_CouldGetTheMineralBySpecificGuildActivity = 0x463,
        [ProtoEnum(Name="StringTableId_BlackMarketHasNoVaildSpecialGiftPackage", Value=0x464)]
        StringTableId_BlackMarketHasNoVaildSpecialGiftPackage = 0x464,
        [ProtoEnum(Name="StringTableId_Development_CatalystNumberSufficient", Value=0x465)]
        StringTableId_Development_CatalystNumberSufficient = 0x465,
        [ProtoEnum(Name="StringTableId_Development_CatalystNumberShort", Value=0x466)]
        StringTableId_Development_CatalystNumberShort = 0x466,
        [ProtoEnum(Name="StringTableId_Development_CurrencyShort", Value=0x467)]
        StringTableId_Development_CurrencyShort = 0x467,
        [ProtoEnum(Name="StringTableId_Development_ItemShort", Value=0x468)]
        StringTableId_Development_ItemShort = 0x468,
        [ProtoEnum(Name="StringTableId_Development_ConfirmationContext", Value=0x469)]
        StringTableId_Development_ConfirmationContext = 0x469,
        [ProtoEnum(Name="StringTableId_Development_AutoFillFailed", Value=0x46a)]
        StringTableId_Development_AutoFillFailed = 0x46a,
        [ProtoEnum(Name="StringTableId_CompoundNounsJointString", Value=0x46b)]
        StringTableId_CompoundNounsJointString = 0x46b,
        [ProtoEnum(Name="StringTableId_PreModificationNounJointString", Value=0x46c)]
        StringTableId_PreModificationNounJointString = 0x46c,
        [ProtoEnum(Name="StringTableId_BlackMarket_Weekly_PackageSoldOut", Value=0x46d)]
        StringTableId_BlackMarket_Weekly_PackageSoldOut = 0x46d,
        [ProtoEnum(Name="StringTableId_BlackMarket_NotAmountLimit", Value=0x46e)]
        StringTableId_BlackMarket_NotAmountLimit = 0x46e,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsWorking", Value=0x46f)]
        StringTableId_GuildBuildingStatsWorking = 0x46f,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsCoolingDown", Value=0x470)]
        StringTableId_GuildBuildingStatsCoolingDown = 0x470,
        [ProtoEnum(Name="StringTableId_BattlePassChallengeTimeOut", Value=0x471)]
        StringTableId_BattlePassChallengeTimeOut = 0x471,
        [ProtoEnum(Name="StringTableId_BattlePassCurChallengeUnlockTime", Value=0x472)]
        StringTableId_BattlePassCurChallengeUnlockTime = 0x472,
        [ProtoEnum(Name="StringTableId_BattlePassCurChallengeOutTime", Value=0x473)]
        StringTableId_BattlePassCurChallengeOutTime = 0x473,
        [ProtoEnum(Name="StringTableId_BattlePassLevelsRequireUpgrade", Value=0x474)]
        StringTableId_BattlePassLevelsRequireUpgrade = 0x474,
        [ProtoEnum(Name="StringTableId_BattlePassChallengeItemNone", Value=0x475)]
        StringTableId_BattlePassChallengeItemNone = 0x475,
        [ProtoEnum(Name="StringTableId_BattlePassChallengeItemLast", Value=0x476)]
        StringTableId_BattlePassChallengeItemLast = 0x476,
        [ProtoEnum(Name="StringTableId_BattlePassChallengeCurWeekIndex", Value=0x477)]
        StringTableId_BattlePassChallengeCurWeekIndex = 0x477,
        [ProtoEnum(Name="StringTableId_BattlePassChallengeAccRequireCount", Value=0x478)]
        StringTableId_BattlePassChallengeAccRequireCount = 0x478,
        [ProtoEnum(Name="StringTableId_BattlePassUpgradeRequire", Value=0x479)]
        StringTableId_BattlePassUpgradeRequire = 0x479,
        [ProtoEnum(Name="StringTableId_BattlePassAwardNone", Value=0x47a)]
        StringTableId_BattlePassAwardNone = 0x47a,
        [ProtoEnum(Name="StringTableId_GuildAnnoucementOrManifestoHaveSensitiveWord", Value=0x47b)]
        StringTableId_GuildAnnoucementOrManifestoHaveSensitiveWord = 0x47b,
        [ProtoEnum(Name="StringTableId_CaptainPropertyQueryFailInCurrUI", Value=0x47c)]
        StringTableId_CaptainPropertyQueryFailInCurrUI = 0x47c,
        [ProtoEnum(Name="StringTableId_ActivityNotStart", Value=0x47d)]
        StringTableId_ActivityNotStart = 0x47d,
        [ProtoEnum(Name="StringTableId_GuildResourceShopInvalid", Value=0x47e)]
        StringTableId_GuildResourceShopInvalid = 0x47e,
        [ProtoEnum(Name="StringTableId_DelegateQuestSimpleInfoListAlreadyLastReport", Value=0x47f)]
        StringTableId_DelegateQuestSimpleInfoListAlreadyLastReport = 0x47f,
        [ProtoEnum(Name="StringTableId_QuestListCancelQuestFail", Value=0x480)]
        StringTableId_QuestListCancelQuestFail = 0x480,
        [ProtoEnum(Name="StringTableId_InvadeAlreadyEnd", Value=0x481)]
        StringTableId_InvadeAlreadyEnd = 0x481,
        [ProtoEnum(Name="StringTableId_InfectIsNotExist", Value=0x482)]
        StringTableId_InfectIsNotExist = 0x482,
        [ProtoEnum(Name="StringTableId_GuildBuildingStatsUnactivated", Value=0x483)]
        StringTableId_GuildBuildingStatsUnactivated = 0x483,
        [ProtoEnum(Name="StringTableId_QuestConditionLvShow", Value=0x484)]
        StringTableId_QuestConditionLvShow = 0x484,
        [ProtoEnum(Name="StringTableId_ChatAllianceNameLinkStr", Value=0x485)]
        StringTableId_ChatAllianceNameLinkStr = 0x485,
        [ProtoEnum(Name="StringTableId_ScenSpaceSignalNeedSpaceSignalScanner", Value=0x486)]
        StringTableId_ScenSpaceSignalNeedSpaceSignalScanner = 0x486,
        [ProtoEnum(Name="StringTableId_CaptainRetireReserveFeatMaxRate", Value=0x487)]
        StringTableId_CaptainRetireReserveFeatMaxRate = 0x487,
        [ProtoEnum(Name="StringTableId_ShipSalvageBuyMothCard", Value=0x488)]
        StringTableId_ShipSalvageBuyMothCard = 0x488,
        [ProtoEnum(Name="StringTableId_ChatUIShowOriginalText", Value=0x489)]
        StringTableId_ChatUIShowOriginalText = 0x489,
        [ProtoEnum(Name="StringTableId_ChatUIShowTranslateText", Value=0x48a)]
        StringTableId_ChatUIShowTranslateText = 0x48a,
        [ProtoEnum(Name="StringTableId_DoShareTile", Value=0x48b)]
        StringTableId_DoShareTile = 0x48b,
        [ProtoEnum(Name="StringTableId_DoShareContent", Value=0x48c)]
        StringTableId_DoShareContent = 0x48c,
        [ProtoEnum(Name="StringTableId_DoShareUrl", Value=0x48d)]
        StringTableId_DoShareUrl = 0x48d,
        [ProtoEnum(Name="StringTableId_BattlePassRewardGetTips", Value=0x48e)]
        StringTableId_BattlePassRewardGetTips = 0x48e,
        [ProtoEnum(Name="StringTableId_BattlePassRewardCatNormal", Value=0x48f)]
        StringTableId_BattlePassRewardCatNormal = 0x48f,
        [ProtoEnum(Name="StringTableId_BattlePassRewardCatUpgrade", Value=0x490)]
        StringTableId_BattlePassRewardCatUpgrade = 0x490,
        [ProtoEnum(Name="StringTableId_BattlePassGetSuperAward", Value=0x491)]
        StringTableId_BattlePassGetSuperAward = 0x491,
        [ProtoEnum(Name="StringTableId_BattlePassLevelMax", Value=0x492)]
        StringTableId_BattlePassLevelMax = 0x492,
        [ProtoEnum(Name="StringTableId_BattlePassLevelGetSuffix", Value=0x493)]
        StringTableId_BattlePassLevelGetSuffix = 0x493,
        [ProtoEnum(Name="StringTableId_BattlePassGetAccNormal", Value=0x494)]
        StringTableId_BattlePassGetAccNormal = 0x494,
        [ProtoEnum(Name="StringTableId_BattlePassGetAccUpgrade", Value=0x495)]
        StringTableId_BattlePassGetAccUpgrade = 0x495,
        [ProtoEnum(Name="StringTableId_FactionQuestScanPlanetCountDesc", Value=0x496)]
        StringTableId_FactionQuestScanPlanetCountDesc = 0x496,
        [ProtoEnum(Name="StringTableId_FactionQuestScanIsInfectedDesc", Value=0x497)]
        StringTableId_FactionQuestScanIsInfectedDesc = 0x497,
        [ProtoEnum(Name="StringTableId_FactionQuestStarRadiusDesc", Value=0x498)]
        StringTableId_FactionQuestStarRadiusDesc = 0x498,
        [ProtoEnum(Name="StringTableId_FactionQuestStarScanStarAgeDesc", Value=0x499)]
        StringTableId_FactionQuestStarScanStarAgeDesc = 0x499,
        [ProtoEnum(Name="StringTableId_FactionQuestScanPlanetTypeDesc", Value=0x49a)]
        StringTableId_FactionQuestScanPlanetTypeDesc = 0x49a,
        [ProtoEnum(Name="StringTableId_FactionQuestScanPlanetMass", Value=0x49b)]
        StringTableId_FactionQuestScanPlanetMass = 0x49b,
        [ProtoEnum(Name="StringTableId_FactionQuestScanPlanetGravity", Value=0x49c)]
        StringTableId_FactionQuestScanPlanetGravity = 0x49c,
        [ProtoEnum(Name="StringTableId_DoShareSuccess", Value=0x49d)]
        StringTableId_DoShareSuccess = 0x49d,
        [ProtoEnum(Name="StringTableId_DoShareFailed", Value=0x49e)]
        StringTableId_DoShareFailed = 0x49e,
        [ProtoEnum(Name="StringTableId_DoShareCancel", Value=0x49f)]
        StringTableId_DoShareCancel = 0x49f,
        [ProtoEnum(Name="StringTableId_BaseShouldRedeployToSolarSytemCanArrived", Value=0x4a0)]
        StringTableId_BaseShouldRedeployToSolarSytemCanArrived = 0x4a0,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenShop_Faction", Value=0x4a1)]
        StringTableId_NpcDialogOptionOpenShop_Faction = 0x4a1,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionQuestFactionAccept", Value=0x4a2)]
        StringTableId_NpcDialogOptionQuestFactionAccept = 0x4a2,
        [ProtoEnum(Name="StringTableId_GuildTradeCreateOrderInvalidAmount", Value=0x4a3)]
        StringTableId_GuildTradeCreateOrderInvalidAmount = 0x4a3,
        [ProtoEnum(Name="StringTableId_GuildTradeCancleOrderConfirm", Value=0x4a4)]
        StringTableId_GuildTradeCancleOrderConfirm = 0x4a4,
        [ProtoEnum(Name="StringTableId_Development_ShortOfChips", Value=0x4a5)]
        StringTableId_Development_ShortOfChips = 0x4a5,
        [ProtoEnum(Name="StringTableId_GuildTradeEditOrderInvalidPrice", Value=0x4a6)]
        StringTableId_GuildTradeEditOrderInvalidPrice = 0x4a6,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportInvalidAmount", Value=0x4a7)]
        StringTableId_GuildTradeTransportInvalidAmount = 0x4a7,
        [ProtoEnum(Name="StringTableId_FactioinCreditRewardTip", Value=0x4a8)]
        StringTableId_FactioinCreditRewardTip = 0x4a8,
        [ProtoEnum(Name="StringTableId_NeedCommanderLevel", Value=0x4a9)]
        StringTableId_NeedCommanderLevel = 0x4a9,
        [ProtoEnum(Name="StringTableId_AntiTeleportActivateConfirm", Value=0x4aa)]
        StringTableId_AntiTeleportActivateConfirm = 0x4aa,
        [ProtoEnum(Name="StringTableId_MsgBoxTipsTitle", Value=0x4ab)]
        StringTableId_MsgBoxTipsTitle = 0x4ab,
        [ProtoEnum(Name="StringTableId_DrivingLicenseUpgrade", Value=0x4ac)]
        StringTableId_DrivingLicenseUpgrade = 0x4ac,
        [ProtoEnum(Name="StringTableId_ConfirmLeaveSpaceSignalScene", Value=0x4ad)]
        StringTableId_ConfirmLeaveSpaceSignalScene = 0x4ad,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_TradePort", Value=0x4ae)]
        StringTableId_GuildStoreItemListUI_Building_TradePort = 0x4ae,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_TeleportStation", Value=0x4af)]
        StringTableId_GuildStoreItemListUI_Building_TeleportStation = 0x4af,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Building_AntiTeleportStation", Value=0x4b0)]
        StringTableId_GuildStoreItemListUI_Building_AntiTeleportStation = 0x4b0,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_ECD", Value=0x4b1)]
        StringTableId_GuildStoreItemListUI_Flagship_ECD = 0x4b1,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_NEF", Value=0x4b2)]
        StringTableId_GuildStoreItemListUI_Flagship_NEF = 0x4b2,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_RS", Value=0x4b3)]
        StringTableId_GuildStoreItemListUI_Flagship_RS = 0x4b3,
        [ProtoEnum(Name="StringTableId_GuildStoreItemListUI_Flagship_OE", Value=0x4b4)]
        StringTableId_GuildStoreItemListUI_Flagship_OE = 0x4b4,
        [ProtoEnum(Name="StringTableId_MultiLanguage_TwoNounsConcatenation", Value=0x4b5)]
        StringTableId_MultiLanguage_TwoNounsConcatenation = 0x4b5,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportNotEnough", Value=0x4b6)]
        StringTableId_GuildTradeTransportNotEnough = 0x4b6,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportNoValid", Value=0x4b7)]
        StringTableId_GuildTradeTransportNoValid = 0x4b7,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportStorageNotEnough", Value=0x4b8)]
        StringTableId_GuildTradeTransportStorageNotEnough = 0x4b8,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportMaxPurchase", Value=0x4b9)]
        StringTableId_GuildTradeTransportMaxPurchase = 0x4b9,
        [ProtoEnum(Name="StringTableId_BattlePassBuyPacketTips", Value=0x4ba)]
        StringTableId_BattlePassBuyPacketTips = 0x4ba,
        [ProtoEnum(Name="StringTableId_BattlePassBuyBindPacketTips", Value=0x4bb)]
        StringTableId_BattlePassBuyBindPacketTips = 0x4bb,
        [ProtoEnum(Name="StringTableId_GuildTradeOrderTradeMoneyNotEnough", Value=0x4bc)]
        StringTableId_GuildTradeOrderTradeMoneyNotEnough = 0x4bc,
        [ProtoEnum(Name="StringTableId_GuildTradeOrderPriceOverLimit", Value=0x4bd)]
        StringTableId_GuildTradeOrderPriceOverLimit = 0x4bd,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenTradeIllegalNpcShop", Value=0x4be)]
        StringTableId_NpcDialogOptionOpenTradeIllegalNpcShop = 0x4be,
        [ProtoEnum(Name="StringTableId_PersonalProduction", Value=0x4bf)]
        StringTableId_PersonalProduction = 0x4bf,
        [ProtoEnum(Name="StringTableId_GuildProduction", Value=0x4c0)]
        StringTableId_GuildProduction = 0x4c0,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenTradeNpcShop", Value=0x4c1)]
        StringTableId_NpcDialogOptionOpenTradeNpcShop = 0x4c1,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelForTradePurchase", Value=0x4c2)]
        StringTableId_FactionCreditLevelForTradePurchase = 0x4c2,
        [ProtoEnum(Name="StringTableId_ItemObtainSource_SpaceSignalTip", Value=0x4c3)]
        StringTableId_ItemObtainSource_SpaceSignalTip = 0x4c3,
        [ProtoEnum(Name="StringTableId_TradePurchaseFactionCreditLevelNotEnough", Value=0x4c4)]
        StringTableId_TradePurchaseFactionCreditLevelNotEnough = 0x4c4,
        [ProtoEnum(Name="StringTableId_BlackMarket_Daily_PackageSoldOut", Value=0x4c5)]
        StringTableId_BlackMarket_Daily_PackageSoldOut = 0x4c5,
        [ProtoEnum(Name="StringTableId_GetFactionCreditRewardTips", Value=0x4c6)]
        StringTableId_GetFactionCreditRewardTips = 0x4c6,
        [ProtoEnum(Name="StringTableId_GuildTradeTransportEntity", Value=0x4c7)]
        StringTableId_GuildTradeTransportEntity = 0x4c7,
        [ProtoEnum(Name="StringTableId_StarMapCanNotJumpTo", Value=0x4c8)]
        StringTableId_StarMapCanNotJumpTo = 0x4c8,
        [ProtoEnum(Name="StringTableId_GuildGalaGetTips", Value=0x4c9)]
        StringTableId_GuildGalaGetTips = 0x4c9,
        [ProtoEnum(Name="StringTableId_PanGalaGetTips", Value=0x4ca)]
        StringTableId_PanGalaGetTips = 0x4ca,
        [ProtoEnum(Name="StringTableId_HornorGetTips", Value=0x4cb)]
        StringTableId_HornorGetTips = 0x4cb,
        [ProtoEnum(Name="StringTableId_GuildInformationPointGetTips", Value=0x4cc)]
        StringTableId_GuildInformationPointGetTips = 0x4cc,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenShop_TradeBuy", Value=0x4cd)]
        StringTableId_NpcDialogOptionOpenShop_TradeBuy = 0x4cd,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenShop_PanGala", Value=0x4ce)]
        StringTableId_NpcDialogOptionOpenShop_PanGala = 0x4ce,
        [ProtoEnum(Name="StringTableId_NpcDialogOptionOpenShop_GuildGala", Value=0x4cf)]
        StringTableId_NpcDialogOptionOpenShop_GuildGala = 0x4cf,
        [ProtoEnum(Name="StringTableId_GotoBlackMarket_LackOfTradeMoney", Value=0x4d0)]
        StringTableId_GotoBlackMarket_LackOfTradeMoney = 0x4d0,
        [ProtoEnum(Name="StringTableId_GotoBlackMarket_LackOfRealMoney", Value=0x4d1)]
        StringTableId_GotoBlackMarket_LackOfRealMoney = 0x4d1,
        [ProtoEnum(Name="StringTableId_GuildTradeNoGuildTradePortOffline", Value=0x4d2)]
        StringTableId_GuildTradeNoGuildTradePortOffline = 0x4d2,
        [ProtoEnum(Name="StringTableId_TranslateStringProcessing", Value=0x4d3)]
        StringTableId_TranslateStringProcessing = 0x4d3,
        [ProtoEnum(Name="StringTableId_TranslateStringFailed", Value=0x4d4)]
        StringTableId_TranslateStringFailed = 0x4d4,
        [ProtoEnum(Name="StringTableId_Audio2StringProcessing", Value=0x4d5)]
        StringTableId_Audio2StringProcessing = 0x4d5,
        [ProtoEnum(Name="StringTableId_Audio2StringFailed", Value=0x4d6)]
        StringTableId_Audio2StringFailed = 0x4d6,
        [ProtoEnum(Name="StringTableId_PersonalTradeGuildFlagShipNotAllow", Value=0x4d7)]
        StringTableId_PersonalTradeGuildFlagShipNotAllow = 0x4d7,
        [ProtoEnum(Name="StringTableId_GuildProudctionLineListNotIdle", Value=0x4d8)]
        StringTableId_GuildProudctionLineListNotIdle = 0x4d8,
        [ProtoEnum(Name="StringTableId_FactionCreditLevelForTradePurchaseWithValue", Value=0x4d9)]
        StringTableId_FactionCreditLevelForTradePurchaseWithValue = 0x4d9,
        [ProtoEnum(Name="StringTableId_MonthCardUnlockTipForAuction", Value=0x4da)]
        StringTableId_MonthCardUnlockTipForAuction = 0x4da,
        [ProtoEnum(Name="StringTableId_MonthCardUnLockTipForTech", Value=0x4db)]
        StringTableId_MonthCardUnLockTipForTech = 0x4db,
        [ProtoEnum(Name="StringTableId_LightYear", Value=0x4dc)]
        StringTableId_LightYear = 0x4dc,
        [ProtoEnum(Name="StringTableId_RandomTransport", Value=0x4dd)]
        StringTableId_RandomTransport = 0x4dd,
        [ProtoEnum(Name="StringTableId_FactionCreditQuestrCooldownHourTips", Value=0x4de)]
        StringTableId_FactionCreditQuestrCooldownHourTips = 0x4de,
        [ProtoEnum(Name="StringTableId_GuildTradeOrderAmountOverLimit", Value=0x4df)]
        StringTableId_GuildTradeOrderAmountOverLimit = 0x4df,
        [ProtoEnum(Name="StringTableId_LogOutInSpaceTip4FlagShip", Value=0x4e0)]
        StringTableId_LogOutInSpaceTip4FlagShip = 0x4e0,
        [ProtoEnum(Name="StringTableId_MultiLanguage_TitleAndNameConcatenation", Value=0x4e1)]
        StringTableId_MultiLanguage_TitleAndNameConcatenation = 0x4e1,
        [ProtoEnum(Name="StringTableId_MultiLanguage_ThreeNounsConcatenation", Value=0x4e2)]
        StringTableId_MultiLanguage_ThreeNounsConcatenation = 0x4e2,
        [ProtoEnum(Name="StringTableId_MultiLanguage_FourNounsConcatenation", Value=0x4e3)]
        StringTableId_MultiLanguage_FourNounsConcatenation = 0x4e3,
        [ProtoEnum(Name="StringTableId_BuildingZeroLevelDesc", Value=0x4e4)]
        StringTableId_BuildingZeroLevelDesc = 0x4e4,
        [ProtoEnum(Name="StringTableId_BuildingMaxLevelDesc", Value=0x4e5)]
        StringTableId_BuildingMaxLevelDesc = 0x4e5,
        [ProtoEnum(Name="StringTableId_GuildFlagShipIsNotAllowedInScene", Value=0x4e6)]
        StringTableId_GuildFlagShipIsNotAllowedInScene = 0x4e6,
        [ProtoEnum(Name="StringTableId_GuildAutoCompensation", Value=0x4e7)]
        StringTableId_GuildAutoCompensation = 0x4e7,
        [ProtoEnum(Name="StringTableId_MonthCardSubscriptionDesc", Value=0x4e8)]
        StringTableId_MonthCardSubscriptionDesc = 0x4e8,
        [ProtoEnum(Name="StringTableId_UnitMonthString", Value=0x4e9)]
        StringTableId_UnitMonthString = 0x4e9,
        [ProtoEnum(Name="StringTableId_FirstLastNameStyleForECD", Value=0x4ea)]
        StringTableId_FirstLastNameStyleForECD = 0x4ea,
        [ProtoEnum(Name="StringTableId_FlagshipNoShipStoreTip", Value=0x4eb)]
        StringTableId_FlagshipNoShipStoreTip = 0x4eb,
        [ProtoEnum(Name="StringTableId_UpLoadClientLogSuccess", Value=0x4ec)]
        StringTableId_UpLoadClientLogSuccess = 0x4ec,
        [ProtoEnum(Name="StringTableId_UpLoadClientLogFailPleaseRetry", Value=0x4ed)]
        StringTableId_UpLoadClientLogFailPleaseRetry = 0x4ed,
        [ProtoEnum(Name="StringTableId_SpaceSignalPossibility", Value=0x4ee)]
        StringTableId_SpaceSignalPossibility = 0x4ee,
        [ProtoEnum(Name="StringTableId_UnitFormatDayString", Value=0x4ef)]
        StringTableId_UnitFormatDayString = 0x4ef,
        [ProtoEnum(Name="StringTableId_UnitLongNumberOne", Value=0x4f0)]
        StringTableId_UnitLongNumberOne = 0x4f0,
        [ProtoEnum(Name="StringTableId_UnitLongNumberTwo", Value=0x4f1)]
        StringTableId_UnitLongNumberTwo = 0x4f1,
        [ProtoEnum(Name="StringTableId_UnitLongNumberThree", Value=0x4f2)]
        StringTableId_UnitLongNumberThree = 0x4f2,
        [ProtoEnum(Name="StringTableId_UnitLongNumberFour", Value=0x4f3)]
        StringTableId_UnitLongNumberFour = 0x4f3,
        [ProtoEnum(Name="StringTableId_UnitLongNumberFive", Value=0x4f4)]
        StringTableId_UnitLongNumberFive = 0x4f4,
        [ProtoEnum(Name="StringTableId_UnitLongNumberSix", Value=0x4f5)]
        StringTableId_UnitLongNumberSix = 0x4f5,
        [ProtoEnum(Name="StringTableId_UnitLongNumberSeven", Value=0x4f6)]
        StringTableId_UnitLongNumberSeven = 0x4f6,
        [ProtoEnum(Name="StringTableId_OverSeaActivity_AccountNotBound", Value=0x4f7)]
        StringTableId_OverSeaActivity_AccountNotBound = 0x4f7,
        [ProtoEnum(Name="StringTableId_OverSeaActivity_IsFormalAccount", Value=0x4f8)]
        StringTableId_OverSeaActivity_IsFormalAccount = 0x4f8,
        [ProtoEnum(Name="StringTableId_NpcShop_BuyListNull", Value=0x4f9)]
        StringTableId_NpcShop_BuyListNull = 0x4f9,
        [ProtoEnum(Name="StringTableId_NpcShop_SaleListNull", Value=0x4fa)]
        StringTableId_NpcShop_SaleListNull = 0x4fa,
        [ProtoEnum(Name="StringTableId_ExploreQuestTypePlanetInjectErrorTip", Value=0x4fb)]
        StringTableId_ExploreQuestTypePlanetInjectErrorTip = 0x4fb,
        [ProtoEnum(Name="StringTableId_ExploreQuestTypeStarErrorTip", Value=0x4fc)]
        StringTableId_ExploreQuestTypeStarErrorTip = 0x4fc,
        [ProtoEnum(Name="StringTableId_ExploreQuestTypePlanetErrorTip", Value=0x4fd)]
        StringTableId_ExploreQuestTypePlanetErrorTip = 0x4fd,
        [ProtoEnum(Name="StringTableId_GameServerShutdownContextReasonGameLogicLimited", Value=0x4fe)]
        StringTableId_GameServerShutdownContextReasonGameLogicLimited = 0x4fe,
        [ProtoEnum(Name="StringTableId_CannotEnterLawlessSpace_Trade", Value=0x4ff)]
        StringTableId_CannotEnterLawlessSpace_Trade = 0x4ff,
        [ProtoEnum(Name="StringTableId_ServerState_Fluent", Value=0x500)]
        StringTableId_ServerState_Fluent = 0x500,
        [ProtoEnum(Name="StringTableId_ServerState_Maintain", Value=0x501)]
        StringTableId_ServerState_Maintain = 0x501,
        [ProtoEnum(Name="StringTableId_ServerState_Busy", Value=0x502)]
        StringTableId_ServerState_Busy = 0x502,
        [ProtoEnum(Name="StringTableId_ServerState_HouseFull", Value=0x503)]
        StringTableId_ServerState_HouseFull = 0x503,
        [ProtoEnum(Name="StringTableId_ServerListGetFail", Value=0x504)]
        StringTableId_ServerListGetFail = 0x504,
        [ProtoEnum(Name="StringTableId_DismissAllianceLeaderGuildConfirm", Value=0x505)]
        StringTableId_DismissAllianceLeaderGuildConfirm = 0x505,
        [ProtoEnum(Name="StringTableId_VirtualBuffSpaceSignalInCDTitle", Value=0x506)]
        StringTableId_VirtualBuffSpaceSignalInCDTitle = 0x506,
        [ProtoEnum(Name="StringTableId_VirtualBuffSpaceSignalInCDDesc", Value=0x507)]
        StringTableId_VirtualBuffSpaceSignalInCDDesc = 0x507,
        [ProtoEnum(Name="StringTableId_VirtualBuffSpaceSignalInCDCountDown", Value=0x508)]
        StringTableId_VirtualBuffSpaceSignalInCDCountDown = 0x508,
        [ProtoEnum(Name="StringTableId_MisbehaviourReportSuccess", Value=0x509)]
        StringTableId_MisbehaviourReportSuccess = 0x509,
        [ProtoEnum(Name="StringTableId_SignalDirectionalScan", Value=0x50a)]
        StringTableId_SignalDirectionalScan = 0x50a,
        [ProtoEnum(Name="StringTableId_Normal", Value=0x50b)]
        StringTableId_Normal = 0x50b,
        [ProtoEnum(Name="StringTableId_ProduceBatchOverLimit", Value=0x50c)]
        StringTableId_ProduceBatchOverLimit = 0x50c,
        [ProtoEnum(Name="StringTableId_TextFormatForQuestRewardType_Currency", Value=0x50d)]
        StringTableId_TextFormatForQuestRewardType_Currency = 0x50d,
        [ProtoEnum(Name="StringTableId_TextFormatForQuestRewardType_Exp", Value=0x50e)]
        StringTableId_TextFormatForQuestRewardType_Exp = 0x50e,
        [ProtoEnum(Name="StringTableId_BindActivity_BindActionTipInPc", Value=0x536)]
        StringTableId_BindActivity_BindActionTipInPc = 0x536
    }
}

