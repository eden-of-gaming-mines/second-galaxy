﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BranchStoryCategory")]
    public enum BranchStoryCategory
    {
        [ProtoEnum(Name="BranchStoryCategory_History", Value=1)]
        BranchStoryCategory_History = 1,
        [ProtoEnum(Name="BranchStoryCategory_Event", Value=2)]
        BranchStoryCategory_Event = 2,
        [ProtoEnum(Name="BranchStoryCategory_Character", Value=3)]
        BranchStoryCategory_Character = 3,
        [ProtoEnum(Name="BranchStoryCategory_Other", Value=4)]
        BranchStoryCategory_Other = 4,
        [ProtoEnum(Name="BranchStoryCategory_Max", Value=5)]
        BranchStoryCategory_Max = 5
    }
}

