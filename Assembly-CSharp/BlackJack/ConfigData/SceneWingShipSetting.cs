﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SceneWingShipSetting")]
    public enum SceneWingShipSetting
    {
        [ProtoEnum(Name="SceneWingShipSetting_DefaultDisable", Value=1)]
        SceneWingShipSetting_DefaultDisable = 1,
        [ProtoEnum(Name="SceneWingShipSetting_DefaultEnable", Value=2)]
        SceneWingShipSetting_DefaultEnable = 2,
        [ProtoEnum(Name="SceneWingShipSetting_AlwaysDisable", Value=3)]
        SceneWingShipSetting_AlwaysDisable = 3
    }
}

