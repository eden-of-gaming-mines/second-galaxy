﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SystemFuncDescType")]
    public enum SystemFuncDescType
    {
        [ProtoEnum(Name="SystemFuncDescType_Invalid", Value=0)]
        SystemFuncDescType_Invalid = 0,
        [ProtoEnum(Name="SystemFuncDescType_CharacterSkill", Value=1)]
        SystemFuncDescType_CharacterSkill = 1,
        [ProtoEnum(Name="SystemFuncDescType_NpcShop", Value=2)]
        SystemFuncDescType_NpcShop = 2,
        [ProtoEnum(Name="SystemFuncDescType_ShipHangar", Value=3)]
        SystemFuncDescType_ShipHangar = 3,
        [ProtoEnum(Name="SystemFuncDescType_ShipAmmoReload", Value=4)]
        SystemFuncDescType_ShipAmmoReload = 4,
        [ProtoEnum(Name="SystemFuncDescType_CharacterPointAdd", Value=5)]
        SystemFuncDescType_CharacterPointAdd = 5,
        [ProtoEnum(Name="SystemFuncDescType_CaptainManagement", Value=6)]
        SystemFuncDescType_CaptainManagement = 6,
        [ProtoEnum(Name="SystemFuncDescType_CaptainTraining", Value=7)]
        SystemFuncDescType_CaptainTraining = 7,
        [ProtoEnum(Name="SystemFuncDescType_CaptainShipManagement", Value=8)]
        SystemFuncDescType_CaptainShipManagement = 8,
        [ProtoEnum(Name="SystemFuncDescType_CaptainRetire", Value=9)]
        SystemFuncDescType_CaptainRetire = 9,
        [ProtoEnum(Name="SystemFuncDescType_CaptainLearning", Value=10)]
        SystemFuncDescType_CaptainLearning = 10,
        [ProtoEnum(Name="SystemFuncDescType_AuctionBuy", Value=11)]
        SystemFuncDescType_AuctionBuy = 11,
        [ProtoEnum(Name="SystemFuncDescType_AuctionSale", Value=12)]
        SystemFuncDescType_AuctionSale = 12,
        [ProtoEnum(Name="SystemFuncDescType_Crack", Value=13)]
        SystemFuncDescType_Crack = 13,
        [ProtoEnum(Name="SystemFuncDescType_TechManagement", Value=14)]
        SystemFuncDescType_TechManagement = 14,
        [ProtoEnum(Name="SystemFuncDescType_TechUpgrade", Value=15)]
        SystemFuncDescType_TechUpgrade = 15,
        [ProtoEnum(Name="SystemFuncDescType_GuildInfo", Value=0x10)]
        SystemFuncDescType_GuildInfo = 0x10,
        [ProtoEnum(Name="SystemFuncDescType_GuildSolarSystem", Value=0x11)]
        SystemFuncDescType_GuildSolarSystem = 0x11,
        [ProtoEnum(Name="SystemFuncDescType_GuildProduce", Value=0x12)]
        SystemFuncDescType_GuildProduce = 0x12,
        [ProtoEnum(Name="SystemFuncDescType_ActivityInfo", Value=0x13)]
        SystemFuncDescType_ActivityInfo = 0x13,
        [ProtoEnum(Name="SystemFuncDescType_GuildFleetManagement", Value=20)]
        SystemFuncDescType_GuildFleetManagement = 20,
        [ProtoEnum(Name="SystemFuncDescType_GuildFleetDetail", Value=0x15)]
        SystemFuncDescType_GuildFleetDetail = 0x15,
        [ProtoEnum(Name="SystemFuncDescType_TradePersonalBuy", Value=0x16)]
        SystemFuncDescType_TradePersonalBuy = 0x16,
        [ProtoEnum(Name="SystemFuncDescType_TradePersonalSale", Value=0x17)]
        SystemFuncDescType_TradePersonalSale = 0x17,
        [ProtoEnum(Name="SystemFuncDescType_TradeGuildSale", Value=0x18)]
        SystemFuncDescType_TradeGuildSale = 0x18,
        [ProtoEnum(Name="SystemFuncDescType_TradeGuildTradePort", Value=0x19)]
        SystemFuncDescType_TradeGuildTradePort = 0x19,
        [ProtoEnum(Name="SystemFuncDescType_GuildFlagShipHangar", Value=0x1a)]
        SystemFuncDescType_GuildFlagShipHangar = 0x1a,
        [ProtoEnum(Name="SystemFuncDescType_FactionCredit", Value=0x1b)]
        SystemFuncDescType_FactionCredit = 0x1b,
        [ProtoEnum(Name="SystemFuncDescType_FactionCreditDetail", Value=0x1c)]
        SystemFuncDescType_FactionCreditDetail = 0x1c,
        [ProtoEnum(Name="SystemFuncDescType_DevelopmentSelection", Value=0x1d)]
        SystemFuncDescType_DevelopmentSelection = 0x1d,
        [ProtoEnum(Name="SystemFuncDescType_DevelopmentTransformation", Value=30)]
        SystemFuncDescType_DevelopmentTransformation = 30,
        [ProtoEnum(Name="SystemFuncDescType_GuildBenefits", Value=0x1f)]
        SystemFuncDescType_GuildBenefits = 0x1f,
        [ProtoEnum(Name="SystemFuncDescType_SpaceSignalScanInStarMap", Value=0x20)]
        SystemFuncDescType_SpaceSignalScanInStarMap = 0x20,
        [ProtoEnum(Name="SystemFuncDescType_SpaceSignalScanInShipHangar", Value=0x21)]
        SystemFuncDescType_SpaceSignalScanInShipHangar = 0x21
    }
}

