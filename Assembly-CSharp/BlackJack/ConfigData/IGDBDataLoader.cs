﻿namespace BlackJack.ConfigData
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGDBDataLoader
    {
        Dictionary<int, GDBSolarSystemSimpleInfo> GetAllGDBSolarSystemSimpleInfo();
        List<int> GetAllLinkedSolarSystemIdListBySolarSystemId(int solarSystemId);
        GDBGalaxyInfo GetGDBGalaxyInfo();
        GDBSolarSystemInfo GetGDBSolarSystemInfo(int solarSystemId);
        GDBSolarSystemSimpleInfo GetGDBSolarSystemSimpleInfo(int solarSystemId);
        GDBSpaceStationInfo GetGDBSpaceStationInfo(GDBSolarSystemInfo solarSystemInfo, int spaceStationId);
        GDBStarfieldsInfo GetGDBStarfieldsInfo(int starFieldId);
        ConfigDataNpcTalkerInfo GetNpcTalkerInfoWithNpcDNId(NpcDNId npcId);
        GDBStarfieldsInfo GetOwnerStarfieldBySolarSystemId(int solarSystemId);
        GDBStargroupInfo GetOwnerStargroupBySolarSystemId(int solarSystemId);
        int GetStargateGDBIdBetweenSolarSystem(int srcSolarsystemId, int destSolarsystemId);
        GDBWormholeGateInfo GetWormholeGateBetweenSolarSystem(int srcSolarsystemId, int destSolarsystemId);
    }
}

