﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="MonthlyCardPriviledgeInfo")]
    public class MonthlyCardPriviledgeInfo : IExtensible
    {
        private MonthlyCardPriviledgeType _PriviledgeType;
        private int _P1;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PriviledgeType;
        private static DelegateBridge __Hotfix_set_PriviledgeType;
        private static DelegateBridge __Hotfix_get_P1;
        private static DelegateBridge __Hotfix_set_P1;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="PriviledgeType", DataFormat=DataFormat.TwosComplement)]
        public MonthlyCardPriviledgeType PriviledgeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="P1", DataFormat=DataFormat.TwosComplement)]
        public int P1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

