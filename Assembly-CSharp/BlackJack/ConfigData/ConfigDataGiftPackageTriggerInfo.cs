﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGiftPackageTriggerInfo")]
    public class ConfigDataGiftPackageTriggerInfo : IExtensible
    {
        private int _ID;
        private int _TriggerQuest;
        private int _ValidTime;
        private int _RefreshTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_TriggerQuest;
        private static DelegateBridge __Hotfix_set_TriggerQuest;
        private static DelegateBridge __Hotfix_get_ValidTime;
        private static DelegateBridge __Hotfix_set_ValidTime;
        private static DelegateBridge __Hotfix_get_RefreshTime;
        private static DelegateBridge __Hotfix_set_RefreshTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TriggerQuest", DataFormat=DataFormat.TwosComplement)]
        public int TriggerQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ValidTime", DataFormat=DataFormat.TwosComplement)]
        public int ValidTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="RefreshTime", DataFormat=DataFormat.TwosComplement)]
        public int RefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

