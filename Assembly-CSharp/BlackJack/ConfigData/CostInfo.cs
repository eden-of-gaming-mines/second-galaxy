﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CostInfo")]
    public class CostInfo : IExtensible
    {
        private BlackJack.ConfigData.CostType _CostType;
        private int _P1;
        private long _P2;
        private long _P3;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CostType;
        private static DelegateBridge __Hotfix_set_CostType;
        private static DelegateBridge __Hotfix_get_P1;
        private static DelegateBridge __Hotfix_set_P1;
        private static DelegateBridge __Hotfix_get_P2;
        private static DelegateBridge __Hotfix_set_P2;
        private static DelegateBridge __Hotfix_get_P3;
        private static DelegateBridge __Hotfix_set_P3;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CostType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.CostType CostType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="P1", DataFormat=DataFormat.TwosComplement)]
        public int P1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="P2", DataFormat=DataFormat.TwosComplement)]
        public long P2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="P3", DataFormat=DataFormat.TwosComplement)]
        public long P3
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

