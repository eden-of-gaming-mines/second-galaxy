﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataAgeInfo")]
    public class ConfigDataAgeInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.AgeType _AgeType;
        private int _StartAge;
        private int _EndAge;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AgeType;
        private static DelegateBridge __Hotfix_set_AgeType;
        private static DelegateBridge __Hotfix_get_StartAge;
        private static DelegateBridge __Hotfix_set_StartAge;
        private static DelegateBridge __Hotfix_get_EndAge;
        private static DelegateBridge __Hotfix_set_EndAge;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AgeType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.AgeType AgeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StartAge", DataFormat=DataFormat.TwosComplement)]
        public int StartAge
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EndAge", DataFormat=DataFormat.TwosComplement)]
        public int EndAge
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

