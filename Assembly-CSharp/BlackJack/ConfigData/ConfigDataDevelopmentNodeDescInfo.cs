﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDevelopmentNodeDescInfo")]
    public class ConfigDataDevelopmentNodeDescInfo : IExtensible
    {
        private int _ID;
        private DevelopmentNodeType _NodeType;
        private DevelopmentNodeIndex _Index;
        private string _NameIcon;
        private readonly List<int> _Children;
        private string _BgIcon;
        private string _PreviewIcon;
        private bool _IsActivated;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_NodeType;
        private static DelegateBridge __Hotfix_set_NodeType;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_NameIcon;
        private static DelegateBridge __Hotfix_set_NameIcon;
        private static DelegateBridge __Hotfix_get_Children;
        private static DelegateBridge __Hotfix_get_BgIcon;
        private static DelegateBridge __Hotfix_set_BgIcon;
        private static DelegateBridge __Hotfix_get_PreviewIcon;
        private static DelegateBridge __Hotfix_set_PreviewIcon;
        private static DelegateBridge __Hotfix_get_IsActivated;
        private static DelegateBridge __Hotfix_set_IsActivated;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="NodeType", DataFormat=DataFormat.TwosComplement)]
        public DevelopmentNodeType NodeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public DevelopmentNodeIndex Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NameIcon", DataFormat=DataFormat.Default)]
        public string NameIcon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="Children", DataFormat=DataFormat.TwosComplement)]
        public List<int> Children
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="BgIcon", DataFormat=DataFormat.Default)]
        public string BgIcon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="PreviewIcon", DataFormat=DataFormat.Default)]
        public string PreviewIcon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IsActivated", DataFormat=DataFormat.Default)]
        public bool IsActivated
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

