﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="TacticalEquipFunctionType")]
    public enum TacticalEquipFunctionType
    {
        [ProtoEnum(Name="TacticalEquipFunctionType_IncByNeadbyDeadThenAttachBuf", Value=1)]
        TacticalEquipFunctionType_IncByNeadbyDeadThenAttachBuf = 1,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBufBySupperEnergyFull", Value=2)]
        TacticalEquipFunctionType_AttachBufBySupperEnergyFull = 2,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBufAndInvisibleByArmorPercent", Value=3)]
        TacticalEquipFunctionType_AttachBufAndInvisibleByArmorPercent = 3,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexByTargetDitance", Value=4)]
        TacticalEquipFunctionType_ProvideFinalexByTargetDitance = 4,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBuff2SelfByAttackCritical", Value=5)]
        TacticalEquipFunctionType_AttachBuff2SelfByAttackCritical = 5,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexBySelfEnergy", Value=6)]
        TacticalEquipFunctionType_ProvideFinalexBySelfEnergy = 6,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalExBySelfSpeed", Value=7)]
        TacticalEquipFunctionType_ProvideFinalExBySelfSpeed = 7,
        [ProtoEnum(Name="TacticalEquipFunctionType_AddShiledAndBuffByArmor", Value=8)]
        TacticalEquipFunctionType_AddShiledAndBuffByArmor = 8,
        [ProtoEnum(Name="TacticalEquipFunctionType_RecoverySuperEnergyByKillingHit", Value=9)]
        TacticalEquipFunctionType_RecoverySuperEnergyByKillingHit = 9,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBuffByNotBeHitForAWhile", Value=10)]
        TacticalEquipFunctionType_AttachBuffByNotBeHitForAWhile = 10,
        [ProtoEnum(Name="TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponEquipLaunch", Value=11)]
        TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponEquipLaunch = 11,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexBySelfShield", Value=12)]
        TacticalEquipFunctionType_ProvideFinalexBySelfShield = 12,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexBySpeedOverload", Value=13)]
        TacticalEquipFunctionType_ProvideFinalexBySpeedOverload = 13,
        [ProtoEnum(Name="TacticalEquipFunctionType_InvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent", Value=14)]
        TacticalEquipFunctionType_InvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent = 14,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBufByKillingHit", Value=15)]
        TacticalEquipFunctionType_AttachBufByKillingHit = 15,
        [ProtoEnum(Name="TacticalEquipFunctionType_AddShiledExBufByCostEnergy", Value=0x10)]
        TacticalEquipFunctionType_AddShiledExBufByCostEnergy = 0x10,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBufByEnergyPercent", Value=0x11)]
        TacticalEquipFunctionType_AttachBufByEnergyPercent = 0x11,
        [ProtoEnum(Name="TacticalEquipFunctionType_IncBufRecovryShieldOnDetachByWeaponEquipLaunch", Value=0x12)]
        TacticalEquipFunctionType_IncBufRecovryShieldOnDetachByWeaponEquipLaunch = 0x12,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBuf2SelfByEnergyAndShieldPercent", Value=0x13)]
        TacticalEquipFunctionType_AttachBuf2SelfByEnergyAndShieldPercent = 0x13,
        [ProtoEnum(Name="TacticalEquipFunctionType_ReduceCDByKillingHit", Value=20)]
        TacticalEquipFunctionType_ReduceCDByKillingHit = 20,
        [ProtoEnum(Name="TacticalEquipFunctionType_IncBufRecoveryEnergyOnNextMakeDamageByWeaponEquipLaunch", Value=0x15)]
        TacticalEquipFunctionType_IncBufRecoveryEnergyOnNextMakeDamageByWeaponEquipLaunch = 0x15,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBufByShieldPercent", Value=0x16)]
        TacticalEquipFunctionType_AttachBufByShieldPercent = 0x16,
        [ProtoEnum(Name="TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponLaunch", Value=0x17)]
        TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponLaunch = 0x17,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexBySpeedOverPercent", Value=0x18)]
        TacticalEquipFunctionType_ProvideFinalexBySpeedOverPercent = 0x18,
        [ProtoEnum(Name="TacticalEquipFunctionType_KeepingInvisibleForWeaponLaunch", Value=0x19)]
        TacticalEquipFunctionType_KeepingInvisibleForWeaponLaunch = 0x19,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexByTargetShieldPercent", Value=0x1a)]
        TacticalEquipFunctionType_ProvideFinalexByTargetShieldPercent = 0x1a,
        [ProtoEnum(Name="TacticalEquipFunctionType_ReduceCDByWeaponEquipLaunch", Value=0x1b)]
        TacticalEquipFunctionType_ReduceCDByWeaponEquipLaunch = 0x1b,
        [ProtoEnum(Name="TacticalEquipFunctionType_AttachBuf2SelfByDodge", Value=0x1c)]
        TacticalEquipFunctionType_AttachBuf2SelfByDodge = 0x1c,
        [ProtoEnum(Name="TacticalEquipFunctionType_IncBufRecovryShieldOnDetachByShieldRepairEquipLaunch", Value=0x1d)]
        TacticalEquipFunctionType_IncBufRecovryShieldOnDetachByShieldRepairEquipLaunch = 0x1d,
        [ProtoEnum(Name="TacticalEquipFunctionType_RecoveryShipEnergyByKillingHit", Value=30)]
        TacticalEquipFunctionType_RecoveryShipEnergyByKillingHit = 30,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexByLocalTargetCount", Value=0x1f)]
        TacticalEquipFunctionType_ProvideFinalexByLocalTargetCount = 0x1f,
        [ProtoEnum(Name="TacticalEquipFunctionType_ProvideFinalexByTargetSpeed", Value=0x20)]
        TacticalEquipFunctionType_ProvideFinalexByTargetSpeed = 0x20
    }
}

