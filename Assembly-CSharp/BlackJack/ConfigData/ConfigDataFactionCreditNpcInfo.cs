﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataFactionCreditNpcInfo")]
    public class ConfigDataFactionCreditNpcInfo : IExtensible
    {
        private int _ID;
        private int _SolarSystemId;
        private int _SpaceStationId;
        private int _NpcId;
        private FactionCreditLevel _Level;
        private int _FactionID;
        private int _QuestMinJump;
        private int _QuestMaxJump;
        private int _QuestMinLevel;
        private int _QuestMaxLevel;
        private ShipType _QuestShipType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_SpaceStationId;
        private static DelegateBridge __Hotfix_set_SpaceStationId;
        private static DelegateBridge __Hotfix_get_NpcId;
        private static DelegateBridge __Hotfix_set_NpcId;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_FactionID;
        private static DelegateBridge __Hotfix_set_FactionID;
        private static DelegateBridge __Hotfix_get_QuestMinJump;
        private static DelegateBridge __Hotfix_set_QuestMinJump;
        private static DelegateBridge __Hotfix_get_QuestMaxJump;
        private static DelegateBridge __Hotfix_set_QuestMaxJump;
        private static DelegateBridge __Hotfix_get_QuestMinLevel;
        private static DelegateBridge __Hotfix_set_QuestMinLevel;
        private static DelegateBridge __Hotfix_get_QuestMaxLevel;
        private static DelegateBridge __Hotfix_set_QuestMaxLevel;
        private static DelegateBridge __Hotfix_get_QuestShipType;
        private static DelegateBridge __Hotfix_set_QuestShipType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SpaceStationId", DataFormat=DataFormat.TwosComplement)]
        public int SpaceStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NpcId", DataFormat=DataFormat.TwosComplement)]
        public int NpcId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public FactionCreditLevel Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="FactionID", DataFormat=DataFormat.TwosComplement)]
        public int FactionID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="QuestMinJump", DataFormat=DataFormat.TwosComplement)]
        public int QuestMinJump
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="QuestMaxJump", DataFormat=DataFormat.TwosComplement)]
        public int QuestMaxJump
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="QuestMinLevel", DataFormat=DataFormat.TwosComplement)]
        public int QuestMinLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="QuestMaxLevel", DataFormat=DataFormat.TwosComplement)]
        public int QuestMaxLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="QuestShipType", DataFormat=DataFormat.TwosComplement)]
        public ShipType QuestShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

