﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataLaserDamageInfo")]
    public class ConfigDataLaserDamageInfo : IExtensible
    {
        private int _ID;
        private float _LaserStep1DamageMulti;
        private float _LaserStep3DamageMulti;
        private uint _LaserStep1Time;
        private uint _LaserStep2Time;
        private uint _LaserStep3Time;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LaserStep1DamageMulti;
        private static DelegateBridge __Hotfix_set_LaserStep1DamageMulti;
        private static DelegateBridge __Hotfix_get_LaserStep3DamageMulti;
        private static DelegateBridge __Hotfix_set_LaserStep3DamageMulti;
        private static DelegateBridge __Hotfix_get_LaserStep1Time;
        private static DelegateBridge __Hotfix_set_LaserStep1Time;
        private static DelegateBridge __Hotfix_get_LaserStep2Time;
        private static DelegateBridge __Hotfix_set_LaserStep2Time;
        private static DelegateBridge __Hotfix_get_LaserStep3Time;
        private static DelegateBridge __Hotfix_set_LaserStep3Time;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LaserStep1DamageMulti", DataFormat=DataFormat.FixedSize)]
        public float LaserStep1DamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="LaserStep3DamageMulti", DataFormat=DataFormat.FixedSize)]
        public float LaserStep3DamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LaserStep1Time", DataFormat=DataFormat.TwosComplement)]
        public uint LaserStep1Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LaserStep2Time", DataFormat=DataFormat.TwosComplement)]
        public uint LaserStep2Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="LaserStep3Time", DataFormat=DataFormat.TwosComplement)]
        public uint LaserStep3Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

