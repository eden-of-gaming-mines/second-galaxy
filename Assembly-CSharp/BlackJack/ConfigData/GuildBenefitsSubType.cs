﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildBenefitsSubType")]
    public enum GuildBenefitsSubType
    {
        [ProtoEnum(Name="GuildBenefitsSubType_GuildGiftPackage", Value=1)]
        GuildBenefitsSubType_GuildGiftPackage = 1,
        [ProtoEnum(Name="GuildBenefitsSubType_SovereigntyBattle", Value=2)]
        GuildBenefitsSubType_SovereigntyBattle = 2,
        [ProtoEnum(Name="GuildBenefitsSubType_BuildingBattle", Value=3)]
        GuildBenefitsSubType_BuildingBattle = 3,
        [ProtoEnum(Name="GuildBenefitsSubType_Action", Value=4)]
        GuildBenefitsSubType_Action = 4,
        [ProtoEnum(Name="GuildBenefitsSubType_Trade", Value=5)]
        GuildBenefitsSubType_Trade = 5,
        [ProtoEnum(Name="GuildBenefitsSubType_BuildingProduce", Value=6)]
        GuildBenefitsSubType_BuildingProduce = 6,
        [ProtoEnum(Name="GuildBenefitsSubType_FlagshipProduce", Value=7)]
        GuildBenefitsSubType_FlagshipProduce = 7,
        [ProtoEnum(Name="GuildBenefitsSubType_InfectFinalBattle", Value=8)]
        GuildBenefitsSubType_InfectFinalBattle = 8,
        [ProtoEnum(Name="GuildBenefitsSubType_SovereigntyBattleExtra", Value=9)]
        GuildBenefitsSubType_SovereigntyBattleExtra = 9,
        [ProtoEnum(Name="GuildBenefitsSubType_NotAvailable", Value=10)]
        GuildBenefitsSubType_NotAvailable = 10
    }
}

