﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataQuestEnvirmentInstanceInfo")]
    public class ConfigDataQuestEnvirmentInstanceInfo : IExtensible
    {
        private int _ID;
        private int _EnvTypeId;
        private int _FlagSolarSystem;
        private readonly List<QuestEnvirmentInstanceInfoSolarSystemIdList> _SolarSystemIdList;
        private readonly List<QuestEnvirmentInstanceInfoNpcList> _NpcList;
        private readonly List<QuestEnvirmentInstanceInfoCustomStringList> _CustomStringList;
        private readonly List<QuestEnvirmentInstanceInfoMonsterSelectList> _MonsterSelectList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_EnvTypeId;
        private static DelegateBridge __Hotfix_set_EnvTypeId;
        private static DelegateBridge __Hotfix_get_FlagSolarSystem;
        private static DelegateBridge __Hotfix_set_FlagSolarSystem;
        private static DelegateBridge __Hotfix_get_SolarSystemIdList;
        private static DelegateBridge __Hotfix_get_NpcList;
        private static DelegateBridge __Hotfix_get_CustomStringList;
        private static DelegateBridge __Hotfix_get_MonsterSelectList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="EnvTypeId", DataFormat=DataFormat.TwosComplement)]
        public int EnvTypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FlagSolarSystem", DataFormat=DataFormat.TwosComplement)]
        public int FlagSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="SolarSystemIdList", DataFormat=DataFormat.Default)]
        public List<QuestEnvirmentInstanceInfoSolarSystemIdList> SolarSystemIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="NpcList", DataFormat=DataFormat.Default)]
        public List<QuestEnvirmentInstanceInfoNpcList> NpcList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="CustomStringList", DataFormat=DataFormat.Default)]
        public List<QuestEnvirmentInstanceInfoCustomStringList> CustomStringList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="MonsterSelectList", DataFormat=DataFormat.Default)]
        public List<QuestEnvirmentInstanceInfoMonsterSelectList> MonsterSelectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

