﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcInteractionTemplate")]
    public class ConfigDataNpcInteractionTemplate : IExtensible
    {
        private int _ID;
        private int _AttachBuf;
        private readonly List<NpcInteractionTemplateContinuousEffectInfo> _ContinuousEffectInfo;
        private readonly List<NpcInteractionTemplateOnceEffectInfo> _OnceEffectInfo;
        private string _LoopChannelEffect;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_AttachBuf;
        private static DelegateBridge __Hotfix_set_AttachBuf;
        private static DelegateBridge __Hotfix_get_ContinuousEffectInfo;
        private static DelegateBridge __Hotfix_get_OnceEffectInfo;
        private static DelegateBridge __Hotfix_get_LoopChannelEffect;
        private static DelegateBridge __Hotfix_set_LoopChannelEffect;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AttachBuf", DataFormat=DataFormat.TwosComplement)]
        public int AttachBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ContinuousEffectInfo", DataFormat=DataFormat.Default)]
        public List<NpcInteractionTemplateContinuousEffectInfo> ContinuousEffectInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="OnceEffectInfo", DataFormat=DataFormat.Default)]
        public List<NpcInteractionTemplateOnceEffectInfo> OnceEffectInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LoopChannelEffect", DataFormat=DataFormat.Default)]
        public string LoopChannelEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

