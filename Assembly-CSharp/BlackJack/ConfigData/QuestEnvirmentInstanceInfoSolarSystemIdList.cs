﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="QuestEnvirmentInstanceInfoSolarSystemIdList")]
    public class QuestEnvirmentInstanceInfoSolarSystemIdList : IExtensible
    {
        private int _index;
        private int _id;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_index;
        private static DelegateBridge __Hotfix_set_index;
        private static DelegateBridge __Hotfix_get_id;
        private static DelegateBridge __Hotfix_set_id;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="index", DataFormat=DataFormat.TwosComplement)]
        public int index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="id", DataFormat=DataFormat.TwosComplement)]
        public int id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

