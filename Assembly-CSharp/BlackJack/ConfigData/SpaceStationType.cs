﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SpaceStationType")]
    public enum SpaceStationType
    {
        [ProtoEnum(Name="SpaceStationType_Invalid", Value=0)]
        SpaceStationType_Invalid = 0,
        [ProtoEnum(Name="SpaceStationType_TradeCenter", Value=1)]
        SpaceStationType_TradeCenter = 1,
        [ProtoEnum(Name="SpaceStationType_Manufactory", Value=2)]
        SpaceStationType_Manufactory = 2,
        [ProtoEnum(Name="SpaceStationType_SentryPost", Value=3)]
        SpaceStationType_SentryPost = 3,
        [ProtoEnum(Name="SpaceStationType_PlanetLikeBuildingPortal", Value=4)]
        SpaceStationType_PlanetLikeBuildingPortal = 4
    }
}

