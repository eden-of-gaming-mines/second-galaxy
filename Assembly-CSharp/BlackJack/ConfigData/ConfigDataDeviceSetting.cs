﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDeviceSetting")]
    public class ConfigDataDeviceSetting : IExtensible
    {
        private int _ID;
        private string _Name;
        private string _DeviceModel;
        private bool _MarginFixHorizontal;
        private bool _MarginFixVertical;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_DeviceModel;
        private static DelegateBridge __Hotfix_set_DeviceModel;
        private static DelegateBridge __Hotfix_get_MarginFixHorizontal;
        private static DelegateBridge __Hotfix_set_MarginFixHorizontal;
        private static DelegateBridge __Hotfix_get_MarginFixVertical;
        private static DelegateBridge __Hotfix_set_MarginFixVertical;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DeviceModel", DataFormat=DataFormat.Default)]
        public string DeviceModel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MarginFixHorizontal", DataFormat=DataFormat.Default)]
        public bool MarginFixHorizontal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="MarginFixVertical", DataFormat=DataFormat.Default)]
        public bool MarginFixVertical
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

