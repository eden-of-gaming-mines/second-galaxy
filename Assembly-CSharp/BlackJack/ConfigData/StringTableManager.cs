﻿namespace BlackJack.ConfigData
{
    using BlackJack.BJFramework.Runtime.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StringTableManager : StringTableManagerBase, IStringTableProvider
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_InitDefaultStringTable;
        private static DelegateBridge __Hotfix_ClearCurrentLocalizeion;
        private static DelegateBridge __Hotfix_GetStringInDefaultStringTable;
        private static DelegateBridge __Hotfix_GetStringInST;
        private static DelegateBridge __Hotfix_GetStringInGDBStringTable;
        private static DelegateBridge __Hotfix_GetStringInStoryStringTable;
        private static DelegateBridge __Hotfix_GetStringInSST;
        private static DelegateBridge __Hotfix_GetStringInGDBST;
        private static DelegateBridge __Hotfix_ParseStringKey;

        [MethodImpl(0x8000)]
        public StringTableManager(ClientConfigDataLoaderBase configLoader)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearCurrentLocalizeion()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetStringInDefaultStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        private string GetStringInGDBST(string key)
        {
        }

        [MethodImpl(0x8000)]
        public string GetStringInGDBStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        private string GetStringInSST(string key)
        {
        }

        [MethodImpl(0x8000)]
        public string GetStringInST(string key)
        {
        }

        [MethodImpl(0x8000)]
        public string GetStringInStoryStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitDefaultStringTable()
        {
        }

        [MethodImpl(0x8000)]
        private void ParseStringKey(string key, out string stringTableName, out int stringTableId)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetLocalization(string localization)
        {
        }
    }
}

