﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildBenefitsType")]
    public enum GuildBenefitsType
    {
        [ProtoEnum(Name="GuildBenefitsType_Common", Value=1)]
        GuildBenefitsType_Common = 1,
        [ProtoEnum(Name="GuildBenefitsType_Sovereignty", Value=2)]
        GuildBenefitsType_Sovereignty = 2,
        [ProtoEnum(Name="GuildBenefitsType_InformationPoint", Value=3)]
        GuildBenefitsType_InformationPoint = 3
    }
}

