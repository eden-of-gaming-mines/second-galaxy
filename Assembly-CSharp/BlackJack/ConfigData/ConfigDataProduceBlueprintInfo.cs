﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataProduceBlueprintInfo")]
    public class ConfigDataProduceBlueprintInfo : IExtensible
    {
        private int _ID;
        private float _PackedSize;
        private bool _IsBindOnPick;
        private float _SalePrice;
        private readonly List<ConfIdLevelInfo> _DependTechList;
        private PropertyCategory _CaptainPropertyCategory;
        private StoreItemType _ProductType;
        private int _ProductId;
        private int _ProductCount;
        private readonly List<CostInfo> _CostList;
        private int _TimeCost;
        private string _IconResPath;
        private readonly List<ItemObtainSourceType> _ObtainSourceList;
        private int _DevelopProjectId;
        private BlackJack.ConfigData.BlueprintType _BlueprintType;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_PackedSize;
        private static DelegateBridge __Hotfix_set_PackedSize;
        private static DelegateBridge __Hotfix_get_IsBindOnPick;
        private static DelegateBridge __Hotfix_set_IsBindOnPick;
        private static DelegateBridge __Hotfix_get_SalePrice;
        private static DelegateBridge __Hotfix_set_SalePrice;
        private static DelegateBridge __Hotfix_get_DependTechList;
        private static DelegateBridge __Hotfix_get_CaptainPropertyCategory;
        private static DelegateBridge __Hotfix_set_CaptainPropertyCategory;
        private static DelegateBridge __Hotfix_get_ProductType;
        private static DelegateBridge __Hotfix_set_ProductType;
        private static DelegateBridge __Hotfix_get_ProductId;
        private static DelegateBridge __Hotfix_set_ProductId;
        private static DelegateBridge __Hotfix_get_ProductCount;
        private static DelegateBridge __Hotfix_set_ProductCount;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_get_TimeCost;
        private static DelegateBridge __Hotfix_set_TimeCost;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ObtainSourceList;
        private static DelegateBridge __Hotfix_get_DevelopProjectId;
        private static DelegateBridge __Hotfix_set_DevelopProjectId;
        private static DelegateBridge __Hotfix_get_BlueprintType;
        private static DelegateBridge __Hotfix_set_BlueprintType;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PackedSize", DataFormat=DataFormat.FixedSize)]
        public float PackedSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="IsBindOnPick", DataFormat=DataFormat.Default)]
        public bool IsBindOnPick
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SalePrice", DataFormat=DataFormat.FixedSize)]
        public float SalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="DependTechList", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> DependTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="CaptainPropertyCategory", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory CaptainPropertyCategory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="ProductType", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType ProductType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="ProductId", DataFormat=DataFormat.TwosComplement)]
        public int ProductId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="ProductCount", DataFormat=DataFormat.TwosComplement)]
        public int ProductCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, Name="CostList", DataFormat=DataFormat.Default)]
        public List<CostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="TimeCost", DataFormat=DataFormat.TwosComplement)]
        public int TimeCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, Name="ObtainSourceList", DataFormat=DataFormat.TwosComplement)]
        public List<ItemObtainSourceType> ObtainSourceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="DevelopProjectId", DataFormat=DataFormat.TwosComplement)]
        public int DevelopProjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="BlueprintType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.BlueprintType BlueprintType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

