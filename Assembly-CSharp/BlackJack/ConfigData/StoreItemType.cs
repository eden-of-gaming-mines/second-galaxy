﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="StoreItemType")]
    public enum StoreItemType
    {
        [ProtoEnum(Name="StoreItemType_Normal", Value=1)]
        StoreItemType_Normal = 1,
        [ProtoEnum(Name="StoreItemType_Weapon", Value=2)]
        StoreItemType_Weapon = 2,
        [ProtoEnum(Name="StoreItemType_Equip", Value=3)]
        StoreItemType_Equip = 3,
        [ProtoEnum(Name="StoreItemType_Ship", Value=4)]
        StoreItemType_Ship = 4,
        [ProtoEnum(Name="StoreItemType_NormalAmmo", Value=5)]
        StoreItemType_NormalAmmo = 5,
        [ProtoEnum(Name="StoreItemType_MissileAmmo", Value=6)]
        StoreItemType_MissileAmmo = 6,
        [ProtoEnum(Name="StoreItemType_DroneAmmo", Value=7)]
        StoreItemType_DroneAmmo = 7,
        [ProtoEnum(Name="StoreItemType_Currency", Value=8)]
        StoreItemType_Currency = 8,
        [ProtoEnum(Name="StoreItemType_Mineral", Value=9)]
        StoreItemType_Mineral = 9,
        [ProtoEnum(Name="StoreItemType_Chip", Value=10)]
        StoreItemType_Chip = 10,
        [ProtoEnum(Name="StoreItemType_FeatsBook", Value=11)]
        StoreItemType_FeatsBook = 11,
        [ProtoEnum(Name="StoreItemType_ProduceBlueprint", Value=12)]
        StoreItemType_ProduceBlueprint = 12,
        [ProtoEnum(Name="StoreItemType_CrackBox", Value=13)]
        StoreItemType_CrackBox = 13,
        [ProtoEnum(Name="StoreItemType_QuestItem", Value=14)]
        StoreItemType_QuestItem = 14,
        [ProtoEnum(Name="StoreItemType_GuildProduceTemplete", Value=15)]
        StoreItemType_GuildProduceTemplete = 15,
        [ProtoEnum(Name="StoreItemType_Max", Value=0x10)]
        StoreItemType_Max = 0x10
    }
}

