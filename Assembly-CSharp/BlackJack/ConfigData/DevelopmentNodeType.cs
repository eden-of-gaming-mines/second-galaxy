﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DevelopmentNodeType")]
    public enum DevelopmentNodeType
    {
        [ProtoEnum(Name="DevelopmentNodeType_Invalid", Value=0)]
        DevelopmentNodeType_Invalid = 0,
        [ProtoEnum(Name="DevelopmentNodeType_Ship", Value=1)]
        DevelopmentNodeType_Ship = 1,
        [ProtoEnum(Name="DevelopmentNodeType_Weapon", Value=2)]
        DevelopmentNodeType_Weapon = 2,
        [ProtoEnum(Name="DevelopmentNodeType_Device", Value=3)]
        DevelopmentNodeType_Device = 3,
        [ProtoEnum(Name="DevelopmentNodeType_Ammo", Value=4)]
        DevelopmentNodeType_Ammo = 4,
        [ProtoEnum(Name="DevelopmentNodeType_Component", Value=5)]
        DevelopmentNodeType_Component = 5,
        [ProtoEnum(Name="DevelopmentNodeType_Chip", Value=6)]
        DevelopmentNodeType_Chip = 6,
        [ProtoEnum(Name="DevelopmentNodeType_Frigate", Value=7)]
        DevelopmentNodeType_Frigate = 7,
        [ProtoEnum(Name="DevelopmentNodeType_Destroyer", Value=8)]
        DevelopmentNodeType_Destroyer = 8,
        [ProtoEnum(Name="DevelopmentNodeType_Cruiser", Value=9)]
        DevelopmentNodeType_Cruiser = 9,
        [ProtoEnum(Name="DevelopmentNodeType_BattleCruiser", Value=10)]
        DevelopmentNodeType_BattleCruiser = 10,
        [ProtoEnum(Name="DevelopmentNodeType_BattleShip", Value=11)]
        DevelopmentNodeType_BattleShip = 11,
        [ProtoEnum(Name="DevelopmentNodeType_N", Value=12)]
        DevelopmentNodeType_N = 12,
        [ProtoEnum(Name="DevelopmentNodeType_S", Value=13)]
        DevelopmentNodeType_S = 13,
        [ProtoEnum(Name="DevelopmentNodeType_M", Value=14)]
        DevelopmentNodeType_M = 14,
        [ProtoEnum(Name="DevelopmentNodeType_L", Value=15)]
        DevelopmentNodeType_L = 15,
        [ProtoEnum(Name="DevelopmentNodeType_Attack", Value=0x10)]
        DevelopmentNodeType_Attack = 0x10,
        [ProtoEnum(Name="DevelopmentNodeType_Defence", Value=0x11)]
        DevelopmentNodeType_Defence = 0x11,
        [ProtoEnum(Name="DevelopmentNodeType_Electron", Value=0x12)]
        DevelopmentNodeType_Electron = 0x12,
        [ProtoEnum(Name="DevelopmentNodeType_Drive", Value=0x13)]
        DevelopmentNodeType_Drive = 0x13,
        [ProtoEnum(Name="DevelopmentNodeType_RapidPrototype", Value=20)]
        DevelopmentNodeType_RapidPrototype = 20,
        [ProtoEnum(Name="DevelopmentNodeType_Stagewise", Value=0x15)]
        DevelopmentNodeType_Stagewise = 0x15,
        [ProtoEnum(Name="DevelopmentNodeType_Spiral", Value=0x16)]
        DevelopmentNodeType_Spiral = 0x16,
        [ProtoEnum(Name="DevelopmentNodeType_Fountain", Value=0x17)]
        DevelopmentNodeType_Fountain = 0x17,
        [ProtoEnum(Name="DevelopmentNodeType_Evolutionary", Value=0x18)]
        DevelopmentNodeType_Evolutionary = 0x18,
        [ProtoEnum(Name="DevelopmentNodeType_4GL", Value=0x19)]
        DevelopmentNodeType_4GL = 0x19,
        [ProtoEnum(Name="DevelopmentNodeType_Max", Value=0x1a)]
        DevelopmentNodeType_Max = 0x1a
    }
}

