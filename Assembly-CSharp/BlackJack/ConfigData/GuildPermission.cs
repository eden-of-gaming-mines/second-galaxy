﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildPermission")]
    public enum GuildPermission
    {
        [ProtoEnum(Name="GuildPermission_None", Value=0)]
        GuildPermission_None = 0,
        [ProtoEnum(Name="GuildPermission_JoinApplyAudit", Value=1)]
        GuildPermission_JoinApplyAudit = 1,
        [ProtoEnum(Name="GuildPermission_RemoveMember", Value=2)]
        GuildPermission_RemoveMember = 2,
        [ProtoEnum(Name="GuildPermission_ChangeGuildBasicInfo", Value=3)]
        GuildPermission_ChangeGuildBasicInfo = 3,
        [ProtoEnum(Name="GuildPermission_DismissGuild", Value=4)]
        GuildPermission_DismissGuild = 4,
        [ProtoEnum(Name="GuildPermission_DiplomacyUpdate", Value=5)]
        GuildPermission_DiplomacyUpdate = 5,
        [ProtoEnum(Name="GuildPermission_MailSend", Value=6)]
        GuildPermission_MailSend = 6,
        [ProtoEnum(Name="GuildPermission_BaseSolarSystemSet", Value=7)]
        GuildPermission_BaseSolarSystemSet = 7,
        [ProtoEnum(Name="GuildPermission_TransferGuildLeader", Value=8)]
        GuildPermission_TransferGuildLeader = 8,
        [ProtoEnum(Name="GuildPermission_AppointManager", Value=9)]
        GuildPermission_AppointManager = 9,
        [ProtoEnum(Name="GuildPermission_AppointHROfficer", Value=10)]
        GuildPermission_AppointHROfficer = 10,
        [ProtoEnum(Name="GuildPermission_AppointSupportOfficer", Value=11)]
        GuildPermission_AppointSupportOfficer = 11,
        [ProtoEnum(Name="GuildPermission_AppointDiplomatOfficer", Value=12)]
        GuildPermission_AppointDiplomatOfficer = 12,
        [ProtoEnum(Name="GuildPermission_AppointFleetOfficer", Value=13)]
        GuildPermission_AppointFleetOfficer = 13,
        [ProtoEnum(Name="GuildPermission_GuildMessageAdmin", Value=14)]
        GuildPermission_GuildMessageAdmin = 14,
        [ProtoEnum(Name="GuildPermission_GuildMomentsAdmin", Value=15)]
        GuildPermission_GuildMomentsAdmin = 15,
        [ProtoEnum(Name="GuildPermission_GuildActionAdmin", Value=0x10)]
        GuildPermission_GuildActionAdmin = 0x10,
        [ProtoEnum(Name="GuildPermission_GuildProductionAdmin", Value=0x11)]
        GuildPermission_GuildProductionAdmin = 0x11,
        [ProtoEnum(Name="GuildPermission_GuildBuildingAdmin", Value=0x12)]
        GuildPermission_GuildBuildingAdmin = 0x12,
        [ProtoEnum(Name="GuildPermission_GuildSuppliesManager", Value=0x13)]
        GuildPermission_GuildSuppliesManager = 0x13,
        [ProtoEnum(Name="GuildPermission_GuildShipConfig", Value=20)]
        GuildPermission_GuildShipConfig = 20,
        [ProtoEnum(Name="GuildPermission_FleetCreate", Value=0x15)]
        GuildPermission_FleetCreate = 0x15,
        [ProtoEnum(Name="GuildPermission_FleetDismiss", Value=0x16)]
        GuildPermission_FleetDismiss = 0x16,
        [ProtoEnum(Name="GuildPermission_FleetRename", Value=0x17)]
        GuildPermission_FleetRename = 0x17,
        [ProtoEnum(Name="GuildPermission_FleetSetCommander", Value=0x18)]
        GuildPermission_FleetSetCommander = 0x18,
        [ProtoEnum(Name="GuildPermission_DeclareWar", Value=0x19)]
        GuildPermission_DeclareWar = 0x19,
        [ProtoEnum(Name="GuildPermission_CreateGuildConfig", Value=0x1a)]
        GuildPermission_CreateGuildConfig = 0x1a,
        [ProtoEnum(Name="GuildPermission_GuildPurchaseOrderManage", Value=0x1b)]
        GuildPermission_GuildPurchaseOrderManage = 0x1b,
        [ProtoEnum(Name="GuildPermission_GuildResourceView", Value=0x1c)]
        GuildPermission_GuildResourceView = 0x1c,
        [ProtoEnum(Name="GuildPermission_FleetSetInternalPosition", Value=0x1d)]
        GuildPermission_FleetSetInternalPosition = 0x1d,
        [ProtoEnum(Name="GuildPermission_GuildMiningAdmin", Value=30)]
        GuildPermission_GuildMiningAdmin = 30,
        [ProtoEnum(Name="GuildPermission_ChangeAnnouncementManifesto", Value=0x1f)]
        GuildPermission_ChangeAnnouncementManifesto = 0x1f,
        [ProtoEnum(Name="GuildPermission_BattleSet", Value=0x20)]
        GuildPermission_BattleSet = 0x20,
        [ProtoEnum(Name="GuildPermission_FlagShipManage", Value=0x21)]
        GuildPermission_FlagShipManage = 0x21,
        [ProtoEnum(Name="GuildPermission_FlagShipDrive", Value=0x22)]
        GuildPermission_FlagShipDrive = 0x22,
        [ProtoEnum(Name="GuildPermission_GuildTradeManager", Value=0x23)]
        GuildPermission_GuildTradeManager = 0x23,
        [ProtoEnum(Name="GuildPermission_AntiTeleportActivate", Value=0x24)]
        GuildPermission_AntiTeleportActivate = 0x24,
        [ProtoEnum(Name="GuildPermission_Max", Value=0x25)]
        GuildPermission_Max = 0x25
    }
}

