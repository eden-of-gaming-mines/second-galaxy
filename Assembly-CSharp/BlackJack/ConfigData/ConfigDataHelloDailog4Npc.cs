﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataHelloDailog4Npc")]
    public class ConfigDataHelloDailog4Npc : IExtensible
    {
        private int _ID;
        private readonly List<int> _DailogIdList;
        private readonly List<int> _GreetingAudioNameLIST;
        private readonly List<int> _LeaveTalkAudioNameLIST;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_DailogIdList;
        private static DelegateBridge __Hotfix_get_GreetingAudioNameLIST;
        private static DelegateBridge __Hotfix_get_LeaveTalkAudioNameLIST;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="DailogIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DailogIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="GreetingAudioNameLIST", DataFormat=DataFormat.TwosComplement)]
        public List<int> GreetingAudioNameLIST
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="LeaveTalkAudioNameLIST", DataFormat=DataFormat.TwosComplement)]
        public List<int> LeaveTalkAudioNameLIST
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

