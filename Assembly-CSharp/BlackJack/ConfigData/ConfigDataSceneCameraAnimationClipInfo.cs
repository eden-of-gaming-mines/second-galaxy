﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSceneCameraAnimationClipInfo")]
    public class ConfigDataSceneCameraAnimationClipInfo : IExtensible
    {
        private int _ID;
        private string _ClipResString;
        private readonly List<SceneCameraAnimationClipFilterType> _FilterTypeList;
        private readonly List<string> _FilterStringList;
        private bool _DisableSceneLight;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ClipResString;
        private static DelegateBridge __Hotfix_set_ClipResString;
        private static DelegateBridge __Hotfix_get_FilterTypeList;
        private static DelegateBridge __Hotfix_get_FilterStringList;
        private static DelegateBridge __Hotfix_get_DisableSceneLight;
        private static DelegateBridge __Hotfix_set_DisableSceneLight;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ClipResString", DataFormat=DataFormat.Default)]
        public string ClipResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="FilterTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<SceneCameraAnimationClipFilterType> FilterTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="FilterStringList", DataFormat=DataFormat.Default)]
        public List<string> FilterStringList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DisableSceneLight", DataFormat=DataFormat.Default)]
        public bool DisableSceneLight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

