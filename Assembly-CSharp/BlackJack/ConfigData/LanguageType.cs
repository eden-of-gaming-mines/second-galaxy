﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="LanguageType")]
    public enum LanguageType
    {
        [ProtoEnum(Name="LanguageType_Base", Value=1)]
        LanguageType_Base = 1,
        [ProtoEnum(Name="LanguageType_English", Value=2)]
        LanguageType_English = 2,
        [ProtoEnum(Name="LanguageType_Chinese", Value=3)]
        LanguageType_Chinese = 3,
        [ProtoEnum(Name="LanguageType_Russian", Value=4)]
        LanguageType_Russian = 4,
        [ProtoEnum(Name="LanguageType_Japanese", Value=5)]
        LanguageType_Japanese = 5,
        [ProtoEnum(Name="LanguageType_French", Value=6)]
        LanguageType_French = 6,
        [ProtoEnum(Name="LanguageType_German", Value=7)]
        LanguageType_German = 7,
        [ProtoEnum(Name="LanguageType_Portuguese", Value=8)]
        LanguageType_Portuguese = 8,
        [ProtoEnum(Name="LanguageType_Spanish", Value=9)]
        LanguageType_Spanish = 9,
        [ProtoEnum(Name="LanguageType_TaiWan", Value=10)]
        LanguageType_TaiWan = 10,
        [ProtoEnum(Name="LanguageType_Max", Value=11)]
        LanguageType_Max = 11
    }
}

