﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcCaptainShipGrowthLineInfo")]
    public class ConfigDataNpcCaptainShipGrowthLineInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.ShipType _ShipType;
        private readonly List<int> _ShipList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ShipType;
        private static DelegateBridge __Hotfix_set_ShipType;
        private static DelegateBridge __Hotfix_get_ShipList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.ShipType ShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ShipList", DataFormat=DataFormat.TwosComplement)]
        public List<int> ShipList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

