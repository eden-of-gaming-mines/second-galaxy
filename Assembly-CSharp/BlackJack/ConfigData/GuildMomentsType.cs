﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildMomentsType")]
    public enum GuildMomentsType
    {
        [ProtoEnum(Name="GuildMomentsType_Admin", Value=1)]
        GuildMomentsType_Admin = 1,
        [ProtoEnum(Name="GuildMomentsType_Donate", Value=2)]
        GuildMomentsType_Donate = 2,
        [ProtoEnum(Name="GuildMomentsType_Action", Value=3)]
        GuildMomentsType_Action = 3,
        [ProtoEnum(Name="GuildMomentsType_Trade", Value=4)]
        GuildMomentsType_Trade = 4,
        [ProtoEnum(Name="GuildMomentsType_Produce", Value=5)]
        GuildMomentsType_Produce = 5,
        [ProtoEnum(Name="GuildMomentsType_Development", Value=6)]
        GuildMomentsType_Development = 6,
        [ProtoEnum(Name="GuildMomentsType_Battle", Value=7)]
        GuildMomentsType_Battle = 7
    }
}

