﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ProfessionType")]
    public enum ProfessionType
    {
        [ProtoEnum(Name="ProfessionType_Engineer", Value=1)]
        ProfessionType_Engineer = 1,
        [ProtoEnum(Name="ProfessionType_Scientist", Value=2)]
        ProfessionType_Scientist = 2,
        [ProtoEnum(Name="ProfessionType_Soldier", Value=3)]
        ProfessionType_Soldier = 3,
        [ProtoEnum(Name="ProfessionType_Explorer", Value=4)]
        ProfessionType_Explorer = 4,
        [ProtoEnum(Name="ProfessionType_Max", Value=5)]
        ProfessionType_Max = 5
    }
}

