﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BlackMarketShopItemType")]
    public enum BlackMarketShopItemType
    {
        [ProtoEnum(Name="BlackMarketShopItemType_PlayerTradeMoney", Value=1)]
        BlackMarketShopItemType_PlayerTradeMoney = 1,
        [ProtoEnum(Name="BlackMarketShopItemType_PlayerBindMoney", Value=2)]
        BlackMarketShopItemType_PlayerBindMoney = 2,
        [ProtoEnum(Name="BlackMarketShopItemType_PlayerMineral", Value=3)]
        BlackMarketShopItemType_PlayerMineral = 3,
        [ProtoEnum(Name="BlackMarketShopItemType_GuildTacticalPoint", Value=4)]
        BlackMarketShopItemType_GuildTacticalPoint = 4,
        [ProtoEnum(Name="BlackMarketShopItemType_GuildMineral", Value=5)]
        BlackMarketShopItemType_GuildMineral = 5,
        [ProtoEnum(Name="BlackMarketShopItemType_NormalItem", Value=6)]
        BlackMarketShopItemType_NormalItem = 6,
        [ProtoEnum(Name="BlackMarketShopItemType_Max", Value=7)]
        BlackMarketShopItemType_Max = 7
    }
}

