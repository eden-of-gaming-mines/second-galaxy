﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="AgeType")]
    public enum AgeType
    {
        [ProtoEnum(Name="AgeType_Yong", Value=1)]
        AgeType_Yong = 1,
        [ProtoEnum(Name="AgeType_Middle", Value=2)]
        AgeType_Middle = 2,
        [ProtoEnum(Name="AgeType_Old", Value=3)]
        AgeType_Old = 3
    }
}

