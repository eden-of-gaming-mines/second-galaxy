﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPredefineMailInfo")]
    public class ConfigDataPredefineMailInfo : IExtensible
    {
        private int _ID;
        private readonly List<CommonFormatStringParamType> _TitleFormatStringParamTypeList;
        private readonly List<CommonFormatStringParamType> _ContentFormatStringParamTypeList;
        private MailDeleteType _DeleteType;
        private string _TitleStrKey;
        private string _ContentStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_TitleFormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_ContentFormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_DeleteType;
        private static DelegateBridge __Hotfix_set_DeleteType;
        private static DelegateBridge __Hotfix_get_TitleStrKey;
        private static DelegateBridge __Hotfix_set_TitleStrKey;
        private static DelegateBridge __Hotfix_get_ContentStrKey;
        private static DelegateBridge __Hotfix_set_ContentStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="TitleFormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> TitleFormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="ContentFormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> ContentFormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DeleteType", DataFormat=DataFormat.TwosComplement)]
        public MailDeleteType DeleteType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="TitleStrKey", DataFormat=DataFormat.Default)]
        public string TitleStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="ContentStrKey", DataFormat=DataFormat.Default)]
        public string ContentStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

