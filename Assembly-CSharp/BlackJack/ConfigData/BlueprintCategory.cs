﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="BlueprintCategory")]
    public enum BlueprintCategory
    {
        [ProtoEnum(Name="BlueprintCategory_Invalid", Value=0)]
        BlueprintCategory_Invalid = 0,
        [ProtoEnum(Name="BlueprintCategory_ShipBlueprint", Value=1)]
        BlueprintCategory_ShipBlueprint = 1,
        [ProtoEnum(Name="BlueprintCategory_WeaponBlueprint", Value=2)]
        BlueprintCategory_WeaponBlueprint = 2,
        [ProtoEnum(Name="BlueprintCategory_AmmoBlueprint", Value=3)]
        BlueprintCategory_AmmoBlueprint = 3,
        [ProtoEnum(Name="BlueprintCategory_DeviceBlueprint", Value=4)]
        BlueprintCategory_DeviceBlueprint = 4,
        [ProtoEnum(Name="BlueprintCategory_ComponentBlueprint", Value=5)]
        BlueprintCategory_ComponentBlueprint = 5,
        [ProtoEnum(Name="BlueprintCategory_OtherBlueprint", Value=6)]
        BlueprintCategory_OtherBlueprint = 6,
        [ProtoEnum(Name="BlueprintCategory_Max", Value=7)]
        BlueprintCategory_Max = 7
    }
}

