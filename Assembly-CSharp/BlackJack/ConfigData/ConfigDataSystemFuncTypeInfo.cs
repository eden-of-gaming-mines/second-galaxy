﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSystemFuncTypeInfo")]
    public class ConfigDataSystemFuncTypeInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.SystemFuncType _SystemFuncType;
        private float _OpenEffectTime;
        private int _ErrorCode;
        private int _UnlockLevel;
        private int _UnlockQuestId;
        private string _LockTipsKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SystemFuncType;
        private static DelegateBridge __Hotfix_set_SystemFuncType;
        private static DelegateBridge __Hotfix_get_OpenEffectTime;
        private static DelegateBridge __Hotfix_set_OpenEffectTime;
        private static DelegateBridge __Hotfix_get_ErrorCode;
        private static DelegateBridge __Hotfix_set_ErrorCode;
        private static DelegateBridge __Hotfix_get_UnlockLevel;
        private static DelegateBridge __Hotfix_set_UnlockLevel;
        private static DelegateBridge __Hotfix_get_UnlockQuestId;
        private static DelegateBridge __Hotfix_set_UnlockQuestId;
        private static DelegateBridge __Hotfix_get_LockTipsKey;
        private static DelegateBridge __Hotfix_set_LockTipsKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SystemFuncType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SystemFuncType SystemFuncType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="OpenEffectTime", DataFormat=DataFormat.FixedSize)]
        public float OpenEffectTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ErrorCode", DataFormat=DataFormat.TwosComplement)]
        public int ErrorCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="UnlockLevel", DataFormat=DataFormat.TwosComplement)]
        public int UnlockLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="UnlockQuestId", DataFormat=DataFormat.TwosComplement)]
        public int UnlockQuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="LockTipsKey", DataFormat=DataFormat.Default)]
        public string LockTipsKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

