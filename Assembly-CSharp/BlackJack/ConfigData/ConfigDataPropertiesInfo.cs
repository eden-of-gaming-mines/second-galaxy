﻿namespace BlackJack.ConfigData
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataPropertiesInfo")]
    public class ConfigDataPropertiesInfo : IExtensible, IPropertiesInfo
    {
        private int _ID;
        private PropertyCategory _Category;
        private int _Group;
        private int _Algorithm;
        private bool _Visiable;
        private int _SortId;
        private PropertyViewType _ViewType;
        private bool _PropertyActivate;
        private bool _IsNegative;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_Group;
        private static DelegateBridge __Hotfix_set_Group;
        private static DelegateBridge __Hotfix_get_Algorithm;
        private static DelegateBridge __Hotfix_set_Algorithm;
        private static DelegateBridge __Hotfix_get_Visiable;
        private static DelegateBridge __Hotfix_set_Visiable;
        private static DelegateBridge __Hotfix_get_SortId;
        private static DelegateBridge __Hotfix_set_SortId;
        private static DelegateBridge __Hotfix_get_ViewType;
        private static DelegateBridge __Hotfix_set_ViewType;
        private static DelegateBridge __Hotfix_get_PropertyActivate;
        private static DelegateBridge __Hotfix_set_PropertyActivate;
        private static DelegateBridge __Hotfix_get_IsNegative;
        private static DelegateBridge __Hotfix_set_IsNegative;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesInfo.get_PropertyId;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesInfo.get_PropertyGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesInfo.get_CalcAlgorithm;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesInfo.get_IsPropertyActive;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        int IPropertiesInfo.PropertyId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        int IPropertiesInfo.PropertyGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        PropertiesALG IPropertiesInfo.CalcAlgorithm
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        bool IPropertiesInfo.IsPropertyActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public PropertyCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Group", DataFormat=DataFormat.TwosComplement)]
        public int Group
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Algorithm", DataFormat=DataFormat.TwosComplement)]
        public int Algorithm
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Visiable", DataFormat=DataFormat.Default)]
        public bool Visiable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SortId", DataFormat=DataFormat.TwosComplement)]
        public int SortId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ViewType", DataFormat=DataFormat.TwosComplement)]
        public PropertyViewType ViewType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="PropertyActivate", DataFormat=DataFormat.Default)]
        public bool PropertyActivate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="IsNegative", DataFormat=DataFormat.Default)]
        public bool IsNegative
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

