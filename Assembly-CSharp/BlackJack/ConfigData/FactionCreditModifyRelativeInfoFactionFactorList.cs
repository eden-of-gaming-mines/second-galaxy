﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="FactionCreditModifyRelativeInfoFactionFactorList")]
    public class FactionCreditModifyRelativeInfoFactionFactorList : IExtensible
    {
        private int _factionId;
        private float _factor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_factionId;
        private static DelegateBridge __Hotfix_set_factionId;
        private static DelegateBridge __Hotfix_get_factor;
        private static DelegateBridge __Hotfix_set_factor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="factionId", DataFormat=DataFormat.TwosComplement)]
        public int factionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="factor", DataFormat=DataFormat.FixedSize)]
        public float factor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

