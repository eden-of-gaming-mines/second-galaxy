﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildBuildingLevelDetailInfo")]
    public class ConfigDataGuildBuildingLevelDetailInfo : IExtensible
    {
        private int _ID;
        private int _Level;
        private string _Desc;
        private int _SolarSystemFlourishLevel;
        private readonly List<ConfIdLevelInfo> _BuildingRequire;
        private int _DeployItemId;
        private int _DeployTime;
        private int _RecycleTime;
        private readonly List<GuildBuildingLevelDetailInfoRecycleItem> _RecycleItem;
        private int _NpcMonsterId;
        private int _NpcMonsterShieldMax;
        private int _NpcMonsterArmorMax;
        private readonly List<GuildBuildingLevelDetailInfoGuildLogicEffectList> _GuildLogicEffectList;
        private int _InvadeNpcMonsterId;
        private long _InvadeTradeMoneyCost;
        private string _IconResPath;
        private string _ModelInStarMapResPath;
        private string _BigDisplayIconResPath;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_Desc;
        private static DelegateBridge __Hotfix_set_Desc;
        private static DelegateBridge __Hotfix_get_SolarSystemFlourishLevel;
        private static DelegateBridge __Hotfix_set_SolarSystemFlourishLevel;
        private static DelegateBridge __Hotfix_get_BuildingRequire;
        private static DelegateBridge __Hotfix_get_DeployItemId;
        private static DelegateBridge __Hotfix_set_DeployItemId;
        private static DelegateBridge __Hotfix_get_DeployTime;
        private static DelegateBridge __Hotfix_set_DeployTime;
        private static DelegateBridge __Hotfix_get_RecycleTime;
        private static DelegateBridge __Hotfix_set_RecycleTime;
        private static DelegateBridge __Hotfix_get_RecycleItem;
        private static DelegateBridge __Hotfix_get_NpcMonsterId;
        private static DelegateBridge __Hotfix_set_NpcMonsterId;
        private static DelegateBridge __Hotfix_get_NpcMonsterShieldMax;
        private static DelegateBridge __Hotfix_set_NpcMonsterShieldMax;
        private static DelegateBridge __Hotfix_get_NpcMonsterArmorMax;
        private static DelegateBridge __Hotfix_set_NpcMonsterArmorMax;
        private static DelegateBridge __Hotfix_get_GuildLogicEffectList;
        private static DelegateBridge __Hotfix_get_InvadeNpcMonsterId;
        private static DelegateBridge __Hotfix_set_InvadeNpcMonsterId;
        private static DelegateBridge __Hotfix_get_InvadeTradeMoneyCost;
        private static DelegateBridge __Hotfix_set_InvadeTradeMoneyCost;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ModelInStarMapResPath;
        private static DelegateBridge __Hotfix_set_ModelInStarMapResPath;
        private static DelegateBridge __Hotfix_get_BigDisplayIconResPath;
        private static DelegateBridge __Hotfix_set_BigDisplayIconResPath;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Desc", DataFormat=DataFormat.Default)]
        public string Desc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SolarSystemFlourishLevel", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemFlourishLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="BuildingRequire", DataFormat=DataFormat.Default)]
        public List<ConfIdLevelInfo> BuildingRequire
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DeployItemId", DataFormat=DataFormat.TwosComplement)]
        public int DeployItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="DeployTime", DataFormat=DataFormat.TwosComplement)]
        public int DeployTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="RecycleTime", DataFormat=DataFormat.TwosComplement)]
        public int RecycleTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="RecycleItem", DataFormat=DataFormat.Default)]
        public List<GuildBuildingLevelDetailInfoRecycleItem> RecycleItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NpcMonsterId", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NpcMonsterShieldMax", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="NpcMonsterArmorMax", DataFormat=DataFormat.TwosComplement)]
        public int NpcMonsterArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, Name="GuildLogicEffectList", DataFormat=DataFormat.Default)]
        public List<GuildBuildingLevelDetailInfoGuildLogicEffectList> GuildLogicEffectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="InvadeNpcMonsterId", DataFormat=DataFormat.TwosComplement)]
        public int InvadeNpcMonsterId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="InvadeTradeMoneyCost", DataFormat=DataFormat.TwosComplement)]
        public long InvadeTradeMoneyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="ModelInStarMapResPath", DataFormat=DataFormat.Default)]
        public string ModelInStarMapResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="BigDisplayIconResPath", DataFormat=DataFormat.Default)]
        public string BigDisplayIconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

