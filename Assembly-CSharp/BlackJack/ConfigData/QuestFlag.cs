﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestFlag")]
    public enum QuestFlag
    {
        [ProtoEnum(Name="QuestFlag_AutoAccept", Value=2)]
        QuestFlag_AutoAccept = 2,
        [ProtoEnum(Name="QuestFlag_SkipAcceptPanel", Value=4)]
        QuestFlag_SkipAcceptPanel = 4,
        [ProtoEnum(Name="QuestFlag_AcceptDialogDelay", Value=8)]
        QuestFlag_AcceptDialogDelay = 8,
        [ProtoEnum(Name="QuestFlag_CanNotCancel", Value=0x10)]
        QuestFlag_CanNotCancel = 0x10,
        [ProtoEnum(Name="QuestFlag_CondDialogAutoLaunch", Value=0x20)]
        QuestFlag_CondDialogAutoLaunch = 0x20,
        [ProtoEnum(Name="QuestFlag_CancelIfLeaveScene", Value=0x40)]
        QuestFlag_CancelIfLeaveScene = 0x40,
        [ProtoEnum(Name="QuestFlag_RecordComplete", Value=0x80)]
        QuestFlag_RecordComplete = 0x80,
        [ProtoEnum(Name="QuestFlag_InheritBindScene", Value=0x100)]
        QuestFlag_InheritBindScene = 0x100,
        [ProtoEnum(Name="QuestFlag_CancelIfFail", Value=0x200)]
        QuestFlag_CancelIfFail = 0x200,
        [ProtoEnum(Name="QuestFlag_DisableReturnStationNotice", Value=0x400)]
        QuestFlag_DisableReturnStationNotice = 0x400,
        [ProtoEnum(Name="QuestFlag_NotShowQuestTargetOnUI", Value=0x800)]
        QuestFlag_NotShowQuestTargetOnUI = 0x800,
        [ProtoEnum(Name="QuestFlag_ReDeployMotherShip", Value=0x1000)]
        QuestFlag_ReDeployMotherShip = 0x1000,
        [ProtoEnum(Name="QuestFlag_NotShowQuestCompleteEffect", Value=0x2000)]
        QuestFlag_NotShowQuestCompleteEffect = 0x2000,
        [ProtoEnum(Name="QuestFlag_RetryOnFail", Value=0x4000)]
        QuestFlag_RetryOnFail = 0x4000,
        [ProtoEnum(Name="QuestFlag_SaveDataOnQuestComplete", Value=0x8000)]
        QuestFlag_SaveDataOnQuestComplete = 0x8000,
        [ProtoEnum(Name="QuestFlag_HideToPlayer", Value=0x10000)]
        QuestFlag_HideToPlayer = 0x10000
    }
}

