﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="CommonFormatStringParamType")]
    public enum CommonFormatStringParamType
    {
        [ProtoEnum(Name="CommonFormatStringParamType_CurrSolarSystemName", Value=1)]
        CommonFormatStringParamType_CurrSolarSystemName = 1,
        [ProtoEnum(Name="CommonFormatStringParamType_CurrPlayerName", Value=2)]
        CommonFormatStringParamType_CurrPlayerName = 2,
        [ProtoEnum(Name="CommonFormatStringParamType_ShipName", Value=3)]
        CommonFormatStringParamType_ShipName = 3,
        [ProtoEnum(Name="CommonFormatStringParamType_ItemName", Value=4)]
        CommonFormatStringParamType_ItemName = 4,
        [ProtoEnum(Name="CommonFormatStringParamType_SolarSystemName", Value=5)]
        CommonFormatStringParamType_SolarSystemName = 5,
        [ProtoEnum(Name="CommonFormatStringParamType_EnvCustomString", Value=6)]
        CommonFormatStringParamType_EnvCustomString = 6,
        [ProtoEnum(Name="CommonFormatStringParamType_QuestAcceptStaionName", Value=7)]
        CommonFormatStringParamType_QuestAcceptStaionName = 7,
        [ProtoEnum(Name="CommonFormatStringParamType_QuestCompleteStationName", Value=8)]
        CommonFormatStringParamType_QuestCompleteStationName = 8,
        [ProtoEnum(Name="CommonFormatStringParamType_InterNum", Value=9)]
        CommonFormatStringParamType_InterNum = 9,
        [ProtoEnum(Name="CommonFormatStringParamType_FloatNum", Value=10)]
        CommonFormatStringParamType_FloatNum = 10,
        [ProtoEnum(Name="CommonFormatStringParamType_GlobalNpcName", Value=11)]
        CommonFormatStringParamType_GlobalNpcName = 11,
        [ProtoEnum(Name="CommonFormatStringParamType_StationNpcName", Value=12)]
        CommonFormatStringParamType_StationNpcName = 12,
        [ProtoEnum(Name="CommonFormatStringParamType_ServerStringValue", Value=13)]
        CommonFormatStringParamType_ServerStringValue = 13,
        [ProtoEnum(Name="CommonFormatStringParamType_QuestAcceptSolarSystemName", Value=14)]
        CommonFormatStringParamType_QuestAcceptSolarSystemName = 14,
        [ProtoEnum(Name="CommonFormatStringParamType_QuestCompleteSolarSystemName", Value=15)]
        CommonFormatStringParamType_QuestCompleteSolarSystemName = 15,
        [ProtoEnum(Name="CommonFormatStringParamType_QuestNameByConfigId", Value=0x10)]
        CommonFormatStringParamType_QuestNameByConfigId = 0x10,
        [ProtoEnum(Name="CommonFormatStringParamType_HiredCaptainName", Value=0x11)]
        CommonFormatStringParamType_HiredCaptainName = 0x11,
        [ProtoEnum(Name="CommonFormatStringParamType_SpaceStationName", Value=0x12)]
        CommonFormatStringParamType_SpaceStationName = 0x12,
        [ProtoEnum(Name="CommonFormatStringParamType_SkillName", Value=0x13)]
        CommonFormatStringParamType_SkillName = 0x13,
        [ProtoEnum(Name="CommonFormatStringParamType_GuildJobName", Value=20)]
        CommonFormatStringParamType_GuildJobName = 20,
        [ProtoEnum(Name="CommonFormatStringParamType_CurrPlayerFullName", Value=0x15)]
        CommonFormatStringParamType_CurrPlayerFullName = 0x15,
        [ProtoEnum(Name="CommonFormatStringParamType_TechName", Value=0x16)]
        CommonFormatStringParamType_TechName = 0x16,
        [ProtoEnum(Name="CommonFormatStringParamType_SignalName", Value=0x17)]
        CommonFormatStringParamType_SignalName = 0x17,
        [ProtoEnum(Name="CommonFormatStringParamType_GuildActionName", Value=0x18)]
        CommonFormatStringParamType_GuildActionName = 0x18,
        [ProtoEnum(Name="CommonFormatStringParamType_Time", Value=0x19)]
        CommonFormatStringParamType_Time = 0x19,
        [ProtoEnum(Name="CommonFormatStringParamType_GuildBuildingName", Value=0x1a)]
        CommonFormatStringParamType_GuildBuildingName = 0x1a,
        [ProtoEnum(Name="CommonFormatStringParamType_SceneName", Value=0x1b)]
        CommonFormatStringParamType_SceneName = 0x1b,
        [ProtoEnum(Name="CommonFormatStringParamType_GiftPackageName", Value=0x1c)]
        CommonFormatStringParamType_GiftPackageName = 0x1c,
        [ProtoEnum(Name="CommonFormatStringParamType_GuildLink", Value=0x1d)]
        CommonFormatStringParamType_GuildLink = 0x1d,
        [ProtoEnum(Name="CommonFormatStringParamType_MonthlyCardName", Value=30)]
        CommonFormatStringParamType_MonthlyCardName = 30
    }
}

