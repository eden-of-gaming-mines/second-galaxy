﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildMiningAreaLevelDetailInfo")]
    public class ConfigDataGuildMiningAreaLevelDetailInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _HourlyOutputList;
        private int _TotalRoundCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_HourlyOutputList;
        private static DelegateBridge __Hotfix_get_TotalRoundCount;
        private static DelegateBridge __Hotfix_set_TotalRoundCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="HourlyOutputList", DataFormat=DataFormat.TwosComplement)]
        public List<int> HourlyOutputList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="TotalRoundCount", DataFormat=DataFormat.TwosComplement)]
        public int TotalRoundCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

