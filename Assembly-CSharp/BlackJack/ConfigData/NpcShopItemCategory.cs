﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcShopItemCategory")]
    public enum NpcShopItemCategory
    {
        [ProtoEnum(Name="NpcShopItemCategory_Invalid", Value=1)]
        NpcShopItemCategory_Invalid = 1,
        [ProtoEnum(Name="NpcShopItemCategory_Ship", Value=2)]
        NpcShopItemCategory_Ship = 2,
        [ProtoEnum(Name="NpcShopItemCategory_Weapon", Value=3)]
        NpcShopItemCategory_Weapon = 3,
        [ProtoEnum(Name="NpcShopItemCategory_Ammo", Value=4)]
        NpcShopItemCategory_Ammo = 4,
        [ProtoEnum(Name="NpcShopItemCategory_Device", Value=5)]
        NpcShopItemCategory_Device = 5,
        [ProtoEnum(Name="NpcShopItemCategory_Component", Value=6)]
        NpcShopItemCategory_Component = 6,
        [ProtoEnum(Name="NpcShopItemCategory_CharChip", Value=7)]
        NpcShopItemCategory_CharChip = 7,
        [ProtoEnum(Name="NpcShopItemCategory_ProductionMaterial", Value=8)]
        NpcShopItemCategory_ProductionMaterial = 8,
        [ProtoEnum(Name="NpcShopItemCategory_ShipBlueprint", Value=9)]
        NpcShopItemCategory_ShipBlueprint = 9,
        [ProtoEnum(Name="NpcShopItemCategory_WeaponBlueprint", Value=10)]
        NpcShopItemCategory_WeaponBlueprint = 10,
        [ProtoEnum(Name="NpcShopItemCategory_AmmoBlueprint", Value=11)]
        NpcShopItemCategory_AmmoBlueprint = 11,
        [ProtoEnum(Name="NpcShopItemCategory_DeviceBlueprint", Value=12)]
        NpcShopItemCategory_DeviceBlueprint = 12,
        [ProtoEnum(Name="NpcShopItemCategory_ComponentBlueprint", Value=13)]
        NpcShopItemCategory_ComponentBlueprint = 13,
        [ProtoEnum(Name="NpcShopItemCategory_OtherBlueprint", Value=14)]
        NpcShopItemCategory_OtherBlueprint = 14,
        [ProtoEnum(Name="NpcShopItemCategory_FeatsBook", Value=15)]
        NpcShopItemCategory_FeatsBook = 15,
        [ProtoEnum(Name="NpcShopItemCategory_TradeGood", Value=0x10)]
        NpcShopItemCategory_TradeGood = 0x10,
        [ProtoEnum(Name="NpcShopItemCategory_NormalItemFunctionItem", Value=0x11)]
        NpcShopItemCategory_NormalItemFunctionItem = 0x11,
        [ProtoEnum(Name="NpcShopItemCategory_NormalItemExchangeItem", Value=0x12)]
        NpcShopItemCategory_NormalItemExchangeItem = 0x12,
        [ProtoEnum(Name="NpcShopItemCategory_EspecialRMBItem", Value=0x13)]
        NpcShopItemCategory_EspecialRMBItem = 0x13,
        [ProtoEnum(Name="NpcShopItemCategory_Other", Value=20)]
        NpcShopItemCategory_Other = 20,
        [ProtoEnum(Name="NpcShopItemCategory_Max", Value=0x15)]
        NpcShopItemCategory_Max = 0x15
    }
}

