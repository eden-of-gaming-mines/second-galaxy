﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCutInfo")]
    public class ConfigDataCutInfo : IExtensible
    {
        private int _ID;
        private string _ArtSourcePath;
        private readonly List<int> _TextList;
        private bool _IsPlayCGFrist;
        private int _CGChapterID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ArtSourcePath;
        private static DelegateBridge __Hotfix_set_ArtSourcePath;
        private static DelegateBridge __Hotfix_get_TextList;
        private static DelegateBridge __Hotfix_get_IsPlayCGFrist;
        private static DelegateBridge __Hotfix_set_IsPlayCGFrist;
        private static DelegateBridge __Hotfix_get_CGChapterID;
        private static DelegateBridge __Hotfix_set_CGChapterID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ArtSourcePath", DataFormat=DataFormat.Default)]
        public string ArtSourcePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="TextList", DataFormat=DataFormat.TwosComplement)]
        public List<int> TextList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="IsPlayCGFrist", DataFormat=DataFormat.Default)]
        public bool IsPlayCGFrist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="CGChapterID", DataFormat=DataFormat.TwosComplement)]
        public int CGChapterID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

