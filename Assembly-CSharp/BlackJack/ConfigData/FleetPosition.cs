﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="FleetPosition")]
    public enum FleetPosition
    {
        [ProtoEnum(Name="FleetPosition_Commander", Value=0)]
        FleetPosition_Commander = 0,
        [ProtoEnum(Name="FleetPosition_Navigator", Value=1)]
        FleetPosition_Navigator = 1,
        [ProtoEnum(Name="FleetPosition_FireController", Value=2)]
        FleetPosition_FireController = 2,
        [ProtoEnum(Name="FleetPosition_Max", Value=3)]
        FleetPosition_Max = 3
    }
}

