﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="TestEnumType")]
    public enum TestEnumType
    {
        [ProtoEnum(Name="TestEnumType_PropertiesModify", Value=1)]
        TestEnumType_PropertiesModify = 1
    }
}

