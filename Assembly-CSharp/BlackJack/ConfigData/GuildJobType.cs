﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildJobType")]
    public enum GuildJobType
    {
        [ProtoEnum(Name="GuildJobType_None", Value=0)]
        GuildJobType_None = 0,
        [ProtoEnum(Name="GuildJobType_Leader", Value=1)]
        GuildJobType_Leader = 1,
        [ProtoEnum(Name="GuildJobType_Manager", Value=2)]
        GuildJobType_Manager = 2,
        [ProtoEnum(Name="GuildJobType_HROfficer", Value=3)]
        GuildJobType_HROfficer = 3,
        [ProtoEnum(Name="GuildJobType_SupportOfficer", Value=4)]
        GuildJobType_SupportOfficer = 4,
        [ProtoEnum(Name="GuildJobType_DiplomatOfficer", Value=5)]
        GuildJobType_DiplomatOfficer = 5,
        [ProtoEnum(Name="GuildJobType_FleetOfficer", Value=6)]
        GuildJobType_FleetOfficer = 6,
        [ProtoEnum(Name="GuildJobType_FlagShipDriver", Value=7)]
        GuildJobType_FlagShipDriver = 7,
        [ProtoEnum(Name="GuildJobType_Default", Value=8)]
        GuildJobType_Default = 8,
        [ProtoEnum(Name="GuildJobType_Max", Value=9)]
        GuildJobType_Max = 9
    }
}

