﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SuperEquipFunctionType")]
    public enum SuperEquipFunctionType
    {
        [ProtoEnum(Name="SuperEquipFunctionType_AttachBuf2Self", Value=1)]
        SuperEquipFunctionType_AttachBuf2Self = 1,
        [ProtoEnum(Name="SuperEquipFunctionType_AttachBuf2Enemy", Value=2)]
        SuperEquipFunctionType_AttachBuf2Enemy = 2,
        [ProtoEnum(Name="SuperEquipFunctionType_AoeAndAttachBuf", Value=3)]
        SuperEquipFunctionType_AoeAndAttachBuf = 3,
        [ProtoEnum(Name="SuperEquipFunctionType_AddEnergyAndAttachBuf", Value=4)]
        SuperEquipFunctionType_AddEnergyAndAttachBuf = 4,
        [ProtoEnum(Name="SuperEquipFunctionType_AddShieldAndAttachBuf", Value=5)]
        SuperEquipFunctionType_AddShieldAndAttachBuf = 5,
        [ProtoEnum(Name="SuperEquipFunctionType_BlinkAndAttachBuf2Self", Value=6)]
        SuperEquipFunctionType_BlinkAndAttachBuf2Self = 6,
        [ProtoEnum(Name="SuperEquipFunctionType_ShipShield2ShieldExAndAttachBuff2Self", Value=7)]
        SuperEquipFunctionType_ShipShield2ShieldExAndAttachBuff2Self = 7,
        [ProtoEnum(Name="SuperEquipFunctionType_ClearCDAndAttachBuff2Self", Value=8)]
        SuperEquipFunctionType_ClearCDAndAttachBuff2Self = 8,
        [ProtoEnum(Name="SuperEquipFunctionType_ChannelMakeDamageAndAttachBuff", Value=9)]
        SuperEquipFunctionType_ChannelMakeDamageAndAttachBuff = 9,
        [ProtoEnum(Name="SuperEquipFunctionType_PeriodicDamageAndRecovrgyEnergy", Value=10)]
        SuperEquipFunctionType_PeriodicDamageAndRecovrgyEnergy = 10,
        [ProtoEnum(Name="SuperEquipFunctionType_BlinkAndAoeAttachBuf2Target", Value=11)]
        SuperEquipFunctionType_BlinkAndAoeAttachBuf2Target = 11,
        [ProtoEnum(Name="SuperEquipFunctionType_DamageRebound", Value=12)]
        SuperEquipFunctionType_DamageRebound = 12,
        [ProtoEnum(Name="SuperEquipFunctionType_TranslateEnergyFromDamageOfTarget", Value=13)]
        SuperEquipFunctionType_TranslateEnergyFromDamageOfTarget = 13,
        [ProtoEnum(Name="SuperEquipFunctionType_AddShieldWhenEnergyCosting", Value=14)]
        SuperEquipFunctionType_AddShieldWhenEnergyCosting = 14,
        [ProtoEnum(Name="SuperEquipFunctionType_AddExtraShieldByCurrEnergy", Value=15)]
        SuperEquipFunctionType_AddExtraShieldByCurrEnergy = 15,
        [ProtoEnum(Name="SuperEquipFunctionType_AntiJumpingForceShield", Value=0x10)]
        SuperEquipFunctionType_AntiJumpingForceShield = 0x10,
        [ProtoEnum(Name="SuperEquipFunctionType_TransformToTeleportTunnel", Value=0x11)]
        SuperEquipFunctionType_TransformToTeleportTunnel = 0x11
    }
}

