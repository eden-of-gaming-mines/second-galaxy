﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SpaceStationRoomType")]
    public enum SpaceStationRoomType
    {
        [ProtoEnum(Name="SpaceStationRoomType_NONE", Value=0)]
        SpaceStationRoomType_NONE = 0,
        [ProtoEnum(Name="SpaceStationRoomType_Shop", Value=1)]
        SpaceStationRoomType_Shop = 1,
        [ProtoEnum(Name="SpaceStationRoomType_HeadQuarter", Value=2)]
        SpaceStationRoomType_HeadQuarter = 2,
        [ProtoEnum(Name="SpaceStationRoomType_TradingCenter", Value=3)]
        SpaceStationRoomType_TradingCenter = 3,
        [ProtoEnum(Name="SpaceStationRoomType_Billboard", Value=4)]
        SpaceStationRoomType_Billboard = 4,
        [ProtoEnum(Name="SpaceStationRoomType_Ranking", Value=5)]
        SpaceStationRoomType_Ranking = 5,
        [ProtoEnum(Name="SpaceStationRoomType_Bar", Value=6)]
        SpaceStationRoomType_Bar = 6,
        [ProtoEnum(Name="SpaceStationRoomType_BlackMarket", Value=7)]
        SpaceStationRoomType_BlackMarket = 7
    }
}

