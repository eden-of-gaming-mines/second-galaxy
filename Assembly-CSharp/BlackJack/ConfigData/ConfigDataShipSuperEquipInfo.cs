﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataShipSuperEquipInfo")]
    public class ConfigDataShipSuperEquipInfo : IExtensible
    {
        private int _ID;
        private SuperEquipFunctionType _FunctionType;
        private EquipType _GroupByEquipType;
        private uint _DamageBasic;
        private float _RangMax;
        private uint _ChargeTime;
        private uint _MWEnergyNeed;
        private uint _MWEnergyGrow;
        private float _AddEnergyOnHitByEnemy;
        private float _AddEnergyOnEnergyUse;
        private int _DefaultSuperEquipRunningId;
        private readonly List<CommonPropertyInfo> _LocalPropertiesList;
        private string _IconResPath;
        private float _ChargeAndChannelBreakFactor;
        private float _TransverseVelocity;
        private readonly List<int> _DrivingLicenseSuperEquipRunningInfoList;
        private readonly List<float> _DamageComposeList;
        private float _FireCtrlAccuracy;
        private string _ChargeEffect;
        private string _LaunchEffect;
        private string _ExtraEffect1;
        private string _ExtraEffect2;
        private string _ExtraEffect3;
        private string _ExtraEffect4;
        private float _ActivateStateDuration;
        private readonly List<SoundInfo> _SoundEffect;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_GroupByEquipType;
        private static DelegateBridge __Hotfix_set_GroupByEquipType;
        private static DelegateBridge __Hotfix_get_DamageBasic;
        private static DelegateBridge __Hotfix_set_DamageBasic;
        private static DelegateBridge __Hotfix_get_RangMax;
        private static DelegateBridge __Hotfix_set_RangMax;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_MWEnergyNeed;
        private static DelegateBridge __Hotfix_set_MWEnergyNeed;
        private static DelegateBridge __Hotfix_get_MWEnergyGrow;
        private static DelegateBridge __Hotfix_set_MWEnergyGrow;
        private static DelegateBridge __Hotfix_get_AddEnergyOnHitByEnemy;
        private static DelegateBridge __Hotfix_set_AddEnergyOnHitByEnemy;
        private static DelegateBridge __Hotfix_get_AddEnergyOnEnergyUse;
        private static DelegateBridge __Hotfix_set_AddEnergyOnEnergyUse;
        private static DelegateBridge __Hotfix_get_DefaultSuperEquipRunningId;
        private static DelegateBridge __Hotfix_set_DefaultSuperEquipRunningId;
        private static DelegateBridge __Hotfix_get_LocalPropertiesList;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_set_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_get_TransverseVelocity;
        private static DelegateBridge __Hotfix_set_TransverseVelocity;
        private static DelegateBridge __Hotfix_get_DrivingLicenseSuperEquipRunningInfoList;
        private static DelegateBridge __Hotfix_get_DamageComposeList;
        private static DelegateBridge __Hotfix_get_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_set_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_get_ChargeEffect;
        private static DelegateBridge __Hotfix_set_ChargeEffect;
        private static DelegateBridge __Hotfix_get_LaunchEffect;
        private static DelegateBridge __Hotfix_set_LaunchEffect;
        private static DelegateBridge __Hotfix_get_ExtraEffect1;
        private static DelegateBridge __Hotfix_set_ExtraEffect1;
        private static DelegateBridge __Hotfix_get_ExtraEffect2;
        private static DelegateBridge __Hotfix_set_ExtraEffect2;
        private static DelegateBridge __Hotfix_get_ExtraEffect3;
        private static DelegateBridge __Hotfix_set_ExtraEffect3;
        private static DelegateBridge __Hotfix_get_ExtraEffect4;
        private static DelegateBridge __Hotfix_set_ExtraEffect4;
        private static DelegateBridge __Hotfix_get_ActivateStateDuration;
        private static DelegateBridge __Hotfix_set_ActivateStateDuration;
        private static DelegateBridge __Hotfix_get_SoundEffect;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public SuperEquipFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="GroupByEquipType", DataFormat=DataFormat.TwosComplement)]
        public EquipType GroupByEquipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DamageBasic", DataFormat=DataFormat.TwosComplement)]
        public uint DamageBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="RangMax", DataFormat=DataFormat.FixedSize)]
        public float RangMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ChargeTime", DataFormat=DataFormat.TwosComplement)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="MWEnergyNeed", DataFormat=DataFormat.TwosComplement)]
        public uint MWEnergyNeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="MWEnergyGrow", DataFormat=DataFormat.TwosComplement)]
        public uint MWEnergyGrow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="AddEnergyOnHitByEnemy", DataFormat=DataFormat.FixedSize)]
        public float AddEnergyOnHitByEnemy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="AddEnergyOnEnergyUse", DataFormat=DataFormat.FixedSize)]
        public float AddEnergyOnEnergyUse
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DefaultSuperEquipRunningId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultSuperEquipRunningId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="LocalPropertiesList", DataFormat=DataFormat.Default)]
        public List<CommonPropertyInfo> LocalPropertiesList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="ChargeAndChannelBreakFactor", DataFormat=DataFormat.FixedSize)]
        public float ChargeAndChannelBreakFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="TransverseVelocity", DataFormat=DataFormat.FixedSize)]
        public float TransverseVelocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, Name="DrivingLicenseSuperEquipRunningInfoList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DrivingLicenseSuperEquipRunningInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(20, Name="DamageComposeList", DataFormat=DataFormat.FixedSize)]
        public List<float> DamageComposeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="FireCtrlAccuracy", DataFormat=DataFormat.FixedSize)]
        public float FireCtrlAccuracy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="ChargeEffect", DataFormat=DataFormat.Default)]
        public string ChargeEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="LaunchEffect", DataFormat=DataFormat.Default)]
        public string LaunchEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="ExtraEffect1", DataFormat=DataFormat.Default)]
        public string ExtraEffect1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="ExtraEffect2", DataFormat=DataFormat.Default)]
        public string ExtraEffect2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="ExtraEffect3", DataFormat=DataFormat.Default)]
        public string ExtraEffect3
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="ExtraEffect4", DataFormat=DataFormat.Default)]
        public string ExtraEffect4
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="ActivateStateDuration", DataFormat=DataFormat.FixedSize)]
        public float ActivateStateDuration
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, Name="SoundEffect", DataFormat=DataFormat.Default)]
        public List<SoundInfo> SoundEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(30, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

