﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGEGrayColorToSolarSystemTemplateMapInfo")]
    public class ConfigDataGEGrayColorToSolarSystemTemplateMapInfo : IExtensible
    {
        private int _ID;
        private readonly List<int> _TemplateIdList;
        private readonly List<GEGrayColorToSolarSystemTemplateMapInfoLevel> _Level;
        private BlackJack.ConfigData.GrandFaction _GrandFaction;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_TemplateIdList;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="TemplateIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> TemplateIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="Level", DataFormat=DataFormat.Default)]
        public List<GEGrayColorToSolarSystemTemplateMapInfoLevel> Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.GrandFaction GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

