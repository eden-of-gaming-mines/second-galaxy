﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="SignalType")]
    public enum SignalType
    {
        [ProtoEnum(Name="SignalType_None", Value=0)]
        SignalType_None = 0,
        [ProtoEnum(Name="SignalType_DelegateFight", Value=1)]
        SignalType_DelegateFight = 1,
        [ProtoEnum(Name="SignalType_DelegateMineral", Value=2)]
        SignalType_DelegateMineral = 2,
        [ProtoEnum(Name="SignalType_DelegateTransport", Value=3)]
        SignalType_DelegateTransport = 3,
        [ProtoEnum(Name="SignalType_ManualQuest", Value=4)]
        SignalType_ManualQuest = 4,
        [ProtoEnum(Name="SignalType_PVP2DelegateFight", Value=5)]
        SignalType_PVP2DelegateFight = 5,
        [ProtoEnum(Name="SignalType_PVP2DelegateMineral", Value=6)]
        SignalType_PVP2DelegateMineral = 6,
        [ProtoEnum(Name="SignalType_PVP2DelegateTransport", Value=7)]
        SignalType_PVP2DelegateTransport = 7,
        [ProtoEnum(Name="SignalType_PVP2ManualScene", Value=8)]
        SignalType_PVP2ManualScene = 8,
        [ProtoEnum(Name="SignalType_SpaceSignalGravity", Value=9)]
        SignalType_SpaceSignalGravity = 9,
        [ProtoEnum(Name="SignalType_SpaceSignalMicroWave", Value=10)]
        SignalType_SpaceSignalMicroWave = 10,
        [ProtoEnum(Name="SignalType_SpaceSignalRadiation", Value=11)]
        SignalType_SpaceSignalRadiation = 11
    }
}

