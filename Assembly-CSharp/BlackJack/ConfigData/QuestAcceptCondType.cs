﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="QuestAcceptCondType")]
    public enum QuestAcceptCondType
    {
        [ProtoEnum(Name="QuestAcceptCondType_Level", Value=1)]
        QuestAcceptCondType_Level = 1,
        [ProtoEnum(Name="QuestAcceptCondType_MSLevel", Value=2)]
        QuestAcceptCondType_MSLevel = 2,
        [ProtoEnum(Name="QuestAcceptCondType_PreQuest", Value=3)]
        QuestAcceptCondType_PreQuest = 3,
        [ProtoEnum(Name="QuestAcceptCondType_FactionCredit", Value=4)]
        QuestAcceptCondType_FactionCredit = 4,
        [ProtoEnum(Name="QuestAcceptCondType_TimePeriodInDay", Value=5)]
        QuestAcceptCondType_TimePeriodInDay = 5,
        [ProtoEnum(Name="QuestAcceptCondType_Time", Value=6)]
        QuestAcceptCondType_Time = 6,
        [ProtoEnum(Name="QuestAcceptCondType_DrivingLicense", Value=7)]
        QuestAcceptCondType_DrivingLicense = 7,
        [ProtoEnum(Name="QuestAcceptCondType_FactionCreditLevel", Value=8)]
        QuestAcceptCondType_FactionCreditLevel = 8
    }
}

