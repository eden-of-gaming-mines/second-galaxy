﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataWormholeStarGroupInfo")]
    public class ConfigDataWormholeStarGroupInfo : IExtensible
    {
        private int _ID;
        private int _PLevel;
        private readonly List<int> _EntrySolarSystemIdListForFrigate;
        private readonly List<int> _EntrySolarSystemIdListForDestroyer;
        private readonly List<int> _EntrySolarSystemIdListForCruiser;
        private readonly List<int> _EntrySolarSystemIdListForBattleCruiser;
        private readonly List<int> _EntrySolarSystemIdListForBattleShip;
        private readonly List<ShipType> _ShipTypeLimit;
        private int _RecommendLevel;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_PLevel;
        private static DelegateBridge __Hotfix_set_PLevel;
        private static DelegateBridge __Hotfix_get_EntrySolarSystemIdListForFrigate;
        private static DelegateBridge __Hotfix_get_EntrySolarSystemIdListForDestroyer;
        private static DelegateBridge __Hotfix_get_EntrySolarSystemIdListForCruiser;
        private static DelegateBridge __Hotfix_get_EntrySolarSystemIdListForBattleCruiser;
        private static DelegateBridge __Hotfix_get_EntrySolarSystemIdListForBattleShip;
        private static DelegateBridge __Hotfix_get_ShipTypeLimit;
        private static DelegateBridge __Hotfix_get_RecommendLevel;
        private static DelegateBridge __Hotfix_set_RecommendLevel;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PLevel", DataFormat=DataFormat.TwosComplement)]
        public int PLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="EntrySolarSystemIdListForFrigate", DataFormat=DataFormat.TwosComplement)]
        public List<int> EntrySolarSystemIdListForFrigate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="EntrySolarSystemIdListForDestroyer", DataFormat=DataFormat.TwosComplement)]
        public List<int> EntrySolarSystemIdListForDestroyer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="EntrySolarSystemIdListForCruiser", DataFormat=DataFormat.TwosComplement)]
        public List<int> EntrySolarSystemIdListForCruiser
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="EntrySolarSystemIdListForBattleCruiser", DataFormat=DataFormat.TwosComplement)]
        public List<int> EntrySolarSystemIdListForBattleCruiser
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="EntrySolarSystemIdListForBattleShip", DataFormat=DataFormat.TwosComplement)]
        public List<int> EntrySolarSystemIdListForBattleShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="ShipTypeLimit", DataFormat=DataFormat.TwosComplement)]
        public List<ShipType> ShipTypeLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="RecommendLevel", DataFormat=DataFormat.TwosComplement)]
        public int RecommendLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

