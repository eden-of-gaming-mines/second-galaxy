﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GEGrayColorToSolarSystemTemplateMapInfoLevel")]
    public class GEGrayColorToSolarSystemTemplateMapInfoLevel : IExtensible
    {
        private int _MinValue;
        private int _MaxValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MinValue;
        private static DelegateBridge __Hotfix_set_MinValue;
        private static DelegateBridge __Hotfix_get_MaxValue;
        private static DelegateBridge __Hotfix_set_MaxValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MinValue", DataFormat=DataFormat.TwosComplement)]
        public int MinValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="MaxValue", DataFormat=DataFormat.TwosComplement)]
        public int MaxValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

