﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataQuestInfo")]
    public class ConfigDataQuestInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.QuestType _QuestType;
        private int _DefaultFactionId;
        private int _Level;
        private int _LevelMulti;
        private int _QuestFlag;
        private int _EnvTypeId;
        private int _AcceptNpcNDIdSolarSystemId;
        private int _AcceptNpcNDIdSpaceStationId;
        private int _AcceptNpcNDIdNpcId;
        private int _AcceptNpcDialogStart;
        private int _AcceptNpcDialogEnd;
        private readonly List<ShipType> _ShipTypeLimit;
        private readonly List<GrandFaction> _GrandFactionLimit;
        private readonly List<QuestAcceptCondInfo> _VisibleCondList;
        private readonly List<QuestAcceptCondInfo> _AcceptCondList;
        private bool _MultiAccept;
        private readonly List<QuestCompleteCondInfo> _CompleteCondList;
        private readonly List<QuestPostEventInfo> _PostEventList;
        private int _CompleteCondNpcNDIdSolarSystemId;
        private int _CompleteCondNpcNDIdSpaceStationId;
        private int _CompleteCondNpcNDIdNpcId;
        private int _QuestSceneId;
        private DynamicSceneLocationType _QuestSceneLocationType;
        private DynamicSceneNearCelestialType _SceneNearCelestialType;
        private int _QuestSceneSolarSystemId;
        private bool _TakeQuestSceneSolarSystemIdAsEnvIndex;
        private readonly List<QuestRewardInfo> _RewardList;
        private int _AdditionRewardListId;
        private bool _RewardItemSendByMail;
        private float _ReduceCriminalLevel;
        private float _ReduceInfectProgress;
        private int _PushQuest4Cancel;
        private string _IconResPath;
        private readonly List<CommonFormatStringParamType> _FormatStringParamTypeList;
        private readonly List<FormatStringParamInfo> _FormatStringParamInfoList;
        private int _StorageIndex;
        private readonly List<RecommendItemInfo> _RecommendItemInfoList;
        private ShipType _RecommendShipType;
        private string _NameStrKey;
        private string _SceneEventCompleteCondDescStrKey;
        private string _DescStrKey;
        private string _LimitEffectKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_QuestType;
        private static DelegateBridge __Hotfix_set_QuestType;
        private static DelegateBridge __Hotfix_get_DefaultFactionId;
        private static DelegateBridge __Hotfix_set_DefaultFactionId;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_LevelMulti;
        private static DelegateBridge __Hotfix_set_LevelMulti;
        private static DelegateBridge __Hotfix_get_QuestFlag;
        private static DelegateBridge __Hotfix_set_QuestFlag;
        private static DelegateBridge __Hotfix_get_EnvTypeId;
        private static DelegateBridge __Hotfix_set_EnvTypeId;
        private static DelegateBridge __Hotfix_get_AcceptNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_set_AcceptNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_get_AcceptNpcNDIdSpaceStationId;
        private static DelegateBridge __Hotfix_set_AcceptNpcNDIdSpaceStationId;
        private static DelegateBridge __Hotfix_get_AcceptNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_set_AcceptNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_get_AcceptNpcDialogStart;
        private static DelegateBridge __Hotfix_set_AcceptNpcDialogStart;
        private static DelegateBridge __Hotfix_get_AcceptNpcDialogEnd;
        private static DelegateBridge __Hotfix_set_AcceptNpcDialogEnd;
        private static DelegateBridge __Hotfix_get_ShipTypeLimit;
        private static DelegateBridge __Hotfix_get_GrandFactionLimit;
        private static DelegateBridge __Hotfix_get_VisibleCondList;
        private static DelegateBridge __Hotfix_get_AcceptCondList;
        private static DelegateBridge __Hotfix_get_MultiAccept;
        private static DelegateBridge __Hotfix_set_MultiAccept;
        private static DelegateBridge __Hotfix_get_CompleteCondList;
        private static DelegateBridge __Hotfix_get_PostEventList;
        private static DelegateBridge __Hotfix_get_CompleteCondNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_set_CompleteCondNpcNDIdSolarSystemId;
        private static DelegateBridge __Hotfix_get_CompleteCondNpcNDIdSpaceStationId;
        private static DelegateBridge __Hotfix_set_CompleteCondNpcNDIdSpaceStationId;
        private static DelegateBridge __Hotfix_get_CompleteCondNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_set_CompleteCondNpcNDIdNpcId;
        private static DelegateBridge __Hotfix_get_QuestSceneId;
        private static DelegateBridge __Hotfix_set_QuestSceneId;
        private static DelegateBridge __Hotfix_get_QuestSceneLocationType;
        private static DelegateBridge __Hotfix_set_QuestSceneLocationType;
        private static DelegateBridge __Hotfix_get_SceneNearCelestialType;
        private static DelegateBridge __Hotfix_set_SceneNearCelestialType;
        private static DelegateBridge __Hotfix_get_QuestSceneSolarSystemId;
        private static DelegateBridge __Hotfix_set_QuestSceneSolarSystemId;
        private static DelegateBridge __Hotfix_get_TakeQuestSceneSolarSystemIdAsEnvIndex;
        private static DelegateBridge __Hotfix_set_TakeQuestSceneSolarSystemIdAsEnvIndex;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_AdditionRewardListId;
        private static DelegateBridge __Hotfix_set_AdditionRewardListId;
        private static DelegateBridge __Hotfix_get_RewardItemSendByMail;
        private static DelegateBridge __Hotfix_set_RewardItemSendByMail;
        private static DelegateBridge __Hotfix_get_ReduceCriminalLevel;
        private static DelegateBridge __Hotfix_set_ReduceCriminalLevel;
        private static DelegateBridge __Hotfix_get_ReduceInfectProgress;
        private static DelegateBridge __Hotfix_set_ReduceInfectProgress;
        private static DelegateBridge __Hotfix_get_PushQuest4Cancel;
        private static DelegateBridge __Hotfix_set_PushQuest4Cancel;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_FormatStringParamTypeList;
        private static DelegateBridge __Hotfix_get_FormatStringParamInfoList;
        private static DelegateBridge __Hotfix_get_StorageIndex;
        private static DelegateBridge __Hotfix_set_StorageIndex;
        private static DelegateBridge __Hotfix_get_RecommendItemInfoList;
        private static DelegateBridge __Hotfix_get_RecommendShipType;
        private static DelegateBridge __Hotfix_set_RecommendShipType;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_SceneEventCompleteCondDescStrKey;
        private static DelegateBridge __Hotfix_set_SceneEventCompleteCondDescStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_LimitEffectKey;
        private static DelegateBridge __Hotfix_set_LimitEffectKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="QuestType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.QuestType QuestType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="DefaultFactionId", DataFormat=DataFormat.TwosComplement)]
        public int DefaultFactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="LevelMulti", DataFormat=DataFormat.TwosComplement)]
        public int LevelMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="QuestFlag", DataFormat=DataFormat.TwosComplement)]
        public int QuestFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="EnvTypeId", DataFormat=DataFormat.TwosComplement)]
        public int EnvTypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="AcceptNpcNDIdSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int AcceptNpcNDIdSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="AcceptNpcNDIdSpaceStationId", DataFormat=DataFormat.TwosComplement)]
        public int AcceptNpcNDIdSpaceStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="AcceptNpcNDIdNpcId", DataFormat=DataFormat.TwosComplement)]
        public int AcceptNpcNDIdNpcId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="AcceptNpcDialogStart", DataFormat=DataFormat.TwosComplement)]
        public int AcceptNpcDialogStart
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="AcceptNpcDialogEnd", DataFormat=DataFormat.TwosComplement)]
        public int AcceptNpcDialogEnd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="ShipTypeLimit", DataFormat=DataFormat.TwosComplement)]
        public List<ShipType> ShipTypeLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, Name="GrandFactionLimit", DataFormat=DataFormat.TwosComplement)]
        public List<GrandFaction> GrandFactionLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, Name="VisibleCondList", DataFormat=DataFormat.Default)]
        public List<QuestAcceptCondInfo> VisibleCondList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x12, Name="AcceptCondList", DataFormat=DataFormat.Default)]
        public List<QuestAcceptCondInfo> AcceptCondList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="MultiAccept", DataFormat=DataFormat.Default)]
        public bool MultiAccept
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, Name="CompleteCondList", DataFormat=DataFormat.Default)]
        public List<QuestCompleteCondInfo> CompleteCondList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x15, Name="PostEventList", DataFormat=DataFormat.Default)]
        public List<QuestPostEventInfo> PostEventList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="CompleteCondNpcNDIdSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int CompleteCondNpcNDIdSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="CompleteCondNpcNDIdSpaceStationId", DataFormat=DataFormat.TwosComplement)]
        public int CompleteCondNpcNDIdSpaceStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="CompleteCondNpcNDIdNpcId", DataFormat=DataFormat.TwosComplement)]
        public int CompleteCondNpcNDIdNpcId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="QuestSceneId", DataFormat=DataFormat.TwosComplement)]
        public int QuestSceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="QuestSceneLocationType", DataFormat=DataFormat.TwosComplement)]
        public DynamicSceneLocationType QuestSceneLocationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="SceneNearCelestialType", DataFormat=DataFormat.TwosComplement)]
        public DynamicSceneNearCelestialType SceneNearCelestialType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="QuestSceneSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int QuestSceneSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=true, Name="TakeQuestSceneSolarSystemIdAsEnvIndex", DataFormat=DataFormat.Default)]
        public bool TakeQuestSceneSolarSystemIdAsEnvIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<QuestRewardInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="AdditionRewardListId", DataFormat=DataFormat.TwosComplement)]
        public int AdditionRewardListId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=true, Name="RewardItemSendByMail", DataFormat=DataFormat.Default)]
        public bool RewardItemSendByMail
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=true, Name="ReduceCriminalLevel", DataFormat=DataFormat.FixedSize)]
        public float ReduceCriminalLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=true, Name="ReduceInfectProgress", DataFormat=DataFormat.FixedSize)]
        public float ReduceInfectProgress
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x23, IsRequired=true, Name="PushQuest4Cancel", DataFormat=DataFormat.TwosComplement)]
        public int PushQuest4Cancel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x25, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(40, Name="FormatStringParamTypeList", DataFormat=DataFormat.TwosComplement)]
        public List<CommonFormatStringParamType> FormatStringParamTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x29, Name="FormatStringParamInfoList", DataFormat=DataFormat.Default)]
        public List<FormatStringParamInfo> FormatStringParamInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=true, Name="StorageIndex", DataFormat=DataFormat.TwosComplement)]
        public int StorageIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, Name="RecommendItemInfoList", DataFormat=DataFormat.Default)]
        public List<RecommendItemInfo> RecommendItemInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=true, Name="RecommendShipType", DataFormat=DataFormat.TwosComplement)]
        public ShipType RecommendShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2e, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2f, IsRequired=true, Name="SceneEventCompleteCondDescStrKey", DataFormat=DataFormat.Default)]
        public string SceneEventCompleteCondDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x30, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x31, IsRequired=true, Name="LimitEffectKey", DataFormat=DataFormat.Default)]
        public string LimitEffectKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

