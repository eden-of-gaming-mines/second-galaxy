﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NormalItemType")]
    public enum NormalItemType
    {
        [ProtoEnum(Name="NormalItemType_Normal", Value=1)]
        NormalItemType_Normal = 1,
        [ProtoEnum(Name="NormalItemType_NpcCaptainShipComp", Value=2)]
        NormalItemType_NpcCaptainShipComp = 2,
        [ProtoEnum(Name="NormalItemType_TransportSignalGoods", Value=3)]
        NormalItemType_TransportSignalGoods = 3,
        [ProtoEnum(Name="NormalItemType_StarFieldChatItem", Value=4)]
        NormalItemType_StarFieldChatItem = 4,
        [ProtoEnum(Name="NormalItemType_HiredCaptainExpAdd", Value=5)]
        NormalItemType_HiredCaptainExpAdd = 5,
        [ProtoEnum(Name="NormalItemType_TechSpeedUp", Value=6)]
        NormalItemType_TechSpeedUp = 6,
        [ProtoEnum(Name="NormalItemType_MSRedeploySpeedUp", Value=7)]
        NormalItemType_MSRedeploySpeedUp = 7,
        [ProtoEnum(Name="NormalItemType_ProduceSpeedUp", Value=8)]
        NormalItemType_ProduceSpeedUp = 8,
        [ProtoEnum(Name="NormalItemType_DrivingLicenseSpeedUp", Value=9)]
        NormalItemType_DrivingLicenseSpeedUp = 9,
        [ProtoEnum(Name="NormalItemType_ZipMineral", Value=10)]
        NormalItemType_ZipMineral = 10,
        [ProtoEnum(Name="NormalItemType_UsableBox", Value=11)]
        NormalItemType_UsableBox = 11,
        [ProtoEnum(Name="NormalItemType_ExploreTicket", Value=12)]
        NormalItemType_ExploreTicket = 12,
        [ProtoEnum(Name="NormalItemType_RandomBlueprint", Value=13)]
        NormalItemType_RandomBlueprint = 13,
        [ProtoEnum(Name="NormalItemType_ProduceMaterial", Value=14)]
        NormalItemType_ProduceMaterial = 14,
        [ProtoEnum(Name="NormalItemType_GuildBuilding", Value=15)]
        NormalItemType_GuildBuilding = 15,
        [ProtoEnum(Name="NormalItemType_GuildInformationPointItem", Value=0x10)]
        NormalItemType_GuildInformationPointItem = 0x10,
        [ProtoEnum(Name="NormalItemType_GuildTacticalPointItem", Value=0x11)]
        NormalItemType_GuildTacticalPointItem = 0x11,
        [ProtoEnum(Name="NormalItemType_ManualScanProbe", Value=0x12)]
        NormalItemType_ManualScanProbe = 0x12,
        [ProtoEnum(Name="NormalItemType_DelegateScanProbe", Value=0x13)]
        NormalItemType_DelegateScanProbe = 0x13,
        [ProtoEnum(Name="NormalItemType_PVPScanProbe", Value=20)]
        NormalItemType_PVPScanProbe = 20,
        [ProtoEnum(Name="NormalItemType_HiredCaptainAdd", Value=0x15)]
        NormalItemType_HiredCaptainAdd = 0x15,
        [ProtoEnum(Name="NormalItemType_HangarShipSalvage", Value=0x16)]
        NormalItemType_HangarShipSalvage = 0x16,
        [ProtoEnum(Name="NormalItemType_TradeLegalGood_ECD", Value=0x17)]
        NormalItemType_TradeLegalGood_ECD = 0x17,
        [ProtoEnum(Name="NormalItemType_TradeLegalGood_NEF", Value=0x18)]
        NormalItemType_TradeLegalGood_NEF = 0x18,
        [ProtoEnum(Name="NormalItemType_TradeLegalGood_RS", Value=0x19)]
        NormalItemType_TradeLegalGood_RS = 0x19,
        [ProtoEnum(Name="NormalItemType_TradeLegalGood_OE", Value=0x1a)]
        NormalItemType_TradeLegalGood_OE = 0x1a,
        [ProtoEnum(Name="NormalItemType_TradeLegalGood_USG", Value=0x1b)]
        NormalItemType_TradeLegalGood_USG = 0x1b,
        [ProtoEnum(Name="NormalItemType_TradeIllegalGood", Value=0x1c)]
        NormalItemType_TradeIllegalGood = 0x1c,
        [ProtoEnum(Name="NormalItemType_FlagShipComp", Value=0x1d)]
        NormalItemType_FlagShipComp = 0x1d,
        [ProtoEnum(Name="NormalItemType_FlagShipEnergyPack", Value=30)]
        NormalItemType_FlagShipEnergyPack = 30,
        [ProtoEnum(Name="NormalItemType_PlayerExpAdd", Value=0x1f)]
        NormalItemType_PlayerExpAdd = 0x1f,
        [ProtoEnum(Name="NormalItemType_GuildMineral", Value=0x20)]
        NormalItemType_GuildMineral = 0x20,
        [ProtoEnum(Name="NormalItemType_ZipGuildMineral", Value=0x21)]
        NormalItemType_ZipGuildMineral = 0x21,
        [ProtoEnum(Name="NormalItemType_BindMoneyExchange", Value=0x22)]
        NormalItemType_BindMoneyExchange = 0x22,
        [ProtoEnum(Name="NormalItemType_Catalyst", Value=0x23)]
        NormalItemType_Catalyst = 0x23
    }
}

