﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CameraAnimationInfo")]
    public class CameraAnimationInfo : IExtensible
    {
        private int _CameraType;
        private int _ConfgId;
        private bool _InitRotationWithTarget;
        private bool _IsFollowTarget;
        private bool _IsLookAtTarget;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CameraType;
        private static DelegateBridge __Hotfix_set_CameraType;
        private static DelegateBridge __Hotfix_get_ConfgId;
        private static DelegateBridge __Hotfix_set_ConfgId;
        private static DelegateBridge __Hotfix_get_InitRotationWithTarget;
        private static DelegateBridge __Hotfix_set_InitRotationWithTarget;
        private static DelegateBridge __Hotfix_get_IsFollowTarget;
        private static DelegateBridge __Hotfix_set_IsFollowTarget;
        private static DelegateBridge __Hotfix_get_IsLookAtTarget;
        private static DelegateBridge __Hotfix_set_IsLookAtTarget;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CameraType", DataFormat=DataFormat.TwosComplement)]
        public int CameraType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ConfgId", DataFormat=DataFormat.TwosComplement)]
        public int ConfgId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="InitRotationWithTarget", DataFormat=DataFormat.Default)]
        public bool InitRotationWithTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="IsFollowTarget", DataFormat=DataFormat.Default)]
        public bool IsFollowTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="IsLookAtTarget", DataFormat=DataFormat.Default)]
        public bool IsLookAtTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

