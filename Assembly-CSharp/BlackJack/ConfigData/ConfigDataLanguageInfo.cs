﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataLanguageInfo")]
    public class ConfigDataLanguageInfo : IExtensible
    {
        private int _ID;
        private BlackJack.ConfigData.LanguageType _LanguageType;
        private string _SimpleName;
        private string _SdkName;
        private string _StandardSimpleName;
        private bool _Enable;
        private float _CharLengthRatio;
        private readonly List<int> _CharRangeList;
        private string _NameStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_SimpleName;
        private static DelegateBridge __Hotfix_set_SimpleName;
        private static DelegateBridge __Hotfix_get_SdkName;
        private static DelegateBridge __Hotfix_set_SdkName;
        private static DelegateBridge __Hotfix_get_StandardSimpleName;
        private static DelegateBridge __Hotfix_set_StandardSimpleName;
        private static DelegateBridge __Hotfix_get_Enable;
        private static DelegateBridge __Hotfix_set_Enable;
        private static DelegateBridge __Hotfix_get_CharLengthRatio;
        private static DelegateBridge __Hotfix_set_CharLengthRatio;
        private static DelegateBridge __Hotfix_get_CharRangeList;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="LanguageType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.LanguageType LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SimpleName", DataFormat=DataFormat.Default)]
        public string SimpleName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SdkName", DataFormat=DataFormat.Default)]
        public string SdkName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="StandardSimpleName", DataFormat=DataFormat.Default)]
        public string StandardSimpleName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Enable", DataFormat=DataFormat.Default)]
        public bool Enable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CharLengthRatio", DataFormat=DataFormat.FixedSize)]
        public float CharLengthRatio
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="CharRangeList", DataFormat=DataFormat.TwosComplement)]
        public List<int> CharRangeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

