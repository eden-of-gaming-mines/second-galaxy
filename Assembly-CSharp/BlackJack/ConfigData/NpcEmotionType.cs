﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcEmotionType")]
    public enum NpcEmotionType
    {
        [ProtoEnum(Name="NpcEmotionType_Default", Value=0)]
        NpcEmotionType_Default = 0,
        [ProtoEnum(Name="NpcEmotionType_Calm", Value=1)]
        NpcEmotionType_Calm = 1,
        [ProtoEnum(Name="NpcEmotionType_Smile", Value=2)]
        NpcEmotionType_Smile = 2,
        [ProtoEnum(Name="NpcEmotionType_Excite", Value=3)]
        NpcEmotionType_Excite = 3,
        [ProtoEnum(Name="NpcEmotionType_Melancholy", Value=4)]
        NpcEmotionType_Melancholy = 4
    }
}

