﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataCharProfessionInfo")]
    public class ConfigDataCharProfessionInfo : IExtensible
    {
        private int _ID;
        private ProfessionType _Profession;
        private readonly List<CharProfessionInfoInitPropertiesBasic> _InitPropertiesBasic;
        private readonly List<CharProfessionInfoAutoAddProperties> _AutoAddProperties;
        private int _LeveUpPropertiesFree4Add;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _FeatureStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_InitPropertiesBasic;
        private static DelegateBridge __Hotfix_get_AutoAddProperties;
        private static DelegateBridge __Hotfix_get_LeveUpPropertiesFree4Add;
        private static DelegateBridge __Hotfix_set_LeveUpPropertiesFree4Add;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_FeatureStrKey;
        private static DelegateBridge __Hotfix_set_FeatureStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public ProfessionType Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="InitPropertiesBasic", DataFormat=DataFormat.Default)]
        public List<CharProfessionInfoInitPropertiesBasic> InitPropertiesBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="AutoAddProperties", DataFormat=DataFormat.Default)]
        public List<CharProfessionInfoAutoAddProperties> AutoAddProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LeveUpPropertiesFree4Add", DataFormat=DataFormat.TwosComplement)]
        public int LeveUpPropertiesFree4Add
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="FeatureStrKey", DataFormat=DataFormat.Default)]
        public string FeatureStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

