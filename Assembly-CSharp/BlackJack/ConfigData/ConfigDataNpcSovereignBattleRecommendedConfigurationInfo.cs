﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataNpcSovereignBattleRecommendedConfigurationInfo")]
    public class ConfigDataNpcSovereignBattleRecommendedConfigurationInfo : IExtensible
    {
        private int _ID;
        private int _DifficultLevel;
        private int _CommanderLevel;
        private RankType _EquipRank;
        private SubRankType _EquipSubRank;
        private int _ShipNumber;
        private int _FlagShipNumber;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_DifficultLevel;
        private static DelegateBridge __Hotfix_set_DifficultLevel;
        private static DelegateBridge __Hotfix_get_CommanderLevel;
        private static DelegateBridge __Hotfix_set_CommanderLevel;
        private static DelegateBridge __Hotfix_get_EquipRank;
        private static DelegateBridge __Hotfix_set_EquipRank;
        private static DelegateBridge __Hotfix_get_EquipSubRank;
        private static DelegateBridge __Hotfix_set_EquipSubRank;
        private static DelegateBridge __Hotfix_get_ShipNumber;
        private static DelegateBridge __Hotfix_set_ShipNumber;
        private static DelegateBridge __Hotfix_get_FlagShipNumber;
        private static DelegateBridge __Hotfix_set_FlagShipNumber;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DifficultLevel", DataFormat=DataFormat.TwosComplement)]
        public int DifficultLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CommanderLevel", DataFormat=DataFormat.TwosComplement)]
        public int CommanderLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EquipRank", DataFormat=DataFormat.TwosComplement)]
        public RankType EquipRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="EquipSubRank", DataFormat=DataFormat.TwosComplement)]
        public SubRankType EquipSubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ShipNumber", DataFormat=DataFormat.TwosComplement)]
        public int ShipNumber
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="FlagShipNumber", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipNumber
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

