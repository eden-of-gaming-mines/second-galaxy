﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GEBrushType")]
    public enum GEBrushType
    {
        [ProtoEnum(Name="GEBrushType_Invalid", Value=0)]
        GEBrushType_Invalid = 0,
        [ProtoEnum(Name="GEBrushType_None", Value=1)]
        GEBrushType_None = 1,
        [ProtoEnum(Name="GEBrushType_SecurityLevel", Value=2)]
        GEBrushType_SecurityLevel = 2,
        [ProtoEnum(Name="GEBrushType_MiningVitality", Value=3)]
        GEBrushType_MiningVitality = 3,
        [ProtoEnum(Name="GEBrushType_PiratesVitality", Value=4)]
        GEBrushType_PiratesVitality = 4,
        [ProtoEnum(Name="GEBrushType_CommercialVitality", Value=5)]
        GEBrushType_CommercialVitality = 5,
        [ProtoEnum(Name="GEBrushType_ExplodedShipCount", Value=6)]
        GEBrushType_ExplodedShipCount = 6,
        [ProtoEnum(Name="GEBrushType_PlayerSovereign", Value=7)]
        GEBrushType_PlayerSovereign = 7,
        [ProtoEnum(Name="GEBrushType_NpcSovereign", Value=8)]
        GEBrushType_NpcSovereign = 8,
        [ProtoEnum(Name="GEBrushType_Sovereign", Value=9)]
        GEBrushType_Sovereign = 9,
        [ProtoEnum(Name="GEBrushType_WormHoleDistribution", Value=10)]
        GEBrushType_WormHoleDistribution = 10,
        [ProtoEnum(Name="GEBrushType_RareWormHoleDistribution", Value=11)]
        GEBrushType_RareWormHoleDistribution = 11,
        [ProtoEnum(Name="GEBrushType_InfectProgress", Value=12)]
        GEBrushType_InfectProgress = 12,
        [ProtoEnum(Name="GEBrushType_MineralTypeDistribution", Value=13)]
        GEBrushType_MineralTypeDistribution = 13,
        [ProtoEnum(Name="GEBrushType_SignalLevel", Value=14)]
        GEBrushType_SignalLevel = 14,
        [ProtoEnum(Name="GEBrushType_QuestPoolCount", Value=15)]
        GEBrushType_QuestPoolCount = 15,
        [ProtoEnum(Name="GEBrushType_GuildBattleState", Value=0x10)]
        GEBrushType_GuildBattleState = 0x10,
        [ProtoEnum(Name="GEBrushType_AllianceSovereign", Value=0x11)]
        GEBrushType_AllianceSovereign = 0x11
    }
}

