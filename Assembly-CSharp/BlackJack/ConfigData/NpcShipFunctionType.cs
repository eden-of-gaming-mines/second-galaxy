﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="NpcShipFunctionType")]
    public enum NpcShipFunctionType
    {
        [ProtoEnum(Name="NpcShipFunctionType_Normal", Value=0)]
        NpcShipFunctionType_Normal = 0,
        [ProtoEnum(Name="NpcShipFunctionType_StargatePoliceTurret", Value=1)]
        NpcShipFunctionType_StargatePoliceTurret = 1,
        [ProtoEnum(Name="NpcShipFunctionType_Custom", Value=2)]
        NpcShipFunctionType_Custom = 2,
        [ProtoEnum(Name="NpcShipFunctionType_Swat", Value=3)]
        NpcShipFunctionType_Swat = 3,
        [ProtoEnum(Name="NpcShipFunctionType_FakeBuilding", Value=4)]
        NpcShipFunctionType_FakeBuilding = 4,
        [ProtoEnum(Name="NpcShipFunctionType_GuildBuilding", Value=5)]
        NpcShipFunctionType_GuildBuilding = 5,
        [ProtoEnum(Name="NpcShipFunctionType_Declaringfacility", Value=6)]
        NpcShipFunctionType_Declaringfacility = 6,
        [ProtoEnum(Name="NpcShipFunctionType_ShieldStabiliser", Value=7)]
        NpcShipFunctionType_ShieldStabiliser = 7,
        [ProtoEnum(Name="NpcShipFunctionType_GuildDisturber", Value=8)]
        NpcShipFunctionType_GuildDisturber = 8,
        [ProtoEnum(Name="NpcShipFunctionType_Max", Value=9)]
        NpcShipFunctionType_Max = 9
    }
}

