﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="WeaponCategory")]
    public enum WeaponCategory
    {
        [ProtoEnum(Name="WeaponCategory_Railgun", Value=1)]
        WeaponCategory_Railgun = 1,
        [ProtoEnum(Name="WeaponCategory_Plasma", Value=2)]
        WeaponCategory_Plasma = 2,
        [ProtoEnum(Name="WeaponCategory_Cannon", Value=3)]
        WeaponCategory_Cannon = 3,
        [ProtoEnum(Name="WeaponCategory_Laser", Value=4)]
        WeaponCategory_Laser = 4,
        [ProtoEnum(Name="WeaponCategory_Drone", Value=5)]
        WeaponCategory_Drone = 5,
        [ProtoEnum(Name="WeaponCategory_Missile", Value=6)]
        WeaponCategory_Missile = 6,
        [ProtoEnum(Name="WeaponCategory_NormalAttack", Value=7)]
        WeaponCategory_NormalAttack = 7,
        [ProtoEnum(Name="WeaponCategory_Equip", Value=8)]
        WeaponCategory_Equip = 8,
        [ProtoEnum(Name="WeaponCategory_Max", Value=9)]
        WeaponCategory_Max = 9
    }
}

