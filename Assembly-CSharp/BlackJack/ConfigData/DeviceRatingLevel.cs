﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DeviceRatingLevel")]
    public enum DeviceRatingLevel
    {
        [ProtoEnum(Name="DeviceRatingLevel_UnSupported", Value=0)]
        DeviceRatingLevel_UnSupported = 0,
        [ProtoEnum(Name="DeviceRatingLevel_Low", Value=1)]
        DeviceRatingLevel_Low = 1,
        [ProtoEnum(Name="DeviceRatingLevel_Medium", Value=2)]
        DeviceRatingLevel_Medium = 2,
        [ProtoEnum(Name="DeviceRatingLevel_High", Value=3)]
        DeviceRatingLevel_High = 3,
        [ProtoEnum(Name="DeviceRatingLevel_Perfect", Value=4)]
        DeviceRatingLevel_Perfect = 4
    }
}

