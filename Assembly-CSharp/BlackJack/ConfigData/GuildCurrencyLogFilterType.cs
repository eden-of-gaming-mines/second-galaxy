﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="GuildCurrencyLogFilterType")]
    public enum GuildCurrencyLogFilterType
    {
        [ProtoEnum(Name="GuildCurrencyLogFilterType_Invalid", Value=0)]
        GuildCurrencyLogFilterType_Invalid = 0,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildActionIncome", Value=1)]
        GuildCurrencyLogFilterType_TradeMoneyGuildActionIncome = 1,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyDonateIncome", Value=2)]
        GuildCurrencyLogFilterType_TradeMoneyDonateIncome = 2,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildGiftBagIncome", Value=3)]
        GuildCurrencyLogFilterType_TradeMoneyGuildGiftBagIncome = 3,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyPurchaseOutcome", Value=4)]
        GuildCurrencyLogFilterType_TradeMoneyPurchaseOutcome = 4,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildTradeOutcome", Value=5)]
        GuildCurrencyLogFilterType_TradeMoneyGuildTradeOutcome = 5,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneySpeedUpOutcome", Value=6)]
        GuildCurrencyLogFilterType_TradeMoneySpeedUpOutcome = 6,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyTacticalPointOutcome", Value=7)]
        GuildCurrencyLogFilterType_TradeMoneyTacticalPointOutcome = 7,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildRedeployOutcome", Value=8)]
        GuildCurrencyLogFilterType_TradeMoneyGuildRedeployOutcome = 8,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyOccupyAintenanceOutcome", Value=9)]
        GuildCurrencyLogFilterType_TradeMoneyOccupyAintenanceOutcome = 9,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyWarOnSovereignOutcome", Value=10)]
        GuildCurrencyLogFilterType_TradeMoneyWarOnSovereignOutcome = 10,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyAllianceCreateOutcome", Value=11)]
        GuildCurrencyLogFilterType_TradeMoneyAllianceCreateOutcome = 11,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyAllianceNameChangeOutcome", Value=12)]
        GuildCurrencyLogFilterType_TradeMoneyAllianceNameChangeOutcome = 12,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyAllianceLogoChangeOutcome", Value=13)]
        GuildCurrencyLogFilterType_TradeMoneyAllianceLogoChangeOutcome = 13,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildCompensationOutcome", Value=14)]
        GuildCurrencyLogFilterType_TradeMoneyGuildCompensationOutcome = 14,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseOrderEditIncome", Value=15)]
        GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseOrderEditIncome = 15,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseOrderEditOutcome", Value=0x10)]
        GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseOrderEditOutcome = 0x10,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseTransportFailIncome", Value=0x11)]
        GuildCurrencyLogFilterType_TradeMoneyGuildTradePurchaseTransportFailIncome = 0x11,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyGuildTradeTransportSucceedIncome", Value=0x12)]
        GuildCurrencyLogFilterType_TradeMoneyGuildTradeTransportSucceedIncome = 0x12,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TradeMoneyProduceOutcome", Value=0x13)]
        GuildCurrencyLogFilterType_TradeMoneyProduceOutcome = 0x13,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TacticalPointGuildActionIncome", Value=20)]
        GuildCurrencyLogFilterType_TacticalPointGuildActionIncome = 20,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TacticalPointDonateIncome", Value=0x15)]
        GuildCurrencyLogFilterType_TacticalPointDonateIncome = 0x15,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_TacticalPointLearnOutcome", Value=0x16)]
        GuildCurrencyLogFilterType_TacticalPointLearnOutcome = 0x16,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_InformationPointOccupyIncome", Value=0x17)]
        GuildCurrencyLogFilterType_InformationPointOccupyIncome = 0x17,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_InformationPointDonateIncome", Value=0x18)]
        GuildCurrencyLogFilterType_InformationPointDonateIncome = 0x18,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_InformationPointGuildActionOutcome", Value=0x19)]
        GuildCurrencyLogFilterType_InformationPointGuildActionOutcome = 0x19,
        [ProtoEnum(Name="GuildCurrencyLogFilterType_Max", Value=0x1a)]
        GuildCurrencyLogFilterType_Max = 0x1a
    }
}

