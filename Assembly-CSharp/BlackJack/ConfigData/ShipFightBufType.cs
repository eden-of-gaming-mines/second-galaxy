﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="ShipFightBufType")]
    public enum ShipFightBufType
    {
        [ProtoEnum(Name="ShipFightBufType_None", Value=0)]
        ShipFightBufType_None = 0,
        [ProtoEnum(Name="ShipFightBufType_Attack", Value=1)]
        ShipFightBufType_Attack = 1,
        [ProtoEnum(Name="ShipFightBufType_Defense", Value=2)]
        ShipFightBufType_Defense = 2,
        [ProtoEnum(Name="ShipFightBufType_Electric", Value=3)]
        ShipFightBufType_Electric = 3,
        [ProtoEnum(Name="ShipFightBufType_Move", Value=4)]
        ShipFightBufType_Move = 4,
        [ProtoEnum(Name="ShipFightBufType_Max", Value=5)]
        ShipFightBufType_Max = 5
    }
}

