﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDeviceSystemInfo")]
    public class ConfigDataDeviceSystemInfo : IExtensible
    {
        private int _ID;
        private string _GpuNameMatchStr;
        private BlackJack.ConfigData.DeviceRatingLevel _DeviceRatingLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_GpuNameMatchStr;
        private static DelegateBridge __Hotfix_set_GpuNameMatchStr;
        private static DelegateBridge __Hotfix_get_DeviceRatingLevel;
        private static DelegateBridge __Hotfix_set_DeviceRatingLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="GpuNameMatchStr", DataFormat=DataFormat.Default)]
        public string GpuNameMatchStr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="DeviceRatingLevel", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.DeviceRatingLevel DeviceRatingLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

