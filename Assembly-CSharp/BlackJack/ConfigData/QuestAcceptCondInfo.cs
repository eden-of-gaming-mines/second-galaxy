﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="QuestAcceptCondInfo")]
    public class QuestAcceptCondInfo : IExtensible
    {
        private QuestAcceptCondType _CondType;
        private int _P1;
        private int _P2;
        private int _P3;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CondType;
        private static DelegateBridge __Hotfix_set_CondType;
        private static DelegateBridge __Hotfix_get_P1;
        private static DelegateBridge __Hotfix_set_P1;
        private static DelegateBridge __Hotfix_get_P2;
        private static DelegateBridge __Hotfix_set_P2;
        private static DelegateBridge __Hotfix_get_P3;
        private static DelegateBridge __Hotfix_set_P3;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CondType", DataFormat=DataFormat.TwosComplement)]
        public QuestAcceptCondType CondType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="P1", DataFormat=DataFormat.TwosComplement)]
        public int P1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="P2", DataFormat=DataFormat.TwosComplement)]
        public int P2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="P3", DataFormat=DataFormat.TwosComplement)]
        public int P3
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

