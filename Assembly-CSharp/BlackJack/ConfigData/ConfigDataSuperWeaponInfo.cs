﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSuperWeaponInfo")]
    public class ConfigDataSuperWeaponInfo : IExtensible
    {
        private int _ID;
        private WeaponCategory _Category;
        private int _AmmoId;
        private uint _ChargeTime;
        private uint _UnitCount;
        private int _TurretCount;
        private int _CD4NextUnit;
        private int _CD4NextTurret;
        private int _CD4NextWave;
        private uint _WaveCount;
        private uint _DamageBasic;
        private float _HitBasic;
        private float _FireCtrlAccuracy;
        private int _LaserDamageInfoId;
        private int _FireRangeMax;
        private float _PropertyCriticalBasic;
        private float _PropertyCriticalDamageMulti;
        private int _BulletFlySpeed;
        private uint _MWEnergyNeed;
        private uint _MWEnergyGrow;
        private float _AddtionParam1;
        private float _AddtionParam2;
        private string _IconResPath;
        private float _AddEnergyOnHitByEnemy;
        private float _AddEnergyOnEnergyUse;
        private float _ChargeAndChannelBreakFactor;
        private float _TransverseVelocity;
        private readonly List<float> _DrivingLicenseDamageMulti;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_AmmoId;
        private static DelegateBridge __Hotfix_set_AmmoId;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_UnitCount;
        private static DelegateBridge __Hotfix_set_UnitCount;
        private static DelegateBridge __Hotfix_get_TurretCount;
        private static DelegateBridge __Hotfix_set_TurretCount;
        private static DelegateBridge __Hotfix_get_CD4NextUnit;
        private static DelegateBridge __Hotfix_set_CD4NextUnit;
        private static DelegateBridge __Hotfix_get_CD4NextTurret;
        private static DelegateBridge __Hotfix_set_CD4NextTurret;
        private static DelegateBridge __Hotfix_get_CD4NextWave;
        private static DelegateBridge __Hotfix_set_CD4NextWave;
        private static DelegateBridge __Hotfix_get_WaveCount;
        private static DelegateBridge __Hotfix_set_WaveCount;
        private static DelegateBridge __Hotfix_get_DamageBasic;
        private static DelegateBridge __Hotfix_set_DamageBasic;
        private static DelegateBridge __Hotfix_get_HitBasic;
        private static DelegateBridge __Hotfix_set_HitBasic;
        private static DelegateBridge __Hotfix_get_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_set_FireCtrlAccuracy;
        private static DelegateBridge __Hotfix_get_LaserDamageInfoId;
        private static DelegateBridge __Hotfix_set_LaserDamageInfoId;
        private static DelegateBridge __Hotfix_get_FireRangeMax;
        private static DelegateBridge __Hotfix_set_FireRangeMax;
        private static DelegateBridge __Hotfix_get_PropertyCriticalBasic;
        private static DelegateBridge __Hotfix_set_PropertyCriticalBasic;
        private static DelegateBridge __Hotfix_get_PropertyCriticalDamageMulti;
        private static DelegateBridge __Hotfix_set_PropertyCriticalDamageMulti;
        private static DelegateBridge __Hotfix_get_BulletFlySpeed;
        private static DelegateBridge __Hotfix_set_BulletFlySpeed;
        private static DelegateBridge __Hotfix_get_MWEnergyNeed;
        private static DelegateBridge __Hotfix_set_MWEnergyNeed;
        private static DelegateBridge __Hotfix_get_MWEnergyGrow;
        private static DelegateBridge __Hotfix_set_MWEnergyGrow;
        private static DelegateBridge __Hotfix_get_AddtionParam1;
        private static DelegateBridge __Hotfix_set_AddtionParam1;
        private static DelegateBridge __Hotfix_get_AddtionParam2;
        private static DelegateBridge __Hotfix_set_AddtionParam2;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_AddEnergyOnHitByEnemy;
        private static DelegateBridge __Hotfix_set_AddEnergyOnHitByEnemy;
        private static DelegateBridge __Hotfix_get_AddEnergyOnEnergyUse;
        private static DelegateBridge __Hotfix_set_AddEnergyOnEnergyUse;
        private static DelegateBridge __Hotfix_get_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_set_ChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_get_TransverseVelocity;
        private static DelegateBridge __Hotfix_set_TransverseVelocity;
        private static DelegateBridge __Hotfix_get_DrivingLicenseDamageMulti;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public WeaponCategory Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="AmmoId", DataFormat=DataFormat.TwosComplement)]
        public int AmmoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ChargeTime", DataFormat=DataFormat.TwosComplement)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="UnitCount", DataFormat=DataFormat.TwosComplement)]
        public uint UnitCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="TurretCount", DataFormat=DataFormat.TwosComplement)]
        public int TurretCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CD4NextUnit", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextUnit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="CD4NextTurret", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextTurret
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="CD4NextWave", DataFormat=DataFormat.TwosComplement)]
        public int CD4NextWave
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="WaveCount", DataFormat=DataFormat.TwosComplement)]
        public uint WaveCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DamageBasic", DataFormat=DataFormat.TwosComplement)]
        public uint DamageBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="HitBasic", DataFormat=DataFormat.FixedSize)]
        public float HitBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="FireCtrlAccuracy", DataFormat=DataFormat.FixedSize)]
        public float FireCtrlAccuracy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="LaserDamageInfoId", DataFormat=DataFormat.TwosComplement)]
        public int LaserDamageInfoId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="FireRangeMax", DataFormat=DataFormat.TwosComplement)]
        public int FireRangeMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="PropertyCriticalBasic", DataFormat=DataFormat.FixedSize)]
        public float PropertyCriticalBasic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=true, Name="PropertyCriticalDamageMulti", DataFormat=DataFormat.FixedSize)]
        public float PropertyCriticalDamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=true, Name="BulletFlySpeed", DataFormat=DataFormat.TwosComplement)]
        public int BulletFlySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, IsRequired=true, Name="MWEnergyNeed", DataFormat=DataFormat.TwosComplement)]
        public uint MWEnergyNeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="MWEnergyGrow", DataFormat=DataFormat.TwosComplement)]
        public uint MWEnergyGrow
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=true, Name="AddtionParam1", DataFormat=DataFormat.FixedSize)]
        public float AddtionParam1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=true, Name="AddtionParam2", DataFormat=DataFormat.FixedSize)]
        public float AddtionParam2
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1c, IsRequired=true, Name="AddEnergyOnHitByEnemy", DataFormat=DataFormat.FixedSize)]
        public float AddEnergyOnHitByEnemy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=true, Name="AddEnergyOnEnergyUse", DataFormat=DataFormat.FixedSize)]
        public float AddEnergyOnEnergyUse
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, IsRequired=true, Name="ChargeAndChannelBreakFactor", DataFormat=DataFormat.FixedSize)]
        public float ChargeAndChannelBreakFactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=true, Name="TransverseVelocity", DataFormat=DataFormat.FixedSize)]
        public float TransverseVelocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, Name="DrivingLicenseDamageMulti", DataFormat=DataFormat.FixedSize)]
        public List<float> DrivingLicenseDamageMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x21, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

