﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGESpectrumDataInfo")]
    public class ConfigDataGESpectrumDataInfo : IExtensible
    {
        private int _ID;
        private string _SpectrumName;
        private int _AppearProbability;
        private string _Color;
        private readonly List<int> _SubSpectrumList;
        private readonly List<int> _ResPathList;
        private readonly List<float> _SunColor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SpectrumName;
        private static DelegateBridge __Hotfix_set_SpectrumName;
        private static DelegateBridge __Hotfix_get_AppearProbability;
        private static DelegateBridge __Hotfix_set_AppearProbability;
        private static DelegateBridge __Hotfix_get_Color;
        private static DelegateBridge __Hotfix_set_Color;
        private static DelegateBridge __Hotfix_get_SubSpectrumList;
        private static DelegateBridge __Hotfix_get_ResPathList;
        private static DelegateBridge __Hotfix_get_SunColor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SpectrumName", DataFormat=DataFormat.Default)]
        public string SpectrumName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AppearProbability", DataFormat=DataFormat.TwosComplement)]
        public int AppearProbability
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Color", DataFormat=DataFormat.Default)]
        public string Color
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="SubSpectrumList", DataFormat=DataFormat.TwosComplement)]
        public List<int> SubSpectrumList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="ResPathList", DataFormat=DataFormat.TwosComplement)]
        public List<int> ResPathList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="SunColor", DataFormat=DataFormat.FixedSize)]
        public List<float> SunColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

