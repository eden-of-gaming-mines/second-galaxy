﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="EquipType")]
    public enum EquipType
    {
        [ProtoEnum(Name="EquipType_Booster", Value=1)]
        EquipType_Booster = 1,
        [ProtoEnum(Name="EquipType_BlinkExecutor", Value=2)]
        EquipType_BlinkExecutor = 2,
        [ProtoEnum(Name="EquipType_ShieldRepairer", Value=3)]
        EquipType_ShieldRepairer = 3,
        [ProtoEnum(Name="EquipType_ShieldDisturber", Value=4)]
        EquipType_ShieldDisturber = 4,
        [ProtoEnum(Name="EquipType_RangeDisturber", Value=5)]
        EquipType_RangeDisturber = 5,
        [ProtoEnum(Name="EquipType_WeaponDisturber", Value=6)]
        EquipType_WeaponDisturber = 6,
        [ProtoEnum(Name="EquipType_SpeedReducer", Value=7)]
        EquipType_SpeedReducer = 7,
        [ProtoEnum(Name="EquipType_EnergyDec", Value=8)]
        EquipType_EnergyDec = 8,
        [ProtoEnum(Name="EquipType_ExtraEnergyPackage", Value=9)]
        EquipType_ExtraEnergyPackage = 9,
        [ProtoEnum(Name="EquipType_RemoteShieldRepairer", Value=10)]
        EquipType_RemoteShieldRepairer = 10,
        [ProtoEnum(Name="EquipType_RemoteEnergyRepairer", Value=11)]
        EquipType_RemoteEnergyRepairer = 11,
        [ProtoEnum(Name="EquipType_TeamAtkAsst", Value=12)]
        EquipType_TeamAtkAsst = 12,
        [ProtoEnum(Name="EquipType_TeamDefAsst", Value=13)]
        EquipType_TeamDefAsst = 13,
        [ProtoEnum(Name="EquipType_TeamElecAsst", Value=14)]
        EquipType_TeamElecAsst = 14,
        [ProtoEnum(Name="EquipType_TeamMovAsst", Value=15)]
        EquipType_TeamMovAsst = 15,
        [ProtoEnum(Name="EquipType_Invisibility", Value=0x10)]
        EquipType_Invisibility = 0x10,
        [ProtoEnum(Name="EquipType_SelfWeaponModule", Value=0x11)]
        EquipType_SelfWeaponModule = 0x11,
        [ProtoEnum(Name="EquipType_SelfRangeModule", Value=0x12)]
        EquipType_SelfRangeModule = 0x12,
        [ProtoEnum(Name="EquipType_SelfDefModule", Value=0x13)]
        EquipType_SelfDefModule = 0x13,
        [ProtoEnum(Name="EquipType_BlinkBlocker", Value=20)]
        EquipType_BlinkBlocker = 20,
        [ProtoEnum(Name="EquipType_ShieldCreator", Value=0x15)]
        EquipType_ShieldCreator = 0x15,
        [ProtoEnum(Name="EquipType_SelfAtkAsst", Value=0x16)]
        EquipType_SelfAtkAsst = 0x16,
        [ProtoEnum(Name="EquipType_SelfRangeAsst", Value=0x17)]
        EquipType_SelfRangeAsst = 0x17,
        [ProtoEnum(Name="EquipType_SelfCritAsst", Value=0x18)]
        EquipType_SelfCritAsst = 0x18,
        [ProtoEnum(Name="EquipType_SelfAccuracyAsst", Value=0x19)]
        EquipType_SelfAccuracyAsst = 0x19,
        [ProtoEnum(Name="EquipType_SelfDefAsst", Value=0x1a)]
        EquipType_SelfDefAsst = 0x1a,
        [ProtoEnum(Name="EquipType_RemoteDefAsst", Value=0x1b)]
        EquipType_RemoteDefAsst = 0x1b,
        [ProtoEnum(Name="EquipType_RemoteMovAsst", Value=0x1c)]
        EquipType_RemoteMovAsst = 0x1c,
        [ProtoEnum(Name="EquipType_RemoteAtkDisturber", Value=0x1d)]
        EquipType_RemoteAtkDisturber = 0x1d,
        [ProtoEnum(Name="EquipType_RemoteAccuracyDisturber", Value=30)]
        EquipType_RemoteAccuracyDisturber = 30,
        [ProtoEnum(Name="EquipType_RemoteSignalDisturber", Value=0x1f)]
        EquipType_RemoteSignalDisturber = 0x1f,
        [ProtoEnum(Name="EquipType_RemoteDefDisturber", Value=0x20)]
        EquipType_RemoteDefDisturber = 0x20,
        [ProtoEnum(Name="EquipType_RemoteMovDisturber", Value=0x21)]
        EquipType_RemoteMovDisturber = 0x21,
        [ProtoEnum(Name="EquipType_TeamRangeAsst", Value=0x22)]
        EquipType_TeamRangeAsst = 0x22,
        [ProtoEnum(Name="EquipType_TeamCritAsst", Value=0x23)]
        EquipType_TeamCritAsst = 0x23,
        [ProtoEnum(Name="EquipType_TeamAccuracyAsst", Value=0x24)]
        EquipType_TeamAccuracyAsst = 0x24,
        [ProtoEnum(Name="EquipType_TeamAtkDisturber", Value=0x25)]
        EquipType_TeamAtkDisturber = 0x25,
        [ProtoEnum(Name="EquipType_TeamRangeDisturber", Value=0x26)]
        EquipType_TeamRangeDisturber = 0x26,
        [ProtoEnum(Name="EquipType_TeamAccuracyDisturber", Value=0x27)]
        EquipType_TeamAccuracyDisturber = 0x27,
        [ProtoEnum(Name="EquipType_TeamSignalDisturber", Value=40)]
        EquipType_TeamSignalDisturber = 40,
        [ProtoEnum(Name="EquipType_TeamDefDisturber", Value=0x29)]
        EquipType_TeamDefDisturber = 0x29,
        [ProtoEnum(Name="EquipType_TeamMovDisturber", Value=0x2a)]
        EquipType_TeamMovDisturber = 0x2a,
        [ProtoEnum(Name="EquipType_TeamEnergyDec", Value=0x2b)]
        EquipType_TeamEnergyDec = 0x2b,
        [ProtoEnum(Name="EquipType_TeamBlinkBlocker", Value=0x2c)]
        EquipType_TeamBlinkBlocker = 0x2c,
        [ProtoEnum(Name="EquipType_SpaceSignalGravityScanner", Value=0x2d)]
        EquipType_SpaceSignalGravityScanner = 0x2d,
        [ProtoEnum(Name="EquipType_SpaceSignalMicroWaveScanner", Value=0x2e)]
        EquipType_SpaceSignalMicroWaveScanner = 0x2e,
        [ProtoEnum(Name="EquipType_SpaceSignalRadiationScanner", Value=0x2f)]
        EquipType_SpaceSignalRadiationScanner = 0x2f,
        [ProtoEnum(Name="EquipType_Max", Value=0x30)]
        EquipType_Max = 0x30
    }
}

