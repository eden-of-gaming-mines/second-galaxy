﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSolarSystemQuestPoolCountInfo")]
    public class ConfigDataSolarSystemQuestPoolCountInfo : IExtensible
    {
        private int _ID;
        private int _Difficult;
        private int _MinPoolCount;
        private int _MaxPoolCount;
        private float _ExpReduce;
        private float _CurrencyReduce;
        private string _IconResString;
        private float _SpaceSignalPossibilityReduce;
        private string _SpaceSignalIconResString;
        private string _NameStrKey;
        private string _DescStrKey;
        private string _SpaceSignalNameStrKey;
        private string _SpaceSignalDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_Difficult;
        private static DelegateBridge __Hotfix_set_Difficult;
        private static DelegateBridge __Hotfix_get_MinPoolCount;
        private static DelegateBridge __Hotfix_set_MinPoolCount;
        private static DelegateBridge __Hotfix_get_MaxPoolCount;
        private static DelegateBridge __Hotfix_set_MaxPoolCount;
        private static DelegateBridge __Hotfix_get_ExpReduce;
        private static DelegateBridge __Hotfix_set_ExpReduce;
        private static DelegateBridge __Hotfix_get_CurrencyReduce;
        private static DelegateBridge __Hotfix_set_CurrencyReduce;
        private static DelegateBridge __Hotfix_get_IconResString;
        private static DelegateBridge __Hotfix_set_IconResString;
        private static DelegateBridge __Hotfix_get_SpaceSignalPossibilityReduce;
        private static DelegateBridge __Hotfix_set_SpaceSignalPossibilityReduce;
        private static DelegateBridge __Hotfix_get_SpaceSignalIconResString;
        private static DelegateBridge __Hotfix_set_SpaceSignalIconResString;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_SpaceSignalNameStrKey;
        private static DelegateBridge __Hotfix_set_SpaceSignalNameStrKey;
        private static DelegateBridge __Hotfix_get_SpaceSignalDescStrKey;
        private static DelegateBridge __Hotfix_set_SpaceSignalDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Difficult", DataFormat=DataFormat.TwosComplement)]
        public int Difficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="MinPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int MinPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MaxPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int MaxPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ExpReduce", DataFormat=DataFormat.FixedSize)]
        public float ExpReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="CurrencyReduce", DataFormat=DataFormat.FixedSize)]
        public float CurrencyReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IconResString", DataFormat=DataFormat.Default)]
        public string IconResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="SpaceSignalPossibilityReduce", DataFormat=DataFormat.FixedSize)]
        public float SpaceSignalPossibilityReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="SpaceSignalIconResString", DataFormat=DataFormat.Default)]
        public string SpaceSignalIconResString
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="SpaceSignalNameStrKey", DataFormat=DataFormat.Default)]
        public string SpaceSignalNameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="SpaceSignalDescStrKey", DataFormat=DataFormat.Default)]
        public string SpaceSignalDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

