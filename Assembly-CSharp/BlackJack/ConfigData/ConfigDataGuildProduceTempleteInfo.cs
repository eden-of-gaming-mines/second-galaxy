﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGuildProduceTempleteInfo")]
    public class ConfigDataGuildProduceTempleteInfo : IExtensible
    {
        private int _ID;
        private GuildProduceCategory _TempleteCategory;
        private GuildProduceType _TempleteType;
        private BlackJack.ConfigData.SubRankType _SubRankType;
        private StoreItemType _ProduceCategory;
        private int _ProductId;
        private int _ProductCount;
        private readonly List<CostInfo> _CostList;
        private int _TimeCost;
        private int _SovereignGalaxyCount;
        private string _IconResPath;
        private string _NameStrKey;
        private string _DescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_TempleteCategory;
        private static DelegateBridge __Hotfix_set_TempleteCategory;
        private static DelegateBridge __Hotfix_get_TempleteType;
        private static DelegateBridge __Hotfix_set_TempleteType;
        private static DelegateBridge __Hotfix_get_SubRankType;
        private static DelegateBridge __Hotfix_set_SubRankType;
        private static DelegateBridge __Hotfix_get_ProduceCategory;
        private static DelegateBridge __Hotfix_set_ProduceCategory;
        private static DelegateBridge __Hotfix_get_ProductId;
        private static DelegateBridge __Hotfix_set_ProductId;
        private static DelegateBridge __Hotfix_get_ProductCount;
        private static DelegateBridge __Hotfix_set_ProductCount;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_get_TimeCost;
        private static DelegateBridge __Hotfix_set_TimeCost;
        private static DelegateBridge __Hotfix_get_SovereignGalaxyCount;
        private static DelegateBridge __Hotfix_set_SovereignGalaxyCount;
        private static DelegateBridge __Hotfix_get_IconResPath;
        private static DelegateBridge __Hotfix_set_IconResPath;
        private static DelegateBridge __Hotfix_get_NameStrKey;
        private static DelegateBridge __Hotfix_set_NameStrKey;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="TempleteCategory", DataFormat=DataFormat.TwosComplement)]
        public GuildProduceCategory TempleteCategory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="TempleteType", DataFormat=DataFormat.TwosComplement)]
        public GuildProduceType TempleteType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SubRankType", DataFormat=DataFormat.TwosComplement)]
        public BlackJack.ConfigData.SubRankType SubRankType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ProduceCategory", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType ProduceCategory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ProductId", DataFormat=DataFormat.TwosComplement)]
        public int ProductId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="ProductCount", DataFormat=DataFormat.TwosComplement)]
        public int ProductCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="CostList", DataFormat=DataFormat.Default)]
        public List<CostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="TimeCost", DataFormat=DataFormat.TwosComplement)]
        public int TimeCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="SovereignGalaxyCount", DataFormat=DataFormat.TwosComplement)]
        public int SovereignGalaxyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="IconResPath", DataFormat=DataFormat.Default)]
        public string IconResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="NameStrKey", DataFormat=DataFormat.Default)]
        public string NameStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

