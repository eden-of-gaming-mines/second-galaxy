﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataDevelopmentProjectInfo")]
    public class ConfigDataDevelopmentProjectInfo : IExtensible
    {
        private int _ID;
        private StoreItemType _StoreItemTypeFilter;
        private int _FilterID;
        private int _MaterialFilterID;
        private DevelopmentType _DevelopmentTypeFilter;
        private readonly List<DevelopmentCostItemList> _DevelopmentCostItemFilterList;
        private readonly List<CostInfo> _DevelopmentCostList;
        private readonly List<IntWeightListItem> _DevelopmentDropWeightList;
        private string _NameIcon;
        private readonly List<BlackJack.ConfigData.ResultRange> _ResultRange;
        private string _DescStrKey;
        private string _DetailDescStrKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_StoreItemTypeFilter;
        private static DelegateBridge __Hotfix_set_StoreItemTypeFilter;
        private static DelegateBridge __Hotfix_get_FilterID;
        private static DelegateBridge __Hotfix_set_FilterID;
        private static DelegateBridge __Hotfix_get_MaterialFilterID;
        private static DelegateBridge __Hotfix_set_MaterialFilterID;
        private static DelegateBridge __Hotfix_get_DevelopmentTypeFilter;
        private static DelegateBridge __Hotfix_set_DevelopmentTypeFilter;
        private static DelegateBridge __Hotfix_get_DevelopmentCostItemFilterList;
        private static DelegateBridge __Hotfix_get_DevelopmentCostList;
        private static DelegateBridge __Hotfix_get_DevelopmentDropWeightList;
        private static DelegateBridge __Hotfix_get_NameIcon;
        private static DelegateBridge __Hotfix_set_NameIcon;
        private static DelegateBridge __Hotfix_get_ResultRange;
        private static DelegateBridge __Hotfix_get_DescStrKey;
        private static DelegateBridge __Hotfix_set_DescStrKey;
        private static DelegateBridge __Hotfix_get_DetailDescStrKey;
        private static DelegateBridge __Hotfix_set_DetailDescStrKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StoreItemTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public StoreItemType StoreItemTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="FilterID", DataFormat=DataFormat.TwosComplement)]
        public int FilterID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="MaterialFilterID", DataFormat=DataFormat.TwosComplement)]
        public int MaterialFilterID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DevelopmentTypeFilter", DataFormat=DataFormat.TwosComplement)]
        public DevelopmentType DevelopmentTypeFilter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="DevelopmentCostItemFilterList", DataFormat=DataFormat.Default)]
        public List<DevelopmentCostItemList> DevelopmentCostItemFilterList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="DevelopmentCostList", DataFormat=DataFormat.Default)]
        public List<CostInfo> DevelopmentCostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="DevelopmentDropWeightList", DataFormat=DataFormat.Default)]
        public List<IntWeightListItem> DevelopmentDropWeightList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="NameIcon", DataFormat=DataFormat.Default)]
        public string NameIcon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="ResultRange", DataFormat=DataFormat.Default)]
        public List<BlackJack.ConfigData.ResultRange> ResultRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DescStrKey", DataFormat=DataFormat.Default)]
        public string DescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="DetailDescStrKey", DataFormat=DataFormat.Default)]
        public string DetailDescStrKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

