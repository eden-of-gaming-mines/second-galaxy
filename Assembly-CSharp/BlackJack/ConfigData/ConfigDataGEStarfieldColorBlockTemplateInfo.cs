﻿namespace BlackJack.ConfigData
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataGEStarfieldColorBlockTemplateInfo")]
    public class ConfigDataGEStarfieldColorBlockTemplateInfo : IExtensible
    {
        private int _ID;
        private string _ColorBlockMeshResPath;
        private string _BorderLineMeshResPath;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_ColorBlockMeshResPath;
        private static DelegateBridge __Hotfix_set_ColorBlockMeshResPath;
        private static DelegateBridge __Hotfix_get_BorderLineMeshResPath;
        private static DelegateBridge __Hotfix_set_BorderLineMeshResPath;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ColorBlockMeshResPath", DataFormat=DataFormat.Default)]
        public string ColorBlockMeshResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="BorderLineMeshResPath", DataFormat=DataFormat.Default)]
        public string BorderLineMeshResPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

