﻿namespace BlackJack.ConfigData
{
    using ProtoBuf;
    using System;

    [ProtoContract(Name="DevelopmentNodeIndex")]
    public enum DevelopmentNodeIndex
    {
        [ProtoEnum(Name="DevelopmentNodeIndex_Project", Value=0)]
        DevelopmentNodeIndex_Project = 0,
        [ProtoEnum(Name="DevelopmentNodeIndex_Secondary", Value=1)]
        DevelopmentNodeIndex_Secondary = 1,
        [ProtoEnum(Name="DevelopmentNodeIndex_Tertiary", Value=2)]
        DevelopmentNodeIndex_Tertiary = 2,
        [ProtoEnum(Name="DevelopmentNodeIndex_Max", Value=3)]
        DevelopmentNodeIndex_Max = 3
    }
}

