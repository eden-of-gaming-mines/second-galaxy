﻿namespace BlackJack.Utils
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TinyCorutineHelper
    {
        private readonly LinkedList<IEnumerator> m_corutineList = new LinkedList<IEnumerator>();
        private bool m_cancel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCorutine_1;
        private static DelegateBridge __Hotfix_StartCorutine_0;
        private static DelegateBridge __Hotfix_CancelAll;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_MoveNext;

        [MethodImpl(0x8000)]
        public TinyCorutineHelper()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void CancelAll()
        {
        }

        [MethodImpl(0x8000)]
        protected bool MoveNext(IEnumerator iter)
        {
            DelegateBridge bridge = __Hotfix_MoveNext;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, iter);
            }
            if (iter == null)
            {
                return false;
            }
            if (iter.Current != null)
            {
                IEnumerator current = iter.Current as IEnumerator;
                if (this.MoveNext(current))
                {
                    return true;
                }
            }
            return iter.MoveNext();
        }

        [MethodImpl(0x8000)]
        public void StartCorutine(IEnumerator corutine)
        {
            DelegateBridge bridge = __Hotfix_StartCorutine_0;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, corutine);
            }
            else
            {
                this.m_corutineList.AddLast(corutine);
            }
        }

        [MethodImpl(0x8000)]
        public void StartCorutine(Func<IEnumerator> corutine)
        {
            DelegateBridge bridge = __Hotfix_StartCorutine_1;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, corutine);
            }
            else
            {
                this.m_corutineList.AddLast(corutine());
            }
        }

        [MethodImpl(0x8000)]
        public bool Tick()
        {
            LinkedListNode<IEnumerator> next;
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            if (this.m_corutineList.Count == 0)
            {
                return false;
            }
            for (LinkedListNode<IEnumerator> node = this.m_corutineList.First; (node != null) && !this.m_cancel; node = next)
            {
                next = node.Next;
                if (!this.MoveNext(node.Value))
                {
                    this.m_corutineList.Remove(node);
                }
            }
            if (this.m_cancel)
            {
                this.m_corutineList.Clear();
                this.m_cancel = false;
            }
            return true;
        }
    }
}

