﻿namespace BlackJack.LibClient
{
    using System;

    public static class ProtoConst
    {
        public const int MaxPackageLength = 0x10000;
        public const int ZipBufferMinLength = 400;
    }
}

