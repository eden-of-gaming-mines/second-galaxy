﻿namespace BlackJack.LibClient
{
    using System;

    public enum LocalMsgId
    {
        CCMSGConnectionReady = 0x2329,
        CCMSGConnectionBreak = 0x232a,
        CCMSGConnectionFailure = 0x232b,
        CCMSGConnectionSendFailure = 0x232c,
        CCMSGConnectionRecvFailure = 0x232d
    }
}

