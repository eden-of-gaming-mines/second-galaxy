﻿namespace BlackJack.LibClient.Protocol
{
    using System;

    public interface IProtoProvider
    {
        int GetIdByType(Type vType);
        Type GetTypeById(int vId);
    }
}

