﻿namespace BlackJack.LibClient
{
    using System;

    public enum ConnectionState
    {
        None,
        Connecting,
        Established,
        Disconnecting,
        Closed
    }
}

