﻿namespace BlackJack.LibClient
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CCMSGConnectionRecvFailure")]
    public class CCMSGConnectionRecvFailure
    {
        private int _MessageId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <ExceptionInfo>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MessageId;
        private static DelegateBridge __Hotfix_set_MessageId;
        private static DelegateBridge __Hotfix_get_ExceptionInfo;
        private static DelegateBridge __Hotfix_set_ExceptionInfo;

        [ProtoMember(1, IsRequired=true, Name="MessageId", DataFormat=DataFormat.Default)]
        public int MessageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ExceptionInfo", DataFormat=DataFormat.Default)]
        public string ExceptionInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

