﻿namespace BlackJack.LibClient
{
    using IL;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public class MessageBlock
    {
        private byte[] m_buffer;
        private int m_rdPtr;
        private int m_wrPtr;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Write;
        private static DelegateBridge __Hotfix_ReadInt32;
        private static DelegateBridge __Hotfix_ReadUInt16;
        private static DelegateBridge __Hotfix_PeekInt32;
        private static DelegateBridge __Hotfix_ReadPtr;
        private static DelegateBridge __Hotfix_GetReadStream;
        private static DelegateBridge __Hotfix_Crunch;
        private static DelegateBridge __Hotfix_get_Length;
        private static DelegateBridge __Hotfix_get_Space;

        [MethodImpl(0x8000)]
        public MessageBlock(int maxLength)
        {
        }

        [MethodImpl(0x8000)]
        public void Crunch()
        {
        }

        [MethodImpl(0x8000)]
        public MemoryStream GetReadStream(int dataLen)
        {
        }

        [MethodImpl(0x8000)]
        public int PeekInt32()
        {
        }

        [MethodImpl(0x8000)]
        public int ReadInt32()
        {
        }

        [MethodImpl(0x8000)]
        public int ReadPtr(int ahOffset)
        {
        }

        [MethodImpl(0x8000)]
        public ushort ReadUInt16()
        {
        }

        [MethodImpl(0x8000)]
        public int Write(byte[] dataBlock, int dataLen)
        {
        }

        public int Length
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int Space
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

