﻿namespace BlackJack.LibClient
{
    using BlackJack.LibClient.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Remoting.Contexts;
    using System.Threading;

    [Synchronization]
    public class Connection
    {
        private int m_currDecodeMsgSeq;
        private DateTime m_nextPrintLogTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ConnectionState <State>k__BackingField;
        private IProtoProvider m_provider;
        private Socket m_socket;
        private Queue<KeyValuePair<int, object>> m_recvQueue;
        private MessageBlock m_recvCache;
        private int m_lastRecvCacheLength;
        private IPEndPoint m_ipEndPoint;
        private SocketAsyncEventArgs m_connEventArg;
        private SocketAsyncEventArgs m_receiveEventArg;
        private Timer m_disconnectTimer;
        private Func<Stream, Type, int, object> m_messageDeserializeAction;
        private bool m_needLog;
        private bool m_decodeMsgOnGetMessage;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnLogPrint;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_StartConnection;
        private static DelegateBridge __Hotfix_Disconnect;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_GetMessagePair;
        private static DelegateBridge __Hotfix_SendMessage;
        private static DelegateBridge __Hotfix_OnCompletedForSend;
        private static DelegateBridge __Hotfix_OnCompletedForSendImpl;
        private static DelegateBridge __Hotfix_OnCompletedForConnect;
        private static DelegateBridge __Hotfix_OnCompletedForReceive;
        private static DelegateBridge __Hotfix_WriteMsg2RecvCache;
        private static DelegateBridge __Hotfix_TryDecodeMsg;
        private static DelegateBridge __Hotfix_GetIPv6String;
        private static DelegateBridge __Hotfix_GetIpType;
        private static DelegateBridge __Hotfix_ParseServerAdressFromDomain;
        private static DelegateBridge __Hotfix_FireEventOnLogPrint;
        private static DelegateBridge __Hotfix_GetEndpoint;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_add_EventOnLogPrint;
        private static DelegateBridge __Hotfix_remove_EventOnLogPrint;

        public event Action<string> EventOnLogPrint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Connection(IProtoProvider provider, Func<Stream, Type, int, object> deserializeMessageAction = null, bool needLog = false, bool decodeMsgOnGetMessage = true)
        {
        }

        [MethodImpl(0x8000)]
        public void Close()
        {
        }

        [MethodImpl(0x8000)]
        public void Disconnect()
        {
        }

        [MethodImpl(0x8000)]
        private void FireEventOnLogPrint(string callFun, string log = "")
        {
        }

        [MethodImpl(0x8000)]
        public int GetEndpoint()
        {
        }

        [MethodImpl(0x8000)]
        private void GetIpType(string serverIp, string serverPorts, out string newServerIp, out AddressFamily mIpType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetIPv6String(string mHost, string mPort)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<int, object> GetMessagePair()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(string remoteAddress, int remotePort, string remoteDomain = "")
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompletedForConnect(object sender, SocketAsyncEventArgs eventArgs)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompletedForReceive(object sender, SocketAsyncEventArgs eventArgs)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompletedForSend(object sender, SocketAsyncEventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCompletedForSendImpl(SocketAsyncEventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        public IPAddress ParseServerAdressFromDomain(string domain)
        {
        }

        [MethodImpl(0x8000)]
        public void SendMessage(object msg, ulong? key = new ulong?())
        {
        }

        [MethodImpl(0x8000)]
        private void StartConnection()
        {
        }

        [MethodImpl(0x8000)]
        private void TryDecodeMsg()
        {
        }

        [MethodImpl(0x8000)]
        private void WriteMsg2RecvCache(object msg)
        {
        }

        public ConnectionState State
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <Disconnect>c__AnonStorey0
        {
            internal Socket oldSocket;
            internal Connection $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(object o)
            {
            }
        }
    }
}

