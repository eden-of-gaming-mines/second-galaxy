﻿namespace BlackJack.LibClient
{
    using BlackJack.LibClient.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class ProtoHelper
    {
        private static DelegateBridge __Hotfix_DecodeMessage;
        private static DelegateBridge __Hotfix_EncodeMessage;
        private static DelegateBridge __Hotfix_EncryptBufferByXor;

        [MethodImpl(0x8000)]
        public static object DecodeMessage(MessageBlock recvBuffer, IProtoProvider protoProvider, out int msgId, Func<Stream, Type, int, object> deserializeMessageAction = null, bool needLog = false)
        {
        }

        [MethodImpl(0x8000)]
        public static ArraySegment<byte> EncodeMessage(object vMsg, IProtoProvider protoProvider, ulong? key = new ulong?())
        {
        }

        [MethodImpl(0x8000)]
        public static void EncryptBufferByXor(byte[] buffer, int offset, int datalen, ulong? key = new ulong?())
        {
        }
    }
}

