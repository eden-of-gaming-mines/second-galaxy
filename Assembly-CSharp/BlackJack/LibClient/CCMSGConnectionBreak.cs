﻿namespace BlackJack.LibClient
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CCMSGConnectionBreak")]
    public class CCMSGConnectionBreak
    {
        private int _MessageId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MessageId;
        private static DelegateBridge __Hotfix_set_MessageId;

        [ProtoMember(1, IsRequired=true, Name="MessageId", DataFormat=DataFormat.Default)]
        public int MessageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

