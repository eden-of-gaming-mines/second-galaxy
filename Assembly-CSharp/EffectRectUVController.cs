﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class EffectRectUVController : MonoBehaviour
{
    public int RowNumber;
    public int ColNumber;
    public bool IsLoop;
    public int StartFrame;
    public int EndFrame;
    public float DurationTime;
    public bool AutoPlayOnStart;
    private EffectRectUVController[] m_effectChilds;
    private bool m_isPlay;
    private float m_curFramePosX;
    private float m_curFramePosY;
    private float m_frameWidth;
    private float m_frameHeight;
    private float m_deltaTime;
    private float m_lastUpdateTime;
    private int m_totalFrame;
    private int m_curFrame;
    private RawImage m_rawImg;

    [MethodImpl(0x8000)]
    private void CalcCurFramePos()
    {
    }

    public bool IsPlaying() => 
        this.m_isPlay;

    [MethodImpl(0x8000)]
    private void PlayUVRectEffectToNextFrame()
    {
    }

    [MethodImpl(0x8000)]
    public void RePlayEffect()
    {
    }

    [MethodImpl(0x8000)]
    private void ShowCurrentFrame()
    {
    }

    [MethodImpl(0x8000)]
    private void Start()
    {
    }

    [MethodImpl(0x8000)]
    private void Update()
    {
    }
}

