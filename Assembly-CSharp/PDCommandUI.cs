﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PDCommandUI : MonoBehaviour
{
    public GridLayoutGroup Grid;
    public GameObject CommandBtnPrefab;
    public GameObject CommandInputPrefab;
    private string stringToEdit;
    private Dictionary<string, UnityAction> btnDataModelDic = new Dictionary<string, UnityAction>();
    private InputField inputCallVebViewAction;
    private InputField inputCallVebViewCustom;
    private InputField inputUserid;
    private InputField inputRoleid;
    private InputField inputServerid;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache0;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache1;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache2;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache3;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache4;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache5;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache6;
    [CompilerGenerated]
    private static UnityAction <>f__am$cache7;

    private void Awake()
    {
    }

    [MethodImpl(0x8000)]
    private void ConfigData()
    {
    }

    [MethodImpl(0x8000)]
    private void ConstructCommandBtn(string title, UnityAction clickEvent)
    {
    }

    [MethodImpl(0x8000)]
    private void ConstructCommandInput(string tips, out InputField input)
    {
    }

    private void OnGUI()
    {
    }

    [MethodImpl(0x8000)]
    private void Start()
    {
    }

    [CompilerGenerated]
    private sealed class <ConfigData>c__AnonStorey0
    {
        internal string goodsName;
        internal int goodsNumber;
        internal string goodsId;
        internal string goodsRegisterId;
        internal double goodsPrice;
        internal string goodsDescribe;

        [MethodImpl(0x8000)]
        internal void <>m__0()
        {
        }
    }
}

