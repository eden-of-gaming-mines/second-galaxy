﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EffectPlayManager : MonoBehaviour
{
    [HideInInspector]
    public List<TweenMain> TweenList;
    [HideInInspector]
    public TweenMain MaxTweenMain;

    [MethodImpl(0x8000)]
    public void AddEvent(UnityAction action)
    {
    }

    private void Awake()
    {
        this.GatherEffect();
    }

    [MethodImpl(0x8000)]
    public void GatherEffect()
    {
    }

    [MethodImpl(0x8000)]
    public void PlayTween()
    {
    }

    [MethodImpl(0x8000)]
    public void RePlayReverseTween()
    {
    }

    [MethodImpl(0x8000)]
    public void RePlayTween()
    {
    }

    [MethodImpl(0x8000)]
    public void ResetToBeginningTween()
    {
    }

    public void ResetToEndTween()
    {
    }
}

