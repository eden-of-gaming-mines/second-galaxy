﻿using BlackJack.ProjectX.Runtime.SDK;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

public class PDSDK : MonoBehaviour
{
    public Action<bool> m_eventInitCallback;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnForceUpdate;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action<bool> m_eventLoginCallback;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action<bool> m_eventLogoutCallback;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action m_eventOnMemoryWarning;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action eventSwitchUserSuccessCallback;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action<bool> m_eventOnPayCallback;
    public Action<bool> m_eventOnGetProductsList;
    public Action m_eventOnAllowBuySubscription;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action m_eventOnPayCancel;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action<string> m_eventOnSDKPromotingPaySuccess;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action m_eventOnSDKPromotingPayCancel;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action<bool> EventOnDoShareCallBack;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnDoShareCancel;
    public static Action m_eventDoQuestionSucceed;
    public static Action m_eventDoQuestionFailed;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnNoticeCenterStateSuccess;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnNoticeCenterStateFailed;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnInfoWebViewSuccess;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnBindGuestEnd;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action<bool> EventOnQRLoginCallBack;
    public List<string> m_LoginResult = new List<string>();
    private bool _isLogining;
    private bool _isInit;
    public const string CenterState_Info = "root_info";
    public Dictionary<string, DateTime> nextNoticeCenterStateTimeDic = new Dictionary<string, DateTime>();
    public Dictionary<string, JsonData> noticeCenterStateInfoDic = new Dictionary<string, JsonData>();
    public bool isDebug;
    public bool isVerfy;
    public bool IsShowCommandUI;
    public static JsonData goodlistjson;
    public string m_promotingPayGoodsRegisterId;
    public string stringToEdit = string.Empty;
    public const string CUSTOMPARAMS = "SecondGalaxy";
    public GameObject CommondUIPrefab;
    private string url = string.Empty;
    private string fileID = string.Empty;
    private static bool isLogin;
    private static PDSDK _instance;
    public Action m_onLogoutMsgBoxCanceled;
    public const string RecordPermission = "record";
    public const string CameraPermission = "camera";
    private static DelegateBridge __Hotfix_add_EventOnForceUpdate;
    private static DelegateBridge __Hotfix_remove_EventOnForceUpdate;
    private static DelegateBridge __Hotfix_add_m_eventLoginCallback;
    private static DelegateBridge __Hotfix_remove_m_eventLoginCallback;
    private static DelegateBridge __Hotfix_add_m_eventLogoutCallback;
    private static DelegateBridge __Hotfix_remove_m_eventLogoutCallback;
    private static DelegateBridge __Hotfix_add_m_eventOnMemoryWarning;
    private static DelegateBridge __Hotfix_remove_m_eventOnMemoryWarning;
    private static DelegateBridge __Hotfix_add_eventSwitchUserSuccessCallback;
    private static DelegateBridge __Hotfix_remove_eventSwitchUserSuccessCallback;
    private static DelegateBridge __Hotfix_add_m_eventOnPayCallback;
    private static DelegateBridge __Hotfix_remove_m_eventOnPayCallback;
    private static DelegateBridge __Hotfix_add_m_eventOnPayCancel;
    private static DelegateBridge __Hotfix_remove_m_eventOnPayCancel;
    private static DelegateBridge __Hotfix_add_m_eventOnSDKPromotingPaySuccess;
    private static DelegateBridge __Hotfix_remove_m_eventOnSDKPromotingPaySuccess;
    private static DelegateBridge __Hotfix_add_m_eventOnSDKPromotingPayCancel;
    private static DelegateBridge __Hotfix_remove_m_eventOnSDKPromotingPayCancel;
    private static DelegateBridge __Hotfix_add_EventOnDoShareCallBack;
    private static DelegateBridge __Hotfix_remove_EventOnDoShareCallBack;
    private static DelegateBridge __Hotfix_add_EventOnDoShareCancel;
    private static DelegateBridge __Hotfix_remove_EventOnDoShareCancel;
    private static DelegateBridge __Hotfix_add_EventOnNoticeCenterStateSuccess;
    private static DelegateBridge __Hotfix_remove_EventOnNoticeCenterStateSuccess;
    private static DelegateBridge __Hotfix_add_EventOnNoticeCenterStateFailed;
    private static DelegateBridge __Hotfix_remove_EventOnNoticeCenterStateFailed;
    private static DelegateBridge __Hotfix_add_EventOnInfoWebViewSuccess;
    private static DelegateBridge __Hotfix_remove_EventOnInfoWebViewSuccess;
    private static DelegateBridge __Hotfix_add_EventOnBindGuestEnd;
    private static DelegateBridge __Hotfix_remove_EventOnBindGuestEnd;
    private static DelegateBridge __Hotfix_add_EventOnQRLoginCallBack;
    private static DelegateBridge __Hotfix_remove_EventOnQRLoginCallBack;
    private static DelegateBridge __Hotfix_get_isInit;
    private static DelegateBridge __Hotfix_get_IsLogin;
    private static DelegateBridge __Hotfix_get_Instance;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_Login;
    private static DelegateBridge __Hotfix_ResetGuest;
    private static DelegateBridge __Hotfix_LoginWithType;
    private static DelegateBridge __Hotfix_Logout;
    private static DelegateBridge __Hotfix_StartGame;
    private static DelegateBridge __Hotfix_GetProductsList;
    private static DelegateBridge __Hotfix_DoPay;
    private static DelegateBridge __Hotfix_DoPayWithLimit;
    private static DelegateBridge __Hotfix_DoPromotingPay;
    private static DelegateBridge __Hotfix_DoPromotingWithLimitPay;
    private static DelegateBridge __Hotfix_doAddLocalPush;
    private static DelegateBridge __Hotfix_doFirstScreen;
    private static DelegateBridge __Hotfix_SwitchUser;
    private static DelegateBridge __Hotfix_userCenter;
    private static DelegateBridge __Hotfix_exit;
    private static DelegateBridge __Hotfix_GetPustToken;
    private static DelegateBridge __Hotfix_GetDeviceID;
    private static DelegateBridge __Hotfix_GetBattery;
    private static DelegateBridge __Hotfix_WebInvestigation;
    private static DelegateBridge __Hotfix_OpenInvestigation;
    private static DelegateBridge __Hotfix_PathOrder;
    private static DelegateBridge __Hotfix_ShowPayHelp;
    private static DelegateBridge __Hotfix_printGameEventLog;
    private static DelegateBridge __Hotfix_doSetExtData;
    private static DelegateBridge __Hotfix_getChannelID;
    private static DelegateBridge __Hotfix_doSaveImageToPhotoLibrary;
    private static DelegateBridge __Hotfix_doQQVIP;
    private static DelegateBridge __Hotfix_callWebView;
    private static DelegateBridge __Hotfix_clearLocalNotifications;
    private static DelegateBridge __Hotfix_callCustomerServiceWeb;
    private static DelegateBridge __Hotfix_callCustomerService;
    private static DelegateBridge __Hotfix_doshare;
    private static DelegateBridge __Hotfix_doGetImagePath;
    private static DelegateBridge __Hotfix_UserLoginType;
    private static DelegateBridge __Hotfix_doUnBind;
    private static DelegateBridge __Hotfix_doBindGuest;
    private static DelegateBridge __Hotfix_setSDKLangue;
    private static DelegateBridge __Hotfix_getSysCountry;
    private static DelegateBridge __Hotfix_getSysLangue;
    private static DelegateBridge __Hotfix_doOpenFBFanPage;
    private static DelegateBridge __Hotfix_getECID;
    private static DelegateBridge __Hotfix_isEuCountry;
    private static DelegateBridge __Hotfix_payGiftCard;
    private static DelegateBridge __Hotfix_requestPermission;
    private static DelegateBridge __Hotfix_checkPermission;
    private static DelegateBridge __Hotfix_noticeCenterState;
    private static DelegateBridge __Hotfix_doStartRecord;
    private static DelegateBridge __Hotfix_doStopRecord;
    private static DelegateBridge __Hotfix_doBoradcast;
    private static DelegateBridge __Hotfix_doOpenRequestReview;
    private static DelegateBridge __Hotfix_doStartQRLogin;
    private static DelegateBridge __Hotfix_onCheckGiftCardItemInfoSuccess;
    private static DelegateBridge __Hotfix_onPayGiftCardSuccess;
    private static DelegateBridge __Hotfix_onPayGiftCardFailed;
    private static DelegateBridge __Hotfix_onLoginSuccess;
    private static DelegateBridge __Hotfix_onLoginFailed;
    private static DelegateBridge __Hotfix_onLoginCancel;
    private static DelegateBridge __Hotfix_onLogoutSuccess;
    private static DelegateBridge __Hotfix_onLogoutFailed;
    private static DelegateBridge __Hotfix_onLogoutCancel;
    private static DelegateBridge __Hotfix_onGetProductsListSuccess;
    private static DelegateBridge __Hotfix_onGetProductsListFailed;
    private static DelegateBridge __Hotfix_onPaySuccess;
    private static DelegateBridge __Hotfix_onPayFailed;
    private static DelegateBridge __Hotfix_onPayCancel;
    private static DelegateBridge __Hotfix_onSDKPromotingPaySuccess;
    private static DelegateBridge __Hotfix_onSDKPromotingPayFailed;
    private static DelegateBridge __Hotfix_onSDKPromotingPayCancel;
    private static DelegateBridge __Hotfix_onGetPayHistorySuccess;
    private static DelegateBridge __Hotfix_onGetPayHistoryFailed;
    private static DelegateBridge __Hotfix_onInitSuccess;
    private static DelegateBridge __Hotfix_onInitFailed;
    private static DelegateBridge __Hotfix_onForceUpdate;
    private static DelegateBridge __Hotfix_onswitchUserSuccess;
    private static DelegateBridge __Hotfix_onswitchUserFailed;
    private static DelegateBridge __Hotfix_onswitchUserCancel;
    private static DelegateBridge __Hotfix_onVerifyTokenSuccess;
    private static DelegateBridge __Hotfix_onVerifyTokenFailed;
    private static DelegateBridge __Hotfix_onDoSetExtDataSuccess;
    private static DelegateBridge __Hotfix_onDoSetExtDataFailed;
    private static DelegateBridge __Hotfix_onExitSuccess;
    private static DelegateBridge __Hotfix_onExitFailed;
    private static DelegateBridge __Hotfix_onMemoryWarning;
    private static DelegateBridge __Hotfix_onShareSuccess;
    private static DelegateBridge __Hotfix_onShareFailed;
    private static DelegateBridge __Hotfix_onShareCancel;
    private static DelegateBridge __Hotfix_onRecordStartSuccess;
    private static DelegateBridge __Hotfix_onRecordStartFail;
    private static DelegateBridge __Hotfix_onRecordStopSuccess;
    private static DelegateBridge __Hotfix_onRecordStopFail;
    private static DelegateBridge __Hotfix_onBoradcastSuccess;
    private static DelegateBridge __Hotfix_onBoradcastFail;
    private static DelegateBridge __Hotfix_onDoQuestionSuccess;
    private static DelegateBridge __Hotfix_onDoQuestionFailed;
    private static DelegateBridge __Hotfix_oncallWebViewSuccess;
    private static DelegateBridge __Hotfix_oncallWebViewFailed;
    private static DelegateBridge __Hotfix_oncallWebViewCancel;
    private static DelegateBridge __Hotfix_onWebViewOpen;
    private static DelegateBridge __Hotfix_onWebViewClose;
    private static DelegateBridge __Hotfix_onQRLoginSuccess;
    private static DelegateBridge __Hotfix_onQRLoginFailed;
    private static DelegateBridge __Hotfix_onQRLoginCancel;
    private static DelegateBridge __Hotfix_onDoSaveImageToPhotoLibrarySuccess;
    private static DelegateBridge __Hotfix_onDoSaveImageToPhotoLibraryFailed;
    private static DelegateBridge __Hotfix_onBindGuestSuccess;
    private static DelegateBridge __Hotfix_onBindGuestFailed;
    private static DelegateBridge __Hotfix_onBindGuestCancel;
    private static DelegateBridge __Hotfix_onLangueChangeSuccess;
    private static DelegateBridge __Hotfix_onLangueChangeFailed;
    private static DelegateBridge __Hotfix_onUnBindSuccess;
    private static DelegateBridge __Hotfix_onUnBindFailed;
    private static DelegateBridge __Hotfix_onUnBindCancel;
    private static DelegateBridge __Hotfix_onRequestPermissionSuccess;
    private static DelegateBridge __Hotfix_onRequestPermissionFailed;
    private static DelegateBridge __Hotfix_onNoticeCenterStateSuccess;
    private static DelegateBridge __Hotfix_onNoticeCenterStateFailed;
    private static DelegateBridge __Hotfix_showAndroidToast;
    private static DelegateBridge __Hotfix_verifyToken;
    private static DelegateBridge __Hotfix_Perform;
    private static DelegateBridge __Hotfix_DoCoroutine;
    private static DelegateBridge __Hotfix_OnGUI;
    private static DelegateBridge __Hotfix_InitLoginResult;

    public event Action EventOnBindGuestEnd
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<bool> EventOnDoShareCallBack
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnDoShareCancel
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnForceUpdate
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnInfoWebViewSuccess
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnNoticeCenterStateFailed
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnNoticeCenterStateSuccess
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<bool> EventOnQRLoginCallBack
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action eventSwitchUserSuccessCallback
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<bool> m_eventLoginCallback
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<bool> m_eventLogoutCallback
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action m_eventOnMemoryWarning
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<bool> m_eventOnPayCallback
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action m_eventOnPayCancel
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action m_eventOnSDKPromotingPayCancel
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action<string> m_eventOnSDKPromotingPaySuccess
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    [MethodImpl(0x8000)]
    private void Awake()
    {
        DelegateBridge bridge = __Hotfix_Awake;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp1(this);
        }
        else if (this.IsShowCommandUI && (null != this.CommondUIPrefab))
        {
            GameObject obj2 = Instantiate<GameObject>(this.CommondUIPrefab);
            obj2.transform.SetParent(base.transform, false);
            obj2.transform.localScale = Vector3.one;
            obj2.transform.SetSiblingIndex(0);
        }
    }

    [MethodImpl(0x8000)]
    public void callCustomerService()
    {
    }

    [MethodImpl(0x8000)]
    public void callCustomerServiceWeb()
    {
    }

    [MethodImpl(0x8000)]
    public void callWebView(string title, int fullscreen_flag, int title_flag, string action, string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public bool checkPermission(string permission, string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void clearLocalNotifications()
    {
    }

    [MethodImpl(0x8000)]
    public void doAddLocalPush(string Title, string Content, string Date, string Hour, string Min)
    {
    }

    [MethodImpl(0x8000)]
    public void doBindGuest(string bind_type)
    {
    }

    [MethodImpl(0x8000)]
    public void doBoradcast()
    {
    }

    [MethodImpl(0x8000)]
    public static void DoCoroutine(IEnumerator coroutine, Action onComplete = null)
    {
        DelegateBridge bridge = __Hotfix_DoCoroutine;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp5(coroutine, onComplete);
        }
        else
        {
            _instance.StartCoroutine(_instance.Perform(coroutine, onComplete));
        }
    }

    [MethodImpl(0x8000)]
    public void doFirstScreen()
    {
    }

    [MethodImpl(0x8000)]
    public void doGetImagePath(int nType, int fromType)
    {
    }

    [MethodImpl(0x8000)]
    public void doOpenFBFanPage()
    {
    }

    [MethodImpl(0x8000)]
    public void doOpenRequestReview()
    {
    }

    [MethodImpl(0x8000)]
    public void DoPay(string goodsName, int goodsNumber, string goodsId, string goodsRegisterId, double goodsPrice, string customparams, string goodsDes)
    {
    }

    [MethodImpl(0x8000)]
    public void DoPayWithLimit(int limitLevel, string goodsName, int goodsNumber, string goodsId, string goodsRegisterId, double goodsPrice, string customparams, string goodsDes)
    {
    }

    [MethodImpl(0x8000)]
    public void DoPromotingPay(string goodsName, int goodsNumber, string goodsId, string goodsRegisterId, double goodsPrice, string customparams, string goodsDes)
    {
    }

    [MethodImpl(0x8000)]
    public void DoPromotingWithLimitPay(int limitLevel, string goodsName, int goodsNumber, string goodsId, string goodsRegisterId, double goodsPrice, string customparams, string goodsDes)
    {
    }

    [MethodImpl(0x8000)]
    public void doQQVIP()
    {
    }

    [MethodImpl(0x8000)]
    public void doSaveImageToPhotoLibrary(string data)
    {
    }

    [MethodImpl(0x8000)]
    public string doSetExtData(string data, string type)
    {
    }

    [MethodImpl(0x8000)]
    public void doshare(string data)
    {
    }

    [MethodImpl(0x8000)]
    public void doStartQRLogin(string customParams)
    {
    }

    [MethodImpl(0x8000)]
    public void doStartRecord()
    {
    }

    [MethodImpl(0x8000)]
    public void doStopRecord()
    {
    }

    [MethodImpl(0x8000)]
    public void doUnBind()
    {
    }

    [MethodImpl(0x8000)]
    public void exit()
    {
    }

    [MethodImpl(0x8000)]
    public int GetBattery()
    {
    }

    [MethodImpl(0x8000)]
    public string getChannelID()
    {
    }

    [MethodImpl(0x8000)]
    public string GetDeviceID()
    {
    }

    [MethodImpl(0x8000)]
    public string getECID()
    {
    }

    [MethodImpl(0x8000)]
    public void GetProductsList()
    {
    }

    [MethodImpl(0x8000)]
    public string GetPustToken()
    {
    }

    [MethodImpl(0x8000)]
    public string getSysCountry()
    {
    }

    [MethodImpl(0x8000)]
    public string getSysLangue()
    {
    }

    [MethodImpl(0x8000)]
    public void Init()
    {
        DelegateBridge bridge = __Hotfix_Init;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp1(this);
        }
        else if (!this.isInit)
        {
            Debug.Log("Start PC PDSDK init");
            PDSDK_PC.Init();
            GameObject gameObject = GameObject.Find("PDSDKMain").gameObject;
            if (gameObject && (PDSDK_PC.getCommonProperty("pc_store", string.Empty) == "steam"))
            {
                gameObject.AddComponent<SteamManager>();
            }
            BuglyAgent.EnableExceptionHandler();
        }
    }

    [MethodImpl(0x8000)]
    private void InitLoginResult(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public bool isEuCountry()
    {
    }

    [MethodImpl(0x8000)]
    public void Login(string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void LoginWithType(string login_type, string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void Logout(string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void noticeCenterState(int mode, string noticeID)
    {
    }

    [MethodImpl(0x8000)]
    private void onBindGuestCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onBindGuestFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onBindGuestSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onBoradcastFail(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onBoradcastSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void oncallWebViewCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void oncallWebViewFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void oncallWebViewSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onCheckGiftCardItemInfoSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onDoQuestionFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onDoQuestionSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onDoSaveImageToPhotoLibraryFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onDoSaveImageToPhotoLibrarySuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onDoSetExtDataFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onDoSetExtDataSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onExitFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onExitSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onForceUpdate(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onGetPayHistoryFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onGetPayHistorySuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onGetProductsListFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onGetProductsListSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void OnGUI()
    {
        DelegateBridge bridge = __Hotfix_OnGUI;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp1(this);
        }
        else if (this.isDebug)
        {
            string stringToEdit;
            GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("init", options))
            {
                this.stringToEdit = this.stringToEdit + "init\n";
                this.Init();
            }
            GUILayout.BeginHorizontal("button", new GUILayoutOption[0]);
            GUILayoutOption[] optionArray2 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("login", optionArray2))
            {
                this.stringToEdit = this.stringToEdit + "start login \n";
                Instance.Login("loginpara");
            }
            GUILayoutOption[] optionArray3 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("logout", optionArray3))
            {
                Instance.Logout("logoutpara");
            }
            GUILayoutOption[] optionArray4 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("start game", optionArray4))
            {
                Instance.StartGame("{\"RoleId\":\"123213123\", \"GameUid\":\"GameUid\", \"RoleLevel\":\"10\", \"ServerId\":\"001\",\"ServerName\":\"ServerName\", \"GameName\":\"GameName\", \"Balance\":100, \"PartyName\":\"PartyName\", \"VipLevel\":0, \"PushInfo\":\"PushInfo\"}");
            }
            GUILayoutOption[] optionArray5 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("get porducts list", optionArray5))
            {
                Instance.GetProductsList();
            }
            GUILayoutOption[] optionArray6 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("getDeviceID", optionArray6))
            {
                this.stringToEdit = this.stringToEdit + Instance.GetDeviceID() + "\n";
            }
            GUILayoutOption[] optionArray7 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("getPushToken", optionArray7))
            {
                this.stringToEdit = this.stringToEdit + Instance.GetPustToken() + "\n";
            }
            GUILayoutOption[] optionArray8 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("doAddLocalPush", optionArray8))
            {
                string title = "我是标题";
                string date = DateTime.Now.AddMinutes(1.0).ToString("yyyyMMdd");
                string hour = DateTime.Now.AddMinutes(1.0).ToString("HH");
                string min = DateTime.Now.AddMinutes(1.0).ToString("mm");
                Debug.Log("fwltest is date is " + date);
                Debug.Log("fwltest is Hour is " + hour);
                Debug.Log("fwltest is Min is " + min);
                Instance.doAddLocalPush(title, "我是内容", date, hour, min);
            }
            GUILayoutOption[] optionArray9 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("用户中心", optionArray9))
            {
                Instance.userCenter();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal("button", new GUILayoutOption[0]);
            GUILayoutOption[] optionArray10 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("switchUser", optionArray10))
            {
                Instance.SwitchUser();
            }
            GUILayoutOption[] optionArray11 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("exit", optionArray11))
            {
                Instance.exit();
            }
            GUILayoutOption[] optionArray12 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("print gameevent", optionArray12))
            {
                string remark = "{\"serverid\":\"fwl\"}";
                string eventID = "1";
                Instance.printGameEventLog(eventID, remark);
                stringToEdit = this.stringToEdit;
                string[] textArray1 = new string[] { stringToEdit, " printGameEventLog ID is ", eventID, " remark is ", remark, "\n" };
                this.stringToEdit = string.Concat(textArray1);
            }
            GUILayoutOption[] optionArray13 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("doExtData", optionArray13))
            {
                JsonData data = new JsonData {
                    ["RoleId"] = "112223333",
                    ["ServerId"] = "6666666"
                };
                Debug.Log(" data is " + data.ToJson());
                Instance.doSetExtData(data.ToJson(), "levelup");
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal("button", new GUILayoutOption[0]);
            if (goodlistjson != null)
            {
                for (int i = 0; i < goodlistjson.Count; i++)
                {
                    string goodsId = goodlistjson[i]["goods_id"].ToString();
                    string str10 = goodlistjson[i]["goods_icon"].ToString();
                    int goodsNumber = int.Parse(goodlistjson[i]["goods_number"].ToString());
                    string goodsDes = goodlistjson[i]["goods_describe"].ToString();
                    double goodsPrice = double.Parse(goodlistjson[i]["goods_price"].ToString());
                    int num4 = int.Parse(goodlistjson[i]["type"].ToString());
                    string goodsRegisterId = goodlistjson[i]["goods_register_id"].ToString();
                    string goodsName = goodlistjson[i]["goods_name"].ToString();
                    GUILayoutOption[] optionArray14 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
                    if (GUILayout.Button("商品名称:" + goodsName, optionArray14))
                    {
                        Instance.DoPay(goodsName, goodsNumber, goodsId, goodsRegisterId, goodsPrice, "我是透传", goodsDes);
                    }
                }
            }
            GUILayoutOption[] optionArray15 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("getbattery", optionArray15))
            {
                Debug.Log(" getbattery  begin");
                int battery = Instance.GetBattery();
                stringToEdit = this.stringToEdit;
                object[] objArray1 = new object[] { stringToEdit, " Battery is ", battery, "\n" };
                this.stringToEdit = string.Concat(objArray1);
            }
            GUILayoutOption[] optionArray16 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("WebInvestigation", optionArray16))
            {
                Debug.Log(" WebInvestigation  begin");
                Instance.WebInvestigation();
            }
            GUILayoutOption[] optionArray17 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("Doshare", optionArray17))
            {
                Debug.Log(" Doshare  begin");
                JsonData data2 = new JsonData {
                    ["Title"] = "这是分享的标题",
                    ["TitleUrl"] = "http://q.zilongame.com/",
                    ["Text"] = "这是分享的正文",
                    ["ImagePath"] = "sdcard/image.jpg",
                    ["SiteUrl"] = "hhttp://q.zilongame.com/",
                    ["Url"] = "http://q.zilongame.com/",
                    ["shareType"] = 3
                };
                Instance.doshare(data2.ToJson());
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal("button", new GUILayoutOption[0]);
            GUILayoutOption[] optionArray18 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("开始录屏", optionArray18))
            {
                Instance.doStartRecord();
            }
            GUILayoutOption[] optionArray19 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("停止录屏", optionArray19))
            {
                Instance.doStopRecord();
            }
            GUILayoutOption[] optionArray20 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("开启直播", optionArray20))
            {
                Instance.doBoradcast();
            }
            GUILayoutOption[] optionArray21 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("获取渠道ID", optionArray21))
            {
                Instance.getChannelID();
            }
            GUILayoutOption[] optionArray22 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("保存图片", optionArray22))
            {
                Instance.doSaveImageToPhotoLibrary("2222");
            }
            GUILayoutOption[] optionArray23 = new GUILayoutOption[] { GUILayout.Height(100f), GUILayout.Width(200f) };
            if (GUILayout.Button("通用web接口", optionArray23))
            {
                string title = "测试";
                Instance.callWebView(title, 0, 1, "index", "strCm");
            }
            GUILayout.EndHorizontal();
            GUILayoutOption[] optionArray24 = new GUILayoutOption[] { GUILayout.Height(700f), GUILayout.Width(1080f) };
            this.stringToEdit = GUILayout.TextField(this.stringToEdit, optionArray24);
        }
    }

    [MethodImpl(0x8000)]
    public void onInitFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onInitSuccess(string msg)
    {
        DelegateBridge bridge = __Hotfix_onInitSuccess;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp5(this, msg);
        }
        else
        {
            Debug.Log("onInitSuccess " + msg);
            if (!string.IsNullOrEmpty(msg))
            {
                JsonData data = JsonMapper.ToObject(msg);
                if (((IDictionary) data).Contains("isReview"))
                {
                    SDKHelper.IsIosReview = data["isReview"].ToString() == "1";
                }
            }
            this._isInit = true;
            if (this.m_eventInitCallback != null)
            {
                this.m_eventInitCallback(true);
            }
        }
    }

    [MethodImpl(0x8000)]
    private void onLangueChangeFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onLangueChangeSuccess(string msg)
    {
        DelegateBridge bridge = __Hotfix_onLangueChangeSuccess;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp5(this, msg);
        }
        else
        {
            Debug.Log("onLangueChangeSuccess" + msg);
            string str = (string) JsonMapper.ToObject(msg)["message"];
            this.showAndroidToast("onLangueChangeSuccess" + msg);
        }
    }

    [MethodImpl(0x8000)]
    public void onLoginCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onLoginFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onLoginSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onLogoutCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onLogoutFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onLogoutSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onMemoryWarning(string recode)
    {
    }

    [MethodImpl(0x8000)]
    public void onNoticeCenterStateFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onNoticeCenterStateSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onPayCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onPayFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onPayGiftCardFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onPayGiftCardSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onPaySuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onQRLoginCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onQRLoginFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onQRLoginSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onRecordStartFail(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onRecordStartSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onRecordStopFail(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onRecordStopSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onRequestPermissionFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onRequestPermissionSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onSDKPromotingPayCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onSDKPromotingPayFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onSDKPromotingPaySuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onShareCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onShareFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onShareSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onswitchUserCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onswitchUserFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onswitchUserSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onUnBindCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onUnBindFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    private void onUnBindSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onVerifyTokenFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onVerifyTokenSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public void onWebViewClose()
    {
    }

    [MethodImpl(0x8000)]
    public void onWebViewOpen()
    {
    }

    [MethodImpl(0x8000)]
    public void OpenInvestigation(int enqId = 1)
    {
    }

    [MethodImpl(0x8000)]
    public void PathOrder()
    {
    }

    [MethodImpl(0x8000)]
    public void payGiftCard(string giftid, string customparams)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private IEnumerator Perform(IEnumerator coroutine, Action onComplete = null)
    {
        DelegateBridge bridge = __Hotfix_Perform;
        if (bridge != null)
        {
            return bridge.__Gen_Delegate_Imp24(this, coroutine, onComplete);
        }
        return new <Perform>c__Iterator0 { 
            onComplete = onComplete,
            coroutine = coroutine,
            $this = this
        };
    }

    [MethodImpl(0x8000)]
    public void printGameEventLog(string eventID, string remark)
    {
        DelegateBridge bridge = __Hotfix_printGameEventLog;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp26(this, eventID, remark);
        }
        else
        {
            PDSDK_PC.PrintGameEventLog(eventID, remark);
        }
    }

    [MethodImpl(0x8000)]
    public bool requestPermission(string permission, string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void ResetGuest(string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public void setSDKLangue(string langue)
    {
        DelegateBridge bridge = __Hotfix_setSDKLangue;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp5(this, langue);
        }
        else
        {
            Debug.Log("setSDKLangue " + langue);
            PDSDK_PC.SetSDKLanguage(langue, new Action<string>(this.onLangueChangeSuccess));
        }
    }

    [MethodImpl(0x8000)]
    public void showAndroidToast(string info)
    {
        DelegateBridge bridge = __Hotfix_showAndroidToast;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp5(this, info);
        }
    }

    [MethodImpl(0x8000)]
    public void ShowPayHelp()
    {
    }

    [MethodImpl(0x8000)]
    public void StartGame(string gameparams)
    {
    }

    [MethodImpl(0x8000)]
    public void SwitchUser()
    {
    }

    [MethodImpl(0x8000)]
    private void Update()
    {
        DelegateBridge bridge = __Hotfix_Update;
        if (bridge != null)
        {
            bridge.__Gen_Delegate_Imp1(this);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && this._isLogining)
        {
            PDWebViewManager.Manager.CloseWebBrowser();
            this.m_onLogoutMsgBoxCanceled = delegate {
            };
        }
    }

    [MethodImpl(0x8000)]
    public void userCenter()
    {
    }

    [MethodImpl(0x8000)]
    public string UserLoginType()
    {
    }

    [MethodImpl(0x8000)]
    public void verifyToken(string info)
    {
    }

    [MethodImpl(0x8000)]
    public void WebInvestigation()
    {
    }

    public bool isInit
    {
        [MethodImpl(0x8000)]
        get
        {
            DelegateBridge bridge = __Hotfix_get_isInit;
            return ((bridge == null) ? this._isInit : bridge.__Gen_Delegate_Imp9(this));
        }
    }

    public static bool IsLogin
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    public static PDSDK Instance
    {
        [MethodImpl(0x8000)]
        get
        {
            DelegateBridge bridge = __Hotfix_get_Instance;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp3370();
            }
            if (_instance == null)
            {
                _instance = FindObjectOfType<PDSDK>();
                if (_instance == null)
                {
                    _instance = new GameObject("PDSDKMain").AddComponent<PDSDK>();
                }
            }
            return _instance;
        }
    }

    [CompilerGenerated]
    private sealed class <Perform>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal Action onComplete;
        internal IEnumerator coroutine;
        internal PDSDK $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private static Action <>f__am$cache0;

        private static void <>m__0()
        {
        }

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                {
                    Action onComplete = this.onComplete;
                    if (this.onComplete == null)
                    {
                        Action local1 = this.onComplete;
                        if (<>f__am$cache0 == null)
                        {
                            <>f__am$cache0 = new Action(PDSDK.<Perform>c__Iterator0.<>m__0);
                        }
                        onComplete = <>f__am$cache0;
                    }
                    this.onComplete = onComplete;
                    this.$current = this.$this.StartCoroutine(this.coroutine);
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                case 1:
                    this.onComplete();
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct LoginSuccessMsg
    {
        public string data;
        public string opcode;
        public string channel_id;
        public string customparams;
        [MethodImpl(0x8000)]
        public LoginSuccessMsg(string d, string o, string ch, string cu)
        {
        }

        [MethodImpl(0x8000)]
        public static PDSDK.LoginSuccessMsg Parse(string msg)
        {
        }
    }
}

