﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Distance
    {
        [MethodImpl(0x8000)]
        private static void Case0(ref Box3 mBox, ref double mLineParameter, int i0, int i1, int i2, ref Vector3D pnt, ref Vector3D dir, ref double sqrDistance)
        {
        }

        [MethodImpl(0x8000)]
        private static void Case00(ref Box3 mBox, ref double mLineParameter, int i0, int i1, int i2, ref Vector3D pnt, ref Vector3D dir, ref double sqrDistance)
        {
        }

        [MethodImpl(0x8000)]
        private static void Case000(ref Box3 mBox, ref double mLineParameter, ref Vector3D pnt, ref double sqrDistance)
        {
        }

        [MethodImpl(0x8000)]
        private static void CaseNoZeros(ref Box3 mBox, ref double mLineParameter, ref Vector3D pnt, ref Vector3D dir, ref double sqrDistance)
        {
        }

        [MethodImpl(0x8000)]
        private static void Face(ref Box3 mBox, ref double mLineParameter, int i0, int i1, int i2, ref Vector3D pnt, ref Vector3D dir, ref Vector3D PmE, ref double sqrDistance)
        {
        }

        public static double Line2Line2(ref Line2 line0, ref Line2 line1) => 
            Math.Sqrt(SqrLine2Line2(ref line0, ref line1));

        public static double Line2Line2(ref Line2 line0, ref Line2 line1, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrLine2Line2(ref line0, ref line1, out closestPoint0, out closestPoint1));

        public static double Line2Ray2(ref Line2 line, ref Ray2 ray) => 
            Math.Sqrt(SqrLine2Ray2(ref line, ref ray));

        public static double Line2Ray2(ref Line2 line, ref Ray2 ray, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrLine2Ray2(ref line, ref ray, out closestPoint0, out closestPoint1));

        public static double Line2Segment2(ref Line2 line, ref Segment2 segment) => 
            Math.Sqrt(SqrLine2Segment2(ref line, ref segment));

        public static double Line2Segment2(ref Line2 line, ref Segment2 segment, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrLine2Segment2(ref line, ref segment, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Line3Box3(ref Line3 line, ref Box3 box)
        {
        }

        public static double Line3Box3(ref Line3 line, ref Box3 box, out Line3Box3Dist info) => 
            Math.Sqrt(SqrLine3Box3(ref line, ref box, out info));

        [MethodImpl(0x8000)]
        public static double Line3Line3(ref Line3 line0, ref Line3 line1)
        {
        }

        public static double Line3Line3(ref Line3 line0, ref Line3 line1, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrLine3Line3(ref line0, ref line1, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Line3Ray3(ref Line3 line, ref Ray3 ray)
        {
        }

        public static double Line3Ray3(ref Line3 line, ref Ray3 ray, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrLine3Ray3(ref line, ref ray, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Line3Segment3(ref Line3 line, ref Segment3 segment)
        {
        }

        public static double Line3Segment3(ref Line3 line, ref Segment3 segment, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrLine3Segment3(ref line, ref segment, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Point2AAB2(ref Vector2D point, ref AAB2 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point2AAB2(ref Vector2D point, ref AAB2 box, out Vector2D closestPoint)
        {
        }

        public static double Point2Box2(ref Vector2D point, ref Box2 box) => 
            Math.Sqrt(SqrPoint2Box2(ref point, ref box));

        public static double Point2Box2(ref Vector2D point, ref Box2 box, out Vector2D closestPoint) => 
            Math.Sqrt(SqrPoint2Box2(ref point, ref box, out closestPoint));

        [MethodImpl(0x8000)]
        public static double Point2Circle2(ref Vector2D point, ref Circle2 circle)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point2Circle2(ref Vector2D point, ref Circle2 circle, out Vector2D closestPoint)
        {
        }

        public static double Point2Line2(ref Vector2D point, ref Line2 line) => 
            Math.Sqrt(SqrPoint2Line2(ref point, ref line));

        public static double Point2Line2(ref Vector2D point, ref Line2 line, out Vector2D closestPoint) => 
            Math.Sqrt(SqrPoint2Line2(ref point, ref line, out closestPoint));

        public static double Point2Ray2(ref Vector2D point, ref Ray2 ray) => 
            Math.Sqrt(SqrPoint2Ray2(ref point, ref ray));

        public static double Point2Ray2(ref Vector2D point, ref Ray2 ray, out Vector2D closestPoint) => 
            Math.Sqrt(SqrPoint2Ray2(ref point, ref ray, out closestPoint));

        public static double Point2Segment2(ref Vector2D point, ref Segment2 segment) => 
            Math.Sqrt(SqrPoint2Segment2(ref point, ref segment));

        public static double Point2Segment2(ref Vector2D point, ref Segment2 segment, out Vector2D closestPoint) => 
            Math.Sqrt(SqrPoint2Segment2(ref point, ref segment, out closestPoint));

        [MethodImpl(0x8000)]
        public static double Point2Triangle2(ref Vector2D point, ref Triangle2 triangle)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point2Triangle2(ref Vector2D point, ref Triangle2 triangle, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point3AAB3(ref Vector3D point, ref AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point3AAB3(ref Vector3D point, ref AAB3 box, out Vector3D closestPoint)
        {
        }

        public static double Point3Box3(ref Vector3D point, ref Box3 box) => 
            Math.Sqrt(SqrPoint3Box3(ref point, ref box));

        public static double Point3Box3(ref Vector3D point, ref Box3 box, out Vector3D closestPoint) => 
            Math.Sqrt(SqrPoint3Box3(ref point, ref box, out closestPoint));

        [MethodImpl(0x8000)]
        public static double Point3Circle3(ref Vector3D point, ref Circle3 circle, bool solid = true)
        {
        }

        public static double Point3Circle3(ref Vector3D point, ref Circle3 circle, out Vector3D closestPoint, bool solid = true) => 
            Math.Sqrt(SqrPoint3Circle3(ref point, ref circle, out closestPoint, solid));

        public static double Point3Line3(ref Vector3D point, ref Line3 line) => 
            Math.Sqrt(SqrPoint3Line3(ref point, ref line));

        public static double Point3Line3(ref Vector3D point, ref Line3 line, out Vector3D closestPoint) => 
            Math.Sqrt(SqrPoint3Line3(ref point, ref line, out closestPoint));

        [MethodImpl(0x8000)]
        public static double Point3Plane3(ref Vector3D point, ref Plane3 plane)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point3Plane3(ref Vector3D point, ref Plane3 plane, out Vector3D closestPoint)
        {
        }

        public static double Point3Ray3(ref Vector3D point, ref Ray3 ray) => 
            Math.Sqrt(SqrPoint3Ray3(ref point, ref ray));

        public static double Point3Ray3(ref Vector3D point, ref Ray3 ray, out Vector3D closestPoint) => 
            Math.Sqrt(SqrPoint3Ray3(ref point, ref ray, out closestPoint));

        public static double Point3Rectangle3(ref Vector3D point, ref Rectangle3 rectangle) => 
            Math.Sqrt(SqrPoint3Rectangle3(ref point, ref rectangle));

        public static double Point3Rectangle3(ref Vector3D point, ref Rectangle3 rectangle, out Vector3D closestPoint) => 
            Math.Sqrt(SqrPoint3Rectangle3(ref point, ref rectangle, out closestPoint));

        public static double Point3Segment3(ref Vector3D point, ref Segment3 segment) => 
            Math.Sqrt(SqrPoint3Segment3(ref point, ref segment));

        public static double Point3Segment3(ref Vector3D point, ref Segment3 segment, out Vector3D closestPoint) => 
            Math.Sqrt(SqrPoint3Segment3(ref point, ref segment, out closestPoint));

        [MethodImpl(0x8000)]
        public static double Point3Sphere3(ref Vector3D point, ref Sphere3 sphere)
        {
        }

        [MethodImpl(0x8000)]
        public static double Point3Sphere3(ref Vector3D point, ref Sphere3 sphere, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double Ray2Ray2(ref Ray2 ray0, ref Ray2 ray1)
        {
        }

        public static double Ray2Ray2(ref Ray2 ray0, ref Ray2 ray1, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrRay2Ray2(ref ray0, ref ray1, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Ray2Segment2(ref Ray2 ray, ref Segment2 segment)
        {
        }

        public static double Ray2Segment2(ref Ray2 ray, ref Segment2 segment, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrRay2Segment2(ref ray, ref segment, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Ray3Ray3(ref Ray3 ray0, ref Ray3 ray1)
        {
        }

        public static double Ray3Ray3(ref Ray3 ray0, ref Ray3 ray1, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrRay3Ray3(ref ray0, ref ray1, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Ray3Segment3(ref Ray3 ray, ref Segment3 segment)
        {
        }

        public static double Ray3Segment3(ref Ray3 ray, ref Segment3 segment, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrRay3Segment3(ref ray, ref segment, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Segment2Segment2(ref Segment2 segment0, ref Segment2 segment1)
        {
        }

        public static double Segment2Segment2(ref Segment2 segment0, ref Segment2 segment1, out Vector2D closestPoint0, out Vector2D closestPoint1) => 
            Math.Sqrt(SqrSegment2Segment2(ref segment0, ref segment1, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Segment3Box3(ref Segment3 segment, ref Box3 box)
        {
        }

        public static double Segment3Box3(ref Segment3 segment, ref Box3 box, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrSegment3Box3(ref segment, ref box, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double Segment3Segment3(ref Segment3 segment0, ref Segment3 segment1)
        {
        }

        public static double Segment3Segment3(ref Segment3 segment0, ref Segment3 segment1, out Vector3D closestPoint0, out Vector3D closestPoint1) => 
            Math.Sqrt(SqrSegment3Segment3(ref segment0, ref segment1, out closestPoint0, out closestPoint1));

        [MethodImpl(0x8000)]
        public static double SqrLine2Line2(ref Line2 line0, ref Line2 line1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine2Line2(ref Line2 line0, ref Line2 line1, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine2Ray2(ref Line2 line, ref Ray2 ray)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine2Ray2(ref Line2 line, ref Ray2 ray, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine2Segment2(ref Line2 line, ref Segment2 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine2Segment2(ref Line2 line, ref Segment2 segment, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Box3(ref Line3 line, ref Box3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Box3(ref Line3 line, ref Box3 box, out Line3Box3Dist info)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Line3(ref Line3 line0, ref Line3 line1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Line3(ref Line3 line0, ref Line3 line1, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Ray3(ref Line3 line, ref Ray3 ray)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Ray3(ref Line3 line, ref Ray3 ray, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Segment3(ref Line3 line, ref Segment3 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrLine3Segment3(ref Line3 line, ref Segment3 segment, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2AAB2(ref Vector2D point, ref AAB2 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2AAB2(ref Vector2D point, ref AAB2 box, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Box2(ref Vector2D point, ref Box2 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Box2(ref Vector2D point, ref Box2 box, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Circle2(ref Vector2D point, ref Circle2 circle)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Circle2(ref Vector2D point, ref Circle2 circle, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Line2(ref Vector2D point, ref Line2 line)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Line2(ref Vector2D point, ref Line2 line, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Ray2(ref Vector2D point, ref Ray2 ray)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Ray2(ref Vector2D point, ref Ray2 ray, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Segment2(ref Vector2D point, ref Segment2 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Segment2(ref Vector2D point, ref Segment2 segment, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Triangle2(ref Vector2D point, ref Triangle2 triangle)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint2Triangle2(ref Vector2D point, ref Triangle2 triangle, out Vector2D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3AAB3(ref Vector3D point, ref AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3AAB3(ref Vector3D point, ref AAB3 box, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Box3(ref Vector3D point, ref Box3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Box3(ref Vector3D point, ref Box3 box, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Circle3(ref Vector3D point, ref Circle3 circle, bool solid = true)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Circle3(ref Vector3D point, ref Circle3 circle, out Vector3D closestPoint, bool solid = true)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Line3(ref Vector3D point, ref Line3 line)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Line3(ref Vector3D point, ref Line3 line, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Plane3(ref Vector3D point, ref Plane3 plane)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Plane3(ref Vector3D point, ref Plane3 plane, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Ray3(ref Vector3D point, ref Ray3 ray)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Ray3(ref Vector3D point, ref Ray3 ray, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Rectangle3(ref Vector3D point, ref Rectangle3 rectangle)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Rectangle3(ref Vector3D point, ref Rectangle3 rectangle, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Segment3(ref Vector3D point, ref Segment3 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Segment3(ref Vector3D point, ref Segment3 segment, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Sphere3(ref Vector3D point, ref Sphere3 sphere)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrPoint3Sphere3(ref Vector3D point, ref Sphere3 sphere, out Vector3D closestPoint)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay2Ray2(ref Ray2 ray0, ref Ray2 ray1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay2Ray2(ref Ray2 ray0, ref Ray2 ray1, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay2Segment2(ref Ray2 ray, ref Segment2 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay2Segment2(ref Ray2 ray, ref Segment2 segment, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay3Ray3(ref Ray3 ray0, ref Ray3 ray1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay3Ray3(ref Ray3 ray0, ref Ray3 ray1, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay3Segment3(ref Ray3 ray, ref Segment3 segment)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrRay3Segment3(ref Ray3 ray, ref Segment3 segment, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment2Segment2(ref Segment2 segment0, ref Segment2 segment1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment2Segment2(ref Segment2 segment0, ref Segment2 segment1, out Vector2D closestPoint0, out Vector2D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment3Box3(ref Segment3 segment, ref Box3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment3Box3(ref Segment3 segment, ref Box3 box, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment3Segment3(ref Segment3 segment0, ref Segment3 segment1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SqrSegment3Segment3(ref Segment3 segment0, ref Segment3 segment1, out Vector3D closestPoint0, out Vector3D closestPoint1)
        {
        }
    }
}

