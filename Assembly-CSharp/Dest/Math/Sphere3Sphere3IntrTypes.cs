﻿namespace Dest.Math
{
    using System;

    public enum Sphere3Sphere3IntrTypes
    {
        Empty,
        Point,
        Circle,
        Sphere0,
        Sphere0Point,
        Sphere1,
        Sphere1Point,
        Same
    }
}

