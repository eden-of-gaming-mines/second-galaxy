﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Approximation
    {
        [MethodImpl(0x8000)]
        public static Box2 GaussPointsFit2(IList<Vector2D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static Box3 GaussPointsFit3(IList<Vector3D> points)
        {
        }

        [MethodImpl(0x8000)]
        internal static bool HeightLineFit2(IList<Vector2D> points, out double a, out double b)
        {
        }

        [MethodImpl(0x8000)]
        internal static bool HeightPlaneFit3(IList<Vector3D> points, out double a, out double b, out double c)
        {
        }

        [MethodImpl(0x8000)]
        public static Line2 LeastSquaresLineFit2(IList<Vector2D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static Plane3 LeastSquaresPlaneFit3(IList<Vector3D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static Line3 LeastsSquaresLineFit3(IList<Vector3D> points)
        {
        }
    }
}

