﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public class OdeEuler : OdeSolver
    {
        public OdeEuler(int dim, double step, OdeFunction function) : base(dim, step, function)
        {
        }

        [MethodImpl(0x8000)]
        public override void Update(double tIn, double[] yIn, ref double tOut, double[] yOut)
        {
        }
    }
}

