﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public static class Quaternionex
    {
        public static Quaternion DeltaTo(this Quaternion quat, Quaternion target) => 
            (target * Quaternion.Inverse(quat));

        [MethodImpl(0x8000)]
        public static double GetRotationDouble(uint rotationInt)
        {
        }

        [MethodImpl(0x8000)]
        public static ushort GetRotationUInt16(double rotationValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string ToStringEx(this Quaternion quat)
        {
        }
    }
}

