﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Polygon3
    {
        private Vector3D[] _vertices;
        private Edge3[] _edges;
        private Plane3 _plane;

        private Polygon3()
        {
        }

        [MethodImpl(0x8000)]
        public Polygon3(Vector3D[] vertices, Plane3 plane)
        {
        }

        [MethodImpl(0x8000)]
        public Polygon3(int vertexCount, Plane3 plane)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D CalcCenter()
        {
        }

        [MethodImpl(0x8000)]
        public double CalcPerimeter()
        {
        }

        [MethodImpl(0x8000)]
        public Edge3 GetEdge(int edgeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasZeroCorners(double threshold = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public void ProjectVertices()
        {
        }

        [MethodImpl(0x8000)]
        public void ReverseVertices()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVertexProjected(int vertexIndex, Vector3D vertex)
        {
        }

        [MethodImpl(0x8000)]
        public Segment3[] ToSegmentArray()
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEdge(int edgeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEdges()
        {
        }

        public Vector3D[] Vertices =>
            this._vertices;

        public Edge3[] Edges =>
            this._edges;

        public int VertexCount =>
            this._vertices.Length;

        public Vector3D this[int vertexIndex]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public Plane3 Plane
        {
            get => 
                this._plane;
            set => 
                (this._plane = value);
        }
    }
}

