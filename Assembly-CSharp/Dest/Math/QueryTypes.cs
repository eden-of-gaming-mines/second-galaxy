﻿namespace Dest.Math
{
    using System;

    internal enum QueryTypes
    {
        Int64,
        Integer,
        Rational,
        Real,
        Filtered
    }
}

