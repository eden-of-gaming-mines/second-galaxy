﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Vector3Dex
    {
        public static readonly Vector3D Zero;
        public static readonly Vector3D One;
        public static readonly Vector3D UnitX;
        public static readonly Vector3D UnitY;
        public static readonly Vector3D UnitZ;
        public static readonly Vector3D PositiveInfinity;
        public static readonly Vector3D NegativeInfinity;

        [MethodImpl(0x8000)]
        public static double AngleDeg(this Vector3D vector, Vector3D target)
        {
        }

        [MethodImpl(0x8000)]
        public static double AngleRad(this Vector3D vector, Vector3D target)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateOrthonormalBasis(out Vector3D u, out Vector3D v, ref Vector3D w)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Cross(this Vector3D vector, Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Cross(this Vector3D vector, ref Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Cross(ref Vector3D vector, ref Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(this Vector3D vector, Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(this Vector3D vector, ref Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(ref Vector3D vector, ref Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        internal static Information GetInformation(IList<Vector3D> points, double epsilon)
        {
        }

        [MethodImpl(0x8000)]
        public static ProjectionPlanes GetProjectionPlane(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static double GrowLength(ref Vector3D vector, double lengthDelta, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static double Length(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static double LengthSqr(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static double Normalize(ref Vector3D vector, double epsilon = 1E-05)
        {
        }

        public static Vector3D Replicate(double value) => 
            new Vector3D(value, value, value);

        [MethodImpl(0x8000)]
        public static bool SameDirection(Vector3D value0, Vector3D value1)
        {
        }

        [MethodImpl(0x8000)]
        public static double SetLength(ref Vector3D vector, double lengthValue, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static double SignedAngleDeg(this Vector3D vector, Vector3D target, Vector3D normal)
        {
        }

        [MethodImpl(0x8000)]
        public static double SignedAngleRad(this Vector3D vector, Vector3D target, Vector3D normal)
        {
        }

        [MethodImpl(0x8000)]
        public static string ToStringEx(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D ToVector2(this Vector3D vector, ProjectionPlanes projectionPlane)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D ToVector2XY(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D ToVector2XZ(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D ToVector2YZ(this Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D UnitCross(this Vector3D vector, Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D UnitCross(this Vector3D vector, ref Vector3D value)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D UnitCross(ref Vector3D vector, ref Vector3D value)
        {
        }

        internal class Information
        {
            public int Dimension;
            public Vector3D Min;
            public Vector3D Max;
            public double MaxRange;
            public Vector3D Origin;
            public Vector3D[] Direction;
            public int[] Extreme;
            public bool ExtremeCCW;
        }
    }
}

