﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Polygon2
    {
        private Vector2D[] _vertices;
        private Edge2[] _edges;

        private Polygon2()
        {
        }

        [MethodImpl(0x8000)]
        public Polygon2(Vector2D[] vertices)
        {
        }

        [MethodImpl(0x8000)]
        public Polygon2(int vertexCount)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcArea()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2D CalcCenter()
        {
        }

        [MethodImpl(0x8000)]
        public double CalcPerimeter()
        {
        }

        public bool ContainsConvexCCW(ref Vector2D point) => 
            this.SubContainsPointCCW(ref point, 0, 0);

        public bool ContainsConvexCCW(Vector2D point) => 
            this.ContainsConvexCCW(ref point);

        public bool ContainsConvexCW(ref Vector2D point) => 
            this.SubContainsPointCW(ref point, 0, 0);

        public bool ContainsConvexCW(Vector2D point) => 
            this.ContainsConvexCW(ref point);

        [MethodImpl(0x8000)]
        public bool ContainsConvexQuadCCW(ref Vector2D point)
        {
        }

        public bool ContainsConvexQuadCCW(Vector2D point) => 
            this.ContainsConvexQuadCCW(ref point);

        [MethodImpl(0x8000)]
        public bool ContainsConvexQuadCW(ref Vector2D point)
        {
        }

        public bool ContainsConvexQuadCW(Vector2D point) => 
            this.ContainsConvexQuadCW(ref point);

        [MethodImpl(0x8000)]
        public bool ContainsSimple(ref Vector2D point)
        {
        }

        public bool ContainsSimple(Vector2D point) => 
            this.ContainsSimple(ref point);

        [MethodImpl(0x8000)]
        public static Polygon2 CreateProjected(Polygon3 polygon, ProjectionPlanes projectionPlane)
        {
        }

        [MethodImpl(0x8000)]
        public Edge2 GetEdge(int edgeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasZeroCorners(double threshold = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsConvex(double threshold = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsConvex(out Orientations orientation, double threshold = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public void ReverseVertices()
        {
        }

        [MethodImpl(0x8000)]
        private bool SubContainsPointCCW(ref Vector2D p, int i0, int i1)
        {
        }

        [MethodImpl(0x8000)]
        private bool SubContainsPointCW(ref Vector2D p, int i0, int i1)
        {
        }

        [MethodImpl(0x8000)]
        public Segment2[] ToSegmentArray()
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEdge(int edgeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEdges()
        {
        }

        public Vector2D[] Vertices =>
            this._vertices;

        public Edge2[] Edges =>
            this._edges;

        public int VertexCount =>
            this._vertices.Length;

        public Vector2D this[int vertexIndex]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

