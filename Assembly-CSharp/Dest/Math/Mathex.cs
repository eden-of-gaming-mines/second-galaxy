﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Size=1)]
    public struct Mathex
    {
        public const double Rad2Deg = 57.295780181884766;
        public const double Deg2Rad = 0.017453299835324287;
        public const double ZeroTolerance = 1E-05;
        public const double NegativeZeroTolerance = -1E-05;
        public const double ZeroToleranceSqr = 1.0000000000000002E-10;
        public const double Pi = 3.1415926535897931;
        public const double HalfPi = 1.5707963267948966;
        public const double TwoPi = 6.2831853071795862;
        public const double Ln9999 = 9.21024036697585;
        private const double Ln99999 = 11.512915465;
        public static double EvalSquared(double x) => 
            (x * x);

        public static double EvalInvSquared(double x) => 
            Math.Sqrt(x);

        public static double EvalCubic(double x) => 
            ((x * x) * x);

        [MethodImpl(0x8000)]
        public static double EvalInvCubic(double x)
        {
        }

        public static double EvalQuadratic(double x, double a, double b, double c) => 
            ((((a * x) * x) + (b * x)) + c);

        [MethodImpl(0x8000)]
        public static double EvalSigmoid(double x)
        {
        }

        [MethodImpl(0x8000)]
        public static double EvalOverlappedStep(double x, double overlap, int objectIndex, int objectCount)
        {
        }

        [MethodImpl(0x8000)]
        public static double EvalSmoothOverlappedStep(double x, double overlap, int objectIndex, int objectCount)
        {
        }

        [MethodImpl(0x8000)]
        public static double EvalGaussian(double x, double a, double b, double c)
        {
        }

        [MethodImpl(0x8000)]
        public static double EvalGaussian2D(double x, double y, double x0, double y0, double A, double a, double b, double c)
        {
        }

        [MethodImpl(0x8000)]
        public static double Lerp(double value0, double value1, double factor)
        {
        }

        public static double LerpUnclamped(double value0, double value1, double factor) => 
            (value0 + ((value1 - value0) * factor));

        [MethodImpl(0x8000)]
        public static double SigmoidInterp(double value0, double value1, double factor)
        {
        }

        [MethodImpl(0x8000)]
        public static double SinInterp(double value0, double value1, double factor)
        {
        }

        [MethodImpl(0x8000)]
        public static double CosInterp(double value0, double value1, double factor)
        {
        }

        [MethodImpl(0x8000)]
        public static double WobbleInterp(double value0, double value1, double factor)
        {
        }

        [MethodImpl(0x8000)]
        public static double FuncInterp(double value0, double value1, double factor, Func<double, double> func)
        {
        }

        [MethodImpl(0x8000)]
        public static double InvSqrt(double value)
        {
        }

        public static bool Near(double value0, double value1, double epsilon = 1E-05) => 
            (Math.Abs((double) (value0 - value1)) < epsilon);

        public static bool NearZero(double value, double epsilon = 1E-05) => 
            (Math.Abs(value) < epsilon);

        [MethodImpl(0x8000)]
        public static Vector2D CartesianToPolar(Vector2D cartesianCoordinates)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D PolarToCartesian(Vector2D polarCoordinates)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D CartesianToSpherical(Vector3D cartesianCoordinates)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D SphericalToCartesian(Vector3D sphericalCoordinates)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D CartesianToCylindrical(Vector3D cartesianCoordinates)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D CylindricalToCartesian(Vector3D cylindricalCoordinates)
        {
        }
    }
}

