﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Line3Rectangle3Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector3D Point;
    }
}

