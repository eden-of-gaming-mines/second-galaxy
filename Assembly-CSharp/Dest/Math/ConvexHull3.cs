﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ConvexHull3
    {
        [MethodImpl(0x8000)]
        public static bool Create(IList<Vector3D> vertices, double epsilon, out int dimension, out int[] indices)
        {
        }

        [MethodImpl(0x8000)]
        private static void ExtractIndices(HashSet<Triangle> hull, out int[] indices)
        {
        }

        [MethodImpl(0x8000)]
        private static bool Update(HashSet<Triangle> hull, int i, Query3 query)
        {
        }

        private class TerminatorData
        {
            public int V0;
            public int V1;
            public int NullIndex;
            public ConvexHull3.Triangle T;

            public TerminatorData(int v0 = -1, int v1 = -1, int nullIndex = -1, ConvexHull3.Triangle tri = null)
            {
                this.NullIndex = nullIndex;
                this.T = tri;
                this.V0 = v0;
                this.V1 = v1;
            }
        }

        private class Triangle
        {
            public int V0;
            public int V1;
            public int V2;
            public ConvexHull3.Triangle Adj0;
            public ConvexHull3.Triangle Adj1;
            public ConvexHull3.Triangle Adj2;
            public int Sign;
            public int Time;
            public bool OnStack;

            [MethodImpl(0x8000)]
            public Triangle(int v0, int v1, int v2)
            {
            }

            [MethodImpl(0x8000)]
            public void AttachTo(ConvexHull3.Triangle adj0, ConvexHull3.Triangle adj1, ConvexHull3.Triangle adj2)
            {
            }

            [MethodImpl(0x8000)]
            public int DetachFrom(int adjIndex, ConvexHull3.Triangle adj)
            {
            }

            [MethodImpl(0x8000)]
            public ConvexHull3.Triangle GetAdj(int index)
            {
            }

            [MethodImpl(0x8000)]
            public int GetSign(int i, Query3 query)
            {
            }

            [MethodImpl(0x8000)]
            public int GetV(int index)
            {
            }

            [MethodImpl(0x8000)]
            public void SetAdj(int index, ConvexHull3.Triangle value)
            {
            }
        }
    }
}

