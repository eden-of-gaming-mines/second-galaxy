﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Line3Triangle3Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector3D Point;
        public double LineParameter;
        public double TriBary0;
        public double TriBary1;
        public double TriBary2;
    }
}

