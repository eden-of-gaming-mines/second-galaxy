﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    internal class Query2 : Query
    {
        private static double Zero;
        private Vector2D[] _vertices;

        public Query2(Vector2D[] vertices)
        {
            this._vertices = vertices;
        }

        public double Det2(double x0, double y0, double x1, double y1) => 
            ((x0 * y1) - (x1 * y0));

        [MethodImpl(0x8000)]
        public double Det3(double x0, double y0, double z0, double x1, double y1, double z1, double x2, double y2, double z2)
        {
        }

        public double Dot(double x0, double y0, double x1, double y1) => 
            ((x0 * x1) + (y0 * y1));

        [MethodImpl(0x8000)]
        public int ToCircumcircle(int i, int v0, int v1, int v2)
        {
        }

        [MethodImpl(0x8000)]
        public int ToCircumcircle(ref Vector2D test, int v0, int v1, int v2)
        {
        }

        [MethodImpl(0x8000)]
        public int ToLine(int i, int v0, int v1)
        {
        }

        [MethodImpl(0x8000)]
        public int ToLine(ref Vector2D test, int v0, int v1)
        {
        }

        [MethodImpl(0x8000)]
        public int ToTriangle(int i, int v0, int v1, int v2)
        {
        }

        [MethodImpl(0x8000)]
        public int ToTriangle(ref Vector2D test, int v0, int v1, int v2)
        {
        }
    }
}

