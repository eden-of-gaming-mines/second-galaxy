﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Ray3
    {
        public Vector3D Center;
        public Vector3D Direction;
        [MethodImpl(0x8000)]
        public Ray3(ref Vector3D center, ref Vector3D direction)
        {
        }

        public Ray3(Vector3D center, Vector3D direction)
        {
            this.Center = center;
            this.Direction = direction;
        }

        [MethodImpl(0x8000)]
        public Vector3D Eval(double t)
        {
        }

        public double DistanceTo(Vector3D point) => 
            Distance.Point3Ray3(ref point, ref this);

        [MethodImpl(0x8000)]
        public Vector3D Project(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

