﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Vector2Dex
    {
        public static readonly Vector2D Zero;
        public static readonly Vector2D One;
        public static readonly Vector2D UnitX;
        public static readonly Vector2D UnitY;
        public static readonly Vector2D PositiveInfinity;
        public static readonly Vector2D NegativeInfinity;

        [MethodImpl(0x8000)]
        public static double AngleDeg(this Vector2D vector, Vector2D target)
        {
        }

        [MethodImpl(0x8000)]
        public static double AngleRad(this Vector2D vector, Vector2D target)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(this Vector2D vector, Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(this Vector2D vector, ref Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double Dot(ref Vector2D vector, ref Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double DotPerp(this Vector2D vector, Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double DotPerp(this Vector2D vector, ref Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        public static double DotPerp(ref Vector2D vector, ref Vector2D value)
        {
        }

        [MethodImpl(0x8000)]
        internal static Information GetInformation(IList<Vector2D> points, double epsilon)
        {
        }

        [MethodImpl(0x8000)]
        public static double GrowLength(ref Vector2D vector, double lengthDelta, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static double Length(this Vector2D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static double LengthSqr(this Vector2D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static double Normalize(ref Vector2D vector, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2D Perp(this Vector2D vector)
        {
        }

        public static Vector2D Replicate(double value) => 
            new Vector2D(value, value);

        [MethodImpl(0x8000)]
        public static double SetLength(ref Vector2D vector, double lengthValue, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static string ToStringEx(this Vector2D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D ToVector3XY(this Vector2D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D ToVector3XZ(this Vector2D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D ToVector3YZ(this Vector2D vector)
        {
        }

        internal class Information
        {
            public int Dimension;
            public Vector2D Min;
            public Vector2D Max;
            public double MaxRange;
            public Vector2D Origin;
            public Vector2D[] Direction;
            public int[] Extreme;
            public bool ExtremeCCW;
        }
    }
}

