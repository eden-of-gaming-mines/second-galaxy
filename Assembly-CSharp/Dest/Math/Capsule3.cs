﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Capsule3
    {
        public Segment3 Segment;
        public double Radius;
        [MethodImpl(0x8000)]
        public Capsule3(ref Segment3 segment, double radius)
        {
        }

        public Capsule3(Segment3 segment, double radius)
        {
            this.Segment = segment;
            this.Radius = radius;
        }
    }
}

