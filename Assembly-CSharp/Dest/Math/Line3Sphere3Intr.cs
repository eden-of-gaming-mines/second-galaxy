﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Line3Sphere3Intr
    {
        public IntersectionTypes IntersectionType;
        public int Quantity;
        public Vector3D Point0;
        public Vector3D Point1;
        public double LineParameter0;
        public double LineParameter1;
    }
}

