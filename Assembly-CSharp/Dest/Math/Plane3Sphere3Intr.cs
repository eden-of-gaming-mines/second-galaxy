﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Plane3Sphere3Intr
    {
        public IntersectionTypes IntersectionType;
        public Circle3 Circle;
    }
}

