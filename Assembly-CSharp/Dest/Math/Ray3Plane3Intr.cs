﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Ray3Plane3Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector3D Point;
        public double RayParameter;
    }
}

