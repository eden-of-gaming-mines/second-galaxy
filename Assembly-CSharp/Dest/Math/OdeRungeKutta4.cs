﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public class OdeRungeKutta4 : OdeSolver
    {
        private double _halfStep;
        private double _sixthStep;
        private double[] _temp1;
        private double[] _temp2;
        private double[] _temp3;
        private double[] _temp4;
        private double[] _yTemp;

        [MethodImpl(0x8000)]
        public OdeRungeKutta4(int dim, double step, OdeFunction function)
        {
        }

        [MethodImpl(0x8000)]
        public override void Update(double tIn, double[] yIn, ref double tOut, double[] yOut)
        {
        }

        public override double Step
        {
            get => 
                base.Step;
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

