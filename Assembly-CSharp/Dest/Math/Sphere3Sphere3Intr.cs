﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Sphere3Sphere3Intr
    {
        public Sphere3Sphere3IntrTypes IntersectionType;
        public Circle3 Circle;
        public Vector3D ContactPoint;
    }
}

