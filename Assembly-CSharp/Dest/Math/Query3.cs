﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    internal class Query3 : Query
    {
        private static double Zero;
        private Vector3D[] _vertices;

        public Query3(Vector3D[] vertices)
        {
            this._vertices = vertices;
        }

        [MethodImpl(0x8000)]
        public double Det3(double x0, double y0, double z0, double x1, double y1, double z1, double x2, double y2, double z2)
        {
        }

        [MethodImpl(0x8000)]
        public double Det4(double x0, double y0, double z0, double w0, double x1, double y1, double z1, double w1, double x2, double y2, double z2, double w2, double x3, double y3, double z3, double w3)
        {
        }

        public double Dot(double x0, double y0, double z0, double x1, double y1, double z1) => 
            (((x0 * x1) + (y0 * y1)) + (z0 * z1));

        [MethodImpl(0x8000)]
        public int ToCircumsphere(int i, int v0, int v1, int v2, int v3)
        {
        }

        [MethodImpl(0x8000)]
        public int ToCircumsphere(ref Vector3D test, int v0, int v1, int v2, int v3)
        {
        }

        [MethodImpl(0x8000)]
        public int ToPlane(int i, int v0, int v1, int v2)
        {
        }

        [MethodImpl(0x8000)]
        public int ToPlane(ref Vector3D test, int v0, int v1, int v2)
        {
        }

        [MethodImpl(0x8000)]
        public int ToTetrahedron(int i, int v0, int v1, int v2, int v3)
        {
        }

        [MethodImpl(0x8000)]
        public int ToTetrahedron(ref Vector3D test, int v0, int v1, int v2, int v3)
        {
        }
    }
}

