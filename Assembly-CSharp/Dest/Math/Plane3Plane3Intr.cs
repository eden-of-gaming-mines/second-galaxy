﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Plane3Plane3Intr
    {
        public IntersectionTypes IntersectionType;
        public Line3 Line;
    }
}

