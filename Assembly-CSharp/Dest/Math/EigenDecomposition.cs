﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class EigenDecomposition
    {
        [MethodImpl(0x8000)]
        private static void DecreasingSort(double[] diagonal, double[] subdiagonal, double[,] matrix, ref bool isRotation)
        {
        }

        [MethodImpl(0x8000)]
        private static void GuaranteeRotation(double[,] matrix, bool isRotation)
        {
        }

        [MethodImpl(0x8000)]
        private static void IncreasingSort(double[] diagonal, double[] subdiagonal, double[,] matrix, ref bool isRotation)
        {
        }

        [MethodImpl(0x8000)]
        private static bool QLAlgorithm(double[] diagonal, double[] subdiagonal, double[,] matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static EigenData Solve(double[,] symmetricSquareMatrix, bool increasingSort)
        {
        }

        [MethodImpl(0x8000)]
        private static void Tridiagonal2(double[] diagonal, double[] subdiagonal, double[,] matrix, out bool isRotation)
        {
        }

        [MethodImpl(0x8000)]
        private static void Tridiagonal3(double[] diagonal, double[] subdiagonal, double[,] matrix, out bool isRotation)
        {
        }

        [MethodImpl(0x8000)]
        private static void TridiagonalN(double[] diagonal, double[] subdiagonal, double[,] matrix, out bool isRotation)
        {
        }
    }
}

