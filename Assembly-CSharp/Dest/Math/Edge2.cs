﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Edge2
    {
        public Vector2D Point0;
        public Vector2D Point1;
        public Vector2D Direction;
        public Vector2D Normal;
        public double Length;
    }
}

