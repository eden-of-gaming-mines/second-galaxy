﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ConcaveHull2
    {
        [MethodImpl(0x8000)]
        private static double CalcDistanceFromPointToEdge(ref Vector2D pointA, ref Vector2D v0, ref Vector2D v1)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Create(Vector2D[] points, out int[] concaveHull, int[] convexHull, double N, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        private static void Quicksort(InnerPoint[] x, int first, int last)
        {
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Edge
        {
            public int V0;
            public int V1;
            public Edge(int v0, int v1)
            {
                this.V0 = v0;
                this.V1 = v1;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct InnerPoint
        {
            public double AverageDistance;
            public double Distance0;
            public double Distance1;
            public int Index;
        }
    }
}

