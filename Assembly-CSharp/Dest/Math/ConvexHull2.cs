﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ConvexHull2
    {
        [MethodImpl(0x8000)]
        public static bool Create(IList<Vector2D> vertices, double epsilon, out int dimension, out int[] indices)
        {
        }

        [MethodImpl(0x8000)]
        private static bool Update(ref Edge hull, int i, Query2 query)
        {
        }

        private class Edge
        {
            public int V0;
            public int V1;
            public ConvexHull2.Edge E0;
            public ConvexHull2.Edge E1;
            public int Sign;
            public int Time;

            [MethodImpl(0x8000)]
            public Edge(int v0, int v1)
            {
            }

            [MethodImpl(0x8000)]
            public void DeleteSelf()
            {
            }

            [MethodImpl(0x8000)]
            public void GetIndices(out int[] indices)
            {
            }

            [MethodImpl(0x8000)]
            public int GetSign(int i, Query2 query)
            {
            }

            [MethodImpl(0x8000)]
            public void Insert(ConvexHull2.Edge adj0, ConvexHull2.Edge adj1)
            {
            }
        }
    }
}

