﻿namespace Dest.Math
{
    using System;

    public enum ProjectionPlanes
    {
        XY,
        XZ,
        YZ
    }
}

