﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class LinearSystem
    {
        [MethodImpl(0x8000)]
        public static bool Inverse(double[,] A, out double[,] invA)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Solve(double[,] A, double[] B, out double[] X)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Solve2(double[,] A, double[] B, out double[] X, double zeroTolerance = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Solve2(double[,] A, double[] B, out Vector2D X, double zeroTolerance = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Solve3(double[,] A, double[] B, out double[] X, double zeroTolerance = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Solve3(double[,] A, double[] B, out Vector3D X, double zeroTolerance = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static bool SolveTridiagonal(double[] A, double[] B, double[] C, double[] R, out double[] U)
        {
        }

        [MethodImpl(0x8000)]
        private static void SwapRows(double[,] matrix, int row0, int row1, int columnCount)
        {
        }
    }
}

