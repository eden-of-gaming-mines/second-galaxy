﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Triangle2Triangle2Intr
    {
        public IntersectionTypes IntersectionType;
        public int Quantity;
        public Vector2D Point0;
        public Vector2D Point1;
        public Vector2D Point2;
        public Vector2D Point3;
        public Vector2D Point4;
        public Vector2D Point5;
        public Vector2D this[int i]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            internal set
            {
            }
        }
    }
}

