﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct BrentsRoot
    {
        public double X;
        public int Iterations;
        public bool ExceededMaxIterations;
    }
}

