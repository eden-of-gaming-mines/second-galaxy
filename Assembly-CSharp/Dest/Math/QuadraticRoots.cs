﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct QuadraticRoots
    {
        public double X0;
        public double X1;
        public int RootCount;
        public double this[int rootIndex]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

