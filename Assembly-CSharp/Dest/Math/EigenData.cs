﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public class EigenData
    {
        private int _size;
        private double[] _diagonal;
        private double[,] _matrix;

        [MethodImpl(0x8000)]
        internal EigenData(double[] diagonal, double[,] matrix)
        {
        }

        public double GetEigenvalue(int index) => 
            this._diagonal[index];

        [MethodImpl(0x8000)]
        public double[] GetEigenvector(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void GetEigenvector(int index, double[] out_eigenvector)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2D GetEigenvector2(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetEigenvector3(int index)
        {
        }

        public int Size =>
            this._size;
    }
}

