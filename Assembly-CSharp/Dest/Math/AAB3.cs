﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct AAB3
    {
        public Vector3D Min;
        public Vector3D Max;
        [MethodImpl(0x8000)]
        public AAB3(ref Vector3D min, ref Vector3D max)
        {
        }

        public AAB3(Vector3D min, Vector3D max)
        {
            this.Min = min;
            this.Max = max;
        }

        [MethodImpl(0x8000)]
        public AAB3(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromPoint(ref Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromPoint(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromTwoPoints(ref Vector3D point0, ref Vector3D point1)
        {
        }

        public static AAB3 CreateFromTwoPoints(Vector3D point0, Vector3D point1) => 
            CreateFromTwoPoints(ref point0, ref point1);

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromPoints(IEnumerable<Vector3D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromPoints(IList<Vector3D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB3 CreateFromPoints(Vector3D[] points)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcCenterExtents(out Vector3D center, out Vector3D extents)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(out Vector3D vertex0, out Vector3D vertex1, out Vector3D vertex2, out Vector3D vertex3, out Vector3D vertex4, out Vector3D vertex5, out Vector3D vertex6, out Vector3D vertex7)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D[] CalcVertices()
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(Vector3D[] array)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcVolume()
        {
        }

        public double DistanceTo(Vector3D point) => 
            Distance.Point3AAB3(ref point, ref this);

        [MethodImpl(0x8000)]
        public Vector3D Project(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(ref Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(ref Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(ref AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

