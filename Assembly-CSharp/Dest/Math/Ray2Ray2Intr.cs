﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Ray2Ray2Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector2D Point;
        public double Parameter;
    }
}

