﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class OdeSolver
    {
        protected int _dim;
        protected double _step;
        protected OdeFunction _function;
        protected double[] _FValue;

        [MethodImpl(0x8000)]
        public OdeSolver(int dim, double step, OdeFunction function)
        {
        }

        public abstract void Update(double tIn, double[] yIn, ref double tOut, double[] yOut);

        public virtual double Step
        {
            get => 
                this._step;
            set => 
                (this._step = value);
        }
    }
}

