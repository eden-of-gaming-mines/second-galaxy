﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Matrix4X4
    {
        public double m00;
        public double m10;
        public double m20;
        public double m30;
        public double m01;
        public double m11;
        public double m21;
        public double m31;
        public double m02;
        public double m12;
        public double m22;
        public double m32;
        public double m03;
        public double m13;
        public double m23;
        public double m33;
        public double this[int row, int column]
        {
            get => 
                this[row + (column * 4)];
            set => 
                (this[row + (column * 4)] = value);
        }
        public double this[int index]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
        public Matrix4X4 inverse =>
            Inverse(this);
        public Matrix4X4 transpose =>
            Transpose(this);
        public bool isIdentity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        public static Matrix4X4 zero
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        public static Matrix4X4 identity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        [MethodImpl(0x8000)]
        public static Matrix4X4 operator *(Matrix4X4 lhs, Matrix4X4 rhs)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector4D operator *(Matrix4X4 lhs, Vector4D v)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator ==(Matrix4X4 lhs, Matrix4X4 rhs)
        {
        }

        public static bool operator !=(Matrix4X4 lhs, Matrix4X4 rhs) => 
            !(lhs == rhs);

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object other)
        {
        }

        public static Matrix4X4 Inverse(Matrix4X4 m) => 
            INTERNAL_CALL_Inverse(ref m);

        [MethodImpl(0x8000)]
        private static Matrix4X4 INTERNAL_CALL_Inverse(ref Matrix4X4 m)
        {
        }

        public static Matrix4X4 Transpose(Matrix4X4 m) => 
            INTERNAL_CALL_Transpose(ref m);

        [MethodImpl(0x8000)]
        private static Matrix4X4 INTERNAL_CALL_Transpose(ref Matrix4X4 m)
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D GetColumn(int i)
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D GetRow(int i)
        {
        }

        [MethodImpl(0x8000)]
        public void SetColumn(int i, Vector4D v)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRow(int i, Vector4D v)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D MultiplyPoint(Vector3D v)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D MultiplyPoint3x4(Vector3D v)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D MultiplyVector(Vector3D v)
        {
        }

        [MethodImpl(0x8000)]
        public static Matrix4X4 Scale(Vector3D v)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        [MethodImpl(0x8000)]
        public string ToString(string format)
        {
        }
    }
}

