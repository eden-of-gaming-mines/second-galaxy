﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Polynomial
    {
        private int _degree;
        private double[] _coeffs;

        public Polynomial(int degree)
        {
            this.Degree = degree;
        }

        [MethodImpl(0x8000)]
        public Polynomial CalcDerivative()
        {
        }

        [MethodImpl(0x8000)]
        public Polynomial CalcInversion()
        {
        }

        [MethodImpl(0x8000)]
        public void Compress(double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public Polynomial DeepCopy()
        {
        }

        [MethodImpl(0x8000)]
        public double Eval(double t)
        {
        }

        public int Degree
        {
            get => 
                this._degree;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public double this[int index]
        {
            get => 
                this._coeffs[index];
            set => 
                (this._coeffs[index] = value);
        }
    }
}

