﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Segment3Triangle3Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector3D Point;
        public double SegmentParameter;
        public double TriBary0;
        public double TriBary1;
        public double TriBary2;
    }
}

