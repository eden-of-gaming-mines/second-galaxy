﻿namespace Dest.Math
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Triangle3Triangle3Intr
    {
        public IntersectionTypes IntersectionType;
        public IntersectionTypes CoplanarIntersectionType;
        public bool Touching;
        public int Quantity;
        public Vector3D Point0;
        public Vector3D Point1;
        public Vector3D Point2;
        public Vector3D Point3;
        public Vector3D Point4;
        public Vector3D Point5;
        public Vector3D this[int i]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            internal set
            {
            }
        }
    }
}

