﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Segment2Triangle2Intr
    {
        public IntersectionTypes IntersectionType;
        public int Quantity;
        public Vector2D Point0;
        public Vector2D Point1;
    }
}

