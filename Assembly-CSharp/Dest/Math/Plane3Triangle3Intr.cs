﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Plane3Triangle3Intr
    {
        public IntersectionTypes IntersectionType;
        public int Quantity;
        public Vector3D Point0;
        public Vector3D Point1;
        public Vector3D Point2;
    }
}

