﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;

    public static class Integrator
    {
        private const int _degree = 5;
        private static double[] root;
        private static double[] coeff;

        [MethodImpl(0x8000)]
        public static double GaussianQuadrature(Func<double, double> function, double a, double b)
        {
        }

        [MethodImpl(0x8000)]
        public static double RombergIntegral(Func<double, double> function, double a, double b, int order)
        {
        }

        [MethodImpl(0x8000)]
        public static double TrapezoidRule(Func<double, double> function, double a, double b, int sampleCount)
        {
        }
    }
}

