﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Circle
    {
        [MethodImpl(0x8000)]
        public static Vector2D FindCircleNearestPoint(Vector2D circleCenter, double circleRadius, Vector2D p)
        {
        }

        [MethodImpl(0x8000)]
        public static bool FindCircleNearestTangentPoint(Vector2D circleCenter, double circleRadius, Vector2D p, bool clockwise, out Vector2D tp)
        {
        }
    }
}

