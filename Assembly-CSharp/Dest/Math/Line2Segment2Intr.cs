﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Line2Segment2Intr
    {
        public IntersectionTypes IntersectionType;
        public Vector2D Point;
        public double Parameter;
    }
}

