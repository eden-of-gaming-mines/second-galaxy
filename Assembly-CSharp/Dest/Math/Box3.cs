﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Box3
    {
        public Vector3D Center;
        public Vector3D Axis0;
        public Vector3D Axis1;
        public Vector3D Axis2;
        public Vector3D Extents;
        [MethodImpl(0x8000)]
        public Box3(ref Vector3D center, ref Vector3D axis0, ref Vector3D axis1, ref Vector3D axis2, ref Vector3D extents)
        {
        }

        [MethodImpl(0x8000)]
        public Box3(Vector3D center, Vector3D axis0, Vector3D axis1, Vector3D axis2, Vector3D extents)
        {
        }

        [MethodImpl(0x8000)]
        public Box3(ref AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public Box3(AAB3 box)
        {
        }

        [MethodImpl(0x8000)]
        public static Box3 CreateFromPoints(IList<Vector3D> points)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetAxis(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(out Vector3D vertex0, out Vector3D vertex1, out Vector3D vertex2, out Vector3D vertex3, out Vector3D vertex4, out Vector3D vertex5, out Vector3D vertex6, out Vector3D vertex7)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D[] CalcVertices()
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(Vector3D[] array)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcVolume()
        {
        }

        public double DistanceTo(Vector3D point) => 
            Distance.Point3Box3(ref point, ref this);

        [MethodImpl(0x8000)]
        public Vector3D Project(Vector3D point)
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(ref Vector3D point)
        {
        }

        public bool Contains(Vector3D point) => 
            this.Contains(ref point);

        [MethodImpl(0x8000)]
        public void Include(ref Box3 box)
        {
        }

        public void Include(Box3 box)
        {
            this.Include(ref box);
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

