﻿namespace Dest.Math
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct AAB2
    {
        public Vector2D Min;
        public Vector2D Max;
        [MethodImpl(0x8000)]
        public AAB2(ref Vector2D min, ref Vector2D max)
        {
        }

        public AAB2(Vector2D min, Vector2D max)
        {
            this.Min = min;
            this.Max = max;
        }

        [MethodImpl(0x8000)]
        public AAB2(double xMin, double xMax, double yMin, double yMax)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromPoint(ref Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromPoint(Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromTwoPoints(ref Vector2D point0, ref Vector2D point1)
        {
        }

        public static AAB2 CreateFromTwoPoints(Vector2D point0, Vector2D point1) => 
            CreateFromTwoPoints(ref point0, ref point1);

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromPoints(IEnumerable<Vector2D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromPoints(IList<Vector2D> points)
        {
        }

        [MethodImpl(0x8000)]
        public static AAB2 CreateFromPoints(Vector2D[] points)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcCenterExtents(out Vector2D center, out Vector2D extents)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(out Vector2D vertex0, out Vector2D vertex1, out Vector2D vertex2, out Vector2D vertex3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2D[] CalcVertices()
        {
        }

        [MethodImpl(0x8000)]
        public void CalcVertices(Vector2D[] array)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcArea()
        {
        }

        public double DistanceTo(Vector2D point) => 
            Distance.Point2AAB2(ref point, ref this);

        [MethodImpl(0x8000)]
        public Vector2D Project(Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(ref Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(ref Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(Vector2D point)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(ref AAB2 box)
        {
        }

        [MethodImpl(0x8000)]
        public void Include(AAB2 box)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

