﻿namespace Dest.Math
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Matrix4X4ex
    {
        public static readonly Matrix4X4 Identity;

        [MethodImpl(0x8000)]
        public static double CalcDeterminant(ref Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CopyMatrix(ref Matrix4X4 source, out Matrix4X4 destination)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotation(Vector3D rotationOrigin, Quaternion rotation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotation(ref Vector3D rotationOrigin, ref Quaternion rotation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationAngleAxis(double angleInDegrees, Vector3D rotationAxis, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationAngleUnitAxis(double angleInDegrees, Vector3D normalizedAxis, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationEuler(Vector3D eulerAngles, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationEuler(ref Vector3D eulerAngles, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationEuler(double eulerX, double eulerY, double eulerZ, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationFromColumns(Vector3D column0, Vector3D column1, Vector3D column2, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationFromColumns(ref Vector3D column0, ref Vector3D column1, ref Vector3D column2, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationX(double angleInDegrees, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationY(double angleInDegrees, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRotationZ(double angleInDegrees, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRT(Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRT(ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRT(Vector3D rotationOrigin, Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateRT(ref Vector3D rotationOrigin, ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateScale(Vector3D scale, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateScale(ref Vector3D scale, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateScale(double scale, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadow(Plane3 shadowPlane, Vector4D lightData, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadow(ref Plane3 shadowPlane, ref Vector4D lightData, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadowDirectional(Plane3 shadowPlane, Vector3D dirLightOppositeDirection, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadowDirectional(ref Plane3 shadowPlane, ref Vector3D dirLightOppositeDirection, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadowPoint(Plane3 shadowPlane, Vector3D pointLightPosition, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateShadowPoint(ref Plane3 shadowPlane, ref Vector3D pointLightPosition, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(Vector3D scaling, Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(ref Vector3D scaling, ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(double scaling, Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(double scaling, ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(Vector3D scaling, Vector3D rotationOrigin, Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(ref Vector3D scaling, ref Vector3D rotationOrigin, ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(double scaling, Vector3D rotationOrigin, Quaternion rotation, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateSRT(double scaling, ref Vector3D rotationOrigin, ref Quaternion rotation, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateST(Vector3D scaling, Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateST(ref Vector3D scaling, ref Vector3D translation, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateTranslation(Vector3D position, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void CreateTranslation(ref Vector3D position, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void Inverse(ref Matrix4X4 matrix, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static void Inverse(ref Matrix4X4 matrix, out Matrix4X4 inverse, double epsilon = 1E-05)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector4D Multiply(ref Matrix4X4 matrix, Vector4D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector4D Multiply(ref Matrix4X4 matrix, ref Vector4D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static void Multiply(ref Matrix4X4 matrix, double scalar)
        {
        }

        [MethodImpl(0x8000)]
        public static void Multiply(ref Matrix4X4 matrix0, ref Matrix4X4 matrix1, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void Multiply(ref Matrix4X4 matrix, double scalar, out Matrix4X4 result)
        {
        }

        [MethodImpl(0x8000)]
        public static void MultiplyLeft(ref Matrix4X4 matrix1, ref Matrix4X4 matrix0)
        {
        }

        [MethodImpl(0x8000)]
        public static void MultiplyRight(ref Matrix4X4 matrix0, ref Matrix4X4 matrix1)
        {
        }

        [MethodImpl(0x8000)]
        public static void QuaternionToRotationMatrix(Quaternion quaternion, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void QuaternionToRotationMatrix(ref Quaternion quaternion, out Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void RotationMatrixToQuaternion(ref Matrix4X4 matrix, out Quaternion quaternion)
        {
        }

        [MethodImpl(0x8000)]
        public static void Transpose(ref Matrix4X4 matrix)
        {
        }

        [MethodImpl(0x8000)]
        public static void Transpose(ref Matrix4X4 matrix, out Matrix4X4 transpose)
        {
        }
    }
}

