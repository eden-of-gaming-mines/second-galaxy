﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Segment3Box3Intr
    {
        public IntersectionTypes IntersectionType;
        public int Quantity;
        public Vector3D Point0;
        public Vector3D Point1;
    }
}

