﻿namespace Dest.Math
{
    using System;

    public enum IntersectionTypes
    {
        Empty,
        Point,
        Segment,
        Ray,
        Line,
        Polygon,
        Plane,
        Polyhedron,
        Other
    }
}

