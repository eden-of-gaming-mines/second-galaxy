﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Edge3
    {
        public Vector3D Point0;
        public Vector3D Point1;
        public Vector3D Direction;
        public Vector3D Normal;
        public double Length;
    }
}

