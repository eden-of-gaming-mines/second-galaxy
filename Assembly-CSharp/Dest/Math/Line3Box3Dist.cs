﻿namespace Dest.Math
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Line3Box3Dist
    {
        public Vector3D ClosestPoint0;
        public Vector3D ClosestPoint1;
        public double LineParameter;
    }
}

