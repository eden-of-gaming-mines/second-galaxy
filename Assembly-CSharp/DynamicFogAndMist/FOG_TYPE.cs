﻿namespace DynamicFogAndMist
{
    using System;

    public enum FOG_TYPE
    {
        DesktopFogWithSkyHaze,
        MobileFogWithSkyHaze,
        MobileFogOnlyGround,
        DesktopFogPlusWithSkyHaze,
        MobileFogSimple,
        MobileFogBasic,
        MobileFogOrthogonal,
        DesktopFogPlusOrthogonal
    }
}

