﻿namespace DynamicFogAndMist
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, RequireComponent(typeof(Camera)), HelpURL("http://kronnect.com/taptapgo")]
    public class DynamicFog : MonoBehaviour
    {
        public FOG_TYPE effectType;
        public FOG_PRESET preset;
        public bool useFogVolumes;
        public bool enableDithering;
        [Range(0f, 1f)]
        public float alpha;
        [Range(0f, 1f)]
        public float noiseStrength;
        [Range(0.01f, 1f)]
        public float noiseScale;
        [Range(0f, 0.999f)]
        public float distance;
        [Range(0.0001f, 2f)]
        public float distanceFallOff;
        [Range(0f, 1.2f)]
        public float maxDistance;
        [Range(0.0001f, 0.5f)]
        public float maxDistanceFallOff;
        [Range(0f, 500f)]
        public float height;
        [Range(0f, 500f)]
        public float maxHeight;
        [Range(0.0001f, 1f)]
        public float heightFallOff;
        public float baselineHeight;
        public bool clipUnderBaseline;
        [Range(0f, 15f)]
        public float turbulence;
        [Range(0f, 5f)]
        public float speed;
        public Color color;
        public Color color2;
        [Range(0f, 500f)]
        public float skyHaze;
        [Range(0f, 1f)]
        public float skySpeed;
        [Range(0f, 1f)]
        public float skyNoiseStrength;
        [Range(0f, 1f)]
        public float skyAlpha;
        public GameObject sun;
        public bool fogOfWarEnabled;
        public Vector3 fogOfWarCenter;
        public Vector3 fogOfWarSize;
        public int fogOfWarTextureSize;
        public bool useSinglePassStereoRenderingMatrix;
        public bool useXZDistance;
        [Range(0f, 1f)]
        public float scattering;
        public Color scatteringColor;
        private Material fogMatAdv;
        private Material fogMatFogSky;
        private Material fogMatOnlyFog;
        private Material fogMatVol;
        private Material fogMatSimple;
        private Material fogMatBasic;
        private Material fogMatOrthogonal;
        private Material fogMatDesktopPlusOrthogonal;
        [SerializeField]
        private Material fogMat;
        private float initialFogAlpha;
        private float targetFogAlpha;
        private float initialSkyHazeAlpha;
        private float targetSkyHazeAlpha;
        private bool targetFogColors;
        private Color initialFogColor1;
        private Color targetFogColor1;
        private Color initialFogColor2;
        private Color targetFogColor2;
        private float transitionDuration;
        private float transitionStartTime;
        private float currentFogAlpha;
        private float currentSkyHazeAlpha;
        private Color currentFogColor1;
        private Color currentFogColor2;
        private Camera currentCamera;
        private Texture2D fogOfWarTexture;
        private Color32[] fogOfWarColorBuffer;
        private Light sunLight;
        private Vector3 sunDirection;
        private Color sunColor;
        private float sunIntensity;
        private static DynamicFog _fog;
        private List<string> shaderKeywords;
        private bool matOrtho;

        [MethodImpl(0x8000)]
        public void CheckPreset()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTargetAlpha(float duration)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTargetColors(float duration)
        {
        }

        [MethodImpl(0x8000)]
        private void CopyTransitionValues()
        {
        }

        [MethodImpl(0x8000)]
        public string GetCurrentPresetName()
        {
        }

        [MethodImpl(0x8000)]
        private int GetScaledSize(int size, float factor)
        {
        }

        [MethodImpl(0x8000)]
        private void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        private void OnEnable()
        {
            this.Init();
            this.UpdateMaterialProperties();
        }

        [MethodImpl(0x8000)]
        private void OnPreCull()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        private void Reset()
        {
            this.UpdateMaterialProperties();
        }

        [MethodImpl(0x8000)]
        public void ResetFogOfWar()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetFogOfWarAlpha(Vector3 worldPosition, float radius)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetMaterial()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFogOfWarAlpha(Vector3 worldPosition, float radius, float fogNewAlpha)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSkyData()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetAlpha(float newFogAlpha, float newSkyHazeAlpha, float duration)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetColors(Color color1, Color color2, float duration)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFogColor()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFogOfWarTexture()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMaterialProperties()
        {
        }

        public static DynamicFog instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Camera fogCamera =>
            this.currentCamera;
    }
}

