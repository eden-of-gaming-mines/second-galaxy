﻿namespace DynamicFogAndMist
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class FogVolume : MonoBehaviour
    {
        private const float GRAY = 0.8901961f;
        [Range(0f, 1f), Tooltip("Target alpha for fog when camera enters this fog volume")]
        public float targetFogAlpha;
        [Range(0f, 1f), Tooltip("Target alpha for sky haze when camera enters this fog volume")]
        public float targetSkyHazeAlpha;
        [Tooltip("Target fog color 1 when gamera enters this fog folume")]
        public Color targetFogColor1;
        [Tooltip("Target fog color 2 when gamera enters this fog folume")]
        public Color targetFogColor2;
        [Tooltip("Set this to zero for changing fog alpha immediately upon enter/exit fog volume.")]
        public float transitionDuration;
        private DynamicFog fog;
        private bool cameraInside;

        [MethodImpl(0x8000)]
        private void OnTriggerEnter(Collider other)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerExit(Collider other)
        {
        }

        private void Start()
        {
            this.fog = DynamicFog.instance;
        }
    }
}

