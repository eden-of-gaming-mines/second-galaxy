﻿namespace DynamicFogAndMist
{
    using System;

    public enum FOG_PRESET
    {
        Clear,
        Mist,
        WindyMist,
        GroundFog,
        Fog,
        HeavyFog,
        SandStorm,
        Custom
    }
}

