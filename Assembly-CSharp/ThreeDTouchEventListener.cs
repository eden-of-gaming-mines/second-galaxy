﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThreeDTouchEventListener : MonoBehaviour, IUpdateSelectedHandler, IPointerExitHandler, IEventSystemHandler
{
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnThreeDTouchTriggered;
    public float ThreeDTouchThreshhold;
    public bool IsThreeDTouchTriggered;

    public event Action EventOnThreeDTouchTriggered
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public void OnDisable()
    {
        this.IsThreeDTouchTriggered = false;
    }

    public void OnPointerExit(PointerEventData data)
    {
        this.IsThreeDTouchTriggered = false;
    }

    [MethodImpl(0x8000)]
    public void OnUpdateSelected(BaseEventData data)
    {
    }
}

