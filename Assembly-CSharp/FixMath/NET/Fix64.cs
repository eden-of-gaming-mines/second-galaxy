﻿namespace FixMath.NET
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Fix64 : IEquatable<Fix64>, IComparable<Fix64>
    {
        private readonly long m_rawValue;
        public static readonly decimal Precision;
        public static readonly Fix64 MaxValue;
        public static readonly Fix64 MinValue;
        public static readonly Fix64 One;
        public static readonly Fix64 Zero;
        public static readonly Fix64 Pi;
        public static readonly Fix64 PiOver2;
        public static readonly Fix64 PiTimes2;
        public static readonly Fix64 PiInv;
        public static readonly Fix64 PiOver2Inv;
        private static readonly Fix64 LutInterval;
        private const long MAX_VALUE = 0x7fffffffffffffffL;
        private const long MIN_VALUE = -9223372036854775808L;
        private const int NUM_BITS = 0x40;
        private const int FRACTIONAL_PLACES = 0x20;
        private const long ONE = 0x100000000L;
        private const long PI_TIMES_2 = 0x6487ed511L;
        private const long PI = 0x3243f6a88L;
        private const long PI_OVER_2 = 0x1921fb544L;
        private const int LUT_SIZE = 0x3243f;
        private static long[] m_sinLut;
        private static long[] m_tanLut;
        private Fix64(long rawValue)
        {
            this.m_rawValue = rawValue;
        }

        [MethodImpl(0x8000)]
        public Fix64(int value)
        {
        }

        [MethodImpl(0x8000)]
        public static int Sign(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Abs(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastAbs(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Floor(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Ceiling(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Round(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator +(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastAdd(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator -(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastSub(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        private static long AddOverflowHelper(long x, long y, ref bool overflow)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator *(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastMul(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        private static int CountLeadingZeroes(ulong x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator /(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator %(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastMod(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 operator -(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator ==(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator !=(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator >(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator <(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator >=(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator <=(Fix64 x, Fix64 y)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Sqrt(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Sin(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastSin(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        private static long ClampSinValue(long angle, out bool flipHorizontal, out bool flipVertical)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Cos(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 FastCos(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Tan(Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static Fix64 Atan2(Fix64 y, Fix64 x)
        {
        }

        [MethodImpl(0x8000)]
        public static explicit operator Fix64(long value)
        {
        }

        public static explicit operator long(Fix64 value) => 
            (value.m_rawValue >> 0x20);

        public static explicit operator Fix64(float value) => 
            new Fix64((long) (value * 4.294967E+09f));

        public static explicit operator float(Fix64 value) => 
            (((float) value.m_rawValue) / 4.294967E+09f);

        [MethodImpl(0x8000)]
        public static explicit operator Fix64(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static explicit operator double(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public static explicit operator Fix64(decimal value)
        {
        }

        [MethodImpl(0x8000)]
        public static explicit operator decimal(Fix64 value)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public bool Equals(Fix64 other)
        {
        }

        [MethodImpl(0x8000)]
        public int CompareTo(Fix64 other)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public static Fix64 FromRaw(long rawValue) => 
            new Fix64(rawValue);

        [MethodImpl(0x8000)]
        internal static void GenerateSinLut()
        {
        }

        [MethodImpl(0x8000)]
        internal static void GenerateTanLut()
        {
        }

        public long RawValue =>
            this.m_rawValue;
        public static long[] SinLut
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            private set
            {
            }
        }
        [MethodImpl(0x8000)]
        private static long[] InitSinLutArray()
        {
        }

        public static long[] TanLut
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            private set
            {
            }
        }
        [MethodImpl(0x8000)]
        private static long[] InitTanLutArray()
        {
        }

        [MethodImpl(0x8000)]
        static Fix64()
        {
        }
    }
}

