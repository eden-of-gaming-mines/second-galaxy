﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class EffectGroupPlayManager : MonoBehaviour
{
    public List<EffectGroupItem> EffectGroupItemList;

    private void Awake()
    {
        this.GroupAddPlayMgr();
    }

    [MethodImpl(0x8000)]
    private void GroupAddPlayMgr()
    {
    }

    public void ReplayAllEffectGroup()
    {
        base.StopAllCoroutines();
        this.RePlayAllEffectGroupImp();
    }

    [MethodImpl(0x8000)]
    private void RePlayAllEffectGroupImp()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private IEnumerator ReplayEffectGroup(EffectGroupItem item)
    {
    }

    public void ReversePlayAllEffectGroup()
    {
        base.StopAllCoroutines();
        this.ReversePlayAllEffectGroupImp();
    }

    [MethodImpl(0x8000)]
    private void ReversePlayAllEffectGroupImp()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private IEnumerator ReversePlayEffectGroup(EffectGroupItem item, float maxDelayTime)
    {
    }

    [CompilerGenerated]
    private sealed class <ReplayEffectGroup>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal EffectGroupItem item;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        [MethodImpl(0x8000)]
        public bool MoveNext()
        {
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <ReversePlayEffectGroup>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal EffectGroupItem item;
        internal float maxDelayTime;
        internal float <reverseDelayTime>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.<reverseDelayTime>__0 = Math.Abs((float) (this.item.DelayTime - this.maxDelayTime));
                    if (this.<reverseDelayTime>__0 <= 0f)
                    {
                        break;
                    }
                    this.$current = new WaitForSeconds(this.<reverseDelayTime>__0);
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    break;

                default:
                    goto TR_0000;
            }
            this.item.EffectParentTransfrom.gameObject.GetComponent<EffectPlayManager>().RePlayReverseTween();
            this.$PC = -1;
        TR_0000:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }
}

