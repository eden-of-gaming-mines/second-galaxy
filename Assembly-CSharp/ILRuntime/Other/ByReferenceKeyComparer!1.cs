﻿namespace ILRuntime.Other
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class ByReferenceKeyComparer<T> : IEqualityComparer<T>
    {
        [MethodImpl(0x8000)]
        public bool Equals(T x, T y)
        {
        }

        public int GetHashCode(T obj) => 
            RuntimeHelpers.GetHashCode(obj);
    }
}

