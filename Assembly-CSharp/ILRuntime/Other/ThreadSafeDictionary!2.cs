﻿namespace ILRuntime.Other
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ThreadSafeDictionary<K, V> : IDictionary<K, V>, ICollection<KeyValuePair<K, V>>, IEnumerable<KeyValuePair<K, V>>, IEnumerable
    {
        private Dictionary<K, V> dic;

        [MethodImpl(0x8000)]
        public void Add(KeyValuePair<K, V> item)
        {
        }

        [MethodImpl(0x8000)]
        public void Add(K key, V value)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool Contains(KeyValuePair<K, V> item)
        {
        }

        [MethodImpl(0x8000)]
        public bool ContainsKey(K key)
        {
        }

        public void CopyTo(KeyValuePair<K, V>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<K, V> item)
        {
            throw new NotImplementedException();
        }

        [MethodImpl(0x8000)]
        public bool Remove(K key)
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        [MethodImpl(0x8000)]
        public bool TryGetValue(K key, out V value)
        {
        }

        public Dictionary<K, V> InnerDictionary =>
            this.dic;

        public V this[K key]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int Count
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsReadOnly
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ICollection<K> Keys
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ICollection<V> Values
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}

