﻿namespace ILRuntime.Mono
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal static class TypeExtensions
    {
        public static System.Reflection.Assembly Assembly(this Type type) => 
            type.Assembly;

        public static MethodBase DeclaringMethod(this Type type) => 
            type.DeclaringMethod;

        public static Type[] GetGenericArguments(this Type type) => 
            type.GetGenericArguments();

        public static TypeCode GetTypeCode(this Type type) => 
            Type.GetTypeCode(type);

        public static bool IsGenericType(this Type type) => 
            type.IsGenericType;

        public static bool IsGenericTypeDefinition(this Type type) => 
            type.IsGenericTypeDefinition;

        public static bool IsValueType(this Type type) => 
            type.IsValueType;
    }
}

