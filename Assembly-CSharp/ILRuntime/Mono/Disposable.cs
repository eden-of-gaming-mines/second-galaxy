﻿namespace ILRuntime.Mono
{
    using System;

    internal static class Disposable
    {
        public static Disposable<T> NotOwned<T>(T value) where T: class, IDisposable => 
            new Disposable<T>(value, false);

        public static Disposable<T> Owned<T>(T value) where T: class, IDisposable => 
            new Disposable<T>(value, true);
    }
}

