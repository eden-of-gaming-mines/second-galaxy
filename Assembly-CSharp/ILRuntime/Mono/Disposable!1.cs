﻿namespace ILRuntime.Mono
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct Disposable<T> : IDisposable where T: class, IDisposable
    {
        internal readonly T value;
        private readonly bool owned;
        public Disposable(T value, bool owned)
        {
            this.value = value;
            this.owned = owned;
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }
    }
}

