﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [DebuggerDisplay("{AttributeType}")]
    public sealed class SecurityAttribute : ICustomAttribute
    {
        private TypeReference attribute_type;
        internal Collection<CustomAttributeNamedArgument> fields;
        internal Collection<CustomAttributeNamedArgument> properties;

        public SecurityAttribute(TypeReference attributeType)
        {
            this.attribute_type = attributeType;
        }

        bool ICustomAttribute.HasConstructorArguments =>
            false;

        Collection<CustomAttributeArgument> ICustomAttribute.ConstructorArguments
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public TypeReference AttributeType
        {
            get => 
                this.attribute_type;
            set => 
                (this.attribute_type = value);
        }

        public bool HasFields =>
            !this.fields.IsNullOrEmpty<CustomAttributeNamedArgument>();

        public Collection<CustomAttributeNamedArgument> Fields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasProperties =>
            !this.properties.IsNullOrEmpty<CustomAttributeNamedArgument>();

        public Collection<CustomAttributeNamedArgument> Properties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

