﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class LinkedResource : Resource
    {
        internal byte[] hash;
        private string file;

        public LinkedResource(string name, ManifestResourceAttributes flags) : base(name, flags)
        {
        }

        [MethodImpl(0x8000)]
        public LinkedResource(string name, ManifestResourceAttributes flags, string file)
        {
        }

        public byte[] Hash =>
            this.hash;

        public string File
        {
            get => 
                this.file;
            set => 
                (this.file = value);
        }

        public override ILRuntime.Mono.Cecil.ResourceType ResourceType =>
            ILRuntime.Mono.Cecil.ResourceType.Linked;
    }
}

