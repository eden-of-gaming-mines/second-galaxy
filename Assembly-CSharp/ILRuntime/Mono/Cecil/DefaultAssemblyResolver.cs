﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DefaultAssemblyResolver : BaseAssemblyResolver
    {
        private readonly IDictionary<string, AssemblyDefinition> cache;

        [MethodImpl(0x8000)]
        protected override void Dispose(bool disposing)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterAssembly(AssemblyDefinition assembly)
        {
        }

        [MethodImpl(0x8000)]
        public override AssemblyDefinition Resolve(AssemblyNameReference name)
        {
        }
    }
}

