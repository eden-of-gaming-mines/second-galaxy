﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public class ModuleReference : IMetadataScope, IMetadataTokenProvider
    {
        private string name;
        internal ILRuntime.Mono.Cecil.MetadataToken token;

        [MethodImpl(0x8000)]
        internal ModuleReference()
        {
        }

        public ModuleReference(string name) : this()
        {
            this.name = name;
        }

        public override string ToString() => 
            this.name;

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public virtual ILRuntime.Mono.Cecil.MetadataScopeType MetadataScopeType =>
            ILRuntime.Mono.Cecil.MetadataScopeType.ModuleReference;

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }
    }
}

