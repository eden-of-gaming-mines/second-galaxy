﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class CallSite : IMethodSignature, IMetadataTokenProvider
    {
        private readonly MethodReference signature;

        [MethodImpl(0x8000)]
        internal CallSite()
        {
        }

        [MethodImpl(0x8000)]
        public CallSite(TypeReference returnType)
        {
        }

        public override string ToString() => 
            this.FullName;

        public bool HasThis
        {
            get => 
                this.signature.HasThis;
            set => 
                (this.signature.HasThis = value);
        }

        public bool ExplicitThis
        {
            get => 
                this.signature.ExplicitThis;
            set => 
                (this.signature.ExplicitThis = value);
        }

        public MethodCallingConvention CallingConvention
        {
            get => 
                this.signature.CallingConvention;
            set => 
                (this.signature.CallingConvention = value);
        }

        public bool HasParameters =>
            this.signature.HasParameters;

        public Collection<ParameterDefinition> Parameters =>
            this.signature.Parameters;

        public TypeReference ReturnType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ILRuntime.Mono.Cecil.MethodReturnType MethodReturnType =>
            this.signature.MethodReturnType;

        public string Name
        {
            get => 
                string.Empty;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public string Namespace
        {
            get => 
                string.Empty;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public ModuleDefinition Module =>
            this.ReturnType.Module;

        public IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.signature.token;
            set => 
                (this.signature.token = value);
        }

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

