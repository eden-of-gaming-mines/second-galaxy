﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface IGenericInstance : IMetadataTokenProvider
    {
        bool HasGenericArguments { get; }

        Collection<TypeReference> GenericArguments { get; }
    }
}

