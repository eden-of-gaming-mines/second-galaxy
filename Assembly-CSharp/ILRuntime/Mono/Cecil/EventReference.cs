﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class EventReference : MemberReference
    {
        private TypeReference event_type;

        [MethodImpl(0x8000)]
        protected EventReference(string name, TypeReference eventType)
        {
        }

        public abstract EventDefinition Resolve();
        protected override IMemberDefinition ResolveDefinition() => 
            this.Resolve();

        public TypeReference EventType
        {
            get => 
                this.event_type;
            set => 
                (this.event_type = value);
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

