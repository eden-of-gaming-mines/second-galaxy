﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SentinelType : TypeSpecification
    {
        [MethodImpl(0x8000)]
        public SentinelType(TypeReference type)
        {
        }

        public override bool IsValueType
        {
            get => 
                false;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override bool IsSentinel =>
            true;
    }
}

