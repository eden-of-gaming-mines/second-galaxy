﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class FunctionPointerType : TypeSpecification, IMethodSignature, IMetadataTokenProvider
    {
        private readonly MethodReference function;

        public override TypeReference GetElementType() => 
            this;

        public override TypeDefinition Resolve() => 
            null;

        public bool HasThis
        {
            get => 
                this.function.HasThis;
            set => 
                (this.function.HasThis = value);
        }

        public bool ExplicitThis
        {
            get => 
                this.function.ExplicitThis;
            set => 
                (this.function.ExplicitThis = value);
        }

        public MethodCallingConvention CallingConvention
        {
            get => 
                this.function.CallingConvention;
            set => 
                (this.function.CallingConvention = value);
        }

        public bool HasParameters =>
            this.function.HasParameters;

        public Collection<ParameterDefinition> Parameters =>
            this.function.Parameters;

        public TypeReference ReturnType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ILRuntime.Mono.Cecil.MethodReturnType MethodReturnType =>
            this.function.MethodReturnType;

        public override string Name
        {
            get => 
                this.function.Name;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override string Namespace
        {
            get => 
                string.Empty;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override ModuleDefinition Module =>
            this.ReturnType.Module;

        public override IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override bool IsFunctionPointer =>
            true;

        public override bool ContainsGenericParameter =>
            this.function.ContainsGenericParameter;

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

