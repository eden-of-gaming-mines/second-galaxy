﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class GenericParameterCollection : Collection<GenericParameter>
    {
        private readonly IGenericParameterProvider owner;

        internal GenericParameterCollection(IGenericParameterProvider owner)
        {
            this.owner = owner;
        }

        internal GenericParameterCollection(IGenericParameterProvider owner, int capacity) : base(capacity)
        {
            this.owner = owner;
        }

        protected override void OnAdd(GenericParameter item, int index)
        {
            this.UpdateGenericParameter(item, index);
        }

        [MethodImpl(0x8000)]
        protected override void OnInsert(GenericParameter item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnRemove(GenericParameter item, int index)
        {
        }

        protected override void OnSet(GenericParameter item, int index)
        {
            this.UpdateGenericParameter(item, index);
        }

        [MethodImpl(0x8000)]
        private void UpdateGenericParameter(GenericParameter item, int index)
        {
        }
    }
}

