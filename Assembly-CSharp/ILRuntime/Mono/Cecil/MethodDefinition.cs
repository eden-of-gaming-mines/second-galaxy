﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MethodDefinition : MethodReference, IMemberDefinition, ISecurityDeclarationProvider, ICustomDebugInformationProvider, ICustomAttributeProvider, IMetadataTokenProvider
    {
        private ushort attributes;
        private ushort impl_attributes;
        internal volatile bool sem_attrs_ready;
        internal MethodSemanticsAttributes sem_attrs;
        private Collection<CustomAttribute> custom_attributes;
        private Collection<SecurityDeclaration> security_declarations;
        internal uint rva;
        internal ILRuntime.Mono.Cecil.PInvokeInfo pinvoke;
        private Collection<MethodReference> overrides;
        internal MethodBody body;
        internal MethodDebugInformation debug_info;
        internal Collection<CustomDebugInformation> custom_infos;
        [CompilerGenerated]
        private static Action<MethodDefinition, MetadataReader> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<MethodDefinition, MetadataReader, MethodBody> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<MethodDefinition, MetadataReader, ILRuntime.Mono.Cecil.PInvokeInfo> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<MethodDefinition, MetadataReader, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<MethodDefinition, MetadataReader, Collection<MethodReference>> <>f__am$cache4;

        [MethodImpl(0x8000)]
        internal MethodDefinition()
        {
        }

        [MethodImpl(0x8000)]
        public MethodDefinition(string name, MethodAttributes attributes, TypeReference returnType)
        {
        }

        [MethodImpl(0x8000)]
        internal void ReadSemantics()
        {
        }

        public override MethodDefinition Resolve() => 
            this;

        public override string Name
        {
            get => 
                base.Name;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public MethodAttributes Attributes
        {
            get => 
                ((MethodAttributes) this.attributes);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public MethodImplAttributes ImplAttributes
        {
            get => 
                ((MethodImplAttributes) this.impl_attributes);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public MethodSemanticsAttributes SemanticsAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.sem_attrs = value);
        }

        internal MethodDefinitionProjection WindowsRuntimeProjection
        {
            get => 
                ((MethodDefinitionProjection) base.projection);
            set => 
                (base.projection = value);
        }

        public bool HasSecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<SecurityDeclaration> SecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int RVA =>
            ((int) this.rva);

        public bool HasBody
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public MethodBody Body
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public MethodDebugInformation DebugInformation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.debug_info = value);
        }

        public bool HasPInvokeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.PInvokeInfo PInvokeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                this.IsPInvokeImpl = true;
                this.pinvoke = value;
            }
        }

        public bool HasOverrides
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<MethodReference> Overrides
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool HasGenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override Collection<GenericParameter> GenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomDebugInformations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomDebugInformation> CustomDebugInformations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsCompilerControlled
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPrivate
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamilyAndAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 3);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamily
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamilyOrAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 5);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 6);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsStatic
        {
            get => 
                this.attributes.GetAttributes(0x10);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFinal
        {
            get => 
                this.attributes.GetAttributes(0x20);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsVirtual
        {
            get => 
                this.attributes.GetAttributes(0x40);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsHideBySig
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsReuseSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNewSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCheckAccessOnOverride
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAbstract
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPInvokeImpl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsUnmanagedExport
        {
            get => 
                this.attributes.GetAttributes(8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasSecurity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsIL
        {
            get => 
                this.impl_attributes.GetMaskedAttributes(3, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNative
        {
            get => 
                this.impl_attributes.GetMaskedAttributes(3, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntime
        {
            get => 
                this.impl_attributes.GetMaskedAttributes(3, 3);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsUnmanaged
        {
            get => 
                this.impl_attributes.GetMaskedAttributes(4, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsManaged
        {
            get => 
                this.impl_attributes.GetMaskedAttributes(4, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsForwardRef
        {
            get => 
                this.impl_attributes.GetAttributes(0x10);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPreserveSig
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsInternalCall
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSynchronized
        {
            get => 
                this.impl_attributes.GetAttributes(0x20);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool NoInlining
        {
            get => 
                this.impl_attributes.GetAttributes(8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool NoOptimization
        {
            get => 
                this.impl_attributes.GetAttributes(0x40);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool AggressiveInlining
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSetter
        {
            get => 
                this.GetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.Setter));
            set => 
                this.SetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.Setter), value);
        }

        public bool IsGetter
        {
            get => 
                this.GetSemantics(MethodSemanticsAttributes.Getter);
            set => 
                this.SetSemantics(MethodSemanticsAttributes.Getter, value);
        }

        public bool IsOther
        {
            get => 
                this.GetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.Other));
            set => 
                this.SetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.Other), value);
        }

        public bool IsAddOn
        {
            get => 
                this.GetSemantics(MethodSemanticsAttributes.AddOn);
            set => 
                this.SetSemantics(MethodSemanticsAttributes.AddOn, value);
        }

        public bool IsRemoveOn
        {
            get => 
                this.GetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.RemoveOn));
            set => 
                this.SetSemantics((MethodSemanticsAttributes.None | MethodSemanticsAttributes.RemoveOn), value);
        }

        public bool IsFire
        {
            get => 
                this.GetSemantics(MethodSemanticsAttributes.Fire);
            set => 
                this.SetSemantics(MethodSemanticsAttributes.Fire, value);
        }

        public TypeDefinition DeclaringType
        {
            get => 
                ((TypeDefinition) base.DeclaringType);
            set => 
                (base.DeclaringType = value);
        }

        public bool IsConstructor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsDefinition =>
            true;
    }
}

