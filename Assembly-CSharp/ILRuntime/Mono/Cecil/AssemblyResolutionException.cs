﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [Serializable]
    public sealed class AssemblyResolutionException : FileNotFoundException
    {
        private readonly AssemblyNameReference reference;

        public AssemblyResolutionException(AssemblyNameReference reference) : this(reference, null)
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyResolutionException(AssemblyNameReference reference, Exception innerException)
        {
        }

        private AssemblyResolutionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public AssemblyNameReference AssemblyReference =>
            this.reference;
    }
}

