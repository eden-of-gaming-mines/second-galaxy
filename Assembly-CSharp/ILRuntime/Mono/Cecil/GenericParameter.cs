﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class GenericParameter : TypeReference, ICustomAttributeProvider, IMetadataTokenProvider
    {
        internal int position;
        internal GenericParameterType type;
        internal IGenericParameterProvider owner;
        private ushort attributes;
        private Collection<TypeReference> constraints;
        private Collection<CustomAttribute> custom_attributes;
        [CompilerGenerated]
        private static Func<GenericParameter, MetadataReader, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<GenericParameter, MetadataReader, Collection<TypeReference>> <>f__am$cache1;

        public GenericParameter(IGenericParameterProvider owner) : this(string.Empty, owner)
        {
        }

        [MethodImpl(0x8000)]
        public GenericParameter(string name, IGenericParameterProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        internal GenericParameter(int position, GenericParameterType type, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        private static ElementType ConvertGenericParameterType(GenericParameterType type)
        {
        }

        public override TypeDefinition Resolve() => 
            null;

        public GenericParameterAttributes Attributes
        {
            get => 
                ((GenericParameterAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public int Position =>
            this.position;

        public GenericParameterType Type =>
            this.type;

        public IGenericParameterProvider Owner =>
            this.owner;

        public bool HasConstraints
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<TypeReference> Constraints
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override TypeReference DeclaringType
        {
            get => 
                (this.owner as TypeReference);
            set
            {
                throw new InvalidOperationException();
            }
        }

        public MethodReference DeclaringMethod =>
            (this.owner as MethodReference);

        public override ModuleDefinition Module
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string Namespace
        {
            get => 
                string.Empty;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override string FullName =>
            this.Name;

        public override bool IsGenericParameter =>
            true;

        public override bool ContainsGenericParameter =>
            true;

        public override ILRuntime.Mono.Cecil.MetadataType MetadataType =>
            ((ILRuntime.Mono.Cecil.MetadataType) base.etype);

        public bool IsNonVariant
        {
            get => 
                this.attributes.GetMaskedAttributes(3, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCovariant
        {
            get => 
                this.attributes.GetMaskedAttributes(3, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsContravariant
        {
            get => 
                this.attributes.GetMaskedAttributes(3, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasReferenceTypeConstraint
        {
            get => 
                this.attributes.GetAttributes(4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasNotNullableValueTypeConstraint
        {
            get => 
                this.attributes.GetAttributes(8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasDefaultConstructorConstraint
        {
            get => 
                this.attributes.GetAttributes(0x10);
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

