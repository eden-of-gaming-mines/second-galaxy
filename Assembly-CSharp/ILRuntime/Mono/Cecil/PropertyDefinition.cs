﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class PropertyDefinition : PropertyReference, IMemberDefinition, IConstantProvider, ICustomAttributeProvider, IMetadataTokenProvider
    {
        private bool? has_this;
        private ushort attributes;
        private Collection<CustomAttribute> custom_attributes;
        internal MethodDefinition get_method;
        internal MethodDefinition set_method;
        internal Collection<MethodDefinition> other_methods;
        private object constant;
        [CompilerGenerated]
        private static Action<PropertyDefinition, MetadataReader> <>f__am$cache0;

        [MethodImpl(0x8000)]
        public PropertyDefinition(string name, PropertyAttributes attributes, TypeReference propertyType)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethods()
        {
        }

        [MethodImpl(0x8000)]
        private static Collection<ParameterDefinition> MirrorParameters(MethodDefinition method, int bound)
        {
        }

        public override PropertyDefinition Resolve() => 
            this;

        public PropertyAttributes Attributes
        {
            get => 
                ((PropertyAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public bool HasThis
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.has_this = new bool?(value));
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public MethodDefinition GetMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.get_method = value);
        }

        public MethodDefinition SetMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.set_method = value);
        }

        public bool HasOtherMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<MethodDefinition> OtherMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override Collection<ParameterDefinition> Parameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasConstant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public object Constant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.constant = value);
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasDefault
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TypeDefinition DeclaringType
        {
            get => 
                ((TypeDefinition) base.DeclaringType);
            set => 
                (base.DeclaringType = value);
        }

        public override bool IsDefinition =>
            true;

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

