﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum ReadingMode
    {
        Immediate = 1,
        Deferred = 2
    }
}

