﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class EventDefinition : EventReference, IMemberDefinition, ICustomAttributeProvider, IMetadataTokenProvider
    {
        private ushort attributes;
        private Collection<CustomAttribute> custom_attributes;
        internal MethodDefinition add_method;
        internal MethodDefinition invoke_method;
        internal MethodDefinition remove_method;
        internal Collection<MethodDefinition> other_methods;
        [CompilerGenerated]
        private static Action<EventDefinition, MetadataReader> <>f__am$cache0;

        [MethodImpl(0x8000)]
        public EventDefinition(string name, EventAttributes attributes, TypeReference eventType)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethods()
        {
        }

        public override EventDefinition Resolve() => 
            this;

        public EventAttributes Attributes
        {
            get => 
                ((EventAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public MethodDefinition AddMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.add_method = value);
        }

        public MethodDefinition InvokeMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.invoke_method = value);
        }

        public MethodDefinition RemoveMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.remove_method = value);
        }

        public bool HasOtherMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<MethodDefinition> OtherMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TypeDefinition DeclaringType
        {
            get => 
                ((TypeDefinition) base.DeclaringType);
            set => 
                (base.DeclaringType = value);
        }

        public override bool IsDefinition =>
            true;
    }
}

