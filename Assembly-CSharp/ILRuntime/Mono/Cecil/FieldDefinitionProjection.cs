﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class FieldDefinitionProjection
    {
        public readonly FieldAttributes Attributes;
        public readonly FieldDefinitionTreatment Treatment;

        [MethodImpl(0x8000)]
        public FieldDefinitionProjection(FieldDefinition field, FieldDefinitionTreatment treatment)
        {
        }
    }
}

