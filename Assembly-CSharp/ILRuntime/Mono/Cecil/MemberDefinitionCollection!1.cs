﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class MemberDefinitionCollection<T> : Collection<T> where T: IMemberDefinition
    {
        private TypeDefinition container;

        internal MemberDefinitionCollection(TypeDefinition container)
        {
            this.container = container;
        }

        internal MemberDefinitionCollection(TypeDefinition container, int capacity) : base(capacity)
        {
            this.container = container;
        }

        [MethodImpl(0x8000)]
        private void Attach(T element)
        {
        }

        private static void Detach(T element)
        {
            element.DeclaringType = null;
        }

        protected override void OnAdd(T item, int index)
        {
            this.Attach(item);
        }

        [MethodImpl(0x8000)]
        protected sealed override void OnClear()
        {
        }

        protected sealed override void OnInsert(T item, int index)
        {
            this.Attach(item);
        }

        protected sealed override void OnRemove(T item, int index)
        {
            MemberDefinitionCollection<T>.Detach(item);
        }

        protected sealed override void OnSet(T item, int index)
        {
            this.Attach(item);
        }
    }
}

