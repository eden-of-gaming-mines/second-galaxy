﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct MetadataToken : IEquatable<MetadataToken>
    {
        private readonly uint token;
        public static readonly MetadataToken Zero;
        public MetadataToken(uint token)
        {
            this.token = token;
        }

        public MetadataToken(ILRuntime.Mono.Cecil.TokenType type) : this(type, 0)
        {
        }

        public MetadataToken(ILRuntime.Mono.Cecil.TokenType type, uint rid)
        {
            this.token = ((uint) type) | rid;
        }

        public MetadataToken(ILRuntime.Mono.Cecil.TokenType type, int rid)
        {
            this.token = (uint) (type | ((ILRuntime.Mono.Cecil.TokenType) rid));
        }

        public uint RID =>
            (this.token & 0xffffff);
        public ILRuntime.Mono.Cecil.TokenType TokenType =>
            (((ILRuntime.Mono.Cecil.TokenType) this.token) & ((ILRuntime.Mono.Cecil.TokenType) (-16777216)));
        public int ToInt32() => 
            ((int) this.token);

        public uint ToUInt32() => 
            this.token;

        public override int GetHashCode() => 
            ((int) this.token);

        [MethodImpl(0x8000)]
        public bool Equals(MetadataToken other)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator ==(MetadataToken one, MetadataToken other)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator !=(MetadataToken one, MetadataToken other)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        static MetadataToken()
        {
            Zero = new MetadataToken(0);
        }
    }
}

