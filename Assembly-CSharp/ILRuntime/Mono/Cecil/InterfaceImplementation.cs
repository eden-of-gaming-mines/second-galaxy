﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class InterfaceImplementation : ICustomAttributeProvider, IMetadataTokenProvider
    {
        internal TypeDefinition type;
        internal ILRuntime.Mono.Cecil.MetadataToken token;
        private TypeReference interface_type;
        private Collection<CustomAttribute> custom_attributes;

        [MethodImpl(0x8000)]
        public InterfaceImplementation(TypeReference interfaceType)
        {
        }

        [MethodImpl(0x8000)]
        internal InterfaceImplementation(TypeReference interfaceType, ILRuntime.Mono.Cecil.MetadataToken token)
        {
        }

        public TypeReference InterfaceType
        {
            get => 
                this.interface_type;
            set => 
                (this.interface_type = value);
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }
    }
}

