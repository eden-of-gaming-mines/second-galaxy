﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Cecil.PE;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class SignatureReader : ByteBuffer
    {
        private readonly MetadataReader reader;
        internal readonly uint start;
        internal readonly uint sig_length;

        [MethodImpl(0x8000)]
        public SignatureReader(uint blob, MetadataReader reader)
        {
        }

        [MethodImpl(0x8000)]
        public bool CanReadMore()
        {
        }

        [MethodImpl(0x8000)]
        private static void CheckGenericContext(IGenericParameterProvider owner, int index)
        {
        }

        [MethodImpl(0x8000)]
        private static Collection<CustomAttributeNamedArgument> GetCustomAttributeNamedArgumentCollection(ref Collection<CustomAttributeNamedArgument> collection)
        {
        }

        [MethodImpl(0x8000)]
        private GenericParameter GetGenericParameter(GenericParameterType type, uint var)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference GetPrimitiveType(ElementType etype)
        {
        }

        private TypeReference GetTypeDefOrRef(MetadataToken token) => 
            this.reader.GetTypeDefOrRef(token);

        [MethodImpl(0x8000)]
        private GenericParameter GetUnboundGenericParameter(GenericParameterType type, int index)
        {
        }

        [MethodImpl(0x8000)]
        private ArrayType ReadArrayTypeSignature()
        {
        }

        public object ReadConstantSignature(ElementType type) => 
            this.ReadPrimitiveValue(type);

        [MethodImpl(0x8000)]
        public void ReadCustomAttributeConstructorArguments(CustomAttribute attribute, Collection<ParameterDefinition> parameters)
        {
        }

        [MethodImpl(0x8000)]
        private CustomAttributeArgument ReadCustomAttributeElement(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private object ReadCustomAttributeElementValue(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private object ReadCustomAttributeEnum(TypeReference enum_type)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference ReadCustomAttributeFieldOrPropType()
        {
        }

        [MethodImpl(0x8000)]
        private CustomAttributeArgument ReadCustomAttributeFixedArgument(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private CustomAttributeArgument ReadCustomAttributeFixedArrayArgument(ArrayType type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadCustomAttributeNamedArgument(ref Collection<CustomAttributeNamedArgument> fields, ref Collection<CustomAttributeNamedArgument> properties)
        {
        }

        [MethodImpl(0x8000)]
        public void ReadCustomAttributeNamedArguments(ushort count, ref Collection<CustomAttributeNamedArgument> fields, ref Collection<CustomAttributeNamedArgument> properties)
        {
        }

        [MethodImpl(0x8000)]
        public string ReadDocumentName()
        {
        }

        [MethodImpl(0x8000)]
        public void ReadGenericInstanceSignature(IGenericParameterProvider provider, IGenericInstance instance)
        {
        }

        [MethodImpl(0x8000)]
        public MarshalInfo ReadMarshalInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ReadMethodSignature(IMethodSignature method)
        {
        }

        private NativeType ReadNativeType() => 
            ((NativeType) base.ReadByte());

        [MethodImpl(0x8000)]
        private object ReadPrimitiveValue(ElementType type)
        {
        }

        [MethodImpl(0x8000)]
        public SecurityAttribute ReadSecurityAttribute()
        {
        }

        [MethodImpl(0x8000)]
        public Collection<SequencePoint> ReadSequencePoints(Document document)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference ReadTypeReference()
        {
        }

        public TypeReference ReadTypeSignature() => 
            this.ReadTypeSignature((ElementType) base.ReadByte());

        [MethodImpl(0x8000)]
        private TypeReference ReadTypeSignature(ElementType etype)
        {
        }

        public TypeReference ReadTypeToken() => 
            this.GetTypeDefOrRef(this.ReadTypeTokenSignature());

        private MetadataToken ReadTypeTokenSignature() => 
            CodedIndex.TypeDefOrRef.GetMetadataToken(base.ReadCompressedUInt32());

        [MethodImpl(0x8000)]
        private string ReadUTF8String()
        {
        }

        private VariantType ReadVariantType() => 
            ((VariantType) base.ReadByte());

        private ILRuntime.Mono.Cecil.TypeSystem TypeSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

