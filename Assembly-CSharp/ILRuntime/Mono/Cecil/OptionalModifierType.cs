﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class OptionalModifierType : TypeSpecification, IModifierType
    {
        private TypeReference modifier_type;

        [MethodImpl(0x8000)]
        public OptionalModifierType(TypeReference modifierType, TypeReference type)
        {
        }

        public TypeReference ModifierType
        {
            get => 
                this.modifier_type;
            set => 
                (this.modifier_type = value);
        }

        public override string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private string Suffix
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsValueType
        {
            get => 
                false;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override bool IsOptionalModifier =>
            true;

        public override bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

