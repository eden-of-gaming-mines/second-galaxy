﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class ParameterReference : IMetadataTokenProvider
    {
        private string name;
        internal int index;
        protected TypeReference parameter_type;
        internal ILRuntime.Mono.Cecil.MetadataToken token;

        [MethodImpl(0x8000)]
        internal ParameterReference(string name, TypeReference parameterType)
        {
        }

        public abstract ParameterDefinition Resolve();
        public override string ToString() => 
            this.name;

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public int Index =>
            this.index;

        public TypeReference ParameterType
        {
            get => 
                this.parameter_type;
            set => 
                (this.parameter_type = value);
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }
    }
}

