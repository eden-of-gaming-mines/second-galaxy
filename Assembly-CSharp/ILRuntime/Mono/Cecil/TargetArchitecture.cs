﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum TargetArchitecture
    {
        I386 = 0x14c,
        AMD64 = 0x8664,
        IA64 = 0x200,
        ARM = 0x1c0,
        ARMv7 = 0x1c4,
        ARM64 = 0xaa64
    }
}

