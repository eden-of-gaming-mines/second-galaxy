﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Cecil.PE;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;

    internal sealed class MetadataReader : ByteBuffer
    {
        internal readonly Image image;
        internal readonly ModuleDefinition module;
        internal readonly MetadataSystem metadata;
        internal CodeReader code;
        internal IGenericContext context;
        private readonly MetadataReader metadata_reader;
        [CompilerGenerated]
        private static Func<uint, BinaryStreamReader, byte[]> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<int, BinaryStreamReader, byte[]> <>f__am$cache1;

        [MethodImpl(0x8000)]
        public MetadataReader(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public MetadataReader(Image image, ModuleDefinition module, MetadataReader metadata_reader)
        {
        }

        [MethodImpl(0x8000)]
        private void AddGenericConstraintMapping(uint generic_parameter, MetadataToken constraint)
        {
        }

        [MethodImpl(0x8000)]
        private void AddInterfaceMapping(uint type, Row<uint, MetadataToken> @interface)
        {
        }

        [MethodImpl(0x8000)]
        private static Collection<TValue> AddMapping<TKey, TValue>(Dictionary<TKey, Collection<TValue>> cache, TKey key, TValue value)
        {
        }

        [MethodImpl(0x8000)]
        private void AddNestedMapping(uint declaring, uint nested)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOverrideMapping(uint method_rid, MetadataToken @override)
        {
        }

        [MethodImpl(0x8000)]
        private static void AddRange(Dictionary<MetadataToken, Range[]> ranges, MetadataToken owner, Range range)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AddScope(Collection<ScopeDebugInformation> scopes, ScopeDebugInformation scope)
        {
        }

        [MethodImpl(0x8000)]
        private void CompleteTypes()
        {
        }

        [MethodImpl(0x8000)]
        private void GetBlobView(uint signature, out byte[] blob, out int index, out int count)
        {
        }

        private int GetCodedIndexSize(CodedIndex index) => 
            this.image.GetCodedIndexSize(index);

        [MethodImpl(0x8000)]
        public IEnumerable<CustomAttribute> GetCustomAttributes()
        {
        }

        [MethodImpl(0x8000)]
        public Collection<CustomDebugInformation> GetCustomDebugInformation(ICustomDebugInformationProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public Document GetDocument(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private static EventDefinition GetEvent(TypeDefinition type, MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        private IMetadataScope GetExportedTypeScope(MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        public FieldDefinition GetFieldDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private byte[] GetFieldInitializeValue(int size, uint rva)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetFieldTypeSize(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetManagedResource(uint offset)
        {
        }

        [MethodImpl(0x8000)]
        private static TMember GetMember<TMember>(Collection<TMember> members, MetadataToken token) where TMember: IMemberDefinition
        {
        }

        [MethodImpl(0x8000)]
        private MemberReference GetMemberReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<MemberReference> GetMemberReferences()
        {
        }

        [MethodImpl(0x8000)]
        public MethodDefinition GetMethodDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private MethodSpecification GetMethodSpecification(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private string GetModuleFileName(string name)
        {
        }

        [MethodImpl(0x8000)]
        private ModuleReference GetModuleReferenceFromFile(MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        private TypeDefinition GetNestedTypeDeclaringType(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private static PropertyDefinition GetProperty(TypeDefinition type, MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetTypeDefinition(uint rid)
        {
        }

        public TypeReference GetTypeDefOrRef(MetadataToken token) => 
            ((TypeReference) this.LookupToken(token));

        [MethodImpl(0x8000)]
        private TypeReference GetTypeReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference GetTypeReference(string scope, string full_name)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<TypeReference> GetTypeReferences()
        {
        }

        [MethodImpl(0x8000)]
        private IMetadataScope GetTypeReferenceScope(MetadataToken scope)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference GetTypeSpecification(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasCustomAttributes(ICustomAttributeProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEvents(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasFileResource()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasGenericConstraints(GenericParameter generic_parameter)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasGenericParameters(IGenericParameterProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasInterfaces(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasMarshalInfo(IMarshalInfoProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasNestedTypes(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasOverrides(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperties(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSecurityDeclarations(ISecurityDeclarationProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeAssemblyReferences()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeConstants()
        {
        }

        [MethodImpl(0x8000)]
        internal void InitializeCustomAttributes()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeCustomDebugInformations()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeDocuments()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFieldLayouts()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFieldRVAs()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFields()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeGenericConstraints()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeGenericParameters()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeImportScopes()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeInterfaces()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeLocalScopes()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMarshalInfos()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMemberReferences()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethods()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethodSemantics()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeModuleReferences()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeNestedTypes()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeOverrides()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializePInvokes()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeProperties()
        {
        }

        [MethodImpl(0x8000)]
        private Dictionary<MetadataToken, Range[]> InitializeRanges(Table table, Func<MetadataToken> get_next)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeSecurityDeclarations()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeStateMachineMethods()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeTypeDefinitions()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeTypeLayouts()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeTypeReferences()
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsDeleted(IMemberDefinition member)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsNested(TypeAttributes attributes)
        {
        }

        [MethodImpl(0x8000)]
        private FieldDefinition LookupField(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private MethodDefinition LookupMethod(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public IMetadataTokenProvider LookupToken(MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        private int MoveTo(Table table)
        {
        }

        [MethodImpl(0x8000)]
        private bool MoveTo(Table table, uint row)
        {
        }

        [MethodImpl(0x8000)]
        public ModuleDefinition Populate(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        private void PopulateNameAndCulture(AssemblyNameReference name)
        {
        }

        [MethodImpl(0x8000)]
        private void PopulateVersionAndFlags(AssemblyNameReference name)
        {
        }

        [MethodImpl(0x8000)]
        private static int RangesSize(Range[] ranges)
        {
        }

        public void ReadAllSemantics(MethodDefinition method)
        {
            this.ReadAllSemantics(method.DeclaringType);
        }

        [MethodImpl(0x8000)]
        private void ReadAllSemantics(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyNameDefinition ReadAssemblyNameDefinition()
        {
        }

        [MethodImpl(0x8000)]
        public Collection<AssemblyNameReference> ReadAssemblyReferences()
        {
        }

        [MethodImpl(0x8000)]
        private byte[] ReadBlob()
        {
        }

        [MethodImpl(0x8000)]
        private byte[] ReadBlob(uint signature)
        {
        }

        [MethodImpl(0x8000)]
        private uint ReadBlobIndex()
        {
        }

        [MethodImpl(0x8000)]
        private uint ReadByIndexSize(int size)
        {
        }

        [MethodImpl(0x8000)]
        public CallSite ReadCallSite(MetadataToken token)
        {
        }

        public int ReadCodeSize(MethodDefinition method) => 
            this.code.ReadCodeSize(method);

        [MethodImpl(0x8000)]
        public object ReadConstant(IConstantProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        private object ReadConstantPrimitive(ElementType type, uint signature)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference ReadConstantSignature(MetadataToken token)
        {
        }

        [MethodImpl(0x8000)]
        private string ReadConstantString(uint signature)
        {
        }

        [MethodImpl(0x8000)]
        private object ReadConstantValue(ElementType etype, uint signature)
        {
        }

        public byte[] ReadCustomAttributeBlob(uint signature) => 
            this.ReadBlob(signature);

        [MethodImpl(0x8000)]
        private void ReadCustomAttributeRange(Range range, Collection<CustomAttribute> custom_attributes)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<CustomAttribute> ReadCustomAttributes(ICustomAttributeProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public void ReadCustomAttributeSignature(CustomAttribute attribute)
        {
        }

        [MethodImpl(0x8000)]
        public MethodDefinition ReadEntryPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadEvent(uint event_rid, Collection<EventDefinition> events)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<EventDefinition> ReadEvents(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<ExportedType> ReadExportedTypes()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadField(uint field_rid, Collection<FieldDefinition> fields)
        {
        }

        [MethodImpl(0x8000)]
        public int ReadFieldLayout(FieldDefinition field)
        {
        }

        [MethodImpl(0x8000)]
        public int ReadFieldRVA(FieldDefinition field)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<FieldDefinition> ReadFields(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference ReadFieldType(uint signature)
        {
        }

        [MethodImpl(0x8000)]
        private Row<FileAttributes, string, uint> ReadFileRecord(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<TypeReference> ReadGenericConstraints(GenericParameter generic_parameter)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<GenericParameter> ReadGenericParameters(IGenericParameterProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadGenericParametersRange(Range range, IGenericParameterProvider provider, GenericParameterCollection generic_parameters)
        {
        }

        [MethodImpl(0x8000)]
        private Guid ReadGuid()
        {
        }

        [MethodImpl(0x8000)]
        private ImportTarget ReadImportTarget(SignatureReader signature)
        {
        }

        [MethodImpl(0x8000)]
        public InterfaceImplementationCollection ReadInterfaces(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private Range ReadListRange(uint current_index, Table current, Table target)
        {
        }

        [MethodImpl(0x8000)]
        private ConstantDebugInformation ReadLocalConstant(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private ScopeDebugInformation ReadLocalScope(Row<uint, Range, Range, uint, uint, uint> record)
        {
        }

        [MethodImpl(0x8000)]
        private VariableDebugInformation ReadLocalVariable(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public MarshalInfo ReadMarshalInfo(IMarshalInfoProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        private MemberReference ReadMemberReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private MemberReference ReadMemberReferenceSignature(uint signature, TypeReference declaring_type)
        {
        }

        [MethodImpl(0x8000)]
        private MetadataToken ReadMetadataToken(CodedIndex index)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMethod(uint method_rid, Collection<MethodDefinition> methods)
        {
        }

        public MethodBody ReadMethodBody(MethodDefinition method) => 
            this.code.ReadMethodBody(method);

        [MethodImpl(0x8000)]
        private MemberReference ReadMethodMemberReference(MetadataToken token, string name, uint signature)
        {
        }

        public void ReadMethods(EventDefinition @event)
        {
            this.ReadAllSemantics(@event.DeclaringType);
        }

        public void ReadMethods(PropertyDefinition property)
        {
            this.ReadAllSemantics(property.DeclaringType);
        }

        [MethodImpl(0x8000)]
        public Collection<MethodDefinition> ReadMethods(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private MethodSemanticsAttributes ReadMethodSemantics(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMethodSignature(uint signature, IMethodSignature method)
        {
        }

        [MethodImpl(0x8000)]
        private MethodSpecification ReadMethodSpecSignature(uint signature, MethodReference method)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<ModuleReference> ReadModuleReferences()
        {
        }

        [MethodImpl(0x8000)]
        public Collection<ModuleDefinition> ReadModules()
        {
        }

        [MethodImpl(0x8000)]
        public Collection<TypeDefinition> ReadNestedTypes(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<MethodReference> ReadOverrides(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadParameter(uint param_rid, MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadParameterPointers(MethodDefinition method, Range range)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadParameters(MethodDefinition method, Range param_range)
        {
        }

        [MethodImpl(0x8000)]
        public PInvokeInfo ReadPInvokeInfo(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadPointers<TMember>(Table ptr, Table table, Range range, Collection<TMember> members, Action<uint, Collection<TMember>> reader) where TMember: IMemberDefinition
        {
        }

        [MethodImpl(0x8000)]
        public Collection<PropertyDefinition> ReadProperties(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadProperty(uint property_rid, Collection<PropertyDefinition> properties)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<Resource> ReadResources()
        {
        }

        [MethodImpl(0x8000)]
        public ScopeDebugInformation ReadScope(MethodDefinition method)
        {
        }

        public byte[] ReadSecurityDeclarationBlob(uint signature) => 
            this.ReadBlob(signature);

        [MethodImpl(0x8000)]
        private void ReadSecurityDeclarationRange(Range range, Collection<SecurityDeclaration> security_declarations)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<SecurityDeclaration> ReadSecurityDeclarations(ISecurityDeclarationProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public void ReadSecurityDeclarationSignature(SecurityDeclaration declaration)
        {
        }

        [MethodImpl(0x8000)]
        public Collection<SequencePoint> ReadSequencePoints(MethodDefinition method)
        {
        }

        private SignatureReader ReadSignature(uint signature) => 
            new SignatureReader(signature, this);

        [MethodImpl(0x8000)]
        public MethodDefinition ReadStateMachineKickoffMethod(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private string ReadString()
        {
        }

        [MethodImpl(0x8000)]
        private string ReadStringBlob(uint signature, Encoding encoding)
        {
        }

        [MethodImpl(0x8000)]
        private uint ReadStringIndex()
        {
        }

        [MethodImpl(0x8000)]
        private uint ReadTableIndex(Table table)
        {
        }

        [MethodImpl(0x8000)]
        private TypeDefinition ReadType(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private TypeDefinition ReadTypeDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public Row<short, int> ReadTypeLayout(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private MemberReference ReadTypeMemberReference(MetadataToken type, string name, uint signature)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference ReadTypeReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinitionCollection ReadTypes()
        {
        }

        private string ReadUnicodeStringBlob(uint signature) => 
            this.ReadStringBlob(signature, Encoding.Unicode);

        public string ReadUTF8StringBlob(uint signature) => 
            this.ReadStringBlob(signature, Encoding.UTF8);

        [MethodImpl(0x8000)]
        public VariableDefinitionCollection ReadVariables(MetadataToken local_var_token)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadXmlSecurityDeclaration(uint signature, SecurityDeclaration declaration)
        {
        }
    }
}

