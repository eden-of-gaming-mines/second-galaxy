﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate AssemblyDefinition AssemblyResolveEventHandler(object sender, AssemblyNameReference reference);
}

