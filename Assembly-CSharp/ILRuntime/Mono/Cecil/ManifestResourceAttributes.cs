﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    [Flags]
    public enum ManifestResourceAttributes : uint
    {
        VisibilityMask = 7,
        Public = 1,
        Private = 2
    }
}

