﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface IGenericParameterProvider : IMetadataTokenProvider
    {
        bool HasGenericParameters { get; }

        bool IsDefinition { get; }

        ModuleDefinition Module { get; }

        Collection<GenericParameter> GenericParameters { get; }

        ILRuntime.Mono.Cecil.GenericParameterType GenericParameterType { get; }
    }
}

