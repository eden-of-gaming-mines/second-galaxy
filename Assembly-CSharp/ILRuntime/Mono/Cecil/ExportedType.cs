﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ExportedType : IMetadataTokenProvider
    {
        private string @namespace;
        private string name;
        private uint attributes;
        private IMetadataScope scope;
        private ModuleDefinition module;
        private int identifier;
        private ExportedType declaring_type;
        internal ILRuntime.Mono.Cecil.MetadataToken token;

        [MethodImpl(0x8000)]
        public ExportedType(string @namespace, string name, ModuleDefinition module, IMetadataScope scope)
        {
        }

        [MethodImpl(0x8000)]
        internal TypeReference CreateReference()
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition Resolve()
        {
        }

        public override string ToString() => 
            this.FullName;

        public string Namespace
        {
            get => 
                this.@namespace;
            set => 
                (this.@namespace = value);
        }

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public TypeAttributes Attributes
        {
            get => 
                ((TypeAttributes) this.attributes);
            set => 
                (this.attributes = (uint) value);
        }

        public IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ExportedType DeclaringType
        {
            get => 
                this.declaring_type;
            set => 
                (this.declaring_type = value);
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }

        public int Identifier
        {
            get => 
                this.identifier;
            set => 
                (this.identifier = value);
        }

        public bool IsNotPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedPrivate
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 3);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamily
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 5);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamilyAndAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 6);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamilyOrAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 7);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAutoLayout
        {
            get => 
                this.attributes.GetMaskedAttributes(0x18, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSequentialLayout
        {
            get => 
                this.attributes.GetMaskedAttributes(0x18, 8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsExplicitLayout
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsClass
        {
            get => 
                this.attributes.GetMaskedAttributes(0x20, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAbstract
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSealed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsImport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSerializable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAnsiClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsUnicodeClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAutoClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsBeforeFieldInit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasSecurity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsForwarder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

