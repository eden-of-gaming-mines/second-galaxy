﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class PinnedType : TypeSpecification
    {
        [MethodImpl(0x8000)]
        public PinnedType(TypeReference type)
        {
        }

        public override bool IsValueType
        {
            get => 
                false;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override bool IsPinned =>
            true;
    }
}

