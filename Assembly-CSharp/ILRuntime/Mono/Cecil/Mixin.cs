﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;

    internal static class Mixin
    {
        public const int TableCount = 0x3a;
        public const int CodedIndexCount = 14;
        public static Version ZeroVersion;
        public const int NotResolvedMarker = -2;
        public const int NoDataMarker = -1;
        internal static object NoValue;
        internal static object NotResolved;
        public const string mscorlib = "mscorlib";
        public const string system_runtime = "System.Runtime";
        public const string system_private_corelib = "System.Private.CoreLib";
        public const string netstandard = "netstandard";
        [CompilerGenerated]
        private static Func<IConstantProvider, MetadataReader, object> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<ICustomAttributeProvider, MetadataReader, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<ICustomAttributeProvider, MetadataReader, Collection<CustomAttribute>> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<IGenericParameterProvider, MetadataReader, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<IGenericParameterProvider, MetadataReader, Collection<GenericParameter>> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<IMarshalInfoProvider, MetadataReader, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<IMarshalInfoProvider, MetadataReader, MarshalInfo> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<ISecurityDeclarationProvider, MetadataReader, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<ISecurityDeclarationProvider, MetadataReader, Collection<SecurityDeclaration>> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, bool> <>f__am$cache9;

        [MethodImpl(0x8000)]
        public static T[] Add<T>(this T[] self, T item)
        {
        }

        [MethodImpl(0x8000)]
        public static ImageDebugHeader AddDeterministicEntry(this ImageDebugHeader header)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeDefinition CheckedResolve(this TypeReference self)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckField(object field)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckFileName(string fileName)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckFullName(string fullName)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckMethod(object method)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckModule(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckName(object name)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckParameters(object parameters)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckReadSeek(Stream stream)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckStream(object stream)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckType(object type)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckType(object type, Argument argument)
        {
        }

        [MethodImpl(0x8000)]
        public static Version CheckVersion(Version version)
        {
        }

        [MethodImpl(0x8000)]
        public static void CheckWriteSeek(Stream stream)
        {
        }

        [MethodImpl(0x8000)]
        public static bool ContainsGenericParameter(this IGenericInstance self)
        {
        }

        [MethodImpl(0x8000)]
        public static void CopyTo(this Stream self, Stream target)
        {
        }

        [MethodImpl(0x8000)]
        private static bool Equals(byte[] a, byte[] b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool Equals(AssemblyNameReference a, AssemblyNameReference b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool Equals<T>(T a, T b) where T: class, IEquatable<T>
        {
        }

        [MethodImpl(0x8000)]
        public static void GenericInstanceFullName(this IGenericInstance self, StringBuilder builder)
        {
        }

        public static bool GetAttributes(this ushort self, ushort attributes) => 
            ((self & attributes) != 0);

        public static bool GetAttributes(this uint self, uint attributes) => 
            ((self & attributes) != 0);

        public static ImageDebugHeaderEntry GetCodeViewEntry(this ImageDebugHeader header) => 
            header.GetEntry(ImageDebugType.CodeView);

        [MethodImpl(0x8000)]
        public static Collection<CustomAttribute> GetCustomAttributes(this ICustomAttributeProvider self, ref Collection<CustomAttribute> variable, ModuleDefinition module)
        {
        }

        public static ImageDebugHeaderEntry GetDeterministicEntry(this ImageDebugHeader header) => 
            header.GetEntry(ImageDebugType.Deterministic);

        public static ImageDebugHeaderEntry GetEmbeddedPortablePdbEntry(this ImageDebugHeader header) => 
            header.GetEntry(ImageDebugType.EmbeddedPortablePdb);

        [MethodImpl(0x8000)]
        private static ImageDebugHeaderEntry GetEntry(this ImageDebugHeader header, ImageDebugType type)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeReference GetEnumUnderlyingType(this TypeDefinition self)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFileName(this Stream self)
        {
        }

        [MethodImpl(0x8000)]
        public static Collection<GenericParameter> GetGenericParameters(this IGenericParameterProvider self, ref Collection<GenericParameter> collection, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetHasCustomAttributes(this ICustomAttributeProvider self, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetHasGenericParameters(this IGenericParameterProvider self, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetHasMarshalInfo(this IMarshalInfoProvider self, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetHasSecurityDeclarations(this ISecurityDeclarationProvider self, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static MarshalInfo GetMarshalInfo(this IMarshalInfoProvider self, ref MarshalInfo variable, ModuleDefinition module)
        {
        }

        public static bool GetMaskedAttributes(this ushort self, ushort mask, uint attributes) => 
            ((self & mask) == attributes);

        public static bool GetMaskedAttributes(this uint self, uint mask, uint attributes) => 
            ((self & mask) == attributes);

        public static string GetMdbFileName(string assemblyFileName) => 
            (assemblyFileName + ".mdb");

        [MethodImpl(0x8000)]
        public static MetadataToken GetMetadataToken(this CodedIndex self, uint data)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeDefinition GetNestedType(this TypeDefinition self, string fullname)
        {
        }

        [MethodImpl(0x8000)]
        public static ParameterDefinition GetParameter(this MethodBody self, int index)
        {
        }

        public static string GetPdbFileName(string assemblyFileName) => 
            Path.ChangeExtension(assemblyFileName, ".pdb");

        [MethodImpl(0x8000)]
        public static Collection<SecurityDeclaration> GetSecurityDeclarations(this ISecurityDeclarationProvider self, ref Collection<SecurityDeclaration> variable, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetSemantics(this MethodDefinition self, MethodSemanticsAttributes semantics)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSentinelPosition(this IMethodSignature self)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSize(this CodedIndex self, Func<Table, int> counter)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetTimestamp()
        {
        }

        [MethodImpl(0x8000)]
        public static VariableDefinition GetVariable(this MethodBody self, int index)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasImage(this ModuleDefinition self)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasImplicitThis(this IMethodSignature self)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsCoreLibrary(AssemblyNameReference reference)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsCoreLibrary(this ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsNullOrEmpty<T>(this Collection<T> self)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsNullOrEmpty<T>(this T[] self)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPortablePdb(Stream stream)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPortablePdb(string fileName)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPrimitive(this ElementType self)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsTypeOf(this TypeReference self, string @namespace, string name)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsTypeSpecification(this TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsVarArg(this IMethodSignature self)
        {
        }

        public static bool IsWindowsMetadata(this ModuleDefinition module) => 
            (module.MetadataKind != MetadataKind.Ecma335);

        [MethodImpl(0x8000)]
        public static void KnownValueType(this TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public static void MethodSignatureFullName(this IMethodSignature self, StringBuilder builder)
        {
        }

        [MethodImpl(0x8000)]
        public static TargetRuntime ParseRuntime(this string self)
        {
        }

        public static void Read(object o)
        {
        }

        [MethodImpl(0x8000)]
        public static byte[] ReadAll(this Stream self)
        {
        }

        [MethodImpl(0x8000)]
        public static uint ReadCompressedUInt32(this byte[] data, ref int position)
        {
        }

        public static T[] Resize<T>(this T[] self, int length)
        {
            Array.Resize<T>(ref self, length);
            return self;
        }

        [MethodImpl(0x8000)]
        public static void ResolveConstant(this IConstantProvider self, ref object constant, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public static string RuntimeVersionString(this TargetRuntime runtime)
        {
        }

        [MethodImpl(0x8000)]
        public static ushort SetAttributes(this ushort self, ushort attributes, bool value)
        {
        }

        public static uint SetAttributes(this uint self, uint attributes, bool value) => 
            (!value ? (self & ~attributes) : (self | attributes));

        [MethodImpl(0x8000)]
        public static ushort SetMaskedAttributes(this ushort self, ushort mask, uint attributes, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public static uint SetMaskedAttributes(this uint self, uint mask, uint attributes, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetSemantics(this MethodDefinition self, MethodSemanticsAttributes semantics, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetAssemblyNameReference(this ModuleDefinition module, AssemblyNameReference name_reference, out AssemblyNameReference assembly_reference)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetCoreLibraryReference(this ModuleDefinition module, out AssemblyNameReference reference)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetUniqueDocument(this MethodDebugInformation info, out Document document)
        {
        }

        [MethodImpl(0x8000)]
        public static string TypeFullName(this TypeReference self)
        {
        }

        public enum Argument
        {
            name,
            fileName,
            fullName,
            stream,
            type,
            method,
            field,
            parameters,
            module,
            modifierType,
            eventType,
            fieldType,
            declaringType,
            returnType,
            propertyType,
            interfaceType
        }
    }
}

