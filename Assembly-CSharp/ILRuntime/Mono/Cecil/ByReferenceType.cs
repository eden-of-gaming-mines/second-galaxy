﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ByReferenceType : TypeSpecification
    {
        [MethodImpl(0x8000)]
        public ByReferenceType(TypeReference type)
        {
        }

        public override string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsValueType
        {
            get => 
                false;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override bool IsByReference =>
            true;
    }
}

