﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public interface IAssemblyResolver : IDisposable
    {
        AssemblyDefinition Resolve(AssemblyNameReference name);
        AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters);
    }
}

