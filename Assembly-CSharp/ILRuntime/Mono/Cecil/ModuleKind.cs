﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum ModuleKind
    {
        Dll,
        Console,
        Windows,
        NetModule
    }
}

