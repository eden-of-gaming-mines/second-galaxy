﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ImportDebugInformation : DebugInformation
    {
        internal ImportDebugInformation parent;
        internal Collection<ImportTarget> targets;

        public bool HasTargets =>
            !this.targets.IsNullOrEmpty<ImportTarget>();

        public Collection<ImportTarget> Targets
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ImportDebugInformation Parent
        {
            get => 
                this.parent;
            set => 
                (this.parent = value);
        }
    }
}

