﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class StateMachineScope
    {
        internal InstructionOffset start;
        internal InstructionOffset end;

        [MethodImpl(0x8000)]
        public StateMachineScope(Instruction start, Instruction end)
        {
        }

        [MethodImpl(0x8000)]
        internal StateMachineScope(int start, int end)
        {
        }

        public InstructionOffset Start
        {
            get => 
                this.start;
            set => 
                (this.start = value);
        }

        public InstructionOffset End
        {
            get => 
                this.end;
            set => 
                (this.end = value);
        }
    }
}

