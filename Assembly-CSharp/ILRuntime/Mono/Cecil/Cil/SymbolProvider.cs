﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal static class SymbolProvider
    {
        [MethodImpl(0x8000)]
        public static ISymbolReaderProvider GetReaderProvider(SymbolKind kind)
        {
        }

        [MethodImpl(0x8000)]
        private static AssemblyName GetSymbolAssemblyName(SymbolKind kind)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetSymbolNamespace(SymbolKind kind)
        {
        }

        [MethodImpl(0x8000)]
        private static Type GetSymbolType(SymbolKind kind, string fullname)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetSymbolTypeName(SymbolKind kind, string name)
        {
        }
    }
}

