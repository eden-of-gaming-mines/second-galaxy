﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class MethodDebugInformation : DebugInformation
    {
        internal MethodDefinition method;
        internal Collection<SequencePoint> sequence_points;
        internal ScopeDebugInformation scope;
        internal MethodDefinition kickoff_method;
        internal int code_size;
        internal MetadataToken local_var_token;

        [MethodImpl(0x8000)]
        internal MethodDebugInformation(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<ScopeDebugInformation> GetScopes()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private static IEnumerable<ScopeDebugInformation> GetScopes(IList<ScopeDebugInformation> scopes)
        {
        }

        [MethodImpl(0x8000)]
        public SequencePoint GetSequencePoint(Instruction instruction)
        {
        }

        [MethodImpl(0x8000)]
        public IDictionary<Instruction, SequencePoint> GetSequencePointMapping()
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetName(VariableDefinition variable, out string name)
        {
        }

        public MethodDefinition Method =>
            this.method;

        public bool HasSequencePoints =>
            !this.sequence_points.IsNullOrEmpty<SequencePoint>();

        public Collection<SequencePoint> SequencePoints
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ScopeDebugInformation Scope
        {
            get => 
                this.scope;
            set => 
                (this.scope = value);
        }

        public MethodDefinition StateMachineKickOffMethod
        {
            get => 
                this.kickoff_method;
            set => 
                (this.kickoff_method = value);
        }

        [CompilerGenerated]
        private sealed class <GetScopes>c__Iterator0 : IEnumerable, IEnumerable<ScopeDebugInformation>, IEnumerator, IDisposable, IEnumerator<ScopeDebugInformation>
        {
            internal int <i>__1;
            internal IList<ScopeDebugInformation> scopes;
            internal ScopeDebugInformation <scope>__2;
            internal IEnumerator<ScopeDebugInformation> $locvar0;
            internal ScopeDebugInformation <sub_scope>__3;
            internal ScopeDebugInformation $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<ScopeDebugInformation> IEnumerable<ScopeDebugInformation>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<ILRuntime.Mono.Cecil.Cil.ScopeDebugInformation>.GetEnumerator();

            ScopeDebugInformation IEnumerator<ScopeDebugInformation>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

