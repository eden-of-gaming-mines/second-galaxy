﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct InstructionOffset
    {
        private readonly Instruction instruction;
        private readonly int? offset;
        [MethodImpl(0x8000)]
        public InstructionOffset(Instruction instruction)
        {
        }

        [MethodImpl(0x8000)]
        public InstructionOffset(int offset)
        {
        }

        public int Offset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        public bool IsEndOfMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

