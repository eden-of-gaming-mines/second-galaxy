﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.PE;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class PortablePdbReader : ISymbolReader, IDisposable
    {
        private readonly Image image;
        private readonly ModuleDefinition module;
        private readonly MetadataReader reader;
        private readonly MetadataReader debug_reader;

        [MethodImpl(0x8000)]
        internal PortablePdbReader(Image image, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }

        [MethodImpl(0x8000)]
        public bool ProcessDebugHeader(ImageDebugHeader header)
        {
        }

        [MethodImpl(0x8000)]
        public MethodDebugInformation Read(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadCustomDebugInformations(MethodDebugInformation info)
        {
        }

        [MethodImpl(0x8000)]
        private static int ReadInt32(byte[] bytes, int start)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadModule()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadScope(MethodDebugInformation method_info)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSequencePoints(MethodDebugInformation method_info)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadStateMachineKickOffMethod(MethodDebugInformation method_info)
        {
        }

        private bool IsEmbedded
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

