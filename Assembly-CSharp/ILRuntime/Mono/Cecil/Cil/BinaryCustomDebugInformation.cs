﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public sealed class BinaryCustomDebugInformation : CustomDebugInformation
    {
        private byte[] data;

        public BinaryCustomDebugInformation(Guid identifier, byte[] data) : base(identifier)
        {
            this.data = data;
        }

        public byte[] Data
        {
            get => 
                this.data;
            set => 
                (this.data = value);
        }

        public override CustomDebugInformationKind Kind =>
            CustomDebugInformationKind.Binary;
    }
}

