﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public sealed class EmbeddedPortablePdbReaderProvider : ISymbolReaderProvider
    {
        [MethodImpl(0x8000)]
        private static Stream GetPortablePdbStream(ImageDebugHeaderEntry entry)
        {
        }

        public ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream)
        {
            throw new NotSupportedException();
        }

        [MethodImpl(0x8000)]
        public ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName)
        {
        }
    }
}

