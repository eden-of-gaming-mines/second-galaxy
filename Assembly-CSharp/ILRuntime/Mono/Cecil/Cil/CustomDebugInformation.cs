﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class CustomDebugInformation : DebugInformation
    {
        private Guid identifier;

        [MethodImpl(0x8000)]
        internal CustomDebugInformation(Guid identifier)
        {
        }

        public Guid Identifier =>
            this.identifier;

        public abstract CustomDebugInformationKind Kind { get; }
    }
}

