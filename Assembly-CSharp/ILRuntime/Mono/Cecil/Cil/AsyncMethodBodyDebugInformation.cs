﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AsyncMethodBodyDebugInformation : CustomDebugInformation
    {
        internal InstructionOffset catch_handler;
        internal Collection<InstructionOffset> yields;
        internal Collection<InstructionOffset> resumes;
        internal Collection<MethodDefinition> resume_methods;
        public static Guid KindIdentifier;

        [MethodImpl(0x8000)]
        public AsyncMethodBodyDebugInformation()
        {
        }

        [MethodImpl(0x8000)]
        public AsyncMethodBodyDebugInformation(Instruction catchHandler)
        {
        }

        [MethodImpl(0x8000)]
        internal AsyncMethodBodyDebugInformation(int catchHandler)
        {
        }

        public InstructionOffset CatchHandler
        {
            get => 
                this.catch_handler;
            set => 
                (this.catch_handler = value);
        }

        public Collection<InstructionOffset> Yields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<InstructionOffset> Resumes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<MethodDefinition> ResumeMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override CustomDebugInformationKind Kind =>
            CustomDebugInformationKind.AsyncMethodBody;
    }
}

