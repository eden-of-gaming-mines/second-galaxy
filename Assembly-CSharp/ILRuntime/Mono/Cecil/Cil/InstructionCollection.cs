﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal class InstructionCollection : Collection<Instruction>
    {
        private readonly MethodDefinition method;

        internal InstructionCollection(MethodDefinition method)
        {
            this.method = method;
        }

        internal InstructionCollection(MethodDefinition method, int capacity) : base(capacity)
        {
            this.method = method;
        }

        [MethodImpl(0x8000)]
        protected override void OnAdd(Instruction item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnInsert(Instruction item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnRemove(Instruction item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSet(Instruction item, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveSequencePoint(Instruction instruction)
        {
        }
    }
}

