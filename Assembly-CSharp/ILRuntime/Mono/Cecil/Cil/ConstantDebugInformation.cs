﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ConstantDebugInformation : DebugInformation
    {
        private string name;
        private TypeReference constant_type;
        private object value;

        [MethodImpl(0x8000)]
        public ConstantDebugInformation(string name, TypeReference constant_type, object value)
        {
        }

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public TypeReference ConstantType
        {
            get => 
                this.constant_type;
            set => 
                (this.constant_type = value);
        }

        public object Value
        {
            get => 
                this.value;
            set => 
                (this.value = value);
        }
    }
}

