﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.PE;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class CodeReader : BinaryStreamReader
    {
        internal readonly MetadataReader reader;
        private int start;
        private MethodDefinition method;
        private MethodBody body;

        [MethodImpl(0x8000)]
        public CodeReader(MetadataReader reader)
        {
        }

        public CallSite GetCallSite(MetadataToken token) => 
            this.reader.ReadCallSite(token);

        [MethodImpl(0x8000)]
        private Instruction GetInstruction(int offset)
        {
        }

        [MethodImpl(0x8000)]
        private static Instruction GetInstruction(Collection<Instruction> instructions, int offset)
        {
        }

        public ParameterDefinition GetParameter(int index) => 
            this.body.GetParameter(index);

        [MethodImpl(0x8000)]
        public string GetString(MetadataToken token)
        {
        }

        public VariableDefinition GetVariable(int index) => 
            this.body.GetVariable(index);

        [MethodImpl(0x8000)]
        public void MoveBackTo(int position)
        {
        }

        [MethodImpl(0x8000)]
        public int MoveTo(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadAsyncMethodBody(AsyncMethodBodyDebugInformation async_method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadCode()
        {
        }

        [MethodImpl(0x8000)]
        private int ReadCodeSize()
        {
        }

        [MethodImpl(0x8000)]
        public int ReadCodeSize(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadCustomDebugInformations(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadDebugInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadExceptionHandlers(int count, Func<int> read_entry, Func<int> read_length)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadExceptionHandlerSpecific(ExceptionHandler handler)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadFatMethod()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadFatSection()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMethodBody()
        {
        }

        [MethodImpl(0x8000)]
        public MethodBody ReadMethodBody(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private OpCode ReadOpCode()
        {
        }

        [MethodImpl(0x8000)]
        private object ReadOperand(Instruction instruction)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadScope(ScopeDebugInformation scope)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadScopes(Collection<ScopeDebugInformation> scopes)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSection()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSequencePoints()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSmallSection()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadStateMachineScope(StateMachineScopeDebugInformation state_machine_scope)
        {
        }

        public MetadataToken ReadToken() => 
            new MetadataToken(this.ReadUInt32());

        [MethodImpl(0x8000)]
        public VariableDefinitionCollection ReadVariables(MetadataToken local_var_token)
        {
        }

        [MethodImpl(0x8000)]
        private void ResolveBranches(Collection<Instruction> instructions)
        {
        }

        private int Offset =>
            (base.Position - this.start);
    }
}

