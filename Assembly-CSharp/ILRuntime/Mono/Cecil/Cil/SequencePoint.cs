﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SequencePoint
    {
        internal InstructionOffset offset;
        private ILRuntime.Mono.Cecil.Cil.Document document;
        private int start_line;
        private int start_column;
        private int end_line;
        private int end_column;

        [MethodImpl(0x8000)]
        public SequencePoint(Instruction instruction, ILRuntime.Mono.Cecil.Cil.Document document)
        {
        }

        [MethodImpl(0x8000)]
        internal SequencePoint(int offset, ILRuntime.Mono.Cecil.Cil.Document document)
        {
        }

        public int Offset =>
            this.offset.Offset;

        public int StartLine
        {
            get => 
                this.start_line;
            set => 
                (this.start_line = value);
        }

        public int StartColumn
        {
            get => 
                this.start_column;
            set => 
                (this.start_column = value);
        }

        public int EndLine
        {
            get => 
                this.end_line;
            set => 
                (this.end_line = value);
        }

        public int EndColumn
        {
            get => 
                this.end_column;
            set => 
                (this.end_column = value);
        }

        public bool IsHidden
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.Cil.Document Document
        {
            get => 
                this.document;
            set => 
                (this.document = value);
        }
    }
}

