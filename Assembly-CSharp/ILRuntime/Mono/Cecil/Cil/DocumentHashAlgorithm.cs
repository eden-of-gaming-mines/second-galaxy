﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public enum DocumentHashAlgorithm
    {
        None,
        MD5,
        SHA1,
        SHA256
    }
}

