﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public enum ImageDebugType
    {
        CodeView = 2,
        Deterministic = 0x10,
        EmbeddedPortablePdb = 0x11
    }
}

