﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface ICustomDebugInformationProvider : IMetadataTokenProvider
    {
        bool HasCustomDebugInformations { get; }

        Collection<CustomDebugInformation> CustomDebugInformations { get; }
    }
}

