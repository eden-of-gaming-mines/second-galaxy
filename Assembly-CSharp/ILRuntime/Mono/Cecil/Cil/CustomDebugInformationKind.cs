﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public enum CustomDebugInformationKind
    {
        Binary,
        StateMachineScope,
        DynamicVariable,
        DefaultNamespace,
        AsyncMethodBody,
        EmbeddedSource,
        SourceLink
    }
}

