﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public enum DocumentLanguage
    {
        Other,
        C,
        Cpp,
        CSharp,
        Basic,
        Java,
        Cobol,
        Pascal,
        Cil,
        JScript,
        Smc,
        MCpp,
        FSharp
    }
}

