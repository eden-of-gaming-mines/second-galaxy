﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;

    public sealed class VariableDefinition : VariableReference
    {
        public VariableDefinition(TypeReference variableType) : base(variableType)
        {
        }

        public override VariableDefinition Resolve() => 
            this;

        public bool IsPinned =>
            base.variable_type.IsPinned;
    }
}

