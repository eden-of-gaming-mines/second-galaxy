﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    internal enum SymbolKind
    {
        NativePdb,
        PortablePdb,
        EmbeddedPortablePdb,
        Mdb
    }
}

