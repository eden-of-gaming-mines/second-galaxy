﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    [Flags]
    public enum VariableAttributes : ushort
    {
        None = 0,
        DebuggerHidden = 1
    }
}

