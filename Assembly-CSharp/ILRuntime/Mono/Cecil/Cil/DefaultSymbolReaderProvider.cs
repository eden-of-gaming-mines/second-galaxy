﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public class DefaultSymbolReaderProvider : ISymbolReaderProvider
    {
        private readonly bool throw_if_no_symbol;

        public DefaultSymbolReaderProvider() : this(true)
        {
        }

        public DefaultSymbolReaderProvider(bool throwIfNoSymbol)
        {
            this.throw_if_no_symbol = throwIfNoSymbol;
        }

        [MethodImpl(0x8000)]
        public ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream)
        {
        }

        [MethodImpl(0x8000)]
        public ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName)
        {
        }
    }
}

