﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class VariableDebugInformation : DebugInformation
    {
        private string name;
        private ushort attributes;
        internal VariableIndex index;

        [MethodImpl(0x8000)]
        public VariableDebugInformation(VariableDefinition variable, string name)
        {
        }

        [MethodImpl(0x8000)]
        internal VariableDebugInformation(int index, string name)
        {
        }

        public int Index =>
            this.index.Index;

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public VariableAttributes Attributes
        {
            get => 
                ((VariableAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public bool IsDebuggerHidden
        {
            get => 
                this.attributes.GetAttributes(1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

