﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;
    using System.Text;

    public sealed class Instruction
    {
        internal int offset;
        internal ILRuntime.Mono.Cecil.Cil.OpCode opcode;
        internal object operand;
        internal Instruction previous;
        internal Instruction next;

        [MethodImpl(0x8000)]
        internal Instruction(ILRuntime.Mono.Cecil.Cil.OpCode opcode, object operand)
        {
        }

        [MethodImpl(0x8000)]
        internal Instruction(int offset, ILRuntime.Mono.Cecil.Cil.OpCode opCode)
        {
        }

        [MethodImpl(0x8000)]
        private static void AppendLabel(StringBuilder builder, Instruction instruction)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, CallSite site)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, Instruction target)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, VariableDefinition variable)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, FieldReference field)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, MethodReference method)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, Instruction[] targets)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, ParameterDefinition parameter)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, byte value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, double value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, int value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, long value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, sbyte value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, float value)
        {
        }

        [MethodImpl(0x8000)]
        public static Instruction Create(ILRuntime.Mono.Cecil.Cil.OpCode opcode, string value)
        {
        }

        [MethodImpl(0x8000)]
        public int GetSize()
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public int Offset
        {
            get => 
                this.offset;
            set => 
                (this.offset = value);
        }

        public ILRuntime.Mono.Cecil.Cil.OpCode OpCode
        {
            get => 
                this.opcode;
            set => 
                (this.opcode = value);
        }

        public object Operand
        {
            get => 
                this.operand;
            set => 
                (this.operand = value);
        }

        public Instruction Previous
        {
            get => 
                this.previous;
            set => 
                (this.previous = value);
        }

        public Instruction Next
        {
            get => 
                this.next;
            set => 
                (this.next = value);
        }
    }
}

