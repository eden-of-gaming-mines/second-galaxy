﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public sealed class SymbolsNotMatchingException : InvalidOperationException
    {
        public SymbolsNotMatchingException(string message) : base(message)
        {
        }

        private SymbolsNotMatchingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

