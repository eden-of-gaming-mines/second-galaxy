﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal static class PdbGuidMapping
    {
        private static readonly Dictionary<Guid, DocumentLanguage> guid_language;
        private static readonly Dictionary<DocumentLanguage, Guid> language_guid;
        private static readonly Guid type_text;
        private static readonly Guid hash_md5;
        private static readonly Guid hash_sha1;
        private static readonly Guid hash_sha256;
        private static readonly Guid vendor_ms;

        [MethodImpl(0x8000)]
        private static void AddMapping(DocumentLanguage language, Guid guid)
        {
        }

        [MethodImpl(0x8000)]
        public static Guid ToGuid(this DocumentHashAlgorithm hash_algo)
        {
        }

        [MethodImpl(0x8000)]
        public static Guid ToGuid(this DocumentLanguage language)
        {
        }

        [MethodImpl(0x8000)]
        public static Guid ToGuid(this DocumentLanguageVendor vendor)
        {
        }

        [MethodImpl(0x8000)]
        public static Guid ToGuid(this DocumentType type)
        {
        }

        [MethodImpl(0x8000)]
        public static DocumentHashAlgorithm ToHashAlgorithm(this Guid guid)
        {
        }

        [MethodImpl(0x8000)]
        public static DocumentLanguage ToLanguage(this Guid guid)
        {
        }

        [MethodImpl(0x8000)]
        public static DocumentType ToType(this Guid guid)
        {
        }

        [MethodImpl(0x8000)]
        public static DocumentLanguageVendor ToVendor(this Guid guid)
        {
        }
    }
}

