﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct VariableIndex
    {
        private readonly VariableDefinition variable;
        private readonly int? index;
        [MethodImpl(0x8000)]
        public VariableIndex(VariableDefinition variable)
        {
        }

        [MethodImpl(0x8000)]
        public VariableIndex(int index)
        {
        }

        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

