﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class EmbeddedPortablePdbReader : ISymbolReader, IDisposable
    {
        private readonly PortablePdbReader reader;

        [MethodImpl(0x8000)]
        internal EmbeddedPortablePdbReader(PortablePdbReader reader)
        {
        }

        public void Dispose()
        {
            this.reader.Dispose();
        }

        public bool ProcessDebugHeader(ImageDebugHeader header) => 
            this.reader.ProcessDebugHeader(header);

        public MethodDebugInformation Read(MethodDefinition method) => 
            this.reader.Read(method);
    }
}

