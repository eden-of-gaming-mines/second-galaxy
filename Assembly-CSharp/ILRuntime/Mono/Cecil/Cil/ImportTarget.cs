﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;

    public sealed class ImportTarget
    {
        internal ImportTargetKind kind;
        internal string @namespace;
        internal TypeReference type;
        internal AssemblyNameReference reference;
        internal string alias;

        public ImportTarget(ImportTargetKind kind)
        {
            this.kind = kind;
        }

        public string Namespace
        {
            get => 
                this.@namespace;
            set => 
                (this.@namespace = value);
        }

        public TypeReference Type
        {
            get => 
                this.type;
            set => 
                (this.type = value);
        }

        public AssemblyNameReference AssemblyReference
        {
            get => 
                this.reference;
            set => 
                (this.reference = value);
        }

        public string Alias
        {
            get => 
                this.alias;
            set => 
                (this.alias = value);
        }

        public ImportTargetKind Kind
        {
            get => 
                this.kind;
            set => 
                (this.kind = value);
        }
    }
}

