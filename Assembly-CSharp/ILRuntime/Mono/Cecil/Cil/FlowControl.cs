﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;

    public enum FlowControl
    {
        Branch,
        Break,
        Call,
        Cond_Branch,
        Meta,
        Next,
        Phi,
        Return,
        Throw
    }
}

