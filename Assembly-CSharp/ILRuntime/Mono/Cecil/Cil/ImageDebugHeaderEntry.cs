﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ImageDebugHeaderEntry
    {
        private ImageDebugDirectory directory;
        private readonly byte[] data;

        [MethodImpl(0x8000)]
        public ImageDebugHeaderEntry(ImageDebugDirectory directory, byte[] data)
        {
        }

        public ImageDebugDirectory Directory
        {
            get => 
                this.directory;
            internal set => 
                (this.directory = value);
        }

        public byte[] Data =>
            this.data;
    }
}

