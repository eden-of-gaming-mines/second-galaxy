﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class StateMachineScopeDebugInformation : CustomDebugInformation
    {
        internal Collection<StateMachineScope> scopes;
        public static Guid KindIdentifier;

        public StateMachineScopeDebugInformation() : base(KindIdentifier)
        {
        }

        public Collection<StateMachineScope> Scopes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override CustomDebugInformationKind Kind =>
            CustomDebugInformationKind.StateMachineScope;
    }
}

