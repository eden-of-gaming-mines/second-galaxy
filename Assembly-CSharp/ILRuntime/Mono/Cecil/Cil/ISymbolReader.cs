﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;

    public interface ISymbolReader : IDisposable
    {
        bool ProcessDebugHeader(ImageDebugHeader header);
        MethodDebugInformation Read(MethodDefinition method);
    }
}

