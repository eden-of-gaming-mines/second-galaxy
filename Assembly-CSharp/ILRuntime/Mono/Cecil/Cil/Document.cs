﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Document : DebugInformation
    {
        private string url;
        private Guid type;
        private Guid hash_algorithm;
        private Guid language;
        private Guid language_vendor;
        private byte[] hash;
        private byte[] embedded_source;

        [MethodImpl(0x8000)]
        public Document(string url)
        {
        }

        public string Url
        {
            get => 
                this.url;
            set => 
                (this.url = value);
        }

        public DocumentType Type
        {
            get => 
                this.type.ToType();
            set => 
                (this.type = value.ToGuid());
        }

        public Guid TypeGuid
        {
            get => 
                this.type;
            set => 
                (this.type = value);
        }

        public DocumentHashAlgorithm HashAlgorithm
        {
            get => 
                this.hash_algorithm.ToHashAlgorithm();
            set => 
                (this.hash_algorithm = value.ToGuid());
        }

        public Guid HashAlgorithmGuid
        {
            get => 
                this.hash_algorithm;
            set => 
                (this.hash_algorithm = value);
        }

        public DocumentLanguage Language
        {
            get => 
                this.language.ToLanguage();
            set => 
                (this.language = value.ToGuid());
        }

        public Guid LanguageGuid
        {
            get => 
                this.language;
            set => 
                (this.language = value);
        }

        public DocumentLanguageVendor LanguageVendor
        {
            get => 
                this.language_vendor.ToVendor();
            set => 
                (this.language_vendor = value.ToGuid());
        }

        public Guid LanguageVendorGuid
        {
            get => 
                this.language_vendor;
            set => 
                (this.language_vendor = value);
        }

        public byte[] Hash
        {
            get => 
                this.hash;
            set => 
                (this.hash = value);
        }

        public byte[] EmbeddedSource
        {
            get => 
                this.embedded_source;
            set => 
                (this.embedded_source = value);
        }
    }
}

