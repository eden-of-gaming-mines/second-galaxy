﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SourceLinkDebugInformation : CustomDebugInformation
    {
        internal string content;
        public static Guid KindIdentifier;

        [MethodImpl(0x8000)]
        public SourceLinkDebugInformation(string content)
        {
        }

        public string Content
        {
            get => 
                this.content;
            set => 
                (this.content = value);
        }

        public override CustomDebugInformationKind Kind =>
            CustomDebugInformationKind.SourceLink;
    }
}

