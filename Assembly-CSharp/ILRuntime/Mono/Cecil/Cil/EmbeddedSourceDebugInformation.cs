﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class EmbeddedSourceDebugInformation : CustomDebugInformation
    {
        internal byte[] content;
        internal bool compress;
        public static Guid KindIdentifier;

        [MethodImpl(0x8000)]
        public EmbeddedSourceDebugInformation(byte[] content, bool compress)
        {
        }

        public byte[] Content
        {
            get => 
                this.content;
            set => 
                (this.content = value);
        }

        public bool Compress
        {
            get => 
                this.compress;
            set => 
                (this.compress = value);
        }

        public override CustomDebugInformationKind Kind =>
            CustomDebugInformationKind.EmbeddedSource;
    }
}

