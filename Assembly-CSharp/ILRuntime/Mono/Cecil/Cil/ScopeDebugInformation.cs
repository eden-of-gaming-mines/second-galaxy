﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class ScopeDebugInformation : DebugInformation
    {
        internal InstructionOffset start;
        internal InstructionOffset end;
        internal ImportDebugInformation import;
        internal Collection<ScopeDebugInformation> scopes;
        internal Collection<VariableDebugInformation> variables;
        internal Collection<ConstantDebugInformation> constants;

        [MethodImpl(0x8000)]
        internal ScopeDebugInformation()
        {
        }

        [MethodImpl(0x8000)]
        public ScopeDebugInformation(Instruction start, Instruction end)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetName(VariableDefinition variable, out string name)
        {
        }

        public InstructionOffset Start
        {
            get => 
                this.start;
            set => 
                (this.start = value);
        }

        public InstructionOffset End
        {
            get => 
                this.end;
            set => 
                (this.end = value);
        }

        public ImportDebugInformation Import
        {
            get => 
                this.import;
            set => 
                (this.import = value);
        }

        public bool HasScopes =>
            !this.scopes.IsNullOrEmpty<ScopeDebugInformation>();

        public Collection<ScopeDebugInformation> Scopes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasVariables =>
            !this.variables.IsNullOrEmpty<VariableDebugInformation>();

        public Collection<VariableDebugInformation> Variables
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasConstants =>
            !this.constants.IsNullOrEmpty<ConstantDebugInformation>();

        public Collection<ConstantDebugInformation> Constants
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

