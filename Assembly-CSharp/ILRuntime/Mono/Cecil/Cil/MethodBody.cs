﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MethodBody
    {
        internal readonly MethodDefinition method;
        internal ParameterDefinition this_parameter;
        internal int max_stack_size;
        internal int code_size;
        internal bool init_locals;
        internal MetadataToken local_var_token;
        internal Collection<Instruction> instructions;
        internal Collection<ExceptionHandler> exceptions;
        internal Collection<VariableDefinition> variables;

        public MethodBody(MethodDefinition method)
        {
            this.method = method;
        }

        [MethodImpl(0x8000)]
        private static ParameterDefinition CreateThisParameter(MethodDefinition method)
        {
        }

        public ILProcessor GetILProcessor() => 
            new ILProcessor(this);

        public MethodDefinition Method =>
            this.method;

        public int MaxStackSize
        {
            get => 
                this.max_stack_size;
            set => 
                (this.max_stack_size = value);
        }

        public int CodeSize =>
            this.code_size;

        public bool InitLocals
        {
            get => 
                this.init_locals;
            set => 
                (this.init_locals = value);
        }

        public MetadataToken LocalVarToken
        {
            get => 
                this.local_var_token;
            set => 
                (this.local_var_token = value);
        }

        public Collection<Instruction> Instructions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasExceptionHandlers =>
            !this.exceptions.IsNullOrEmpty<ExceptionHandler>();

        public Collection<ExceptionHandler> ExceptionHandlers
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasVariables =>
            !this.variables.IsNullOrEmpty<VariableDefinition>();

        public Collection<VariableDefinition> Variables
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ParameterDefinition ThisParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

