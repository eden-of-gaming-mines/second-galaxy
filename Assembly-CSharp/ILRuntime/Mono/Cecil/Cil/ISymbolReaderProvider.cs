﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.IO;

    public interface ISymbolReaderProvider
    {
        ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream);
        ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName);
    }
}

