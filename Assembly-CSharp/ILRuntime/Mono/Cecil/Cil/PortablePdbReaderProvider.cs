﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.PE;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public sealed class PortablePdbReaderProvider : ISymbolReaderProvider
    {
        [MethodImpl(0x8000)]
        public ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream)
        {
        }

        [MethodImpl(0x8000)]
        public ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName)
        {
        }

        private ISymbolReader GetSymbolReader(ModuleDefinition module, Disposable<Stream> symbolStream, string fileName) => 
            new PortablePdbReader(ImageReader.ReadPortablePdb(symbolStream, fileName), module);
    }
}

