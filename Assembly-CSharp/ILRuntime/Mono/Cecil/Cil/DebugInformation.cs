﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class DebugInformation : ICustomDebugInformationProvider, IMetadataTokenProvider
    {
        internal ILRuntime.Mono.Cecil.MetadataToken token;
        internal Collection<CustomDebugInformation> custom_infos;

        internal DebugInformation()
        {
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }

        public bool HasCustomDebugInformations =>
            !this.custom_infos.IsNullOrEmpty<CustomDebugInformation>();

        public Collection<CustomDebugInformation> CustomDebugInformations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

