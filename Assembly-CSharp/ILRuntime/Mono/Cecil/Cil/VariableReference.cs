﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class VariableReference
    {
        internal int index;
        protected TypeReference variable_type;

        [MethodImpl(0x8000)]
        internal VariableReference(TypeReference variable_type)
        {
        }

        public abstract VariableDefinition Resolve();
        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public TypeReference VariableType
        {
            get => 
                this.variable_type;
            set => 
                (this.variable_type = value);
        }

        public int Index =>
            this.index;
    }
}

