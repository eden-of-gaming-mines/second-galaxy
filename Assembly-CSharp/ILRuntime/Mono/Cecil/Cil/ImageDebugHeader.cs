﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ImageDebugHeader
    {
        private readonly ImageDebugHeaderEntry[] entries;

        public ImageDebugHeader() : this(Empty<ImageDebugHeaderEntry>.Array)
        {
        }

        [MethodImpl(0x8000)]
        public ImageDebugHeader(ImageDebugHeaderEntry[] entries)
        {
        }

        [MethodImpl(0x8000)]
        public ImageDebugHeader(ImageDebugHeaderEntry entry)
        {
        }

        public bool HasEntries =>
            !this.entries.IsNullOrEmpty<ImageDebugHeaderEntry>();

        public ImageDebugHeaderEntry[] Entries =>
            this.entries;
    }
}

