﻿namespace ILRuntime.Mono.Cecil.Cil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct OpCode : IEquatable<OpCode>
    {
        private readonly byte op1;
        private readonly byte op2;
        private readonly byte code;
        private readonly byte flow_control;
        private readonly byte opcode_type;
        private readonly byte operand_type;
        private readonly byte stack_behavior_pop;
        private readonly byte stack_behavior_push;
        [MethodImpl(0x8000)]
        internal OpCode(int x, int y)
        {
        }

        public string Name =>
            OpCodeNames.names[(int) this.Code];
        public int Size
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        public byte Op1 =>
            this.op1;
        public byte Op2 =>
            this.op2;
        public short Value
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        public ILRuntime.Mono.Cecil.Cil.Code Code =>
            ((ILRuntime.Mono.Cecil.Cil.Code) this.code);
        public ILRuntime.Mono.Cecil.Cil.FlowControl FlowControl =>
            ((ILRuntime.Mono.Cecil.Cil.FlowControl) this.flow_control);
        public ILRuntime.Mono.Cecil.Cil.OpCodeType OpCodeType =>
            ((ILRuntime.Mono.Cecil.Cil.OpCodeType) this.opcode_type);
        public ILRuntime.Mono.Cecil.Cil.OperandType OperandType =>
            ((ILRuntime.Mono.Cecil.Cil.OperandType) this.operand_type);
        public StackBehaviour StackBehaviourPop =>
            ((StackBehaviour) this.stack_behavior_pop);
        public StackBehaviour StackBehaviourPush =>
            ((StackBehaviour) this.stack_behavior_push);
        public override int GetHashCode() => 
            this.Value;

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public bool Equals(OpCode opcode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator ==(OpCode one, OpCode other)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator !=(OpCode one, OpCode other)
        {
        }

        public override string ToString() => 
            this.Name;
    }
}

