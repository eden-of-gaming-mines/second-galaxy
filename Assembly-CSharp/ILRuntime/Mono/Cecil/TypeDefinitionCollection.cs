﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal sealed class TypeDefinitionCollection : Collection<TypeDefinition>
    {
        private readonly ModuleDefinition container;
        private readonly Dictionary<Row<string, string>, TypeDefinition> name_cache;

        [MethodImpl(0x8000)]
        internal TypeDefinitionCollection(ModuleDefinition container)
        {
        }

        [MethodImpl(0x8000)]
        internal TypeDefinitionCollection(ModuleDefinition container, int capacity)
        {
        }

        [MethodImpl(0x8000)]
        private void Attach(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void Detach(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetType(string fullname)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetType(string @namespace, string name)
        {
        }

        protected override void OnAdd(TypeDefinition item, int index)
        {
            this.Attach(item);
        }

        [MethodImpl(0x8000)]
        protected override void OnClear()
        {
        }

        protected override void OnInsert(TypeDefinition item, int index)
        {
            this.Attach(item);
        }

        protected override void OnRemove(TypeDefinition item, int index)
        {
            this.Detach(item);
        }

        protected override void OnSet(TypeDefinition item, int index)
        {
            this.Attach(item);
        }
    }
}

