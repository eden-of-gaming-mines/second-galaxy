﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class GenericInstanceType : TypeSpecification, IGenericInstance, IGenericContext, IMetadataTokenProvider
    {
        private Collection<TypeReference> arguments;

        [MethodImpl(0x8000)]
        public GenericInstanceType(TypeReference type)
        {
        }

        IGenericParameterProvider IGenericContext.Type =>
            base.ElementType;

        public bool HasGenericArguments =>
            !this.arguments.IsNullOrEmpty<TypeReference>();

        public Collection<TypeReference> GenericArguments
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override TypeReference DeclaringType
        {
            get => 
                base.ElementType.DeclaringType;
            set
            {
                throw new NotSupportedException();
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsGenericInstance =>
            true;

        public override bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

