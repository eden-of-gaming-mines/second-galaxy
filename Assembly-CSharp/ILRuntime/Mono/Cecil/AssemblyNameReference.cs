﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public class AssemblyNameReference : IMetadataScope, IMetadataTokenProvider
    {
        private string name;
        private string culture;
        private System.Version version;
        private uint attributes;
        private byte[] public_key;
        private byte[] public_key_token;
        private AssemblyHashAlgorithm hash_algorithm;
        private byte[] hash;
        internal ILRuntime.Mono.Cecil.MetadataToken token;
        private string full_name;

        [MethodImpl(0x8000)]
        internal AssemblyNameReference()
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyNameReference(string name, System.Version version)
        {
        }

        [MethodImpl(0x8000)]
        private byte[] HashPublicKey()
        {
        }

        [MethodImpl(0x8000)]
        public static AssemblyNameReference Parse(string fullName)
        {
        }

        public override string ToString() => 
            this.FullName;

        public string Name
        {
            get => 
                this.name;
            set
            {
                this.name = value;
                this.full_name = null;
            }
        }

        public string Culture
        {
            get => 
                this.culture;
            set
            {
                this.culture = value;
                this.full_name = null;
            }
        }

        public System.Version Version
        {
            get => 
                this.version;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public AssemblyAttributes Attributes
        {
            get => 
                ((AssemblyAttributes) this.attributes);
            set => 
                (this.attributes = (uint) value);
        }

        public bool HasPublicKey
        {
            get => 
                this.attributes.GetAttributes(1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSideBySideCompatible
        {
            get => 
                this.attributes.GetAttributes(0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRetargetable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsWindowsRuntime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public byte[] PublicKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public byte[] PublicKeyToken
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                this.public_key_token = value;
                this.full_name = null;
            }
        }

        public virtual ILRuntime.Mono.Cecil.MetadataScopeType MetadataScopeType =>
            ILRuntime.Mono.Cecil.MetadataScopeType.AssemblyNameReference;

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public AssemblyHashAlgorithm HashAlgorithm
        {
            get => 
                this.hash_algorithm;
            set => 
                (this.hash_algorithm = value);
        }

        public virtual byte[] Hash
        {
            get => 
                this.hash;
            set => 
                (this.hash = value);
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }
    }
}

