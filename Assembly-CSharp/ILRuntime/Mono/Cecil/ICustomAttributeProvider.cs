﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface ICustomAttributeProvider : IMetadataTokenProvider
    {
        Collection<CustomAttribute> CustomAttributes { get; }

        bool HasCustomAttributes { get; }
    }
}

