﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public class FieldReference : MemberReference
    {
        private TypeReference field_type;

        [MethodImpl(0x8000)]
        internal FieldReference()
        {
        }

        [MethodImpl(0x8000)]
        public FieldReference(string name, TypeReference fieldType)
        {
        }

        [MethodImpl(0x8000)]
        public FieldReference(string name, TypeReference fieldType, TypeReference declaringType)
        {
        }

        [MethodImpl(0x8000)]
        public virtual FieldDefinition Resolve()
        {
        }

        protected override IMemberDefinition ResolveDefinition() => 
            this.Resolve();

        public TypeReference FieldType
        {
            get => 
                this.field_type;
            set => 
                (this.field_type = value);
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

