﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct ArrayDimension
    {
        private int? lower_bound;
        private int? upper_bound;
        public ArrayDimension(int? lowerBound, int? upperBound)
        {
            this.lower_bound = lowerBound;
            this.upper_bound = upperBound;
        }

        public int? LowerBound
        {
            get => 
                this.lower_bound;
            set => 
                (this.lower_bound = value);
        }
        public int? UpperBound
        {
            get => 
                this.upper_bound;
            set => 
                (this.upper_bound = value);
        }
        public bool IsSized
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

