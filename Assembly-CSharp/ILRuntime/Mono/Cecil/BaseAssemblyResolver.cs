﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public abstract class BaseAssemblyResolver : IAssemblyResolver, IDisposable
    {
        private static readonly bool on_mono;
        private readonly Collection<string> directories;
        private Collection<string> gac_paths;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private AssemblyResolveEventHandler ResolveFailure;

        public event AssemblyResolveEventHandler ResolveFailure
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected BaseAssemblyResolver()
        {
        }

        public void AddSearchDirectory(string directory)
        {
            this.directories.Add(directory);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyDefinition GetAssembly(string file, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetAssemblyFile(AssemblyNameReference reference, string prefix, string gac)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyDefinition GetAssemblyInGac(AssemblyNameReference reference, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyDefinition GetAssemblyInMonoGac(AssemblyNameReference reference, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyDefinition GetAssemblyInNetGac(AssemblyNameReference reference, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyDefinition GetCorlib(AssemblyNameReference reference, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetCurrentMonoGac()
        {
        }

        [MethodImpl(0x8000)]
        private static Collection<string> GetDefaultMonoGacPaths()
        {
        }

        [MethodImpl(0x8000)]
        private static Collection<string> GetGacPaths()
        {
        }

        [MethodImpl(0x8000)]
        public string[] GetSearchDirectories()
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsZero(Version version)
        {
        }

        public void RemoveSearchDirectory(string directory)
        {
            this.directories.Remove(directory);
        }

        public virtual AssemblyDefinition Resolve(AssemblyNameReference name) => 
            this.Resolve(name, new ReaderParameters());

        [MethodImpl(0x8000)]
        public virtual AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual AssemblyDefinition SearchDirectory(AssemblyNameReference name, IEnumerable<string> directories, ReaderParameters parameters)
        {
        }
    }
}

