﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public class MethodReference : MemberReference, IMethodSignature, IGenericParameterProvider, IGenericContext, IMetadataTokenProvider
    {
        private int hashCode;
        private static int instance_id;
        internal ParameterDefinitionCollection parameters;
        private ILRuntime.Mono.Cecil.MethodReturnType return_type;
        private bool has_this;
        private bool explicit_this;
        private MethodCallingConvention calling_convention;
        internal Collection<GenericParameter> generic_parameters;

        [MethodImpl(0x8000)]
        internal MethodReference()
        {
        }

        [MethodImpl(0x8000)]
        public MethodReference(string name, TypeReference returnType)
        {
        }

        [MethodImpl(0x8000)]
        public MethodReference(string name, TypeReference returnType, TypeReference declaringType)
        {
        }

        public virtual MethodReference GetElementMethod() => 
            this;

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public virtual MethodDefinition Resolve()
        {
        }

        protected override IMemberDefinition ResolveDefinition() => 
            this.Resolve();

        IGenericParameterProvider IGenericContext.Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        IGenericParameterProvider IGenericContext.Method =>
            this;

        GenericParameterType IGenericParameterProvider.GenericParameterType =>
            GenericParameterType.Method;

        public virtual bool HasThis
        {
            get => 
                this.has_this;
            set => 
                (this.has_this = value);
        }

        public virtual bool ExplicitThis
        {
            get => 
                this.explicit_this;
            set => 
                (this.explicit_this = value);
        }

        public virtual MethodCallingConvention CallingConvention
        {
            get => 
                this.calling_convention;
            set => 
                (this.calling_convention = value);
        }

        public virtual bool HasParameters =>
            !this.parameters.IsNullOrEmpty<ParameterDefinition>();

        public virtual Collection<ParameterDefinition> Parameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual bool HasGenericParameters =>
            !this.generic_parameters.IsNullOrEmpty<GenericParameter>();

        public virtual Collection<GenericParameter> GenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference ReturnType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public virtual ILRuntime.Mono.Cecil.MethodReturnType MethodReturnType
        {
            get => 
                this.return_type;
            set => 
                (this.return_type = value);
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual bool IsGenericInstance =>
            false;

        public override bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

