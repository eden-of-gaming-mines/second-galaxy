﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public sealed class FixedArrayMarshalInfo : MarshalInfo
    {
        internal NativeType element_type;
        internal int size;

        public NativeType ElementType
        {
            get => 
                this.element_type;
            set => 
                (this.element_type = value);
        }

        public int Size
        {
            get => 
                this.size;
            set => 
                (this.size = value);
        }
    }
}

