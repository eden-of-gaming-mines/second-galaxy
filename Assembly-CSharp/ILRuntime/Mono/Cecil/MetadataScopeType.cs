﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum MetadataScopeType
    {
        AssemblyNameReference,
        ModuleReference,
        ModuleDefinition
    }
}

