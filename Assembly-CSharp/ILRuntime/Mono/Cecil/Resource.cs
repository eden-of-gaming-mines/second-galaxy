﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class Resource
    {
        private string name;
        private uint attributes;

        [MethodImpl(0x8000)]
        internal Resource(string name, ManifestResourceAttributes attributes)
        {
        }

        public string Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public ManifestResourceAttributes Attributes
        {
            get => 
                ((ManifestResourceAttributes) this.attributes);
            set => 
                (this.attributes = (uint) value);
        }

        public abstract ILRuntime.Mono.Cecil.ResourceType ResourceType { get; }

        public bool IsPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPrivate
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

