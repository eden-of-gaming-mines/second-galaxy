﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public interface IMarshalInfoProvider : IMetadataTokenProvider
    {
        bool HasMarshalInfo { get; }

        ILRuntime.Mono.Cecil.MarshalInfo MarshalInfo { get; set; }
    }
}

