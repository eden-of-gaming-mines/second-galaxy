﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public class TypeReference : MemberReference, IGenericParameterProvider, IGenericContext, IMetadataTokenProvider
    {
        private string @namespace;
        private bool value_type;
        private int hashCode;
        private static int instance_id;
        internal IMetadataScope scope;
        internal ModuleDefinition module;
        internal ElementType etype;
        private string fullname;
        protected Collection<GenericParameter> generic_parameters;

        [MethodImpl(0x8000)]
        protected TypeReference(string @namespace, string name)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference(string @namespace, string name, ModuleDefinition module, IMetadataScope scope)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference(string @namespace, string name, ModuleDefinition module, IMetadataScope scope, bool valueType)
        {
        }

        protected virtual void ClearFullName()
        {
            this.fullname = null;
        }

        public virtual TypeReference GetElementType() => 
            this;

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public virtual TypeDefinition Resolve()
        {
        }

        protected override IMemberDefinition ResolveDefinition() => 
            this.Resolve();

        IGenericParameterProvider IGenericContext.Type =>
            this;

        IGenericParameterProvider IGenericContext.Method =>
            null;

        GenericParameterType IGenericParameterProvider.GenericParameterType =>
            GenericParameterType.Type;

        public override string Name
        {
            get => 
                base.Name;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public virtual string Namespace
        {
            get => 
                this.@namespace;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public virtual bool IsValueType
        {
            get => 
                this.value_type;
            set => 
                (this.value_type = value);
        }

        public override ModuleDefinition Module
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal TypeReferenceProjection WindowsRuntimeProjection
        {
            get => 
                ((TypeReferenceProjection) base.projection);
            set => 
                (base.projection = value);
        }

        public virtual bool HasGenericParameters =>
            !this.generic_parameters.IsNullOrEmpty<GenericParameter>();

        public virtual Collection<GenericParameter> GenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNested =>
            !ReferenceEquals(this.DeclaringType, null);

        public override TypeReference DeclaringType
        {
            get => 
                base.DeclaringType;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual bool IsByReference =>
            false;

        public virtual bool IsPointer =>
            false;

        public virtual bool IsSentinel =>
            false;

        public virtual bool IsArray =>
            false;

        public virtual bool IsGenericParameter =>
            false;

        public virtual bool IsGenericInstance =>
            false;

        public virtual bool IsRequiredModifier =>
            false;

        public virtual bool IsOptionalModifier =>
            false;

        public virtual bool IsPinned =>
            false;

        public virtual bool IsFunctionPointer =>
            false;

        public virtual bool IsPrimitive =>
            this.etype.IsPrimitive();

        public virtual ILRuntime.Mono.Cecil.MetadataType MetadataType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

