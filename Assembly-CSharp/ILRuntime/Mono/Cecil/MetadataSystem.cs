﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.Metadata;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal sealed class MetadataSystem
    {
        internal AssemblyNameReference[] AssemblyReferences;
        internal ModuleReference[] ModuleReferences;
        internal TypeDefinition[] Types;
        internal TypeReference[] TypeReferences;
        internal FieldDefinition[] Fields;
        internal MethodDefinition[] Methods;
        internal MemberReference[] MemberReferences;
        internal Dictionary<uint, Collection<uint>> NestedTypes;
        internal Dictionary<uint, uint> ReverseNestedTypes;
        internal Dictionary<uint, Collection<Row<uint, MetadataToken>>> Interfaces;
        internal Dictionary<uint, Row<ushort, uint>> ClassLayouts;
        internal Dictionary<uint, uint> FieldLayouts;
        internal Dictionary<uint, uint> FieldRVAs;
        internal Dictionary<MetadataToken, uint> FieldMarshals;
        internal Dictionary<MetadataToken, Row<ElementType, uint>> Constants;
        internal Dictionary<uint, Collection<MetadataToken>> Overrides;
        internal Dictionary<MetadataToken, Range[]> CustomAttributes;
        internal Dictionary<MetadataToken, Range[]> SecurityDeclarations;
        internal Dictionary<uint, Range> Events;
        internal Dictionary<uint, Range> Properties;
        internal Dictionary<uint, Row<MethodSemanticsAttributes, MetadataToken>> Semantics;
        internal Dictionary<uint, Row<PInvokeAttributes, uint, uint>> PInvokes;
        internal Dictionary<MetadataToken, Range[]> GenericParameters;
        internal Dictionary<uint, Collection<MetadataToken>> GenericConstraints;
        internal Document[] Documents;
        internal Dictionary<uint, Collection<Row<uint, Range, Range, uint, uint, uint>>> LocalScopes;
        internal ImportDebugInformation[] ImportScopes;
        internal Dictionary<uint, uint> StateMachineMethods;
        internal Dictionary<MetadataToken, Row<Guid, uint, uint>[]> CustomDebugInformations;
        private static Dictionary<string, Row<ElementType, bool>> primitive_value_types;

        public void AddEventsRange(uint type_rid, Range range)
        {
            this.Events.Add(type_rid, range);
        }

        [MethodImpl(0x8000)]
        public void AddFieldDefinition(FieldDefinition field)
        {
        }

        [MethodImpl(0x8000)]
        public void AddMemberReference(MemberReference member)
        {
        }

        [MethodImpl(0x8000)]
        public void AddMethodDefinition(MethodDefinition method)
        {
        }

        public void AddPropertiesRange(uint type_rid, Range range)
        {
            this.Properties.Add(type_rid, range);
        }

        [MethodImpl(0x8000)]
        public void AddTypeDefinition(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTypeReference(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeDefinition BinaryRangeSearch(TypeDefinition[] types, uint rid, bool field)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyNameReference GetAssemblyNameReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public Document GetDocument(uint rid)
        {
        }

        public TypeDefinition GetFieldDeclaringType(uint field_rid) => 
            BinaryRangeSearch(this.Types, field_rid, true);

        [MethodImpl(0x8000)]
        public FieldDefinition GetFieldDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public ImportDebugInformation GetImportScope(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public MemberReference GetMemberReference(uint rid)
        {
        }

        public TypeDefinition GetMethodDeclaringType(uint method_rid) => 
            BinaryRangeSearch(this.Types, method_rid, false);

        [MethodImpl(0x8000)]
        public MethodDefinition GetMethodDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetTypeDefinition(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference GetTypeReference(uint rid)
        {
        }

        [MethodImpl(0x8000)]
        private static void InitializePrimitives()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveCustomAttributeRange(ICustomAttributeProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveEventsRange(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGenericConstraintMapping(GenericParameter generic_parameter)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGenericParameterRange(IGenericParameterProvider owner)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveInterfaceMapping(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveNestedTypeMapping(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveOverrideMapping(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        public void RemovePropertiesRange(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveReverseNestedTypeMapping(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSecurityDeclarationRange(ISecurityDeclarationProvider owner)
        {
        }

        public void SetGenericConstraintMapping(uint gp_rid, Collection<MetadataToken> mapping)
        {
            this.GenericConstraints[gp_rid] = mapping;
        }

        public void SetInterfaceMapping(uint type_rid, Collection<Row<uint, MetadataToken>> mapping)
        {
            this.Interfaces[type_rid] = mapping;
        }

        public void SetLocalScopes(uint method_rid, Collection<Row<uint, Range, Range, uint, uint, uint>> records)
        {
            this.LocalScopes[method_rid] = records;
        }

        public void SetNestedTypeMapping(uint type_rid, Collection<uint> mapping)
        {
            this.NestedTypes[type_rid] = mapping;
        }

        public void SetOverrideMapping(uint rid, Collection<MetadataToken> mapping)
        {
            this.Overrides[rid] = mapping;
        }

        public void SetReverseNestedTypeMapping(uint nested, uint declaring)
        {
            this.ReverseNestedTypes[nested] = declaring;
        }

        [MethodImpl(0x8000)]
        public bool TryGetCustomAttributeRanges(ICustomAttributeProvider owner, out Range[] ranges)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetEventsRange(TypeDefinition type, out Range range)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetGenericConstraintMapping(GenericParameter generic_parameter, out Collection<MetadataToken> mapping)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetGenericParameterRanges(IGenericParameterProvider owner, out Range[] ranges)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetInterfaceMapping(TypeDefinition type, out Collection<Row<uint, MetadataToken>> mapping)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetLocalScopes(MethodDefinition method, out Collection<Row<uint, Range, Range, uint, uint, uint>> scopes)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetNestedTypeMapping(TypeDefinition type, out Collection<uint> mapping)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetOverrideMapping(MethodDefinition method, out Collection<MetadataToken> mapping)
        {
        }

        [MethodImpl(0x8000)]
        private static bool TryGetPrimitiveData(TypeReference type, out Row<ElementType, bool> primitive_data)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetPrimitiveElementType(TypeDefinition type, out ElementType etype)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetPropertiesRange(TypeDefinition type, out Range range)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetReverseNestedTypeMapping(TypeDefinition type, out uint declaring)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetSecurityDeclarationRanges(ISecurityDeclarationProvider owner, out Range[] ranges)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetStateMachineKickOffMethod(MethodDefinition method, out uint rid)
        {
        }

        [MethodImpl(0x8000)]
        public static void TryProcessPrimitiveTypeReference(TypeReference type)
        {
        }
    }
}

