﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class TypeSpecification : TypeReference
    {
        private readonly TypeReference element_type;

        [MethodImpl(0x8000)]
        internal TypeSpecification(TypeReference type)
        {
        }

        public override TypeReference GetElementType() => 
            this.element_type.GetElementType();

        public TypeReference ElementType =>
            this.element_type;

        public override string Name
        {
            get => 
                this.element_type.Name;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override string Namespace
        {
            get => 
                this.element_type.Namespace;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override IMetadataScope Scope
        {
            get => 
                this.element_type.Scope;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override ModuleDefinition Module =>
            this.element_type.Module;

        public override string FullName =>
            this.element_type.FullName;

        public override bool ContainsGenericParameter =>
            this.element_type.ContainsGenericParameter;

        public override ILRuntime.Mono.Cecil.MetadataType MetadataType =>
            ((ILRuntime.Mono.Cecil.MetadataType) base.etype);
    }
}

