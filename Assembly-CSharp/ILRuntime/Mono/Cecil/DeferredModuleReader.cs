﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.PE;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class DeferredModuleReader : ModuleReader
    {
        public DeferredModuleReader(Image image) : base(image, ReadingMode.Deferred)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ReadModule()
        {
        }

        public override void ReadSymbols(ModuleDefinition module)
        {
        }
    }
}

