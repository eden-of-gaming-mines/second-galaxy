﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Metadata;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class TypeSystem
    {
        private readonly ModuleDefinition module;
        private TypeReference type_object;
        private TypeReference type_void;
        private TypeReference type_bool;
        private TypeReference type_char;
        private TypeReference type_sbyte;
        private TypeReference type_byte;
        private TypeReference type_int16;
        private TypeReference type_uint16;
        private TypeReference type_int32;
        private TypeReference type_uint32;
        private TypeReference type_int64;
        private TypeReference type_uint64;
        private TypeReference type_single;
        private TypeReference type_double;
        private TypeReference type_intptr;
        private TypeReference type_uintptr;
        private TypeReference type_string;
        private TypeReference type_typedref;

        private TypeSystem(ModuleDefinition module)
        {
            this.module = module;
        }

        [MethodImpl(0x8000)]
        internal static TypeSystem CreateTypeSystem(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference LookupSystemType(ref TypeReference reference, string name, ElementType element_type)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference LookupSystemValueType(ref TypeReference typeRef, string name, ElementType element_type)
        {
        }

        internal abstract TypeReference LookupType(string @namespace, string name);

        [Obsolete("Use CoreLibrary")]
        public IMetadataScope Corlib =>
            this.CoreLibrary;

        public IMetadataScope CoreLibrary
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Object
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Void
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Boolean
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Char
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference SByte
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Byte
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Int16
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference UInt16
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Int32
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference UInt32
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Int64
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference UInt64
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Single
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference Double
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference IntPtr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference UIntPtr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference String
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TypeReference TypedReference
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private sealed class CommonTypeSystem : TypeSystem
        {
            private AssemblyNameReference core_library;

            public CommonTypeSystem(ModuleDefinition module) : base(module)
            {
            }

            private TypeReference CreateTypeReference(string @namespace, string name) => 
                new TypeReference(@namespace, name, base.module, this.GetCoreLibraryReference());

            public AssemblyNameReference GetCoreLibraryReference()
            {
                if (this.core_library == null)
                {
                    if (base.module.TryGetCoreLibraryReference(out this.core_library))
                    {
                        return this.core_library;
                    }
                    AssemblyNameReference reference = new AssemblyNameReference {
                        Name = "mscorlib",
                        Version = this.GetCorlibVersion(),
                        PublicKeyToken = new byte[] { 
                            0xb7,
                            0x7a,
                            0x5c,
                            0x56,
                            0x19,
                            0x34,
                            0xe0,
                            0x89
                        }
                    };
                    this.core_library = reference;
                    base.module.AssemblyReferences.Add(this.core_library);
                }
                return this.core_library;
            }

            private Version GetCorlibVersion()
            {
                switch (base.module.Runtime)
                {
                    case TargetRuntime.Net_1_0:
                    case TargetRuntime.Net_1_1:
                        return new Version(1, 0, 0, 0);

                    case TargetRuntime.Net_2_0:
                        return new Version(2, 0, 0, 0);

                    case TargetRuntime.Net_4_0:
                        return new Version(4, 0, 0, 0);
                }
                throw new NotSupportedException();
            }

            internal override TypeReference LookupType(string @namespace, string name) => 
                this.CreateTypeReference(@namespace, name);
        }

        private sealed class CoreTypeSystem : TypeSystem
        {
            [CompilerGenerated]
            private static Func<Row<string, string>, MetadataReader, TypeDefinition> <>f__am$cache0;

            public CoreTypeSystem(ModuleDefinition module) : base(module)
            {
            }

            private static void Initialize(object obj)
            {
            }

            [MethodImpl(0x8000)]
            internal override TypeReference LookupType(string @namespace, string name)
            {
            }

            [MethodImpl(0x8000)]
            private TypeReference LookupTypeDefinition(string @namespace, string name)
            {
            }

            [MethodImpl(0x8000)]
            private TypeReference LookupTypeForwarded(string @namespace, string name)
            {
            }
        }
    }
}

