﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ArrayType : TypeSpecification
    {
        private Collection<ArrayDimension> dimensions;

        [MethodImpl(0x8000)]
        public ArrayType(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public ArrayType(TypeReference type, int rank)
        {
        }

        public Collection<ArrayDimension> Dimensions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int Rank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsVector
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsValueType
        {
            get => 
                false;
            set
            {
                throw new InvalidOperationException();
            }
        }

        public override string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private string Suffix
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsArray =>
            true;
    }
}

