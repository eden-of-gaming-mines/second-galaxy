﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal sealed class BlobHeap : Heap
    {
        public BlobHeap(byte[] data) : base(data)
        {
        }

        [MethodImpl(0x8000)]
        public void GetView(uint signature, out byte[] buffer, out int index, out int length)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] Read(uint index)
        {
        }
    }
}

