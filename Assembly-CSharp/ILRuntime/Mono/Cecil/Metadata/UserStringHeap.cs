﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class UserStringHeap : StringHeap
    {
        public UserStringHeap(byte[] data) : base(data)
        {
        }

        [MethodImpl(0x8000)]
        protected override string ReadStringAt(uint index)
        {
        }
    }
}

