﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;

    internal abstract class Heap
    {
        public int IndexSize;
        internal readonly byte[] data;

        protected Heap(byte[] data)
        {
            this.data = data;
        }
    }
}

