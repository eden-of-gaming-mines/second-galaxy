﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal sealed class TableHeap : Heap
    {
        public long Valid;
        public long Sorted;
        public readonly TableInformation[] Tables;

        [MethodImpl(0x8000)]
        public TableHeap(byte[] data)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasTable(Table table)
        {
        }

        public TableInformation this[Table table]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

