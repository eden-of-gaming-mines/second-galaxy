﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class GuidHeap : Heap
    {
        public GuidHeap(byte[] data) : base(data)
        {
        }

        [MethodImpl(0x8000)]
        public Guid Read(uint index)
        {
        }
    }
}

