﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct Row<T1, T2, T3>
    {
        internal T1 Col1;
        internal T2 Col2;
        internal T3 Col3;
        [MethodImpl(0x8000)]
        public Row(T1 col1, T2 col2, T3 col3)
        {
        }
    }
}

