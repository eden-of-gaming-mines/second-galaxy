﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal sealed class RowEqualityComparer : IEqualityComparer<Row<string, string>>, IEqualityComparer<Row<uint, uint>>, IEqualityComparer<Row<uint, uint, uint>>
    {
        [MethodImpl(0x8000)]
        public bool Equals(Row<string, string> x, Row<string, string> y)
        {
        }

        [MethodImpl(0x8000)]
        public bool Equals(Row<uint, uint> x, Row<uint, uint> y)
        {
        }

        [MethodImpl(0x8000)]
        public bool Equals(Row<uint, uint, uint> x, Row<uint, uint, uint> y)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHashCode(Row<string, string> obj)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHashCode(Row<uint, uint> obj)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHashCode(Row<uint, uint, uint> obj)
        {
        }
    }
}

