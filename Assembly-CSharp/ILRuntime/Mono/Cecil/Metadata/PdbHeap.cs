﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class PdbHeap : Heap
    {
        public byte[] Id;
        public uint EntryPoint;
        public long TypeSystemTables;
        public uint[] TypeSystemTableRows;

        public PdbHeap(byte[] data) : base(data)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasTable(Table table)
        {
        }
    }
}

