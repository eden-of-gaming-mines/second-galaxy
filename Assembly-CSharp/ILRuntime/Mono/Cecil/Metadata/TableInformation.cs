﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct TableInformation
    {
        public uint Offset;
        public uint Length;
        public uint RowSize;
        public bool IsLarge =>
            (this.Length > 0xffff);
    }
}

