﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class StringHeap : Heap
    {
        private readonly Dictionary<uint, string> strings;

        [MethodImpl(0x8000)]
        public StringHeap(byte[] data)
        {
        }

        [MethodImpl(0x8000)]
        public string Read(uint index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string ReadStringAt(uint index)
        {
        }
    }
}

