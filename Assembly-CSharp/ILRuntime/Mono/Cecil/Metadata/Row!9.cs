﻿namespace ILRuntime.Mono.Cecil.Metadata
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct Row<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {
        internal T1 Col1;
        internal T2 Col2;
        internal T3 Col3;
        internal T4 Col4;
        internal T5 Col5;
        internal T6 Col6;
        internal T7 Col7;
        internal T8 Col8;
        internal T9 Col9;
        [MethodImpl(0x8000)]
        public Row(T1 col1, T2 col2, T3 col3, T4 col4, T5 col5, T6 col6, T7 col7, T8 col8, T9 col9)
        {
        }
    }
}

