﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct CustomAttributeNamedArgument
    {
        private readonly string name;
        private readonly CustomAttributeArgument argument;
        [MethodImpl(0x8000)]
        public CustomAttributeNamedArgument(string name, CustomAttributeArgument argument)
        {
        }

        public string Name =>
            this.name;
        public CustomAttributeArgument Argument =>
            this.argument;
    }
}

