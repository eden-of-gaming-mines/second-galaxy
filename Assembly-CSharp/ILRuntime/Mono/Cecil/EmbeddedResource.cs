﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public sealed class EmbeddedResource : Resource
    {
        private readonly MetadataReader reader;
        private uint? offset;
        private byte[] data;
        private Stream stream;

        [MethodImpl(0x8000)]
        public EmbeddedResource(string name, ManifestResourceAttributes attributes, byte[] data)
        {
        }

        [MethodImpl(0x8000)]
        public EmbeddedResource(string name, ManifestResourceAttributes attributes, Stream stream)
        {
        }

        [MethodImpl(0x8000)]
        internal EmbeddedResource(string name, ManifestResourceAttributes attributes, uint offset, MetadataReader reader)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetResourceData()
        {
        }

        [MethodImpl(0x8000)]
        public Stream GetResourceStream()
        {
        }

        [MethodImpl(0x8000)]
        private static byte[] ReadStream(Stream stream)
        {
        }

        public override ILRuntime.Mono.Cecil.ResourceType ResourceType =>
            ILRuntime.Mono.Cecil.ResourceType.Embedded;
    }
}

