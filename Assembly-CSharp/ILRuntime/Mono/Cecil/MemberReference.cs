﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class MemberReference : IMetadataTokenProvider
    {
        private string name;
        private TypeReference declaring_type;
        internal ILRuntime.Mono.Cecil.MetadataToken token;
        internal object projection;

        internal MemberReference()
        {
        }

        [MethodImpl(0x8000)]
        internal MemberReference(string name)
        {
        }

        [MethodImpl(0x8000)]
        internal string MemberFullName()
        {
        }

        public IMemberDefinition Resolve() => 
            this.ResolveDefinition();

        protected abstract IMemberDefinition ResolveDefinition();
        public override string ToString() => 
            this.FullName;

        public virtual string Name
        {
            get => 
                this.name;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public abstract string FullName { get; }

        public virtual TypeReference DeclaringType
        {
            get => 
                this.declaring_type;
            set => 
                (this.declaring_type = value);
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.token;
            set => 
                (this.token = value);
        }

        public bool IsWindowsRuntimeProjection =>
            (this.projection != null);

        internal MemberReferenceProjection WindowsRuntimeProjection
        {
            get => 
                ((MemberReferenceProjection) this.projection);
            set => 
                (this.projection = value);
        }

        internal bool HasImage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual ModuleDefinition Module
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual bool IsDefinition =>
            false;

        public virtual bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

