﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public class MetadataResolver : IMetadataResolver
    {
        private readonly IAssemblyResolver assembly_resolver;

        [MethodImpl(0x8000)]
        public MetadataResolver(IAssemblyResolver assemblyResolver)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AreSame(ArrayType a, ArrayType b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AreSame(GenericInstanceType a, GenericInstanceType b)
        {
        }

        private static bool AreSame(GenericParameter a, GenericParameter b) => 
            (a.Position == b.Position);

        [MethodImpl(0x8000)]
        private static bool AreSame(IModifierType a, IModifierType b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AreSame(TypeReference a, TypeReference b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AreSame(TypeSpecification a, TypeSpecification b)
        {
        }

        [MethodImpl(0x8000)]
        private static bool AreSame(Collection<ParameterDefinition> a, Collection<ParameterDefinition> b)
        {
        }

        [MethodImpl(0x8000)]
        private FieldDefinition GetField(TypeDefinition type, FieldReference reference)
        {
        }

        [MethodImpl(0x8000)]
        private static FieldDefinition GetField(Collection<FieldDefinition> fields, FieldReference reference)
        {
        }

        [MethodImpl(0x8000)]
        private MethodDefinition GetMethod(TypeDefinition type, MethodReference reference)
        {
        }

        [MethodImpl(0x8000)]
        public static MethodDefinition GetMethod(Collection<MethodDefinition> methods, MethodReference reference)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeDefinition GetType(ModuleDefinition module, TypeReference reference)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeDefinition GetTypeDefinition(ModuleDefinition module, TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsVarArgCallTo(MethodDefinition method, MethodReference reference)
        {
        }

        [MethodImpl(0x8000)]
        public virtual FieldDefinition Resolve(FieldReference field)
        {
        }

        [MethodImpl(0x8000)]
        public virtual MethodDefinition Resolve(MethodReference method)
        {
        }

        [MethodImpl(0x8000)]
        public virtual TypeDefinition Resolve(TypeReference type)
        {
        }

        public IAssemblyResolver AssemblyResolver =>
            this.assembly_resolver;
    }
}

