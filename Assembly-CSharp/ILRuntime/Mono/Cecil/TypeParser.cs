﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;

    internal class TypeParser
    {
        private readonly string fullname;
        private readonly int length;
        private int position;

        [MethodImpl(0x8000)]
        private TypeParser(string fullname)
        {
        }

        private static void Add<T>(ref T[] array, T item)
        {
            array = array.Add<T>(item);
        }

        [MethodImpl(0x8000)]
        private static void AdjustGenericParameters(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static void AppendNamePart(string part, StringBuilder name)
        {
        }

        [MethodImpl(0x8000)]
        private static void AppendType(TypeReference type, StringBuilder name, bool fq_name, bool top_level)
        {
        }

        [MethodImpl(0x8000)]
        private static void AppendTypeSpecification(TypeSpecification type, StringBuilder name)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeReference CreateReference(Type type_info, ModuleDefinition module, IMetadataScope scope)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeReference CreateSpecs(TypeReference type, Type type_info)
        {
        }

        [MethodImpl(0x8000)]
        private static IMetadataScope GetMetadataScope(ModuleDefinition module, Type type_info)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetScopeFullName(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeReference GetTypeReference(ModuleDefinition module, Type type_info, bool type_def_only)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsDelimiter(char chr)
        {
        }

        [MethodImpl(0x8000)]
        private string ParseAssemblyName()
        {
        }

        [MethodImpl(0x8000)]
        private Type[] ParseGenericArguments(int arity)
        {
        }

        private static bool ParseInt32(string value, out int result) => 
            int.TryParse(value, out result);

        [MethodImpl(0x8000)]
        private string[] ParseNestedNames()
        {
        }

        [MethodImpl(0x8000)]
        private string ParsePart()
        {
        }

        [MethodImpl(0x8000)]
        private int[] ParseSpecs()
        {
        }

        [MethodImpl(0x8000)]
        private Type ParseType(bool fq_name)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeReference ParseType(ModuleDefinition module, string fullname, bool typeDefinitionOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        private static bool RequiresFullyQualifiedName(TypeReference type, bool top_level)
        {
        }

        [MethodImpl(0x8000)]
        public static void SplitFullName(string fullname, out string @namespace, out string name)
        {
        }

        [MethodImpl(0x8000)]
        public static string ToParseable(TypeReference type, bool top_level = true)
        {
        }

        [MethodImpl(0x8000)]
        private static void TryAddArity(string name, ref int arity)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeReference TryCreateGenericInstanceType(TypeReference type, Type type_info)
        {
        }

        [MethodImpl(0x8000)]
        private static bool TryCurrentModule(ModuleDefinition module, Type type_info)
        {
        }

        [MethodImpl(0x8000)]
        private static bool TryGetArity(Type type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool TryGetArity(string name, out int arity)
        {
        }

        [MethodImpl(0x8000)]
        private static bool TryGetDefinition(ModuleDefinition module, Type type_info, out TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private bool TryParse(char chr)
        {
        }

        [MethodImpl(0x8000)]
        private void TryParseWhiteSpace()
        {
        }

        private class Type
        {
            public const int Ptr = -1;
            public const int ByRef = -2;
            public const int SzArray = -3;
            public string type_fullname;
            public string[] nested_names;
            public int arity;
            public int[] specs;
            public TypeParser.Type[] generic_arguments;
            public string assembly;
        }
    }
}

