﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono;
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.PE;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class ModuleDefinition : ModuleReference, ICustomAttributeProvider, ICustomDebugInformationProvider, IDisposable, IMetadataTokenProvider
    {
        internal ILRuntime.Mono.Cecil.PE.Image Image;
        internal ILRuntime.Mono.Cecil.MetadataSystem MetadataSystem;
        internal ILRuntime.Mono.Cecil.ReadingMode ReadingMode;
        internal ISymbolReaderProvider SymbolReaderProvider;
        internal ISymbolReader symbol_reader;
        internal Disposable<IAssemblyResolver> assembly_resolver;
        internal IMetadataResolver metadata_resolver;
        internal ILRuntime.Mono.Cecil.TypeSystem type_system;
        internal readonly MetadataReader reader;
        private readonly string file_name;
        internal string runtime_version;
        internal ModuleKind kind;
        private WindowsRuntimeProjections projections;
        private ILRuntime.Mono.Cecil.MetadataKind metadata_kind;
        private TargetRuntime runtime;
        private TargetArchitecture architecture;
        private ModuleAttributes attributes;
        private ModuleCharacteristics characteristics;
        internal ushort linker_version;
        private Guid mvid;
        internal uint timestamp;
        internal AssemblyDefinition assembly;
        private MethodDefinition entry_point;
        private Collection<CustomAttribute> custom_attributes;
        private Collection<AssemblyNameReference> references;
        private Collection<ModuleReference> modules;
        private Collection<Resource> resources;
        private Collection<ExportedType> exported_types;
        private TypeDefinitionCollection types;
        internal Collection<CustomDebugInformation> custom_infos;
        private readonly object module_lock;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, Collection<AssemblyNameReference>> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, Collection<ModuleReference>> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, Collection<Resource>> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, TypeDefinitionCollection> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, Collection<ExportedType>> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, MethodDefinition> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<Row<string, string>, MetadataReader, TypeReference> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, IEnumerable<TypeReference>> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, IEnumerable<MemberReference>> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<ModuleDefinition, MetadataReader, IEnumerable<CustomAttribute>> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<MetadataToken, MetadataReader, IMetadataTokenProvider> <>f__am$cacheB;

        [MethodImpl(0x8000)]
        internal ModuleDefinition()
        {
        }

        [MethodImpl(0x8000)]
        internal ModuleDefinition(ILRuntime.Mono.Cecil.PE.Image image)
        {
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<CustomAttribute> GetCustomAttributes()
        {
        }

        [MethodImpl(0x8000)]
        public ImageDebugHeader GetDebugHeader()
        {
        }

        [MethodImpl(0x8000)]
        private static Stream GetFileStream(string fileName, FileMode mode, FileAccess access, FileShare share)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<MemberReference> GetMemberReferences()
        {
        }

        [MethodImpl(0x8000)]
        private TypeDefinition GetNestedType(string fullname)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetType(string fullName)
        {
        }

        [MethodImpl(0x8000)]
        public TypeReference GetType(string fullName, bool runtimeName)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition GetType(string @namespace, string name)
        {
        }

        [MethodImpl(0x8000)]
        private TypeReference GetTypeReference(string scope, string fullname)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<TypeReference> GetTypeReferences()
        {
        }

        public IEnumerable<TypeDefinition> GetTypes() => 
            GetTypes(this.Types);

        [MethodImpl(0x8000), DebuggerHidden]
        private static IEnumerable<TypeDefinition> GetTypes(Collection<TypeDefinition> types)
        {
        }

        public bool HasTypeReference(string fullName) => 
            this.HasTypeReference(string.Empty, fullName);

        [MethodImpl(0x8000)]
        public bool HasTypeReference(string scope, string fullName)
        {
        }

        [MethodImpl(0x8000)]
        public IMetadataTokenProvider LookupToken(MetadataToken token)
        {
        }

        public IMetadataTokenProvider LookupToken(int token) => 
            this.LookupToken(new MetadataToken((uint) token));

        [MethodImpl(0x8000)]
        internal void Read<TItem>(TItem item, Action<TItem, MetadataReader> read)
        {
        }

        [MethodImpl(0x8000)]
        internal TRet Read<TItem, TRet>(TItem item, Func<TItem, MetadataReader, TRet> read)
        {
        }

        [MethodImpl(0x8000)]
        internal TRet Read<TItem, TRet>(ref TRet variable, TItem item, Func<TItem, MetadataReader, TRet> read) where TRet: class
        {
        }

        public static ModuleDefinition ReadModule(Stream stream) => 
            ReadModule(stream, new ReaderParameters(ILRuntime.Mono.Cecil.ReadingMode.Deferred));

        public static ModuleDefinition ReadModule(string fileName) => 
            ReadModule(fileName, new ReaderParameters(ILRuntime.Mono.Cecil.ReadingMode.Deferred));

        [MethodImpl(0x8000)]
        public static ModuleDefinition ReadModule(Stream stream, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        public static ModuleDefinition ReadModule(string fileName, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private static ModuleDefinition ReadModule(Disposable<Stream> stream, string fileName, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        public void ReadSymbols()
        {
        }

        public void ReadSymbols(ISymbolReader reader)
        {
            this.ReadSymbols(reader, true);
        }

        [MethodImpl(0x8000)]
        public void ReadSymbols(ISymbolReader reader, bool throwIfSymbolsAreNotMaching)
        {
        }

        internal FieldDefinition Resolve(FieldReference field) => 
            this.MetadataResolver.Resolve(field);

        internal MethodDefinition Resolve(MethodReference method) => 
            this.MetadataResolver.Resolve(method);

        internal TypeDefinition Resolve(TypeReference type) => 
            this.MetadataResolver.Resolve(type);

        public bool TryGetTypeReference(string fullName, out TypeReference type) => 
            this.TryGetTypeReference(string.Empty, fullName, out type);

        [MethodImpl(0x8000)]
        public bool TryGetTypeReference(string scope, string fullName, out TypeReference type)
        {
        }

        public bool IsMain =>
            (this.kind != ModuleKind.NetModule);

        public ModuleKind Kind
        {
            get => 
                this.kind;
            set => 
                (this.kind = value);
        }

        public ILRuntime.Mono.Cecil.MetadataKind MetadataKind
        {
            get => 
                this.metadata_kind;
            set => 
                (this.metadata_kind = value);
        }

        internal WindowsRuntimeProjections Projections
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TargetRuntime Runtime
        {
            get => 
                this.runtime;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public string RuntimeVersion
        {
            get => 
                this.runtime_version;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TargetArchitecture Architecture
        {
            get => 
                this.architecture;
            set => 
                (this.architecture = value);
        }

        public ModuleAttributes Attributes
        {
            get => 
                this.attributes;
            set => 
                (this.attributes = value);
        }

        public ModuleCharacteristics Characteristics
        {
            get => 
                this.characteristics;
            set => 
                (this.characteristics = value);
        }

        [Obsolete("Use FileName")]
        public string FullyQualifiedName =>
            this.file_name;

        public string FileName =>
            this.file_name;

        public Guid Mvid
        {
            get => 
                this.mvid;
            set => 
                (this.mvid = value);
        }

        internal bool HasImage =>
            !ReferenceEquals(this.Image, null);

        public bool HasSymbols =>
            !ReferenceEquals(this.symbol_reader, null);

        public ISymbolReader SymbolReader =>
            this.symbol_reader;

        public override ILRuntime.Mono.Cecil.MetadataScopeType MetadataScopeType =>
            ILRuntime.Mono.Cecil.MetadataScopeType.ModuleDefinition;

        public AssemblyDefinition Assembly =>
            this.assembly;

        public IAssemblyResolver AssemblyResolver
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IMetadataResolver MetadataResolver
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.TypeSystem TypeSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasAssemblyReferences
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<AssemblyNameReference> AssemblyReferences
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasModuleReferences
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<ModuleReference> ModuleReferences
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasResources
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<Resource> Resources
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<TypeDefinition> Types
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasExportedTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<ExportedType> ExportedTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public MethodDefinition EntryPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.entry_point = value);
        }

        public bool HasCustomDebugInformations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomDebugInformation> CustomDebugInformations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal object SyncRoot =>
            this.module_lock;

        public bool HasDebugHeader
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetTypes>c__Iterator0 : IEnumerable, IEnumerable<TypeDefinition>, IEnumerator, IDisposable, IEnumerator<TypeDefinition>
        {
            internal int <i>__1;
            internal Collection<TypeDefinition> types;
            internal TypeDefinition <type>__2;
            internal IEnumerator<TypeDefinition> $locvar0;
            internal TypeDefinition <nested>__3;
            internal TypeDefinition $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<TypeDefinition> IEnumerable<TypeDefinition>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<ILRuntime.Mono.Cecil.TypeDefinition>.GetEnumerator();

            TypeDefinition IEnumerator<TypeDefinition>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

