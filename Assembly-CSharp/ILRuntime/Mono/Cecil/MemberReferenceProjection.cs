﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class MemberReferenceProjection
    {
        public readonly string Name;
        public readonly MemberReferenceTreatment Treatment;

        [MethodImpl(0x8000)]
        public MemberReferenceProjection(MemberReference member, MemberReferenceTreatment treatment)
        {
        }
    }
}

