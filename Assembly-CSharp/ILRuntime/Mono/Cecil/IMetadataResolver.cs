﻿namespace ILRuntime.Mono.Cecil
{
    public interface IMetadataResolver
    {
        FieldDefinition Resolve(FieldReference field);
        MethodDefinition Resolve(MethodReference method);
        TypeDefinition Resolve(TypeReference type);
    }
}

