﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct CustomAttributeArgument
    {
        private readonly TypeReference type;
        private readonly object value;
        [MethodImpl(0x8000)]
        public CustomAttributeArgument(TypeReference type, object value)
        {
        }

        public TypeReference Type =>
            this.type;
        public object Value =>
            this.value;
    }
}

