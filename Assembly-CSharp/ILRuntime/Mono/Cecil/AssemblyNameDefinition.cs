﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AssemblyNameDefinition : AssemblyNameReference
    {
        [MethodImpl(0x8000)]
        internal AssemblyNameDefinition()
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyNameDefinition(string name, Version version)
        {
        }

        public override byte[] Hash =>
            Empty<byte>.Array;
    }
}

