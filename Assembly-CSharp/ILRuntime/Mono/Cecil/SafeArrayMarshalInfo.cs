﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public sealed class SafeArrayMarshalInfo : MarshalInfo
    {
        internal VariantType element_type;

        public VariantType ElementType
        {
            get => 
                this.element_type;
            set => 
                (this.element_type = value);
        }
    }
}

