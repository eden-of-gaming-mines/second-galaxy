﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public sealed class AssemblyDefinition : ICustomAttributeProvider, ISecurityDeclarationProvider, IDisposable, IMetadataTokenProvider
    {
        private AssemblyNameDefinition name;
        internal ModuleDefinition main_module;
        private Collection<ModuleDefinition> modules;
        private Collection<CustomAttribute> custom_attributes;
        private Collection<SecurityDeclaration> security_declarations;
        [CompilerGenerated]
        private static Func<AssemblyDefinition, MetadataReader, Collection<ModuleDefinition>> <>f__am$cache0;

        internal AssemblyDefinition()
        {
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }

        [MethodImpl(0x8000)]
        private static AssemblyDefinition ReadAssembly(ModuleDefinition module)
        {
        }

        public static AssemblyDefinition ReadAssembly(Stream stream) => 
            ReadAssembly(ModuleDefinition.ReadModule(stream));

        public static AssemblyDefinition ReadAssembly(string fileName) => 
            ReadAssembly(ModuleDefinition.ReadModule(fileName));

        public static AssemblyDefinition ReadAssembly(Stream stream, ReaderParameters parameters) => 
            ReadAssembly(ModuleDefinition.ReadModule(stream, parameters));

        public static AssemblyDefinition ReadAssembly(string fileName, ReaderParameters parameters) => 
            ReadAssembly(ModuleDefinition.ReadModule(fileName, parameters));

        public override string ToString() => 
            this.FullName;

        public AssemblyNameDefinition Name
        {
            get => 
                this.name;
            set => 
                (this.name = value);
        }

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                new ILRuntime.Mono.Cecil.MetadataToken(TokenType.Assembly, 1);
            set
            {
            }
        }

        public Collection<ModuleDefinition> Modules
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ModuleDefinition MainModule =>
            this.main_module;

        public MethodDefinition EntryPoint
        {
            get => 
                this.main_module.EntryPoint;
            set => 
                (this.main_module.EntryPoint = value);
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasSecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<SecurityDeclaration> SecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

