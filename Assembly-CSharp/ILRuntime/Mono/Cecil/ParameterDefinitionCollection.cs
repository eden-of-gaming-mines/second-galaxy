﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class ParameterDefinitionCollection : Collection<ParameterDefinition>
    {
        private readonly IMethodSignature method;

        internal ParameterDefinitionCollection(IMethodSignature method)
        {
            this.method = method;
        }

        internal ParameterDefinitionCollection(IMethodSignature method, int capacity) : base(capacity)
        {
            this.method = method;
        }

        [MethodImpl(0x8000)]
        protected override void OnAdd(ParameterDefinition item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnInsert(ParameterDefinition item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnRemove(ParameterDefinition item, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSet(ParameterDefinition item, int index)
        {
        }
    }
}

