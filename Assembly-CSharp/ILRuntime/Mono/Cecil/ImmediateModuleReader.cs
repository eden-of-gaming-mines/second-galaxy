﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Cecil.PE;
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class ImmediateModuleReader : ModuleReader
    {
        private bool resolve_attributes;

        public ImmediateModuleReader(Image image) : base(image, ReadingMode.Immediate)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadCustomAttributes(ICustomAttributeProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadEvents(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadFields(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadGenericParameters(IGenericParameterProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadInterfaces(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMethods(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMethodsSymbols(TypeDefinition type, ISymbolReader symbol_reader)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ReadModule()
        {
        }

        [MethodImpl(0x8000)]
        public void ReadModule(ModuleDefinition module, bool resolve_attributes)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadParameters(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadProperties(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSecurityDeclarations(ISecurityDeclarationProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public override void ReadSymbols(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadType(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadTypes(Collection<TypeDefinition> types)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadTypesSymbols(Collection<TypeDefinition> types, ISymbolReader symbol_reader)
        {
        }
    }
}

