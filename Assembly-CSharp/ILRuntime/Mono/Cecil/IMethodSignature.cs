﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface IMethodSignature : IMetadataTokenProvider
    {
        bool HasThis { get; set; }

        bool ExplicitThis { get; set; }

        MethodCallingConvention CallingConvention { get; set; }

        bool HasParameters { get; }

        Collection<ParameterDefinition> Parameters { get; }

        TypeReference ReturnType { get; set; }

        ILRuntime.Mono.Cecil.MethodReturnType MethodReturnType { get; }
    }
}

