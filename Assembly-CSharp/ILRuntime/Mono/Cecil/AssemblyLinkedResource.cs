﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AssemblyLinkedResource : Resource
    {
        private AssemblyNameReference reference;

        public AssemblyLinkedResource(string name, ManifestResourceAttributes flags) : base(name, flags)
        {
        }

        [MethodImpl(0x8000)]
        public AssemblyLinkedResource(string name, ManifestResourceAttributes flags, AssemblyNameReference reference)
        {
        }

        public AssemblyNameReference Assembly
        {
            get => 
                this.reference;
            set => 
                (this.reference = value);
        }

        public override ILRuntime.Mono.Cecil.ResourceType ResourceType =>
            ILRuntime.Mono.Cecil.ResourceType.AssemblyLinked;
    }
}

