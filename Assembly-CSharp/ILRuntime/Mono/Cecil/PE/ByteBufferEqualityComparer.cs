﻿namespace ILRuntime.Mono.Cecil.PE
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal sealed class ByteBufferEqualityComparer : IEqualityComparer<ByteBuffer>
    {
        [MethodImpl(0x8000)]
        public bool Equals(ByteBuffer x, ByteBuffer y)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHashCode(ByteBuffer buffer)
        {
        }
    }
}

