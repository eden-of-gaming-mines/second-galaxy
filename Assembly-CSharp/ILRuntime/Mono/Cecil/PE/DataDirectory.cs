﻿namespace ILRuntime.Mono.Cecil.PE
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct DataDirectory
    {
        public readonly uint VirtualAddress;
        public readonly uint Size;
        public DataDirectory(uint rva, uint size)
        {
            this.VirtualAddress = rva;
            this.Size = size;
        }

        public bool IsZero
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

