﻿namespace ILRuntime.Mono.Cecil.PE
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    internal class BinaryStreamReader : BinaryReader
    {
        public BinaryStreamReader(Stream stream) : base(stream)
        {
        }

        [MethodImpl(0x8000)]
        public void Advance(int bytes)
        {
        }

        [MethodImpl(0x8000)]
        public void Align(int align)
        {
        }

        [MethodImpl(0x8000)]
        public void MoveTo(uint position)
        {
        }

        [MethodImpl(0x8000)]
        public DataDirectory ReadDataDirectory()
        {
        }

        public int Position
        {
            get => 
                ((int) this.BaseStream.Position);
            set => 
                (this.BaseStream.Position = value);
        }

        public int Length =>
            ((int) this.BaseStream.Length);
    }
}

