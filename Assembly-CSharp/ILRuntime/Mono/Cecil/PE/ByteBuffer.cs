﻿namespace ILRuntime.Mono.Cecil.PE
{
    using System;
    using System.Runtime.CompilerServices;

    internal class ByteBuffer
    {
        internal byte[] buffer;
        internal int length;
        internal int position;

        [MethodImpl(0x8000)]
        public ByteBuffer()
        {
        }

        [MethodImpl(0x8000)]
        public ByteBuffer(int length)
        {
        }

        [MethodImpl(0x8000)]
        public ByteBuffer(byte[] buffer)
        {
        }

        public void Advance(int length)
        {
            this.position += length;
        }

        [MethodImpl(0x8000)]
        public byte ReadByte()
        {
        }

        [MethodImpl(0x8000)]
        public byte[] ReadBytes(int length)
        {
        }

        [MethodImpl(0x8000)]
        public int ReadCompressedInt32()
        {
        }

        [MethodImpl(0x8000)]
        public uint ReadCompressedUInt32()
        {
        }

        [MethodImpl(0x8000)]
        public double ReadDouble()
        {
        }

        public short ReadInt16() => 
            ((short) this.ReadUInt16());

        public int ReadInt32() => 
            ((int) this.ReadUInt32());

        public long ReadInt64() => 
            ((long) this.ReadUInt64());

        public sbyte ReadSByte() => 
            ((sbyte) this.ReadByte());

        [MethodImpl(0x8000)]
        public float ReadSingle()
        {
        }

        [MethodImpl(0x8000)]
        public ushort ReadUInt16()
        {
        }

        [MethodImpl(0x8000)]
        public uint ReadUInt32()
        {
        }

        [MethodImpl(0x8000)]
        public ulong ReadUInt64()
        {
        }
    }
}

