﻿namespace ILRuntime.Mono.Cecil.PE
{
    using ILRuntime.Mono;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.Metadata;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal sealed class ImageReader : BinaryStreamReader
    {
        private readonly Image image;
        private DataDirectory cli;
        private DataDirectory metadata;
        private uint table_heap_offset;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map0;

        [MethodImpl(0x8000)]
        public ImageReader(Disposable<Stream> stream, string file_name)
        {
        }

        [MethodImpl(0x8000)]
        private void ComputeTableInformations()
        {
        }

        private int GetCodedIndexSize(CodedIndex index) => 
            this.image.GetCodedIndexSize(index);

        [MethodImpl(0x8000)]
        private static ModuleKind GetModuleKind(ushort characteristics, ushort subsystem)
        {
        }

        private int GetTableIndexSize(Table table) => 
            this.image.GetTableIndexSize(table);

        [MethodImpl(0x8000)]
        private void MoveTo(DataDirectory directory)
        {
        }

        [MethodImpl(0x8000)]
        private string ReadAlignedString(int length)
        {
        }

        private TargetArchitecture ReadArchitecture() => 
            ((TargetArchitecture) this.ReadUInt16());

        [MethodImpl(0x8000)]
        private void ReadCLIHeader()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadDebugHeader()
        {
        }

        [MethodImpl(0x8000)]
        private byte[] ReadHeapData(uint offset, uint size)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadImage()
        {
        }

        [MethodImpl(0x8000)]
        public static Image ReadImage(Disposable<Stream> stream, string file_name)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMetadata()
        {
        }

        [MethodImpl(0x8000)]
        private void ReadMetadataStream(Section section)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadOptionalHeaders(out ushort subsystem, out ushort dll_characteristics, out ushort linker)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadPdbHeap()
        {
        }

        [MethodImpl(0x8000)]
        public static Image ReadPortablePdb(Disposable<Stream> stream, string file_name)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadSections(ushort count)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadTableHeap()
        {
        }

        [MethodImpl(0x8000)]
        private string ReadZeroTerminatedString(int length)
        {
        }

        [MethodImpl(0x8000)]
        private static void SetIndexSize(Heap heap, uint sizes, byte flag)
        {
        }
    }
}

