﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.PE;
    using System;
    using System.Runtime.CompilerServices;

    internal abstract class ModuleReader
    {
        protected readonly ModuleDefinition module;

        [MethodImpl(0x8000)]
        protected ModuleReader(Image image, ReadingMode mode)
        {
        }

        [MethodImpl(0x8000)]
        public static ModuleDefinition CreateModule(Image image, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private static ModuleReader CreateModuleReader(Image image, ReadingMode mode)
        {
        }

        [MethodImpl(0x8000)]
        private static void GetMetadataKind(ModuleDefinition module, ReaderParameters parameters)
        {
        }

        [MethodImpl(0x8000)]
        private void ReadAssembly(MetadataReader reader)
        {
        }

        protected abstract void ReadModule();
        [MethodImpl(0x8000)]
        protected void ReadModuleManifest(MetadataReader reader)
        {
        }

        public abstract void ReadSymbols(ModuleDefinition module);
        [MethodImpl(0x8000)]
        private static void ReadSymbols(ModuleDefinition module, ReaderParameters parameters)
        {
        }
    }
}

