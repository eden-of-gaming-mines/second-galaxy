﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public sealed class FixedSysStringMarshalInfo : MarshalInfo
    {
        internal int size;

        public int Size
        {
            get => 
                this.size;
            set => 
                (this.size = value);
        }
    }
}

