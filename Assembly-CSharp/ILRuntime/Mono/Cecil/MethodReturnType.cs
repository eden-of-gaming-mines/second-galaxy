﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MethodReturnType : IConstantProvider, ICustomAttributeProvider, IMarshalInfoProvider, IMetadataTokenProvider
    {
        internal IMethodSignature method;
        internal ParameterDefinition parameter;
        private TypeReference return_type;

        public MethodReturnType(IMethodSignature method)
        {
            this.method = method;
        }

        public IMethodSignature Method =>
            this.method;

        public TypeReference ReturnType
        {
            get => 
                this.return_type;
            set => 
                (this.return_type = value);
        }

        internal ParameterDefinition Parameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MetadataToken MetadataToken
        {
            get => 
                this.Parameter.MetadataToken;
            set => 
                (this.Parameter.MetadataToken = value);
        }

        public ParameterAttributes Attributes
        {
            get => 
                this.Parameter.Attributes;
            set => 
                (this.Parameter.Attributes = value);
        }

        public string Name
        {
            get => 
                this.Parameter.Name;
            set => 
                (this.Parameter.Name = value);
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes =>
            this.Parameter.CustomAttributes;

        public bool HasDefault
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.Parameter.HasDefault = value);
        }

        public bool HasConstant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.Parameter.HasConstant = value);
        }

        public object Constant
        {
            get => 
                this.Parameter.Constant;
            set => 
                (this.Parameter.Constant = value);
        }

        public bool HasFieldMarshal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.Parameter.HasFieldMarshal = value);
        }

        public bool HasMarshalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MarshalInfo MarshalInfo
        {
            get => 
                this.Parameter.MarshalInfo;
            set => 
                (this.Parameter.MarshalInfo = value);
        }
    }
}

