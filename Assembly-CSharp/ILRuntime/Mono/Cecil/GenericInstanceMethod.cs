﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class GenericInstanceMethod : MethodSpecification, IGenericInstance, IGenericContext, IMetadataTokenProvider
    {
        private Collection<TypeReference> arguments;

        public GenericInstanceMethod(MethodReference method) : base(method)
        {
        }

        IGenericParameterProvider IGenericContext.Method =>
            base.ElementMethod;

        IGenericParameterProvider IGenericContext.Type =>
            base.ElementMethod.DeclaringType;

        public bool HasGenericArguments =>
            !this.arguments.IsNullOrEmpty<TypeReference>();

        public Collection<TypeReference> GenericArguments
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsGenericInstance =>
            true;

        public override bool ContainsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

