﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum ResourceType
    {
        Linked,
        Embedded,
        AssemblyLinked
    }
}

