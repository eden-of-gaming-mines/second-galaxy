﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class TypeReferenceProjection
    {
        public readonly string Name;
        public readonly string Namespace;
        public readonly IMetadataScope Scope;
        public readonly TypeReferenceTreatment Treatment;

        [MethodImpl(0x8000)]
        public TypeReferenceProjection(TypeReference type, TypeReferenceTreatment treatment)
        {
        }
    }
}

