﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [DebuggerDisplay("{AttributeType}")]
    public sealed class CustomAttribute : ICustomAttribute
    {
        internal CustomAttributeValueProjection projection;
        internal readonly uint signature;
        internal bool resolved;
        private MethodReference constructor;
        private byte[] blob;
        internal Collection<CustomAttributeArgument> arguments;
        internal Collection<CustomAttributeNamedArgument> fields;
        internal Collection<CustomAttributeNamedArgument> properties;
        [CompilerGenerated]
        private static Func<CustomAttribute, MetadataReader, byte[]> <>f__am$cache0;

        [MethodImpl(0x8000)]
        public CustomAttribute(MethodReference constructor)
        {
        }

        [MethodImpl(0x8000)]
        public CustomAttribute(MethodReference constructor, byte[] blob)
        {
        }

        [MethodImpl(0x8000)]
        internal CustomAttribute(uint signature, MethodReference constructor)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetBlob()
        {
        }

        [MethodImpl(0x8000)]
        private void Resolve()
        {
        }

        public MethodReference Constructor
        {
            get => 
                this.constructor;
            set => 
                (this.constructor = value);
        }

        public TypeReference AttributeType =>
            this.constructor.DeclaringType;

        public bool IsResolved =>
            this.resolved;

        public bool HasConstructorArguments
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttributeArgument> ConstructorArguments
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasFields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttributeNamedArgument> Fields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttributeNamedArgument> Properties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal bool HasImage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal ModuleDefinition Module =>
            this.constructor.Module;
    }
}

