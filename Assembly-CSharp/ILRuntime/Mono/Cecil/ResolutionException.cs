﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [Serializable]
    public sealed class ResolutionException : Exception
    {
        private readonly MemberReference member;

        [MethodImpl(0x8000)]
        public ResolutionException(MemberReference member)
        {
        }

        [MethodImpl(0x8000)]
        public ResolutionException(MemberReference member, Exception innerException)
        {
        }

        private ResolutionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public MemberReference Member =>
            this.member;

        public IMetadataScope Scope
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

