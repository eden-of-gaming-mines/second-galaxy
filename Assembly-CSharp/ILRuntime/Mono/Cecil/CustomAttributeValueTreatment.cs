﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    internal enum CustomAttributeValueTreatment
    {
        None,
        AllowSingle,
        AllowMultiple,
        VersionAttribute,
        DeprecatedAttribute
    }
}

