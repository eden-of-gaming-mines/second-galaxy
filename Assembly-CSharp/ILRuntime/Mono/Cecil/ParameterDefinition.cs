﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ParameterDefinition : ParameterReference, ICustomAttributeProvider, IConstantProvider, IMarshalInfoProvider, IMetadataTokenProvider
    {
        private ushort attributes;
        internal IMethodSignature method;
        private object constant;
        private Collection<CustomAttribute> custom_attributes;
        private ILRuntime.Mono.Cecil.MarshalInfo marshal_info;

        public ParameterDefinition(TypeReference parameterType) : this(string.Empty, ParameterAttributes.None, parameterType)
        {
        }

        [MethodImpl(0x8000)]
        internal ParameterDefinition(TypeReference parameterType, IMethodSignature method)
        {
        }

        [MethodImpl(0x8000)]
        public ParameterDefinition(string name, ParameterAttributes attributes, TypeReference parameterType)
        {
        }

        public override ParameterDefinition Resolve() => 
            this;

        public ParameterAttributes Attributes
        {
            get => 
                ((ParameterAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public IMethodSignature Method =>
            this.method;

        public int Sequence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasConstant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public object Constant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.constant = value);
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasMarshalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MarshalInfo MarshalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.marshal_info = value);
        }

        public bool IsIn
        {
            get => 
                this.attributes.GetAttributes(1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsOut
        {
            get => 
                this.attributes.GetAttributes(2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsLcid
        {
            get => 
                this.attributes.GetAttributes(4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsReturnValue
        {
            get => 
                this.attributes.GetAttributes(8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsOptional
        {
            get => 
                this.attributes.GetAttributes(0x10);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasDefault
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasFieldMarshal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

