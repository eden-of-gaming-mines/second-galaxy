﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class MethodDefinitionProjection
    {
        public readonly MethodAttributes Attributes;
        public readonly MethodImplAttributes ImplAttributes;
        public readonly string Name;
        public readonly MethodDefinitionTreatment Treatment;

        [MethodImpl(0x8000)]
        public MethodDefinitionProjection(MethodDefinition method, MethodDefinitionTreatment treatment)
        {
        }
    }
}

