﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Cecil.Cil;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public sealed class ReaderParameters
    {
        private ILRuntime.Mono.Cecil.ReadingMode reading_mode;
        internal IAssemblyResolver assembly_resolver;
        internal IMetadataResolver metadata_resolver;
        private Stream symbol_stream;
        private ISymbolReaderProvider symbol_reader_provider;
        private bool read_symbols;
        private bool throw_symbols_mismatch;
        private bool projections;
        private bool in_memory;
        private bool read_write;

        public ReaderParameters() : this(ILRuntime.Mono.Cecil.ReadingMode.Deferred)
        {
        }

        [MethodImpl(0x8000)]
        public ReaderParameters(ILRuntime.Mono.Cecil.ReadingMode readingMode)
        {
        }

        public ILRuntime.Mono.Cecil.ReadingMode ReadingMode
        {
            get => 
                this.reading_mode;
            set => 
                (this.reading_mode = value);
        }

        public bool InMemory
        {
            get => 
                this.in_memory;
            set => 
                (this.in_memory = value);
        }

        public IAssemblyResolver AssemblyResolver
        {
            get => 
                this.assembly_resolver;
            set => 
                (this.assembly_resolver = value);
        }

        public IMetadataResolver MetadataResolver
        {
            get => 
                this.metadata_resolver;
            set => 
                (this.metadata_resolver = value);
        }

        public Stream SymbolStream
        {
            get => 
                this.symbol_stream;
            set => 
                (this.symbol_stream = value);
        }

        public ISymbolReaderProvider SymbolReaderProvider
        {
            get => 
                this.symbol_reader_provider;
            set => 
                (this.symbol_reader_provider = value);
        }

        public bool ReadSymbols
        {
            get => 
                this.read_symbols;
            set => 
                (this.read_symbols = value);
        }

        public bool ThrowIfSymbolsAreNotMatching
        {
            get => 
                this.throw_symbols_mismatch;
            set => 
                (this.throw_symbols_mismatch = value);
        }

        public bool ReadWrite
        {
            get => 
                this.read_write;
            set => 
                (this.read_write = value);
        }

        public bool ApplyWindowsRuntimeProjections
        {
            get => 
                this.projections;
            set => 
                (this.projections = value);
        }
    }
}

