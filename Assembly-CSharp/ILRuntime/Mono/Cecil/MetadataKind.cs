﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum MetadataKind
    {
        Ecma335,
        WindowsMetadata,
        ManagedWindowsMetadata
    }
}

