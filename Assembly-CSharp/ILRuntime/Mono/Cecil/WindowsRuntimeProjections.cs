﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal sealed class WindowsRuntimeProjections
    {
        private static readonly Version version;
        private static readonly byte[] contract_pk_token;
        private static readonly byte[] contract_pk;
        private static Dictionary<string, ProjectionInfo> projections;
        private readonly ModuleDefinition module;
        private Version corlib_version;
        private AssemblyNameReference[] virtual_references;

        [MethodImpl(0x8000)]
        public WindowsRuntimeProjections(ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public void AddVirtualReferences(Collection<AssemblyNameReference> references)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(CustomAttribute attribute, CustomAttributeValueProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(FieldDefinition field, FieldDefinitionProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(MemberReference member, MemberReferenceProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(MethodDefinition method, MethodDefinitionProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(TypeDefinition type, TypeDefinitionProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        public static void ApplyProjection(TypeReference type, TypeReferenceProjection projection)
        {
        }

        [MethodImpl(0x8000)]
        private AssemblyNameReference GetAssemblyReference(string name)
        {
        }

        [MethodImpl(0x8000)]
        private static AssemblyNameReference[] GetAssemblyReferences(AssemblyNameReference corlib)
        {
        }

        [MethodImpl(0x8000)]
        private static AssemblyNameReference GetCoreLibrary(Collection<AssemblyNameReference> references)
        {
        }

        [MethodImpl(0x8000)]
        private static MethodDefinitionTreatment GetMethodDefinitionTreatmentFromCustomAttributes(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeReferenceTreatment GetSpecialTypeReferenceTreatment(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static TypeDefinitionTreatment GetWellKnownTypeDefinitionTreatment(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool HasAttribute(TypeDefinition type, string @namespace, string name)
        {
        }

        [MethodImpl(0x8000)]
        private static bool ImplementsRedirectedInterface(MemberReference member, out bool disposable)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsAttribute(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsClrImplementationType(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsEnum(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsWindowsAttributeUsageAttribute(ICustomAttributeProvider owner, CustomAttribute attribute)
        {
        }

        [MethodImpl(0x8000)]
        private static bool NeedsWindowsRuntimePrefix(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(FieldDefinition field)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(MemberReference member)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public static void Project(ICustomAttributeProvider owner, CustomAttribute attribute)
        {
        }

        [MethodImpl(0x8000)]
        public static CustomAttributeValueProjection RemoveProjection(CustomAttribute attribute)
        {
        }

        [MethodImpl(0x8000)]
        public static FieldDefinitionProjection RemoveProjection(FieldDefinition field)
        {
        }

        [MethodImpl(0x8000)]
        public static MemberReferenceProjection RemoveProjection(MemberReference member)
        {
        }

        [MethodImpl(0x8000)]
        public static MethodDefinitionProjection RemoveProjection(MethodDefinition method)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeDefinitionProjection RemoveProjection(TypeDefinition type)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeReferenceProjection RemoveProjection(TypeReference type)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveVirtualReferences(Collection<AssemblyNameReference> references)
        {
        }

        private static Dictionary<string, ProjectionInfo> Projections
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private AssemblyNameReference[] VirtualReferences
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct ProjectionInfo
        {
            public readonly string WinRTNamespace;
            public readonly string ClrNamespace;
            public readonly string ClrName;
            public readonly string ClrAssembly;
            public readonly bool Attribute;
            public readonly bool Disposable;
            [MethodImpl(0x8000)]
            public ProjectionInfo(string winrt_namespace, string clr_namespace, string clr_name, string clr_assembly, bool attribute = false, bool disposable = false)
            {
            }
        }
    }
}

