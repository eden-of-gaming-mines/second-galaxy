﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public class MarshalInfo
    {
        internal ILRuntime.Mono.Cecil.NativeType native;

        public MarshalInfo(ILRuntime.Mono.Cecil.NativeType native)
        {
            this.native = native;
        }

        public ILRuntime.Mono.Cecil.NativeType NativeType
        {
            get => 
                this.native;
            set => 
                (this.native = value);
        }
    }
}

