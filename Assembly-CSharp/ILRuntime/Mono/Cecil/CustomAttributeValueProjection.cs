﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class CustomAttributeValueProjection
    {
        public readonly AttributeTargets Targets;
        public readonly CustomAttributeValueTreatment Treatment;

        [MethodImpl(0x8000)]
        public CustomAttributeValueProjection(AttributeTargets targets, CustomAttributeValueTreatment treatment)
        {
        }
    }
}

