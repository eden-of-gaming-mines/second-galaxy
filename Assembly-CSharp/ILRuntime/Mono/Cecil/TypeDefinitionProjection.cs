﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    internal sealed class TypeDefinitionProjection
    {
        public readonly TypeAttributes Attributes;
        public readonly string Name;
        public readonly TypeDefinitionTreatment Treatment;

        [MethodImpl(0x8000)]
        public TypeDefinitionProjection(TypeDefinition type, TypeDefinitionTreatment treatment)
        {
        }
    }
}

