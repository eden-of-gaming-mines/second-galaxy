﻿namespace ILRuntime.Mono.Cecil
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class PInvokeInfo
    {
        private ushort attributes;
        private string entry_point;
        private ModuleReference module;

        [MethodImpl(0x8000)]
        public PInvokeInfo(PInvokeAttributes attributes, string entryPoint, ModuleReference module)
        {
        }

        public PInvokeAttributes Attributes
        {
            get => 
                ((PInvokeAttributes) this.attributes);
            set => 
                (this.attributes = (ushort) value);
        }

        public string EntryPoint
        {
            get => 
                this.entry_point;
            set => 
                (this.entry_point = value);
        }

        public ModuleReference Module
        {
            get => 
                this.module;
            set => 
                (this.module = value);
        }

        public bool IsNoMangle
        {
            get => 
                this.attributes.GetAttributes(1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCharSetNotSpec
        {
            get => 
                this.attributes.GetMaskedAttributes(6, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCharSetAnsi
        {
            get => 
                this.attributes.GetMaskedAttributes(6, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCharSetUnicode
        {
            get => 
                this.attributes.GetMaskedAttributes(6, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCharSetAuto
        {
            get => 
                this.attributes.GetMaskedAttributes(6, 6);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool SupportsLastError
        {
            get => 
                this.attributes.GetAttributes(0x40);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCallConvWinapi
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCallConvCdecl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCallConvStdCall
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCallConvThiscall
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsCallConvFastcall
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsBestFitEnabled
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsBestFitDisabled
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsThrowOnUnmappableCharEnabled
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsThrowOnUnmappableCharDisabled
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

