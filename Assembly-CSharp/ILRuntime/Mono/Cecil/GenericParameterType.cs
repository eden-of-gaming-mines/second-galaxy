﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public enum GenericParameterType
    {
        Type,
        Method
    }
}

