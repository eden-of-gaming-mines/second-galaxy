﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class TypeDefinition : TypeReference, IMemberDefinition, ISecurityDeclarationProvider, ICustomAttributeProvider, IMetadataTokenProvider
    {
        private uint attributes;
        private TypeReference base_type;
        internal Range fields_range;
        internal Range methods_range;
        private short packing_size;
        private int class_size;
        private InterfaceImplementationCollection interfaces;
        private Collection<TypeDefinition> nested_types;
        private Collection<MethodDefinition> methods;
        private Collection<FieldDefinition> fields;
        private Collection<EventDefinition> events;
        private Collection<PropertyDefinition> properties;
        private Collection<CustomAttribute> custom_attributes;
        private Collection<SecurityDeclaration> security_declarations;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Row<short, int>> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, InterfaceImplementationCollection> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Collection<TypeDefinition>> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Collection<MethodDefinition>> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Collection<FieldDefinition>> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Collection<EventDefinition>> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, bool> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<TypeDefinition, MetadataReader, Collection<PropertyDefinition>> <>f__am$cacheA;

        [MethodImpl(0x8000)]
        public TypeDefinition(string @namespace, string name, TypeAttributes attributes)
        {
        }

        [MethodImpl(0x8000)]
        public TypeDefinition(string @namespace, string name, TypeAttributes attributes, TypeReference baseType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearFullName()
        {
        }

        public override TypeDefinition Resolve() => 
            this;

        [MethodImpl(0x8000)]
        private void ResolveLayout()
        {
        }

        public TypeAttributes Attributes
        {
            get => 
                ((TypeAttributes) this.attributes);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TypeReference BaseType
        {
            get => 
                this.base_type;
            set => 
                (this.base_type = value);
        }

        public override string Name
        {
            get => 
                base.Name;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasLayoutInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public short PackingSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.packing_size = value);
        }

        public int ClassSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.class_size = value);
        }

        public bool HasInterfaces
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<InterfaceImplementation> Interfaces
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasNestedTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<TypeDefinition> NestedTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasMethods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<MethodDefinition> Methods
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasFields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<FieldDefinition> Fields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasEvents
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<EventDefinition> Events
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<PropertyDefinition> Properties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasSecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<SecurityDeclaration> SecurityDeclarations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool HasGenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override Collection<GenericParameter> GenericParameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsNotPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedPrivate
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 3);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamily
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 5);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamilyAndAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 6);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNestedFamilyOrAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 7);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAutoLayout
        {
            get => 
                this.attributes.GetMaskedAttributes(0x18, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSequentialLayout
        {
            get => 
                this.attributes.GetMaskedAttributes(0x18, 8);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsExplicitLayout
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsClass
        {
            get => 
                this.attributes.GetMaskedAttributes(0x20, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAbstract
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSealed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsImport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSerializable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsWindowsRuntime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAnsiClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsUnicodeClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAutoClass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsBeforeFieldInit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasSecurity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsEnum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsValueType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public override bool IsPrimitive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override ILRuntime.Mono.Cecil.MetadataType MetadataType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override bool IsDefinition =>
            true;

        public TypeDefinition DeclaringType
        {
            get => 
                ((TypeDefinition) base.DeclaringType);
            set => 
                (base.DeclaringType = value);
        }

        internal TypeDefinitionProjection WindowsRuntimeProjection
        {
            get => 
                ((TypeDefinitionProjection) base.projection);
            set => 
                (base.projection = value);
        }
    }
}

