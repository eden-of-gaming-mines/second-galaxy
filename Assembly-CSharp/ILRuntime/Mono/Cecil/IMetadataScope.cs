﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public interface IMetadataScope : IMetadataTokenProvider
    {
        ILRuntime.Mono.Cecil.MetadataScopeType MetadataScopeType { get; }

        string Name { get; set; }
    }
}

