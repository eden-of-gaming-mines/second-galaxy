﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public sealed class AssemblyResolveEventArgs : EventArgs
    {
        private readonly AssemblyNameReference reference;

        public AssemblyResolveEventArgs(AssemblyNameReference reference)
        {
            this.reference = reference;
        }

        public AssemblyNameReference AssemblyReference =>
            this.reference;
    }
}

