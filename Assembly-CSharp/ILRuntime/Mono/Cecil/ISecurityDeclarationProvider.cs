﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;

    public interface ISecurityDeclarationProvider : IMetadataTokenProvider
    {
        bool HasSecurityDeclarations { get; }

        Collection<SecurityDeclaration> SecurityDeclarations { get; }
    }
}

