﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public interface IMetadataTokenProvider
    {
        ILRuntime.Mono.Cecil.MetadataToken MetadataToken { get; set; }
    }
}

