﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    public interface IConstantProvider : IMetadataTokenProvider
    {
        bool HasConstant { get; set; }

        object Constant { get; set; }
    }
}

