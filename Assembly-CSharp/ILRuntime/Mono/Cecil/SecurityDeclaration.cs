﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SecurityDeclaration
    {
        internal readonly uint signature;
        private byte[] blob;
        private readonly ModuleDefinition module;
        internal bool resolved;
        private SecurityAction action;
        internal Collection<SecurityAttribute> security_attributes;
        [CompilerGenerated]
        private static Func<SecurityDeclaration, MetadataReader, byte[]> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<SecurityDeclaration, MetadataReader> <>f__am$cache1;

        [MethodImpl(0x8000)]
        public SecurityDeclaration(SecurityAction action)
        {
        }

        [MethodImpl(0x8000)]
        public SecurityDeclaration(SecurityAction action, byte[] blob)
        {
        }

        [MethodImpl(0x8000)]
        internal SecurityDeclaration(SecurityAction action, uint signature, ModuleDefinition module)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetBlob()
        {
        }

        [MethodImpl(0x8000)]
        private void Resolve()
        {
        }

        public SecurityAction Action
        {
            get => 
                this.action;
            set => 
                (this.action = value);
        }

        public bool HasSecurityAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<SecurityAttribute> SecurityAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal bool HasImage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

