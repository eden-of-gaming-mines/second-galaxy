﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class PropertyReference : MemberReference
    {
        private TypeReference property_type;

        [MethodImpl(0x8000)]
        internal PropertyReference(string name, TypeReference propertyType)
        {
        }

        public abstract PropertyDefinition Resolve();
        protected override IMemberDefinition ResolveDefinition() => 
            this.Resolve();

        public TypeReference PropertyType
        {
            get => 
                this.property_type;
            set => 
                (this.property_type = value);
        }

        public abstract Collection<ParameterDefinition> Parameters { get; }
    }
}

