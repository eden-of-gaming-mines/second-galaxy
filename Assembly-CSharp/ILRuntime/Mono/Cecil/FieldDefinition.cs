﻿namespace ILRuntime.Mono.Cecil
{
    using ILRuntime.Mono.Collections.Generic;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class FieldDefinition : FieldReference, IMemberDefinition, IConstantProvider, IMarshalInfoProvider, ICustomAttributeProvider, IMetadataTokenProvider
    {
        private ushort attributes;
        private Collection<CustomAttribute> custom_attributes;
        private int offset;
        internal int rva;
        private byte[] initial_value;
        private object constant;
        private ILRuntime.Mono.Cecil.MarshalInfo marshal_info;
        [CompilerGenerated]
        private static Func<FieldDefinition, MetadataReader, int> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<FieldDefinition, MetadataReader, int> <>f__am$cache1;

        [MethodImpl(0x8000)]
        public FieldDefinition(string name, FieldAttributes attributes, TypeReference fieldType)
        {
        }

        public override FieldDefinition Resolve() => 
            this;

        [MethodImpl(0x8000)]
        private void ResolveLayout()
        {
        }

        [MethodImpl(0x8000)]
        private void ResolveRVA()
        {
        }

        public bool HasLayoutInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int Offset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.offset = value);
        }

        internal FieldDefinitionProjection WindowsRuntimeProjection
        {
            get => 
                ((FieldDefinitionProjection) base.projection);
            set => 
                (base.projection = value);
        }

        public int RVA
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public byte[] InitialValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set
            {
                this.initial_value = value;
                this.rva = 0;
            }
        }

        public FieldAttributes Attributes
        {
            get => 
                ((FieldAttributes) this.attributes);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasConstant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public object Constant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.constant = value);
        }

        public bool HasCustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Collection<CustomAttribute> CustomAttributes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasMarshalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.MarshalInfo MarshalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            set => 
                (this.marshal_info = value);
        }

        public bool IsCompilerControlled
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 0);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPrivate
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 1);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamilyAndAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 2);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 3);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamily
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 4);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsFamilyOrAssembly
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 5);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPublic
        {
            get => 
                this.attributes.GetMaskedAttributes(7, 6);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsStatic
        {
            get => 
                this.attributes.GetAttributes(0x10);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsInitOnly
        {
            get => 
                this.attributes.GetAttributes(0x20);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsLiteral
        {
            get => 
                this.attributes.GetAttributes(0x40);
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsNotSerialized
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsPInvokeImpl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsRuntimeSpecialName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool HasDefault
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public override bool IsDefinition =>
            true;

        public TypeDefinition DeclaringType
        {
            get => 
                ((TypeDefinition) base.DeclaringType);
            set => 
                (base.DeclaringType = value);
        }
    }
}

