﻿namespace ILRuntime.Mono.Cecil
{
    using System;

    internal enum TypeReferenceTreatment
    {
        None,
        SystemDelegate,
        SystemAttribute,
        UseProjectionInfo
    }
}

