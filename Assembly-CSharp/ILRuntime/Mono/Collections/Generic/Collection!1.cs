﻿namespace ILRuntime.Mono.Collections.Generic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Collection<T> : IList<T>, IList, ICollection<T>, IEnumerable<T>, IEnumerable, ICollection
    {
        internal T[] items;
        internal int size;
        private int version;

        [MethodImpl(0x8000)]
        public Collection()
        {
        }

        [MethodImpl(0x8000)]
        public Collection(ICollection<T> items)
        {
        }

        [MethodImpl(0x8000)]
        public Collection(int capacity)
        {
        }

        [MethodImpl(0x8000)]
        public void Add(T item)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        public bool Contains(T item) => 
            (this.IndexOf(item) != -1);

        [MethodImpl(0x8000)]
        public void CopyTo(T[] array, int arrayIndex)
        {
        }

        public Enumerator<T> GetEnumerator() => 
            new Enumerator<T>((Collection<T>) this);

        [MethodImpl(0x8000)]
        internal virtual void Grow(int desired)
        {
        }

        [MethodImpl(0x8000)]
        public int IndexOf(T item)
        {
        }

        [MethodImpl(0x8000)]
        public void Insert(int index, T item)
        {
        }

        protected virtual void OnAdd(T item, int index)
        {
        }

        protected virtual void OnClear()
        {
        }

        protected virtual void OnInsert(T item, int index)
        {
        }

        protected virtual void OnRemove(T item, int index)
        {
        }

        protected virtual void OnSet(T item, int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool Remove(T item)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAt(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void Resize(int new_size)
        {
        }

        [MethodImpl(0x8000)]
        private void Shift(int start, int delta)
        {
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => 
            new Enumerator<T>((Collection<T>) this);

        [MethodImpl(0x8000)]
        void ICollection.CopyTo(Array array, int index)
        {
        }

        IEnumerator IEnumerable.GetEnumerator() => 
            new Enumerator<T>((Collection<T>) this);

        [MethodImpl(0x8000)]
        int IList.Add(object value)
        {
        }

        void IList.Clear()
        {
            this.Clear();
        }

        bool IList.Contains(object value) => 
            (((IList) this).IndexOf(value) > -1);

        [MethodImpl(0x8000)]
        int IList.IndexOf(object value)
        {
        }

        [MethodImpl(0x8000)]
        void IList.Insert(int index, object value)
        {
        }

        [MethodImpl(0x8000)]
        void IList.Remove(object value)
        {
        }

        void IList.RemoveAt(int index)
        {
            this.RemoveAt(index);
        }

        [MethodImpl(0x8000)]
        public T[] ToArray()
        {
        }

        bool ICollection<T>.IsReadOnly =>
            false;

        bool IList.IsFixedSize =>
            false;

        bool IList.IsReadOnly =>
            false;

        object IList.this[int index]
        {
            get => 
                this[index];
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        int ICollection.Count =>
            this.Count;

        bool ICollection.IsSynchronized =>
            false;

        object ICollection.SyncRoot =>
            this;

        public int Count =>
            this.size;

        public T this[int index]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int Capacity
        {
            get => 
                this.items.Length;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Enumerator : IEnumerator<T>, IDisposable, IEnumerator
        {
            private Collection<T> collection;
            private T current;
            private int next;
            private readonly int version;
            [MethodImpl(0x8000)]
            internal Enumerator(Collection<T> collection)
            {
            }

            public T Current =>
                this.current;
            object IEnumerator.Current
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }
            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            public void Reset()
            {
                this.CheckState();
                this.next = 0;
            }

            [MethodImpl(0x8000)]
            private void CheckState()
            {
            }

            public void Dispose()
            {
                this.collection = null;
            }
        }
    }
}

