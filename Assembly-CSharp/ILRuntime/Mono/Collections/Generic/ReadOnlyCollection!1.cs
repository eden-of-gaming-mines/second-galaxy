﻿namespace ILRuntime.Mono.Collections.Generic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class ReadOnlyCollection<T> : Collection<T>, ICollection<T>, IList, IEnumerable<T>, IEnumerable, ICollection
    {
        private static ReadOnlyCollection<T> empty;

        private ReadOnlyCollection()
        {
        }

        [MethodImpl(0x8000)]
        public ReadOnlyCollection(Collection<T> collection)
        {
        }

        [MethodImpl(0x8000)]
        public ReadOnlyCollection(T[] array)
        {
        }

        internal override void Grow(int desired)
        {
            throw new InvalidOperationException();
        }

        [MethodImpl(0x8000)]
        private void Initialize(T[] items, int size)
        {
        }

        protected override void OnAdd(T item, int index)
        {
            throw new InvalidOperationException();
        }

        protected override void OnClear()
        {
            throw new InvalidOperationException();
        }

        protected override void OnInsert(T item, int index)
        {
            throw new InvalidOperationException();
        }

        protected override void OnRemove(T item, int index)
        {
            throw new InvalidOperationException();
        }

        protected override void OnSet(T item, int index)
        {
            throw new InvalidOperationException();
        }

        bool ICollection<T>.IsReadOnly =>
            true;

        bool IList.IsFixedSize =>
            true;

        bool IList.IsReadOnly =>
            true;

        public static ReadOnlyCollection<T> Empty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

