﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class UnityEngine_Texture_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache18;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache19;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1A;
        [CompilerGenerated]
        private static CLRCreateDefaultInstanceDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache1;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_anisoLevel_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_anisotropicFiltering_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_dimension_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_filterMode_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_height_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_masterTextureLimit_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_mipMapBias_23(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_texelSize_25(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_width_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_wrapMode_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_wrapModeU_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_wrapModeV_19(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_wrapModeW_21(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_anisoLevel_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_anisotropicFiltering_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_dimension_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_filterMode_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_height_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_masterTextureLimit_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_mipMapBias_24(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_width_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_wrapMode_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_wrapModeU_18(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_wrapModeV_20(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_wrapModeW_22(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SetGlobalAnisotropicFilteringLimits_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }
    }
}

