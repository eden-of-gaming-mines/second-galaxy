﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class System_TimeSpan_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache18;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache19;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache20;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache21;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache22;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache23;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache24;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache25;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache26;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache27;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache28;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache29;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2A;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2B;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2C;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2D;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2E;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2F;
        [CompilerGenerated]
        private static CLRMemberwiseCloneDelegate <>f__mg$cache30;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache31;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache32;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache33;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache34;
        [CompilerGenerated]
        private static CLRCreateDefaultInstanceDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache1;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Add_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Compare_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* CompareTo_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* CompareTo_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Duration_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Equals_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Equals_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Equals_18(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromDays_19(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromHours_20(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromMilliseconds_23(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromMinutes_21(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromSeconds_22(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* FromTicks_24(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Days_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Hours_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_MaxValue_5(ref object o) => 
            TimeSpan.MaxValue;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Milliseconds_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Minutes_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_MinValue_6(ref object o) => 
            TimeSpan.MinValue;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Seconds_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Ticks_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_TicksPerDay_0(ref object o) => 
            0xc92a69c000L;

        private static object get_TicksPerHour_1(ref object o) => 
            0x861c46800L;

        private static object get_TicksPerMillisecond_2(ref object o) => 
            0x2710L;

        private static object get_TicksPerMinute_3(ref object o) => 
            0x23c34600L;

        private static object get_TicksPerSecond_4(ref object o) => 
            0x989680L;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_TotalDays_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_TotalHours_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_TotalMilliseconds_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_TotalMinutes_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_TotalSeconds_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_Zero_7(ref object o) => 
            TimeSpan.Zero;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* GetHashCode_25(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Negate_26(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Addition_31(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Equality_32(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_GreaterThan_33(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_GreaterThanOrEqual_34(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Inequality_35(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_LessThan_36(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_LessThanOrEqual_37(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Subtraction_38(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_UnaryNegation_39(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Parse_27(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static object PerformMemberwiseClone(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Subtract_29(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ToString_30(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* TryParse_28(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe void WriteBackInstance(ILRuntime.Runtime.Enviorment.AppDomain __domain, StackObject* ptr_of_this_method, IList<object> __mStack, ref TimeSpan instance_of_this_method)
        {
        }
    }
}

