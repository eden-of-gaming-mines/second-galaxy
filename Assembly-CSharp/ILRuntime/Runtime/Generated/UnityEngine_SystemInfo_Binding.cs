﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class UnityEngine_SystemInfo_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache18;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache19;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache20;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache21;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache22;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache23;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache24;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache25;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache26;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache27;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache28;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache29;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache30;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache31;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache32;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache33;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache34;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache35;
        [CompilerGenerated]
        private static CLRCreateDefaultInstanceDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache1;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_batteryLevel_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_batteryStatus_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_copyTextureSupport_27(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_deviceModel_41(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_deviceName_40(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_deviceType_47(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_deviceUniqueIdentifier_39(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceID_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceName_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceType_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceVendor_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceVendorID_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsDeviceVersion_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsMemorySize_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsMultiThreaded_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsShaderLevel_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_graphicsUVStartsAtTop_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_maxCubemapSize_49(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_maxTextureSize_48(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_npotSupport_38(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_operatingSystem_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_operatingSystemFamily_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_processorCount_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_processorFrequency_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_processorType_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportedRenderTargetCount_31(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supports2DArrayTextures_24(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supports3DRenderTextures_25(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supports3DTextures_23(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsAccelerometer_42(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsAsyncCompute_50(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsAudio_46(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsComputeShaders_28(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsCubemapArrayTextures_26(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsGPUFence_51(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsGyroscope_43(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsImageEffects_22(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsInstancing_29(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsLocationService_44(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsMotionVectors_20(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsMultisampledTextures_32(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsRawShadowDepthSampling_19(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsRenderToCubemap_21(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsShadows_18(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsSparseTextures_30(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsTextureWrapMirrorOnce_33(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_supportsVibration_45(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_systemMemorySize_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_unsupportedIdentifier_0(ref object o) => 
            "n/a";

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_usesReversedZBuffer_34(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SupportsBlendingOnRenderTextureFormat_36(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SupportsRenderTextureFormat_35(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SupportsTextureFormat_37(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }
    }
}

