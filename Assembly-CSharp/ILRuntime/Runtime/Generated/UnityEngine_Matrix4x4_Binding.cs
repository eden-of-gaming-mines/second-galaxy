﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class UnityEngine_Matrix4x4_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache18;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache19;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache20;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache21;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache22;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache23;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache24;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache25;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache26;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache27;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache28;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache29;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2A;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2B;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache2C;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2D;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache2E;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache2F;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache30;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache31;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache32;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache33;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache34;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache35;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache36;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache37;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache38;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache39;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3A;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache3B;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3C;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache3D;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3E;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache3F;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache40;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache41;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache42;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache43;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache44;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache45;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache46;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache47;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache48;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache49;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache4A;
        [CompilerGenerated]
        private static CLRMemberwiseCloneDelegate <>f__mg$cache4B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4C;
        [CompilerGenerated]
        private static CLRCreateDefaultInstanceDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache1;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Determinant_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Equals_23(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Frustum_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Frustum_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_decomposeProjection_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_determinant_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_identity_40(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_inverse_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_isIdentity_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Item_18(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Item_20(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_lossyScale_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m00_0(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m01_4(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m02_8(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m03_12(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m10_1(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m11_5(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m12_9(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m13_13(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m20_2(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m21_6(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m22_10(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m23_14(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m30_3(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m31_7(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m32_11(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_m33_15(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_rotation_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_transpose_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_zero_39(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* GetColumn_28(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* GetHashCode_22(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* GetRow_29(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Inverse_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* LookAt_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* MultiplyPoint_32(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* MultiplyPoint3x4_33(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* MultiplyVector_34(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Equality_26(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Inequality_27(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Multiply_24(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Multiply_25(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ortho_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static object PerformMemberwiseClone(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Perspective_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Rotate_38(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Scale_36(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_Item_19(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_Item_21(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m00_0(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m01_4(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m02_8(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m03_12(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m10_1(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m11_5(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m12_9(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m13_13(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m20_2(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m21_6(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m22_10(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m23_14(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m30_3(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m31_7(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m32_11(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_m33_15(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SetColumn_30(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SetRow_31(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SetTRS_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ToString_41(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ToString_42(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* TransformPlane_35(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Translate_37(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Transpose_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* TRS_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ValidTRS_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe void WriteBackInstance(ILRuntime.Runtime.Enviorment.AppDomain __domain, StackObject* ptr_of_this_method, IList<object> __mStack, ref Matrix4x4 instance_of_this_method)
        {
        }
    }
}

