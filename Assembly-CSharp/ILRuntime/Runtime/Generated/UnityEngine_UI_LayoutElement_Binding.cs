﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class UnityEngine_UI_LayoutElement_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache0;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* CalculateLayoutInputHorizontal_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* CalculateLayoutInputVertical_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_flexibleHeight_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_flexibleWidth_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_ignoreLayout_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_layoutPriority_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_minHeight_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_minWidth_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_preferredHeight_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_preferredWidth_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_flexibleHeight_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_flexibleWidth_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_ignoreLayout_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_layoutPriority_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_minHeight_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_minWidth_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_preferredHeight_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_preferredWidth_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }
    }
}

