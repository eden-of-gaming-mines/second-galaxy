﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using wxb;

    internal class wxb_Hotfix_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache0;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_bridge_2(ref object o) => 
            ((Hotfix) o).bridge;

        private static object get_field_0(ref object o) => 
            ((Hotfix) o).field;

        private static object get_method_1(ref object o) => 
            ((Hotfix) o).method;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Invoke_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Release_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Run_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Run_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_bridge_2(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_field_0(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_method_1(ref object o, object v)
        {
        }
    }
}

