﻿namespace ILRuntime.Runtime.Generated
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class UnityEngine_Vector3_Binding
    {
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache12;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache13;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache14;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache15;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache16;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache17;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache18;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache19;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache20;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache21;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache22;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache23;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache24;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache25;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache26;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache27;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache28;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache29;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2A;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2B;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2C;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2D;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2E;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache30;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache31;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache32;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache33;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache34;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache35;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache36;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache37;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache38;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache39;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3A;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache3B;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3C;
        [CompilerGenerated]
        private static CLRFieldGetterDelegate <>f__mg$cache3D;
        [CompilerGenerated]
        private static CLRFieldSetterDelegate <>f__mg$cache3E;
        [CompilerGenerated]
        private static CLRMemberwiseCloneDelegate <>f__mg$cache3F;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache40;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache41;
        [CompilerGenerated]
        private static CLRCreateDefaultInstanceDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static CLRCreateArrayInstanceDelegate <>f__am$cache1;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Angle_26(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ClampMagnitude_29(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Cross_16(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Ctor_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Distance_28(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Dot_23(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Equals_18(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_back_39(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_down_41(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_forward_38(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_Item_11(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        private static object get_kEpsilon_0(ref object o) => 
            1E-05f;

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_left_42(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_magnitude_31(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_negativeInfinity_45(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_normalized_22(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_one_37(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_positiveInfinity_44(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_right_43(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_sqrMagnitude_33(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_up_40(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_x_1(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_y_2(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static object get_z_3(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* get_zero_36(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* GetHashCode_17(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Lerp_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* LerpUnclamped_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Magnitude_30(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Max_35(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Min_34(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* MoveTowards_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Normalize_20(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Normalize_21(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Addition_46(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Division_51(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Equality_52(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Inequality_53(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Multiply_49(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Multiply_50(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_Subtraction_47(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* op_UnaryNegation_48(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* OrthoNormalize_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* OrthoNormalize_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static object PerformMemberwiseClone(ref object o)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Project_24(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ProjectOnPlane_25(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Reflect_19(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* RotateTowards_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Scale_14(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Scale_15(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Set_13(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* set_Item_12(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_x_1(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_y_2(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static void set_z_3(ref object o, object v)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SignedAngle_27(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* Slerp_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SlerpUnclamped_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SmoothDamp_10(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SmoothDamp_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SmoothDamp_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* SqrMagnitude_32(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ToString_54(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe StackObject* ToString_55(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        private static unsafe void WriteBackInstance(ILRuntime.Runtime.Enviorment.AppDomain __domain, StackObject* ptr_of_this_method, IList<object> __mStack, ref Vector3 instance_of_this_method)
        {
        }
    }
}

