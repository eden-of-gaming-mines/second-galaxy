﻿namespace ILRuntime.Runtime
{
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Extensions
    {
        [MethodImpl(0x8000)]
        public static Type GetActualType(this object value)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetClassName(this Type type, out string clsName, out string realClsName, out bool isByRef, bool simpleClassName = false)
        {
        }

        [MethodImpl(0x8000)]
        public static bool MatchGenericParameters(this MethodInfo m, Type[] genericArguments, Type returnType, params Type[] parameters)
        {
        }

        [MethodImpl(0x8000)]
        public static bool MatchGenericParameters(this Type[] args, Type type, Type q, Type[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        public static double ToDouble(this object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static float ToFloat(this object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static short ToInt16(this object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static int ToInt32(this object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static long ToInt64(this object obj)
        {
        }
    }
}

