﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public unsafe delegate StackObject* CLRRedirectionDelegate(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj);
}

