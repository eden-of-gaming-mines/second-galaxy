﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class CrossBindingAdaptor : IType
    {
        private IType type;

        protected CrossBindingAdaptor()
        {
        }

        [MethodImpl(0x8000)]
        public bool CanAssignTo(IType type)
        {
        }

        public abstract object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance);
        public IType FindGenericArgument(string key) => 
            this.type.FindGenericArgument(key);

        public IMethod GetConstructor(List<IType> param) => 
            this.type.GetConstructor(param);

        public int GetFieldIndex(object token) => 
            this.type.GetFieldIndex(token);

        public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false) => 
            this.type.GetMethod(name, paramCount, declaredOnly);

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name, List<IType> param, IType[] genericArguments, IType returnType = null, bool declaredOnly = false)
        {
        }

        public List<IMethod> GetMethods() => 
            this.type.GetMethods();

        public IMethod GetVirtualMethod(IMethod method) => 
            this.type.GetVirtualMethod(method);

        public IType MakeArrayType(int rank) => 
            this.type.MakeArrayType(rank);

        public IType MakeByRefType() => 
            this.type.MakeByRefType();

        public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments) => 
            this.type.MakeGenericInstance(genericArguments);

        public IType ResolveGenericType(IType contextType) => 
            this.type.ResolveGenericType(contextType);

        public abstract Type BaseCLRType { get; }

        public virtual Type[] BaseCLRTypes =>
            null;

        public abstract Type AdaptorType { get; }

        internal IType RuntimeType
        {
            get => 
                this.type;
            set => 
                (this.type = value);
        }

        public bool IsGenericInstance =>
            this.type.IsGenericInstance;

        public KeyValuePair<string, IType>[] GenericArguments =>
            this.type.GenericArguments;

        public Type TypeForCLR =>
            this.type.TypeForCLR;

        public IType ByRefType =>
            this.type.ByRefType;

        public IType ArrayType =>
            this.type.ArrayType;

        public string FullName =>
            this.type.FullName;

        public string Name =>
            this.type.Name;

        public bool IsValueType =>
            this.type.IsValueType;

        public bool IsPrimitive =>
            this.type.IsPrimitive;

        public bool IsEnum =>
            this.type.IsEnum;

        public bool IsDelegate =>
            this.type.IsDelegate;

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            this.type.AppDomain;

        public Type ReflectionType =>
            this.type.ReflectionType;

        public IType BaseType =>
            this.type.BaseType;

        public IType[] Implements =>
            this.type.Implements;

        public bool HasGenericParameter =>
            this.type.HasGenericParameter;

        public bool IsGenericParameter =>
            this.type.IsGenericParameter;

        public bool IsArray =>
            false;

        public bool IsByRef =>
            this.type.IsByRef;

        public bool IsInterface =>
            this.type.IsInterface;

        public IType ElementType =>
            this.type.ElementType;

        public int ArrayRank =>
            this.type.ArrayRank;
    }
}

