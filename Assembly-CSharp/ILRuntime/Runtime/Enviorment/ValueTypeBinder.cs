﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class ValueTypeBinder
    {
        protected ILRuntime.CLR.TypeSystem.CLRType clrType;
        protected ILRuntime.Runtime.Enviorment.AppDomain domain;

        protected ValueTypeBinder()
        {
        }

        [MethodImpl(0x8000)]
        protected unsafe void AssignFromStack<K>(ref K ins, StackObject* esp, IList<object> mStack) where K: struct
        {
        }

        public abstract unsafe void CopyValueTypeToStack(object ins, StackObject* ptr, IList<object> mStack);
        [MethodImpl(0x8000)]
        protected unsafe void CopyValueTypeToStack<K>(ref K ins, StackObject* esp, IList<object> mStack) where K: struct
        {
        }

        public virtual void RegisterCLRRedirection(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        public abstract unsafe object ToObject(StackObject* esp, IList<object> managedStack);

        public ILRuntime.CLR.TypeSystem.CLRType CLRType
        {
            get => 
                this.clrType;
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

