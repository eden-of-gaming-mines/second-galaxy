﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class DelegateManager
    {
        private List<DelegateMapNode> methods;
        private List<DelegateMapNode> functions;
        private IDelegateAdapter zeroParamMethodAdapter;
        private IDelegateAdapter dummyAdapter;
        private Dictionary<Type, Func<Delegate, Delegate>> clrDelegates;
        private Func<Delegate, Delegate> defaultConverter;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        [CompilerGenerated]
        private static Func<Delegate, Delegate> <>f__mg$cache0;

        [MethodImpl(0x8000)]
        public DelegateManager(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [MethodImpl(0x8000)]
        internal Delegate ConvertToDelegate(Type clrDelegateType, IDelegateAdapter adapter)
        {
        }

        private static Delegate DefaultConverterStub(Delegate dele) => 
            dele;

        [MethodImpl(0x8000)]
        internal IDelegateAdapter FindDelegateAdapter(ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterDelegateConvertor<T>(Func<Delegate, Delegate> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFunctionDelegate<TResult>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFunctionDelegate<T1, TResult>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFunctionDelegate<T1, T2, TResult>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFunctionDelegate<T1, T2, T3, TResult>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFunctionDelegate<T1, T2, T3, T4, TResult>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterMethodDelegate<T1>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterMethodDelegate<T1, T2>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterMethodDelegate<T1, T2, T3>()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterMethodDelegate<T1, T2, T3, T4>()
        {
        }

        private class DelegateMapNode
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private IDelegateAdapter <Adapter>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private Type[] <ParameterTypes>k__BackingField;

            public IDelegateAdapter Adapter { get; set; }

            public Type[] ParameterTypes { get; set; }
        }
    }
}

