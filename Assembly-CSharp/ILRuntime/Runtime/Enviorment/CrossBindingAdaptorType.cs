﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.Runtime.Intepreter;

    public interface CrossBindingAdaptorType
    {
        ILTypeInstance ILInstance { get; }
    }
}

