﻿namespace ILRuntime.Runtime.Enviorment
{
    using System;
    using System.Runtime.CompilerServices;

    public static class PrimitiveConverter<T>
    {
        public static Func<T, int> ToInteger;
        public static Func<int, T> FromInteger;
        public static Func<T, long> ToLong;
        public static Func<long, T> FromLong;
        public static Func<T, float> ToFloat;
        public static Func<float, T> FromFloat;
        public static Func<T, double> ToDouble;
        public static Func<double, T> FromDouble;

        [MethodImpl(0x8000)]
        public static T CheckAndInvokeFromDouble(double val)
        {
        }

        [MethodImpl(0x8000)]
        public static T CheckAndInvokeFromFloat(float val)
        {
        }

        [MethodImpl(0x8000)]
        public static T CheckAndInvokeFromInteger(int val)
        {
        }

        [MethodImpl(0x8000)]
        public static T CheckAndInvokeFromLong(long val)
        {
        }

        [MethodImpl(0x8000)]
        public static double CheckAndInvokeToDouble(T val)
        {
        }

        [MethodImpl(0x8000)]
        public static float CheckAndInvokeToFloat(T val)
        {
        }

        [MethodImpl(0x8000)]
        public static int CheckAndInvokeToInteger(T val)
        {
        }

        [MethodImpl(0x8000)]
        public static long CheckAndInvokeToLong(T val)
        {
        }
    }
}

