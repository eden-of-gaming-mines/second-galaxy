﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal static class CLRRedirections
    {
        [MethodImpl(0x8000)]
        private static object CheckCrossBindingAdapter(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* CreateInstance(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* CreateInstance2(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* CreateInstance3(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* DelegateCombine(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* DelegateEqulity(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* DelegateInequlity(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* DelegateRemove(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* EnumGetName(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* EnumGetNames(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* EnumGetValues(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* EnumParse(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* EnumToObject(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* GetType(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        public static unsafe StackObject* GetTypeFromHandle(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj) => 
            esp;

        [MethodImpl(0x8000)]
        public static unsafe StackObject* InitializeArray(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* MethodInfoInvoke(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* ObjectGetType(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* TypeEquals(ILIntepreter intp, StackObject* esp, IList<object> mStack, CLRMethod method, bool isNewObj)
        {
        }
    }
}

