﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Other;
    using ILRuntime.Runtime.Debugger;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AppDomain
    {
        private Queue<ILIntepreter> freeIntepreters;
        private Dictionary<int, ILIntepreter> intepreters;
        private Dictionary<System.Type, CrossBindingAdaptor> crossAdaptors;
        private Dictionary<System.Type, ValueTypeBinder> valueTypeBinders;
        private ThreadSafeDictionary<string, IType> mapType;
        private Dictionary<System.Type, IType> clrTypeMapping;
        private ThreadSafeDictionary<int, IType> mapTypeToken;
        private ThreadSafeDictionary<int, IMethod> mapMethod;
        private ThreadSafeDictionary<long, string> mapString;
        private Dictionary<MethodBase, CLRRedirectionDelegate> redirectMap;
        private Dictionary<FieldInfo, CLRFieldGetterDelegate> fieldGetterMap;
        private Dictionary<FieldInfo, CLRFieldSetterDelegate> fieldSetterMap;
        private Dictionary<System.Type, CLRMemberwiseCloneDelegate> memberwiseCloneMap;
        private Dictionary<System.Type, CLRCreateDefaultInstanceDelegate> createDefaultInstanceMap;
        private Dictionary<System.Type, CLRCreateArrayInstanceDelegate> createArrayInstanceMap;
        private IType voidType;
        private IType intType;
        private IType longType;
        private IType boolType;
        private IType floatType;
        private IType doubleType;
        private IType objectType;
        private ILRuntime.Runtime.Enviorment.DelegateManager dMgr;
        private Assembly[] loadedAssemblies;
        private Dictionary<string, byte[]> references;
        private ILRuntime.Runtime.Debugger.DebugService debugService;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <AllowUnboundCLRMethod>k__BackingField;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache0;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache1;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache2;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache3;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache4;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache5;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache6;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache7;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache8;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache9;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheA;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheB;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheC;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheD;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheE;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cacheF;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache10;
        [CompilerGenerated]
        private static CLRRedirectionDelegate <>f__mg$cache11;
        [CompilerGenerated]
        private static Func<Delegate, Delegate> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<TypeSizeInfo> <>f__am$cache1;

        [CompilerGenerated]
        private static Delegate <AppDomain>m__0(Delegate dele) => 
            dele;

        public void AddReferenceBytes(string name, byte[] content)
        {
            this.references[name] = content;
        }

        [MethodImpl(0x8000)]
        public InvocationContext BeginInvoke(IMethod m)
        {
        }

        [MethodImpl(0x8000)]
        internal long CacheString(object token)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckStringCollision(long hashCode, string newStr)
        {
        }

        [MethodImpl(0x8000)]
        internal void FreeILIntepreter(ILIntepreter inteptreter)
        {
        }

        [MethodImpl(0x8000)]
        private string GetAssemblyName(IMetadataScope scope)
        {
        }

        [MethodImpl(0x8000)]
        internal IMethod GetMethod(int tokenHash)
        {
        }

        [MethodImpl(0x8000)]
        internal IMethod GetMethod(object token, ILType contextType, ILMethod contextMethod, out bool invalidToken)
        {
        }

        [MethodImpl(0x8000)]
        public int GetSizeInMemory(out List<TypeSizeInfo> detail)
        {
        }

        [MethodImpl(0x8000)]
        internal long GetStaticFieldIndex(object token, IType contextType, IMethod contextMethod)
        {
        }

        [MethodImpl(0x8000)]
        internal string GetString(long hashCode)
        {
        }

        [MethodImpl(0x8000)]
        public IType GetType(int hash)
        {
        }

        [MethodImpl(0x8000)]
        public IType GetType(string fullname)
        {
        }

        [MethodImpl(0x8000)]
        public IType GetType(System.Type t)
        {
        }

        [MethodImpl(0x8000)]
        internal IType GetType(object token, IType contextType, IMethod contextMethod)
        {
        }

        [MethodImpl(0x8000)]
        public ILTypeInstance Instantiate(string type, object[] args = null)
        {
        }

        [MethodImpl(0x8000)]
        public T Instantiate<T>(string type, object[] args = null)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(IMethod m, object instance, params object[] p)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(string type, string method, object instance, params object[] p)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeGenericMethod(string type, string method, IType[] genericArguments, object instance, params object[] p)
        {
        }

        public void LoadAssembly(Stream stream)
        {
            this.LoadAssembly(stream, null, null);
        }

        [MethodImpl(0x8000)]
        public void LoadAssembly(Stream stream, Stream symbol, ISymbolReaderProvider symbolReader)
        {
        }

        [MethodImpl(0x8000)]
        public void LoadAssemblyFile(string path)
        {
        }

        [MethodImpl(0x8000)]
        internal static void ParseGenericType(string fullname, out string baseType, out List<string> genericParams, out bool isArray)
        {
        }

        [MethodImpl(0x8000)]
        public void Prewarm(string type)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRCreateArrayInstance(System.Type t, CLRCreateArrayInstanceDelegate createArray)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRCreateDefaultInstance(System.Type t, CLRCreateDefaultInstanceDelegate createDefaultInstance)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRFieldGetter(FieldInfo f, CLRFieldGetterDelegate getter)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRFieldSetter(FieldInfo f, CLRFieldSetterDelegate setter)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRMemberwiseClone(System.Type t, CLRMemberwiseCloneDelegate memberwiseClone)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCLRMethodRedirection(MethodBase mi, CLRRedirectionDelegate func)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterCrossBindingAdaptor(CrossBindingAdaptor adaptor)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterValueTypeBinder(System.Type t, ValueTypeBinder binder)
        {
        }

        [MethodImpl(0x8000)]
        private ILIntepreter RequestILIntepreter()
        {
        }

        public bool AllowUnboundCLRMethod { get; set; }

        public IType VoidType =>
            this.voidType;

        public IType IntType =>
            this.intType;

        public IType LongType =>
            this.longType;

        public IType BoolType =>
            this.boolType;

        public IType FloatType =>
            this.floatType;

        public IType DoubleType =>
            this.doubleType;

        public IType ObjectType =>
            this.objectType;

        public Dictionary<string, IType> LoadedTypes =>
            this.mapType.InnerDictionary;

        internal Dictionary<MethodBase, CLRRedirectionDelegate> RedirectMap =>
            this.redirectMap;

        internal Dictionary<FieldInfo, CLRFieldGetterDelegate> FieldGetterMap =>
            this.fieldGetterMap;

        internal Dictionary<FieldInfo, CLRFieldSetterDelegate> FieldSetterMap =>
            this.fieldSetterMap;

        internal Dictionary<System.Type, CLRMemberwiseCloneDelegate> MemberwiseCloneMap =>
            this.memberwiseCloneMap;

        internal Dictionary<System.Type, CLRCreateDefaultInstanceDelegate> CreateDefaultInstanceMap =>
            this.createDefaultInstanceMap;

        internal Dictionary<System.Type, CLRCreateArrayInstanceDelegate> CreateArrayInstanceMap =>
            this.createArrayInstanceMap;

        internal Dictionary<System.Type, CrossBindingAdaptor> CrossBindingAdaptors =>
            this.crossAdaptors;

        internal Dictionary<System.Type, ValueTypeBinder> ValueTypeBinders =>
            this.valueTypeBinders;

        public ILRuntime.Runtime.Debugger.DebugService DebugService =>
            this.debugService;

        internal Dictionary<int, ILIntepreter> Intepreters =>
            this.intepreters;

        internal Queue<ILIntepreter> FreeIntepreters =>
            this.freeIntepreters;

        public ILRuntime.Runtime.Enviorment.DelegateManager DelegateManager =>
            this.dMgr;
    }
}

