﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.TypeSystem;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct TypeSizeInfo
    {
        public ILType Type;
        public int StaticFieldSize;
        public int MethodBodySize;
        public int TotalSize;
    }
}

