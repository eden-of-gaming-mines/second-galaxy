﻿namespace ILRuntime.Runtime.Enviorment
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct InvocationContext : IDisposable
    {
        private unsafe StackObject* esp;
        private ILRuntime.Runtime.Enviorment.AppDomain domain;
        private ILIntepreter intp;
        private ILMethod method;
        private IList<object> mStack;
        private bool invocated;
        private int paramCnt;
        private bool hasReturn;
        private static bool defaultConverterIntialized;
        [CompilerGenerated]
        private static Func<int, int> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<int, int> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<short, int> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<int, short> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<byte, int> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<int, byte> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<sbyte, int> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<int, sbyte> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<ushort, int> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<int, ushort> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<char, int> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<int, char> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<uint, int> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<int, uint> <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<bool, int> <>f__am$cacheE;
        [CompilerGenerated]
        private static Func<int, bool> <>f__am$cacheF;
        [CompilerGenerated]
        private static Func<long, long> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<long, long> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<ulong, long> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<long, ulong> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<float, float> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<float, float> <>f__am$cache15;
        [CompilerGenerated]
        private static Func<double, double> <>f__am$cache16;
        [CompilerGenerated]
        private static Func<double, double> <>f__am$cache17;
        [MethodImpl(0x8000)]
        internal InvocationContext(ILIntepreter intp, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        internal static void InitializeDefaultConverters()
        {
        }

        [MethodImpl(0x8000)]
        public void PushBool(bool val)
        {
        }

        public void PushInteger<T>(T val)
        {
            this.PushInteger(PrimitiveConverter<T>.CheckAndInvokeToInteger(val));
        }

        public void PushLong<T>(T val)
        {
            this.PushInteger(PrimitiveConverter<T>.CheckAndInvokeToLong(val));
        }

        [MethodImpl(0x8000)]
        public void PushInteger(int val)
        {
        }

        [MethodImpl(0x8000)]
        public void PushInteger(long val)
        {
        }

        public void PushFloat<T>(T val)
        {
            this.PushFloat(PrimitiveConverter<T>.CheckAndInvokeToFloat(val));
        }

        [MethodImpl(0x8000)]
        public void PushFloat(float val)
        {
        }

        public void PushDouble<T>(T val)
        {
            this.PushDouble(PrimitiveConverter<T>.CheckAndInvokeToDouble(val));
        }

        [MethodImpl(0x8000)]
        public void PushDouble(double val)
        {
        }

        [MethodImpl(0x8000)]
        public void PushObject(object obj, bool isBox = true)
        {
        }

        [MethodImpl(0x8000)]
        public void Invoke()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckReturnValue()
        {
        }

        [MethodImpl(0x8000)]
        public int ReadInteger()
        {
        }

        public T ReadInteger<T>() => 
            PrimitiveConverter<T>.CheckAndInvokeFromInteger(this.ReadInteger());

        [MethodImpl(0x8000)]
        public long ReadLong()
        {
        }

        public T ReadLong<T>() => 
            PrimitiveConverter<T>.CheckAndInvokeFromLong(this.ReadLong());

        [MethodImpl(0x8000)]
        public float ReadFloat()
        {
        }

        public T ReadFloat<T>() => 
            PrimitiveConverter<T>.CheckAndInvokeFromFloat(this.ReadFloat());

        [MethodImpl(0x8000)]
        public double ReadDouble()
        {
        }

        public T ReadDouble<T>() => 
            PrimitiveConverter<T>.CheckAndInvokeFromDouble(this.ReadDouble());

        [MethodImpl(0x8000)]
        public bool ReadBool()
        {
        }

        [MethodImpl(0x8000)]
        public T ReadObject<T>()
        {
        }

        [MethodImpl(0x8000)]
        public object ReadObject(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }

        static InvocationContext()
        {
        }
    }
}

