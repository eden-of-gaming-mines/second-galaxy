﻿namespace ILRuntime.Runtime.Debugger
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class VariableReference
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private long <Address>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private VariableTypes <Type>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Offset>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <Name>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private VariableReference <Parent>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private VariableReference[] <Parameters>k__BackingField;
        public static VariableReference Null;
        public static VariableReference True;
        public static VariableReference False;

        [MethodImpl(0x8000)]
        public static VariableReference GetInteger(int val)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableReference GetMember(string name, VariableReference parent)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableReference GetString(string val)
        {
        }

        public long Address { get; set; }

        public VariableTypes Type { get; set; }

        public int Offset { get; set; }

        public string Name { get; set; }

        public VariableReference Parent { get; set; }

        public VariableReference[] Parameters { get; set; }

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

