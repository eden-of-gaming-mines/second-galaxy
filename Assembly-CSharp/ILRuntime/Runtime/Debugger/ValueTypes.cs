﻿namespace ILRuntime.Runtime.Debugger
{
    using System;

    public enum ValueTypes
    {
        Object,
        Null,
        Integer,
        Boolean,
        String
    }
}

