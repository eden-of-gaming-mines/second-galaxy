﻿namespace ILRuntime.Runtime.Debugger
{
    using System;

    public enum StepTypes
    {
        None,
        Into,
        Over,
        Out
    }
}

