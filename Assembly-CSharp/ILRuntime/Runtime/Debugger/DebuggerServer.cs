﻿namespace ILRuntime.Runtime.Debugger
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Debugger.Protocol;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Sockets;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class DebuggerServer
    {
        public const int Version = 2;
        private TcpListener listener;
        private bool isUp;
        private int maxNewConnections;
        private int port;
        private Thread mainLoop;
        private DebugSocket clientSocket;
        private MemoryStream sendStream;
        private BinaryWriter bw;
        private DebugService ds;

        [MethodImpl(0x8000)]
        public DebuggerServer(DebugService ds)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckCompilerGeneratedStateMachine(ILMethod ilm, ILRuntime.Runtime.Enviorment.AppDomain domain, int startLine, out ILMethod found)
        {
        }

        private void ClientConnected()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateNewSession(TcpListener listener)
        {
        }

        [MethodImpl(0x8000)]
        private void DoSend(DebugMessageType type)
        {
        }

        [MethodImpl(0x8000)]
        private void NetworkLoop()
        {
        }

        [MethodImpl(0x8000)]
        public void NotifyModuleLoaded(string modulename)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClose()
        {
        }

        [MethodImpl(0x8000)]
        private void OnReceive(DebugMessageType type, byte[] buffer)
        {
        }

        [MethodImpl(0x8000)]
        private VariableReference ReadVariableReference(BinaryReader br)
        {
        }

        [MethodImpl(0x8000)]
        private void SendAttachResult()
        {
        }

        [MethodImpl(0x8000)]
        private void SendSCBindBreakpointResult(SCBindBreakpointResult msg)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCBreakpointHit(int intpHash, int bpHash, KeyValuePair<int, StackFrameInfo[]>[] info)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCEnumChildrenResult(VariableInfo[] info)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCResolveVariableResult(VariableInfo info)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCStepComplete(int intpHash, KeyValuePair<int, StackFrameInfo[]>[] info)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCThreadEnded(int threadHash)
        {
        }

        [MethodImpl(0x8000)]
        internal void SendSCThreadStarted(int threadHash)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Start()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Stop()
        {
        }

        [MethodImpl(0x8000)]
        private void TryBindBreakpoint(CSBindBreakpoint msg)
        {
        }

        [MethodImpl(0x8000)]
        private void WriteStackFrames(KeyValuePair<int, StackFrameInfo[]>[] info)
        {
        }

        [MethodImpl(0x8000)]
        private void WriteVariableInfo(VariableInfo k)
        {
        }

        public int Port
        {
            get => 
                this.port;
            set => 
                (this.port = value);
        }

        public DebugSocket Client =>
            this.clientSocket;

        public bool IsAttached
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

