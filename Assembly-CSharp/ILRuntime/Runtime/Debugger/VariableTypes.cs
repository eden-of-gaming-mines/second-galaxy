﻿namespace ILRuntime.Runtime.Debugger
{
    using System;

    public enum VariableTypes
    {
        Normal,
        FieldReference,
        PropertyReference,
        TypeReference,
        IndexAccess,
        Invocation,
        Integer,
        Boolean,
        String,
        Null,
        Error,
        NotFound,
        Timeout,
        Pending
    }
}

