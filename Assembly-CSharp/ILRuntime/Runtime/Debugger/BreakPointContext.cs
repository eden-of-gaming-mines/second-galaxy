﻿namespace ILRuntime.Runtime.Debugger
{
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    internal class BreakPointContext
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ILIntepreter <Interpreter>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private System.Exception <Exception>k__BackingField;

        public string DumpContext() => 
            null;

        [MethodImpl(0x8000)]
        private string GetStackObjectValue(StackObject val, IList<object> mStack)
        {
        }

        public ILIntepreter Interpreter { get; set; }

        public System.Exception Exception { get; set; }
    }
}

