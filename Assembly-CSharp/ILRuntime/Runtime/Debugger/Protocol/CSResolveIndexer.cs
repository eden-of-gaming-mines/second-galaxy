﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using ILRuntime.Runtime.Debugger;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CSResolveIndexer
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ThreadHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private VariableReference <Index>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private VariableReference <Body>k__BackingField;

        public int ThreadHashCode { get; set; }

        public VariableReference Index { get; set; }

        public VariableReference Body { get; set; }
    }
}

