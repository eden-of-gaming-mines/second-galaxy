﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using ILRuntime.Runtime.Debugger;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCResolveVariableResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private VariableInfo <Info>k__BackingField;

        public VariableInfo Info { get; set; }
    }
}

