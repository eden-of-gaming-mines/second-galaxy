﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCThreadEnded
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ThreadHashCode>k__BackingField;

        public int ThreadHashCode { get; set; }
    }
}

