﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;

    public enum AttachResults
    {
        OK,
        AlreadyAttached
    }
}

