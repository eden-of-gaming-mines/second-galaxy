﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using ILRuntime.Runtime.Debugger;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CSStep
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <ThreadHashCode>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private StepTypes <StepType>k__BackingField;

        public int ThreadHashCode { get; set; }

        public StepTypes StepType { get; set; }
    }
}

