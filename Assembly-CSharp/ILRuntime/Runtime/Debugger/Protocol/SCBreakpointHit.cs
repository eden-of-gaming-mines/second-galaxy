﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCBreakpointHit
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <BreakpointHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ThreadHashCode>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private KeyValuePair<int, StackFrameInfo[]>[] <StackFrame>k__BackingField;

        public int BreakpointHashCode { get; set; }

        public int ThreadHashCode { get; set; }

        public KeyValuePair<int, StackFrameInfo[]>[] StackFrame { get; set; }
    }
}

