﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using ILRuntime.Runtime.Debugger;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CSEnumChildren
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ThreadHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private VariableReference <Parent>k__BackingField;

        public int ThreadHashCode { get; set; }

        public VariableReference Parent { get; set; }
    }
}

