﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCAttachResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private AttachResults <Result>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <DebugServerVersion>k__BackingField;

        public AttachResults Result { get; set; }

        public int DebugServerVersion { get; set; }
    }
}

