﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCBindBreakpointResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <BreakpointHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private BindBreakpointResults <Result>k__BackingField;

        public int BreakpointHashCode { get; set; }

        public BindBreakpointResults Result { get; set; }
    }
}

