﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CSDeleteBreakpoint
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <BreakpointHashCode>k__BackingField;

        public int BreakpointHashCode { get; set; }
    }
}

