﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using ILRuntime.Runtime.Debugger;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCEnumChildrenResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private VariableInfo[] <Children>k__BackingField;

        public VariableInfo[] Children { get; set; }
    }
}

