﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;

    public enum BindBreakpointResults
    {
        OK,
        TypeNotFound,
        CodeNotFound
    }
}

