﻿namespace ILRuntime.Runtime.Debugger.Protocol
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SCModuleLoaded
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <ModuleName>k__BackingField;

        public string ModuleName { get; set; }
    }
}

