﻿namespace ILRuntime.Runtime.Debugger
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;

    public class DebugService
    {
        private BreakPointContext curBreakpoint;
        private DebuggerServer server;
        private ILRuntime.Runtime.Enviorment.AppDomain domain;
        private Dictionary<int, LinkedList<BreakpointInfo>> activeBreakpoints;
        private Dictionary<int, BreakpointInfo> breakpointMapping;
        private Queue<KeyValuePair<int, ILRuntime.Runtime.Debugger.VariableReference>> pendingReferences;
        private Queue<KeyValuePair<int, ILRuntime.Runtime.Debugger.VariableReference>> pendingEnuming;
        private Queue<KeyValuePair<int, KeyValuePair<ILRuntime.Runtime.Debugger.VariableReference, ILRuntime.Runtime.Debugger.VariableReference>>> pendingIndexing;
        private AutoResetEvent evt;
        public Action<string> OnBreakPoint;

        [MethodImpl(0x8000)]
        public DebugService(ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        private unsafe StackObject* Add(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) + (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        internal bool Break(ILIntepreter intpreter, Exception ex = null)
        {
        }

        [MethodImpl(0x8000)]
        internal void CheckShouldBreak(ILMethod method, ILIntepreter intp, int ip)
        {
        }

        [MethodImpl(0x8000)]
        internal void DeleteBreakpoint(int bpHash)
        {
        }

        [MethodImpl(0x8000)]
        internal void Detach()
        {
        }

        [MethodImpl(0x8000)]
        private void DoBreak(ILIntepreter intp, int bpHash, bool isStep)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void DumpStack(StackObject* esp, RuntimeStack stack)
        {
        }

        [MethodImpl(0x8000)]
        private VariableInfo[] EnumArray(Array arr, ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        internal VariableInfo[] EnumChildren(int threadHashCode, ILRuntime.Runtime.Debugger.VariableReference parent)
        {
        }

        private VariableInfo[] EnumCLRObject(object obj, ILIntepreter intepreter) => 
            this.EnumObject(obj, obj.GetType());

        [MethodImpl(0x8000)]
        private VariableInfo[] EnumDictionary(IDictionary lst, ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        private VariableInfo[] EnumILTypeInstance(ILTypeInstance obj, ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        private VariableInfo[] EnumList(IList lst, ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        private VariableInfo[] EnumObject(object obj, Type t)
        {
        }

        [MethodImpl(0x8000)]
        internal void ExecuteThread(int threadHash)
        {
        }

        [MethodImpl(0x8000)]
        internal static SequencePoint FindSequencePoint(Instruction ins, IDictionary<Instruction, SequencePoint> seqMapping)
        {
        }

        [MethodImpl(0x8000)]
        private object[] GetArray(ICollection lst)
        {
        }

        [MethodImpl(0x8000)]
        public string GetLocalVariableInfo(ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        private StackFrameInfo[] GetStackFrameInfo(ILIntepreter intp)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void GetStackObjectText(StringBuilder sb, StackObject* esp, IList<object> mStack, StackObject* valueTypeEnd)
        {
        }

        [MethodImpl(0x8000)]
        public string GetStackTrace(ILIntepreter intepreper)
        {
        }

        [MethodImpl(0x8000)]
        public string GetThisInfo(ILIntepreter intepreter)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe bool GetValueExpandable(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSequenceValid(SequencePoint sp)
        {
        }

        private unsafe StackObject* Minus(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) - (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        internal void NotifyModuleLoaded(string moduleName)
        {
        }

        [MethodImpl(0x8000)]
        internal VariableInfo ResolveIndexAccess(int threadHashCode, ILRuntime.Runtime.Debugger.VariableReference body, ILRuntime.Runtime.Debugger.VariableReference idx, out object res)
        {
        }

        [MethodImpl(0x8000)]
        private VariableInfo ResolveMember(object obj, string name, out object res)
        {
        }

        [MethodImpl(0x8000)]
        internal void ResolvePendingRequests()
        {
        }

        [MethodImpl(0x8000)]
        internal VariableInfo ResolveVariable(int threadHashCode, ILRuntime.Runtime.Debugger.VariableReference variable, out object res)
        {
        }

        [MethodImpl(0x8000)]
        private string SafeToString(object obj)
        {
        }

        [MethodImpl(0x8000)]
        internal void SetBreakPoint(int methodHash, int bpHash, int startLine)
        {
        }

        public void StartDebugService(int port)
        {
        }

        [MethodImpl(0x8000)]
        internal void StepThread(int threadHash, StepTypes type)
        {
        }

        public void StopDebugService()
        {
        }

        [MethodImpl(0x8000)]
        internal void ThreadEnded(ILIntepreter intp)
        {
        }

        [MethodImpl(0x8000)]
        internal void ThreadStarted(ILIntepreter intp)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void VisitValueTypeReference(StackObject* esp, HashSet<long> leak)
        {
        }

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            this.domain;

        public AutoResetEvent BlockEvent =>
            this.evt;

        public bool IsDebuggerAttached =>
            false;
    }
}

