﻿namespace ILRuntime.Runtime.Debugger
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class VariableInfo
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private long <Address>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private VariableTypes <Type>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <Name>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <TypeName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Value>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ValueTypes <ValueType>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <Expandable>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsPrivate>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsProtected>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <Offset>k__BackingField;
        public static VariableInfo NullReferenceExeption;
        public static VariableInfo RequestTimeout;
        public static VariableInfo Null;
        public static VariableInfo True;
        public static VariableInfo False;

        [MethodImpl(0x8000)]
        public static VariableInfo FromObject(object obj, bool retriveType = false)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableInfo GetCannotFind(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableInfo GetException(Exception ex)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableInfo GetInteger(int val)
        {
        }

        [MethodImpl(0x8000)]
        public static VariableInfo GetString(string val)
        {
        }

        public long Address { get; set; }

        public VariableTypes Type { get; set; }

        public string Name { get; set; }

        public string TypeName { get; set; }

        public string Value { get; set; }

        public ValueTypes ValueType { get; set; }

        public bool Expandable { get; set; }

        public bool IsPrivate { get; set; }

        public bool IsProtected { get; set; }

        public int Offset { get; set; }
    }
}

