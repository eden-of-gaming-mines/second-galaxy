﻿namespace ILRuntime.Runtime.Debugger
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    internal class BreakpointInfo
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <BreakpointHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <MethodHashCode>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <StartLine>k__BackingField;

        public int BreakpointHashCode { get; set; }

        public int MethodHashCode { get; set; }

        public int StartLine { get; set; }
    }
}

