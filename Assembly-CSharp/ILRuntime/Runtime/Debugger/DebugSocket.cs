﻿namespace ILRuntime.Runtime.Debugger
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Sockets;
    using System.Runtime.CompilerServices;

    public class DebugSocket
    {
        private Socket _socket;
        private bool _ready;
        private bool connectFailed;
        private const int MAX_BUFF_SIZE = 0x40000;
        private const int HEAD_SIZE = 8;
        private byte[] _headBuffer;
        private byte[] _sendBuffer;
        private MemoryStream _sendStream;
        private BinaryWriter bw;
        private const int RECV_BUFFER_SIZE = 0x400;
        private MemoryStream recvBuffer;
        private int lastMsgLength;
        private byte[] socketAsyncBuffer;
        private SocketAsyncEventArgs saeArgs;
        private object socketLockObj;
        private byte[] _sendHeaderBuffer;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action <OnConnect>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action <OnConnectFailed>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action <OnClose>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DebugMessageType, byte[]> <OnReciveMessage>k__BackingField;

        [MethodImpl(0x8000)]
        public DebugSocket()
        {
        }

        [MethodImpl(0x8000)]
        public DebugSocket(Socket _socket)
        {
        }

        [MethodImpl(0x8000)]
        private void AsyncRecv_Completed(object sender, SocketAsyncEventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void BeginReceive()
        {
        }

        [MethodImpl(0x8000)]
        public void Close()
        {
        }

        [MethodImpl(0x8000)]
        public void Connect(string ip, int port)
        {
        }

        [MethodImpl(0x8000)]
        private void onConnected(IAsyncResult result)
        {
        }

        [MethodImpl(0x8000)]
        private void RawSend(Socket sock, byte[] buf, int end)
        {
        }

        [MethodImpl(0x8000)]
        private void ReceivePayload(byte[] data, int length)
        {
        }

        [MethodImpl(0x8000)]
        public void Send(DebugMessageType type, byte[] buffer, int len)
        {
        }

        public bool Disconnected
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Action OnConnect { get; set; }

        public Action OnConnectFailed { get; set; }

        public Action OnClose { get; set; }

        public Action<DebugMessageType, byte[]> OnReciveMessage { get; set; }
    }
}

