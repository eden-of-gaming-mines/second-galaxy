﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ILRuntimeException : Exception
    {
        private string stackTrace;
        private string thisInfo;
        private string localInfo;

        [MethodImpl(0x8000)]
        internal ILRuntimeException(string message, ILIntepreter intepreter, ILMethod method, Exception innerException = null)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public override string StackTrace =>
            this.stackTrace;

        public string ThisInfo =>
            this.thisInfo;

        public string LocalInfo =>
            this.localInfo;
    }
}

