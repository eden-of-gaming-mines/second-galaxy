﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ILTypeInstance
    {
        protected ILType type;
        protected StackObject[] fields;
        protected IList<object> managedObjs;
        private object clrInstance;
        private Dictionary<ILMethod, IDelegateAdapter> delegates;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <Boxed>k__BackingField;
        private const int SizeOfILTypeInstance = 0x15;

        protected ILTypeInstance()
        {
        }

        [MethodImpl(0x8000)]
        public ILTypeInstance(ILType type, bool initializeCLRInstance = true)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void AssignFromStack(StackObject* esp, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IList<object> managedStack)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void AssignFromStack(int fieldIdx, StackObject* esp, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IList<object> managedStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void AssignFromStackSub(ref StackObject field, int fieldIdx, StackObject* esp, IList<object> managedStack)
        {
        }

        public bool CanAssignTo(IType type) => 
            this.type.CanAssignTo(type);

        internal void Clear()
        {
            this.InitializeFields(this.type);
        }

        [MethodImpl(0x8000)]
        public ILTypeInstance Clone()
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void CopyValueTypeToStack(StackObject* ptr, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        internal IDelegateAdapter GetDelegateAdapter(ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSizeInMemory(HashSet<object> traversedObj)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetSizeInMemory(object obj, HashSet<object> traversedObj)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFields(ILType type)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void PushFieldAddress(int fieldIdx, StackObject* esp, IList<object> managedStack)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe void PushToStack(int fieldIdx, StackObject* esp, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IList<object> managedStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void PushToStackSub(ref StackObject field, int fieldIdx, StackObject* esp, IList<object> managedStack)
        {
        }

        [MethodImpl(0x8000)]
        internal void SetDelegateAdapter(ILMethod method, IDelegateAdapter adapter)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public ILType Type =>
            this.type;

        public StackObject[] Fields =>
            this.fields;

        public virtual bool IsValueType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool Boxed { get; set; }

        public IList<object> ManagedObjects =>
            this.managedObjs;

        public object CLRInstance
        {
            get => 
                this.clrInstance;
            set => 
                (this.clrInstance = value);
        }

        public object this[int index]
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

