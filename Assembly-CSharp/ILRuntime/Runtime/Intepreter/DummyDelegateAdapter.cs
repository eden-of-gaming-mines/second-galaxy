﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Runtime.CompilerServices;

    internal class DummyDelegateAdapter : DelegateAdapter
    {
        public DummyDelegateAdapter()
        {
        }

        protected DummyDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) : base(appdomain, instance, method)
        {
        }

        [MethodImpl(0x8000)]
        public override IDelegateAdapter Clone()
        {
        }

        public override void Combine(System.Delegate dele)
        {
            ThrowAdapterNotFound(base.method);
        }

        public override IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) => 
            new DummyDelegateAdapter(appdomain, instance, method);

        [MethodImpl(0x8000)]
        private void InvokeILMethod()
        {
        }

        public override void Remove(System.Delegate dele)
        {
            ThrowAdapterNotFound(base.method);
        }

        public override System.Delegate Delegate
        {
            get
            {
                ThrowAdapterNotFound(base.method);
                return null;
            }
        }
    }
}

