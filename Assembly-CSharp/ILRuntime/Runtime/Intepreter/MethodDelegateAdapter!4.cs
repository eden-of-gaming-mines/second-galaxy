﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Runtime.CompilerServices;

    internal class MethodDelegateAdapter<T1, T2, T3, T4> : DelegateAdapter
    {
        private Action<T1, T2, T3, T4> action;
        private static InvocationTypes[] pTypes;

        public MethodDelegateAdapter()
        {
        }

        [MethodImpl(0x8000)]
        private MethodDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public override IDelegateAdapter Clone()
        {
        }

        [MethodImpl(0x8000)]
        public override void Combine(System.Delegate dele)
        {
        }

        public override IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) => 
            new MethodDelegateAdapter<T1, T2, T3, T4>(appdomain, instance, method);

        [MethodImpl(0x8000)]
        private void InvokeILMethod(T1 p1, T2 p2, T3 p3, T4 p4)
        {
        }

        [MethodImpl(0x8000)]
        public override void Remove(System.Delegate dele)
        {
        }

        public override System.Delegate Delegate =>
            this.action;
    }
}

