﻿namespace ILRuntime.Runtime.Intepreter
{
    using System;

    internal enum InvocationTypes
    {
        Integer,
        Long,
        Float,
        Double,
        Enum,
        Object
    }
}

