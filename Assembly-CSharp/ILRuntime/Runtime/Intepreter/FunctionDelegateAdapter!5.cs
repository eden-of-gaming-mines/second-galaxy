﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Runtime.CompilerServices;

    internal class FunctionDelegateAdapter<T1, T2, T3, T4, TResult> : DelegateAdapter
    {
        private Func<T1, T2, T3, T4, TResult> action;
        private static InvocationTypes[] pTypes;

        public FunctionDelegateAdapter()
        {
        }

        [MethodImpl(0x8000)]
        private FunctionDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public override IDelegateAdapter Clone()
        {
        }

        [MethodImpl(0x8000)]
        public override void Combine(System.Delegate dele)
        {
        }

        public override IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) => 
            new FunctionDelegateAdapter<T1, T2, T3, T4, TResult>(appdomain, instance, method);

        [MethodImpl(0x8000)]
        private TResult InvokeILMethod(T1 p1, T2 p2, T3 p3, T4 p4)
        {
        }

        [MethodImpl(0x8000)]
        public override void Remove(System.Delegate dele)
        {
        }

        public override System.Delegate Delegate =>
            this.action;
    }
}

