﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Debugger;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ILIntepreter
    {
        private ILRuntime.Runtime.Enviorment.AppDomain domain;
        private RuntimeStack stack;
        private object _lockObj;
        private bool allowUnboundCLRMethod;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <ShouldBreak>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private StepTypes <CurrentStepType>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private unsafe StackObject* <LastStepFrameBase>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <LastStepInstructionIndex>k__BackingField;
        private unsafe StackObject* ValueTypeBasePointer;
        private bool mainthreadLock;

        [MethodImpl(0x8000)]
        public ILIntepreter(ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        public static unsafe StackObject* Add(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) + (sizeof(StackObject) * b)));

        public unsafe void AllocValueType(StackObject* ptr, IType type)
        {
            this.stack.AllocValueType(ptr, type);
        }

        [MethodImpl(0x8000)]
        private void ArraySetValue(Array arr, object obj, int idx)
        {
        }

        [MethodImpl(0x8000)]
        public void Break()
        {
        }

        [MethodImpl(0x8000)]
        private unsafe bool CanCastTo(StackObject* src, StackObject* dst)
        {
        }

        [MethodImpl(0x8000)]
        internal static object CheckAndCloneValueType(object obj, ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckExceptionType(IType catchType, object exception, bool explicitMatch)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearDebugState()
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void CloneStackValueType(StackObject* src, StackObject* dst, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void CopyStackValueType(StackObject* src, StackObject* dst, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void CopyToStack(StackObject* dst, StackObject* src, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void CopyToValueTypeField(StackObject* obj, int idx, StackObject* val, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void CopyValueTypeToStack(StackObject* dst, object ins, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void DumpStack(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        internal unsafe StackObject* Execute(ILMethod method, StackObject* esp, out bool unhandledException)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void Free(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void FreeStackValueType(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        private ExceptionHandler GetCorrespondingExceptionHandler(ILMethod method, object obj, int addr, ExceptionHandlerType type, bool explicitMatch)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* GetObjectAndResolveReference(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void LoadFromArrayReference(object obj, int idx, StackObject* objRef, IType t, IList<object> mStack)
        {
            // Invalid method body.
        }

        [MethodImpl(0x8000)]
        private unsafe void LoadFromArrayReference(object obj, int idx, StackObject* objRef, Type nT, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void LoadFromFieldReference(object obj, int idx, StackObject* dst, IList<object> mStack)
        {
        }

        public static unsafe StackObject* Minus(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) - (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        public static unsafe StackObject* PushNull(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* PushObject(StackObject* esp, IList<object> mStack, object obj, bool isBox = false)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* PushOne(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe StackObject* PushParameters(IMethod method, StackObject* esp, object[] p)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* PushZero(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe StackObject* ResolveReference(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public void Resume()
        {
        }

        [MethodImpl(0x8000)]
        public unsafe double RetriveDouble(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe float RetriveFloat(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe int RetriveInt32(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe long RetriveInt64(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe object RetriveObject(StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public object Run(ILMethod method, object instance, object[] p)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void StLocSub(StackObject* esp, StackObject* v, StackObject* bp, int idx, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void StoreIntValueToArray(Array arr, StackObject* val, StackObject* idx)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void StoreValueToArrayReference(StackObject* objRef, StackObject* val, IType t, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void StoreValueToArrayReference(StackObject* objRef, StackObject* val, Type nT, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void StoreValueToFieldReference(object obj, int idx, StackObject* val, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe void UnboxObject(StackObject* esp, object obj, IList<object> mStack = null, ILRuntime.Runtime.Enviorment.AppDomain domain = null)
        {
        }

        internal RuntimeStack Stack =>
            this.stack;

        public bool ShouldBreak { get; set; }

        public StepTypes CurrentStepType { get; set; }

        public StackObject* LastStepFrameBase { get; set; }

        public int LastStepInstructionIndex { get; set; }

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            this.domain;

        [CompilerGenerated]
        private sealed class <Execute>c__AnonStorey0
        {
            internal int addr;

            [MethodImpl(0x8000)]
            internal bool <>m__0(ExceptionHandler e)
            {
            }
        }
    }
}

