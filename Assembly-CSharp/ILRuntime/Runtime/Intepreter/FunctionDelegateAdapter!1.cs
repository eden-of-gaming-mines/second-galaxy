﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Runtime.CompilerServices;

    internal class FunctionDelegateAdapter<TResult> : DelegateAdapter
    {
        private Func<TResult> action;
        private static InvocationTypes[] pTypes;

        public FunctionDelegateAdapter()
        {
        }

        [MethodImpl(0x8000)]
        private FunctionDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public override IDelegateAdapter Clone()
        {
        }

        [MethodImpl(0x8000)]
        public override void Combine(System.Delegate dele)
        {
        }

        public override IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) => 
            new FunctionDelegateAdapter<TResult>(appdomain, instance, method);

        [MethodImpl(0x8000)]
        private TResult InvokeILMethod()
        {
        }

        [MethodImpl(0x8000)]
        public override void Remove(System.Delegate dele)
        {
        }

        public override System.Delegate Delegate =>
            this.action;
    }
}

