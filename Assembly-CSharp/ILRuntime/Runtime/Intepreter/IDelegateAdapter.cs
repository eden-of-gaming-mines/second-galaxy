﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;

    internal interface IDelegateAdapter
    {
        IDelegateAdapter Clone();
        void Combine(IDelegateAdapter adapter);
        void Combine(System.Delegate dele);
        bool Equals(IDelegateAdapter adapter);
        bool Equals(System.Delegate dele);
        System.Delegate GetConvertor(Type type);
        unsafe StackObject* ILInvoke(ILIntepreter intp, StackObject* esp, IList<object> mStack);
        IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method);
        void Remove(IDelegateAdapter adapter);
        void Remove(System.Delegate dele);

        System.Delegate Delegate { get; }

        IDelegateAdapter Next { get; }

        ILTypeInstance Instance { get; }

        ILMethod Method { get; }

        bool IsClone { get; }
    }
}

