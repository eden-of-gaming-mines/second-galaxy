﻿namespace ILRuntime.Runtime.Intepreter.OpCodes
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct OpCode
    {
        public OpCodeEnum Code;
        public int TokenInteger;
        public long TokenLong;
    }
}

