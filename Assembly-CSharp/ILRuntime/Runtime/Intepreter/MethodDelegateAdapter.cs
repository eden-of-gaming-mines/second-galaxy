﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Runtime.CompilerServices;

    internal class MethodDelegateAdapter : DelegateAdapter
    {
        private Action action;

        public MethodDelegateAdapter()
        {
        }

        [MethodImpl(0x8000)]
        protected MethodDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        public override IDelegateAdapter Clone()
        {
        }

        [MethodImpl(0x8000)]
        public override void Combine(System.Delegate dele)
        {
        }

        public override IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method) => 
            new MethodDelegateAdapter(appdomain, instance, method);

        [MethodImpl(0x8000)]
        private void InvokeILMethod()
        {
        }

        [MethodImpl(0x8000)]
        public override void Remove(System.Delegate dele)
        {
        }

        public override System.Delegate Delegate =>
            this.action;
    }
}

