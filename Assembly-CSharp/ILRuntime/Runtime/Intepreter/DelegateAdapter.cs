﻿namespace ILRuntime.Runtime.Intepreter
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal abstract class DelegateAdapter : ILTypeInstance, IDelegateAdapter
    {
        protected ILMethod method;
        protected ILTypeInstance instance;
        protected ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private Dictionary<Type, System.Delegate> converters;
        private IDelegateAdapter next;
        protected bool isClone;

        protected DelegateAdapter()
        {
        }

        [MethodImpl(0x8000)]
        protected DelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe StackObject* ClearStack(ILIntepreter intp, StackObject* esp, StackObject* ebp, IList<object> mStack)
        {
        }

        public abstract IDelegateAdapter Clone();
        [MethodImpl(0x8000)]
        public virtual void Combine(IDelegateAdapter adapter)
        {
        }

        public abstract void Combine(System.Delegate dele);
        [MethodImpl(0x8000)]
        public virtual bool Equals(IDelegateAdapter adapter)
        {
        }

        public virtual bool Equals(System.Delegate dele) => 
            (this.Delegate == dele);

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public System.Delegate GetConvertor(Type type)
        {
        }

        [MethodImpl(0x8000)]
        protected static InvocationTypes GetInvocationType<T>()
        {
        }

        [MethodImpl(0x8000)]
        public unsafe StackObject* ILInvoke(ILIntepreter intp, StackObject* esp, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe StackObject* ILInvokeSub(ILIntepreter intp, StackObject* esp, IList<object> mStack)
        {
        }

        public abstract IDelegateAdapter Instantiate(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method);
        private unsafe StackObject* Minus(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) - (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        protected static void PushParameter<T>(ref InvocationContext ctx, InvocationTypes type, T val)
        {
        }

        [MethodImpl(0x8000)]
        protected static T ReadResult<T>(ref InvocationContext ctx, InvocationTypes type)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Remove(IDelegateAdapter adapter)
        {
        }

        public abstract void Remove(System.Delegate dele);
        [MethodImpl(0x8000)]
        public static void ThrowAdapterNotFound(IMethod method)
        {
        }

        public override string ToString() => 
            this.method.ToString();

        public abstract System.Delegate Delegate { get; }

        public IDelegateAdapter Next =>
            this.next;

        public ILTypeInstance Instance =>
            this.instance;

        public ILMethod Method =>
            this.method;

        public override bool IsValueType =>
            false;

        public bool IsClone =>
            this.isClone;
    }
}

