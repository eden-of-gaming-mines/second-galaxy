﻿namespace ILRuntime.Runtime.Adaptors
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Runtime.CompilerServices;

    internal class AttributeAdaptor : CrossBindingAdaptor
    {
        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance) => 
            new Adaptor(appdomain, instance);

        public override Type AdaptorType =>
            typeof(Adaptor);

        public override Type BaseCLRType =>
            typeof(Attribute);

        private class Adaptor : Attribute, CrossBindingAdaptorType
        {
            private ILTypeInstance instance;
            private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            private bool isToStringGot;
            private IMethod toString;

            [MethodImpl(0x8000)]
            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
            }

            [MethodImpl(0x8000)]
            public override string ToString()
            {
            }

            public ILTypeInstance ILInstance =>
                this.instance;
        }
    }
}

