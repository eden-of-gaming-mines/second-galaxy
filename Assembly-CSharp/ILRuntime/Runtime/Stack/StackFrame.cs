﻿namespace ILRuntime.Runtime.Stack
{
    using ILRuntime.CLR.Method;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct StackFrame
    {
        public ILMethod Method;
        public unsafe StackObject* LocalVarPointer;
        public unsafe StackObject* BasePointer;
        public unsafe StackObject* ValueTypeBasePointer;
        public IntegerReference Address;
        public int ManagedStackBase;
    }
}

