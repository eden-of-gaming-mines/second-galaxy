﻿namespace ILRuntime.Runtime.Stack
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct StackObject
    {
        public static StackObject Null;
        public ObjectTypes ObjectType;
        public int Value;
        public int ValueLow;
        [MethodImpl(0x8000)]
        public static bool operator ==(StackObject a, StackObject b)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator !=(StackObject a, StackObject b)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe object ToObject(StackObject* esp, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public static void Initialized(ref StackObject esp, int idx, Type t, IType fieldType, IList<object> mStack)
        {
        }

        [MethodImpl(0x8000)]
        public static unsafe void Initialized(StackObject* esp, IType type)
        {
        }

        [MethodImpl(0x8000)]
        static StackObject()
        {
        }
    }
}

