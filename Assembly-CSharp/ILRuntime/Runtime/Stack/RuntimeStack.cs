﻿namespace ILRuntime.Runtime.Stack
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class RuntimeStack : IDisposable
    {
        private ILIntepreter intepreter;
        private unsafe StackObject* pointer;
        private unsafe StackObject* endOfMemory;
        private unsafe StackObject* valueTypePtr;
        private IntPtr nativePointer;
        private IList<object> managedStack;
        private Stack<StackFrame> frames;
        public const int MAXIMAL_STACK_OBJECTS = 0x4000;

        [MethodImpl(0x8000)]
        public RuntimeStack(ILIntepreter intepreter)
        {
        }

        private unsafe StackObject* Add(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) + (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        public unsafe void AllocValueType(StackObject* ptr, IType type)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void ClearValueTypeObject(IType type, StackObject* ptr)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void CountValueTypeManaged(StackObject* esp, ref int start, ref int end, StackObject** endAddr)
        {
        }

        [MethodImpl(0x8000)]
        public void Dispose()
        {
        }

        [MethodImpl(0x8000)]
        protected override void Finalize()
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void FreeValueTypeObject(StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe void InitializeFrame(ILMethod method, StackObject* esp, out StackFrame res)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void InitializeValueTypeObject(IType type, StackObject* ptr)
        {
        }

        [MethodImpl(0x8000)]
        public unsafe StackObject* PopFrame(ref StackFrame frame, StackObject* esp)
        {
        }

        [MethodImpl(0x8000)]
        public void PushFrame(ref StackFrame frame)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void RelocateValueType(StackObject* src, ref StackObject* dst, ref int mStackBase)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetValueTypePointer()
        {
        }

        public Stack<StackFrame> Frames =>
            this.frames;

        public StackObject* StackBase =>
            this.pointer;

        public StackObject* ValueTypeStackPointer =>
            this.valueTypePtr;

        public StackObject* ValueTypeStackBase =>
            (this.endOfMemory - 1);

        public IList<object> ManagedStack =>
            this.managedStack;
    }
}

