﻿namespace ILRuntime.Runtime.Stack
{
    using System;

    public enum ObjectTypes
    {
        Null,
        Integer,
        Long,
        Float,
        Double,
        StackObjectReference,
        StaticFieldReference,
        ValueTypeObjectReference,
        ValueTypeDescriptor,
        Object,
        FieldReference,
        ArrayReference
    }
}

