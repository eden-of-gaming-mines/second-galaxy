﻿namespace ILRuntime.Runtime.Stack
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    internal class IntegerReference
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Value>k__BackingField;

        public int Value { get; set; }
    }
}

