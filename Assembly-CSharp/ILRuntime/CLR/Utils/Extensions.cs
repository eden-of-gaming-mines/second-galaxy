﻿namespace ILRuntime.CLR.Utils
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Extensions
    {
        public static List<IType> EmptyParamList;
        private static readonly Dictionary<System.Type, TypeFlags> typeFlags;

        [MethodImpl(0x8000)]
        public static object CheckCLRTypes(this System.Type pt, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckMethodParams(this MethodInfo m, ParameterInfo[] args)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckMethodParams(this MethodInfo m, System.Type[] args)
        {
        }

        public static bool FastIsByRef(this System.Type pt) => 
            ((pt.GetTypeFlags() & TypeFlags.IsByRef) != TypeFlags.Default);

        public static bool FastIsEnum(this System.Type pt) => 
            ((pt.GetTypeFlags() & TypeFlags.IsEnum) != TypeFlags.Default);

        public static bool FastIsPrimitive(this System.Type pt) => 
            ((pt.GetTypeFlags() & TypeFlags.IsPrimitive) != TypeFlags.Default);

        [MethodImpl(0x8000)]
        public static bool FastIsValueType(this System.Type pt)
        {
        }

        [MethodImpl(0x8000)]
        public static List<IType> GetParamList(this MethodReference def, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IType contextType, IMethod contextMethod, IType[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        public static TypeFlags GetTypeFlags(this System.Type pt)
        {
        }

        [MethodImpl(0x8000)]
        private static string ReplaceGenericArgument(string typename, string argumentName, string argumentType, bool isGA = false)
        {
        }

        [Flags]
        public enum TypeFlags
        {
            Default = 0,
            IsPrimitive = 1,
            IsByRef = 2,
            IsEnum = 4,
            IsDelegate = 8,
            IsValueType = 0x10
        }
    }
}

