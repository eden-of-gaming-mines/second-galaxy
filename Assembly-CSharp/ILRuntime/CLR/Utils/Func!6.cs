﻿namespace ILRuntime.CLR.Utils
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TResult Func<T1, T2, T3, T4, T5, TResult>(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5);
}

