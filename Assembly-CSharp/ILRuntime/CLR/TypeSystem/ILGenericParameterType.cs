﻿namespace ILRuntime.CLR.TypeSystem
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ILGenericParameterType : IType
    {
        private string name;
        private ILGenericParameterType arrayType;

        public ILGenericParameterType(string name)
        {
            this.name = name;
        }

        public bool CanAssignTo(IType type) => 
            false;

        public IType FindGenericArgument(string key) => 
            null;

        public IMethod GetConstructor(List<IType> param) => 
            null;

        public int GetFieldIndex(object token) => 
            -1;

        public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false) => 
            null;

        public IMethod GetMethod(string name, List<IType> param, IType[] genericArguments, IType returnType = null, bool declaredOnly = false) => 
            null;

        public List<IMethod> GetMethods() => 
            null;

        public IMethod GetVirtualMethod(IMethod method) => 
            method;

        [MethodImpl(0x8000)]
        public IType MakeArrayType(int rank)
        {
        }

        public IType MakeByRefType() => 
            this;

        public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments) => 
            null;

        public IType ResolveGenericType(IType contextType)
        {
            throw new NotImplementedException();
        }

        public bool IsGenericInstance =>
            false;

        public KeyValuePair<string, IType>[] GenericArguments =>
            null;

        public bool HasGenericParameter =>
            true;

        public bool IsGenericParameter =>
            true;

        public Type TypeForCLR =>
            typeof(ILGenericParameterType);

        public string FullName =>
            this.name;

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            null;

        public IType ByRefType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IType ArrayType =>
            this.arrayType;

        public bool IsValueType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsPrimitive =>
            false;

        public bool IsEnum =>
            false;

        public bool IsInterface =>
            false;

        public string Name =>
            this.name;

        public bool IsDelegate =>
            false;

        public Type ReflectionType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IType BaseType =>
            null;

        public bool IsArray =>
            false;

        public bool IsByRef =>
            false;

        public IType ElementType =>
            null;

        public int ArrayRank =>
            1;

        public IType[] Implements =>
            null;
    }
}

