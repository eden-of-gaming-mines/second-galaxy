﻿namespace ILRuntime.CLR.TypeSystem
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Reflection;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ILType : IType
    {
        private Dictionary<string, List<ILMethod>> methods;
        private ILRuntime.Mono.Cecil.TypeReference typeRef;
        private ILRuntime.Mono.Cecil.TypeDefinition definition;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private ILMethod staticConstructor;
        private List<ILMethod> constructors;
        private IType[] fieldTypes;
        private FieldDefinition[] fieldDefinitions;
        private IType[] staticFieldTypes;
        private FieldDefinition[] staticFieldDefinitions;
        private Dictionary<string, int> fieldMapping;
        private Dictionary<string, int> staticFieldMapping;
        private ILTypeStaticInstance staticInstance;
        private Dictionary<int, int> fieldTokenMapping;
        private int fieldStartIdx;
        private int totalFieldCnt;
        private KeyValuePair<string, IType>[] genericArguments;
        private IType baseType;
        private IType byRefType;
        private IType enumType;
        private IType elementType;
        private Dictionary<int, IType> arrayTypes;
        private System.Type arrayCLRType;
        private System.Type byRefCLRType;
        private IType[] interfaces;
        private bool baseTypeInitialized;
        private bool interfaceInitialized;
        private List<ILType> genericInstances;
        private bool isDelegate;
        private ILRuntimeType reflectionType;
        private ILType genericDefinition;
        private IType firstCLRBaseType;
        private IType firstCLRInterface;
        private int hashCode;
        private static int instance_id = 0x10000000;
        private bool mToStringGot;
        private bool mEqualsGot;
        private bool mGetHashCodeGot;
        private IMethod mToString;
        private IMethod mEquals;
        private IMethod mGetHashCode;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsArray>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ArrayRank>k__BackingField;
        private bool? isValueType;
        private string fullName;

        [MethodImpl(0x8000)]
        public ILType(ILRuntime.Mono.Cecil.TypeReference def, ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CanAssignTo(IType type)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckGenericArguments(ILMethod i, IType[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        private ILMethod CheckGenericParams(ILMethod i, List<IType> param, ref bool match)
        {
        }

        [MethodImpl(0x8000)]
        public IType FindGenericArgument(string key)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetConstructor(List<IType> param)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetConstructor(int paramCnt)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILMethod> GetConstructors()
        {
        }

        [MethodImpl(0x8000)]
        public IType GetField(int fieldIdx, out FieldDefinition fd)
        {
        }

        [MethodImpl(0x8000)]
        public IType GetField(string name, out int fieldIdx)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFieldIndex(object token)
        {
        }

        public ILType GetGenericDefinition() => 
            this.genericDefinition;

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name, List<IType> param, IType[] genericArguments, IType returnType = null, bool declaredOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMethodBodySizeInMemory()
        {
        }

        [MethodImpl(0x8000)]
        public List<IMethod> GetMethods()
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetStaticConstroctor()
        {
        }

        [MethodImpl(0x8000)]
        public int GetStaticFieldSizeInMemory(HashSet<object> traversed)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetVirtualMethod(IMethod method)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeBaseType()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFields()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeInterfaces()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethods()
        {
        }

        [MethodImpl(0x8000)]
        public ILTypeInstance Instantiate(bool callDefaultConstructor = true)
        {
        }

        [MethodImpl(0x8000)]
        public ILTypeInstance Instantiate(object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeArrayType(int rank)
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeByRefType()
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        public IType ResolveGenericType(IType contextType)
        {
        }

        [MethodImpl(0x8000)]
        private void RetriveDefinitino(ILRuntime.Mono.Cecil.TypeReference def)
        {
        }

        public override string ToString() => 
            this.FullName;

        public ILRuntime.Mono.Cecil.TypeDefinition TypeDefinition =>
            this.definition;

        public IMethod ToStringMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IMethod EqualsMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IMethod GetHashCodeMethod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Mono.Cecil.TypeReference TypeReference
        {
            get => 
                this.typeRef;
            set
            {
                this.typeRef = value;
                this.RetriveDefinitino(value);
            }
        }

        public IType BaseType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType[] Implements
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILTypeStaticInstance StaticInstance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType[] FieldTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType[] StaticFieldTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public FieldDefinition[] StaticFieldDefinitions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Dictionary<string, int> FieldMapping
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType FirstCLRBaseType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType FirstCLRInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Dictionary<string, int> StaticFieldMapping =>
            this.staticFieldMapping;

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            this.appdomain;

        internal int FieldStartIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int TotalFieldCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGenericInstance =>
            (this.genericArguments != null);

        public KeyValuePair<string, IType>[] GenericArguments =>
            this.genericArguments;

        public IType ElementType =>
            this.elementType;

        public bool IsArray { get; private set; }

        public int ArrayRank { get; private set; }

        public bool IsByRef =>
            this.typeRef.IsByReference;

        public bool IsValueType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsDelegate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsPrimitive =>
            false;

        public bool IsInterface =>
            this.TypeDefinition.IsInterface;

        public System.Type TypeForCLR
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public System.Type ReflectionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType ByRefType =>
            this.byRefType;

        public IType ArrayType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsEnum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string FullName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Name =>
            this.typeRef.Name;
    }
}

