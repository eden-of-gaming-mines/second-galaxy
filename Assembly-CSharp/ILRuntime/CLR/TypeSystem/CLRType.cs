﻿namespace ILRuntime.CLR.TypeSystem
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Reflection;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CLRType : IType
    {
        private Type clrType;
        private bool isPrimitive;
        private bool isValueType;
        private bool isEnum;
        private Dictionary<string, List<CLRMethod>> methods;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private List<CLRMethod> constructors;
        private KeyValuePair<string, IType>[] genericArguments;
        private List<CLRType> genericInstances;
        private Dictionary<string, int> fieldMapping;
        private Dictionary<int, FieldInfo> fieldInfoCache;
        private Dictionary<int, CLRFieldGetterDelegate> fieldGetterCache;
        private Dictionary<int, CLRFieldSetterDelegate> fieldSetterCache;
        private Dictionary<int, int> fieldIdxMapping;
        private IType[] orderedFieldTypes;
        private CLRMemberwiseCloneDelegate memberwiseCloneDelegate;
        private CLRCreateDefaultInstanceDelegate createDefaultInstanceDelegate;
        private CLRCreateArrayInstanceDelegate createArrayInstanceDelegate;
        private Dictionary<int, int> fieldTokenMapping;
        private IType byRefType;
        private IType elementType;
        private Dictionary<int, IType> arrayTypes;
        private IType[] interfaces;
        private bool isDelegate;
        private IType baseType;
        private bool isBaseTypeInitialized;
        private bool interfaceInitialized;
        private bool valueTypeBinderGot;
        private ILRuntimeWrapperType wraperType;
        private ILRuntime.Runtime.Enviorment.ValueTypeBinder valueTypeBinder;
        private int hashCode;
        private static int instance_id = 0x20000000;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsArray>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ArrayRank>k__BackingField;

        [MethodImpl(0x8000)]
        public CLRType(Type clrType, ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CanAssignTo(IType type)
        {
        }

        [MethodImpl(0x8000)]
        public object CreateArrayInstance(int size)
        {
        }

        [MethodImpl(0x8000)]
        public object CreateDefaultInstance()
        {
        }

        [MethodImpl(0x8000)]
        public IType FindGenericArgument(string key)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetConstructor(List<IType> param)
        {
        }

        [MethodImpl(0x8000)]
        public FieldInfo GetField(int hash)
        {
        }

        [MethodImpl(0x8000)]
        private CLRFieldGetterDelegate GetFieldGetter(int hash)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFieldIndex(object token)
        {
        }

        [MethodImpl(0x8000)]
        private CLRFieldSetterDelegate GetFieldSetter(int hash)
        {
        }

        [MethodImpl(0x8000)]
        public object GetFieldValue(int hash, object target)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetMethod(string name, List<IType> param, IType[] genericArguments, IType returnType = null, bool declaredOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public List<IMethod> GetMethods()
        {
        }

        [MethodImpl(0x8000)]
        public IMethod GetVirtualMethod(IMethod method)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeBaseType()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeFields()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeInterfaces()
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeMethods()
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeArrayType(int rank)
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeByRefType()
        {
        }

        [MethodImpl(0x8000)]
        public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        private bool MatchGenericParameters(Type[] args, Type type, Type q, IType[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        public object PerformMemberwiseClone(object target)
        {
        }

        public IType ResolveGenericType(IType contextType)
        {
            throw new NotImplementedException();
        }

        [MethodImpl(0x8000)]
        public void SetFieldValue(int hash, ref object target, object value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStaticFieldValue(int hash, object value)
        {
        }

        public override string ToString() => 
            this.clrType.ToString();

        public Dictionary<int, FieldInfo> Fields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Dictionary<int, int> FieldIndexMapping =>
            this.fieldIdxMapping;

        public IType[] OrderedFieldTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int TotalFieldCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Runtime.Enviorment.AppDomain AppDomain =>
            this.appdomain;

        public bool IsGenericInstance =>
            (this.genericArguments != null);

        public KeyValuePair<string, IType>[] GenericArguments =>
            this.genericArguments;

        public IType ElementType =>
            this.elementType;

        public bool HasGenericParameter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGenericParameter =>
            this.clrType.IsGenericParameter;

        public bool IsInterface =>
            this.clrType.IsInterface;

        public Type TypeForCLR =>
            this.clrType;

        public Type ReflectionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType ByRefType =>
            this.byRefType;

        public IType ArrayType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsArray { get; private set; }

        public int ArrayRank { get; private set; }

        public bool IsValueType =>
            this.isValueType;

        public bool IsByRef =>
            this.clrType.IsByRef;

        public bool IsDelegate =>
            this.isDelegate;

        public bool IsPrimitive =>
            this.isPrimitive;

        public bool IsEnum =>
            this.isEnum;

        public string FullName =>
            this.clrType.FullName;

        public string Name =>
            this.clrType.Name;

        public IType BaseType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType[] Implements
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ILRuntime.Runtime.Enviorment.ValueTypeBinder ValueTypeBinder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PerformMemberwiseClone>c__AnonStorey0
        {
            internal MethodInfo memberwiseClone;

            internal object <>m__0(ref object t) => 
                this.memberwiseClone.Invoke(t, null);
        }
    }
}

