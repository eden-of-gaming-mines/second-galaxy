﻿namespace ILRuntime.CLR.TypeSystem
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IType
    {
        bool CanAssignTo(IType type);
        IType FindGenericArgument(string key);
        IMethod GetConstructor(List<IType> param);
        int GetFieldIndex(object token);
        IMethod GetMethod(string name, int paramCount, bool declaredOnly = false);
        IMethod GetMethod(string name, List<IType> param, IType[] genericArguments, IType returnType = null, bool declaredOnly = false);
        List<IMethod> GetMethods();
        IMethod GetVirtualMethod(IMethod method);
        IType MakeArrayType(int rank);
        IType MakeByRefType();
        IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments);
        IType ResolveGenericType(IType contextType);

        bool IsGenericInstance { get; }

        KeyValuePair<string, IType>[] GenericArguments { get; }

        Type TypeForCLR { get; }

        Type ReflectionType { get; }

        IType BaseType { get; }

        IType[] Implements { get; }

        IType ByRefType { get; }

        IType ArrayType { get; }

        string FullName { get; }

        string Name { get; }

        bool IsArray { get; }

        int ArrayRank { get; }

        bool IsValueType { get; }

        bool IsDelegate { get; }

        bool IsPrimitive { get; }

        bool IsEnum { get; }

        bool IsByRef { get; }

        bool IsInterface { get; }

        IType ElementType { get; }

        bool HasGenericParameter { get; }

        bool IsGenericParameter { get; }

        ILRuntime.Runtime.Enviorment.AppDomain AppDomain { get; }
    }
}

