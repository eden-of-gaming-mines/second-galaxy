﻿namespace ILRuntime.CLR.Method
{
    using ILRuntime.CLR.TypeSystem;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    internal class ExceptionHandler
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ExceptionHandlerType <HandlerType>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <TryStart>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TryEnd>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <HandlerStart>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <HandlerEnd>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private IType <CatchType>k__BackingField;

        public ExceptionHandlerType HandlerType { get; set; }

        public int TryStart { get; set; }

        public int TryEnd { get; set; }

        public int HandlerStart { get; set; }

        public int HandlerEnd { get; set; }

        public IType CatchType { get; set; }
    }
}

