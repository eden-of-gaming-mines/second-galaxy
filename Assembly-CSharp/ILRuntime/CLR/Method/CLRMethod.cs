﻿namespace ILRuntime.CLR.Method
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Stack;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CLRMethod : IMethod
    {
        private System.Reflection.MethodInfo def;
        private System.Reflection.ConstructorInfo cDef;
        private List<IType> parameters;
        private ParameterInfo[] parametersCLR;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private CLRType declaringType;
        private ParameterInfo[] param;
        private bool isConstructor;
        private CLRRedirectionDelegate redirect;
        private IType[] genericArguments;
        private Type[] genericArgumentsCLR;
        private object[] invocationParam;
        private bool isDelegateInvoke;
        private int hashCode;
        private static int instance_id = 0x20000000;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private IType <ReturnType>k__BackingField;

        [MethodImpl(0x8000)]
        internal CLRMethod(System.Reflection.ConstructorInfo def, CLRType type, ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        [MethodImpl(0x8000)]
        internal CLRMethod(System.Reflection.MethodInfo def, CLRType type, ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        [MethodImpl(0x8000)]
        private unsafe void FixReference(int paramCount, StackObject* esp, object[] param, IList<object> mStack, object instance, bool hasThis)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParameters()
        {
        }

        [MethodImpl(0x8000)]
        public unsafe object Invoke(ILIntepreter intepreter, StackObject* esp, IList<object> mStack, bool isNewObj = false)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod MakeGenericMethod(IType[] genericArguments)
        {
        }

        private unsafe StackObject* Minus(StackObject* a, int b) => 
            ((StackObject*) (((ulong) a) - (sizeof(StackObject) * b)));

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public IType DeclearingType =>
            this.declaringType;

        public string Name =>
            this.def.Name;

        public bool HasThis
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int GenericParameterCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGenericInstance =>
            (this.genericArguments != null);

        public bool IsDelegateInvoke =>
            this.isDelegateInvoke;

        public bool IsStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CLRRedirectionDelegate Redirection =>
            this.redirect;

        public System.Reflection.MethodInfo MethodInfo =>
            this.def;

        public System.Reflection.ConstructorInfo ConstructorInfo =>
            this.cDef;

        public IType[] GenericArguments =>
            this.genericArguments;

        public Type[] GenericArgumentsCLR
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int ParameterCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<IType> Parameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ParameterInfo[] ParametersCLR
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType ReturnType { get; private set; }

        public bool IsConstructor =>
            !ReferenceEquals(this.cDef, null);
    }
}

