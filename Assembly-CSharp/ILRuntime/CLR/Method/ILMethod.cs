﻿namespace ILRuntime.CLR.Method
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Cecil.Cil;
    using ILRuntime.Mono.Collections.Generic;
    using ILRuntime.Reflection;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using ILRuntime.Runtime.Intepreter.OpCodes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILMethod : IMethod
    {
        private ILRuntime.Runtime.Intepreter.OpCodes.OpCode[] body;
        private MethodDefinition def;
        private List<IType> parameters;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private ILType declaringType;
        private ILRuntime.CLR.Method.ExceptionHandler[] exceptionHandler;
        private KeyValuePair<string, IType>[] genericParameters;
        private IType[] genericArguments;
        private Dictionary<int, int[]> jumptables;
        private bool isDelegateInvoke;
        private ILRuntimeMethodInfo refletionMethodInfo;
        private ILRuntimeConstructorInfo reflectionCtorInfo;
        private int paramCnt;
        private int localVarCnt;
        private Collection<VariableDefinition> variables;
        private int hashCode;
        private static int instance_id = 0x10000000;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private IDelegateAdapter <DelegateAdapter>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <StartLine>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <EndLine>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private IType <ReturnType>k__BackingField;
        private string cachedName;

        [MethodImpl(0x8000)]
        public ILMethod(MethodDefinition def, ILType type, ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckHasGenericParamter(object token)
        {
        }

        [MethodImpl(0x8000)]
        public IType FindGenericArgument(string name)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        private int GetTypeTokenHashCode(object token)
        {
        }

        [MethodImpl(0x8000)]
        private SequencePoint GetValidSequence(int startIdx, int dir)
        {
        }

        [MethodImpl(0x8000)]
        private void InitCodeBody()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParameters()
        {
        }

        [MethodImpl(0x8000)]
        private void InitToken(ref ILRuntime.Runtime.Intepreter.OpCodes.OpCode code, object token, Dictionary<Instruction, int> addr)
        {
        }

        [MethodImpl(0x8000)]
        public IMethod MakeGenericMethod(IType[] genericArguments)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareJumpTable(object token, Dictionary<Instruction, int> addr)
        {
        }

        [MethodImpl(0x8000)]
        public void Prewarm()
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        public MethodDefinition Definition =>
            this.def;

        public Dictionary<int, int[]> JumpTables =>
            this.jumptables;

        internal IDelegateAdapter DelegateAdapter { get; set; }

        internal int StartLine { get; set; }

        internal int EndLine { get; set; }

        public MethodInfo ReflectionMethodInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConstructorInfo ReflectionConstructorInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        internal ILRuntime.CLR.Method.ExceptionHandler[] ExceptionHandler
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Name =>
            this.def.Name;

        public IType DeclearingType =>
            this.declaringType;

        public bool HasThis =>
            this.def.HasThis;

        public int GenericParameterCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGenericInstance =>
            (this.genericParameters != null);

        public Collection<VariableDefinition> Variables =>
            this.variables;

        public KeyValuePair<string, IType>[] GenericArguments =>
            this.genericParameters;

        public IType[] GenericArugmentsArray =>
            this.genericArguments;

        internal ILRuntime.Runtime.Intepreter.OpCodes.OpCode[] Body
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool HasBody =>
            (this.body != null);

        public int LocalVariableCount =>
            this.localVarCnt;

        public bool IsConstructor =>
            this.def.IsConstructor;

        public bool IsDelegateInvoke =>
            this.isDelegateInvoke;

        public bool IsStatic =>
            this.def.IsStatic;

        public int ParameterCount =>
            this.paramCnt;

        public List<IType> Parameters
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public IType ReturnType { get; private set; }
    }
}

