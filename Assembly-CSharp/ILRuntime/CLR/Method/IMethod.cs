﻿namespace ILRuntime.CLR.Method
{
    using ILRuntime.CLR.TypeSystem;
    using System;
    using System.Collections.Generic;

    public interface IMethod
    {
        IMethod MakeGenericMethod(IType[] genericArguments);

        string Name { get; }

        int ParameterCount { get; }

        bool HasThis { get; }

        IType DeclearingType { get; }

        IType ReturnType { get; }

        List<IType> Parameters { get; }

        int GenericParameterCount { get; }

        bool IsGenericInstance { get; }

        bool IsConstructor { get; }

        bool IsDelegateInvoke { get; }

        bool IsStatic { get; }
    }
}

