﻿namespace ILRuntime.CLR.Method
{
    using System;

    internal enum ExceptionHandlerType
    {
        Catch,
        Finally,
        Fault
    }
}

