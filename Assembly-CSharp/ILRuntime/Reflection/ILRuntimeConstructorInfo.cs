﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.Method;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimeConstructorInfo : ConstructorInfo
    {
        private ILRuntime.CLR.Method.ILMethod method;
        private ILRuntimeParameterInfo[] parameters;

        [MethodImpl(0x8000)]
        public ILRuntimeConstructorInfo(ILRuntime.CLR.Method.ILMethod m)
        {
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override MethodImplAttributes GetMethodImplementationFlags()
        {
            throw new NotImplementedException();
        }

        public override ParameterInfo[] GetParameters() => 
            this.parameters;

        [MethodImpl(0x8000)]
        public override object Invoke(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
        {
        }

        [MethodImpl(0x8000)]
        public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
        {
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        internal ILRuntime.CLR.Method.ILMethod ILMethod =>
            this.method;

        public override MethodAttributes Attributes =>
            MethodAttributes.Public;

        public override Type DeclaringType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override RuntimeMethodHandle MethodHandle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string Name =>
            this.method.Name;

        public override Type ReflectedType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

