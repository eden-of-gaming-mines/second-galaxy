﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimeFieldInfo : FieldInfo
    {
        private System.Reflection.FieldAttributes attr;
        private ILRuntimeType dType;
        private ILType ilType;
        private IType fieldType;
        private bool isStatic;
        private int fieldIdx;
        private string name;
        private FieldDefinition definition;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private object[] customAttributes;
        private System.Type[] attributeTypes;

        [MethodImpl(0x8000)]
        public ILRuntimeFieldInfo(FieldDefinition def, ILRuntimeType declaredType, bool isStatic, int fieldIdx)
        {
        }

        [MethodImpl(0x8000)]
        public ILRuntimeFieldInfo(FieldDefinition def, ILRuntimeType declaredType, int fieldIdx, IType fieldType)
        {
        }

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override object GetValue(object obj)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeCustomAttribute()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsDefined(System.Type attributeType, bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, CultureInfo culture)
        {
        }

        public IType ILFieldType =>
            this.fieldType;

        public override System.Reflection.FieldAttributes Attributes =>
            this.attr;

        public override System.Type DeclaringType =>
            this.dType;

        public override RuntimeFieldHandle FieldHandle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override System.Type FieldType =>
            this.fieldType.ReflectionType;

        public override string Name =>
            this.name;

        public override System.Type ReflectedType =>
            this.fieldType.ReflectionType;
    }
}

