﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.TypeSystem;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimeWrapperType : Type
    {
        private ILRuntime.CLR.TypeSystem.CLRType type;
        private Type et;

        [MethodImpl(0x8000)]
        public ILRuntimeWrapperType(ILRuntime.CLR.TypeSystem.CLRType t)
        {
        }

        protected override TypeAttributes GetAttributeFlagsImpl() => 
            this.et.Attributes;

        [MethodImpl(0x8000)]
        protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
        {
        }

        public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr) => 
            this.et.GetConstructors(bindingAttr);

        public override object[] GetCustomAttributes(bool inherit) => 
            this.et.GetCustomAttributes(inherit);

        public override object[] GetCustomAttributes(Type attributeType, bool inherit) => 
            this.et.GetCustomAttributes(attributeType, inherit);

        public override Type GetElementType() => 
            this.et.GetElementType();

        public override EventInfo GetEvent(string name, BindingFlags bindingAttr) => 
            this.et.GetEvent(name, bindingAttr);

        public override EventInfo[] GetEvents(BindingFlags bindingAttr) => 
            this.et.GetEvents(bindingAttr);

        public override FieldInfo GetField(string name, BindingFlags bindingAttr) => 
            this.et.GetField(name, bindingAttr);

        public override FieldInfo[] GetFields(BindingFlags bindingAttr) => 
            this.et.GetFields(bindingAttr);

        public override Type GetGenericTypeDefinition() => 
            this.et.GetGenericTypeDefinition();

        public override int GetHashCode() => 
            this.type.GetHashCode();

        public override Type GetInterface(string name, bool ignoreCase) => 
            this.et.GetInterface(name, ignoreCase);

        public override Type[] GetInterfaces() => 
            this.et.GetInterfaces();

        public override MemberInfo[] GetMembers(BindingFlags bindingAttr) => 
            this.et.GetMembers(bindingAttr);

        [MethodImpl(0x8000)]
        protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
        {
        }

        public override MethodInfo[] GetMethods(BindingFlags bindingAttr) => 
            this.et.GetMethods(bindingAttr);

        public override Type GetNestedType(string name, BindingFlags bindingAttr) => 
            this.et.GetNestedType(name, bindingAttr);

        public override Type[] GetNestedTypes(BindingFlags bindingAttr) => 
            this.et.GetNestedTypes(bindingAttr);

        public override PropertyInfo[] GetProperties(BindingFlags bindingAttr) => 
            this.et.GetProperties(bindingAttr);

        protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers) => 
            this.et.GetProperty(name, bindingAttr);

        protected override bool HasElementTypeImpl() => 
            this.et.HasElementType;

        [MethodImpl(0x8000)]
        public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
        {
        }

        protected override bool IsArrayImpl() => 
            this.et.IsArray;

        [MethodImpl(0x8000)]
        public override bool IsAssignableFrom(Type c)
        {
        }

        protected override bool IsByRefImpl() => 
            this.et.IsByRef;

        protected override bool IsCOMObjectImpl() => 
            this.et.IsCOMObject;

        public override bool IsDefined(Type attributeType, bool inherit) => 
            this.et.IsDefined(attributeType, inherit);

        [MethodImpl(0x8000)]
        public override bool IsInstanceOfType(object o)
        {
        }

        protected override bool IsPointerImpl() => 
            this.et.IsPointer;

        protected override bool IsPrimitiveImpl() => 
            this.et.IsPrimitive;

        public ILRuntime.CLR.TypeSystem.CLRType CLRType =>
            this.type;

        public Type RealType =>
            this.et;

        public override Guid GUID =>
            this.et.GUID;

        public override System.Reflection.Module Module =>
            this.et.Module;

        public override System.Reflection.Assembly Assembly =>
            this.et.Assembly;

        public override string FullName =>
            this.et.FullName;

        public override string Namespace =>
            this.et.Namespace;

        public override string AssemblyQualifiedName =>
            this.et.AssemblyQualifiedName;

        public override Type BaseType =>
            this.et.BaseType;

        public override Type UnderlyingSystemType =>
            this.et.UnderlyingSystemType;

        public override string Name =>
            this.et.Name;

        public override bool IsGenericType =>
            this.et.IsGenericType;

        public override bool IsGenericTypeDefinition =>
            this.et.IsGenericTypeDefinition;

        public override bool IsGenericParameter =>
            this.et.IsGenericParameter;
    }
}

