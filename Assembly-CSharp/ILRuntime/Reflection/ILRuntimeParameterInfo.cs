﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimeParameterInfo : ParameterInfo
    {
        private IType type;
        private MethodBase method;
        private ParameterDefinition definition;

        [MethodImpl(0x8000)]
        public ILRuntimeParameterInfo(ParameterDefinition definition, IType type, MethodBase method)
        {
        }

        public override System.Type ParameterType =>
            this.type.ReflectionType;
    }
}

