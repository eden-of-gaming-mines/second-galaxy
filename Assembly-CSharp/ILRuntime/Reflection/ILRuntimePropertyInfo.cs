﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.Method;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimePropertyInfo : PropertyInfo
    {
        private ILMethod getter;
        private ILMethod setter;
        private ILType dType;
        private PropertyDefinition definition;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private object[] customAttributes;
        private System.Type[] attributeTypes;

        [MethodImpl(0x8000)]
        public ILRuntimePropertyInfo(PropertyDefinition definition, ILType dType)
        {
        }

        public override MethodInfo[] GetAccessors(bool nonPublic)
        {
            throw new NotImplementedException();
        }

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override MethodInfo GetGetMethod(bool nonPublic)
        {
        }

        public override ParameterInfo[] GetIndexParameters() => 
            new ParameterInfo[0];

        [MethodImpl(0x8000)]
        public override MethodInfo GetSetMethod(bool nonPublic)
        {
        }

        [MethodImpl(0x8000)]
        public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeCustomAttribute()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsDefined(System.Type attributeType, bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
        {
        }

        public ILMethod Getter
        {
            get => 
                this.getter;
            set => 
                (this.getter = value);
        }

        public ILMethod Setter
        {
            get => 
                this.setter;
            set => 
                (this.setter = value);
        }

        public bool IsPublic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override string Name =>
            this.definition.Name;

        public override System.Type ReflectedType =>
            this.dType.ReflectionType;

        public override System.Reflection.PropertyAttributes Attributes =>
            System.Reflection.PropertyAttributes.None;

        public override bool CanRead =>
            !ReferenceEquals(this.getter, null);

        public override bool CanWrite =>
            !ReferenceEquals(this.setter, null);

        public override System.Type PropertyType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override System.Type DeclaringType =>
            this.dType.ReflectionType;
    }
}

