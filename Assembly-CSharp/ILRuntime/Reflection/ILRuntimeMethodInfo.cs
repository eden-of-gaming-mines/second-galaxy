﻿namespace ILRuntime.Reflection
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Runtime.Enviorment;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ILRuntimeMethodInfo : MethodInfo
    {
        private ILRuntime.CLR.Method.ILMethod method;
        private ILRuntimeParameterInfo[] parameters;
        private MethodDefinition definition;
        private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
        private object[] customAttributes;
        private System.Type[] attributeTypes;

        [MethodImpl(0x8000)]
        public ILRuntimeMethodInfo(ILRuntime.CLR.Method.ILMethod m)
        {
        }

        public override MethodInfo GetBaseDefinition() => 
            this;

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(bool inherit)
        {
        }

        [MethodImpl(0x8000)]
        public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
        {
        }

        public override System.Reflection.MethodImplAttributes GetMethodImplementationFlags()
        {
            throw new NotImplementedException();
        }

        public override ParameterInfo[] GetParameters() => 
            this.parameters;

        [MethodImpl(0x8000)]
        private void InitializeCustomAttribute()
        {
        }

        [MethodImpl(0x8000)]
        public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsDefined(System.Type attributeType, bool inherit)
        {
        }

        internal ILRuntime.CLR.Method.ILMethod ILMethod =>
            this.method;

        public override System.Reflection.MethodAttributes Attributes =>
            System.Reflection.MethodAttributes.Public;

        public override System.Type DeclaringType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override RuntimeMethodHandle MethodHandle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string Name =>
            this.method.Name;

        public override System.Type ReflectedType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override System.Reflection.ICustomAttributeProvider ReturnTypeCustomAttributes
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override System.Type ReturnType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

