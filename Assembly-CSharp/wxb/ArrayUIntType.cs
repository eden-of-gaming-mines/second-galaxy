﻿namespace wxb
{
    using System;

    internal class ArrayUIntType : ArraySerialize<uint>
    {
        protected override int GetElementSize() => 
            4;

        protected override uint Read(WRStream stream) => 
            stream.ReadUInt32();

        protected override void Write(WRStream stream, uint value)
        {
            stream.WriteInt32(value);
        }
    }
}

