﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;

    public class RefType
    {
        private object instance;
        private string fullType;
        private System.Type type;

        [MethodImpl(0x8000)]
        public RefType(object instance)
        {
        }

        [MethodImpl(0x8000)]
        public RefType(string fullType)
        {
        }

        [MethodImpl(0x8000)]
        public RefType(System.Type type)
        {
        }

        [MethodImpl(0x8000)]
        public RefType(string fullType, object param)
        {
        }

        [MethodImpl(0x8000)]
        public object GetField(string name)
        {
        }

        [MethodImpl(0x8000)]
        public object GetProperty(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name)
        {
        }

        public void InvokeMethod(string name, object[] param)
        {
            this.InvokeMethodReturn(name, param);
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object[] param)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void SetField(string name, object value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProperty(string name, object value)
        {
        }

        [MethodImpl(0x8000)]
        public object TryGetField(string name)
        {
        }

        [MethodImpl(0x8000)]
        public object TryGetProperty(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name)
        {
        }

        public void TryInvokeMethod(string name, object[] param)
        {
            this.TryInvokeMethodReturn(name, param);
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object[] param)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void TrySetField(string name, object value)
        {
        }

        [MethodImpl(0x8000)]
        public void TrySetProperty(string name, object value)
        {
        }

        public object Instance =>
            this.instance;

        public System.Type Type =>
            this.type;
    }
}

