﻿namespace wxb
{
    using System;

    [AttributeUsage(AttributeTargets.Method)]
    public class ReplaceFunction : Attribute
    {
        public ReplaceFunction()
        {
        }

        public ReplaceFunction(string fieldNameOrTypeName)
        {
        }

        public ReplaceFunction(Type type)
        {
        }

        public ReplaceFunction(string typeName, string fieldName)
        {
        }

        public ReplaceFunction(Type type, string fieldName)
        {
        }
    }
}

