﻿namespace wxb
{
    using System;

    public interface ITypeSerialize
    {
        int CalculateSize(object value);
        void MergeFrom(ref object value, MonoStream ms);
        void WriteTo(object value, MonoStream ms);
    }
}

