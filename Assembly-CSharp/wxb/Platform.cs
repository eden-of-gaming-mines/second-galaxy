﻿namespace wxb
{
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class Platform : Attribute
    {
        public Platform(RuntimePlatform platform)
        {
        }
    }
}

