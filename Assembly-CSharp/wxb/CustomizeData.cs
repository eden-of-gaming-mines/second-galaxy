﻿namespace wxb
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class CustomizeData
    {
        [HideInInspector, SerializeField]
        private string typeName;
        [SerializeField, HideInInspector]
        private List<UnityEngine.Object> objs;
        [HideInInspector, SerializeField]
        private byte[] bytes;
        private RefType refType_;

        [MethodImpl(0x8000)]
        public void OnAfterDeserialize(object self)
        {
        }

        public string TypeName =>
            this.typeName;

        public RefType refType =>
            this.refType_;
    }
}

