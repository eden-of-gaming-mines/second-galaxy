﻿namespace wxb
{
    using System;

    internal class ArrayDoubleType : ArraySerialize<double>
    {
        protected override int GetElementSize() => 
            8;

        protected override double Read(WRStream stream) => 
            stream.ReadDouble();

        protected override void Write(WRStream stream, double value)
        {
            stream.WriteDouble(value);
        }
    }
}

