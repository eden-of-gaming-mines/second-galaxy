﻿namespace wxb
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CacheType
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Type <type>k__BackingField;
        private Dictionary<string, MemberInfo> NameToMethodBase;
        private List<FieldInfo> serializes;
        private static readonly BindingFlags Flags = (BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
        private Ctor[] Ctors;
        private ConstructorInfo default_ctor;
        private bool is_default_ctor;

        [MethodImpl(0x8000)]
        public CacheType(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public ConstructorInfo GetCtor()
        {
        }

        [MethodImpl(0x8000)]
        public ConstructorInfo GetCtor(Type param)
        {
        }

        [MethodImpl(0x8000)]
        public FieldInfo GetField(string name)
        {
        }

        [MethodImpl(0x8000)]
        public MethodInfo GetMethod(string name)
        {
        }

        [MethodImpl(0x8000)]
        private static MethodInfo GetMethodInfo(Type type, string name)
        {
        }

        [MethodImpl(0x8000)]
        public PropertyInfo GetProperty(string name)
        {
        }

        [MethodImpl(0x8000)]
        public List<FieldInfo> GetSerializeField()
        {
        }

        public Type type { get; private set; }

        [StructLayout(LayoutKind.Sequential)]
        private struct Ctor
        {
            public Type param;
            public ConstructorInfo info;
        }
    }
}

