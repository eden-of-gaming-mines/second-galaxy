﻿namespace wxb
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class L
    {
        private static I active = new UnityLog();

        [MethodImpl(0x8000)]
        public static void Log(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static void LogError(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public static void LogErrorFormat(string format, params object[] objs)
        {
        }

        [MethodImpl(0x8000)]
        public static void LogException(Exception ex)
        {
        }

        [MethodImpl(0x8000), Conditional("DEBUGLOG")]
        public static void LogFormat(string format, params object[] objs)
        {
        }

        [MethodImpl(0x8000)]
        public static void LogWarningFormat(string format, params object[] objs)
        {
        }

        public static void Set(I l)
        {
            active = l;
        }

        public interface I
        {
            void Log(object obj);
            void LogError(object obj);
            void LogErrorFormat(string format, params object[] objs);
            void LogException(Exception ex);
            void LogFormat(string format, params object[] objs);
            void LogWarningFormat(string format, params object[] objs);
        }

        private class UnityLog : L.I
        {
            void L.I.Log(object obj)
            {
                UnityEngine.Debug.Log(obj);
            }

            void L.I.LogError(object obj)
            {
                UnityEngine.Debug.LogError(obj);
            }

            void L.I.LogErrorFormat(string format, params object[] objs)
            {
                UnityEngine.Debug.LogErrorFormat(format, objs);
            }

            void L.I.LogException(Exception ex)
            {
                UnityEngine.Debug.LogException(ex);
            }

            void L.I.LogFormat(string format, params object[] objs)
            {
                UnityEngine.Debug.LogFormat(format, objs);
            }

            void L.I.LogWarningFormat(string format, params object[] objs)
            {
                UnityEngine.Debug.LogWarningFormat(format, objs);
            }
        }
    }
}

