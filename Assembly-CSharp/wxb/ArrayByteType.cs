﻿namespace wxb
{
    using System;

    internal class ArrayByteType : ArraySerialize<byte>
    {
        protected override int GetElementSize() => 
            1;

        protected override byte Read(WRStream stream) => 
            stream.ReadByte();

        protected override void Write(WRStream stream, byte value)
        {
            stream.WriteByte(value);
        }
    }
}

