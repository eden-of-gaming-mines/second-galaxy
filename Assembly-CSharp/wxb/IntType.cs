﻿namespace wxb
{
    using System;

    internal class IntType : Serialize<int>
    {
        protected override int CalculateSize(int value) => 
            4;

        protected override int Read(WRStream stream) => 
            stream.ReadInt32();

        protected override void Write(WRStream stream, int value)
        {
            stream.WriteInt32(value);
        }
    }
}

