﻿namespace wxb
{
    using System;
    using System.IO;

    public static class ResLoad
    {
        private static IResLoad current;

        public static Stream GetStream(string path) => 
            current.GetStream(path);

        public static void Set(IResLoad load)
        {
            current = load;
        }
    }
}

