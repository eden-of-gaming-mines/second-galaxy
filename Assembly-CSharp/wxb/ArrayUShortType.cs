﻿namespace wxb
{
    using System;

    internal class ArrayUShortType : ArraySerialize<ushort>
    {
        protected override int GetElementSize() => 
            2;

        protected override ushort Read(WRStream stream) => 
            stream.ReadUInt16();

        protected override void Write(WRStream stream, ushort value)
        {
            stream.WriteInt16(value);
        }
    }
}

