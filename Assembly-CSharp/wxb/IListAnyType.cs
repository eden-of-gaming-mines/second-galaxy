﻿namespace wxb
{
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;

    internal abstract class IListAnyType : ITypeSerialize
    {
        protected Type arrayType;
        protected Type elementType;
        protected ITypeSerialize elementTypeSerialize;

        [MethodImpl(0x8000)]
        public IListAnyType(Type arrayType, Type elementType)
        {
        }

        protected abstract IList Create(int lenght);
        [MethodImpl(0x8000)]
        int ITypeSerialize.CalculateSize(object value)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.WriteTo(object value, MonoStream ms)
        {
        }
    }
}

