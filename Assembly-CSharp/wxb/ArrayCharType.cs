﻿namespace wxb
{
    using System;

    internal class ArrayCharType : ArraySerialize<char>
    {
        protected override int GetElementSize() => 
            2;

        protected override char Read(WRStream stream) => 
            ((char) ((ushort) stream.ReadInt16()));

        protected override void Write(WRStream stream, char value)
        {
            stream.WriteInt16(value);
        }
    }
}

