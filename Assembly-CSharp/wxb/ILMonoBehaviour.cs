﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ILMonoBehaviour : MonoBehaviour, ISerializationCallbackReceiver
    {
        [SerializeField]
        private CustomizeData customizeData;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationQuit()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            this.customizeData.OnAfterDeserialize(this);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        }

        public RefType refType =>
            this.customizeData.refType;
    }
}

