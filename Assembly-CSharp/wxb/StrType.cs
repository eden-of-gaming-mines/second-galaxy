﻿namespace wxb
{
    using System;

    internal class StrType : Serialize<string>
    {
        protected override int CalculateSize(string value) => 
            WRStream.ComputeStringSize(value);

        protected override string Read(WRStream stream) => 
            stream.ReadString();

        protected override void Write(WRStream stream, string value)
        {
            stream.WriteString(value);
        }
    }
}

