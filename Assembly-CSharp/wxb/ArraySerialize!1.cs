﻿namespace wxb
{
    using System;
    using System.Collections.Generic;

    internal abstract class ArraySerialize<T> : IListSerialize<T>
    {
        protected ArraySerialize()
        {
        }

        protected override IList<T> Create(int lenght) => 
            new T[lenght];
    }
}

