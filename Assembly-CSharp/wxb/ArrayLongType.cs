﻿namespace wxb
{
    using System;

    internal class ArrayLongType : ArraySerialize<long>
    {
        protected override int GetElementSize() => 
            8;

        protected override long Read(WRStream stream) => 
            stream.ReadInt64();

        protected override void Write(WRStream stream, long value)
        {
            stream.WriteInt64(value);
        }
    }
}

