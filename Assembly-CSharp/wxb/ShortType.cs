﻿namespace wxb
{
    using System;

    internal class ShortType : Serialize<short>
    {
        protected override int CalculateSize(short value) => 
            2;

        protected override short Read(WRStream stream) => 
            stream.ReadInt16();

        protected override void Write(WRStream stream, short value)
        {
            stream.WriteInt16(value);
        }
    }
}

