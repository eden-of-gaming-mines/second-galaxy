﻿namespace wxb
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal abstract class IListSerialize<T> : ITypeSerialize
    {
        protected IListSerialize()
        {
        }

        protected abstract IList<T> Create(int lenght);
        protected abstract int GetElementSize();
        protected abstract T Read(WRStream stream);
        protected abstract void Write(WRStream stream, T value);
        [MethodImpl(0x8000)]
        int ITypeSerialize.CalculateSize(object value)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.WriteTo(object value, MonoStream ms)
        {
        }
    }
}

