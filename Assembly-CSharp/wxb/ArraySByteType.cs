﻿namespace wxb
{
    using System;

    internal class ArraySByteType : ArraySerialize<sbyte>
    {
        protected override int GetElementSize() => 
            1;

        protected override sbyte Read(WRStream stream) => 
            stream.ReadSByte();

        protected override void Write(WRStream stream, sbyte value)
        {
            stream.WriteSByte(value);
        }
    }
}

