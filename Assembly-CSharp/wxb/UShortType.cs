﻿namespace wxb
{
    using System;

    internal class UShortType : Serialize<ushort>
    {
        protected override int CalculateSize(ushort value) => 
            2;

        protected override ushort Read(WRStream stream) => 
            stream.ReadUInt16();

        protected override void Write(WRStream stream, ushort value)
        {
            stream.WriteInt16(value);
        }
    }
}

