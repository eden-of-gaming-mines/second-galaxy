﻿namespace wxb
{
    using System;

    internal class CharType : Serialize<char>
    {
        protected override int CalculateSize(char value) => 
            2;

        protected override char Read(WRStream stream) => 
            ((char) ((ushort) stream.ReadInt16()));

        protected override void Write(WRStream stream, char value)
        {
            stream.WriteInt16(value);
        }
    }
}

