﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;

    internal abstract class Serialize<T> : ITypeSerialize
    {
        protected Serialize()
        {
        }

        protected abstract int CalculateSize(T value);
        protected abstract T Read(WRStream stream);
        protected abstract void Write(WRStream stream, T value);
        int ITypeSerialize.CalculateSize(object value) => 
            this.CalculateSize((T) value);

        [MethodImpl(0x8000)]
        void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.WriteTo(object obj, MonoStream ms)
        {
        }
    }
}

