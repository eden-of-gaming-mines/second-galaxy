﻿namespace wxb
{
    using System;

    internal class FloatType : Serialize<float>
    {
        protected override int CalculateSize(float value) => 
            4;

        protected override float Read(WRStream stream) => 
            stream.ReadFloat();

        protected override void Write(WRStream stream, float value)
        {
            stream.WriteFloat(value);
        }
    }
}

