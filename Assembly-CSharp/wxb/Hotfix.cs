﻿namespace wxb
{
    using IL;
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class Hotfix
    {
        public FieldInfo field;
        public MethodInfo method;
        public DelegateBridge bridge;

        [MethodImpl(0x8000)]
        public Hotfix(FieldInfo field, MethodInfo method, DelegateBridge bridge)
        {
        }

        [MethodImpl(0x8000)]
        public Hotfix(Type type, string name, Type hotfixType)
        {
        }

        [MethodImpl(0x8000)]
        public Hotfix(Type type, string fieldName, string funName, Type hotfixType, string hotfixFunName)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object[] parameters)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void Run(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public object Run(Func<object> fun)
        {
        }
    }
}

