﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Text;

    public class WRStream
    {
        protected int mSize;
        protected byte[] mBuffer;
        protected int mReadPos;
        protected int mWritePos;
        internal static readonly Encoding Utf8Encoding = Encoding.UTF8;

        [MethodImpl(0x8000)]
        public WRStream(int size)
        {
        }

        [MethodImpl(0x8000)]
        public WRStream(byte[] bytes)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckReadSize(int size)
        {
        }

        public static int ComputeLengthSize(int length) => 
            ComputeRawVarint32Size((uint) length);

        [MethodImpl(0x8000)]
        public static int ComputeRawVarint32Size(uint value)
        {
        }

        [MethodImpl(0x8000)]
        public static int ComputeStringSize(string value)
        {
        }

        [MethodImpl(0x8000)]
        private static void Copy(byte[] src, int srcOffset, byte[] dst, int dstOffset, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void ensureCapacity(int length)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] GetBytes()
        {
        }

        [MethodImpl(0x8000)]
        public byte ReadByte()
        {
        }

        [MethodImpl(0x8000)]
        public char ReadChar()
        {
        }

        public double ReadDouble() => 
            BitConverter.Int64BitsToDouble(this.ReadInt64());

        [MethodImpl(0x8000)]
        public float ReadFloat()
        {
        }

        [MethodImpl(0x8000)]
        public short ReadInt16()
        {
        }

        [MethodImpl(0x8000)]
        public int ReadInt32()
        {
        }

        [MethodImpl(0x8000)]
        public long ReadInt64()
        {
        }

        public int ReadLength() => 
            ((int) this.ReadRawVarint32());

        [MethodImpl(0x8000)]
        internal byte ReadRawByte()
        {
        }

        [MethodImpl(0x8000)]
        internal byte[] ReadRawBytes(int size)
        {
        }

        [MethodImpl(0x8000)]
        internal uint ReadRawVarint32()
        {
        }

        [MethodImpl(0x8000)]
        public sbyte ReadSByte()
        {
        }

        [MethodImpl(0x8000)]
        public string ReadString()
        {
        }

        [MethodImpl(0x8000)]
        public ushort ReadUInt16()
        {
        }

        [MethodImpl(0x8000)]
        public uint ReadUInt32()
        {
        }

        [MethodImpl(0x8000)]
        public ulong ReadUInt64()
        {
        }

        [MethodImpl(0x8000)]
        private static void Reverse(byte[] bytes)
        {
        }

        [MethodImpl(0x8000)]
        private uint SlowReadRawVarint32()
        {
        }

        [MethodImpl(0x8000)]
        public void WriteByte(byte value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteChar(char value)
        {
        }

        public void WriteDouble(double value)
        {
            this.WriteInt64(BitConverter.DoubleToInt64Bits(value));
        }

        [MethodImpl(0x8000)]
        public void WriteFloat(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt16(short value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt16(ushort value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt32(int value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt32(uint value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt64(long value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteInt64(ulong value)
        {
        }

        public void WriteLength(int length)
        {
            this.WriteRawVarint32((uint) length);
        }

        [MethodImpl(0x8000)]
        internal void WriteRawBytes(byte[] value, int offset, int length)
        {
        }

        [MethodImpl(0x8000)]
        internal void WriteRawVarint32(uint value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteSByte(sbyte value)
        {
        }

        [MethodImpl(0x8000)]
        public void WriteString(string value)
        {
        }

        public int WriteRemain =>
            (this.mSize - this.mWritePos);

        public int ReadPos
        {
            get => 
                this.mReadPos;
            set => 
                (this.mReadPos = value);
        }

        public int WritePos
        {
            get => 
                this.mWritePos;
            set => 
                (this.mWritePos = value);
        }

        public int ReadSize =>
            (this.WritePos - this.ReadPos);

        public int size =>
            this.mSize;
    }
}

