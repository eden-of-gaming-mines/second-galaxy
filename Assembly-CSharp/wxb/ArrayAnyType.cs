﻿namespace wxb
{
    using System;
    using System.Collections;

    internal class ArrayAnyType : IListAnyType
    {
        public ArrayAnyType(Type arrayType) : base(arrayType, arrayType.GetElementType())
        {
        }

        protected override IList Create(int lenght) => 
            Array.CreateInstance(base.elementType, lenght);
    }
}

