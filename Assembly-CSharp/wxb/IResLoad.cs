﻿namespace wxb
{
    using System;
    using System.IO;

    public interface IResLoad
    {
        Stream GetStream(string path);
    }
}

