﻿namespace wxb
{
    using System;

    internal class ByteType : Serialize<byte>
    {
        protected override int CalculateSize(byte value) => 
            1;

        protected override byte Read(WRStream stream) => 
            stream.ReadByte();

        protected override void Write(WRStream stream, byte value)
        {
            stream.WriteByte(value);
        }
    }
}

