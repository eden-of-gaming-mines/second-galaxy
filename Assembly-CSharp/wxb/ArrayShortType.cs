﻿namespace wxb
{
    using System;

    internal class ArrayShortType : ArraySerialize<short>
    {
        protected override int GetElementSize() => 
            2;

        protected override short Read(WRStream stream) => 
            stream.ReadInt16();

        protected override void Write(WRStream stream, short value)
        {
            stream.WriteInt16(value);
        }
    }
}

