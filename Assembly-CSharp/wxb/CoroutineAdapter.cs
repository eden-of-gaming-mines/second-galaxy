﻿namespace wxb
{
    using ILRuntime.CLR.Method;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CoroutineAdapter : CrossBindingAdaptor
    {
        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance) => 
            new Adaptor(appdomain, instance);

        public override Type BaseCLRType =>
            null;

        public override Type[] BaseCLRTypes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public override Type AdaptorType =>
            typeof(Adaptor);

        internal class Adaptor : IEnumerator<object>, IEnumerator, IDisposable, CrossBindingAdaptorType
        {
            private ILTypeInstance instance;
            private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            private IMethod mCurrentMethod;
            private bool mCurrentMethodGot;
            private IMethod mDisposeMethod;
            private bool mDisposeMethodGot;
            private IMethod mMoveNextMethod;
            private bool mMoveNextMethodGot;
            private IMethod mResetMethod;
            private bool mResetMethodGot;

            public Adaptor()
            {
            }

            [MethodImpl(0x8000)]
            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
            }

            [MethodImpl(0x8000)]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [MethodImpl(0x8000)]
            public void Reset()
            {
            }

            [MethodImpl(0x8000)]
            public override string ToString()
            {
            }

            public ILTypeInstance ILInstance =>
                this.instance;

            public object Current
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }
        }
    }
}

