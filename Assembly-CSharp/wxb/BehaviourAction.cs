﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BehaviourAction : MonoBehaviour
    {
        public Action<BehaviourAction, int> OnAnimatorIKAction;
        public Action<BehaviourAction> OnAnimatorMoveAction;
        public Action<BehaviourAction, bool> OnApplicationFocusAction;
        public Action<BehaviourAction, bool> OnApplicationPauseAction;
        public Action<BehaviourAction> OnApplicationQuitAction;
        public Action<BehaviourAction, float[], int> OnAudioFilterReadAction;
        public Action<BehaviourAction> OnBecameInvisibleAction;
        public Action<BehaviourAction> OnBecameVisibleAction;
        public Action<BehaviourAction> OnBeforeTransformParentChangedAction;
        public Action<BehaviourAction> OnCanvasGroupChangedAction;
        public Action<BehaviourAction, Collision> OnCollisionEnterAction;
        public Action<BehaviourAction, Collision2D> OnCollisionEnter2DAction;
        public Action<BehaviourAction, Collision> OnCollisionExitAction;
        public Action<BehaviourAction, Collision2D> OnCollisionExit2DAction;
        public Action<BehaviourAction, Collision> OnCollisionStayAction;
        public Action<BehaviourAction, Collision2D> OnCollisionStay2DAction;
        public Action<BehaviourAction> OnConnectedToServerAction;
        public Action<BehaviourAction, ControllerColliderHit> OnControllerColliderHitAction;
        public Action<BehaviourAction> OnDestroyAction;
        public Action<BehaviourAction> OnDisableAction;
        public Action<BehaviourAction> OnEnableAction;
        public Action<BehaviourAction, float> OnJointBreakAction;
        public Action<BehaviourAction, Joint2D> OnJointBreak2DAction;
        public Action<BehaviourAction> OnMouseDownAction;
        public Action<BehaviourAction> OnMouseDragAction;
        public Action<BehaviourAction> OnMouseEnterAction;
        public Action<BehaviourAction> OnMouseExitAction;
        public Action<BehaviourAction> OnMouseOverAction;
        public Action<BehaviourAction> OnMouseUpAction;
        public Action<BehaviourAction> OnMouseUpAsButtonAction;
        public Action<BehaviourAction, GameObject> OnParticleCollisionAction;
        public Action<BehaviourAction> OnParticleTriggerAction;
        public Action<BehaviourAction> OnPostRenderAction;
        public Action<BehaviourAction> OnPreCullAction;
        public Action<BehaviourAction> OnPreRenderAction;
        public Action<BehaviourAction> OnRectTransformDimensionsChangeAction;
        public Action<BehaviourAction> OnRectTransformRemovedAction;
        public Action<BehaviourAction, RenderTexture, RenderTexture> OnRenderImageAction;
        public Action<BehaviourAction> OnRenderObjectAction;
        public Action<BehaviourAction> OnTransformChildrenChangedAction;
        public Action<BehaviourAction> OnTransformParentChangedAction;
        public Action<BehaviourAction, Collider> OnTriggerEnterAction;
        public Action<BehaviourAction, Collider2D> OnTriggerEnter2DAction;
        public Action<BehaviourAction, Collider> OnTriggerExitAction;
        public Action<BehaviourAction, Collider2D> OnTriggerExit2DAction;
        public Action<BehaviourAction, Collider> OnTriggerStayAction;
        public Action<BehaviourAction, Collider2D> OnTriggerStay2DAction;
        public Action<BehaviourAction> OnValidateAction;
        public Action<BehaviourAction> OnWillRenderObjectAction;
        public Action<BehaviourAction> StartAction;
        public Action<BehaviourAction> AwakeAction;
        public Action<BehaviourAction> FixedUpdateAction;
        public Action<BehaviourAction> LateUpdateAction;
        public Action<BehaviourAction> UpdateAction;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void FixedUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAnimatorIK(int layerIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAnimatorMove()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationFocus(bool focus)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationPause(bool pause)
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationQuit()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAudioFilterRead(float[] data, int channels)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBecameInvisible()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBecameVisible()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBeforeTransformParentChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCanvasGroupChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionEnter(Collision collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionEnter2D(Collision2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionExit(Collision collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionExit2D(Collision2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionStay(Collision collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCollisionStay2D(Collision2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConnectedToServer()
        {
        }

        [MethodImpl(0x8000)]
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJointBreak(float breakForce)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJointBreak2D(Joint2D joint)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseDown()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseDrag()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseEnter()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseExit()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseOver()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseUp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMouseUpAsButton()
        {
        }

        [MethodImpl(0x8000)]
        private void OnParticleCollision(GameObject other)
        {
        }

        [MethodImpl(0x8000)]
        private void OnParticleTrigger()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPostRender()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreCull()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreRender()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRectTransformDimensionsChange()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRectTransformRemoved()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderObject()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransformChildrenChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransformParentChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerEnter(Collider other)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerEnter2D(Collider2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerExit(Collider other)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerExit2D(Collider2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerStay(Collider other)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTriggerStay2D(Collider2D collision)
        {
        }

        [MethodImpl(0x8000)]
        private void OnValidate()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWillRenderObject()
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

