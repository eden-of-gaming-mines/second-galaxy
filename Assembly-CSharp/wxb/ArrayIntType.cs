﻿namespace wxb
{
    using System;

    internal class ArrayIntType : ArraySerialize<int>
    {
        protected override int GetElementSize() => 
            4;

        protected override int Read(WRStream stream) => 
            stream.ReadInt32();

        protected override void Write(WRStream stream, int value)
        {
            stream.WriteInt32(value);
        }
    }
}

