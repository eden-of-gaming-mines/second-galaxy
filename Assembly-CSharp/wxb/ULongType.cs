﻿namespace wxb
{
    using System;

    internal class ULongType : Serialize<ulong>
    {
        protected override int CalculateSize(ulong value) => 
            8;

        protected override ulong Read(WRStream stream) => 
            stream.ReadUInt64();

        protected override void Write(WRStream stream, ulong value)
        {
            stream.WriteInt64(value);
        }
    }
}

