﻿namespace wxb
{
    using System;

    internal class UIntType : Serialize<uint>
    {
        protected override int CalculateSize(uint value) => 
            4;

        protected override uint Read(WRStream stream) => 
            stream.ReadUInt32();

        protected override void Write(WRStream stream, uint value)
        {
            stream.WriteInt32(value);
        }
    }
}

