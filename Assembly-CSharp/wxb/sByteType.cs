﻿namespace wxb
{
    using System;

    internal class sByteType : Serialize<sbyte>
    {
        protected override int CalculateSize(sbyte value) => 
            1;

        protected override sbyte Read(WRStream stream) => 
            stream.ReadSByte();

        protected override void Write(WRStream stream, sbyte value)
        {
            stream.WriteSByte(value);
        }
    }
}

