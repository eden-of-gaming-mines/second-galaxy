﻿namespace wxb
{
    using IL;
    using ILRuntime.CLR.TypeSystem;
    using ILRuntime.Mono.Cecil;
    using ILRuntime.Mono.Collections.Generic;
    using ILRuntime.Runtime.Enviorment;
    using ILRuntime.Runtime.Intepreter;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using UnityEngine;

    public static class hotMgr
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static ILRuntime.Runtime.Enviorment.AppDomain <appdomain>k__BackingField;
        private static wxb.RefType refType;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static List<IType> <AllTypes>k__BackingField;
        public const BindingFlags bindingFlags = (BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
        private static Stream DllStream;
        private static HashSet<FieldInfo> Fields = new HashSet<FieldInfo>();
        [CompilerGenerated]
        private static Func<Delegate, Delegate> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<MethodInfo> <>f__am$cache1;

        [MethodImpl(0x8000)]
        public static StringBuilder AppendFormatLine(this StringBuilder sb, string format, params object[] objs)
        {
        }

        [MethodImpl(0x8000)]
        private static void AutoReplace(List<IType> types)
        {
        }

        [MethodImpl(0x8000)]
        private static void AutoSetFieldMethodValue(System.Type srcType, FieldInfo srcFieldInfo, ILType type, string fieldName, DelegateBridge bridge, Dictionary<string, List<MethodInfo>> NameToSorted)
        {
        }

        [MethodImpl(0x8000)]
        public static MemoryStream CopyStream(Stream input)
        {
        }

        [MethodImpl(0x8000)]
        public static void FindAttribute(System.Type attType, Action<System.Type> on)
        {
        }

        [MethodImpl(0x8000)]
        private static List<MethodInfo> FindMethodInfo(System.Type type, string name)
        {
        }

        [MethodImpl(0x8000)]
        private static FieldDefinition FindStaticField(ILType type, string name)
        {
        }

        private static RuntimePlatform GetCurrentPlatform() => 
            Application.platform;

        [MethodImpl(0x8000)]
        private static string GetKey(MethodInfo method)
        {
        }

        [MethodImpl(0x8000)]
        private static void GetPlatform(Collection<CustomAttribute> CustomAttributes, Action<RuntimePlatform> action)
        {
        }

        [MethodImpl(0x8000)]
        private static bool GetReplaceFunction(Collection<CustomAttribute> CustomAttributes, out System.Type type, out string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        private static System.Type GetReplaceType(Collection<CustomAttribute> CustomAttributes)
        {
        }

        [MethodImpl(0x8000)]
        private static System.Type GetTypeByName(string fullName)
        {
        }

        [MethodImpl(0x8000)]
        public static void Init(string assemblyName, IResLoad resLoad = null)
        {
        }

        [MethodImpl(0x8000)]
        private static void InitByProperty(System.Type attType, string name, List<IType> types)
        {
        }

        [MethodImpl(0x8000)]
        public static void InitHotModule(string assemblyName)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsStatic(MethodInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static void RegDelegate(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [MethodImpl(0x8000)]
        public static void ReleaseAll()
        {
        }

        [MethodImpl(0x8000)]
        public static bool ReplaceField(System.Type type, string fieldName, MethodInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static bool ReplaceFunc(System.Type type, string name, MethodInfo info)
        {
        }

        public static ILRuntime.Runtime.Enviorment.AppDomain appdomain
        {
            [CompilerGenerated]
            get => 
                <appdomain>k__BackingField;
            [CompilerGenerated]
            private set => 
                (<appdomain>k__BackingField = value);
        }

        public static wxb.RefType RefType =>
            refType;

        public static List<IType> AllTypes
        {
            [CompilerGenerated]
            get => 
                <AllTypes>k__BackingField;
            [CompilerGenerated]
            private set => 
                (<AllTypes>k__BackingField = value);
        }

        [CompilerGenerated]
        private sealed class <AutoReplace>c__AnonStorey1
        {
            internal List<RuntimePlatform> platforms;

            internal void <>m__0(RuntimePlatform p)
            {
                this.platforms.Add(p);
            }

            internal void <>m__1(RuntimePlatform p)
            {
                this.platforms.Add(p);
            }
        }

        [CompilerGenerated]
        private sealed class <RegDelegate>c__AnonStorey0
        {
            internal Delegate act;

            internal int <>m__0(ILTypeInstance x, ILTypeInstance y) => 
                ((Func<ILTypeInstance, ILTypeInstance, int>) this.act)(x, y);
        }
    }
}

