﻿namespace wxb
{
    using System;

    internal class LongType : Serialize<long>
    {
        protected override int CalculateSize(long value) => 
            8;

        protected override long Read(WRStream stream) => 
            stream.ReadInt64();

        protected override void Write(WRStream stream, long value)
        {
            stream.WriteInt64(value);
        }
    }
}

