﻿namespace wxb
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal class AnyType : ITypeSerialize
    {
        private Type type;
        private List<FieldInfo> fieldInfos;

        [MethodImpl(0x8000)]
        public AnyType(Type type, List<FieldInfo> fieldInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void MergeFrom(ref object value, MonoStream ms)
        {
        }

        [MethodImpl(0x8000)]
        int ITypeSerialize.CalculateSize(object value)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.WriteTo(object value, MonoStream ms)
        {
        }

        [CompilerGenerated]
        private sealed class <MergeFrom>c__AnonStorey0
        {
            internal string fieldName;

            [MethodImpl(0x8000)]
            internal bool <>m__0(FieldInfo field)
            {
            }
        }
    }
}

