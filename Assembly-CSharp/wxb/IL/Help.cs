﻿namespace wxb.IL
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using wxb;

    public static class Help
    {
        private static Dictionary<Type, CacheType> Caches;
        public static readonly object[] EmptyObj;
        public static readonly object[] OneObj;
        public static readonly Type[] OneType;
        public static readonly Type[] EmptyType;
        private static HashSet<Type> BaseTypes;
        private static Dictionary<string, Type> AllTypesByFullName;

        [MethodImpl(0x8000)]
        public static object Create(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static object CreateInstaince(Type instanceType, object parent)
        {
        }

        [MethodImpl(0x8000)]
        public static void ForEach(Action<Type> fun)
        {
        }

        private static ConstructorInfo GetConstructor(Type instanceType, Type param) => 
            GetOrCreate(instanceType).GetCtor(param);

        [MethodImpl(0x8000)]
        public static Type GetElementByList(FieldInfo fieldInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static Type GetElementByList(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static FieldInfo GetField(Type type, string name)
        {
        }

        [MethodImpl(0x8000)]
        public static MethodInfo GetMethod(Type type, string name)
        {
        }

        [MethodImpl(0x8000)]
        public static CacheType GetOrCreate(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertyInfo GetProperty(Type type, string name)
        {
        }

        [MethodImpl(0x8000)]
        public static List<FieldInfo> GetSerializeField(object obj)
        {
        }

        public static List<FieldInfo> GetSerializeField(Type type) => 
            GetOrCreate(type).GetSerializeField();

        [MethodImpl(0x8000)]
        public static void GetSerializeField(Type type, List<FieldInfo> fieldinfos)
        {
        }

        public static Type GetType(string name) => 
            GetTypeByFullName(name);

        [MethodImpl(0x8000)]
        public static Type GetTypeByFullName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static void Init()
        {
        }

        public static bool IsBaseType(Type type) => 
            BaseTypes.Contains(type);

        [MethodImpl(0x8000)]
        public static bool isListType(Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static bool isType(Type src, Type baseType)
        {
        }

        [MethodImpl(0x8000)]
        private static void Reg<T>()
        {
        }

        [MethodImpl(0x8000)]
        public static void ReleaseAll()
        {
        }

        [MethodImpl(0x8000)]
        public static bool TryGetTypeByFullName(string name, out Type type)
        {
        }

        [MethodImpl(0x8000)]
        private static void X()
        {
        }
    }
}

