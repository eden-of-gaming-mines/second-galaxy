﻿namespace wxb
{
    using System;

    internal class ArrayFloatType : ArraySerialize<float>
    {
        protected override int GetElementSize() => 
            4;

        protected override float Read(WRStream stream) => 
            stream.ReadFloat();

        protected override void Write(WRStream stream, float value)
        {
            stream.WriteFloat(value);
        }
    }
}

