﻿namespace wxb
{
    using System;

    internal class ArrayULongType : ArraySerialize<ulong>
    {
        protected override int GetElementSize() => 
            8;

        protected override ulong Read(WRStream stream) => 
            stream.ReadUInt64();

        protected override void Write(WRStream stream, ulong value)
        {
            stream.WriteInt64(value);
        }
    }
}

