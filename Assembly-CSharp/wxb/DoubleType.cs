﻿namespace wxb
{
    using System;

    internal class DoubleType : Serialize<double>
    {
        protected override int CalculateSize(double value) => 
            8;

        protected override double Read(WRStream stream) => 
            stream.ReadDouble();

        protected override void Write(WRStream stream, double value)
        {
            stream.WriteDouble(value);
        }
    }
}

