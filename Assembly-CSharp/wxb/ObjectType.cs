﻿namespace wxb
{
    using System;
    using System.Runtime.CompilerServices;

    internal class ObjectType : ITypeSerialize
    {
        int ITypeSerialize.CalculateSize(object value) => 
            ((value != null) ? 2 : 0);

        [MethodImpl(0x8000)]
        void ITypeSerialize.MergeFrom(ref object parent, MonoStream ms)
        {
        }

        [MethodImpl(0x8000)]
        void ITypeSerialize.WriteTo(object value, MonoStream ms)
        {
        }
    }
}

