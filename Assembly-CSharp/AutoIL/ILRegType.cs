﻿namespace AutoIL
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.PathFinding;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.GalaxyEditor;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.ProjectX.Runtime;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using BlackJack.ProjectX.Runtime.SolarSystem.Weapon;
    using BlackJack.ProjectX.Runtime.Timeline;
    using BlackJack.ProjectX.Runtime.UI;
    using BlackJack.ProjectX.ScenePreview;
    using BlackJack.PropertiesCalculater;
    using Dest.Math;
    using ILRuntime.Runtime.Enviorment;
    using ProtoBuf;
    using ProtoBuf.Meta;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using TouchScript.Gestures;
    using TouchScript.Layers;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.Rendering;
    using UnityEngine.UI;

    internal static class ILRegType
    {
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache51;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache52;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache53;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache54;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache55;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache56;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache57;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache58;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache59;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache5F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache60;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache61;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache62;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache63;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache64;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache65;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache66;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache67;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache68;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache69;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache6F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache70;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache71;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache72;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache73;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache74;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache75;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache76;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache77;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache78;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache79;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache7F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache80;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache81;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache82;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache83;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache84;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache85;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache86;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache87;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache88;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache89;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache8F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache90;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache91;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache92;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache93;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache94;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache95;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache96;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache97;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache98;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache99;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache9F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheA9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheAF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheB9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheBF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheC9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheCF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheD9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheDF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheE9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheEA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheEB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheEC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheED;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheEE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheEF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheF9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cacheFF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache100;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache101;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache102;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache103;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache104;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache105;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache106;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache107;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache108;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache109;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache10F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache110;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache111;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache112;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache113;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache114;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache115;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache116;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache117;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache118;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache119;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache11F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache120;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache121;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache122;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache123;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache124;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache125;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache126;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache127;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache128;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache129;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache12F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache130;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache131;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache132;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache133;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache134;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache135;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache136;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache137;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache138;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache139;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache13F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache140;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache141;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache142;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache143;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache144;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache145;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache146;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache147;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache148;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache149;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache14F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache150;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache151;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache152;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache153;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache154;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache155;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache156;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache157;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache158;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache159;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache15F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache160;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache161;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache162;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache163;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache164;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache165;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache166;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache167;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache168;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache169;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache16F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache170;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache171;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache172;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache173;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache174;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache175;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache176;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache177;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache178;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache179;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache17F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache180;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache181;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache182;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache183;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache184;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache185;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache186;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache187;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache188;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache189;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache18F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache190;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache191;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache192;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache193;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache194;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache195;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache196;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache197;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache198;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache199;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache19F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1A9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1AF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1B9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1BF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1C9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1CF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1D9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1DF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1E9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1EA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1EB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1EC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1ED;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1EE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1EF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1F9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache1FF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache200;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache201;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache202;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache203;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache204;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache205;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache206;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache207;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache208;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache209;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache20F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache210;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache211;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache212;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache213;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache214;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache215;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache216;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache217;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache218;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache219;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache21F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache220;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache221;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache222;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache223;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache224;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache225;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache226;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache227;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache228;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache229;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache22F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache230;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache231;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache232;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache233;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache234;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache235;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache236;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache237;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache238;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache239;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache23F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache240;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache241;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache242;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache243;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache244;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache245;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache246;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache247;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache248;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache249;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache24F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache250;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache251;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache252;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache253;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache254;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache255;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache256;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache257;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache258;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache259;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache25F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache260;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache261;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache262;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache263;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache264;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache265;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache266;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache267;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache268;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache269;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache26F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache270;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache271;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache272;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache273;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache274;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache275;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache276;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache277;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache278;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache279;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache27F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache280;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache281;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache282;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache283;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache284;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache285;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache286;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache287;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache288;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache289;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache28F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache290;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache291;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache292;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache293;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache294;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache295;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache296;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache297;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache298;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache299;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache29F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2A9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2AF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2B9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2BF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2C9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2CF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2D9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2DF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2E9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2EA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2EB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2EC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2ED;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2EE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2EF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2F9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache2FF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache300;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache301;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache302;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache303;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache304;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache305;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache306;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache307;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache308;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache309;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache30F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache310;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache311;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache312;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache313;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache314;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache315;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache316;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache317;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache318;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache319;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache31F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache320;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache321;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache322;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache323;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache324;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache325;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache326;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache327;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache328;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache329;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache32F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache330;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache331;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache332;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache333;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache334;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache335;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache336;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache337;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache338;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache339;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache33F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache340;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache341;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache342;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache343;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache344;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache345;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache346;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache347;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache348;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache349;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache34F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache350;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache351;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache352;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache353;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache354;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache355;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache356;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache357;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache358;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache359;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache35F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache360;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache361;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache362;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache363;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache364;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache365;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache366;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache367;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache368;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache369;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache36F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache370;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache371;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache372;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache373;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache374;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache375;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache376;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache377;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache378;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache379;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache37F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache380;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache381;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache382;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache383;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache384;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache385;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache386;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache387;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache388;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache389;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache38F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache390;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache391;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache392;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache393;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache394;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache395;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache396;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache397;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache398;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache399;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache39F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3A9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3AF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3B9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3BF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3C9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3CF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3D9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3DF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3E9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3EA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3EB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3EC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3ED;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3EE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3EF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3F9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache3FF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache400;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache401;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache402;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache403;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache404;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache405;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache406;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache407;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache408;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache409;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache40F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache410;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache411;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache412;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache413;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache414;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache415;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache416;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache417;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache418;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache419;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache41F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache420;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache421;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache422;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache423;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache424;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache425;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache426;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache427;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache428;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache429;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache42F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache430;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache431;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache432;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache433;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache434;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache435;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache436;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache437;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache438;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache439;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache43F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache440;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache441;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache442;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache443;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache444;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache445;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache446;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache447;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache448;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache449;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache44F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache450;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache451;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache452;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache453;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache454;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache455;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache456;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache457;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache458;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache459;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache45F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache460;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache461;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache462;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache463;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache464;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache465;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache466;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache467;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache468;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache469;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache46F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache470;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache471;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache472;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache473;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache474;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache475;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache476;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache477;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache478;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache479;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache47F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache480;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache481;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache482;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache483;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache484;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache485;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache486;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache487;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache488;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache489;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache48F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache490;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache491;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache492;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache493;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache494;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache495;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache496;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache497;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache498;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache499;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache49F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4A9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4AF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4B9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4BF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4C9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4CF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4D9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4DF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4E9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4EA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4EB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4EC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4ED;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4EE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4EF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F0;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F1;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F2;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F3;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F4;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F5;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F6;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F7;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F8;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4F9;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FA;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FB;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FC;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FD;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FE;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache4FF;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache500;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache501;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache502;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache503;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache504;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache505;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache506;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache507;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache508;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache509;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50A;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50B;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50C;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50D;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50E;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache50F;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache510;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache511;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache512;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache513;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache514;
        [CompilerGenerated]
        private static Func<System.Delegate, System.Delegate> <>f__am$cache515;

        public static void Fame()
        {
        }

        [MethodImpl(0x8000)]
        public static void RegisterDelegateConvertor(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [MethodImpl(0x8000)]
        public static void RegisterFunctionDelegate(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [MethodImpl(0x8000)]
        public static void RegisterMethodDelegate(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey0
        {
            internal System.Delegate act;

            [MethodImpl(0x8000)]
            internal bool <>m__0(Component obj)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1
        {
            internal System.Delegate act;

            internal int <>m__0(Component x, Component y) => 
                ((Func<Component, Component, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10
        {
            internal System.Delegate act;

            internal bool <>m__0(CriAtomExPlayback obj) => 
                ((Func<CriAtomExPlayback, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey100
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipSizeType obj) => 
                ((Func<ShipSizeType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey101
        {
            internal System.Delegate act;

            internal int <>m__0(ShipSizeType x, ShipSizeType y) => 
                ((Func<ShipSizeType, ShipSizeType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey102
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfHighSlotInfo obj) => 
                ((Func<ConfHighSlotInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey103
        {
            internal System.Delegate act;

            internal int <>m__0(ConfHighSlotInfo x, ConfHighSlotInfo y) => 
                ((Func<ConfHighSlotInfo, ConfHighSlotInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey104
        {
            internal System.Delegate act;

            internal bool <>m__0(SoundInfo obj) => 
                ((Func<SoundInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey105
        {
            internal System.Delegate act;

            internal int <>m__0(SoundInfo x, SoundInfo y) => 
                ((Func<SoundInfo, SoundInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey106
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipTypeInfoShipTypeInvadeArroundDistanceList obj) => 
                ((Func<ShipTypeInfoShipTypeInvadeArroundDistanceList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey107
        {
            internal System.Delegate act;

            internal int <>m__0(ShipTypeInfoShipTypeInvadeArroundDistanceList x, ShipTypeInfoShipTypeInvadeArroundDistanceList y) => 
                ((Func<ShipTypeInfoShipTypeInvadeArroundDistanceList, ShipTypeInfoShipTypeInvadeArroundDistanceList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey108
        {
            internal System.Delegate act;

            internal bool <>m__0(MonsterSelectInfo obj) => 
                ((Func<MonsterSelectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey109
        {
            internal System.Delegate act;

            internal int <>m__0(MonsterSelectInfo x, MonsterSelectInfo y) => 
                ((Func<MonsterSelectInfo, MonsterSelectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10A
        {
            internal System.Delegate act;

            internal bool <>m__0(SignalInfoMonsterDropInfo obj) => 
                ((Func<SignalInfoMonsterDropInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10B
        {
            internal System.Delegate act;

            internal int <>m__0(SignalInfoMonsterDropInfo x, SignalInfoMonsterDropInfo y) => 
                ((Func<SignalInfoMonsterDropInfo, SignalInfoMonsterDropInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10C
        {
            internal System.Delegate act;

            internal bool <>m__0(FormatStringParamInfo obj) => 
                ((Func<FormatStringParamInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10D
        {
            internal System.Delegate act;

            internal int <>m__0(FormatStringParamInfo x, FormatStringParamInfo y) => 
                ((Func<FormatStringParamInfo, FormatStringParamInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10E
        {
            internal System.Delegate act;

            internal bool <>m__0(PropertyCategory obj) => 
                ((Func<PropertyCategory, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey10F
        {
            internal System.Delegate act;

            internal int <>m__0(PropertyCategory x, PropertyCategory y) => 
                ((Func<PropertyCategory, PropertyCategory, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11
        {
            internal System.Delegate act;

            internal int <>m__0(CriAtomExPlayback x, CriAtomExPlayback y) => 
                ((Func<CriAtomExPlayback, CriAtomExPlayback, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey110
        {
            internal System.Delegate act;

            internal bool <>m__0(UserGuideConditionInfo obj) => 
                ((Func<UserGuideConditionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey111
        {
            internal System.Delegate act;

            internal int <>m__0(UserGuideConditionInfo x, UserGuideConditionInfo y) => 
                ((Func<UserGuideConditionInfo, UserGuideConditionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey112
        {
            internal System.Delegate act;

            internal bool <>m__0(ExtraUserGuideConditionInfo obj) => 
                ((Func<ExtraUserGuideConditionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey113
        {
            internal System.Delegate act;

            internal int <>m__0(ExtraUserGuideConditionInfo x, ExtraUserGuideConditionInfo y) => 
                ((Func<ExtraUserGuideConditionInfo, ExtraUserGuideConditionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey114
        {
            internal System.Delegate act;

            internal bool <>m__0(UserGuideStepInfoPageList obj) => 
                ((Func<UserGuideStepInfoPageList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey115
        {
            internal System.Delegate act;

            internal int <>m__0(UserGuideStepInfoPageList x, UserGuideStepInfoPageList y) => 
                ((Func<UserGuideStepInfoPageList, UserGuideStepInfoPageList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey116
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneCountParamList obj) => 
                ((Func<SceneCountParamList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey117
        {
            internal System.Delegate act;

            internal int <>m__0(SceneCountParamList x, SceneCountParamList y) => 
                ((Func<SceneCountParamList, SceneCountParamList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey118
        {
            internal System.Delegate act;

            internal bool <>m__0(SphereCollisionInfo obj) => 
                ((Func<SphereCollisionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey119
        {
            internal System.Delegate act;

            internal int <>m__0(SphereCollisionInfo x, SphereCollisionInfo y) => 
                ((Func<SphereCollisionInfo, SphereCollisionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11A
        {
            internal System.Delegate act;

            internal bool <>m__0(CapsuleCollisionInfo obj) => 
                ((Func<CapsuleCollisionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11B
        {
            internal System.Delegate act;

            internal int <>m__0(CapsuleCollisionInfo x, CapsuleCollisionInfo y) => 
                ((Func<CapsuleCollisionInfo, CapsuleCollisionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11C
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcShipInfoShipTemplateInfo obj) => 
                ((Func<NpcShipInfoShipTemplateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11D
        {
            internal System.Delegate act;

            internal int <>m__0(NpcShipInfoShipTemplateInfo x, NpcShipInfoShipTemplateInfo y) => 
                ((Func<NpcShipInfoShipTemplateInfo, NpcShipInfoShipTemplateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11E
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestCompleteCondInfo obj) => 
                ((Func<QuestCompleteCondInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey11F
        {
            internal System.Delegate act;

            internal int <>m__0(QuestCompleteCondInfo x, QuestCompleteCondInfo y) => 
                ((Func<QuestCompleteCondInfo, QuestCompleteCondInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12
        {
            internal System.Delegate act;

            internal bool <>m__0(string obj) => 
                ((Func<string, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey120
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestPostEventInfo obj) => 
                ((Func<QuestPostEventInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey121
        {
            internal System.Delegate act;

            internal int <>m__0(QuestPostEventInfo x, QuestPostEventInfo y) => 
                ((Func<QuestPostEventInfo, QuestPostEventInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey122
        {
            internal System.Delegate act;

            internal bool <>m__0(RecommendItemInfo obj) => 
                ((Func<RecommendItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey123
        {
            internal System.Delegate act;

            internal int <>m__0(RecommendItemInfo x, RecommendItemInfo y) => 
                ((Func<RecommendItemInfo, RecommendItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey124
        {
            internal System.Delegate act;

            internal bool <>m__0(UnlockItemInfo obj) => 
                ((Func<UnlockItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey125
        {
            internal System.Delegate act;

            internal int <>m__0(UnlockItemInfo x, UnlockItemInfo y) => 
                ((Func<UnlockItemInfo, UnlockItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey126
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneObject3DInfo obj) => 
                ((Func<SceneObject3DInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey127
        {
            internal System.Delegate act;

            internal int <>m__0(SceneObject3DInfo x, SceneObject3DInfo y) => 
                ((Func<SceneObject3DInfo, SceneObject3DInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey128
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneDummyObjectInfo obj) => 
                ((Func<SceneDummyObjectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey129
        {
            internal System.Delegate act;

            internal int <>m__0(SceneDummyObjectInfo x, SceneDummyObjectInfo y) => 
                ((Func<SceneDummyObjectInfo, SceneDummyObjectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12A
        {
            internal System.Delegate act;

            internal bool <>m__0(EmptyDummyLocationInfo obj) => 
                ((Func<EmptyDummyLocationInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12B
        {
            internal System.Delegate act;

            internal int <>m__0(EmptyDummyLocationInfo x, EmptyDummyLocationInfo y) => 
                ((Func<EmptyDummyLocationInfo, EmptyDummyLocationInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12C
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneWayPointListInfo obj) => 
                ((Func<SceneWayPointListInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12D
        {
            internal System.Delegate act;

            internal int <>m__0(SceneWayPointListInfo x, SceneWayPointListInfo y) => 
                ((Func<SceneWayPointListInfo, SceneWayPointListInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12E
        {
            internal System.Delegate act;

            internal bool <>m__0(PVector3D obj) => 
                ((Func<PVector3D, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey12F
        {
            internal System.Delegate act;

            internal int <>m__0(PVector3D x, PVector3D y) => 
                ((Func<PVector3D, PVector3D, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13
        {
            internal System.Delegate act;

            internal bool <>m__0(string arg) => 
                ((Func<string, bool>) this.act)(arg);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey130
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBStarfieldsSimpleInfo obj) => 
                ((Func<GDBStarfieldsSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey131
        {
            internal System.Delegate act;

            internal int <>m__0(GDBStarfieldsSimpleInfo x, GDBStarfieldsSimpleInfo y) => 
                ((Func<GDBStarfieldsSimpleInfo, GDBStarfieldsSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey132
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBLinkInfo obj) => 
                ((Func<GDBLinkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey133
        {
            internal System.Delegate act;

            internal int <>m__0(GDBLinkInfo x, GDBLinkInfo y) => 
                ((Func<GDBLinkInfo, GDBLinkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey134
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBStargroupInfo obj) => 
                ((Func<GDBStargroupInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey135
        {
            internal System.Delegate act;

            internal int <>m__0(GDBStargroupInfo x, GDBStargroupInfo y) => 
                ((Func<GDBStargroupInfo, GDBStargroupInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey136
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBSolarSystemSimpleInfo obj) => 
                ((Func<GDBSolarSystemSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey137
        {
            internal System.Delegate act;

            internal int <>m__0(GDBSolarSystemSimpleInfo x, GDBSolarSystemSimpleInfo y) => 
                ((Func<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey138
        {
            internal System.Delegate act;

            internal bool <>m__0(IdWeightInfo obj) => 
                ((Func<IdWeightInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey139
        {
            internal System.Delegate act;

            internal int <>m__0(IdWeightInfo x, IdWeightInfo y) => 
                ((Func<IdWeightInfo, IdWeightInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13A
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBPlanetInfo obj) => 
                ((Func<GDBPlanetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13B
        {
            internal System.Delegate act;

            internal int <>m__0(GDBPlanetInfo x, GDBPlanetInfo y) => 
                ((Func<GDBPlanetInfo, GDBPlanetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13C
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBMoonInfo obj) => 
                ((Func<GDBMoonInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13D
        {
            internal System.Delegate act;

            internal int <>m__0(GDBMoonInfo x, GDBMoonInfo y) => 
                ((Func<GDBMoonInfo, GDBMoonInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13E
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBPlanetLikeBuildingInfo obj) => 
                ((Func<GDBPlanetLikeBuildingInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey13F
        {
            internal System.Delegate act;

            internal int <>m__0(GDBPlanetLikeBuildingInfo x, GDBPlanetLikeBuildingInfo y) => 
                ((Func<GDBPlanetLikeBuildingInfo, GDBPlanetLikeBuildingInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14
        {
            internal System.Delegate act;

            internal void <>m__0(string arg0)
            {
                ((Action<string>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey140
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBSpaceStationNpcTalkerInfo obj) => 
                ((Func<GDBSpaceStationNpcTalkerInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey141
        {
            internal System.Delegate act;

            internal int <>m__0(GDBSpaceStationNpcTalkerInfo x, GDBSpaceStationNpcTalkerInfo y) => 
                ((Func<GDBSpaceStationNpcTalkerInfo, GDBSpaceStationNpcTalkerInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey142
        {
            internal System.Delegate act;

            internal bool <>m__0(HelloDialogIdToCompletedMainQuestInfo obj) => 
                ((Func<HelloDialogIdToCompletedMainQuestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey143
        {
            internal System.Delegate act;

            internal int <>m__0(HelloDialogIdToCompletedMainQuestInfo x, HelloDialogIdToCompletedMainQuestInfo y) => 
                ((Func<HelloDialogIdToCompletedMainQuestInfo, HelloDialogIdToCompletedMainQuestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey144
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBAsteroidGatherPointPositionInfo obj) => 
                ((Func<GDBAsteroidGatherPointPositionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey145
        {
            internal System.Delegate act;

            internal int <>m__0(GDBAsteroidGatherPointPositionInfo x, GDBAsteroidGatherPointPositionInfo y) => 
                ((Func<GDBAsteroidGatherPointPositionInfo, GDBAsteroidGatherPointPositionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey146
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBSpaceStationInfo obj) => 
                ((Func<GDBSpaceStationInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey147
        {
            internal System.Delegate act;

            internal int <>m__0(GDBSpaceStationInfo x, GDBSpaceStationInfo y) => 
                ((Func<GDBSpaceStationInfo, GDBSpaceStationInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey148
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBStargateInfo obj) => 
                ((Func<GDBStargateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey149
        {
            internal System.Delegate act;

            internal int <>m__0(GDBStargateInfo x, GDBStargateInfo y) => 
                ((Func<GDBStargateInfo, GDBStargateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14A
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBWormholeGateInfo obj) => 
                ((Func<GDBWormholeGateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14B
        {
            internal System.Delegate act;

            internal int <>m__0(GDBWormholeGateInfo x, GDBWormholeGateInfo y) => 
                ((Func<GDBWormholeGateInfo, GDBWormholeGateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14C
        {
            internal System.Delegate act;

            internal bool <>m__0(GDBSceneDummyInfo obj) => 
                ((Func<GDBSceneDummyInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14D
        {
            internal System.Delegate act;

            internal int <>m__0(GDBSceneDummyInfo x, GDBSceneDummyInfo y) => 
                ((Func<GDBSceneDummyInfo, GDBSceneDummyInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14E
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingLocationInfo obj) => 
                ((Func<GuildBuildingLocationInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey14F
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingLocationInfo x, GuildBuildingLocationInfo y) => 
                ((Func<GuildBuildingLocationInfo, GuildBuildingLocationInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15
        {
            internal System.Delegate act;

            internal int <>m__0(string x, string y) => 
                ((Func<string, string, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey150
        {
            internal System.Delegate act;

            internal bool <>m__0(IClientSpaceObjectEventListener obj) => 
                ((Func<IClientSpaceObjectEventListener, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey151
        {
            internal System.Delegate act;

            internal int <>m__0(IClientSpaceObjectEventListener x, IClientSpaceObjectEventListener y) => 
                ((Func<IClientSpaceObjectEventListener, IClientSpaceObjectEventListener, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey152
        {
            internal System.Delegate act;

            internal bool <>m__0(ClientSpaceObject obj) => 
                ((Func<ClientSpaceObject, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey153
        {
            internal System.Delegate act;

            internal int <>m__0(ClientSpaceObject x, ClientSpaceObject y) => 
                ((Func<ClientSpaceObject, ClientSpaceObject, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey154
        {
            internal System.Delegate act;

            internal bool <>m__0(ClientMoon obj) => 
                ((Func<ClientMoon, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey155
        {
            internal System.Delegate act;

            internal int <>m__0(ClientMoon x, ClientMoon y) => 
                ((Func<ClientMoon, ClientMoon, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey156
        {
            internal System.Delegate act;

            internal bool <>m__0(uint obj) => 
                ((Func<uint, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey157
        {
            internal System.Delegate act;

            internal int <>m__0(uint x, uint y) => 
                ((Func<uint, uint, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey158
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSyncEventOnHitInfo obj) => 
                ((Func<LBSyncEventOnHitInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey159
        {
            internal System.Delegate act;

            internal int <>m__0(LBSyncEventOnHitInfo x, LBSyncEventOnHitInfo y) => 
                ((Func<LBSyncEventOnHitInfo, LBSyncEventOnHitInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15A
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSyncEvent obj) => 
                ((Func<LBSyncEvent, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15B
        {
            internal System.Delegate act;

            internal int <>m__0(LBSyncEvent x, LBSyncEvent y) => 
                ((Func<LBSyncEvent, LBSyncEvent, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15C
        {
            internal System.Delegate act;

            internal bool <>m__0(EquipEffectVirtualBufInfo obj) => 
                ((Func<EquipEffectVirtualBufInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15D
        {
            internal System.Delegate act;

            internal void <>m__0(LBBulletDamageInfo srcInfo, LBBulletDamageInfo destInfo)
            {
                ((Action<LBBulletDamageInfo, LBBulletDamageInfo>) this.act)(srcInfo, destInfo);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15E
        {
            internal System.Delegate act;

            internal bool <>m__0(LBProcessSingleUnitLaunchInfo obj) => 
                ((Func<LBProcessSingleUnitLaunchInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey15F
        {
            internal System.Delegate act;

            internal int <>m__0(LBProcessSingleUnitLaunchInfo x, LBProcessSingleUnitLaunchInfo y) => 
                ((Func<LBProcessSingleUnitLaunchInfo, LBProcessSingleUnitLaunchInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16
        {
            internal System.Delegate act;

            internal bool <>m__0(DummyType obj) => 
                ((Func<DummyType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey160
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSpaceProcessDroneDefenderLaunch.AttackChance obj) => 
                ((Func<LBSpaceProcessDroneDefenderLaunch.AttackChance, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey161
        {
            internal System.Delegate act;

            internal int <>m__0(LBSpaceProcessDroneDefenderLaunch.AttackChance x, LBSpaceProcessDroneDefenderLaunch.AttackChance y) => 
                ((Func<LBSpaceProcessDroneDefenderLaunch.AttackChance, LBSpaceProcessDroneDefenderLaunch.AttackChance, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey162
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<ushort, uint> obj) => 
                ((Func<KeyValuePair<ushort, uint>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey163
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<ushort, uint> x, KeyValuePair<ushort, uint> y) => 
                ((Func<KeyValuePair<ushort, uint>, KeyValuePair<ushort, uint>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey164
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStaticWeaponEquipSlotGroup obj) => 
                ((Func<LBStaticWeaponEquipSlotGroup, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey165
        {
            internal System.Delegate act;

            internal int <>m__0(LBStaticWeaponEquipSlotGroup x, LBStaticWeaponEquipSlotGroup y) => 
                ((Func<LBStaticWeaponEquipSlotGroup, LBStaticWeaponEquipSlotGroup, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey166
        {
            internal System.Delegate act;

            internal bool <>m__0(IPropertiesProvider obj) => 
                ((Func<IPropertiesProvider, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey167
        {
            internal System.Delegate act;

            internal int <>m__0(IPropertiesProvider x, IPropertiesProvider y) => 
                ((Func<IPropertiesProvider, IPropertiesProvider, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey168
        {
            internal System.Delegate act;

            internal bool <>m__0(LBWeaponGroupBase obj) => 
                ((Func<LBWeaponGroupBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey169
        {
            internal System.Delegate act;

            internal int <>m__0(LBWeaponGroupBase x, LBWeaponGroupBase y) => 
                ((Func<LBWeaponGroupBase, LBWeaponGroupBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16A
        {
            internal System.Delegate act;

            internal bool <>m__0(LBEquipGroupBase obj) => 
                ((Func<LBEquipGroupBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16B
        {
            internal System.Delegate act;

            internal int <>m__0(LBEquipGroupBase x, LBEquipGroupBase y) => 
                ((Func<LBEquipGroupBase, LBEquipGroupBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16C
        {
            internal System.Delegate act;

            internal bool <>m__0(ILBSpaceTarget obj) => 
                ((Func<ILBSpaceTarget, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16D
        {
            internal System.Delegate act;

            internal int <>m__0(ILBSpaceTarget x, ILBSpaceTarget y) => 
                ((Func<ILBSpaceTarget, ILBSpaceTarget, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16E
        {
            internal System.Delegate act;

            internal bool <>m__0(object obj) => 
                ((Func<object, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey16F
        {
            internal System.Delegate act;

            internal int <>m__0(object x, object y) => 
                ((Func<object, object, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17
        {
            internal System.Delegate act;

            internal int <>m__0(DummyType x, DummyType y) => 
                ((Func<DummyType, DummyType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey170
        {
            internal System.Delegate act;

            internal bool <>m__0(LBBufBase obj) => 
                ((Func<LBBufBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey171
        {
            internal System.Delegate act;

            internal int <>m__0(LBBufBase x, LBBufBase y) => 
                ((Func<LBBufBase, LBBufBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey172
        {
            internal System.Delegate act;

            internal bool <>m__0(LBShipFightBuf obj) => 
                ((Func<LBShipFightBuf, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey173
        {
            internal System.Delegate act;

            internal int <>m__0(LBShipFightBuf x, LBShipFightBuf y) => 
                ((Func<LBShipFightBuf, LBShipFightBuf, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey174
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStoreItem obj) => 
                ((Func<LBStoreItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey175
        {
            internal System.Delegate act;

            internal int <>m__0(LBStoreItem x, LBStoreItem y) => 
                ((Func<LBStoreItem, LBStoreItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey176
        {
            internal System.Delegate act;

            internal bool <>m__0(ItemInfo obj) => 
                ((Func<ItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey177
        {
            internal System.Delegate act;

            internal int <>m__0(ItemInfo x, ItemInfo y) => 
                ((Func<ItemInfo, ItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey178
        {
            internal System.Delegate act;

            internal bool <>m__0(LBShipStoreItemBase obj) => 
                ((Func<LBShipStoreItemBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey179
        {
            internal System.Delegate act;

            internal int <>m__0(LBShipStoreItemBase x, LBShipStoreItemBase y) => 
                ((Func<LBShipStoreItemBase, LBShipStoreItemBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17A
        {
            internal System.Delegate act;

            internal bool <>m__0(DropInfo4SinglePlayer obj) => 
                ((Func<DropInfo4SinglePlayer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17B
        {
            internal System.Delegate act;

            internal int <>m__0(DropInfo4SinglePlayer x, DropInfo4SinglePlayer y) => 
                ((Func<DropInfo4SinglePlayer, DropInfo4SinglePlayer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17C
        {
            internal System.Delegate act;

            internal bool <>m__0(CustomizedParameterInfo obj) => 
                ((Func<CustomizedParameterInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17D
        {
            internal System.Delegate act;

            internal int <>m__0(CustomizedParameterInfo x, CustomizedParameterInfo y) => 
                ((Func<CustomizedParameterInfo, CustomizedParameterInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17E
        {
            internal System.Delegate act;

            internal bool <>m__0(ILBSpaceDropBox obj) => 
                ((Func<ILBSpaceDropBox, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey17F
        {
            internal System.Delegate act;

            internal bool <>m__0(AllianceMemberInfo obj) => 
                ((Func<AllianceMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey180
        {
            internal System.Delegate act;

            internal int <>m__0(AllianceMemberInfo x, AllianceMemberInfo y) => 
                ((Func<AllianceMemberInfo, AllianceMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey181
        {
            internal System.Delegate act;

            internal bool <>m__0(AllianceInviteInfo obj) => 
                ((Func<AllianceInviteInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey182
        {
            internal System.Delegate act;

            internal int <>m__0(AllianceInviteInfo x, AllianceInviteInfo y) => 
                ((Func<AllianceInviteInfo, AllianceInviteInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey183
        {
            internal System.Delegate act;

            internal bool <>m__0(CharChipSlotInfo obj) => 
                ((Func<CharChipSlotInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey184
        {
            internal System.Delegate act;

            internal int <>m__0(CharChipSlotInfo x, CharChipSlotInfo y) => 
                ((Func<CharChipSlotInfo, CharChipSlotInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey185
        {
            internal System.Delegate act;

            internal bool <>m__0(LostItemInfo obj) => 
                ((Func<LostItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey186
        {
            internal System.Delegate act;

            internal int <>m__0(LostItemInfo x, LostItemInfo y) => 
                ((Func<LostItemInfo, LostItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey187
        {
            internal System.Delegate act;

            internal bool <>m__0(ItemCompensationStatus obj) => 
                ((Func<ItemCompensationStatus, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey188
        {
            internal System.Delegate act;

            internal int <>m__0(ItemCompensationStatus x, ItemCompensationStatus y) => 
                ((Func<ItemCompensationStatus, ItemCompensationStatus, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey189
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipSlotGroupInfo obj) => 
                ((Func<ShipSlotGroupInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18A
        {
            internal System.Delegate act;

            internal int <>m__0(ShipSlotGroupInfo x, ShipSlotGroupInfo y) => 
                ((Func<ShipSlotGroupInfo, ShipSlotGroupInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18B
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcCaptainInfo obj) => 
                ((Func<NpcCaptainInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18C
        {
            internal System.Delegate act;

            internal int <>m__0(NpcCaptainInfo x, NpcCaptainInfo y) => 
                ((Func<NpcCaptainInfo, NpcCaptainInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18D
        {
            internal System.Delegate act;

            internal bool <>m__0(IdLevelInfo obj) => 
                ((Func<IdLevelInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18E
        {
            internal System.Delegate act;

            internal int <>m__0(IdLevelInfo x, IdLevelInfo y) => 
                ((Func<IdLevelInfo, IdLevelInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey18F
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcCaptainShipInfo obj) => 
                ((Func<NpcCaptainShipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey190
        {
            internal System.Delegate act;

            internal int <>m__0(NpcCaptainShipInfo x, NpcCaptainShipInfo y) => 
                ((Func<NpcCaptainShipInfo, NpcCaptainShipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey191
        {
            internal System.Delegate act;

            internal bool <>m__0(DelegateMissionInvadeInfo obj) => 
                ((Func<DelegateMissionInvadeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey192
        {
            internal System.Delegate act;

            internal int <>m__0(DelegateMissionInvadeInfo x, DelegateMissionInvadeInfo y) => 
                ((Func<DelegateMissionInvadeInfo, DelegateMissionInvadeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey193
        {
            internal System.Delegate act;

            internal bool <>m__0(ulong obj) => 
                ((Func<ulong, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey194
        {
            internal System.Delegate act;

            internal int <>m__0(ulong x, ulong y) => 
                ((Func<ulong, ulong, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey195
        {
            internal System.Delegate act;

            internal bool <>m__0(DelegateMissionInvadeEventInfo obj) => 
                ((Func<DelegateMissionInvadeEventInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey196
        {
            internal System.Delegate act;

            internal int <>m__0(DelegateMissionInvadeEventInfo x, DelegateMissionInvadeEventInfo y) => 
                ((Func<DelegateMissionInvadeEventInfo, DelegateMissionInvadeEventInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey197
        {
            internal System.Delegate act;

            internal bool <>m__0(SimpleItemInfoWithCount obj) => 
                ((Func<SimpleItemInfoWithCount, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey198
        {
            internal System.Delegate act;

            internal int <>m__0(SimpleItemInfoWithCount x, SimpleItemInfoWithCount y) => 
                ((Func<SimpleItemInfoWithCount, SimpleItemInfoWithCount, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey199
        {
            internal System.Delegate act;

            internal bool <>m__0(byte[] obj) => 
                ((Func<byte[], bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19A
        {
            internal System.Delegate act;

            internal int <>m__0(byte[] x, byte[] y) => 
                ((Func<byte[], byte[], int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19B
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcDialogOptInfo obj) => 
                ((Func<NpcDialogOptInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19C
        {
            internal System.Delegate act;

            internal int <>m__0(NpcDialogOptInfo x, NpcDialogOptInfo y) => 
                ((Func<NpcDialogOptInfo, NpcDialogOptInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19D
        {
            internal System.Delegate act;

            internal bool <>m__0(RankingTargetInfo obj) => 
                ((Func<RankingTargetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19E
        {
            internal System.Delegate act;

            internal int <>m__0(RankingTargetInfo x, RankingTargetInfo y) => 
                ((Func<RankingTargetInfo, RankingTargetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey19F
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneCameraNameObjIdPair obj) => 
                ((Func<SceneCameraNameObjIdPair, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A0
        {
            internal System.Delegate act;

            internal int <>m__0(SceneCameraNameObjIdPair x, SceneCameraNameObjIdPair y) => 
                ((Func<SceneCameraNameObjIdPair, SceneCameraNameObjIdPair, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A1
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneNpcTalkerChatInfo obj) => 
                ((Func<SceneNpcTalkerChatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A2
        {
            internal System.Delegate act;

            internal int <>m__0(SceneNpcTalkerChatInfo x, SceneNpcTalkerChatInfo y) => 
                ((Func<SceneNpcTalkerChatInfo, SceneNpcTalkerChatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A3
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipStoreItemInfo obj) => 
                ((Func<ShipStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A4
        {
            internal System.Delegate act;

            internal int <>m__0(ShipStoreItemInfo x, ShipStoreItemInfo y) => 
                ((Func<ShipStoreItemInfo, ShipStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A5
        {
            internal System.Delegate act;

            internal bool <>m__0(SignalInfo obj) => 
                ((Func<SignalInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A6
        {
            internal System.Delegate act;

            internal int <>m__0(SignalInfo x, SignalInfo y) => 
                ((Func<SignalInfo, SignalInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A7
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcCaptainStaticInfo obj) => 
                ((Func<NpcCaptainStaticInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A8
        {
            internal System.Delegate act;

            internal int <>m__0(NpcCaptainStaticInfo x, NpcCaptainStaticInfo y) => 
                ((Func<NpcCaptainStaticInfo, NpcCaptainStaticInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1A9
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcCaptainSimpleDynamicInfo obj) => 
                ((Func<NpcCaptainSimpleDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AA
        {
            internal System.Delegate act;

            internal int <>m__0(NpcCaptainSimpleDynamicInfo x, NpcCaptainSimpleDynamicInfo y) => 
                ((Func<NpcCaptainSimpleDynamicInfo, NpcCaptainSimpleDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AB
        {
            internal System.Delegate act;

            internal bool <>m__0(IDFRFilterResultItem obj) => 
                ((Func<IDFRFilterResultItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AC
        {
            internal System.Delegate act;

            internal int <>m__0(IDFRFilterResultItem x, IDFRFilterResultItem y) => 
                ((Func<IDFRFilterResultItem, IDFRFilterResultItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildJobType obj) => 
                ((Func<GuildJobType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildJobType x, GuildJobType y) => 
                ((Func<GuildJobType, GuildJobType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1AF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMemberInfo obj) => 
                ((Func<GuildMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B0
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMemberInfo x, GuildMemberInfo y) => 
                ((Func<GuildMemberInfo, GuildMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B1
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildJoinApplyBasicInfo obj) => 
                ((Func<GuildJoinApplyBasicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B2
        {
            internal System.Delegate act;

            internal int <>m__0(GuildJoinApplyBasicInfo x, GuildJoinApplyBasicInfo y) => 
                ((Func<GuildJoinApplyBasicInfo, GuildJoinApplyBasicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B3
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildStaffingLogInfo obj) => 
                ((Func<GuildStaffingLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B4
        {
            internal System.Delegate act;

            internal int <>m__0(GuildStaffingLogInfo x, GuildStaffingLogInfo y) => 
                ((Func<GuildStaffingLogInfo, GuildStaffingLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B5
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMessageInfo obj) => 
                ((Func<GuildMessageInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B6
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMessageInfo x, GuildMessageInfo y) => 
                ((Func<GuildMessageInfo, GuildMessageInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMomentsInfo obj) => 
                ((Func<GuildMomentsInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMomentsInfo x, GuildMomentsInfo y) => 
                ((Func<GuildMomentsInfo, GuildMomentsInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1B9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildStoreItemListInfo obj) => 
                ((Func<GuildStoreItemListInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildStoreItemListInfo x, GuildStoreItemListInfo y) => 
                ((Func<GuildStoreItemListInfo, GuildStoreItemListInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BB
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildStoreItemInfo obj) => 
                ((Func<GuildStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BC
        {
            internal System.Delegate act;

            internal int <>m__0(GuildStoreItemInfo x, GuildStoreItemInfo y) => 
                ((Func<GuildStoreItemInfo, GuildStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildActionInfo obj) => 
                ((Func<GuildActionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildActionInfo x, GuildActionInfo y) => 
                ((Func<GuildActionInfo, GuildActionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1BF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildProductionLineInfo obj) => 
                ((Func<GuildProductionLineInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C0
        {
            internal System.Delegate act;

            internal int <>m__0(GuildProductionLineInfo x, GuildProductionLineInfo y) => 
                ((Func<GuildProductionLineInfo, GuildProductionLineInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C1
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFleetInfo obj) => 
                ((Func<GuildFleetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C2
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFleetInfo x, GuildFleetInfo y) => 
                ((Func<GuildFleetInfo, GuildFleetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C3
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFleetMemberInfo obj) => 
                ((Func<GuildFleetMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C4
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFleetMemberInfo x, GuildFleetMemberInfo y) => 
                ((Func<GuildFleetMemberInfo, GuildFleetMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C5
        {
            internal System.Delegate act;

            internal bool <>m__0(FleetPosition obj) => 
                ((Func<FleetPosition, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C6
        {
            internal System.Delegate act;

            internal int <>m__0(FleetPosition x, FleetPosition y) => 
                ((Func<FleetPosition, FleetPosition, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildCurrencyLogInfo obj) => 
                ((Func<GuildCurrencyLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildCurrencyLogInfo x, GuildCurrencyLogInfo y) => 
                ((Func<GuildCurrencyLogInfo, GuildCurrencyLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1C9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemInfo obj) => 
                ((Func<GuildSolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemInfo x, GuildSolarSystemInfo y) => 
                ((Func<GuildSolarSystemInfo, GuildSolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CB
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingInfo obj) => 
                ((Func<GuildBuildingInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CC
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingInfo x, GuildBuildingInfo y) => 
                ((Func<GuildBuildingInfo, GuildBuildingInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildPurchaseOrderInfo obj) => 
                ((Func<GuildPurchaseOrderInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildPurchaseOrderInfo x, GuildPurchaseOrderInfo y) => 
                ((Func<GuildPurchaseOrderInfo, GuildPurchaseOrderInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1CF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildPurchaseLogInfo obj) => 
                ((Func<GuildPurchaseLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D0
        {
            internal System.Delegate act;

            internal int <>m__0(GuildPurchaseLogInfo x, GuildPurchaseLogInfo y) => 
                ((Func<GuildPurchaseLogInfo, GuildPurchaseLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D1
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipCustomTemplateInfo obj) => 
                ((Func<ShipCustomTemplateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D2
        {
            internal System.Delegate act;

            internal int <>m__0(ShipCustomTemplateInfo x, ShipCustomTemplateInfo y) => 
                ((Func<ShipCustomTemplateInfo, ShipCustomTemplateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D3
        {
            internal System.Delegate act;

            internal bool <>m__0(LossCompensation obj) => 
                ((Func<LossCompensation, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D4
        {
            internal System.Delegate act;

            internal int <>m__0(LossCompensation x, LossCompensation y) => 
                ((Func<LossCompensation, LossCompensation, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D5
        {
            internal System.Delegate act;

            internal bool <>m__0(LostItem obj) => 
                ((Func<LostItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D6
        {
            internal System.Delegate act;

            internal int <>m__0(LostItem x, LostItem y) => 
                ((Func<LostItem, LostItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMiningAreaInfo obj) => 
                ((Func<GuildMiningAreaInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMiningAreaInfo x, GuildMiningAreaInfo y) => 
                ((Func<GuildMiningAreaInfo, GuildMiningAreaInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1D9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMiningMineralInfo obj) => 
                ((Func<GuildMiningMineralInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMiningMineralInfo x, GuildMiningMineralInfo y) => 
                ((Func<GuildMiningMineralInfo, GuildMiningMineralInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DB
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTradePortExtraInfo obj) => 
                ((Func<GuildTradePortExtraInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DC
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTradePortExtraInfo x, GuildTradePortExtraInfo y) => 
                ((Func<GuildTradePortExtraInfo, GuildTradePortExtraInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTradePurchaseOrderTransportLogInfo obj) => 
                ((Func<GuildTradePurchaseOrderTransportLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTradePurchaseOrderTransportLogInfo x, GuildTradePurchaseOrderTransportLogInfo y) => 
                ((Func<GuildTradePurchaseOrderTransportLogInfo, GuildTradePurchaseOrderTransportLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1DF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBenefitsInfoBase obj) => 
                ((Func<GuildBenefitsInfoBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E
        {
            internal System.Delegate act;

            internal void <>m__0()
            {
                ((Action) this.act)();
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E0
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBenefitsInfoBase x, GuildBenefitsInfoBase y) => 
                ((Func<GuildBenefitsInfoBase, GuildBenefitsInfoBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E1
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildAllianceInviteInfo obj) => 
                ((Func<GuildAllianceInviteInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E2
        {
            internal System.Delegate act;

            internal int <>m__0(GuildAllianceInviteInfo x, GuildAllianceInviteInfo y) => 
                ((Func<GuildAllianceInviteInfo, GuildAllianceInviteInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E3
        {
            internal System.Delegate act;

            internal bool <>m__0(BuildingLostReport obj) => 
                ((Func<BuildingLostReport, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E4
        {
            internal System.Delegate act;

            internal int <>m__0(BuildingLostReport x, BuildingLostReport y) => 
                ((Func<BuildingLostReport, BuildingLostReport, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E5
        {
            internal System.Delegate act;

            internal bool <>m__0(SovereignLostReport obj) => 
                ((Func<SovereignLostReport, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E6
        {
            internal System.Delegate act;

            internal int <>m__0(SovereignLostReport x, SovereignLostReport y) => 
                ((Func<SovereignLostReport, SovereignLostReport, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFlagShipHangarDetailInfo obj) => 
                ((Func<GuildFlagShipHangarDetailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFlagShipHangarDetailInfo x, GuildFlagShipHangarDetailInfo y) => 
                ((Func<GuildFlagShipHangarDetailInfo, GuildFlagShipHangarDetailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1E9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildStaticFlagShipInstanceInfo obj) => 
                ((Func<GuildStaticFlagShipInstanceInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1EA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildStaticFlagShipInstanceInfo x, GuildStaticFlagShipInstanceInfo y) => 
                ((Func<GuildStaticFlagShipInstanceInfo, GuildStaticFlagShipInstanceInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1EB
        {
            internal System.Delegate act;

            internal bool <>m__0(OffLineBufInfo obj) => 
                ((Func<OffLineBufInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1EC
        {
            internal System.Delegate act;

            internal int <>m__0(OffLineBufInfo x, OffLineBufInfo y) => 
                ((Func<OffLineBufInfo, OffLineBufInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1ED
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFlagShipOptLogInfo obj) => 
                ((Func<GuildFlagShipOptLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1EE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFlagShipOptLogInfo x, GuildFlagShipOptLogInfo y) => 
                ((Func<GuildFlagShipOptLogInfo, GuildFlagShipOptLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1EF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildAntiTeleportEffectInfo obj) => 
                ((Func<GuildAntiTeleportEffectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F
        {
            internal System.Delegate act;

            internal bool <>m__0(int obj) => 
                ((Func<int, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F0
        {
            internal System.Delegate act;

            internal int <>m__0(GuildAntiTeleportEffectInfo x, GuildAntiTeleportEffectInfo y) => 
                ((Func<GuildAntiTeleportEffectInfo, GuildAntiTeleportEffectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F1
        {
            internal System.Delegate act;

            internal bool <>m__0(PlayerSimplestInfo obj) => 
                ((Func<PlayerSimplestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F2
        {
            internal System.Delegate act;

            internal int <>m__0(PlayerSimplestInfo x, PlayerSimplestInfo y) => 
                ((Func<PlayerSimplestInfo, PlayerSimplestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F3
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSimplestInfo obj) => 
                ((Func<GuildSimplestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F4
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSimplestInfo x, GuildSimplestInfo y) => 
                ((Func<GuildSimplestInfo, GuildSimplestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F5
        {
            internal System.Delegate act;

            internal bool <>m__0(AllianceBasicInfo obj) => 
                ((Func<AllianceBasicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F6
        {
            internal System.Delegate act;

            internal int <>m__0(AllianceBasicInfo x, AllianceBasicInfo y) => 
                ((Func<AllianceBasicInfo, AllianceBasicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSentryInterestTargetInfo obj) => 
                ((Func<GuildSentryInterestTargetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSentryInterestTargetInfo x, GuildSentryInterestTargetInfo y) => 
                ((Func<GuildSentryInterestTargetInfo, GuildSentryInterestTargetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1F9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingBattleInfo obj) => 
                ((Func<GuildBuildingBattleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingBattleInfo x, GuildBuildingBattleInfo y) => 
                ((Func<GuildBuildingBattleInfo, GuildBuildingBattleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FB
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBattlePlayerKillLostStatInfo obj) => 
                ((Func<GuildBattlePlayerKillLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FC
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBattlePlayerKillLostStatInfo x, GuildBattlePlayerKillLostStatInfo y) => 
                ((Func<GuildBattlePlayerKillLostStatInfo, GuildBattlePlayerKillLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBattleGuildKillLostStatInfo obj) => 
                ((Func<GuildBattleGuildKillLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBattleGuildKillLostStatInfo x, GuildBattleGuildKillLostStatInfo y) => 
                ((Func<GuildBattleGuildKillLostStatInfo, GuildBattleGuildKillLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey1FF
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBattleShipLostStatInfo obj) => 
                ((Func<GuildBattleShipLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2
        {
            internal System.Delegate act;

            internal bool <>m__0(float obj) => 
                ((Func<float, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20
        {
            internal System.Delegate act;

            internal void <>m__0(object obj)
            {
                ((Action<object>) this.act)(obj);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey200
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBattleShipLostStatInfo x, GuildBattleShipLostStatInfo y) => 
                ((Func<GuildBattleShipLostStatInfo, GuildBattleShipLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey201
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBattleGuildBuildingSimpleInfo obj) => 
                ((Func<GuildBattleGuildBuildingSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey202
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBattleGuildBuildingSimpleInfo x, GuildBattleGuildBuildingSimpleInfo y) => 
                ((Func<GuildBattleGuildBuildingSimpleInfo, GuildBattleGuildBuildingSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey203
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo obj) => 
                ((Func<StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey204
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo x, StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo y) => 
                ((Func<StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo, StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey205
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo obj) => 
                ((Func<StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey206
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo x, StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo y) => 
                ((Func<StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo, StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey207
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildSimpleInfo obj) => 
                ((Func<StarMapGuildSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey208
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildSimpleInfo x, StarMapGuildSimpleInfo y) => 
                ((Func<StarMapGuildSimpleInfo, StarMapGuildSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey209
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapAllianceSimpleInfo obj) => 
                ((Func<StarMapAllianceSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20A
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapAllianceSimpleInfo x, StarMapAllianceSimpleInfo y) => 
                ((Func<StarMapAllianceSimpleInfo, StarMapAllianceSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20B
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo obj) => 
                ((Func<StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20C
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo x, StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo y) => 
                ((Func<StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo, StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20D
        {
            internal System.Delegate act;

            internal bool <>m__0(PlayerSimpleInfo obj) => 
                ((Func<PlayerSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20E
        {
            internal System.Delegate act;

            internal int <>m__0(PlayerSimpleInfo x, PlayerSimpleInfo y) => 
                ((Func<PlayerSimpleInfo, PlayerSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey20F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProPlayerSimpleInfo obj) => 
                ((Func<ProPlayerSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21
        {
            internal System.Delegate act;

            internal void <>m__0(object state)
            {
                ((Action<object>) this.act)(state);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey210
        {
            internal System.Delegate act;

            internal int <>m__0(ProPlayerSimpleInfo x, ProPlayerSimpleInfo y) => 
                ((Func<ProPlayerSimpleInfo, ProPlayerSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey211
        {
            internal System.Delegate act;

            internal bool <>m__0(ProItemInfo obj) => 
                ((Func<ProItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey212
        {
            internal System.Delegate act;

            internal int <>m__0(ProItemInfo x, ProItemInfo y) => 
                ((Func<ProItemInfo, ProItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey213
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSimpleItemInfoWithCount obj) => 
                ((Func<ProSimpleItemInfoWithCount, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey214
        {
            internal System.Delegate act;

            internal int <>m__0(ProSimpleItemInfoWithCount x, ProSimpleItemInfoWithCount y) => 
                ((Func<ProSimpleItemInfoWithCount, ProSimpleItemInfoWithCount, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey215
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDelegateMissionInvadeEventInfo obj) => 
                ((Func<ProDelegateMissionInvadeEventInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey216
        {
            internal System.Delegate act;

            internal int <>m__0(ProDelegateMissionInvadeEventInfo x, ProDelegateMissionInvadeEventInfo y) => 
                ((Func<ProDelegateMissionInvadeEventInfo, ProDelegateMissionInvadeEventInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey217
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHiredCaptainInfo obj) => 
                ((Func<ProHiredCaptainInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey218
        {
            internal System.Delegate act;

            internal int <>m__0(ProHiredCaptainInfo x, ProHiredCaptainInfo y) => 
                ((Func<ProHiredCaptainInfo, ProHiredCaptainInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey219
        {
            internal System.Delegate act;

            internal bool <>m__0(ProIdLevelInfo obj) => 
                ((Func<ProIdLevelInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21A
        {
            internal System.Delegate act;

            internal int <>m__0(ProIdLevelInfo x, ProIdLevelInfo y) => 
                ((Func<ProIdLevelInfo, ProIdLevelInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHiredCaptainShipInfo obj) => 
                ((Func<ProHiredCaptainShipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21C
        {
            internal System.Delegate act;

            internal int <>m__0(ProHiredCaptainShipInfo x, ProHiredCaptainShipInfo y) => 
                ((Func<ProHiredCaptainShipInfo, ProHiredCaptainShipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDelegateMissionInvadeInfo obj) => 
                ((Func<ProDelegateMissionInvadeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21E
        {
            internal System.Delegate act;

            internal int <>m__0(ProDelegateMissionInvadeInfo x, ProDelegateMissionInvadeInfo y) => 
                ((Func<ProDelegateMissionInvadeInfo, ProDelegateMissionInvadeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey21F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFormatStringParamInfo obj) => 
                ((Func<ProFormatStringParamInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabResourceContainerBase.AssetCacheItem obj) => 
                ((Func<PrefabResourceContainerBase.AssetCacheItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey220
        {
            internal System.Delegate act;

            internal int <>m__0(ProFormatStringParamInfo x, ProFormatStringParamInfo y) => 
                ((Func<ProFormatStringParamInfo, ProFormatStringParamInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey221
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipStoreItemInfo obj) => 
                ((Func<ProShipStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey222
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipStoreItemInfo x, ProShipStoreItemInfo y) => 
                ((Func<ProShipStoreItemInfo, ProShipStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey223
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCostInfo obj) => 
                ((Func<ProCostInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey224
        {
            internal System.Delegate act;

            internal int <>m__0(ProCostInfo x, ProCostInfo y) => 
                ((Func<ProCostInfo, ProCostInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey225
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipTemplateHighSlotInfo obj) => 
                ((Func<ProShipTemplateHighSlotInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey226
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipTemplateHighSlotInfo x, ProShipTemplateHighSlotInfo y) => 
                ((Func<ProShipTemplateHighSlotInfo, ProShipTemplateHighSlotInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey227
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<CurrencyType, int> obj) => 
                ((Func<KeyValuePair<CurrencyType, int>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey228
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<CurrencyType, int> x, KeyValuePair<CurrencyType, int> y) => 
                ((Func<KeyValuePair<CurrencyType, int>, KeyValuePair<CurrencyType, int>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey229
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLostItemInfo obj) => 
                ((Func<ProLostItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22A
        {
            internal System.Delegate act;

            internal int <>m__0(ProLostItemInfo x, ProLostItemInfo y) => 
                ((Func<ProLostItemInfo, ProLostItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRankingPlayerInfo obj) => 
                ((Func<ProRankingPlayerInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22C
        {
            internal System.Delegate act;

            internal int <>m__0(ProRankingPlayerInfo x, ProRankingPlayerInfo y) => 
                ((Func<ProRankingPlayerInfo, ProRankingPlayerInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRankingGuildInfo obj) => 
                ((Func<ProRankingGuildInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22E
        {
            internal System.Delegate act;

            internal int <>m__0(ProRankingGuildInfo x, ProRankingGuildInfo y) => 
                ((Func<ProRankingGuildInfo, ProRankingGuildInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey22F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRankingAllianceInfo obj) => 
                ((Func<ProRankingAllianceInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabResourceContainerBase.AssetCacheItem x, PrefabResourceContainerBase.AssetCacheItem y) => 
                ((Func<PrefabResourceContainerBase.AssetCacheItem, PrefabResourceContainerBase.AssetCacheItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey230
        {
            internal System.Delegate act;

            internal int <>m__0(ProRankingAllianceInfo x, ProRankingAllianceInfo y) => 
                ((Func<ProRankingAllianceInfo, ProRankingAllianceInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey231
        {
            internal System.Delegate act;

            internal bool <>m__0(ProNpcDialogOptInfo obj) => 
                ((Func<ProNpcDialogOptInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey232
        {
            internal System.Delegate act;

            internal int <>m__0(ProNpcDialogOptInfo x, ProNpcDialogOptInfo y) => 
                ((Func<ProNpcDialogOptInfo, ProNpcDialogOptInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey233
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcCaptainSimpleInfo obj) => 
                ((Func<NpcCaptainSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey234
        {
            internal System.Delegate act;

            internal int <>m__0(NpcCaptainSimpleInfo x, NpcCaptainSimpleInfo y) => 
                ((Func<NpcCaptainSimpleInfo, NpcCaptainSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey235
        {
            internal System.Delegate act;

            internal bool <>m__0(ProNpcCaptainSimpleInfo obj) => 
                ((Func<ProNpcCaptainSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey236
        {
            internal System.Delegate act;

            internal int <>m__0(ProNpcCaptainSimpleInfo x, ProNpcCaptainSimpleInfo y) => 
                ((Func<ProNpcCaptainSimpleInfo, ProNpcCaptainSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey237
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHiredCaptainStaticInfo obj) => 
                ((Func<ProHiredCaptainStaticInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey238
        {
            internal System.Delegate act;

            internal int <>m__0(ProHiredCaptainStaticInfo x, ProHiredCaptainStaticInfo y) => 
                ((Func<ProHiredCaptainStaticInfo, ProHiredCaptainStaticInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey239
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHiredCaptainSimpleDynamicInfo obj) => 
                ((Func<ProHiredCaptainSimpleDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23A
        {
            internal System.Delegate act;

            internal int <>m__0(ProHiredCaptainSimpleDynamicInfo x, ProHiredCaptainSimpleDynamicInfo y) => 
                ((Func<ProHiredCaptainSimpleDynamicInfo, ProHiredCaptainSimpleDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildMemberInfo obj) => 
                ((Func<ProGuildMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23C
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildMemberInfo x, ProGuildMemberInfo y) => 
                ((Func<ProGuildMemberInfo, ProGuildMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildPurchaseLogInfo obj) => 
                ((Func<ProGuildPurchaseLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23E
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildPurchaseLogInfo x, ProGuildPurchaseLogInfo y) => 
                ((Func<ProGuildPurchaseLogInfo, ProGuildPurchaseLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey23F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBuildingInfo obj) => 
                ((Func<ProGuildBuildingInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24
        {
            internal System.Delegate act;

            internal bool <>m__0(ControllerDesc obj) => 
                ((Func<ControllerDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey240
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBuildingInfo x, ProGuildBuildingInfo y) => 
                ((Func<ProGuildBuildingInfo, ProGuildBuildingInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey241
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFleetMemberBasicInfo obj) => 
                ((Func<ProGuildFleetMemberBasicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey242
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFleetMemberBasicInfo x, ProGuildFleetMemberBasicInfo y) => 
                ((Func<ProGuildFleetMemberBasicInfo, ProGuildFleetMemberBasicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey243
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFleetMemberDynamicInfo obj) => 
                ((Func<ProGuildFleetMemberDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey244
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFleetMemberDynamicInfo x, ProGuildFleetMemberDynamicInfo y) => 
                ((Func<ProGuildFleetMemberDynamicInfo, ProGuildFleetMemberDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey245
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFleetMemberDynamicInfo obj) => 
                ((Func<GuildFleetMemberDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey246
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFleetMemberDynamicInfo x, GuildFleetMemberDynamicInfo y) => 
                ((Func<GuildFleetMemberDynamicInfo, GuildFleetMemberDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey247
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFlagShipDetailInfo obj) => 
                ((Func<ProGuildFlagShipDetailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey248
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFlagShipDetailInfo x, ProGuildFlagShipDetailInfo y) => 
                ((Func<ProGuildFlagShipDetailInfo, ProGuildFlagShipDetailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey249
        {
            internal System.Delegate act;

            internal bool <>m__0(ProOffLineBufInfo obj) => 
                ((Func<ProOffLineBufInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24A
        {
            internal System.Delegate act;

            internal int <>m__0(ProOffLineBufInfo x, ProOffLineBufInfo y) => 
                ((Func<ProOffLineBufInfo, ProOffLineBufInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLostItem obj) => 
                ((Func<ProLostItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24C
        {
            internal System.Delegate act;

            internal int <>m__0(ProLostItem x, ProLostItem y) => 
                ((Func<ProLostItem, ProLostItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattlePlayerKillLostStatInfo obj) => 
                ((Func<ProGuildBattlePlayerKillLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24E
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattlePlayerKillLostStatInfo x, ProGuildBattlePlayerKillLostStatInfo y) => 
                ((Func<ProGuildBattlePlayerKillLostStatInfo, ProGuildBattlePlayerKillLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey24F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattleGuildKillLostStatInfo obj) => 
                ((Func<ProGuildBattleGuildKillLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25
        {
            internal System.Delegate act;

            internal int <>m__0(ControllerDesc x, ControllerDesc y) => 
                ((Func<ControllerDesc, ControllerDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey250
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattleGuildKillLostStatInfo x, ProGuildBattleGuildKillLostStatInfo y) => 
                ((Func<ProGuildBattleGuildKillLostStatInfo, ProGuildBattleGuildKillLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey251
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattleShipLostStatInfo obj) => 
                ((Func<ProGuildBattleShipLostStatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey252
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattleShipLostStatInfo x, ProGuildBattleShipLostStatInfo y) => 
                ((Func<ProGuildBattleShipLostStatInfo, ProGuildBattleShipLostStatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey253
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattleGuildBuildingSimpleInfo obj) => 
                ((Func<ProGuildBattleGuildBuildingSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey254
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattleGuildBuildingSimpleInfo x, ProGuildBattleGuildBuildingSimpleInfo y) => 
                ((Func<ProGuildBattleGuildBuildingSimpleInfo, ProGuildBattleGuildBuildingSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey255
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildOccupySolarSystemInfo obj) => 
                ((Func<ProGuildOccupySolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey256
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildOccupySolarSystemInfo x, ProGuildOccupySolarSystemInfo y) => 
                ((Func<ProGuildOccupySolarSystemInfo, ProGuildOccupySolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey257
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAllianceOccupySolarSystemInfo obj) => 
                ((Func<ProAllianceOccupySolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey258
        {
            internal System.Delegate act;

            internal int <>m__0(ProAllianceOccupySolarSystemInfo x, ProAllianceOccupySolarSystemInfo y) => 
                ((Func<ProAllianceOccupySolarSystemInfo, ProAllianceOccupySolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey259
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStarMapGuildSimpleInfo obj) => 
                ((Func<ProStarMapGuildSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25A
        {
            internal System.Delegate act;

            internal int <>m__0(ProStarMapGuildSimpleInfo x, ProStarMapGuildSimpleInfo y) => 
                ((Func<ProStarMapGuildSimpleInfo, ProStarMapGuildSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStarMapAllianceSimpleInfo obj) => 
                ((Func<ProStarMapAllianceSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25C
        {
            internal System.Delegate act;

            internal int <>m__0(ProStarMapAllianceSimpleInfo x, ProStarMapAllianceSimpleInfo y) => 
                ((Func<ProStarMapAllianceSimpleInfo, ProStarMapAllianceSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSolarSystemDynamicInfo obj) => 
                ((Func<ProSolarSystemDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25E
        {
            internal System.Delegate act;

            internal int <>m__0(ProSolarSystemDynamicInfo x, ProSolarSystemDynamicInfo y) => 
                ((Func<ProSolarSystemDynamicInfo, ProSolarSystemDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey25F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildTradePurchaseOrderTransportLogInfo obj) => 
                ((Func<ProGuildTradePurchaseOrderTransportLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabControllerBase obj) => 
                ((Func<PrefabControllerBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey260
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildTradePurchaseOrderTransportLogInfo x, ProGuildTradePurchaseOrderTransportLogInfo y) => 
                ((Func<ProGuildTradePurchaseOrderTransportLogInfo, ProGuildTradePurchaseOrderTransportLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey261
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAllianceMemberInfo obj) => 
                ((Func<ProAllianceMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey262
        {
            internal System.Delegate act;

            internal int <>m__0(ProAllianceMemberInfo x, ProAllianceMemberInfo y) => 
                ((Func<ProAllianceMemberInfo, ProAllianceMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey263
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAllianceInviteInfo obj) => 
                ((Func<ProAllianceInviteInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey264
        {
            internal System.Delegate act;

            internal int <>m__0(ProAllianceInviteInfo x, ProAllianceInviteInfo y) => 
                ((Func<ProAllianceInviteInfo, ProAllianceInviteInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey265
        {
            internal System.Delegate act;

            internal bool <>m__0(DJLink obj) => 
                ((Func<DJLink, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey266
        {
            internal System.Delegate act;

            internal int <>m__0(DJLink x, DJLink y) => 
                ((Func<DJLink, DJLink, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey267
        {
            internal System.Delegate act;

            internal bool <>m__0(DJNode obj) => 
                ((Func<DJNode, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey268
        {
            internal System.Delegate act;

            internal int <>m__0(DJNode x, DJNode y) => 
                ((Func<DJNode, DJNode, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey269
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<DJNode, int> obj) => 
                ((Func<KeyValuePair<DJNode, int>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26A
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<DJNode, int> x, KeyValuePair<DJNode, int> y) => 
                ((Func<KeyValuePair<DJNode, int>, KeyValuePair<DJNode, int>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAuctionItemBriefInfo obj) => 
                ((Func<ProAuctionItemBriefInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26C
        {
            internal System.Delegate act;

            internal int <>m__0(ProAuctionItemBriefInfo x, ProAuctionItemBriefInfo y) => 
                ((Func<ProAuctionItemBriefInfo, ProAuctionItemBriefInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAuctionOrderBriefInfo obj) => 
                ((Func<ProAuctionOrderBriefInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26E
        {
            internal System.Delegate act;

            internal int <>m__0(ProAuctionOrderBriefInfo x, ProAuctionOrderBriefInfo y) => 
                ((Func<ProAuctionOrderBriefInfo, ProAuctionOrderBriefInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey26F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAuctionItemDetailInfo obj) => 
                ((Func<ProAuctionItemDetailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabControllerBase x, PrefabControllerBase y) => 
                ((Func<PrefabControllerBase, PrefabControllerBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey270
        {
            internal System.Delegate act;

            internal int <>m__0(ProAuctionItemDetailInfo x, ProAuctionItemDetailInfo y) => 
                ((Func<ProAuctionItemDetailInfo, ProAuctionItemDetailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey271
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCurrencyUpdateInfo obj) => 
                ((Func<ProCurrencyUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey272
        {
            internal System.Delegate act;

            internal int <>m__0(ProCurrencyUpdateInfo x, ProCurrencyUpdateInfo y) => 
                ((Func<ProCurrencyUpdateInfo, ProCurrencyUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey273
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStoreItemUpdateInfo obj) => 
                ((Func<ProStoreItemUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey274
        {
            internal System.Delegate act;

            internal int <>m__0(ProStoreItemUpdateInfo x, ProStoreItemUpdateInfo y) => 
                ((Func<ProStoreItemUpdateInfo, ProStoreItemUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey275
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipSlotGroupInfo obj) => 
                ((Func<ProShipSlotGroupInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey276
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipSlotGroupInfo x, ProShipSlotGroupInfo y) => 
                ((Func<ProShipSlotGroupInfo, ProShipSlotGroupInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey277
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAmmoStoreItemInfo obj) => 
                ((Func<ProAmmoStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey278
        {
            internal System.Delegate act;

            internal int <>m__0(ProAmmoStoreItemInfo x, ProAmmoStoreItemInfo y) => 
                ((Func<ProAmmoStoreItemInfo, ProAmmoStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey279
        {
            internal System.Delegate act;

            internal bool <>m__0(ProChatContentVoice obj) => 
                ((Func<ProChatContentVoice, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27A
        {
            internal System.Delegate act;

            internal int <>m__0(ProChatContentVoice x, ProChatContentVoice y) => 
                ((Func<ProChatContentVoice, ProChatContentVoice, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27B
        {
            internal System.Delegate act;

            internal bool <>m__0(long obj) => 
                ((Func<long, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27C
        {
            internal System.Delegate act;

            internal int <>m__0(long x, long y) => 
                ((Func<long, long, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCharChipSlotInfo obj) => 
                ((Func<ProCharChipSlotInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27E
        {
            internal System.Delegate act;

            internal int <>m__0(ProCharChipSlotInfo x, ProCharChipSlotInfo y) => 
                ((Func<ProCharChipSlotInfo, ProCharChipSlotInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey27F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSpaceSignalResultInfo obj) => 
                ((Func<ProSpaceSignalResultInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28
        {
            internal System.Delegate act;

            internal bool <>m__0(BundleData.SingleBundleData obj) => 
                ((Func<BundleData.SingleBundleData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey280
        {
            internal System.Delegate act;

            internal int <>m__0(ProSpaceSignalResultInfo x, ProSpaceSignalResultInfo y) => 
                ((Func<ProSpaceSignalResultInfo, ProSpaceSignalResultInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey281
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSignalInfo obj) => 
                ((Func<ProSignalInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey282
        {
            internal System.Delegate act;

            internal int <>m__0(ProSignalInfo x, ProSignalInfo y) => 
                ((Func<ProSignalInfo, ProSignalInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey283
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDevelopmentHistoryInfo obj) => 
                ((Func<ProDevelopmentHistoryInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey284
        {
            internal System.Delegate act;

            internal int <>m__0(ProDevelopmentHistoryInfo x, ProDevelopmentHistoryInfo y) => 
                ((Func<ProDevelopmentHistoryInfo, ProDevelopmentHistoryInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey285
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFactionCreditLevelRewardInfoSub obj) => 
                ((Func<ProFactionCreditLevelRewardInfoSub, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey286
        {
            internal System.Delegate act;

            internal int <>m__0(ProFactionCreditLevelRewardInfoSub x, ProFactionCreditLevelRewardInfoSub y) => 
                ((Func<ProFactionCreditLevelRewardInfoSub, ProFactionCreditLevelRewardInfoSub, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey287
        {
            internal System.Delegate act;

            internal bool <>m__0(ProtoTypeShipListAck.ProShipInstanceData obj) => 
                ((Func<ProtoTypeShipListAck.ProShipInstanceData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey288
        {
            internal System.Delegate act;

            internal int <>m__0(ProtoTypeShipListAck.ProShipInstanceData x, ProtoTypeShipListAck.ProShipInstanceData y) => 
                ((Func<ProtoTypeShipListAck.ProShipInstanceData, ProtoTypeShipListAck.ProShipInstanceData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey289
        {
            internal System.Delegate act;

            internal bool <>m__0(FriendListAck.FriendListData obj) => 
                ((Func<FriendListAck.FriendListData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28A
        {
            internal System.Delegate act;

            internal int <>m__0(FriendListAck.FriendListData x, FriendListAck.FriendListData y) => 
                ((Func<FriendListAck.FriendListData, FriendListAck.FriendListData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProConfigFileMD5Info obj) => 
                ((Func<ProConfigFileMD5Info, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28C
        {
            internal System.Delegate act;

            internal int <>m__0(ProConfigFileMD5Info x, ProConfigFileMD5Info y) => 
                ((Func<ProConfigFileMD5Info, ProConfigFileMD5Info, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFreezingItemInfo obj) => 
                ((Func<ProFreezingItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28E
        {
            internal System.Delegate act;

            internal int <>m__0(ProFreezingItemInfo x, ProFreezingItemInfo y) => 
                ((Func<ProFreezingItemInfo, ProFreezingItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey28F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProActivityInfo obj) => 
                ((Func<ProActivityInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29
        {
            internal System.Delegate act;

            internal int <>m__0(BundleData.SingleBundleData x, BundleData.SingleBundleData y) => 
                ((Func<BundleData.SingleBundleData, BundleData.SingleBundleData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey290
        {
            internal System.Delegate act;

            internal int <>m__0(ProActivityInfo x, ProActivityInfo y) => 
                ((Func<ProActivityInfo, ProActivityInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey291
        {
            internal System.Delegate act;

            internal bool <>m__0(ProPlayerActivityInfo obj) => 
                ((Func<ProPlayerActivityInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey292
        {
            internal System.Delegate act;

            internal int <>m__0(ProPlayerActivityInfo x, ProPlayerActivityInfo y) => 
                ((Func<ProPlayerActivityInfo, ProPlayerActivityInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey293
        {
            internal System.Delegate act;

            internal bool <>m__0(ProPlayerSimplestInfo obj) => 
                ((Func<ProPlayerSimplestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey294
        {
            internal System.Delegate act;

            internal int <>m__0(ProPlayerSimplestInfo x, ProPlayerSimplestInfo y) => 
                ((Func<ProPlayerSimplestInfo, ProPlayerSimplestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey295
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildSimplestInfo obj) => 
                ((Func<ProGuildSimplestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey296
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildSimplestInfo x, ProGuildSimplestInfo y) => 
                ((Func<ProGuildSimplestInfo, ProGuildSimplestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey297
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCustomizedParameterInfo obj) => 
                ((Func<ProCustomizedParameterInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey298
        {
            internal System.Delegate act;

            internal int <>m__0(ProCustomizedParameterInfo x, ProCustomizedParameterInfo y) => 
                ((Func<ProCustomizedParameterInfo, ProCustomizedParameterInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey299
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCreditCurrencyInfo obj) => 
                ((Func<ProCreditCurrencyInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29A
        {
            internal System.Delegate act;

            internal int <>m__0(ProCreditCurrencyInfo x, ProCreditCurrencyInfo y) => 
                ((Func<ProCreditCurrencyInfo, ProCreditCurrencyInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProCharChipSchemeInfo obj) => 
                ((Func<ProCharChipSchemeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29C
        {
            internal System.Delegate act;

            internal int <>m__0(ProCharChipSchemeInfo x, ProCharChipSchemeInfo y) => 
                ((Func<ProCharChipSchemeInfo, ProCharChipSchemeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStoreItemInfo obj) => 
                ((Func<ProStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29E
        {
            internal System.Delegate act;

            internal int <>m__0(ProStoreItemInfo x, ProStoreItemInfo y) => 
                ((Func<ProStoreItemInfo, ProStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey29F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDSHiredCaptainInfo obj) => 
                ((Func<ProDSHiredCaptainInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A
        {
            internal System.Delegate act;

            internal bool <>m__0(StreamingAssetsFileList.ListItem obj) => 
                ((Func<StreamingAssetsFileList.ListItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A0
        {
            internal System.Delegate act;

            internal int <>m__0(ProDSHiredCaptainInfo x, ProDSHiredCaptainInfo y) => 
                ((Func<ProDSHiredCaptainInfo, ProDSHiredCaptainInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFleetInfo obj) => 
                ((Func<ProFleetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A2
        {
            internal System.Delegate act;

            internal int <>m__0(ProFleetInfo x, ProFleetInfo y) => 
                ((Func<ProFleetInfo, ProFleetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A3
        {
            internal System.Delegate act;

            internal bool <>m__0(ProPVPSignalInfo obj) => 
                ((Func<ProPVPSignalInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A4
        {
            internal System.Delegate act;

            internal int <>m__0(ProPVPSignalInfo x, ProPVPSignalInfo y) => 
                ((Func<ProPVPSignalInfo, ProPVPSignalInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProScanProbeInfo obj) => 
                ((Func<ProScanProbeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A6
        {
            internal System.Delegate act;

            internal int <>m__0(ProScanProbeInfo x, ProScanProbeInfo y) => 
                ((Func<ProScanProbeInfo, ProScanProbeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSolarSystemSpaceSignalInfo obj) => 
                ((Func<ProSolarSystemSpaceSignalInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A8
        {
            internal System.Delegate act;

            internal int <>m__0(ProSolarSystemSpaceSignalInfo x, ProSolarSystemSpaceSignalInfo y) => 
                ((Func<ProSolarSystemSpaceSignalInfo, ProSolarSystemSpaceSignalInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2A9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDelegateMissionInfo obj) => 
                ((Func<ProDelegateMissionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AA
        {
            internal System.Delegate act;

            internal int <>m__0(ProDelegateMissionInfo x, ProDelegateMissionInfo y) => 
                ((Func<ProDelegateMissionInfo, ProDelegateMissionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProQuestEnvirmentInfo obj) => 
                ((Func<ProQuestEnvirmentInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AC
        {
            internal System.Delegate act;

            internal int <>m__0(ProQuestEnvirmentInfo x, ProQuestEnvirmentInfo y) => 
                ((Func<ProQuestEnvirmentInfo, ProQuestEnvirmentInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AD
        {
            internal System.Delegate act;

            internal bool <>m__0(ProQuestProcessingInfo obj) => 
                ((Func<ProQuestProcessingInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AE
        {
            internal System.Delegate act;

            internal int <>m__0(ProQuestProcessingInfo x, ProQuestProcessingInfo y) => 
                ((Func<ProQuestProcessingInfo, ProQuestProcessingInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2AF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProQuestWaitForAcceptInfo obj) => 
                ((Func<ProQuestWaitForAcceptInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B
        {
            internal System.Delegate act;

            internal int <>m__0(StreamingAssetsFileList.ListItem x, StreamingAssetsFileList.ListItem y) => 
                ((Func<StreamingAssetsFileList.ListItem, StreamingAssetsFileList.ListItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B0
        {
            internal System.Delegate act;

            internal int <>m__0(ProQuestWaitForAcceptInfo x, ProQuestWaitForAcceptInfo y) => 
                ((Func<ProQuestWaitForAcceptInfo, ProQuestWaitForAcceptInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStoredMailInfo obj) => 
                ((Func<ProStoredMailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B2
        {
            internal System.Delegate act;

            internal int <>m__0(ProStoredMailInfo x, ProStoredMailInfo y) => 
                ((Func<ProStoredMailInfo, ProStoredMailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B3
        {
            internal System.Delegate act;

            internal bool <>m__0(ProIdFloatValueInfo obj) => 
                ((Func<ProIdFloatValueInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B4
        {
            internal System.Delegate act;

            internal int <>m__0(ProIdFloatValueInfo x, ProIdFloatValueInfo y) => 
                ((Func<ProIdFloatValueInfo, ProIdFloatValueInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFactionCreditLevelRewardInfo obj) => 
                ((Func<ProFactionCreditLevelRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B6
        {
            internal System.Delegate act;

            internal int <>m__0(ProFactionCreditLevelRewardInfo x, ProFactionCreditLevelRewardInfo y) => 
                ((Func<ProFactionCreditLevelRewardInfo, ProFactionCreditLevelRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProFactionCreditNpcQuestInfo obj) => 
                ((Func<ProFactionCreditNpcQuestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B8
        {
            internal System.Delegate act;

            internal int <>m__0(ProFactionCreditNpcQuestInfo x, ProFactionCreditNpcQuestInfo y) => 
                ((Func<ProFactionCreditNpcQuestInfo, ProFactionCreditNpcQuestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2B9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProIdValueInfo obj) => 
                ((Func<ProIdValueInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BA
        {
            internal System.Delegate act;

            internal int <>m__0(ProIdValueInfo x, ProIdValueInfo y) => 
                ((Func<ProIdValueInfo, ProIdValueInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProTechUpgradeInfo obj) => 
                ((Func<ProTechUpgradeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BC
        {
            internal System.Delegate act;

            internal int <>m__0(ProTechUpgradeInfo x, ProTechUpgradeInfo y) => 
                ((Func<ProTechUpgradeInfo, ProTechUpgradeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BD
        {
            internal System.Delegate act;

            internal bool <>m__0(ProProductionLineInfo obj) => 
                ((Func<ProProductionLineInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BE
        {
            internal System.Delegate act;

            internal int <>m__0(ProProductionLineInfo x, ProProductionLineInfo y) => 
                ((Func<ProProductionLineInfo, ProProductionLineInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2BF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipCustomTemplateInfo obj) => 
                ((Func<ProShipCustomTemplateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C
        {
            internal System.Delegate act;

            internal void <>m__0(RectTransform driven)
            {
                ((Action<RectTransform>) this.act)(driven);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C0
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipCustomTemplateInfo x, ProShipCustomTemplateInfo y) => 
                ((Func<ProShipCustomTemplateInfo, ProShipCustomTemplateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProKillRecordInfo obj) => 
                ((Func<ProKillRecordInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C2
        {
            internal System.Delegate act;

            internal int <>m__0(ProKillRecordInfo x, ProKillRecordInfo y) => 
                ((Func<ProKillRecordInfo, ProKillRecordInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C3
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDevelopmentProjectInfo obj) => 
                ((Func<ProDevelopmentProjectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C4
        {
            internal System.Delegate act;

            internal int <>m__0(ProDevelopmentProjectInfo x, ProDevelopmentProjectInfo y) => 
                ((Func<ProDevelopmentProjectInfo, ProDevelopmentProjectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProPVPInvadeRescueInfo obj) => 
                ((Func<ProPVPInvadeRescueInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C6
        {
            internal System.Delegate act;

            internal int <>m__0(ProPVPInvadeRescueInfo x, ProPVPInvadeRescueInfo y) => 
                ((Func<ProPVPInvadeRescueInfo, ProPVPInvadeRescueInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRechargeOrder obj) => 
                ((Func<ProRechargeOrder, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C8
        {
            internal System.Delegate act;

            internal int <>m__0(ProRechargeOrder x, ProRechargeOrder y) => 
                ((Func<ProRechargeOrder, ProRechargeOrder, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2C9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRechargeGiftPackage obj) => 
                ((Func<ProRechargeGiftPackage, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CA
        {
            internal System.Delegate act;

            internal int <>m__0(ProRechargeGiftPackage x, ProRechargeGiftPackage y) => 
                ((Func<ProRechargeGiftPackage, ProRechargeGiftPackage, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRechargeMonthlyCard obj) => 
                ((Func<ProRechargeMonthlyCard, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CC
        {
            internal System.Delegate act;

            internal int <>m__0(ProRechargeMonthlyCard x, ProRechargeMonthlyCard y) => 
                ((Func<ProRechargeMonthlyCard, ProRechargeMonthlyCard, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CD
        {
            internal System.Delegate act;

            internal bool <>m__0(ProRechargeItem obj) => 
                ((Func<ProRechargeItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CE
        {
            internal System.Delegate act;

            internal int <>m__0(ProRechargeItem x, ProRechargeItem y) => 
                ((Func<ProRechargeItem, ProRechargeItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2CF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProDailyLoginRewardInfo obj) => 
                ((Func<ProDailyLoginRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D
        {
            internal System.Delegate act;

            internal bool <>m__0(GameObject obj) => 
                ((Func<GameObject, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D0
        {
            internal System.Delegate act;

            internal int <>m__0(ProDailyLoginRewardInfo x, ProDailyLoginRewardInfo y) => 
                ((Func<ProDailyLoginRewardInfo, ProDailyLoginRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStoreItemTransformInfo obj) => 
                ((Func<ProStoreItemTransformInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D2
        {
            internal System.Delegate act;

            internal int <>m__0(ProStoreItemTransformInfo x, ProStoreItemTransformInfo y) => 
                ((Func<ProStoreItemTransformInfo, ProStoreItemTransformInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D3
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipSetAmmoInfo obj) => 
                ((Func<ShipSetAmmoInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D4
        {
            internal System.Delegate act;

            internal int <>m__0(ShipSetAmmoInfo x, ShipSetAmmoInfo y) => 
                ((Func<ShipSetAmmoInfo, ShipSetAmmoInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipStoreItemUpdateInfo obj) => 
                ((Func<ProShipStoreItemUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D6
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipStoreItemUpdateInfo x, ProShipStoreItemUpdateInfo y) => 
                ((Func<ProShipStoreItemUpdateInfo, ProShipStoreItemUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D7
        {
            internal System.Delegate act;

            internal bool <>m__0(GMServerAccountInfoAck.AccountUserInfo obj) => 
                ((Func<GMServerAccountInfoAck.AccountUserInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D8
        {
            internal System.Delegate act;

            internal int <>m__0(GMServerAccountInfoAck.AccountUserInfo x, GMServerAccountInfoAck.AccountUserInfo y) => 
                ((Func<GMServerAccountInfoAck.AccountUserInfo, GMServerAccountInfoAck.AccountUserInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2D9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHangerInfo obj) => 
                ((Func<ProHangerInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DA
        {
            internal System.Delegate act;

            internal int <>m__0(ProHangerInfo x, ProHangerInfo y) => 
                ((Func<ProHangerInfo, ProHangerInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProAllianceBasicInfo obj) => 
                ((Func<ProAllianceBasicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DC
        {
            internal System.Delegate act;

            internal int <>m__0(ProAllianceBasicInfo x, ProAllianceBasicInfo y) => 
                ((Func<ProAllianceBasicInfo, ProAllianceBasicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DD
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildAllianceInviteInfo obj) => 
                ((Func<ProGuildAllianceInviteInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DE
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildAllianceInviteInfo x, ProGuildAllianceInviteInfo y) => 
                ((Func<ProGuildAllianceInviteInfo, ProGuildAllianceInviteInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2DF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattleSimpleReportInfo obj) => 
                ((Func<ProGuildBattleSimpleReportInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E
        {
            internal System.Delegate act;

            internal void <>m__0(GameObject go)
            {
                ((Action<GameObject>) this.act)(go);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E0
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattleSimpleReportInfo x, ProGuildBattleSimpleReportInfo y) => 
                ((Func<ProGuildBattleSimpleReportInfo, ProGuildBattleSimpleReportInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildAntiTeleportInfo obj) => 
                ((Func<ProGuildAntiTeleportInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E2
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildAntiTeleportInfo x, ProGuildAntiTeleportInfo y) => 
                ((Func<ProGuildAntiTeleportInfo, ProGuildAntiTeleportInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E3
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildTeleportTunnelInfo obj) => 
                ((Func<ProGuildTeleportTunnelInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E4
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildTeleportTunnelInfo x, ProGuildTeleportTunnelInfo y) => 
                ((Func<ProGuildTeleportTunnelInfo, ProGuildTeleportTunnelInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildSolarSystemInfo obj) => 
                ((Func<ProGuildSolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E6
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildSolarSystemInfo x, ProGuildSolarSystemInfo y) => 
                ((Func<ProGuildSolarSystemInfo, ProGuildSolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildStoreItemInfo obj) => 
                ((Func<ProGuildStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E8
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildStoreItemInfo x, ProGuildStoreItemInfo y) => 
                ((Func<ProGuildStoreItemInfo, ProGuildStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2E9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBuildingBattleClientSyncInfo obj) => 
                ((Func<ProGuildBuildingBattleClientSyncInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2EA
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBuildingBattleClientSyncInfo x, ProGuildBuildingBattleClientSyncInfo y) => 
                ((Func<ProGuildBuildingBattleClientSyncInfo, ProGuildBuildingBattleClientSyncInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2EB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildSentryPlayerInfo obj) => 
                ((Func<ProGuildSentryPlayerInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2EC
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildSentryPlayerInfo x, ProGuildSentryPlayerInfo y) => 
                ((Func<ProGuildSentryPlayerInfo, ProGuildSentryPlayerInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2ED
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildSentryInterestSceneShipTypeInfo obj) => 
                ((Func<ProGuildSentryInterestSceneShipTypeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2EE
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildSentryInterestSceneShipTypeInfo x, ProGuildSentryInterestSceneShipTypeInfo y) => 
                ((Func<ProGuildSentryInterestSceneShipTypeInfo, ProGuildSentryInterestSceneShipTypeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2EF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLossCompensation obj) => 
                ((Func<ProLossCompensation, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F
        {
            internal System.Delegate act;

            internal int <>m__0(GameObject x, GameObject y) => 
                ((Func<GameObject, GameObject, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F0
        {
            internal System.Delegate act;

            internal int <>m__0(ProLossCompensation x, ProLossCompensation y) => 
                ((Func<ProLossCompensation, ProLossCompensation, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F1
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFlagShipHangarVersionInfo obj) => 
                ((Func<ProGuildFlagShipHangarVersionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F2
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFlagShipHangarVersionInfo x, ProGuildFlagShipHangarVersionInfo y) => 
                ((Func<ProGuildFlagShipHangarVersionInfo, ProGuildFlagShipHangarVersionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F3
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFlagShipHangarDetailInfo obj) => 
                ((Func<ProGuildFlagShipHangarDetailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F4
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFlagShipHangarDetailInfo x, ProGuildFlagShipHangarDetailInfo y) => 
                ((Func<ProGuildFlagShipHangarDetailInfo, ProGuildFlagShipHangarDetailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F5
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFlagShipHangarShipSupplementFuelInfo obj) => 
                ((Func<ProGuildFlagShipHangarShipSupplementFuelInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F6
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFlagShipHangarShipSupplementFuelInfo x, ProGuildFlagShipHangarShipSupplementFuelInfo y) => 
                ((Func<ProGuildFlagShipHangarShipSupplementFuelInfo, ProGuildFlagShipHangarShipSupplementFuelInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFlagShipOptLogInfo obj) => 
                ((Func<ProGuildFlagShipOptLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F8
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFlagShipOptLogInfo x, ProGuildFlagShipOptLogInfo y) => 
                ((Func<ProGuildFlagShipOptLogInfo, ProGuildFlagShipOptLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2F9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildFleetSimpleInfo obj) => 
                ((Func<ProGuildFleetSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FA
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildFleetSimpleInfo x, ProGuildFleetSimpleInfo y) => 
                ((Func<ProGuildFleetSimpleInfo, ProGuildFleetSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSovereignLostReport obj) => 
                ((Func<ProSovereignLostReport, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FC
        {
            internal System.Delegate act;

            internal int <>m__0(ProSovereignLostReport x, ProSovereignLostReport y) => 
                ((Func<ProSovereignLostReport, ProSovereignLostReport, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FD
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildProductionLineInfo obj) => 
                ((Func<ProGuildProductionLineInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FE
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildProductionLineInfo x, ProGuildProductionLineInfo y) => 
                ((Func<ProGuildProductionLineInfo, ProGuildProductionLineInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey2FF
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildMomentsInfo obj) => 
                ((Func<ProGuildMomentsInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3
        {
            internal System.Delegate act;

            internal void <>m__0(float arg0)
            {
                ((Action<float>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30
        {
            internal System.Delegate act;

            internal bool <>m__0(TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup obj) => 
                ((Func<TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey300
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildMomentsInfo x, ProGuildMomentsInfo y) => 
                ((Func<ProGuildMomentsInfo, ProGuildMomentsInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey301
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildJoinApplyViewInfo obj) => 
                ((Func<ProGuildJoinApplyViewInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey302
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildJoinApplyViewInfo x, ProGuildJoinApplyViewInfo y) => 
                ((Func<ProGuildJoinApplyViewInfo, ProGuildJoinApplyViewInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey303
        {
            internal System.Delegate act;

            internal bool <>m__0(ProIFFStatePairInfo obj) => 
                ((Func<ProIFFStatePairInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey304
        {
            internal System.Delegate act;

            internal int <>m__0(ProIFFStatePairInfo x, ProIFFStatePairInfo y) => 
                ((Func<ProIFFStatePairInfo, ProIFFStatePairInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey305
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildStaffingLogInfo obj) => 
                ((Func<ProGuildStaffingLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey306
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildStaffingLogInfo x, ProGuildStaffingLogInfo y) => 
                ((Func<ProGuildStaffingLogInfo, ProGuildStaffingLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey307
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildMessageInfo obj) => 
                ((Func<ProGuildMessageInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey308
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildMessageInfo x, ProGuildMessageInfo y) => 
                ((Func<ProGuildMessageInfo, ProGuildMessageInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey309
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildActionInfo obj) => 
                ((Func<ProGuildActionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30A
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildActionInfo x, ProGuildActionInfo y) => 
                ((Func<ProGuildActionInfo, ProGuildActionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildStoreItemListInfo obj) => 
                ((Func<ProGuildStoreItemListInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30C
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildStoreItemListInfo x, ProGuildStoreItemListInfo y) => 
                ((Func<ProGuildStoreItemListInfo, ProGuildStoreItemListInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildStoreItemUpdateInfo obj) => 
                ((Func<ProGuildStoreItemUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30E
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildStoreItemUpdateInfo x, ProGuildStoreItemUpdateInfo y) => 
                ((Func<ProGuildStoreItemUpdateInfo, ProGuildStoreItemUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey30F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildDonateRankingItemInfo obj) => 
                ((Func<ProGuildDonateRankingItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31
        {
            internal System.Delegate act;

            internal int <>m__0(TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup x, TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup y) => 
                ((Func<TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup, TimelinePredefineObjectTypeSetting.TimelinePredefineObjectTypeGroup, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey310
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildDonateRankingItemInfo x, ProGuildDonateRankingItemInfo y) => 
                ((Func<ProGuildDonateRankingItemInfo, ProGuildDonateRankingItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey311
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildCurrencyLogInfo obj) => 
                ((Func<ProGuildCurrencyLogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey312
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildCurrencyLogInfo x, ProGuildCurrencyLogInfo y) => 
                ((Func<ProGuildCurrencyLogInfo, ProGuildCurrencyLogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey313
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildPurchaseOrderVersionInfo obj) => 
                ((Func<ProGuildPurchaseOrderVersionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey314
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildPurchaseOrderVersionInfo x, ProGuildPurchaseOrderVersionInfo y) => 
                ((Func<ProGuildPurchaseOrderVersionInfo, ProGuildPurchaseOrderVersionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey315
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildPurchaseOrderListItemInfo obj) => 
                ((Func<ProGuildPurchaseOrderListItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey316
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildPurchaseOrderListItemInfo x, ProGuildPurchaseOrderListItemInfo y) => 
                ((Func<ProGuildPurchaseOrderListItemInfo, ProGuildPurchaseOrderListItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey317
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSolarSystemGuildBattleStatusInfo obj) => 
                ((Func<ProSolarSystemGuildBattleStatusInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey318
        {
            internal System.Delegate act;

            internal int <>m__0(ProSolarSystemGuildBattleStatusInfo x, ProSolarSystemGuildBattleStatusInfo y) => 
                ((Func<ProSolarSystemGuildBattleStatusInfo, ProSolarSystemGuildBattleStatusInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey319
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBasicInfoReq obj) => 
                ((Func<GuildSolarSystemBasicInfoReq, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31A
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBasicInfoReq x, GuildSolarSystemBasicInfoReq y) => 
                ((Func<GuildSolarSystemBasicInfoReq, GuildSolarSystemBasicInfoReq, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31B
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBuildingInfoReq obj) => 
                ((Func<GuildSolarSystemBuildingInfoReq, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31C
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBuildingInfoReq x, GuildSolarSystemBuildingInfoReq y) => 
                ((Func<GuildSolarSystemBuildingInfoReq, GuildSolarSystemBuildingInfoReq, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31D
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBattleInfoReq obj) => 
                ((Func<GuildSolarSystemBattleInfoReq, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31E
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBattleInfoReq x, GuildSolarSystemBattleInfoReq y) => 
                ((Func<GuildSolarSystemBattleInfoReq, GuildSolarSystemBattleInfoReq, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey31F
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBasicInfoAck obj) => 
                ((Func<GuildSolarSystemBasicInfoAck, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32
        {
            internal System.Delegate act;

            internal bool <>m__0(TimelineAutoBindGenerator.TimelinePreprocessBindNode obj) => 
                ((Func<TimelineAutoBindGenerator.TimelinePreprocessBindNode, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey320
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBasicInfoAck x, GuildSolarSystemBasicInfoAck y) => 
                ((Func<GuildSolarSystemBasicInfoAck, GuildSolarSystemBasicInfoAck, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey321
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBuildingInfoAck obj) => 
                ((Func<GuildSolarSystemBuildingInfoAck, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey322
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBuildingInfoAck x, GuildSolarSystemBuildingInfoAck y) => 
                ((Func<GuildSolarSystemBuildingInfoAck, GuildSolarSystemBuildingInfoAck, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey323
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBuildingExInfo obj) => 
                ((Func<ProGuildBuildingExInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey324
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBuildingExInfo x, ProGuildBuildingExInfo y) => 
                ((Func<ProGuildBuildingExInfo, ProGuildBuildingExInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey325
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSolarSystemBattleInfoAck obj) => 
                ((Func<GuildSolarSystemBattleInfoAck, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey326
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSolarSystemBattleInfoAck x, GuildSolarSystemBattleInfoAck y) => 
                ((Func<GuildSolarSystemBattleInfoAck, GuildSolarSystemBattleInfoAck, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey327
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBattleClientSyncInfo obj) => 
                ((Func<ProGuildBattleClientSyncInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey328
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBattleClientSyncInfo x, ProGuildBattleClientSyncInfo y) => 
                ((Func<ProGuildBattleClientSyncInfo, ProGuildBattleClientSyncInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey329
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildSentrySceneInfo obj) => 
                ((Func<ProGuildSentrySceneInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32A
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildSentrySceneInfo x, ProGuildSentrySceneInfo y) => 
                ((Func<ProGuildSentrySceneInfo, ProGuildSentrySceneInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBenefitsInformationPoint obj) => 
                ((Func<ProGuildBenefitsInformationPoint, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32C
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBenefitsInformationPoint x, ProGuildBenefitsInformationPoint y) => 
                ((Func<ProGuildBenefitsInformationPoint, ProGuildBenefitsInformationPoint, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildBenefitsCommon obj) => 
                ((Func<ProGuildBenefitsCommon, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32E
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildBenefitsCommon x, ProGuildBenefitsCommon y) => 
                ((Func<ProGuildBenefitsCommon, ProGuildBenefitsCommon, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey32F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProUInt64IdValueInfo obj) => 
                ((Func<ProUInt64IdValueInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33
        {
            internal System.Delegate act;

            internal int <>m__0(TimelineAutoBindGenerator.TimelinePreprocessBindNode x, TimelineAutoBindGenerator.TimelinePreprocessBindNode y) => 
                ((Func<TimelineAutoBindGenerator.TimelinePreprocessBindNode, TimelineAutoBindGenerator.TimelinePreprocessBindNode, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey330
        {
            internal System.Delegate act;

            internal int <>m__0(ProUInt64IdValueInfo x, ProUInt64IdValueInfo y) => 
                ((Func<ProUInt64IdValueInfo, ProUInt64IdValueInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey331
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildTradePurchaseInfo obj) => 
                ((Func<ProGuildTradePurchaseInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey332
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildTradePurchaseInfo x, ProGuildTradePurchaseInfo y) => 
                ((Func<ProGuildTradePurchaseInfo, ProGuildTradePurchaseInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey333
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildTradePortExtraInfo obj) => 
                ((Func<ProGuildTradePortExtraInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey334
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildTradePortExtraInfo x, ProGuildTradePortExtraInfo y) => 
                ((Func<ProGuildTradePortExtraInfo, ProGuildTradePortExtraInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey335
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGuildTradeTransportInfo obj) => 
                ((Func<ProGuildTradeTransportInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey336
        {
            internal System.Delegate act;

            internal int <>m__0(ProGuildTradeTransportInfo x, ProGuildTradeTransportInfo y) => 
                ((Func<ProGuildTradeTransportInfo, ProGuildTradeTransportInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey337
        {
            internal System.Delegate act;

            internal bool <>m__0(ProBufInfo obj) => 
                ((Func<ProBufInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey338
        {
            internal System.Delegate act;

            internal int <>m__0(ProBufInfo x, ProBufInfo y) => 
                ((Func<ProBufInfo, ProBufInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey339
        {
            internal System.Delegate act;

            internal bool <>m__0(ProHateTargetInfo obj) => 
                ((Func<ProHateTargetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33A
        {
            internal System.Delegate act;

            internal int <>m__0(ProHateTargetInfo x, ProHateTargetInfo y) => 
                ((Func<ProHateTargetInfo, ProHateTargetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLBSyncEvent obj) => 
                ((Func<ProLBSyncEvent, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33C
        {
            internal System.Delegate act;

            internal int <>m__0(ProLBSyncEvent x, ProLBSyncEvent y) => 
                ((Func<ProLBSyncEvent, ProLBSyncEvent, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLBProcessSingleUnitLaunchInfo obj) => 
                ((Func<ProLBProcessSingleUnitLaunchInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33E
        {
            internal System.Delegate act;

            internal int <>m__0(ProLBProcessSingleUnitLaunchInfo x, ProLBProcessSingleUnitLaunchInfo y) => 
                ((Func<ProLBProcessSingleUnitLaunchInfo, ProLBProcessSingleUnitLaunchInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey33F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLBProcessSingleSuperMissileUnitLaunchInfo obj) => 
                ((Func<ProLBProcessSingleSuperMissileUnitLaunchInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34
        {
            internal System.Delegate act;

            internal bool <>m__0(TimelineAutoBindGenerator.TimelineBindNode obj) => 
                ((Func<TimelineAutoBindGenerator.TimelineBindNode, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey340
        {
            internal System.Delegate act;

            internal int <>m__0(ProLBProcessSingleSuperMissileUnitLaunchInfo x, ProLBProcessSingleSuperMissileUnitLaunchInfo y) => 
                ((Func<ProLBProcessSingleSuperMissileUnitLaunchInfo, ProLBProcessSingleSuperMissileUnitLaunchInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey341
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLBSpaceProcessDroneLaunchInfo obj) => 
                ((Func<ProLBSpaceProcessDroneLaunchInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey342
        {
            internal System.Delegate act;

            internal int <>m__0(ProLBSpaceProcessDroneLaunchInfo x, ProLBSpaceProcessDroneLaunchInfo y) => 
                ((Func<ProLBSpaceProcessDroneLaunchInfo, ProLBSpaceProcessDroneLaunchInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey343
        {
            internal System.Delegate act;

            internal bool <>m__0(ProLBSyncEventOnHitInfo obj) => 
                ((Func<ProLBSyncEventOnHitInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey344
        {
            internal System.Delegate act;

            internal int <>m__0(ProLBSyncEventOnHitInfo x, ProLBSyncEventOnHitInfo y) => 
                ((Func<ProLBSyncEventOnHitInfo, ProLBSyncEventOnHitInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey345
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSpaceObjectEnterInfo obj) => 
                ((Func<ProSpaceObjectEnterInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey346
        {
            internal System.Delegate act;

            internal int <>m__0(ProSpaceObjectEnterInfo x, ProSpaceObjectEnterInfo y) => 
                ((Func<ProSpaceObjectEnterInfo, ProSpaceObjectEnterInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey347
        {
            internal System.Delegate act;

            internal bool <>m__0(ProShipSlotAmmoInfo obj) => 
                ((Func<ProShipSlotAmmoInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey348
        {
            internal System.Delegate act;

            internal int <>m__0(ProShipSlotAmmoInfo x, ProShipSlotAmmoInfo y) => 
                ((Func<ProShipSlotAmmoInfo, ProShipSlotAmmoInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey349
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSpaceObjectMoveStateSnapshot obj) => 
                ((Func<ProSpaceObjectMoveStateSnapshot, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34A
        {
            internal System.Delegate act;

            internal int <>m__0(ProSpaceObjectMoveStateSnapshot x, ProSpaceObjectMoveStateSnapshot y) => 
                ((Func<ProSpaceObjectMoveStateSnapshot, ProSpaceObjectMoveStateSnapshot, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSynEventList4ObjInfo obj) => 
                ((Func<ProSynEventList4ObjInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34C
        {
            internal System.Delegate act;

            internal int <>m__0(ProSynEventList4ObjInfo x, ProSynEventList4ObjInfo y) => 
                ((Func<ProSynEventList4ObjInfo, ProSynEventList4ObjInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSceneCameraNameObjIdPair obj) => 
                ((Func<ProSceneCameraNameObjIdPair, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34E
        {
            internal System.Delegate act;

            internal int <>m__0(ProSceneCameraNameObjIdPair x, ProSceneCameraNameObjIdPair y) => 
                ((Func<ProSceneCameraNameObjIdPair, ProSceneCameraNameObjIdPair, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey34F
        {
            internal System.Delegate act;

            internal bool <>m__0(InSpaceNpcTalkerChatNtf obj) => 
                ((Func<InSpaceNpcTalkerChatNtf, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35
        {
            internal System.Delegate act;

            internal int <>m__0(TimelineAutoBindGenerator.TimelineBindNode x, TimelineAutoBindGenerator.TimelineBindNode y) => 
                ((Func<TimelineAutoBindGenerator.TimelineBindNode, TimelineAutoBindGenerator.TimelineBindNode, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey350
        {
            internal System.Delegate act;

            internal int <>m__0(InSpaceNpcTalkerChatNtf x, InSpaceNpcTalkerChatNtf y) => 
                ((Func<InSpaceNpcTalkerChatNtf, InSpaceNpcTalkerChatNtf, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey351
        {
            internal System.Delegate act;

            internal bool <>m__0(ProGlobalSceneInfo obj) => 
                ((Func<ProGlobalSceneInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey352
        {
            internal System.Delegate act;

            internal int <>m__0(ProGlobalSceneInfo x, ProGlobalSceneInfo y) => 
                ((Func<ProGlobalSceneInfo, ProGlobalSceneInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey353
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSceneGuildBuildingExtraDataInfo obj) => 
                ((Func<ProSceneGuildBuildingExtraDataInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey354
        {
            internal System.Delegate act;

            internal int <>m__0(ProSceneGuildBuildingExtraDataInfo x, ProSceneGuildBuildingExtraDataInfo y) => 
                ((Func<ProSceneGuildBuildingExtraDataInfo, ProSceneGuildBuildingExtraDataInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey355
        {
            internal System.Delegate act;

            internal bool <>m__0(ProStationDynamicNpcTalkerStateInfo obj) => 
                ((Func<ProStationDynamicNpcTalkerStateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey356
        {
            internal System.Delegate act;

            internal int <>m__0(ProStationDynamicNpcTalkerStateInfo x, ProStationDynamicNpcTalkerStateInfo y) => 
                ((Func<ProStationDynamicNpcTalkerStateInfo, ProStationDynamicNpcTalkerStateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey357
        {
            internal System.Delegate act;

            internal bool <>m__0(ProQuestRewardInfo obj) => 
                ((Func<ProQuestRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey358
        {
            internal System.Delegate act;

            internal int <>m__0(ProQuestRewardInfo x, ProQuestRewardInfo y) => 
                ((Func<ProQuestRewardInfo, ProQuestRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey359
        {
            internal System.Delegate act;

            internal bool <>m__0(ProTeamMemberInfo obj) => 
                ((Func<ProTeamMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35A
        {
            internal System.Delegate act;

            internal int <>m__0(ProTeamMemberInfo x, ProTeamMemberInfo y) => 
                ((Func<ProTeamMemberInfo, ProTeamMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35B
        {
            internal System.Delegate act;

            internal bool <>m__0(ProInfectInfo obj) => 
                ((Func<ProInfectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35C
        {
            internal System.Delegate act;

            internal int <>m__0(ProInfectInfo x, ProInfectInfo y) => 
                ((Func<ProInfectInfo, ProInfectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35D
        {
            internal System.Delegate act;

            internal bool <>m__0(ProSolarSystemPoolCountInfo obj) => 
                ((Func<ProSolarSystemPoolCountInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35E
        {
            internal System.Delegate act;

            internal int <>m__0(ProSolarSystemPoolCountInfo x, ProSolarSystemPoolCountInfo y) => 
                ((Func<ProSolarSystemPoolCountInfo, ProSolarSystemPoolCountInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey35F
        {
            internal System.Delegate act;

            internal bool <>m__0(ProTradeListItemInfo obj) => 
                ((Func<ProTradeListItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36
        {
            internal System.Delegate act;

            internal bool <>m__0(TweenMain obj) => 
                ((Func<TweenMain, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey360
        {
            internal System.Delegate act;

            internal int <>m__0(ProTradeListItemInfo x, ProTradeListItemInfo y) => 
                ((Func<ProTradeListItemInfo, ProTradeListItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey361
        {
            internal System.Delegate act;

            internal bool <>m__0(ProTradeNpcShopItemInfo obj) => 
                ((Func<ProTradeNpcShopItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey362
        {
            internal System.Delegate act;

            internal int <>m__0(ProTradeNpcShopItemInfo x, ProTradeNpcShopItemInfo y) => 
                ((Func<ProTradeNpcShopItemInfo, ProTradeNpcShopItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey363
        {
            internal System.Delegate act;

            internal bool <>m__0(PlayerActivityInfo obj) => 
                ((Func<PlayerActivityInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey364
        {
            internal System.Delegate act;

            internal int <>m__0(PlayerActivityInfo x, PlayerActivityInfo y) => 
                ((Func<PlayerActivityInfo, PlayerActivityInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey365
        {
            internal System.Delegate act;

            internal bool <>m__0(AuctionItemDetailInfo obj) => 
                ((Func<AuctionItemDetailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey366
        {
            internal System.Delegate act;

            internal int <>m__0(AuctionItemDetailInfo x, AuctionItemDetailInfo y) => 
                ((Func<AuctionItemDetailInfo, AuctionItemDetailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey367
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<int, int> obj) => 
                ((Func<KeyValuePair<int, int>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey368
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<int, int> x, KeyValuePair<int, int> y) => 
                ((Func<KeyValuePair<int, int>, KeyValuePair<int, int>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey369
        {
            internal System.Delegate act;

            internal bool <>m__0(LBPassiveSkill obj) => 
                ((Func<LBPassiveSkill, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36A
        {
            internal System.Delegate act;

            internal int <>m__0(LBPassiveSkill x, LBPassiveSkill y) => 
                ((Func<LBPassiveSkill, LBPassiveSkill, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36B
        {
            internal System.Delegate act;

            internal bool <>m__0(LBCharChipSlot obj) => 
                ((Func<LBCharChipSlot, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36C
        {
            internal System.Delegate act;

            internal int <>m__0(LBCharChipSlot x, LBCharChipSlot y) => 
                ((Func<LBCharChipSlot, LBCharChipSlot, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36D
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<CurrencyType, long> obj) => 
                ((Func<KeyValuePair<CurrencyType, long>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36E
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<CurrencyType, long> x, KeyValuePair<CurrencyType, long> y) => 
                ((Func<KeyValuePair<CurrencyType, long>, KeyValuePair<CurrencyType, long>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey36F
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStaticHiredCaptain obj) => 
                ((Func<LBStaticHiredCaptain, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37
        {
            internal System.Delegate act;

            internal int <>m__0(TweenMain x, TweenMain y) => 
                ((Func<TweenMain, TweenMain, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey370
        {
            internal System.Delegate act;

            internal int <>m__0(LBStaticHiredCaptain x, LBStaticHiredCaptain y) => 
                ((Func<LBStaticHiredCaptain, LBStaticHiredCaptain, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey371
        {
            internal System.Delegate act;

            internal bool <>m__0(LBNpcCaptainFeats obj) => 
                ((Func<LBNpcCaptainFeats, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey372
        {
            internal System.Delegate act;

            internal int <>m__0(LBNpcCaptainFeats x, LBNpcCaptainFeats y) => 
                ((Func<LBNpcCaptainFeats, LBNpcCaptainFeats, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey373
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStaticHiredCaptain.ShipInfo obj) => 
                ((Func<LBStaticHiredCaptain.ShipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey374
        {
            internal System.Delegate act;

            internal int <>m__0(LBStaticHiredCaptain.ShipInfo x, LBStaticHiredCaptain.ShipInfo y) => 
                ((Func<LBStaticHiredCaptain.ShipInfo, LBStaticHiredCaptain.ShipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey375
        {
            internal System.Delegate act;

            internal bool <>m__0(LBDelegateMission obj) => 
                ((Func<LBDelegateMission, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey376
        {
            internal System.Delegate act;

            internal int <>m__0(LBDelegateMission x, LBDelegateMission y) => 
                ((Func<LBDelegateMission, LBDelegateMission, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey377
        {
            internal System.Delegate act;

            internal bool <>m__0(DelegateMissionInfo obj) => 
                ((Func<DelegateMissionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey378
        {
            internal System.Delegate act;

            internal int <>m__0(DelegateMissionInfo x, DelegateMissionInfo y) => 
                ((Func<DelegateMissionInfo, DelegateMissionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey379
        {
            internal System.Delegate act;

            internal bool <>m__0(LBPVPInvadeRescue obj) => 
                ((Func<LBPVPInvadeRescue, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37A
        {
            internal System.Delegate act;

            internal int <>m__0(LBPVPInvadeRescue x, LBPVPInvadeRescue y) => 
                ((Func<LBPVPInvadeRescue, LBPVPInvadeRescue, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37B
        {
            internal System.Delegate act;

            internal bool <>m__0(LBDrivingLicense obj) => 
                ((Func<LBDrivingLicense, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37C
        {
            internal System.Delegate act;

            internal int <>m__0(LBDrivingLicense x, LBDrivingLicense y) => 
                ((Func<LBDrivingLicense, LBDrivingLicense, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37D
        {
            internal System.Delegate act;

            internal bool <>m__0(IGuildFlagShipHangarDataContainer obj) => 
                ((Func<IGuildFlagShipHangarDataContainer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37E
        {
            internal System.Delegate act;

            internal int <>m__0(IGuildFlagShipHangarDataContainer x, IGuildFlagShipHangarDataContainer y) => 
                ((Func<IGuildFlagShipHangarDataContainer, IGuildFlagShipHangarDataContainer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey37F
        {
            internal System.Delegate act;

            internal bool <>m__0(ILBStaticFlagShip obj) => 
                ((Func<ILBStaticFlagShip, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38
        {
            internal System.Delegate act;

            internal bool <>m__0(UIStateGradientColorDesc obj) => 
                ((Func<UIStateGradientColorDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey380
        {
            internal System.Delegate act;

            internal int <>m__0(ILBStaticFlagShip x, ILBStaticFlagShip y) => 
                ((Func<ILBStaticFlagShip, ILBStaticFlagShip, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey381
        {
            internal System.Delegate act;

            internal bool <>m__0(IStaticFlagShipDataContainer obj) => 
                ((Func<IStaticFlagShipDataContainer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey382
        {
            internal System.Delegate act;

            internal int <>m__0(IStaticFlagShipDataContainer x, IStaticFlagShipDataContainer y) => 
                ((Func<IStaticFlagShipDataContainer, IStaticFlagShipDataContainer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey383
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMemberBase obj) => 
                ((Func<GuildMemberBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey384
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMemberBase x, GuildMemberBase y) => 
                ((Func<GuildMemberBase, GuildMemberBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey385
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTeleportTunnelEffectInfo obj) => 
                ((Func<GuildTeleportTunnelEffectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey386
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTeleportTunnelEffectInfo x, GuildTeleportTunnelEffectInfo y) => 
                ((Func<GuildTeleportTunnelEffectInfo, GuildTeleportTunnelEffectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey387
        {
            internal System.Delegate act;

            internal string <>m__0(Match match) => 
                ((Func<Match, string>) this.act)(match);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey388
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSentryInterestScene obj) => 
                ((Func<GuildSentryInterestScene, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey389
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSentryInterestScene x, GuildSentryInterestScene y) => 
                ((Func<GuildSentryInterestScene, GuildSentryInterestScene, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38A
        {
            internal System.Delegate act;

            internal bool <>m__0(ILBStaticPlayerShip obj) => 
                ((Func<ILBStaticPlayerShip, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38B
        {
            internal System.Delegate act;

            internal int <>m__0(ILBStaticPlayerShip x, ILBStaticPlayerShip y) => 
                ((Func<ILBStaticPlayerShip, ILBStaticPlayerShip, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38C
        {
            internal System.Delegate act;

            internal bool <>m__0(KillRecordInfo obj) => 
                ((Func<KillRecordInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38D
        {
            internal System.Delegate act;

            internal int <>m__0(KillRecordInfo x, KillRecordInfo y) => 
                ((Func<KillRecordInfo, KillRecordInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38E
        {
            internal System.Delegate act;

            internal bool <>m__0(DailyLoginRewardInfo obj) => 
                ((Func<DailyLoginRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey38F
        {
            internal System.Delegate act;

            internal int <>m__0(DailyLoginRewardInfo x, DailyLoginRewardInfo y) => 
                ((Func<DailyLoginRewardInfo, DailyLoginRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39
        {
            internal System.Delegate act;

            internal int <>m__0(UIStateGradientColorDesc x, UIStateGradientColorDesc y) => 
                ((Func<UIStateGradientColorDesc, UIStateGradientColorDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey390
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<int, long> obj) => 
                ((Func<KeyValuePair<int, long>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey391
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<int, long> x, KeyValuePair<int, long> y) => 
                ((Func<KeyValuePair<int, long>, KeyValuePair<int, long>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey392
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataNpcShopItemInfo obj) => 
                ((Func<ConfigDataNpcShopItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey393
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataNpcShopItemInfo x, ConfigDataNpcShopItemInfo y) => 
                ((Func<ConfigDataNpcShopItemInfo, ConfigDataNpcShopItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey394
        {
            internal System.Delegate act;

            internal bool <>m__0(LBProductionLine obj) => 
                ((Func<LBProductionLine, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey395
        {
            internal System.Delegate act;

            internal int <>m__0(LBProductionLine x, LBProductionLine y) => 
                ((Func<LBProductionLine, LBProductionLine, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey396
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestWaitForAcceptInfo obj) => 
                ((Func<QuestWaitForAcceptInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey397
        {
            internal System.Delegate act;

            internal int <>m__0(QuestWaitForAcceptInfo x, QuestWaitForAcceptInfo y) => 
                ((Func<QuestWaitForAcceptInfo, QuestWaitForAcceptInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey398
        {
            internal System.Delegate act;

            internal bool <>m__0(LBProcessingQuestBase obj) => 
                ((Func<LBProcessingQuestBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey399
        {
            internal System.Delegate act;

            internal int <>m__0(LBProcessingQuestBase x, LBProcessingQuestBase y) => 
                ((Func<LBProcessingQuestBase, LBProcessingQuestBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39A
        {
            internal System.Delegate act;

            internal bool <>m__0(LBRechargeGiftPackage obj) => 
                ((Func<LBRechargeGiftPackage, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39B
        {
            internal System.Delegate act;

            internal int <>m__0(LBRechargeGiftPackage x, LBRechargeGiftPackage y) => 
                ((Func<LBRechargeGiftPackage, LBRechargeGiftPackage, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39C
        {
            internal System.Delegate act;

            internal bool <>m__0(LBRechargeItem obj) => 
                ((Func<LBRechargeItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39D
        {
            internal System.Delegate act;

            internal int <>m__0(LBRechargeItem x, LBRechargeItem y) => 
                ((Func<LBRechargeItem, LBRechargeItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39E
        {
            internal System.Delegate act;

            internal bool <>m__0(LBRechargeMonthlyCard obj) => 
                ((Func<LBRechargeMonthlyCard, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey39F
        {
            internal System.Delegate act;

            internal int <>m__0(LBRechargeMonthlyCard x, LBRechargeMonthlyCard y) => 
                ((Func<LBRechargeMonthlyCard, LBRechargeMonthlyCard, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A
        {
            internal System.Delegate act;

            internal bool <>m__0(Vector3 obj) => 
                ((Func<Vector3, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A0
        {
            internal System.Delegate act;

            internal bool <>m__0(RechargeOrderInfo obj) => 
                ((Func<RechargeOrderInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A1
        {
            internal System.Delegate act;

            internal int <>m__0(RechargeOrderInfo x, RechargeOrderInfo y) => 
                ((Func<RechargeOrderInfo, RechargeOrderInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A2
        {
            internal System.Delegate act;

            internal bool <>m__0(LBBulletDamageInfo obj) => 
                ((Func<LBBulletDamageInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A3
        {
            internal System.Delegate act;

            internal int <>m__0(LBBulletDamageInfo x, LBBulletDamageInfo y) => 
                ((Func<LBBulletDamageInfo, LBBulletDamageInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A4
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSpaceProcessDroneFighterLaunch obj) => 
                ((Func<LBSpaceProcessDroneFighterLaunch, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A5
        {
            internal System.Delegate act;

            internal int <>m__0(LBSpaceProcessDroneFighterLaunch x, LBSpaceProcessDroneFighterLaunch y) => 
                ((Func<LBSpaceProcessDroneFighterLaunch, LBSpaceProcessDroneFighterLaunch, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A6
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance obj) => 
                ((Func<LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A7
        {
            internal System.Delegate act;

            internal int <>m__0(LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance x, LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance y) => 
                ((Func<LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance, LBSyncEventDroneDefenderPreCalcAttackMissile.AttackChance, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A8
        {
            internal System.Delegate act;

            internal bool <>m__0(PVPSignalInfo obj) => 
                ((Func<PVPSignalInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3A9
        {
            internal System.Delegate act;

            internal int <>m__0(PVPSignalInfo x, PVPSignalInfo y) => 
                ((Func<PVPSignalInfo, PVPSignalInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AA
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSignalDelegateBase obj) => 
                ((Func<LBSignalDelegateBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AB
        {
            internal System.Delegate act;

            internal int <>m__0(LBSignalDelegateBase x, LBSignalDelegateBase y) => 
                ((Func<LBSignalDelegateBase, LBSignalDelegateBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AC
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSignalManualBase obj) => 
                ((Func<LBSignalManualBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AD
        {
            internal System.Delegate act;

            internal int <>m__0(LBSignalManualBase x, LBSignalManualBase y) => 
                ((Func<LBSignalManualBase, LBSignalManualBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AE
        {
            internal System.Delegate act;

            internal bool <>m__0(LBPVPSignal obj) => 
                ((Func<LBPVPSignal, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3AF
        {
            internal System.Delegate act;

            internal int <>m__0(LBPVPSignal x, LBPVPSignal y) => 
                ((Func<LBPVPSignal, LBPVPSignal, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B
        {
            internal System.Delegate act;

            internal int <>m__0(Vector3 x, Vector3 y) => 
                ((Func<Vector3, Vector3, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B0
        {
            internal System.Delegate act;

            internal bool <>m__0(LBNpcTalkerBase obj) => 
                ((Func<LBNpcTalkerBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B1
        {
            internal System.Delegate act;

            internal int <>m__0(LBNpcTalkerBase x, LBNpcTalkerBase y) => 
                ((Func<LBNpcTalkerBase, LBNpcTalkerBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B2
        {
            internal System.Delegate act;

            internal bool <>m__0(LBTech obj) => 
                ((Func<LBTech, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B3
        {
            internal System.Delegate act;

            internal int <>m__0(LBTech x, LBTech y) => 
                ((Func<LBTech, LBTech, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B4
        {
            internal System.Delegate act;

            internal bool <>m__0(TechUpgradeInfo obj) => 
                ((Func<TechUpgradeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B5
        {
            internal System.Delegate act;

            internal int <>m__0(TechUpgradeInfo x, TechUpgradeInfo y) => 
                ((Func<TechUpgradeInfo, TechUpgradeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B6
        {
            internal System.Delegate act;

            internal bool <>m__0(ActivityInfo obj) => 
                ((Func<ActivityInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B7
        {
            internal System.Delegate act;

            internal int <>m__0(ActivityInfo x, ActivityInfo y) => 
                ((Func<ActivityInfo, ActivityInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B8
        {
            internal System.Delegate act;

            internal bool <>m__0(CharChipSchemeInfo obj) => 
                ((Func<CharChipSchemeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3B9
        {
            internal System.Delegate act;

            internal int <>m__0(CharChipSchemeInfo x, CharChipSchemeInfo y) => 
                ((Func<CharChipSchemeInfo, CharChipSchemeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BA
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataPassiveSkillInfo obj) => 
                ((Func<ConfigDataPassiveSkillInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BB
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataPassiveSkillInfo x, ConfigDataPassiveSkillInfo y) => 
                ((Func<ConfigDataPassiveSkillInfo, ConfigDataPassiveSkillInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BC
        {
            internal System.Delegate act;

            internal bool <>m__0(ChatInfo obj) => 
                ((Func<ChatInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BD
        {
            internal System.Delegate act;

            internal int <>m__0(ChatInfo x, ChatInfo y) => 
                ((Func<ChatInfo, ChatInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BE
        {
            internal System.Delegate act;

            internal bool <>m__0(StoreItemTransformInfo obj) => 
                ((Func<StoreItemTransformInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3BF
        {
            internal System.Delegate act;

            internal int <>m__0(StoreItemTransformInfo x, StoreItemTransformInfo y) => 
                ((Func<StoreItemTransformInfo, StoreItemTransformInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C
        {
            internal System.Delegate act;

            internal bool <>m__0(Color obj) => 
                ((Func<Color, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C0
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildJoinApplyViewInfo obj) => 
                ((Func<GuildJoinApplyViewInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C1
        {
            internal System.Delegate act;

            internal int <>m__0(GuildJoinApplyViewInfo x, GuildJoinApplyViewInfo y) => 
                ((Func<GuildJoinApplyViewInfo, GuildJoinApplyViewInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C2
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBattleSimpleReportInfo obj) => 
                ((Func<GuildBattleSimpleReportInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C3
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBattleSimpleReportInfo x, GuildBattleSimpleReportInfo y) => 
                ((Func<GuildBattleSimpleReportInfo, GuildBattleSimpleReportInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C4
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTeleportTunnelEffectInfoClient obj) => 
                ((Func<GuildTeleportTunnelEffectInfoClient, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C5
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTeleportTunnelEffectInfoClient x, GuildTeleportTunnelEffectInfoClient y) => 
                ((Func<GuildTeleportTunnelEffectInfoClient, GuildTeleportTunnelEffectInfoClient, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C6
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFleetSimpleInfo obj) => 
                ((Func<GuildFleetSimpleInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C7
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFleetSimpleInfo x, GuildFleetSimpleInfo y) => 
                ((Func<GuildFleetSimpleInfo, GuildFleetSimpleInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C8
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTradePurchaseInfo obj) => 
                ((Func<GuildTradePurchaseInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3C9
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTradePurchaseInfo x, GuildTradePurchaseInfo y) => 
                ((Func<GuildTradePurchaseInfo, GuildTradePurchaseInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CA
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildTradeTransportInfo obj) => 
                ((Func<GuildTradeTransportInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CB
        {
            internal System.Delegate act;

            internal int <>m__0(GuildTradeTransportInfo x, GuildTradeTransportInfo y) => 
                ((Func<GuildTradeTransportInfo, GuildTradeTransportInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CC
        {
            internal System.Delegate act;

            internal bool <>m__0(AmmoStoreItemInfo obj) => 
                ((Func<AmmoStoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CD
        {
            internal System.Delegate act;

            internal int <>m__0(AmmoStoreItemInfo x, AmmoStoreItemInfo y) => 
                ((Func<AmmoStoreItemInfo, AmmoStoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CE
        {
            internal System.Delegate act;

            internal bool <>m__0(StoreItemInfo obj) => 
                ((Func<StoreItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3CF
        {
            internal System.Delegate act;

            internal int <>m__0(StoreItemInfo x, StoreItemInfo y) => 
                ((Func<StoreItemInfo, StoreItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D
        {
            internal System.Delegate act;

            internal int <>m__0(Color x, Color y) => 
                ((Func<Color, Color, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D0
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataItemObtainSourceTypeInfo obj) => 
                ((Func<ConfigDataItemObtainSourceTypeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D1
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataItemObtainSourceTypeInfo x, ConfigDataItemObtainSourceTypeInfo y) => 
                ((Func<ConfigDataItemObtainSourceTypeInfo, ConfigDataItemObtainSourceTypeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D2
        {
            internal System.Delegate act;

            internal bool <>m__0(ILBStoreItemClient obj) => 
                ((Func<ILBStoreItemClient, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D3
        {
            internal System.Delegate act;

            internal int <>m__0(ILBStoreItemClient x, ILBStoreItemClient y) => 
                ((Func<ILBStoreItemClient, ILBStoreItemClient, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D4
        {
            internal System.Delegate act;

            internal bool <>m__0(Func<ILBStoreItemClient, bool> obj) => 
                ((Func<Func<ILBStoreItemClient, bool>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D5
        {
            internal System.Delegate act;

            internal int <>m__0(Func<ILBStoreItemClient, bool> x, Func<ILBStoreItemClient, bool> y) => 
                ((Func<Func<ILBStoreItemClient, bool>, Func<ILBStoreItemClient, bool>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D6
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStoreItemClient obj) => 
                ((Func<LBStoreItemClient, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D7
        {
            internal System.Delegate act;

            internal int <>m__0(LBStoreItemClient x, LBStoreItemClient y) => 
                ((Func<LBStoreItemClient, LBStoreItemClient, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D8
        {
            internal System.Delegate act;

            internal bool <>m__0(LBStoredMail obj) => 
                ((Func<LBStoredMail, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3D9
        {
            internal System.Delegate act;

            internal int <>m__0(LBStoredMail x, LBStoredMail y) => 
                ((Func<LBStoredMail, LBStoredMail, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DA
        {
            internal System.Delegate act;

            internal bool <>m__0(Func<LBStoredMail, bool> obj) => 
                ((Func<Func<LBStoredMail, bool>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DB
        {
            internal System.Delegate act;

            internal int <>m__0(Func<LBStoredMail, bool> x, Func<LBStoredMail, bool> y) => 
                ((Func<Func<LBStoredMail, bool>, Func<LBStoredMail, bool>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DC
        {
            internal System.Delegate act;

            internal bool <>m__0(VirtualBuffDesc obj) => 
                ((Func<VirtualBuffDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DD
        {
            internal System.Delegate act;

            internal int <>m__0(VirtualBuffDesc x, VirtualBuffDesc y) => 
                ((Func<VirtualBuffDesc, VirtualBuffDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DE
        {
            internal System.Delegate act;

            internal bool <>m__0(RechargeGiftPackageInfo obj) => 
                ((Func<RechargeGiftPackageInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3DF
        {
            internal System.Delegate act;

            internal int <>m__0(RechargeGiftPackageInfo x, RechargeGiftPackageInfo y) => 
                ((Func<RechargeGiftPackageInfo, RechargeGiftPackageInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E
        {
            internal System.Delegate act;

            internal bool <>m__0(Color32 obj) => 
                ((Func<Color32, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E0
        {
            internal System.Delegate act;

            internal bool <>m__0(RechargeItemInfo obj) => 
                ((Func<RechargeItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E1
        {
            internal System.Delegate act;

            internal int <>m__0(RechargeItemInfo x, RechargeItemInfo y) => 
                ((Func<RechargeItemInfo, RechargeItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E2
        {
            internal System.Delegate act;

            internal bool <>m__0(RechargeMonthlyCardInfo obj) => 
                ((Func<RechargeMonthlyCardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E3
        {
            internal System.Delegate act;

            internal int <>m__0(RechargeMonthlyCardInfo x, RechargeMonthlyCardInfo y) => 
                ((Func<RechargeMonthlyCardInfo, RechargeMonthlyCardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E4
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<string, long> obj) => 
                ((Func<KeyValuePair<string, long>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E5
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<string, long> x, KeyValuePair<string, long> y) => 
                ((Func<KeyValuePair<string, long>, KeyValuePair<string, long>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E6
        {
            internal System.Delegate act;

            internal bool <>m__0(SailReportSolarSystemInfo.SailReportEvent obj) => 
                ((Func<SailReportSolarSystemInfo.SailReportEvent, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E7
        {
            internal System.Delegate act;

            internal int <>m__0(SailReportSolarSystemInfo.SailReportEvent x, SailReportSolarSystemInfo.SailReportEvent y) => 
                ((Func<SailReportSolarSystemInfo.SailReportEvent, SailReportSolarSystemInfo.SailReportEvent, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E8
        {
            internal System.Delegate act;

            internal bool <>m__0(SailReportSolarSystemInfo obj) => 
                ((Func<SailReportSolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3E9
        {
            internal System.Delegate act;

            internal int <>m__0(SailReportSolarSystemInfo x, SailReportSolarSystemInfo y) => 
                ((Func<SailReportSolarSystemInfo, SailReportSolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3EA
        {
            internal System.Delegate act;

            internal bool <>m__0(GlobalSceneInfo obj) => 
                ((Func<GlobalSceneInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3EB
        {
            internal System.Delegate act;

            internal int <>m__0(GlobalSceneInfo x, GlobalSceneInfo y) => 
                ((Func<GlobalSceneInfo, GlobalSceneInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3EC
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<ulong, int> obj) => 
                ((Func<KeyValuePair<ulong, int>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3ED
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<ulong, int> x, KeyValuePair<ulong, int> y) => 
                ((Func<KeyValuePair<ulong, int>, KeyValuePair<ulong, int>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3EE
        {
            internal System.Delegate act;

            internal bool <>m__0(StationDynamicNpcTalkerStateInfo obj) => 
                ((Func<StationDynamicNpcTalkerStateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3EF
        {
            internal System.Delegate act;

            internal int <>m__0(StationDynamicNpcTalkerStateInfo x, StationDynamicNpcTalkerStateInfo y) => 
                ((Func<StationDynamicNpcTalkerStateInfo, StationDynamicNpcTalkerStateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F
        {
            internal System.Delegate act;

            internal int <>m__0(Color32 x, Color32 y) => 
                ((Func<Color32, Color32, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F0
        {
            internal System.Delegate act;

            internal bool <>m__0(TeamMemberInfo obj) => 
                ((Func<TeamMemberInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F1
        {
            internal System.Delegate act;

            internal int <>m__0(TeamMemberInfo x, TeamMemberInfo y) => 
                ((Func<TeamMemberInfo, TeamMemberInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F2
        {
            internal System.Delegate act;

            internal bool <>m__0(TradeListItemInfo obj) => 
                ((Func<TradeListItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F3
        {
            internal System.Delegate act;

            internal int <>m__0(TradeListItemInfo x, TradeListItemInfo y) => 
                ((Func<TradeListItemInfo, TradeListItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F4
        {
            internal System.Delegate act;

            internal bool <>m__0(TradeNpcShopItemInfo obj) => 
                ((Func<TradeNpcShopItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F5
        {
            internal System.Delegate act;

            internal int <>m__0(TradeNpcShopItemInfo x, TradeNpcShopItemInfo y) => 
                ((Func<TradeNpcShopItemInfo, TradeNpcShopItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F6
        {
            internal System.Delegate act;

            internal bool <>m__0(RechargeGoodsInfo obj) => 
                ((Func<RechargeGoodsInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F7
        {
            internal System.Delegate act;

            internal int <>m__0(RechargeGoodsInfo x, RechargeGoodsInfo y) => 
                ((Func<RechargeGoodsInfo, RechargeGoodsInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F8
        {
            internal System.Delegate act;

            internal bool <>m__0(ClientPlayerShipInfo obj) => 
                ((Func<ClientPlayerShipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3F9
        {
            internal System.Delegate act;

            internal int <>m__0(ClientPlayerShipInfo x, ClientPlayerShipInfo y) => 
                ((Func<ClientPlayerShipInfo, ClientPlayerShipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FA
        {
            internal System.Delegate act;

            internal bool <>m__0(LBShipStoreItemClient obj) => 
                ((Func<LBShipStoreItemClient, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FB
        {
            internal System.Delegate act;

            internal int <>m__0(LBShipStoreItemClient x, LBShipStoreItemClient y) => 
                ((Func<LBShipStoreItemClient, LBShipStoreItemClient, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FC
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.PlayerCustomDataInt64 obj) => 
                ((Func<ProjectXPlayerContext.PlayerCustomDataInt64, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FD
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.PlayerCustomDataInt64 x, ProjectXPlayerContext.PlayerCustomDataInt64 y) => 
                ((Func<ProjectXPlayerContext.PlayerCustomDataInt64, ProjectXPlayerContext.PlayerCustomDataInt64, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FE
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.PlayerCustomDataBoolean obj) => 
                ((Func<ProjectXPlayerContext.PlayerCustomDataBoolean, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey3FF
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.PlayerCustomDataBoolean x, ProjectXPlayerContext.PlayerCustomDataBoolean y) => 
                ((Func<ProjectXPlayerContext.PlayerCustomDataBoolean, ProjectXPlayerContext.PlayerCustomDataBoolean, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4
        {
            internal System.Delegate act;

            internal int <>m__0(float x, float y) => 
                ((Func<float, float, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40
        {
            internal System.Delegate act;

            internal bool <>m__0(Vector2 obj) => 
                ((Func<Vector2, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey400
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.GuildCompensationDataSection obj) => 
                ((Func<ProjectXPlayerContext.GuildCompensationDataSection, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey401
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.GuildCompensationDataSection x, ProjectXPlayerContext.GuildCompensationDataSection y) => 
                ((Func<ProjectXPlayerContext.GuildCompensationDataSection, ProjectXPlayerContext.GuildCompensationDataSection, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey402
        {
            internal System.Delegate act;

            internal bool <>m__0(FleetInfo obj) => 
                ((Func<FleetInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey403
        {
            internal System.Delegate act;

            internal int <>m__0(FleetInfo x, FleetInfo y) => 
                ((Func<FleetInfo, FleetInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey404
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.GuildStoreItemListInfoDataSection obj) => 
                ((Func<ProjectXPlayerContext.GuildStoreItemListInfoDataSection, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey405
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.GuildStoreItemListInfoDataSection x, ProjectXPlayerContext.GuildStoreItemListInfoDataSection y) => 
                ((Func<ProjectXPlayerContext.GuildStoreItemListInfoDataSection, ProjectXPlayerContext.GuildStoreItemListInfoDataSection, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey406
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection obj) => 
                ((Func<ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey407
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection x, ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection y) => 
                ((Func<ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection, ProjectXPlayerContext.GuildPurchaseOrderInfoDataSection, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey408
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.DataSectionHiredCaptainInfo obj) => 
                ((Func<ProjectXPlayerContext.DataSectionHiredCaptainInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey409
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.DataSectionHiredCaptainInfo x, ProjectXPlayerContext.DataSectionHiredCaptainInfo y) => 
                ((Func<ProjectXPlayerContext.DataSectionHiredCaptainInfo, ProjectXPlayerContext.DataSectionHiredCaptainInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40A
        {
            internal System.Delegate act;

            internal bool <>m__0(PVPInvadeRescueInfo obj) => 
                ((Func<PVPInvadeRescueInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40B
        {
            internal System.Delegate act;

            internal int <>m__0(PVPInvadeRescueInfo x, PVPInvadeRescueInfo y) => 
                ((Func<PVPInvadeRescueInfo, PVPInvadeRescueInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40C
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.DataSectionItemStoreInfo obj) => 
                ((Func<ProjectXPlayerContext.DataSectionItemStoreInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40D
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.DataSectionItemStoreInfo x, ProjectXPlayerContext.DataSectionItemStoreInfo y) => 
                ((Func<ProjectXPlayerContext.DataSectionItemStoreInfo, ProjectXPlayerContext.DataSectionItemStoreInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40E
        {
            internal System.Delegate act;

            internal bool <>m__0(StoreItemRecommendMarkInfo obj) => 
                ((Func<StoreItemRecommendMarkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey40F
        {
            internal System.Delegate act;

            internal int <>m__0(StoreItemRecommendMarkInfo x, StoreItemRecommendMarkInfo y) => 
                ((Func<StoreItemRecommendMarkInfo, StoreItemRecommendMarkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41
        {
            internal System.Delegate act;

            internal void <>m__0(Vector2 arg0)
            {
                ((Action<Vector2>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey410
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.DataSectionKillRecordInfo obj) => 
                ((Func<ProjectXPlayerContext.DataSectionKillRecordInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey411
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.DataSectionKillRecordInfo x, ProjectXPlayerContext.DataSectionKillRecordInfo y) => 
                ((Func<ProjectXPlayerContext.DataSectionKillRecordInfo, ProjectXPlayerContext.DataSectionKillRecordInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey412
        {
            internal System.Delegate act;

            internal bool <>m__0(StoredMailInfo obj) => 
                ((Func<StoredMailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey413
        {
            internal System.Delegate act;

            internal int <>m__0(StoredMailInfo x, StoredMailInfo y) => 
                ((Func<StoredMailInfo, StoredMailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey414
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.DataSectionMailInfo obj) => 
                ((Func<ProjectXPlayerContext.DataSectionMailInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey415
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.DataSectionMailInfo x, ProjectXPlayerContext.DataSectionMailInfo y) => 
                ((Func<ProjectXPlayerContext.DataSectionMailInfo, ProjectXPlayerContext.DataSectionMailInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey416
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<CurrencyType, ulong> obj) => 
                ((Func<KeyValuePair<CurrencyType, ulong>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey417
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<CurrencyType, ulong> x, KeyValuePair<CurrencyType, ulong> y) => 
                ((Func<KeyValuePair<CurrencyType, ulong>, KeyValuePair<CurrencyType, ulong>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey418
        {
            internal System.Delegate act;

            internal bool <>m__0(ProductionLineInfo obj) => 
                ((Func<ProductionLineInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey419
        {
            internal System.Delegate act;

            internal int <>m__0(ProductionLineInfo x, ProductionLineInfo y) => 
                ((Func<ProductionLineInfo, ProductionLineInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41A
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestProcessingInfo obj) => 
                ((Func<QuestProcessingInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41B
        {
            internal System.Delegate act;

            internal int <>m__0(QuestProcessingInfo x, QuestProcessingInfo y) => 
                ((Func<QuestProcessingInfo, QuestProcessingInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41C
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestEnvirmentInfo obj) => 
                ((Func<QuestEnvirmentInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41D
        {
            internal System.Delegate act;

            internal int <>m__0(QuestEnvirmentInfo x, QuestEnvirmentInfo y) => 
                ((Func<QuestEnvirmentInfo, QuestEnvirmentInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41E
        {
            internal System.Delegate act;

            internal bool <>m__0(ProjectXPlayerContext.DataSectionShipHangarInfo obj) => 
                ((Func<ProjectXPlayerContext.DataSectionShipHangarInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey41F
        {
            internal System.Delegate act;

            internal int <>m__0(ProjectXPlayerContext.DataSectionShipHangarInfo x, ProjectXPlayerContext.DataSectionShipHangarInfo y) => 
                ((Func<ProjectXPlayerContext.DataSectionShipHangarInfo, ProjectXPlayerContext.DataSectionShipHangarInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42
        {
            internal System.Delegate act;

            internal int <>m__0(Vector2 x, Vector2 y) => 
                ((Func<Vector2, Vector2, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey420
        {
            internal System.Delegate act;

            internal bool <>m__0(ScanProbeInfo obj) => 
                ((Func<ScanProbeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey421
        {
            internal System.Delegate act;

            internal int <>m__0(ScanProbeInfo x, ScanProbeInfo y) => 
                ((Func<ScanProbeInfo, ScanProbeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey422
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildOccupyStaticInfo obj) => 
                ((Func<StarMapGuildOccupyStaticInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey423
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildOccupyStaticInfo x, StarMapGuildOccupyStaticInfo y) => 
                ((Func<StarMapGuildOccupyStaticInfo, StarMapGuildOccupyStaticInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey424
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapGuildOccupyDynamicInfo obj) => 
                ((Func<StarMapGuildOccupyDynamicInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey425
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapGuildOccupyDynamicInfo x, StarMapGuildOccupyDynamicInfo y) => 
                ((Func<StarMapGuildOccupyDynamicInfo, StarMapGuildOccupyDynamicInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey426
        {
            internal System.Delegate act;

            internal bool <>m__0(SpaceShipController obj) => 
                ((Func<SpaceShipController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey427
        {
            internal System.Delegate act;

            internal int <>m__0(SpaceShipController x, SpaceShipController y) => 
                ((Func<SpaceShipController, SpaceShipController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey428
        {
            internal System.Delegate act;

            internal bool <>m__0(BulletTargetProviderWarpper obj) => 
                ((Func<BulletTargetProviderWarpper, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey429
        {
            internal System.Delegate act;

            internal int <>m__0(BulletTargetProviderWarpper x, BulletTargetProviderWarpper y) => 
                ((Func<BulletTargetProviderWarpper, BulletTargetProviderWarpper, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42A
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabSuperWeaponDesc.RealSlotPoint obj) => 
                ((Func<PrefabSuperWeaponDesc.RealSlotPoint, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42B
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabSuperWeaponDesc.RealSlotPoint x, PrefabSuperWeaponDesc.RealSlotPoint y) => 
                ((Func<PrefabSuperWeaponDesc.RealSlotPoint, PrefabSuperWeaponDesc.RealSlotPoint, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42C
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabNormalWeaponDesc.RealSlotPoint obj) => 
                ((Func<PrefabNormalWeaponDesc.RealSlotPoint, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42D
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabNormalWeaponDesc.RealSlotPoint x, PrefabNormalWeaponDesc.RealSlotPoint y) => 
                ((Func<PrefabNormalWeaponDesc.RealSlotPoint, PrefabNormalWeaponDesc.RealSlotPoint, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42E
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabShipDesc.LogicWeaponGroupDesc obj) => 
                ((Func<PrefabShipDesc.LogicWeaponGroupDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey42F
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabShipDesc.LogicWeaponGroupDesc x, PrefabShipDesc.LogicWeaponGroupDesc y) => 
                ((Func<PrefabShipDesc.LogicWeaponGroupDesc, PrefabShipDesc.LogicWeaponGroupDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43
        {
            internal System.Delegate act;

            internal void <>m__0(int arg0)
            {
                ((Action<int>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey430
        {
            internal System.Delegate act;

            internal bool <>m__0(PrefabShipDesc.LogicWeaponSlotDesc obj) => 
                ((Func<PrefabShipDesc.LogicWeaponSlotDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey431
        {
            internal System.Delegate act;

            internal int <>m__0(PrefabShipDesc.LogicWeaponSlotDesc x, PrefabShipDesc.LogicWeaponSlotDesc y) => 
                ((Func<PrefabShipDesc.LogicWeaponSlotDesc, PrefabShipDesc.LogicWeaponSlotDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey432
        {
            internal System.Delegate act;

            internal bool <>m__0(WeaponSlotGroupConstructInfo obj) => 
                ((Func<WeaponSlotGroupConstructInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey433
        {
            internal System.Delegate act;

            internal int <>m__0(WeaponSlotGroupConstructInfo x, WeaponSlotGroupConstructInfo y) => 
                ((Func<WeaponSlotGroupConstructInfo, WeaponSlotGroupConstructInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey434
        {
            internal System.Delegate act;

            internal bool <>m__0(TimelineBindInfo obj) => 
                ((Func<TimelineBindInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey435
        {
            internal System.Delegate act;

            internal int <>m__0(TimelineBindInfo x, TimelineBindInfo y) => 
                ((Func<TimelineBindInfo, TimelineBindInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey436
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, EventArgs e)
            {
                ((Action<object, EventArgs>) this.act)(sender, e);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey437
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, EventArgs e)
            {
                ((Action<object, EventArgs>) this.act)(sender, e);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey438
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, GestureStateChangeEventArgs e)
            {
                ((Action<object, GestureStateChangeEventArgs>) this.act)(sender, e);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey439
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, TouchLayerEventArgs e)
            {
                ((Action<object, TouchLayerEventArgs>) this.act)(sender, e);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43A
        {
            internal System.Delegate act;

            internal Vector3 <>m__0(Vector3 direction, float scaledTime) => 
                ((Func<Vector3, float, Vector3>) this.act)(direction, scaledTime);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43B
        {
            internal System.Delegate act;

            internal bool <>m__0(SpaceObjectControllerBase obj) => 
                ((Func<SpaceObjectControllerBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43C
        {
            internal System.Delegate act;

            internal int <>m__0(SpaceObjectControllerBase x, SpaceObjectControllerBase y) => 
                ((Func<SpaceObjectControllerBase, SpaceObjectControllerBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43D
        {
            internal System.Delegate act;

            internal bool <>m__0(ParticleSystemVertexStream obj) => 
                ((Func<ParticleSystemVertexStream, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43E
        {
            internal System.Delegate act;

            internal int <>m__0(ParticleSystemVertexStream x, ParticleSystemVertexStream y) => 
                ((Func<ParticleSystemVertexStream, ParticleSystemVertexStream, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey43F
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipSignalLightController.LightsAndParams obj) => 
                ((Func<ShipSignalLightController.LightsAndParams, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44
        {
            internal System.Delegate act;

            internal int <>m__0(int x, int y) => 
                ((Func<int, int, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey440
        {
            internal System.Delegate act;

            internal int <>m__0(ShipSignalLightController.LightsAndParams x, ShipSignalLightController.LightsAndParams y) => 
                ((Func<ShipSignalLightController.LightsAndParams, ShipSignalLightController.LightsAndParams, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey441
        {
            internal System.Delegate act;

            internal bool <>m__0(LogicWeaponSlot obj) => 
                ((Func<LogicWeaponSlot, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey442
        {
            internal System.Delegate act;

            internal int <>m__0(LogicWeaponSlot x, LogicWeaponSlot y) => 
                ((Func<LogicWeaponSlot, LogicWeaponSlot, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey443
        {
            internal System.Delegate act;

            internal bool <>m__0(RealWeaponSlot obj) => 
                ((Func<RealWeaponSlot, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey444
        {
            internal System.Delegate act;

            internal int <>m__0(RealWeaponSlot x, RealWeaponSlot y) => 
                ((Func<RealWeaponSlot, RealWeaponSlot, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey445
        {
            internal System.Delegate act;

            internal bool <>m__0(Transform obj) => 
                ((Func<Transform, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey446
        {
            internal System.Delegate act;

            internal int <>m__0(Transform x, Transform y) => 
                ((Func<Transform, Transform, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey447
        {
            internal System.Delegate act;

            internal bool <>m__0(MeshRenderer obj) => 
                ((Func<MeshRenderer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey448
        {
            internal System.Delegate act;

            internal int <>m__0(MeshRenderer x, MeshRenderer y) => 
                ((Func<MeshRenderer, MeshRenderer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey449
        {
            internal System.Delegate act;

            internal bool <>m__0(Renderer obj) => 
                ((Func<Renderer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44A
        {
            internal System.Delegate act;

            internal int <>m__0(Renderer x, Renderer y) => 
                ((Func<Renderer, Renderer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44B
        {
            internal System.Delegate act;

            internal bool <>m__0(TrailRenderer obj) => 
                ((Func<TrailRenderer, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44C
        {
            internal System.Delegate act;

            internal int <>m__0(TrailRenderer x, TrailRenderer y) => 
                ((Func<TrailRenderer, TrailRenderer, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44D
        {
            internal System.Delegate act;

            internal bool <>m__0(DelayPlayEffect obj) => 
                ((Func<DelayPlayEffect, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44E
        {
            internal System.Delegate act;

            internal int <>m__0(DelayPlayEffect x, DelayPlayEffect y) => 
                ((Func<DelayPlayEffect, DelayPlayEffect, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey44F
        {
            internal System.Delegate act;

            internal bool <>m__0(Dropdown.OptionData obj) => 
                ((Func<Dropdown.OptionData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45
        {
            internal System.Delegate act;

            internal bool <>m__0(BoneWeight obj) => 
                ((Func<BoneWeight, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey450
        {
            internal System.Delegate act;

            internal int <>m__0(Dropdown.OptionData x, Dropdown.OptionData y) => 
                ((Func<Dropdown.OptionData, Dropdown.OptionData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey451
        {
            internal System.Delegate act;

            internal bool <>m__0(Sprite obj) => 
                ((Func<Sprite, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey452
        {
            internal System.Delegate act;

            internal int <>m__0(Sprite x, Sprite y) => 
                ((Func<Sprite, Sprite, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey453
        {
            internal System.Delegate act;

            internal bool <>m__0(ItemSimpleInfoBlueprintMaterialUICtrl obj) => 
                ((Func<ItemSimpleInfoBlueprintMaterialUICtrl, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey454
        {
            internal System.Delegate act;

            internal int <>m__0(ItemSimpleInfoBlueprintMaterialUICtrl x, ItemSimpleInfoBlueprintMaterialUICtrl y) => 
                ((Func<ItemSimpleInfoBlueprintMaterialUICtrl, ItemSimpleInfoBlueprintMaterialUICtrl, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey455
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonDependenceAndRelativeTechItemUICtrl obj) => 
                ((Func<CommonDependenceAndRelativeTechItemUICtrl, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey456
        {
            internal System.Delegate act;

            internal int <>m__0(CommonDependenceAndRelativeTechItemUICtrl x, CommonDependenceAndRelativeTechItemUICtrl y) => 
                ((Func<CommonDependenceAndRelativeTechItemUICtrl, CommonDependenceAndRelativeTechItemUICtrl, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey457
        {
            internal System.Delegate act;

            internal char <>m__0(string text, int charIndex, char addedChar) => 
                ((Func<string, int, char, char>) this.act)(text, charIndex, addedChar);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey458
        {
            internal System.Delegate act;

            internal bool <>m__0(LongPressDesc.LongPressSingleDesc obj) => 
                ((Func<LongPressDesc.LongPressSingleDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey459
        {
            internal System.Delegate act;

            internal int <>m__0(LongPressDesc.LongPressSingleDesc x, LongPressDesc.LongPressSingleDesc y) => 
                ((Func<LongPressDesc.LongPressSingleDesc, LongPressDesc.LongPressSingleDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45A
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemNavigationPointUIController obj) => 
                ((Func<SolarSystemNavigationPointUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45B
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemNavigationPointUIController x, SolarSystemNavigationPointUIController y) => 
                ((Func<SolarSystemNavigationPointUIController, SolarSystemNavigationPointUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45C
        {
            internal System.Delegate act;

            internal bool <>m__0(EventTrigger.Entry obj) => 
                ((Func<EventTrigger.Entry, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45D
        {
            internal System.Delegate act;

            internal int <>m__0(EventTrigger.Entry x, EventTrigger.Entry y) => 
                ((Func<EventTrigger.Entry, EventTrigger.Entry, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45E
        {
            internal System.Delegate act;

            internal void <>m__0(BaseEventData arg0)
            {
                ((Action<BaseEventData>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey45F
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemUITask.TargetItemUIInfo obj) => 
                ((Func<SolarSystemUITask.TargetItemUIInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46
        {
            internal System.Delegate act;

            internal int <>m__0(BoneWeight x, BoneWeight y) => 
                ((Func<BoneWeight, BoneWeight, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey460
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemUITask.TargetItemUIInfo x, SolarSystemUITask.TargetItemUIInfo y) => 
                ((Func<SolarSystemUITask.TargetItemUIInfo, SolarSystemUITask.TargetItemUIInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey461
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemUITask.WeaponEquipGroupItemUIInfo obj) => 
                ((Func<SolarSystemUITask.WeaponEquipGroupItemUIInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey462
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemUITask.WeaponEquipGroupItemUIInfo x, SolarSystemUITask.WeaponEquipGroupItemUIInfo y) => 
                ((Func<SolarSystemUITask.WeaponEquipGroupItemUIInfo, SolarSystemUITask.WeaponEquipGroupItemUIInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey463
        {
            internal System.Delegate act;

            internal bool <>m__0(Button obj) => 
                ((Func<Button, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey464
        {
            internal System.Delegate act;

            internal int <>m__0(Button x, Button y) => 
                ((Func<Button, Button, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey465
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemWeaponEquipItemUIController obj) => 
                ((Func<SolarSystemWeaponEquipItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey466
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemWeaponEquipItemUIController x, SolarSystemWeaponEquipItemUIController y) => 
                ((Func<SolarSystemWeaponEquipItemUIController, SolarSystemWeaponEquipItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey467
        {
            internal System.Delegate act;

            internal bool <>m__0(TimelineShipActionInfo obj) => 
                ((Func<TimelineShipActionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey468
        {
            internal System.Delegate act;

            internal int <>m__0(TimelineShipActionInfo x, TimelineShipActionInfo y) => 
                ((Func<TimelineShipActionInfo, TimelineShipActionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey469
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonItemIconUIController obj) => 
                ((Func<CommonItemIconUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46A
        {
            internal System.Delegate act;

            internal int <>m__0(CommonItemIconUIController x, CommonItemIconUIController y) => 
                ((Func<CommonItemIconUIController, CommonItemIconUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46B
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataVitalityRewardInfo obj) => 
                ((Func<ConfigDataVitalityRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46C
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataVitalityRewardInfo x, ConfigDataVitalityRewardInfo y) => 
                ((Func<ConfigDataVitalityRewardInfo, ConfigDataVitalityRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46D
        {
            internal System.Delegate act;

            internal bool <>m__0(BranchStoryUITask.BranchStoryItemInfo obj) => 
                ((Func<BranchStoryUITask.BranchStoryItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46E
        {
            internal System.Delegate act;

            internal int <>m__0(BranchStoryUITask.BranchStoryItemInfo x, BranchStoryUITask.BranchStoryItemInfo y) => 
                ((Func<BranchStoryUITask.BranchStoryItemInfo, BranchStoryUITask.BranchStoryItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey46F
        {
            internal System.Delegate act;

            internal bool <>m__0(BranchStoryUITask.BranchStorySubQuestInfo obj) => 
                ((Func<BranchStoryUITask.BranchStorySubQuestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47
        {
            internal System.Delegate act;

            internal bool <>m__0(UIVertex obj) => 
                ((Func<UIVertex, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey470
        {
            internal System.Delegate act;

            internal int <>m__0(BranchStoryUITask.BranchStorySubQuestInfo x, BranchStoryUITask.BranchStorySubQuestInfo y) => 
                ((Func<BranchStoryUITask.BranchStorySubQuestInfo, BranchStoryUITask.BranchStorySubQuestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey471
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataFactionCreditNpcInfo obj) => 
                ((Func<ConfigDataFactionCreditNpcInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey472
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataFactionCreditNpcInfo x, ConfigDataFactionCreditNpcInfo y) => 
                ((Func<ConfigDataFactionCreditNpcInfo, ConfigDataFactionCreditNpcInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey473
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataFactionCreditLevelRewardInfo obj) => 
                ((Func<ConfigDataFactionCreditLevelRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey474
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataFactionCreditLevelRewardInfo x, ConfigDataFactionCreditLevelRewardInfo y) => 
                ((Func<ConfigDataFactionCreditLevelRewardInfo, ConfigDataFactionCreditLevelRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey475
        {
            internal System.Delegate act;

            internal bool <>m__0(UnAcceptedQuestUIInfo obj) => 
                ((Func<UnAcceptedQuestUIInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey476
        {
            internal System.Delegate act;

            internal int <>m__0(UnAcceptedQuestUIInfo x, UnAcceptedQuestUIInfo y) => 
                ((Func<UnAcceptedQuestUIInfo, UnAcceptedQuestUIInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey477
        {
            internal System.Delegate act;

            internal bool <>m__0(ResourceShopItemCategory obj) => 
                ((Func<ResourceShopItemCategory, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey478
        {
            internal System.Delegate act;

            internal int <>m__0(ResourceShopItemCategory x, ResourceShopItemCategory y) => 
                ((Func<ResourceShopItemCategory, ResourceShopItemCategory, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey479
        {
            internal System.Delegate act;

            internal bool <>m__0(CaptainWingManInfoUIController obj) => 
                ((Func<CaptainWingManInfoUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47A
        {
            internal System.Delegate act;

            internal int <>m__0(CaptainWingManInfoUIController x, CaptainWingManInfoUIController y) => 
                ((Func<CaptainWingManInfoUIController, CaptainWingManInfoUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47B
        {
            internal System.Delegate act;

            internal bool <>m__0(StoreItemUpdateInfo obj) => 
                ((Func<StoreItemUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47C
        {
            internal System.Delegate act;

            internal int <>m__0(StoreItemUpdateInfo x, StoreItemUpdateInfo y) => 
                ((Func<StoreItemUpdateInfo, StoreItemUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47D
        {
            internal System.Delegate act;

            internal bool <>m__0(CaptainMemoirsBookUIController obj) => 
                ((Func<CaptainMemoirsBookUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47E
        {
            internal System.Delegate act;

            internal int <>m__0(CaptainMemoirsBookUIController x, CaptainMemoirsBookUIController y) => 
                ((Func<CaptainMemoirsBookUIController, CaptainMemoirsBookUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey47F
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataCharChipInfo obj) => 
                ((Func<ConfigDataCharChipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48
        {
            internal System.Delegate act;

            internal int <>m__0(UIVertex x, UIVertex y) => 
                ((Func<UIVertex, UIVertex, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey480
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataCharChipInfo x, ConfigDataCharChipInfo y) => 
                ((Func<ConfigDataCharChipInfo, ConfigDataCharChipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey481
        {
            internal System.Delegate act;

            internal bool <>m__0(bool obj) => 
                ((Func<bool, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey482
        {
            internal System.Delegate act;

            internal int <>m__0(bool x, bool y) => 
                ((Func<bool, bool, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey483
        {
            internal System.Delegate act;

            internal bool <>m__0(KillRecordDetaiLostItemInfoUICtrl obj) => 
                ((Func<KillRecordDetaiLostItemInfoUICtrl, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey484
        {
            internal System.Delegate act;

            internal int <>m__0(KillRecordDetaiLostItemInfoUICtrl x, KillRecordDetaiLostItemInfoUICtrl y) => 
                ((Func<KillRecordDetaiLostItemInfoUICtrl, KillRecordDetaiLostItemInfoUICtrl, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey485
        {
            internal System.Delegate act;

            internal bool <>m__0(CharacterSkillTypeToggleUIController obj) => 
                ((Func<CharacterSkillTypeToggleUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey486
        {
            internal System.Delegate act;

            internal int <>m__0(CharacterSkillTypeToggleUIController x, CharacterSkillTypeToggleUIController y) => 
                ((Func<CharacterSkillTypeToggleUIController, CharacterSkillTypeToggleUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey487
        {
            internal System.Delegate act;

            internal bool <>m__0(CharacterSkillTreeUIController.SkillLearningInfoForSkillTree obj) => 
                ((Func<CharacterSkillTreeUIController.SkillLearningInfoForSkillTree, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey488
        {
            internal System.Delegate act;

            internal int <>m__0(CharacterSkillTreeUIController.SkillLearningInfoForSkillTree x, CharacterSkillTreeUIController.SkillLearningInfoForSkillTree y) => 
                ((Func<CharacterSkillTreeUIController.SkillLearningInfoForSkillTree, CharacterSkillTreeUIController.SkillLearningInfoForSkillTree, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey489
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataPassiveSkillTypeInfo obj) => 
                ((Func<ConfigDataPassiveSkillTypeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48A
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataPassiveSkillTypeInfo x, ConfigDataPassiveSkillTypeInfo y) => 
                ((Func<ConfigDataPassiveSkillTypeInfo, ConfigDataPassiveSkillTypeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48B
        {
            internal System.Delegate act;

            internal bool <>m__0(CharacterSkillCategoryToggleUIController obj) => 
                ((Func<CharacterSkillCategoryToggleUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48C
        {
            internal System.Delegate act;

            internal int <>m__0(CharacterSkillCategoryToggleUIController x, CharacterSkillCategoryToggleUIController y) => 
                ((Func<CharacterSkillCategoryToggleUIController, CharacterSkillCategoryToggleUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48D
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, AssemblyLoadEventArgs args)
            {
                ((Action<object, AssemblyLoadEventArgs>) this.act)(sender, args);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48E
        {
            internal System.Delegate act;

            internal Assembly <>m__0(object sender, ResolveEventArgs args) => 
                ((Func<object, ResolveEventArgs, Assembly>) this.act)(sender, args);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey48F
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, UnhandledExceptionEventArgs e)
            {
                ((Action<object, UnhandledExceptionEventArgs>) this.act)(sender, e);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49
        {
            internal System.Delegate act;

            internal bool <>m__0(UIStateDesc obj) => 
                ((Func<UIStateDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey490
        {
            internal System.Delegate act;

            internal bool <>m__0(ChatContentVoice obj) => 
                ((Func<ChatContentVoice, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey491
        {
            internal System.Delegate act;

            internal int <>m__0(ChatContentVoice x, ChatContentVoice y) => 
                ((Func<ChatContentVoice, ChatContentVoice, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey492
        {
            internal System.Delegate act;

            internal bool <>m__0(AllianceSimplestInfo obj) => 
                ((Func<AllianceSimplestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey493
        {
            internal System.Delegate act;

            internal int <>m__0(AllianceSimplestInfo x, AllianceSimplestInfo y) => 
                ((Func<AllianceSimplestInfo, AllianceSimplestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey494
        {
            internal System.Delegate act;

            internal bool <>m__0(CurrencyUpdateInfo obj) => 
                ((Func<CurrencyUpdateInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey495
        {
            internal System.Delegate act;

            internal int <>m__0(CurrencyUpdateInfo x, CurrencyUpdateInfo y) => 
                ((Func<CurrencyUpdateInfo, CurrencyUpdateInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey496
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonMenuItemUIController.CommonMenuItemKey obj) => 
                ((Func<CommonMenuItemUIController.CommonMenuItemKey, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey497
        {
            internal System.Delegate act;

            internal int <>m__0(CommonMenuItemUIController.CommonMenuItemKey x, CommonMenuItemUIController.CommonMenuItemKey y) => 
                ((Func<CommonMenuItemUIController.CommonMenuItemKey, CommonMenuItemUIController.CommonMenuItemKey, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey498
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonMenuItemUIController obj) => 
                ((Func<CommonMenuItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey499
        {
            internal System.Delegate act;

            internal int <>m__0(CommonMenuItemUIController x, CommonMenuItemUIController y) => 
                ((Func<CommonMenuItemUIController, CommonMenuItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49A
        {
            internal System.Delegate act;

            internal bool <>m__0(FakeLBStoreItem obj) => 
                ((Func<FakeLBStoreItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49B
        {
            internal System.Delegate act;

            internal int <>m__0(FakeLBStoreItem x, FakeLBStoreItem y) => 
                ((Func<FakeLBStoreItem, FakeLBStoreItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49C
        {
            internal System.Delegate act;

            internal bool <>m__0(VirtualBuffItemUIController obj) => 
                ((Func<VirtualBuffItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49D
        {
            internal System.Delegate act;

            internal int <>m__0(VirtualBuffItemUIController x, VirtualBuffItemUIController y) => 
                ((Func<VirtualBuffItemUIController, VirtualBuffItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49E
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonDelayDesc.DelayDesc obj) => 
                ((Func<CommonDelayDesc.DelayDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey49F
        {
            internal System.Delegate act;

            internal int <>m__0(CommonDelayDesc.DelayDesc x, CommonDelayDesc.DelayDesc y) => 
                ((Func<CommonDelayDesc.DelayDesc, CommonDelayDesc.DelayDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A
        {
            internal System.Delegate act;

            internal int <>m__0(UIStateDesc x, UIStateDesc y) => 
                ((Func<UIStateDesc, UIStateDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A0
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataDevelopmentNodeDescInfo obj) => 
                ((Func<ConfigDataDevelopmentNodeDescInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A1
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataDevelopmentNodeDescInfo x, ConfigDataDevelopmentNodeDescInfo y) => 
                ((Func<ConfigDataDevelopmentNodeDescInfo, ConfigDataDevelopmentNodeDescInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A2
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataDevelopmentProjectInfo obj) => 
                ((Func<ConfigDataDevelopmentProjectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A3
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataDevelopmentProjectInfo x, ConfigDataDevelopmentProjectInfo y) => 
                ((Func<ConfigDataDevelopmentProjectInfo, ConfigDataDevelopmentProjectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A4
        {
            internal System.Delegate act;

            internal bool <>m__0(DiplomacyItemInfo obj) => 
                ((Func<DiplomacyItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A5
        {
            internal System.Delegate act;

            internal int <>m__0(DiplomacyItemInfo x, DiplomacyItemInfo y) => 
                ((Func<DiplomacyItemInfo, DiplomacyItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A6
        {
            internal System.Delegate act;

            internal bool <>m__0(DrivingQuestMonryOrExpRewardItemUIController obj) => 
                ((Func<DrivingQuestMonryOrExpRewardItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A7
        {
            internal System.Delegate act;

            internal int <>m__0(DrivingQuestMonryOrExpRewardItemUIController x, DrivingQuestMonryOrExpRewardItemUIController y) => 
                ((Func<DrivingQuestMonryOrExpRewardItemUIController, DrivingQuestMonryOrExpRewardItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A8
        {
            internal System.Delegate act;

            internal bool <>m__0(DrivingLicenseNodeInfoUIController obj) => 
                ((Func<DrivingLicenseNodeInfoUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4A9
        {
            internal System.Delegate act;

            internal int <>m__0(DrivingLicenseNodeInfoUIController x, DrivingLicenseNodeInfoUIController y) => 
                ((Func<DrivingLicenseNodeInfoUIController, DrivingLicenseNodeInfoUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AA
        {
            internal System.Delegate act;

            internal bool <>m__0(DrivingToggleItemUIController obj) => 
                ((Func<DrivingToggleItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AB
        {
            internal System.Delegate act;

            internal int <>m__0(DrivingToggleItemUIController x, DrivingToggleItemUIController y) => 
                ((Func<DrivingToggleItemUIController, DrivingToggleItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AC
        {
            internal System.Delegate act;

            internal bool <>m__0(RectTransform obj) => 
                ((Func<RectTransform, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AD
        {
            internal System.Delegate act;

            internal int <>m__0(RectTransform x, RectTransform y) => 
                ((Func<RectTransform, RectTransform, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AE
        {
            internal System.Delegate act;

            internal bool <>m__0(SystemFuncType obj) => 
                ((Func<SystemFuncType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4AF
        {
            internal System.Delegate act;

            internal int <>m__0(SystemFuncType x, SystemFuncType y) => 
                ((Func<SystemFuncType, SystemFuncType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B
        {
            internal System.Delegate act;

            internal bool <>m__0(ColorSetDesc obj) => 
                ((Func<ColorSetDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B0
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataGuildActionInfo obj) => 
                ((Func<ConfigDataGuildActionInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B1
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataGuildActionInfo x, ConfigDataGuildActionInfo y) => 
                ((Func<ConfigDataGuildActionInfo, ConfigDataGuildActionInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B2
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildCompensationHintItemUIController obj) => 
                ((Func<GuildCompensationHintItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B3
        {
            internal System.Delegate act;

            internal int <>m__0(GuildCompensationHintItemUIController x, GuildCompensationHintItemUIController y) => 
                ((Func<GuildCompensationHintItemUIController, GuildCompensationHintItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B4
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildCompensationContributItemUIController obj) => 
                ((Func<GuildCompensationContributItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B5
        {
            internal System.Delegate act;

            internal int <>m__0(GuildCompensationContributItemUIController x, GuildCompensationContributItemUIController y) => 
                ((Func<GuildCompensationContributItemUIController, GuildCompensationContributItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B6
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildInformationPointBenefitsInfo obj) => 
                ((Func<GuildInformationPointBenefitsInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B7
        {
            internal System.Delegate act;

            internal int <>m__0(GuildInformationPointBenefitsInfo x, GuildInformationPointBenefitsInfo y) => 
                ((Func<GuildInformationPointBenefitsInfo, GuildInformationPointBenefitsInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B8
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataGuildBenefitsConfigInfo obj) => 
                ((Func<ConfigDataGuildBenefitsConfigInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4B9
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataGuildBenefitsConfigInfo x, ConfigDataGuildBenefitsConfigInfo y) => 
                ((Func<ConfigDataGuildBenefitsConfigInfo, ConfigDataGuildBenefitsConfigInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BA
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildCommonBenefitsInfo obj) => 
                ((Func<GuildCommonBenefitsInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BB
        {
            internal System.Delegate act;

            internal int <>m__0(GuildCommonBenefitsInfo x, GuildCommonBenefitsInfo y) => 
                ((Func<GuildCommonBenefitsInfo, GuildCommonBenefitsInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BC
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFlagShipHangarShipListItemUIController obj) => 
                ((Func<GuildFlagShipHangarShipListItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BD
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFlagShipHangarShipListItemUIController x, GuildFlagShipHangarShipListItemUIController y) => 
                ((Func<GuildFlagShipHangarShipListItemUIController, GuildFlagShipHangarShipListItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BE
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo obj) => 
                ((Func<GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4BF
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo x, GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo y) => 
                ((Func<GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo, GuildFlagShipOptLogUITask.GuildFlagShipLogTypeFilterItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C
        {
            internal System.Delegate act;

            internal int <>m__0(ColorSetDesc x, ColorSetDesc y) => 
                ((Func<ColorSetDesc, ColorSetDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C0
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo obj) => 
                ((Func<GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C1
        {
            internal System.Delegate act;

            internal int <>m__0(GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo x, GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo y) => 
                ((Func<GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo, GuildFlagShipOptLogUITask.GuildFlagShipLogSolarSystemFilterItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C2
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildLogoEditorUIController.AssetWithId obj) => 
                ((Func<GuildLogoEditorUIController.AssetWithId, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C3
        {
            internal System.Delegate act;

            internal int <>m__0(GuildLogoEditorUIController.AssetWithId x, GuildLogoEditorUIController.AssetWithId y) => 
                ((Func<GuildLogoEditorUIController.AssetWithId, GuildLogoEditorUIController.AssetWithId, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C4
        {
            internal System.Delegate act;

            internal void <>m__0(List<TextExpand.LinkInfo> arg0)
            {
                ((Action<List<TextExpand.LinkInfo>>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C5
        {
            internal System.Delegate act;

            internal bool <>m__0(TextExpand.LinkInfo obj) => 
                ((Func<TextExpand.LinkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C6
        {
            internal System.Delegate act;

            internal int <>m__0(TextExpand.LinkInfo x, TextExpand.LinkInfo y) => 
                ((Func<TextExpand.LinkInfo, TextExpand.LinkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C7
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildJobCtrlJobContentItemUnitController obj) => 
                ((Func<GuildJobCtrlJobContentItemUnitController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C8
        {
            internal System.Delegate act;

            internal int <>m__0(GuildJobCtrlJobContentItemUnitController x, GuildJobCtrlJobContentItemUnitController y) => 
                ((Func<GuildJobCtrlJobContentItemUnitController, GuildJobCtrlJobContentItemUnitController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4C9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildMemberJobCtrlUITask.JobInfo obj) => 
                ((Func<GuildMemberJobCtrlUITask.JobInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildMemberJobCtrlUITask.JobInfo x, GuildMemberJobCtrlUITask.JobInfo y) => 
                ((Func<GuildMemberJobCtrlUITask.JobInfo, GuildMemberJobCtrlUITask.JobInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProductTypeController obj) => 
                ((Func<ProductTypeController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CC
        {
            internal System.Delegate act;

            internal int <>m__0(ProductTypeController x, ProductTypeController y) => 
                ((Func<ProductTypeController, ProductTypeController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildItemTypeMenuItemData obj) => 
                ((Func<GuildItemTypeMenuItemData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildItemTypeMenuItemData x, GuildItemTypeMenuItemData y) => 
                ((Func<GuildItemTypeMenuItemData, GuildItemTypeMenuItemData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4CF
        {
            internal System.Delegate act;

            internal bool <>m__0(ServerInfo obj) => 
                ((Func<ServerInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D
        {
            internal System.Delegate act;

            internal bool <>m__0(UIStateColorDesc obj) => 
                ((Func<UIStateColorDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D0
        {
            internal System.Delegate act;

            internal int <>m__0(ServerInfo x, ServerInfo y) => 
                ((Func<ServerInfo, ServerInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D1
        {
            internal System.Delegate act;

            internal bool <>m__0(MailItemUIController obj) => 
                ((Func<MailItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D2
        {
            internal System.Delegate act;

            internal int <>m__0(MailItemUIController x, MailItemUIController y) => 
                ((Func<MailItemUIController, MailItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D3
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcShopMainUITask.NpcShopItemUIInfo obj) => 
                ((Func<NpcShopMainUITask.NpcShopItemUIInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D4
        {
            internal System.Delegate act;

            internal int <>m__0(NpcShopMainUITask.NpcShopItemUIInfo x, NpcShopMainUITask.NpcShopItemUIInfo y) => 
                ((Func<NpcShopMainUITask.NpcShopItemUIInfo, NpcShopMainUITask.NpcShopItemUIInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D5
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<string, NpcDialogOptType> obj) => 
                ((Func<KeyValuePair<string, NpcDialogOptType>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D6
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<string, NpcDialogOptType> x, KeyValuePair<string, NpcDialogOptType> y) => 
                ((Func<KeyValuePair<string, NpcDialogOptType>, KeyValuePair<string, NpcDialogOptType>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D7
        {
            internal System.Delegate act;

            internal bool <>m__0(ProduceItemStoreTypeToggleUIController obj) => 
                ((Func<ProduceItemStoreTypeToggleUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D8
        {
            internal System.Delegate act;

            internal int <>m__0(ProduceItemStoreTypeToggleUIController x, ProduceItemStoreTypeToggleUIController y) => 
                ((Func<ProduceItemStoreTypeToggleUIController, ProduceItemStoreTypeToggleUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4D9
        {
            internal System.Delegate act;

            internal bool <>m__0(ProduceItemStoreCategoryToggleUIController obj) => 
                ((Func<ProduceItemStoreCategoryToggleUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DA
        {
            internal System.Delegate act;

            internal int <>m__0(ProduceItemStoreCategoryToggleUIController x, ProduceItemStoreCategoryToggleUIController y) => 
                ((Func<ProduceItemStoreCategoryToggleUIController, ProduceItemStoreCategoryToggleUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DB
        {
            internal System.Delegate act;

            internal bool <>m__0(ProduceQueueItemUIController obj) => 
                ((Func<ProduceQueueItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DC
        {
            internal System.Delegate act;

            internal int <>m__0(ProduceQueueItemUIController x, ProduceQueueItemUIController y) => 
                ((Func<ProduceQueueItemUIController, ProduceQueueItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DD
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildProduceQueueItemUIController obj) => 
                ((Func<GuildProduceQueueItemUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildProduceQueueItemUIController x, GuildProduceQueueItemUIController y) => 
                ((Func<GuildProduceQueueItemUIController, GuildProduceQueueItemUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4DF
        {
            internal System.Delegate act;

            internal bool <>m__0(AmmoInfo obj) => 
                ((Func<AmmoInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E
        {
            internal System.Delegate act;

            internal int <>m__0(UIStateColorDesc x, UIStateColorDesc y) => 
                ((Func<UIStateColorDesc, UIStateColorDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E0
        {
            internal System.Delegate act;

            internal int <>m__0(AmmoInfo x, AmmoInfo y) => 
                ((Func<AmmoInfo, AmmoInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E1
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipStrikeUITask.CachedHireCaptain obj) => 
                ((Func<ShipStrikeUITask.CachedHireCaptain, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E2
        {
            internal System.Delegate act;

            internal int <>m__0(ShipStrikeUITask.CachedHireCaptain x, ShipStrikeUITask.CachedHireCaptain y) => 
                ((Func<ShipStrikeUITask.CachedHireCaptain, ShipStrikeUITask.CachedHireCaptain, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E3
        {
            internal System.Delegate act;

            internal bool <>m__0(SpaceStationUI3DSceneObjectDesc obj) => 
                ((Func<SpaceStationUI3DSceneObjectDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E4
        {
            internal System.Delegate act;

            internal int <>m__0(SpaceStationUI3DSceneObjectDesc x, SpaceStationUI3DSceneObjectDesc y) => 
                ((Func<SpaceStationUI3DSceneObjectDesc, SpaceStationUI3DSceneObjectDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E5
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapForGuildUITask.GuildOccupiedSolarSystemInfo obj) => 
                ((Func<StarMapForGuildUITask.GuildOccupiedSolarSystemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E6
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapForGuildUITask.GuildOccupiedSolarSystemInfo x, StarMapForGuildUITask.GuildOccupiedSolarSystemInfo y) => 
                ((Func<StarMapForGuildUITask.GuildOccupiedSolarSystemInfo, StarMapForGuildUITask.GuildOccupiedSolarSystemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E7
        {
            internal System.Delegate act;

            internal bool <>m__0(LogicBlockStarMapInfoClient.LocationItemInfo obj) => 
                ((Func<LogicBlockStarMapInfoClient.LocationItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E8
        {
            internal System.Delegate act;

            internal int <>m__0(LogicBlockStarMapInfoClient.LocationItemInfo x, LogicBlockStarMapInfoClient.LocationItemInfo y) => 
                ((Func<LogicBlockStarMapInfoClient.LocationItemInfo, LogicBlockStarMapInfoClient.LocationItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4E9
        {
            internal System.Delegate act;

            internal bool <>m__0(LBSignalBase obj) => 
                ((Func<LBSignalBase, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4EA
        {
            internal System.Delegate act;

            internal int <>m__0(LBSignalBase x, LBSignalBase y) => 
                ((Func<LBSignalBase, LBSignalBase, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4EB
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapItemLocationUIController obj) => 
                ((Func<StarMapItemLocationUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4EC
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapItemLocationUIController x, StarMapItemLocationUIController y) => 
                ((Func<StarMapItemLocationUIController, StarMapItemLocationUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4ED
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildSentrySignalHudUIController obj) => 
                ((Func<GuildSentrySignalHudUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4EE
        {
            internal System.Delegate act;

            internal int <>m__0(GuildSentrySignalHudUIController x, GuildSentrySignalHudUIController y) => 
                ((Func<GuildSentrySignalHudUIController, GuildSentrySignalHudUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4EF
        {
            internal System.Delegate act;

            internal bool <>m__0(Vector3D obj) => 
                ((Func<Vector3D, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F
        {
            internal System.Delegate act;

            internal bool <>m__0(Image obj) => 
                ((Func<Image, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F0
        {
            internal System.Delegate act;

            internal int <>m__0(Vector3D x, Vector3D y) => 
                ((Func<Vector3D, Vector3D, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F1
        {
            internal System.Delegate act;

            internal bool <>m__0(GEPlanetType obj) => 
                ((Func<GEPlanetType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F2
        {
            internal System.Delegate act;

            internal int <>m__0(GEPlanetType x, GEPlanetType y) => 
                ((Func<GEPlanetType, GEPlanetType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F3
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildLogoDisplay obj) => 
                ((Func<GuildLogoDisplay, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F4
        {
            internal System.Delegate act;

            internal int <>m__0(GuildLogoDisplay x, GuildLogoDisplay y) => 
                ((Func<GuildLogoDisplay, GuildLogoDisplay, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F5
        {
            internal System.Delegate act;

            internal bool <>m__0(StarMapForStarfieldSSPointUIController obj) => 
                ((Func<StarMapForStarfieldSSPointUIController, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F6
        {
            internal System.Delegate act;

            internal int <>m__0(StarMapForStarfieldSSPointUIController x, StarMapForStarfieldSSPointUIController y) => 
                ((Func<StarMapForStarfieldSSPointUIController, StarMapForStarfieldSSPointUIController, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F7
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<float, Color> obj) => 
                ((Func<KeyValuePair<float, Color>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F8
        {
            internal System.Delegate act;

            internal int <>m__0(KeyValuePair<float, Color> x, KeyValuePair<float, Color> y) => 
                ((Func<KeyValuePair<float, Color>, KeyValuePair<float, Color>, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4F9
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildViewHelper.StarArea obj) => 
                ((Func<GuildViewHelper.StarArea, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FA
        {
            internal System.Delegate act;

            internal int <>m__0(GuildViewHelper.StarArea x, GuildViewHelper.StarArea y) => 
                ((Func<GuildViewHelper.StarArea, GuildViewHelper.StarArea, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FB
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo obj) => 
                ((Func<SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FC
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo x, SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo y) => 
                ((Func<SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo, SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FD
        {
            internal System.Delegate act;

            internal bool <>m__0(TeamMainUIController.TeamMemberUIItemInfo obj) => 
                ((Func<TeamMainUIController.TeamMemberUIItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FE
        {
            internal System.Delegate act;

            internal int <>m__0(TeamMainUIController.TeamMemberUIItemInfo x, TeamMainUIController.TeamMemberUIItemInfo y) => 
                ((Func<TeamMainUIController.TeamMemberUIItemInfo, TeamMainUIController.TeamMemberUIItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey4FF
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataTechUITypeInfo obj) => 
                ((Func<ConfigDataTechUITypeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5
        {
            internal System.Delegate act;

            internal bool <>m__0(Vector4 obj) => 
                ((Func<Vector4, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50
        {
            internal System.Delegate act;

            internal int <>m__0(Image x, Image y) => 
                ((Func<Image, Image, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey500
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataTechUITypeInfo x, ConfigDataTechUITypeInfo y) => 
                ((Func<ConfigDataTechUITypeInfo, ConfigDataTechUITypeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey501
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataTechInfo obj) => 
                ((Func<ConfigDataTechInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey502
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataTechInfo x, ConfigDataTechInfo y) => 
                ((Func<ConfigDataTechInfo, ConfigDataTechInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey503
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigDataNormalItemInfo obj) => 
                ((Func<ConfigDataNormalItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey504
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigDataNormalItemInfo x, ConfigDataNormalItemInfo y) => 
                ((Func<ConfigDataNormalItemInfo, ConfigDataNormalItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey505
        {
            internal System.Delegate act;

            internal bool <>m__0(TradeItemTypeMenuItemData obj) => 
                ((Func<TradeItemTypeMenuItemData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey506
        {
            internal System.Delegate act;

            internal int <>m__0(TradeItemTypeMenuItemData x, TradeItemTypeMenuItemData y) => 
                ((Func<TradeItemTypeMenuItemData, TradeItemTypeMenuItemData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey507
        {
            internal System.Delegate act;

            internal bool <>m__0(TradeItemMenuItemData obj) => 
                ((Func<TradeItemMenuItemData, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey508
        {
            internal System.Delegate act;

            internal int <>m__0(TradeItemMenuItemData x, TradeItemMenuItemData y) => 
                ((Func<TradeItemMenuItemData, TradeItemMenuItemData, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey509
        {
            internal System.Delegate act;

            internal bool <>m__0(TradeListItemSellInfo obj) => 
                ((Func<TradeListItemSellInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50A
        {
            internal System.Delegate act;

            internal int <>m__0(TradeListItemSellInfo x, TradeListItemSellInfo y) => 
                ((Func<TradeListItemSellInfo, TradeListItemSellInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50B
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonUIResourceContainer.AssetCacheItem obj) => 
                ((Func<CommonUIResourceContainer.AssetCacheItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50C
        {
            internal System.Delegate act;

            internal int <>m__0(CommonUIResourceContainer.AssetCacheItem x, CommonUIResourceContainer.AssetCacheItem y) => 
                ((Func<CommonUIResourceContainer.AssetCacheItem, CommonUIResourceContainer.AssetCacheItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50D
        {
            internal System.Delegate act;

            internal bool <>m__0(PropertyValueStringPair obj) => 
                ((Func<PropertyValueStringPair, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50E
        {
            internal System.Delegate act;

            internal int <>m__0(PropertyValueStringPair x, PropertyValueStringPair y) => 
                ((Func<PropertyValueStringPair, PropertyValueStringPair, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey50F
        {
            internal System.Delegate act;

            internal bool <>m__0(FunctionOpenStateUtil.FunctionOpenStateDisplayInfo obj) => 
                ((Func<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey51
        {
            internal System.Delegate act;

            internal bool <>m__0(Rect obj) => 
                ((Func<Rect, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey510
        {
            internal System.Delegate act;

            internal int <>m__0(FunctionOpenStateUtil.FunctionOpenStateDisplayInfo x, FunctionOpenStateUtil.FunctionOpenStateDisplayInfo y) => 
                ((Func<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo, FunctionOpenStateUtil.FunctionOpenStateDisplayInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey511
        {
            internal System.Delegate act;

            internal bool <>m__0(IProtobufExtensionTypeInfo obj) => 
                ((Func<IProtobufExtensionTypeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey512
        {
            internal System.Delegate act;

            internal int <>m__0(IProtobufExtensionTypeInfo x, IProtobufExtensionTypeInfo y) => 
                ((Func<IProtobufExtensionTypeInfo, IProtobufExtensionTypeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey513
        {
            internal System.Delegate act;

            internal System.Type <>m__0(int fieldNumber) => 
                ((Func<int, System.Type>) this.act)(fieldNumber);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey514
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, TypeFormatEventArgs args)
            {
                ((Action<object, TypeFormatEventArgs>) this.act)(sender, args);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey515
        {
            internal System.Delegate act;

            internal void <>m__0(object sender, LockContentedEventArgs args)
            {
                ((Action<object, LockContentedEventArgs>) this.act)(sender, args);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey52
        {
            internal System.Delegate act;

            internal int <>m__0(Rect x, Rect y) => 
                ((Func<Rect, Rect, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey53
        {
            internal System.Delegate act;

            internal bool <>m__0(Text obj) => 
                ((Func<Text, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey54
        {
            internal System.Delegate act;

            internal int <>m__0(Text x, Text y) => 
                ((Func<Text, Text, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey55
        {
            internal System.Delegate act;

            internal bool <>m__0(UICharInfo obj) => 
                ((Func<UICharInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey56
        {
            internal System.Delegate act;

            internal int <>m__0(UICharInfo x, UICharInfo y) => 
                ((Func<UICharInfo, UICharInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey57
        {
            internal System.Delegate act;

            internal bool <>m__0(UILineInfo obj) => 
                ((Func<UILineInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey58
        {
            internal System.Delegate act;

            internal int <>m__0(UILineInfo x, UILineInfo y) => 
                ((Func<UILineInfo, UILineInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey59
        {
            internal System.Delegate act;

            internal bool <>m__0(FrequentChangeText obj) => 
                ((Func<FrequentChangeText, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5A
        {
            internal System.Delegate act;

            internal int <>m__0(FrequentChangeText x, FrequentChangeText y) => 
                ((Func<FrequentChangeText, FrequentChangeText, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5B
        {
            internal System.Delegate act;

            internal bool <>m__0(ButtonEx obj) => 
                ((Func<ButtonEx, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5C
        {
            internal System.Delegate act;

            internal int <>m__0(ButtonEx x, ButtonEx y) => 
                ((Func<ButtonEx, ButtonEx, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5D
        {
            internal System.Delegate act;

            internal bool <>m__0(RaycastResult obj) => 
                ((Func<RaycastResult, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5E
        {
            internal System.Delegate act;

            internal int <>m__0(RaycastResult x, RaycastResult y) => 
                ((Func<RaycastResult, RaycastResult, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey5F
        {
            internal System.Delegate act;

            internal bool <>m__0(AnimatorClipInfo obj) => 
                ((Func<AnimatorClipInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6
        {
            internal System.Delegate act;

            internal int <>m__0(Vector4 x, Vector4 y) => 
                ((Func<Vector4, Vector4, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey60
        {
            internal System.Delegate act;

            internal int <>m__0(AnimatorClipInfo x, AnimatorClipInfo y) => 
                ((Func<AnimatorClipInfo, AnimatorClipInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey61
        {
            internal System.Delegate act;

            internal bool <>m__0(Selectable obj) => 
                ((Func<Selectable, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey62
        {
            internal System.Delegate act;

            internal int <>m__0(Selectable x, Selectable y) => 
                ((Func<Selectable, Selectable, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey63
        {
            internal System.Delegate act;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                ((Action<UIProcess, bool>) this.act)(process, isCompleted);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey64
        {
            internal System.Delegate act;

            internal void <>m__0(Action<bool> onExecEnd)
            {
                ((Action<Action<bool>>) this.act)(onExecEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey65
        {
            internal System.Delegate act;

            internal bool <>m__0(UserGuideStepInfo.IPageInfo obj) => 
                ((Func<UserGuideStepInfo.IPageInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey66
        {
            internal System.Delegate act;

            internal int <>m__0(UserGuideStepInfo.IPageInfo x, UserGuideStepInfo.IPageInfo y) => 
                ((Func<UserGuideStepInfo.IPageInfo, UserGuideStepInfo.IPageInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey67
        {
            internal System.Delegate act;

            internal bool <>m__0(StarfieldsLinkInfo obj) => 
                ((Func<StarfieldsLinkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey68
        {
            internal System.Delegate act;

            internal int <>m__0(StarfieldsLinkInfo x, StarfieldsLinkInfo y) => 
                ((Func<StarfieldsLinkInfo, StarfieldsLinkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey69
        {
            internal System.Delegate act;

            internal bool <>m__0(StargateDesc obj) => 
                ((Func<StargateDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6A
        {
            internal System.Delegate act;

            internal int <>m__0(StargateDesc x, StargateDesc y) => 
                ((Func<StargateDesc, StargateDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6B
        {
            internal System.Delegate act;

            internal bool <>m__0(PlanetDesc obj) => 
                ((Func<PlanetDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6C
        {
            internal System.Delegate act;

            internal int <>m__0(PlanetDesc x, PlanetDesc y) => 
                ((Func<PlanetDesc, PlanetDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6D
        {
            internal System.Delegate act;

            internal bool <>m__0(MoonDesc obj) => 
                ((Func<MoonDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6E
        {
            internal System.Delegate act;

            internal int <>m__0(MoonDesc x, MoonDesc y) => 
                ((Func<MoonDesc, MoonDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey6F
        {
            internal System.Delegate act;

            internal bool <>m__0(PlanetLikeBuildingDesc obj) => 
                ((Func<PlanetLikeBuildingDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7
        {
            internal System.Delegate act;

            internal bool <>m__0(Matrix4x4 obj) => 
                ((Func<Matrix4x4, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey70
        {
            internal System.Delegate act;

            internal int <>m__0(PlanetLikeBuildingDesc x, PlanetLikeBuildingDesc y) => 
                ((Func<PlanetLikeBuildingDesc, PlanetLikeBuildingDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey71
        {
            internal System.Delegate act;

            internal bool <>m__0(SpaceStationDesc.NPCInfo obj) => 
                ((Func<SpaceStationDesc.NPCInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey72
        {
            internal System.Delegate act;

            internal int <>m__0(SpaceStationDesc.NPCInfo x, SpaceStationDesc.NPCInfo y) => 
                ((Func<SpaceStationDesc.NPCInfo, SpaceStationDesc.NPCInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey73
        {
            internal System.Delegate act;

            internal bool <>m__0(SpaceStationDesc obj) => 
                ((Func<SpaceStationDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey74
        {
            internal System.Delegate act;

            internal int <>m__0(SpaceStationDesc x, SpaceStationDesc y) => 
                ((Func<SpaceStationDesc, SpaceStationDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey75
        {
            internal System.Delegate act;

            internal bool <>m__0(AsteroidBeltOrbitPosition obj) => 
                ((Func<AsteroidBeltOrbitPosition, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey76
        {
            internal System.Delegate act;

            internal int <>m__0(AsteroidBeltOrbitPosition x, AsteroidBeltOrbitPosition y) => 
                ((Func<AsteroidBeltOrbitPosition, AsteroidBeltOrbitPosition, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey77
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemSceneDesc obj) => 
                ((Func<SolarSystemSceneDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey78
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemSceneDesc x, SolarSystemSceneDesc y) => 
                ((Func<SolarSystemSceneDesc, SolarSystemSceneDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey79
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingDesc obj) => 
                ((Func<GuildBuildingDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7A
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingDesc x, GuildBuildingDesc y) => 
                ((Func<GuildBuildingDesc, GuildBuildingDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7B
        {
            internal System.Delegate act;

            internal bool <>m__0(StargroupsLinkInfo obj) => 
                ((Func<StargroupsLinkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7C
        {
            internal System.Delegate act;

            internal int <>m__0(StargroupsLinkInfo x, StargroupsLinkInfo y) => 
                ((Func<StargroupsLinkInfo, StargroupsLinkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7D
        {
            internal System.Delegate act;

            internal bool <>m__0(SolarSystemsLinkInfo obj) => 
                ((Func<SolarSystemsLinkInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7E
        {
            internal System.Delegate act;

            internal int <>m__0(SolarSystemsLinkInfo x, SolarSystemsLinkInfo y) => 
                ((Func<SolarSystemsLinkInfo, SolarSystemsLinkInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey7F
        {
            internal System.Delegate act;

            internal bool <>m__0(StarGroupIslandInfo obj) => 
                ((Func<StarGroupIslandInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8
        {
            internal System.Delegate act;

            internal int <>m__0(Matrix4x4 x, Matrix4x4 y) => 
                ((Func<Matrix4x4, Matrix4x4, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey80
        {
            internal System.Delegate act;

            internal int <>m__0(StarGroupIslandInfo x, StarGroupIslandInfo y) => 
                ((Func<StarGroupIslandInfo, StarGroupIslandInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey81
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonPropertyInfo obj) => 
                ((Func<CommonPropertyInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey82
        {
            internal System.Delegate act;

            internal int <>m__0(CommonPropertyInfo x, CommonPropertyInfo y) => 
                ((Func<CommonPropertyInfo, CommonPropertyInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey83
        {
            internal System.Delegate act;

            internal bool <>m__0(ItemObtainSourceType obj) => 
                ((Func<ItemObtainSourceType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey84
        {
            internal System.Delegate act;

            internal int <>m__0(ItemObtainSourceType x, ItemObtainSourceType y) => 
                ((Func<ItemObtainSourceType, ItemObtainSourceType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey85
        {
            internal System.Delegate act;

            internal bool <>m__0(ScenePreview_CelestialInfoDesc obj) => 
                ((Func<ScenePreview_CelestialInfoDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey86
        {
            internal System.Delegate act;

            internal int <>m__0(ScenePreview_CelestialInfoDesc x, ScenePreview_CelestialInfoDesc y) => 
                ((Func<ScenePreview_CelestialInfoDesc, ScenePreview_CelestialInfoDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey87
        {
            internal System.Delegate act;

            internal bool <>m__0(ScenePreview_CelestialAutoGenSceneDesc obj) => 
                ((Func<ScenePreview_CelestialAutoGenSceneDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey88
        {
            internal System.Delegate act;

            internal int <>m__0(ScenePreview_CelestialAutoGenSceneDesc x, ScenePreview_CelestialAutoGenSceneDesc y) => 
                ((Func<ScenePreview_CelestialAutoGenSceneDesc, ScenePreview_CelestialAutoGenSceneDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey89
        {
            internal System.Delegate act;

            internal bool <>m__0(ScenePreview_SceneInfoDesc obj) => 
                ((Func<ScenePreview_SceneInfoDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8A
        {
            internal System.Delegate act;

            internal int <>m__0(ScenePreview_SceneInfoDesc x, ScenePreview_SceneInfoDesc y) => 
                ((Func<ScenePreview_SceneInfoDesc, ScenePreview_SceneInfoDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8B
        {
            internal System.Delegate act;

            internal bool <>m__0(KeyValuePair<string, string> obj) => 
                ((Func<KeyValuePair<string, string>, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8C
        {
            internal System.Delegate act;

            internal bool <>m__0(ActivityRewardInfo obj) => 
                ((Func<ActivityRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8D
        {
            internal System.Delegate act;

            internal int <>m__0(ActivityRewardInfo x, ActivityRewardInfo y) => 
                ((Func<ActivityRewardInfo, ActivityRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8E
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestRewardInfo obj) => 
                ((Func<QuestRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey8F
        {
            internal System.Delegate act;

            internal int <>m__0(QuestRewardInfo x, QuestRewardInfo y) => 
                ((Func<QuestRewardInfo, QuestRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9
        {
            internal System.Delegate act;

            internal void <>m__0(Camera cam)
            {
                ((Action<Camera>) this.act)(cam);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey90
        {
            internal System.Delegate act;

            internal bool <>m__0(BattlePassRewardEffect obj) => 
                ((Func<BattlePassRewardEffect, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey91
        {
            internal System.Delegate act;

            internal int <>m__0(BattlePassRewardEffect x, BattlePassRewardEffect y) => 
                ((Func<BattlePassRewardEffect, BattlePassRewardEffect, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey92
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestAcceptCondInfo obj) => 
                ((Func<QuestAcceptCondInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey93
        {
            internal System.Delegate act;

            internal int <>m__0(QuestAcceptCondInfo x, QuestAcceptCondInfo y) => 
                ((Func<QuestAcceptCondInfo, QuestAcceptCondInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey94
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfigParams obj) => 
                ((Func<ConfigParams, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey95
        {
            internal System.Delegate act;

            internal int <>m__0(ConfigParams x, ConfigParams y) => 
                ((Func<ConfigParams, ConfigParams, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey96
        {
            internal System.Delegate act;

            internal bool <>m__0(CharProfessionInfoInitPropertiesBasic obj) => 
                ((Func<CharProfessionInfoInitPropertiesBasic, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey97
        {
            internal System.Delegate act;

            internal int <>m__0(CharProfessionInfoInitPropertiesBasic x, CharProfessionInfoInitPropertiesBasic y) => 
                ((Func<CharProfessionInfoInitPropertiesBasic, CharProfessionInfoInitPropertiesBasic, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey98
        {
            internal System.Delegate act;

            internal bool <>m__0(CharProfessionInfoAutoAddProperties obj) => 
                ((Func<CharProfessionInfoAutoAddProperties, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey99
        {
            internal System.Delegate act;

            internal int <>m__0(CharProfessionInfoAutoAddProperties x, CharProfessionInfoAutoAddProperties y) => 
                ((Func<CharProfessionInfoAutoAddProperties, CharProfessionInfoAutoAddProperties, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9A
        {
            internal System.Delegate act;

            internal bool <>m__0(LoginRewardInfo obj) => 
                ((Func<LoginRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9B
        {
            internal System.Delegate act;

            internal int <>m__0(LoginRewardInfo x, LoginRewardInfo y) => 
                ((Func<LoginRewardInfo, LoginRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9C
        {
            internal System.Delegate act;

            internal bool <>m__0(SignalType obj) => 
                ((Func<SignalType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9D
        {
            internal System.Delegate act;

            internal int <>m__0(SignalType x, SignalType y) => 
                ((Func<SignalType, SignalType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9E
        {
            internal System.Delegate act;

            internal bool <>m__0(DevelopmentCostItemList obj) => 
                ((Func<DevelopmentCostItemList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStorey9F
        {
            internal System.Delegate act;

            internal int <>m__0(DevelopmentCostItemList x, DevelopmentCostItemList y) => 
                ((Func<DevelopmentCostItemList, DevelopmentCostItemList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA
        {
            internal System.Delegate act;

            internal bool <>m__0(ReflectionProbeBlendInfo obj) => 
                ((Func<ReflectionProbeBlendInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA0
        {
            internal System.Delegate act;

            internal bool <>m__0(CostInfo obj) => 
                ((Func<CostInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA1
        {
            internal System.Delegate act;

            internal int <>m__0(CostInfo x, CostInfo y) => 
                ((Func<CostInfo, CostInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA2
        {
            internal System.Delegate act;

            internal bool <>m__0(IntWeightListItem obj) => 
                ((Func<IntWeightListItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA3
        {
            internal System.Delegate act;

            internal int <>m__0(IntWeightListItem x, IntWeightListItem y) => 
                ((Func<IntWeightListItem, IntWeightListItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA4
        {
            internal System.Delegate act;

            internal bool <>m__0(ResultRange obj) => 
                ((Func<ResultRange, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA5
        {
            internal System.Delegate act;

            internal int <>m__0(ResultRange x, ResultRange y) => 
                ((Func<ResultRange, ResultRange, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA6
        {
            internal System.Delegate act;

            internal bool <>m__0(DynamicSceneLocationInfoOrbitRadiusRange obj) => 
                ((Func<DynamicSceneLocationInfoOrbitRadiusRange, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA7
        {
            internal System.Delegate act;

            internal int <>m__0(DynamicSceneLocationInfoOrbitRadiusRange x, DynamicSceneLocationInfoOrbitRadiusRange y) => 
                ((Func<DynamicSceneLocationInfoOrbitRadiusRange, DynamicSceneLocationInfoOrbitRadiusRange, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA8
        {
            internal System.Delegate act;

            internal bool <>m__0(ParamInfo obj) => 
                ((Func<ParamInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyA9
        {
            internal System.Delegate act;

            internal int <>m__0(ParamInfo x, ParamInfo y) => 
                ((Func<ParamInfo, ParamInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAA
        {
            internal System.Delegate act;

            internal bool <>m__0(CreditRewardItemInfo obj) => 
                ((Func<CreditRewardItemInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAB
        {
            internal System.Delegate act;

            internal int <>m__0(CreditRewardItemInfo x, CreditRewardItemInfo y) => 
                ((Func<CreditRewardItemInfo, CreditRewardItemInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAC
        {
            internal System.Delegate act;

            internal bool <>m__0(FactionCreditModifyRelativeInfoFactionFactorList obj) => 
                ((Func<FactionCreditModifyRelativeInfoFactionFactorList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAD
        {
            internal System.Delegate act;

            internal int <>m__0(FactionCreditModifyRelativeInfoFactionFactorList x, FactionCreditModifyRelativeInfoFactionFactorList y) => 
                ((Func<FactionCreditModifyRelativeInfoFactionFactorList, FactionCreditModifyRelativeInfoFactionFactorList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAE
        {
            internal System.Delegate act;

            internal bool <>m__0(CreditRewardInfo obj) => 
                ((Func<CreditRewardInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyAF
        {
            internal System.Delegate act;

            internal int <>m__0(CreditRewardInfo x, CreditRewardInfo y) => 
                ((Func<CreditRewardInfo, CreditRewardInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB
        {
            internal System.Delegate act;

            internal int <>m__0(ReflectionProbeBlendInfo x, ReflectionProbeBlendInfo y) => 
                ((Func<ReflectionProbeBlendInfo, ReflectionProbeBlendInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB0
        {
            internal System.Delegate act;

            internal bool <>m__0(GEBrushInfoGradualLegendColorElementList obj) => 
                ((Func<GEBrushInfoGradualLegendColorElementList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB1
        {
            internal System.Delegate act;

            internal int <>m__0(GEBrushInfoGradualLegendColorElementList x, GEBrushInfoGradualLegendColorElementList y) => 
                ((Func<GEBrushInfoGradualLegendColorElementList, GEBrushInfoGradualLegendColorElementList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB2
        {
            internal System.Delegate act;

            internal bool <>m__0(GEBrushInfoBrushColorElementList obj) => 
                ((Func<GEBrushInfoBrushColorElementList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB3
        {
            internal System.Delegate act;

            internal int <>m__0(GEBrushInfoBrushColorElementList x, GEBrushInfoBrushColorElementList y) => 
                ((Func<GEBrushInfoBrushColorElementList, GEBrushInfoBrushColorElementList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB4
        {
            internal System.Delegate act;

            internal bool <>m__0(GEGrayColorToSolarSystemTemplateMapInfoLevel obj) => 
                ((Func<GEGrayColorToSolarSystemTemplateMapInfoLevel, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB5
        {
            internal System.Delegate act;

            internal int <>m__0(GEGrayColorToSolarSystemTemplateMapInfoLevel x, GEGrayColorToSolarSystemTemplateMapInfoLevel y) => 
                ((Func<GEGrayColorToSolarSystemTemplateMapInfoLevel, GEGrayColorToSolarSystemTemplateMapInfoLevel, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB6
        {
            internal System.Delegate act;

            internal bool <>m__0(GEMoonCountDataInfoOrbitRadiusRange obj) => 
                ((Func<GEMoonCountDataInfoOrbitRadiusRange, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB7
        {
            internal System.Delegate act;

            internal int <>m__0(GEMoonCountDataInfoOrbitRadiusRange x, GEMoonCountDataInfoOrbitRadiusRange y) => 
                ((Func<GEMoonCountDataInfoOrbitRadiusRange, GEMoonCountDataInfoOrbitRadiusRange, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB8
        {
            internal System.Delegate act;

            internal bool <>m__0(GEPlanetDataInfoOrbitRadiusRange obj) => 
                ((Func<GEPlanetDataInfoOrbitRadiusRange, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyB9
        {
            internal System.Delegate act;

            internal int <>m__0(GEPlanetDataInfoOrbitRadiusRange x, GEPlanetDataInfoOrbitRadiusRange y) => 
                ((Func<GEPlanetDataInfoOrbitRadiusRange, GEPlanetDataInfoOrbitRadiusRange, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBA
        {
            internal System.Delegate act;

            internal bool <>m__0(GEPlanetDataInfoDisplayOnStarMapOrbitSpace obj) => 
                ((Func<GEPlanetDataInfoDisplayOnStarMapOrbitSpace, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBB
        {
            internal System.Delegate act;

            internal int <>m__0(GEPlanetDataInfoDisplayOnStarMapOrbitSpace x, GEPlanetDataInfoDisplayOnStarMapOrbitSpace y) => 
                ((Func<GEPlanetDataInfoDisplayOnStarMapOrbitSpace, GEPlanetDataInfoDisplayOnStarMapOrbitSpace, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBC
        {
            internal System.Delegate act;

            internal bool <>m__0(GERockyPlanetTemperatureInfoOrbitRadiusRange obj) => 
                ((Func<GERockyPlanetTemperatureInfoOrbitRadiusRange, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBD
        {
            internal System.Delegate act;

            internal int <>m__0(GERockyPlanetTemperatureInfoOrbitRadiusRange x, GERockyPlanetTemperatureInfoOrbitRadiusRange y) => 
                ((Func<GERockyPlanetTemperatureInfoOrbitRadiusRange, GERockyPlanetTemperatureInfoOrbitRadiusRange, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBE
        {
            internal System.Delegate act;

            internal bool <>m__0(GrandFactionInfoNPCCaptainPropertiesBasicMultiList obj) => 
                ((Func<GrandFactionInfoNPCCaptainPropertiesBasicMultiList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyBF
        {
            internal System.Delegate act;

            internal int <>m__0(GrandFactionInfoNPCCaptainPropertiesBasicMultiList x, GrandFactionInfoNPCCaptainPropertiesBasicMultiList y) => 
                ((Func<GrandFactionInfoNPCCaptainPropertiesBasicMultiList, GrandFactionInfoNPCCaptainPropertiesBasicMultiList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC
        {
            internal System.Delegate act;

            internal void <>m__0(bool arg0)
            {
                ((Action<bool>) this.act)(arg0);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC0
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildActionAdditionRewardInfoRewardItemList obj) => 
                ((Func<GuildActionAdditionRewardInfoRewardItemList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC1
        {
            internal System.Delegate act;

            internal int <>m__0(GuildActionAdditionRewardInfoRewardItemList x, GuildActionAdditionRewardInfoRewardItemList y) => 
                ((Func<GuildActionAdditionRewardInfoRewardItemList, GuildActionAdditionRewardInfoRewardItemList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC2
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipType obj) => 
                ((Func<ShipType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC3
        {
            internal System.Delegate act;

            internal int <>m__0(ShipType x, ShipType y) => 
                ((Func<ShipType, ShipType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC4
        {
            internal System.Delegate act;

            internal bool <>m__0(ConfIdLevelInfo obj) => 
                ((Func<ConfIdLevelInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC5
        {
            internal System.Delegate act;

            internal int <>m__0(ConfIdLevelInfo x, ConfIdLevelInfo y) => 
                ((Func<ConfIdLevelInfo, ConfIdLevelInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC6
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingLevelDetailInfoRecycleItem obj) => 
                ((Func<GuildBuildingLevelDetailInfoRecycleItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC7
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingLevelDetailInfoRecycleItem x, GuildBuildingLevelDetailInfoRecycleItem y) => 
                ((Func<GuildBuildingLevelDetailInfoRecycleItem, GuildBuildingLevelDetailInfoRecycleItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC8
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildBuildingLevelDetailInfoGuildLogicEffectList obj) => 
                ((Func<GuildBuildingLevelDetailInfoGuildLogicEffectList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyC9
        {
            internal System.Delegate act;

            internal int <>m__0(GuildBuildingLevelDetailInfoGuildLogicEffectList x, GuildBuildingLevelDetailInfoGuildLogicEffectList y) => 
                ((Func<GuildBuildingLevelDetailInfoGuildLogicEffectList, GuildBuildingLevelDetailInfoGuildLogicEffectList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCA
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildEvaluateScoreInfoBuildingScoreList obj) => 
                ((Func<GuildEvaluateScoreInfoBuildingScoreList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCB
        {
            internal System.Delegate act;

            internal int <>m__0(GuildEvaluateScoreInfoBuildingScoreList x, GuildEvaluateScoreInfoBuildingScoreList y) => 
                ((Func<GuildEvaluateScoreInfoBuildingScoreList, GuildEvaluateScoreInfoBuildingScoreList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCC
        {
            internal System.Delegate act;

            internal bool <>m__0(CommonFormatStringParamType obj) => 
                ((Func<CommonFormatStringParamType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCD
        {
            internal System.Delegate act;

            internal int <>m__0(CommonFormatStringParamType x, CommonFormatStringParamType y) => 
                ((Func<CommonFormatStringParamType, CommonFormatStringParamType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCE
        {
            internal System.Delegate act;

            internal bool <>m__0(GuildPermission obj) => 
                ((Func<GuildPermission, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyCF
        {
            internal System.Delegate act;

            internal int <>m__0(GuildPermission x, GuildPermission y) => 
                ((Func<GuildPermission, GuildPermission, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD
        {
            internal System.Delegate act;

            internal void <>m__0(float[] dataL, float[] dataR, int numChannels, int numData)
            {
                ((Action<float[], float[], int, int>) this.act)(dataL, dataR, numChannels, numData);
            }
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD0
        {
            internal System.Delegate act;

            internal bool <>m__0(HelloDailogInfo obj) => 
                ((Func<HelloDailogInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD1
        {
            internal System.Delegate act;

            internal int <>m__0(HelloDailogInfo x, HelloDailogInfo y) => 
                ((Func<HelloDailogInfo, HelloDailogInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD2
        {
            internal System.Delegate act;

            internal bool <>m__0(IslandStarGroupInfoIslandInfoList obj) => 
                ((Func<IslandStarGroupInfoIslandInfoList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD3
        {
            internal System.Delegate act;

            internal int <>m__0(IslandStarGroupInfoIslandInfoList x, IslandStarGroupInfoIslandInfoList y) => 
                ((Func<IslandStarGroupInfoIslandInfoList, IslandStarGroupInfoIslandInfoList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD4
        {
            internal System.Delegate act;

            internal bool <>m__0(IslandTypeInfoSolarSystemGuildMinrealOutputInfoList obj) => 
                ((Func<IslandTypeInfoSolarSystemGuildMinrealOutputInfoList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD5
        {
            internal System.Delegate act;

            internal int <>m__0(IslandTypeInfoSolarSystemGuildMinrealOutputInfoList x, IslandTypeInfoSolarSystemGuildMinrealOutputInfoList y) => 
                ((Func<IslandTypeInfoSolarSystemGuildMinrealOutputInfoList, IslandTypeInfoSolarSystemGuildMinrealOutputInfoList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD6
        {
            internal System.Delegate act;

            internal bool <>m__0(ItemDropInfoUIShowItemList obj) => 
                ((Func<ItemDropInfoUIShowItemList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD7
        {
            internal System.Delegate act;

            internal int <>m__0(ItemDropInfoUIShowItemList x, ItemDropInfoUIShowItemList y) => 
                ((Func<ItemDropInfoUIShowItemList, ItemDropInfoUIShowItemList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD8
        {
            internal System.Delegate act;

            internal bool <>m__0(IdLevelWeightItem obj) => 
                ((Func<IdLevelWeightItem, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyD9
        {
            internal System.Delegate act;

            internal int <>m__0(IdLevelWeightItem x, IdLevelWeightItem y) => 
                ((Func<IdLevelWeightItem, IdLevelWeightItem, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDA
        {
            internal System.Delegate act;

            internal bool <>m__0(MonthlyCardPriviledgeInfo obj) => 
                ((Func<MonthlyCardPriviledgeInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDB
        {
            internal System.Delegate act;

            internal int <>m__0(MonthlyCardPriviledgeInfo x, MonthlyCardPriviledgeInfo y) => 
                ((Func<MonthlyCardPriviledgeInfo, MonthlyCardPriviledgeInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDC
        {
            internal System.Delegate act;

            internal bool <>m__0(ShipCompListConfInfo obj) => 
                ((Func<ShipCompListConfInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDD
        {
            internal System.Delegate act;

            internal int <>m__0(ShipCompListConfInfo x, ShipCompListConfInfo y) => 
                ((Func<ShipCompListConfInfo, ShipCompListConfInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDE
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcInteractionTemplateContinuousEffectInfo obj) => 
                ((Func<NpcInteractionTemplateContinuousEffectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyDF
        {
            internal System.Delegate act;

            internal int <>m__0(NpcInteractionTemplateContinuousEffectInfo x, NpcInteractionTemplateContinuousEffectInfo y) => 
                ((Func<NpcInteractionTemplateContinuousEffectInfo, NpcInteractionTemplateContinuousEffectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE
        {
            internal System.Delegate act;

            internal bool <>m__0(CRIDesc.SheetDesc obj) => 
                ((Func<CRIDesc.SheetDesc, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE0
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcInteractionTemplateOnceEffectInfo obj) => 
                ((Func<NpcInteractionTemplateOnceEffectInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE1
        {
            internal System.Delegate act;

            internal int <>m__0(NpcInteractionTemplateOnceEffectInfo x, NpcInteractionTemplateOnceEffectInfo y) => 
                ((Func<NpcInteractionTemplateOnceEffectInfo, NpcInteractionTemplateOnceEffectInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE2
        {
            internal System.Delegate act;

            internal bool <>m__0(HelloDialogIdByCompletedMainQuestInfo obj) => 
                ((Func<HelloDialogIdByCompletedMainQuestInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE3
        {
            internal System.Delegate act;

            internal int <>m__0(HelloDialogIdByCompletedMainQuestInfo x, HelloDialogIdByCompletedMainQuestInfo y) => 
                ((Func<HelloDialogIdByCompletedMainQuestInfo, HelloDialogIdByCompletedMainQuestInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE4
        {
            internal System.Delegate act;

            internal bool <>m__0(SubRankType obj) => 
                ((Func<SubRankType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE5
        {
            internal System.Delegate act;

            internal int <>m__0(SubRankType x, SubRankType y) => 
                ((Func<SubRankType, SubRankType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE6
        {
            internal System.Delegate act;

            internal bool <>m__0(GrandFaction obj) => 
                ((Func<GrandFaction, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE7
        {
            internal System.Delegate act;

            internal int <>m__0(GrandFaction x, GrandFaction y) => 
                ((Func<GrandFaction, GrandFaction, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE8
        {
            internal System.Delegate act;

            internal bool <>m__0(ProfessionType obj) => 
                ((Func<ProfessionType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyE9
        {
            internal System.Delegate act;

            internal int <>m__0(ProfessionType x, ProfessionType y) => 
                ((Func<ProfessionType, ProfessionType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyEA
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcFunctionType obj) => 
                ((Func<NpcFunctionType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyEB
        {
            internal System.Delegate act;

            internal int <>m__0(NpcFunctionType x, NpcFunctionType y) => 
                ((Func<NpcFunctionType, NpcFunctionType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyEC
        {
            internal System.Delegate act;

            internal bool <>m__0(NpcPersonalityType obj) => 
                ((Func<NpcPersonalityType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyED
        {
            internal System.Delegate act;

            internal int <>m__0(NpcPersonalityType x, NpcPersonalityType y) => 
                ((Func<NpcPersonalityType, NpcPersonalityType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyEE
        {
            internal System.Delegate act;

            internal bool <>m__0(PreDefineNpcCaptainStaticInfoInitFeats obj) => 
                ((Func<PreDefineNpcCaptainStaticInfoInitFeats, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyEF
        {
            internal System.Delegate act;

            internal int <>m__0(PreDefineNpcCaptainStaticInfoInitFeats x, PreDefineNpcCaptainStaticInfoInitFeats y) => 
                ((Func<PreDefineNpcCaptainStaticInfoInitFeats, PreDefineNpcCaptainStaticInfoInitFeats, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF
        {
            internal System.Delegate act;

            internal int <>m__0(CRIDesc.SheetDesc x, CRIDesc.SheetDesc y) => 
                ((Func<CRIDesc.SheetDesc, CRIDesc.SheetDesc, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF0
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestEnvirmentInstanceInfoSolarSystemIdList obj) => 
                ((Func<QuestEnvirmentInstanceInfoSolarSystemIdList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF1
        {
            internal System.Delegate act;

            internal int <>m__0(QuestEnvirmentInstanceInfoSolarSystemIdList x, QuestEnvirmentInstanceInfoSolarSystemIdList y) => 
                ((Func<QuestEnvirmentInstanceInfoSolarSystemIdList, QuestEnvirmentInstanceInfoSolarSystemIdList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF2
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestEnvirmentInstanceInfoNpcList obj) => 
                ((Func<QuestEnvirmentInstanceInfoNpcList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF3
        {
            internal System.Delegate act;

            internal int <>m__0(QuestEnvirmentInstanceInfoNpcList x, QuestEnvirmentInstanceInfoNpcList y) => 
                ((Func<QuestEnvirmentInstanceInfoNpcList, QuestEnvirmentInstanceInfoNpcList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF4
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestEnvirmentInstanceInfoCustomStringList obj) => 
                ((Func<QuestEnvirmentInstanceInfoCustomStringList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF5
        {
            internal System.Delegate act;

            internal int <>m__0(QuestEnvirmentInstanceInfoCustomStringList x, QuestEnvirmentInstanceInfoCustomStringList y) => 
                ((Func<QuestEnvirmentInstanceInfoCustomStringList, QuestEnvirmentInstanceInfoCustomStringList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF6
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestEnvirmentInstanceInfoMonsterSelectList obj) => 
                ((Func<QuestEnvirmentInstanceInfoMonsterSelectList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF7
        {
            internal System.Delegate act;

            internal int <>m__0(QuestEnvirmentInstanceInfoMonsterSelectList x, QuestEnvirmentInstanceInfoMonsterSelectList y) => 
                ((Func<QuestEnvirmentInstanceInfoMonsterSelectList, QuestEnvirmentInstanceInfoMonsterSelectList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF8
        {
            internal System.Delegate act;

            internal bool <>m__0(QuestGrandFactionRewardInfoRewardList obj) => 
                ((Func<QuestGrandFactionRewardInfoRewardList, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyF9
        {
            internal System.Delegate act;

            internal int <>m__0(QuestGrandFactionRewardInfoRewardList x, QuestGrandFactionRewardInfoRewardList y) => 
                ((Func<QuestGrandFactionRewardInfoRewardList, QuestGrandFactionRewardInfoRewardList, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFA
        {
            internal System.Delegate act;

            internal bool <>m__0(SceneCameraAnimationClipFilterType obj) => 
                ((Func<SceneCameraAnimationClipFilterType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFB
        {
            internal System.Delegate act;

            internal int <>m__0(SceneCameraAnimationClipFilterType x, SceneCameraAnimationClipFilterType y) => 
                ((Func<SceneCameraAnimationClipFilterType, SceneCameraAnimationClipFilterType, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFC
        {
            internal System.Delegate act;

            internal bool <>m__0(CameraAnimationInfo obj) => 
                ((Func<CameraAnimationInfo, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFD
        {
            internal System.Delegate act;

            internal int <>m__0(CameraAnimationInfo x, CameraAnimationInfo y) => 
                ((Func<CameraAnimationInfo, CameraAnimationInfo, int>) this.act)(x, y);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFE
        {
            internal System.Delegate act;

            internal bool <>m__0(RationalEquipType obj) => 
                ((Func<RationalEquipType, bool>) this.act)(obj);
        }

        [CompilerGenerated]
        private sealed class <RegisterDelegateConvertor>c__AnonStoreyFF
        {
            internal System.Delegate act;

            internal int <>m__0(RationalEquipType x, RationalEquipType y) => 
                ((Func<RationalEquipType, RationalEquipType, int>) this.act)(x, y);
        }
    }
}

