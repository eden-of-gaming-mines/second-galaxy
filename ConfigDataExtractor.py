import os
from UnityPy import AssetsManager
from settings import ROOT, ASSET_PATH, CONFIG_DATA_PATH

src = ASSET_PATH
path = ROOT
dst = os.path.join(CONFIG_DATA_PATH, 'raw')

os.makedirs(dst, exist_ok=True)

for fp in os.listdir(src):
    if not 'configdata' in fp:
        continue
    a = AssetsManager()
    a.load_file(os.path.join(src, fp))
    for asset in a.assets.values():
        for obj in asset.objects.values():
            if not obj.type == "MonoBehaviour":
                continue
            _data = obj.read()
            name = getattr(_data, 'name', None)
            print(name)
            if name:
                dst_fp = os.path.join(dst, name)
                os.makedirs(os.path.dirname(dst_fp), exist_ok=True)
                with open(dst_fp, 'wb') as f:
                    f.write(bytes(_data.read_typetree()['m_bytes']))
