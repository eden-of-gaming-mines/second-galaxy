import os
import json
from google.protobuf.json_format import MessageToDict

import importlib.util

import os,json

from settings import CONFIG_DATA_PATH, PROTOPY_PATH, PROTO_PATH


def main():
    src = os.path.join(CONFIG_DATA_PATH,'raw')
    dst = CONFIG_DATA_PATH
    
    
    for fp in os.listdir(src):
        ConvertConfigData(
            os.path.join(src, fp),
            os.path.join(dst, fp.lstrip('ConfigData')+'.json'),
            PROTOPY_PATH
        )

    
    # fix ConfigDataConfigableConst
    configuable_const={
        int(i):val[18:]
        for i,val in json.loads(open(os.path.join(PROTO_PATH, 'enums.json'),'rb').read())['ConfigableConstId'].items()
    }
        
    buffer=getBuffer('ConfigableConst',os.path.join(PROTOPY_PATH,'ConfigDataConfigableConst_pb2.py'))
    if not buffer:
        print('Missing decoder for :',name)
        return False
    
    data=buffer()
    try:
        fp = os.path.join(CONFIG_DATA_PATH, 'raw', 'ConfigDataConfigableConst')
        data.ParseFromString(open(fp,'rb').read())
        data=MessageToDict(data)['items']
        for entry in data:
            if entry['ID'] in configuable_const:
                entry['iname'] = configuable_const[entry['ID']]
    except:
        pass
    open(os.path.join(CONFIG_DATA_PATH,'ConfigableConst.json'),'wb').write(json.dumps(data,ensure_ascii=False,indent='\t').encode('utf8'))


def getBuffer(name,path):
    try:
        spec = importlib.util.spec_from_file_location(name, path)
        foo = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(foo)
        return getattr(foo, 'Items', False)
    except:
        return False

def ConvertConfigData(p_raw,p_fin,protopy_path):
    name=os.path.basename(p_raw).rsplit('.',1)[0]
        
    buffer=getBuffer(name,os.path.join(protopy_path,f'{name}_pb2.py'))
    if not buffer:
        print('Missing decoder for :',name)
        return False
    
    data=buffer()
    try:
        data.ParseFromString(open(p_raw,'rb').read())
        data=MessageToDict(data)
        if 'items' in data:
            data=data['items']
        else:
            print(p_raw,data)
        if isinstance(data, (dict, list)):
            open(p_fin,'wb').write(json.dumps(data,indent='\t',ensure_ascii=False).encode('utf8'))
        else:
            print()
        #print('\\/',os.path.basename(p_fin))
    except Exception as e:
        print('Failed to decode',name)

if __name__ == "__main__":
    main()