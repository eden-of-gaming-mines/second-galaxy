# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataConfigableConst.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataConfigableConst.proto',
  package='ConfigDataConfigableConst',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1f\x43onfigDataConfigableConst.proto\x12\x19\x43onfigDataConfigableConst\"\x8a\x01\n\x19\x43onfigDataConfigableConst\x12\n\n\x02ID\x18\x02 \x02(\x05\x12R\n\x18\x43onfigableConstEnumValue\x18\x03 \x02(\x0e\x32\x30.ConfigDataConfigableConst.ENUMConfigableConstId\x12\r\n\x05Value\x18\x05 \x02(\x05\"L\n\x05Items\x12\x43\n\x05items\x18\x01 \x03(\x0b\x32\x34.ConfigDataConfigableConst.ConfigDataConfigableConst*\xb3\x11\n\x15\x45NUMConfigableConstId\x12)\n%ConfigableConstId_ShieldRecoverPeriod\x10\x01\x12)\n%ConfigableConstId_EnergyRecoverPeriod\x10\x02\x12*\n&ConfigableConstId_ShieldRecoverPercent\x10\x03\x12*\n&ConfigableConstId_EnergyRecoverPercent\x10\x04\x12)\n%ConfigableConstId_ShieldFailurePeriod\x10\x05\x12-\n)ConfigableConstId_FightStateCheckDistance\x10\x06\x12$\n ConfigableConstId_BulletLifeTime\x10\x07\x12(\n$ConfigableConstId_DroneBulletFlyTime\x10\x08\x12\x34\n0ConfigableConstId_DistanceToBuildingAfterJumping\x10\t\x12\x36\n2ConfigableConstId_CelestialJumpingStopDistanceMult\x10\x0c\x12\x33\n/ConfigableConstId_BurstDamagePercentForBadCombo\x10\r\x12,\n(ConfigableConstId_DefenceDroneFormupTime\x10\x0e\x12+\n\'ConfigableConstId_AttackDroneFormupTime\x10\x0f\x12\x34\n0ConfigableConstId_BurstGoodTimeStartRatioPercent\x10\x14\x12-\n)ConfigableConstId_ShipSpeedMaxModifyMulti\x10\x1e\x12&\n\"ConfigableConstId_ShipFireRangeMax\x10(\x12/\n+ConfigableConstId_NPCTalkerChatDurationTime\x10\x32\x12:\n6ConfigableConstId_MotherShipSingleSolarSystemSpainTime\x10<\x12-\n)ConfigableConstId_CaptainExpAddItemHighId\x10\x46\x12*\n&ConfigableConstId_PVPScanProbeCountMax\x10P\x12\'\n#ConfigableConstId_FleetShipCountMax\x10Z\x12-\n)ConfigableConstId_CriminalHunterCountDown\x10\x64\x12\x30\n,ConfigableConstId_InfectSelfHealingAlertTime\x10n\x12;\n7ConfigableConstId_CharactorAttackPropertyGainMultiValue\x10x\x12(\n#ConfigableConstId_ShipNameMaxLength\x10\x82\x01\x12I\nDConfigableConstId_QuestPoolCountReduceInSolarSystemDifficultLevelMin\x10\x8c\x01\x12\x35\n0ConfigableConstId_PlayerAuctionItemOrderCountMax\x10\x96\x01\x12.\n)ConfigableConstId_PuzzleGamLastAssistCost\x10\xa0\x01\x12.\n)ConfigableConstId_GuildDismissWaitingTime\x10\xaa\x01\x12.\n)ConfigableConstId_ManualSignalShiftFactor\x10\xb4\x01\x12H\nCConfigableConstId_PlayerHostileBehaviorCountDownForLeaveSolarSystem\x10\xbe\x01\x12\x39\n4ConfigableConstId_GuildProductionMidLevelSpeedUpCost\x10\xc8\x01\x12\x32\n-ConfigableConstId_GuildCompensationExpireDays\x10\xd2\x01\x12\x36\n1ConfigableConstId_GuildSentryThreatValuePVPAttack\x10\xdc\x01\x12\x32\n-ConfigableConstId_GuildProductionDefaultCount\x10\xe6\x01\x12:\n5ConfigableConstId_GuildMiningBalancingTimeForOneRound\x10\xf0\x01\x12\x32\n-ConfigableConstId_GuildAnnoucementLengthLimit\x10\xfa\x01\x12\x34\n/ConfigableConstId_AllianceRegionChangeCDMinutes\x10\x84\x02\x12\x30\n+ConfigableConstId_BattlePassUpgradeExpBonus\x10\x98\x02\x12;\n6ConfigableConstId_GuildTradeMoneyDonateGuildGalaFactor\x10\xa2\x02\x12\x45\n@ConfigableConstId_RechargeMonthlyCardNearExpireNotificationHours\x10\xac\x02\x12\x37\n2ConfigableConstId_GuildFlagShipOffLineAutoPackTime\x10\xb6\x02\x12\x32\n-ConfigableConstId_GuildCompensationCountLimit\x10\xc0\x02')
)

_ENUMCONFIGABLECONSTID = _descriptor.EnumDescriptor(
  name='ENUMConfigableConstId',
  full_name='ConfigDataConfigableConst.ENUMConfigableConstId',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShieldRecoverPeriod', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_EnergyRecoverPeriod', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShieldRecoverPercent', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_EnergyRecoverPercent', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShieldFailurePeriod', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_FightStateCheckDistance', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_BulletLifeTime', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_DroneBulletFlyTime', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_DistanceToBuildingAfterJumping', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_CelestialJumpingStopDistanceMult', index=9, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_BurstDamagePercentForBadCombo', index=10, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_DefenceDroneFormupTime', index=11, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_AttackDroneFormupTime', index=12, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_BurstGoodTimeStartRatioPercent', index=13, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShipSpeedMaxModifyMulti', index=14, number=30,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShipFireRangeMax', index=15, number=40,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_NPCTalkerChatDurationTime', index=16, number=50,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_MotherShipSingleSolarSystemSpainTime', index=17, number=60,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_CaptainExpAddItemHighId', index=18, number=70,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_PVPScanProbeCountMax', index=19, number=80,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_FleetShipCountMax', index=20, number=90,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_CriminalHunterCountDown', index=21, number=100,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_InfectSelfHealingAlertTime', index=22, number=110,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_CharactorAttackPropertyGainMultiValue', index=23, number=120,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ShipNameMaxLength', index=24, number=130,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_QuestPoolCountReduceInSolarSystemDifficultLevelMin', index=25, number=140,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_PlayerAuctionItemOrderCountMax', index=26, number=150,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_PuzzleGamLastAssistCost', index=27, number=160,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildDismissWaitingTime', index=28, number=170,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_ManualSignalShiftFactor', index=29, number=180,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_PlayerHostileBehaviorCountDownForLeaveSolarSystem', index=30, number=190,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildProductionMidLevelSpeedUpCost', index=31, number=200,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildCompensationExpireDays', index=32, number=210,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildSentryThreatValuePVPAttack', index=33, number=220,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildProductionDefaultCount', index=34, number=230,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildMiningBalancingTimeForOneRound', index=35, number=240,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildAnnoucementLengthLimit', index=36, number=250,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_AllianceRegionChangeCDMinutes', index=37, number=260,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_BattlePassUpgradeExpBonus', index=38, number=280,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildTradeMoneyDonateGuildGalaFactor', index=39, number=290,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_RechargeMonthlyCardNearExpireNotificationHours', index=40, number=300,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildFlagShipOffLineAutoPackTime', index=41, number=310,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ConfigableConstId_GuildCompensationCountLimit', index=42, number=320,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=282,
  serialized_end=2509,
)
_sym_db.RegisterEnumDescriptor(_ENUMCONFIGABLECONSTID)

ENUMConfigableConstId = enum_type_wrapper.EnumTypeWrapper(_ENUMCONFIGABLECONSTID)
ConfigableConstId_ShieldRecoverPeriod = 1
ConfigableConstId_EnergyRecoverPeriod = 2
ConfigableConstId_ShieldRecoverPercent = 3
ConfigableConstId_EnergyRecoverPercent = 4
ConfigableConstId_ShieldFailurePeriod = 5
ConfigableConstId_FightStateCheckDistance = 6
ConfigableConstId_BulletLifeTime = 7
ConfigableConstId_DroneBulletFlyTime = 8
ConfigableConstId_DistanceToBuildingAfterJumping = 9
ConfigableConstId_CelestialJumpingStopDistanceMult = 12
ConfigableConstId_BurstDamagePercentForBadCombo = 13
ConfigableConstId_DefenceDroneFormupTime = 14
ConfigableConstId_AttackDroneFormupTime = 15
ConfigableConstId_BurstGoodTimeStartRatioPercent = 20
ConfigableConstId_ShipSpeedMaxModifyMulti = 30
ConfigableConstId_ShipFireRangeMax = 40
ConfigableConstId_NPCTalkerChatDurationTime = 50
ConfigableConstId_MotherShipSingleSolarSystemSpainTime = 60
ConfigableConstId_CaptainExpAddItemHighId = 70
ConfigableConstId_PVPScanProbeCountMax = 80
ConfigableConstId_FleetShipCountMax = 90
ConfigableConstId_CriminalHunterCountDown = 100
ConfigableConstId_InfectSelfHealingAlertTime = 110
ConfigableConstId_CharactorAttackPropertyGainMultiValue = 120
ConfigableConstId_ShipNameMaxLength = 130
ConfigableConstId_QuestPoolCountReduceInSolarSystemDifficultLevelMin = 140
ConfigableConstId_PlayerAuctionItemOrderCountMax = 150
ConfigableConstId_PuzzleGamLastAssistCost = 160
ConfigableConstId_GuildDismissWaitingTime = 170
ConfigableConstId_ManualSignalShiftFactor = 180
ConfigableConstId_PlayerHostileBehaviorCountDownForLeaveSolarSystem = 190
ConfigableConstId_GuildProductionMidLevelSpeedUpCost = 200
ConfigableConstId_GuildCompensationExpireDays = 210
ConfigableConstId_GuildSentryThreatValuePVPAttack = 220
ConfigableConstId_GuildProductionDefaultCount = 230
ConfigableConstId_GuildMiningBalancingTimeForOneRound = 240
ConfigableConstId_GuildAnnoucementLengthLimit = 250
ConfigableConstId_AllianceRegionChangeCDMinutes = 260
ConfigableConstId_BattlePassUpgradeExpBonus = 280
ConfigableConstId_GuildTradeMoneyDonateGuildGalaFactor = 290
ConfigableConstId_RechargeMonthlyCardNearExpireNotificationHours = 300
ConfigableConstId_GuildFlagShipOffLineAutoPackTime = 310
ConfigableConstId_GuildCompensationCountLimit = 320



_CONFIGDATACONFIGABLECONST = _descriptor.Descriptor(
  name='ConfigDataConfigableConst',
  full_name='ConfigDataConfigableConst.ConfigDataConfigableConst',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataConfigableConst.ConfigDataConfigableConst.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ConfigableConstEnumValue', full_name='ConfigDataConfigableConst.ConfigDataConfigableConst.ConfigableConstEnumValue', index=1,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Value', full_name='ConfigDataConfigableConst.ConfigDataConfigableConst.Value', index=2,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=63,
  serialized_end=201,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataConfigableConst.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataConfigableConst.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=203,
  serialized_end=279,
)

_CONFIGDATACONFIGABLECONST.fields_by_name['ConfigableConstEnumValue'].enum_type = _ENUMCONFIGABLECONSTID
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATACONFIGABLECONST
DESCRIPTOR.message_types_by_name['ConfigDataConfigableConst'] = _CONFIGDATACONFIGABLECONST
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMConfigableConstId'] = _ENUMCONFIGABLECONSTID
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataConfigableConst = _reflection.GeneratedProtocolMessageType('ConfigDataConfigableConst', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATACONFIGABLECONST,
  '__module__' : 'ConfigDataConfigableConst_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataConfigableConst.ConfigDataConfigableConst)
  })
_sym_db.RegisterMessage(ConfigDataConfigableConst)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataConfigableConst_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataConfigableConst.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
