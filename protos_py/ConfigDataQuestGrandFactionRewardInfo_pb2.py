# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataQuestGrandFactionRewardInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataQuestGrandFactionRewardInfo.proto',
  package='ConfigDataQuestGrandFactionRewardInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n+ConfigDataQuestGrandFactionRewardInfo.proto\x12%ConfigDataQuestGrandFactionRewardInfo\"\xe8\x01\n(SUBQuestGrandFactionRewardInfoRewardList\x12M\n\x0cGrandFaction\x18\x01 \x02(\x0e\x32\x37.ConfigDataQuestGrandFactionRewardInfo.ENUMGrandFaction\x12J\n\x08ItemType\x18\x02 \x02(\x0e\x32\x38.ConfigDataQuestGrandFactionRewardInfo.ENUMStoreItemType\x12\x0e\n\x06ItemId\x18\x03 \x02(\x05\x12\x11\n\tItemCount\x18\x04 \x02(\x05\"\x98\x01\n%ConfigDataQuestGrandFactionRewardInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x63\n\nRewardList\x18\x03 \x03(\x0b\x32O.ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList\"d\n\x05Items\x12[\n\x05items\x18\x01 \x03(\x0b\x32L.ConfigDataQuestGrandFactionRewardInfo.ConfigDataQuestGrandFactionRewardInfo*\xd9\x01\n\x10\x45NUMGrandFaction\x12\x18\n\x14GrandFaction_Invalid\x10\x00\x12\x14\n\x10GrandFaction_ECD\x10\x01\x12\x14\n\x10GrandFaction_NEF\x10\x02\x12\x13\n\x0fGrandFaction_RS\x10\x03\x12\x13\n\x0fGrandFaction_OE\x10\x04\x12\x14\n\x10GrandFaction_USG\x10\x05\x12\x14\n\x10GrandFaction_CIV\x10\x06\x12\x13\n\x0fGrandFaction_TT\x10\x07\x12\x14\n\x10GrandFaction_Max\x10\x08*\xc3\x03\n\x11\x45NUMStoreItemType\x12\x18\n\x14StoreItemType_Normal\x10\x01\x12\x18\n\x14StoreItemType_Weapon\x10\x02\x12\x17\n\x13StoreItemType_Equip\x10\x03\x12\x16\n\x12StoreItemType_Ship\x10\x04\x12\x1c\n\x18StoreItemType_NormalAmmo\x10\x05\x12\x1d\n\x19StoreItemType_MissileAmmo\x10\x06\x12\x1b\n\x17StoreItemType_DroneAmmo\x10\x07\x12\x1a\n\x16StoreItemType_Currency\x10\x08\x12\x19\n\x15StoreItemType_Mineral\x10\t\x12\x16\n\x12StoreItemType_Chip\x10\n\x12\x1b\n\x17StoreItemType_FeatsBook\x10\x0b\x12\"\n\x1eStoreItemType_ProduceBlueprint\x10\x0c\x12\x1a\n\x16StoreItemType_CrackBox\x10\r\x12\x1b\n\x17StoreItemType_QuestItem\x10\x0e\x12&\n\"StoreItemType_GuildProduceTemplete\x10\x0f')
)

_ENUMGRANDFACTION = _descriptor.EnumDescriptor(
  name='ENUMGrandFaction',
  full_name='ConfigDataQuestGrandFactionRewardInfo.ENUMGrandFaction',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_Invalid', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_ECD', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_NEF', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_RS', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_OE', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_USG', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_CIV', index=6, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_TT', index=7, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GrandFaction_Max', index=8, number=8,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=579,
  serialized_end=796,
)
_sym_db.RegisterEnumDescriptor(_ENUMGRANDFACTION)

ENUMGrandFaction = enum_type_wrapper.EnumTypeWrapper(_ENUMGRANDFACTION)
_ENUMSTOREITEMTYPE = _descriptor.EnumDescriptor(
  name='ENUMStoreItemType',
  full_name='ConfigDataQuestGrandFactionRewardInfo.ENUMStoreItemType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Normal', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Weapon', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Equip', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Ship', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_NormalAmmo', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_MissileAmmo', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_DroneAmmo', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Currency', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Mineral', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_Chip', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_FeatsBook', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_ProduceBlueprint', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_CrackBox', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_QuestItem', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StoreItemType_GuildProduceTemplete', index=14, number=15,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=799,
  serialized_end=1250,
)
_sym_db.RegisterEnumDescriptor(_ENUMSTOREITEMTYPE)

ENUMStoreItemType = enum_type_wrapper.EnumTypeWrapper(_ENUMSTOREITEMTYPE)
GrandFaction_Invalid = 0
GrandFaction_ECD = 1
GrandFaction_NEF = 2
GrandFaction_RS = 3
GrandFaction_OE = 4
GrandFaction_USG = 5
GrandFaction_CIV = 6
GrandFaction_TT = 7
GrandFaction_Max = 8
StoreItemType_Normal = 1
StoreItemType_Weapon = 2
StoreItemType_Equip = 3
StoreItemType_Ship = 4
StoreItemType_NormalAmmo = 5
StoreItemType_MissileAmmo = 6
StoreItemType_DroneAmmo = 7
StoreItemType_Currency = 8
StoreItemType_Mineral = 9
StoreItemType_Chip = 10
StoreItemType_FeatsBook = 11
StoreItemType_ProduceBlueprint = 12
StoreItemType_CrackBox = 13
StoreItemType_QuestItem = 14
StoreItemType_GuildProduceTemplete = 15



_SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST = _descriptor.Descriptor(
  name='SUBQuestGrandFactionRewardInfoRewardList',
  full_name='ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='GrandFaction', full_name='ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList.GrandFaction', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ItemType', full_name='ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList.ItemType', index=1,
      number=2, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ItemId', full_name='ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList.ItemId', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ItemCount', full_name='ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList.ItemCount', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=87,
  serialized_end=319,
)


_CONFIGDATAQUESTGRANDFACTIONREWARDINFO = _descriptor.Descriptor(
  name='ConfigDataQuestGrandFactionRewardInfo',
  full_name='ConfigDataQuestGrandFactionRewardInfo.ConfigDataQuestGrandFactionRewardInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataQuestGrandFactionRewardInfo.ConfigDataQuestGrandFactionRewardInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RewardList', full_name='ConfigDataQuestGrandFactionRewardInfo.ConfigDataQuestGrandFactionRewardInfo.RewardList', index=1,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=322,
  serialized_end=474,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataQuestGrandFactionRewardInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataQuestGrandFactionRewardInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=476,
  serialized_end=576,
)

_SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST.fields_by_name['GrandFaction'].enum_type = _ENUMGRANDFACTION
_SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST.fields_by_name['ItemType'].enum_type = _ENUMSTOREITEMTYPE
_CONFIGDATAQUESTGRANDFACTIONREWARDINFO.fields_by_name['RewardList'].message_type = _SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAQUESTGRANDFACTIONREWARDINFO
DESCRIPTOR.message_types_by_name['SUBQuestGrandFactionRewardInfoRewardList'] = _SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST
DESCRIPTOR.message_types_by_name['ConfigDataQuestGrandFactionRewardInfo'] = _CONFIGDATAQUESTGRANDFACTIONREWARDINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMGrandFaction'] = _ENUMGRANDFACTION
DESCRIPTOR.enum_types_by_name['ENUMStoreItemType'] = _ENUMSTOREITEMTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBQuestGrandFactionRewardInfoRewardList = _reflection.GeneratedProtocolMessageType('SUBQuestGrandFactionRewardInfoRewardList', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTGRANDFACTIONREWARDINFOREWARDLIST,
  '__module__' : 'ConfigDataQuestGrandFactionRewardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestGrandFactionRewardInfo.SUBQuestGrandFactionRewardInfoRewardList)
  })
_sym_db.RegisterMessage(SUBQuestGrandFactionRewardInfoRewardList)

ConfigDataQuestGrandFactionRewardInfo = _reflection.GeneratedProtocolMessageType('ConfigDataQuestGrandFactionRewardInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAQUESTGRANDFACTIONREWARDINFO,
  '__module__' : 'ConfigDataQuestGrandFactionRewardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestGrandFactionRewardInfo.ConfigDataQuestGrandFactionRewardInfo)
  })
_sym_db.RegisterMessage(ConfigDataQuestGrandFactionRewardInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataQuestGrandFactionRewardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestGrandFactionRewardInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
