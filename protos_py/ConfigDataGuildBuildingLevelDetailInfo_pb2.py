# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGuildBuildingLevelDetailInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGuildBuildingLevelDetailInfo.proto',
  package='ConfigDataGuildBuildingLevelDetailInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n,ConfigDataGuildBuildingLevelDetailInfo.proto\x12&ConfigDataGuildBuildingLevelDetailInfo\"\x9d\x01\n3SUBGuildBuildingLevelDetailInfoGuildLogicEffectList\x12N\n\x04type\x18\x01 \x02(\x0e\x32@.ConfigDataGuildBuildingLevelDetailInfo.ENUMGuildLogicEffectType\x12\n\n\x02P1\x18\x02 \x02(\x05\x12\n\n\x02P2\x18\x03 \x02(\x02\"/\n\x12SUBConfIdLevelInfo\x12\n\n\x02Id\x18\x01 \x02(\x05\x12\r\n\x05Level\x18\x02 \x02(\x05\"O\n*SUBGuildBuildingLevelDetailInfoRecycleItem\x12\x0e\n\x06itemId\x18\x01 \x02(\x05\x12\x11\n\titemCount\x18\x02 \x02(\x05\"\xea\x04\n&ConfigDataGuildBuildingLevelDetailInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\r\n\x05Level\x18\x03 \x02(\x05\x12\x0c\n\x04\x44\x65sc\x18\x04 \x02(\t\x12 \n\x18SolarSystemFlourishLevel\x18\x05 \x02(\x05\x12S\n\x0f\x42uildingRequire\x18\x06 \x03(\x0b\x32:.ConfigDataGuildBuildingLevelDetailInfo.SUBConfIdLevelInfo\x12\x14\n\x0c\x44\x65ployItemId\x18\x07 \x02(\x05\x12\x12\n\nDeployTime\x18\x08 \x02(\x05\x12\x13\n\x0bRecycleTime\x18\t \x02(\x05\x12g\n\x0bRecycleItem\x18\n \x03(\x0b\x32R.ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoRecycleItem\x12\x14\n\x0cNpcMonsterId\x18\x0b \x02(\x05\x12\x1b\n\x13NpcMonsterShieldMax\x18\x0c \x02(\x05\x12\x1a\n\x12NpcMonsterArmorMax\x18\r \x02(\x05\x12y\n\x14GuildLogicEffectList\x18\x0e \x03(\x0b\x32[.ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList\x12\x1a\n\x12InvadeNpcMonsterId\x18\x0f \x02(\x05\x12\x12\n\nDescStrKey\x18\x14 \x02(\t\"f\n\x05Items\x12]\n\x05items\x18\x01 \x03(\x0b\x32N.ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo*\xb6\x02\n\x18\x45NUMGuildLogicEffectType\x12\x1d\n\x19GuildLogicEffectType_None\x10\x00\x12(\n$GuildLogicEffectType_GuildProperties\x10\x01\x12\"\n\x1eGuildLogicEffectType_GuildFlag\x10\x02\x12+\n\'GuildLogicEffectType_SolarSystemShipBuf\x10\x03\x12\x31\n-GuildLogicEffectType_SolarSystemMiningShipBuf\x10\x04\x12/\n+GuildLogicEffectType_SolarSystemFlagShipBuf\x10\x05\x12\x1c\n\x18GuildLogicEffectType_Max\x10\x06')
)

_ENUMGUILDLOGICEFFECTTYPE = _descriptor.EnumDescriptor(
  name='ENUMGuildLogicEffectType',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.ENUMGuildLogicEffectType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_GuildProperties', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_GuildFlag', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_SolarSystemShipBuf', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_SolarSystemMiningShipBuf', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_SolarSystemFlagShipBuf', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildLogicEffectType_Max', index=6, number=6,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1104,
  serialized_end=1414,
)
_sym_db.RegisterEnumDescriptor(_ENUMGUILDLOGICEFFECTTYPE)

ENUMGuildLogicEffectType = enum_type_wrapper.EnumTypeWrapper(_ENUMGUILDLOGICEFFECTTYPE)
GuildLogicEffectType_None = 0
GuildLogicEffectType_GuildProperties = 1
GuildLogicEffectType_GuildFlag = 2
GuildLogicEffectType_SolarSystemShipBuf = 3
GuildLogicEffectType_SolarSystemMiningShipBuf = 4
GuildLogicEffectType_SolarSystemFlagShipBuf = 5
GuildLogicEffectType_Max = 6



_SUBGUILDBUILDINGLEVELDETAILINFOGUILDLOGICEFFECTLIST = _descriptor.Descriptor(
  name='SUBGuildBuildingLevelDetailInfoGuildLogicEffectList',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='type', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList.type', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P1', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList.P1', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P2', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList.P2', index=2,
      number=3, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=89,
  serialized_end=246,
)


_SUBCONFIDLEVELINFO = _descriptor.Descriptor(
  name='SUBConfIdLevelInfo',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBConfIdLevelInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBConfIdLevelInfo.Id', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Level', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBConfIdLevelInfo.Level', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=248,
  serialized_end=295,
)


_SUBGUILDBUILDINGLEVELDETAILINFORECYCLEITEM = _descriptor.Descriptor(
  name='SUBGuildBuildingLevelDetailInfoRecycleItem',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoRecycleItem',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='itemId', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoRecycleItem.itemId', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='itemCount', full_name='ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoRecycleItem.itemCount', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=297,
  serialized_end=376,
)


_CONFIGDATAGUILDBUILDINGLEVELDETAILINFO = _descriptor.Descriptor(
  name='ConfigDataGuildBuildingLevelDetailInfo',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Level', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.Level', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Desc', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.Desc', index=2,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SolarSystemFlourishLevel', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.SolarSystemFlourishLevel', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BuildingRequire', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.BuildingRequire', index=4,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DeployItemId', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.DeployItemId', index=5,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DeployTime', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.DeployTime', index=6,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RecycleTime', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.RecycleTime', index=7,
      number=9, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RecycleItem', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.RecycleItem', index=8,
      number=10, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NpcMonsterId', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.NpcMonsterId', index=9,
      number=11, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NpcMonsterShieldMax', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.NpcMonsterShieldMax', index=10,
      number=12, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NpcMonsterArmorMax', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.NpcMonsterArmorMax', index=11,
      number=13, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GuildLogicEffectList', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.GuildLogicEffectList', index=12,
      number=14, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='InvadeNpcMonsterId', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.InvadeNpcMonsterId', index=13,
      number=15, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DescStrKey', full_name='ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo.DescStrKey', index=14,
      number=20, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=379,
  serialized_end=997,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGuildBuildingLevelDetailInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGuildBuildingLevelDetailInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=999,
  serialized_end=1101,
)

_SUBGUILDBUILDINGLEVELDETAILINFOGUILDLOGICEFFECTLIST.fields_by_name['type'].enum_type = _ENUMGUILDLOGICEFFECTTYPE
_CONFIGDATAGUILDBUILDINGLEVELDETAILINFO.fields_by_name['BuildingRequire'].message_type = _SUBCONFIDLEVELINFO
_CONFIGDATAGUILDBUILDINGLEVELDETAILINFO.fields_by_name['RecycleItem'].message_type = _SUBGUILDBUILDINGLEVELDETAILINFORECYCLEITEM
_CONFIGDATAGUILDBUILDINGLEVELDETAILINFO.fields_by_name['GuildLogicEffectList'].message_type = _SUBGUILDBUILDINGLEVELDETAILINFOGUILDLOGICEFFECTLIST
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGUILDBUILDINGLEVELDETAILINFO
DESCRIPTOR.message_types_by_name['SUBGuildBuildingLevelDetailInfoGuildLogicEffectList'] = _SUBGUILDBUILDINGLEVELDETAILINFOGUILDLOGICEFFECTLIST
DESCRIPTOR.message_types_by_name['SUBConfIdLevelInfo'] = _SUBCONFIDLEVELINFO
DESCRIPTOR.message_types_by_name['SUBGuildBuildingLevelDetailInfoRecycleItem'] = _SUBGUILDBUILDINGLEVELDETAILINFORECYCLEITEM
DESCRIPTOR.message_types_by_name['ConfigDataGuildBuildingLevelDetailInfo'] = _CONFIGDATAGUILDBUILDINGLEVELDETAILINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMGuildLogicEffectType'] = _ENUMGUILDLOGICEFFECTTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBGuildBuildingLevelDetailInfoGuildLogicEffectList = _reflection.GeneratedProtocolMessageType('SUBGuildBuildingLevelDetailInfoGuildLogicEffectList', (_message.Message,), {
  'DESCRIPTOR' : _SUBGUILDBUILDINGLEVELDETAILINFOGUILDLOGICEFFECTLIST,
  '__module__' : 'ConfigDataGuildBuildingLevelDetailInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoGuildLogicEffectList)
  })
_sym_db.RegisterMessage(SUBGuildBuildingLevelDetailInfoGuildLogicEffectList)

SUBConfIdLevelInfo = _reflection.GeneratedProtocolMessageType('SUBConfIdLevelInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBCONFIDLEVELINFO,
  '__module__' : 'ConfigDataGuildBuildingLevelDetailInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildBuildingLevelDetailInfo.SUBConfIdLevelInfo)
  })
_sym_db.RegisterMessage(SUBConfIdLevelInfo)

SUBGuildBuildingLevelDetailInfoRecycleItem = _reflection.GeneratedProtocolMessageType('SUBGuildBuildingLevelDetailInfoRecycleItem', (_message.Message,), {
  'DESCRIPTOR' : _SUBGUILDBUILDINGLEVELDETAILINFORECYCLEITEM,
  '__module__' : 'ConfigDataGuildBuildingLevelDetailInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildBuildingLevelDetailInfo.SUBGuildBuildingLevelDetailInfoRecycleItem)
  })
_sym_db.RegisterMessage(SUBGuildBuildingLevelDetailInfoRecycleItem)

ConfigDataGuildBuildingLevelDetailInfo = _reflection.GeneratedProtocolMessageType('ConfigDataGuildBuildingLevelDetailInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGUILDBUILDINGLEVELDETAILINFO,
  '__module__' : 'ConfigDataGuildBuildingLevelDetailInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildBuildingLevelDetailInfo.ConfigDataGuildBuildingLevelDetailInfo)
  })
_sym_db.RegisterMessage(ConfigDataGuildBuildingLevelDetailInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGuildBuildingLevelDetailInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildBuildingLevelDetailInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
