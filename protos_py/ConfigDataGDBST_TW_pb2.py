# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGDBST_TW.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGDBST_TW.proto',
  package='ConfigDataGDBST_TW',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x18\x43onfigDataGDBST_TW.proto\x12\x12\x43onfigDataGDBST_TW\"/\n\x12\x43onfigDataGDBST_TW\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\r\n\x05Value\x18\x04 \x02(\t\">\n\x05Items\x12\x35\n\x05items\x18\x01 \x03(\x0b\x32&.ConfigDataGDBST_TW.ConfigDataGDBST_TW')
)




_CONFIGDATAGDBST_TW = _descriptor.Descriptor(
  name='ConfigDataGDBST_TW',
  full_name='ConfigDataGDBST_TW.ConfigDataGDBST_TW',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGDBST_TW.ConfigDataGDBST_TW.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Value', full_name='ConfigDataGDBST_TW.ConfigDataGDBST_TW.Value', index=1,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=48,
  serialized_end=95,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGDBST_TW.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGDBST_TW.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=97,
  serialized_end=159,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGDBST_TW
DESCRIPTOR.message_types_by_name['ConfigDataGDBST_TW'] = _CONFIGDATAGDBST_TW
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGDBST_TW = _reflection.GeneratedProtocolMessageType('ConfigDataGDBST_TW', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGDBST_TW,
  '__module__' : 'ConfigDataGDBST_TW_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGDBST_TW.ConfigDataGDBST_TW)
  })
_sym_db.RegisterMessage(ConfigDataGDBST_TW)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGDBST_TW_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGDBST_TW.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
