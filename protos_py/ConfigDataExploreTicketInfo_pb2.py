# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataExploreTicketInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataExploreTicketInfo.proto',
  package='ConfigDataExploreTicketInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n!ConfigDataExploreTicketInfo.proto\x12\x1b\x43onfigDataExploreTicketInfo\"2\n\x14SUBIntWeightListItem\x12\n\n\x02Id\x18\x01 \x02(\x05\x12\x0e\n\x06Weight\x18\x02 \x02(\x05\"\xcd\x01\n\x1b\x43onfigDataExploreTicketInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x19\n\x11\x44ifficultLevelMin\x18\x03 \x02(\x05\x12\x19\n\x11\x44ifficultLevelMax\x18\x04 \x02(\x05\x12\x15\n\rConcentration\x18\x05 \x02(\x05\x12\x0f\n\x07MaxJump\x18\x06 \x02(\x05\x12\x44\n\tLevelPool\x18\t \x03(\x0b\x32\x31.ConfigDataExploreTicketInfo.SUBIntWeightListItem\"P\n\x05Items\x12G\n\x05items\x18\x01 \x03(\x0b\x32\x38.ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo')
)




_SUBINTWEIGHTLISTITEM = _descriptor.Descriptor(
  name='SUBIntWeightListItem',
  full_name='ConfigDataExploreTicketInfo.SUBIntWeightListItem',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataExploreTicketInfo.SUBIntWeightListItem.Id', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Weight', full_name='ConfigDataExploreTicketInfo.SUBIntWeightListItem.Weight', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=66,
  serialized_end=116,
)


_CONFIGDATAEXPLORETICKETINFO = _descriptor.Descriptor(
  name='ConfigDataExploreTicketInfo',
  full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DifficultLevelMin', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.DifficultLevelMin', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DifficultLevelMax', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.DifficultLevelMax', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Concentration', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.Concentration', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MaxJump', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.MaxJump', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelPool', full_name='ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo.LevelPool', index=5,
      number=9, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=119,
  serialized_end=324,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataExploreTicketInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataExploreTicketInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=326,
  serialized_end=406,
)

_CONFIGDATAEXPLORETICKETINFO.fields_by_name['LevelPool'].message_type = _SUBINTWEIGHTLISTITEM
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAEXPLORETICKETINFO
DESCRIPTOR.message_types_by_name['SUBIntWeightListItem'] = _SUBINTWEIGHTLISTITEM
DESCRIPTOR.message_types_by_name['ConfigDataExploreTicketInfo'] = _CONFIGDATAEXPLORETICKETINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBIntWeightListItem = _reflection.GeneratedProtocolMessageType('SUBIntWeightListItem', (_message.Message,), {
  'DESCRIPTOR' : _SUBINTWEIGHTLISTITEM,
  '__module__' : 'ConfigDataExploreTicketInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataExploreTicketInfo.SUBIntWeightListItem)
  })
_sym_db.RegisterMessage(SUBIntWeightListItem)

ConfigDataExploreTicketInfo = _reflection.GeneratedProtocolMessageType('ConfigDataExploreTicketInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAEXPLORETICKETINFO,
  '__module__' : 'ConfigDataExploreTicketInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataExploreTicketInfo.ConfigDataExploreTicketInfo)
  })
_sym_db.RegisterMessage(ConfigDataExploreTicketInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataExploreTicketInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataExploreTicketInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
