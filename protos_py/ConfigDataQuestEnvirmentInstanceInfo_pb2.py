# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataQuestEnvirmentInstanceInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataQuestEnvirmentInstanceInfo.proto',
  package='ConfigDataQuestEnvirmentInstanceInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n*ConfigDataQuestEnvirmentInstanceInfo.proto\x12$ConfigDataQuestEnvirmentInstanceInfo\"s\n$SUBQuestEnvirmentInstanceInfoNpcList\x12\r\n\x05index\x18\x01 \x02(\x05\x12\x15\n\rSolarSystemId\x18\x02 \x02(\x05\x12\x16\n\x0eSpaceStationId\x18\x03 \x02(\x05\x12\r\n\x05NpcId\x18\x04 \x02(\x05\"K\n.SUBQuestEnvirmentInstanceInfoSolarSystemIdList\x12\r\n\x05index\x18\x01 \x02(\x05\x12\n\n\x02id\x18\x02 \x02(\x05\"\x96\x01\n.SUBQuestEnvirmentInstanceInfoMonsterSelectList\x12\r\n\x05index\x18\x01 \x02(\x05\x12\n\n\x02Id\x18\x02 \x02(\x05\x12I\n\x04Type\x18\x03 \x02(\x0e\x32;.ConfigDataQuestEnvirmentInstanceInfo.ENUMMonsterSelsetType\"J\n-SUBQuestEnvirmentInstanceInfoCustomStringList\x12\r\n\x05index\x18\x01 \x02(\x05\x12\n\n\x02id\x18\x02 \x02(\x05\"\x8c\x04\n$ConfigDataQuestEnvirmentInstanceInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x11\n\tEnvTypeId\x18\x03 \x02(\x05\x12\x17\n\x0f\x46lagSolarSystem\x18\x04 \x02(\x05\x12o\n\x11SolarSystemIdList\x18\x06 \x03(\x0b\x32T.ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoSolarSystemIdList\x12[\n\x07NpcList\x18\x07 \x03(\x0b\x32J.ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList\x12m\n\x10\x43ustomStringList\x18\x08 \x03(\x0b\x32S.ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoCustomStringList\x12o\n\x11MonsterSelectList\x18\t \x03(\x0b\x32T.ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList\"b\n\x05Items\x12Y\n\x05items\x18\x01 \x03(\x0b\x32J.ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo*\xb1\x01\n\x15\x45NUMMonsterSelsetType\x12#\n\x1fMonsterSelsetType_MonsterTeamId\x10\x00\x12\'\n#MonsterSelsetType_MonsterTeamPoolId\x10\x01\x12)\n%MonsterSelsetType_EnvMonsterTeamIndex\x10\x02\x12\x1f\n\x1bMonsterSelsetType_MonsterId\x10\x03')
)

_ENUMMONSTERSELSETTYPE = _descriptor.EnumDescriptor(
  name='ENUMMonsterSelsetType',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.ENUMMonsterSelsetType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='MonsterSelsetType_MonsterTeamId', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonsterSelsetType_MonsterTeamPoolId', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonsterSelsetType_EnvMonsterTeamIndex', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonsterSelsetType_MonsterId', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1135,
  serialized_end=1312,
)
_sym_db.RegisterEnumDescriptor(_ENUMMONSTERSELSETTYPE)

ENUMMonsterSelsetType = enum_type_wrapper.EnumTypeWrapper(_ENUMMONSTERSELSETTYPE)
MonsterSelsetType_MonsterTeamId = 0
MonsterSelsetType_MonsterTeamPoolId = 1
MonsterSelsetType_EnvMonsterTeamIndex = 2
MonsterSelsetType_MonsterId = 3



_SUBQUESTENVIRMENTINSTANCEINFONPCLIST = _descriptor.Descriptor(
  name='SUBQuestEnvirmentInstanceInfoNpcList',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList.index', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SolarSystemId', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList.SolarSystemId', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SpaceStationId', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList.SpaceStationId', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NpcId', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList.NpcId', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=84,
  serialized_end=199,
)


_SUBQUESTENVIRMENTINSTANCEINFOSOLARSYSTEMIDLIST = _descriptor.Descriptor(
  name='SUBQuestEnvirmentInstanceInfoSolarSystemIdList',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoSolarSystemIdList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoSolarSystemIdList.index', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='id', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoSolarSystemIdList.id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=201,
  serialized_end=276,
)


_SUBQUESTENVIRMENTINSTANCEINFOMONSTERSELECTLIST = _descriptor.Descriptor(
  name='SUBQuestEnvirmentInstanceInfoMonsterSelectList',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList.index', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList.Id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Type', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList.Type', index=2,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=279,
  serialized_end=429,
)


_SUBQUESTENVIRMENTINSTANCEINFOCUSTOMSTRINGLIST = _descriptor.Descriptor(
  name='SUBQuestEnvirmentInstanceInfoCustomStringList',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoCustomStringList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoCustomStringList.index', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='id', full_name='ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoCustomStringList.id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=431,
  serialized_end=505,
)


_CONFIGDATAQUESTENVIRMENTINSTANCEINFO = _descriptor.Descriptor(
  name='ConfigDataQuestEnvirmentInstanceInfo',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='EnvTypeId', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.EnvTypeId', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FlagSolarSystem', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.FlagSolarSystem', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SolarSystemIdList', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.SolarSystemIdList', index=3,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NpcList', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.NpcList', index=4,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CustomStringList', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.CustomStringList', index=5,
      number=8, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MonsterSelectList', full_name='ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo.MonsterSelectList', index=6,
      number=9, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=508,
  serialized_end=1032,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataQuestEnvirmentInstanceInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataQuestEnvirmentInstanceInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1034,
  serialized_end=1132,
)

_SUBQUESTENVIRMENTINSTANCEINFOMONSTERSELECTLIST.fields_by_name['Type'].enum_type = _ENUMMONSTERSELSETTYPE
_CONFIGDATAQUESTENVIRMENTINSTANCEINFO.fields_by_name['SolarSystemIdList'].message_type = _SUBQUESTENVIRMENTINSTANCEINFOSOLARSYSTEMIDLIST
_CONFIGDATAQUESTENVIRMENTINSTANCEINFO.fields_by_name['NpcList'].message_type = _SUBQUESTENVIRMENTINSTANCEINFONPCLIST
_CONFIGDATAQUESTENVIRMENTINSTANCEINFO.fields_by_name['CustomStringList'].message_type = _SUBQUESTENVIRMENTINSTANCEINFOCUSTOMSTRINGLIST
_CONFIGDATAQUESTENVIRMENTINSTANCEINFO.fields_by_name['MonsterSelectList'].message_type = _SUBQUESTENVIRMENTINSTANCEINFOMONSTERSELECTLIST
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAQUESTENVIRMENTINSTANCEINFO
DESCRIPTOR.message_types_by_name['SUBQuestEnvirmentInstanceInfoNpcList'] = _SUBQUESTENVIRMENTINSTANCEINFONPCLIST
DESCRIPTOR.message_types_by_name['SUBQuestEnvirmentInstanceInfoSolarSystemIdList'] = _SUBQUESTENVIRMENTINSTANCEINFOSOLARSYSTEMIDLIST
DESCRIPTOR.message_types_by_name['SUBQuestEnvirmentInstanceInfoMonsterSelectList'] = _SUBQUESTENVIRMENTINSTANCEINFOMONSTERSELECTLIST
DESCRIPTOR.message_types_by_name['SUBQuestEnvirmentInstanceInfoCustomStringList'] = _SUBQUESTENVIRMENTINSTANCEINFOCUSTOMSTRINGLIST
DESCRIPTOR.message_types_by_name['ConfigDataQuestEnvirmentInstanceInfo'] = _CONFIGDATAQUESTENVIRMENTINSTANCEINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMMonsterSelsetType'] = _ENUMMONSTERSELSETTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBQuestEnvirmentInstanceInfoNpcList = _reflection.GeneratedProtocolMessageType('SUBQuestEnvirmentInstanceInfoNpcList', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTENVIRMENTINSTANCEINFONPCLIST,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoNpcList)
  })
_sym_db.RegisterMessage(SUBQuestEnvirmentInstanceInfoNpcList)

SUBQuestEnvirmentInstanceInfoSolarSystemIdList = _reflection.GeneratedProtocolMessageType('SUBQuestEnvirmentInstanceInfoSolarSystemIdList', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTENVIRMENTINSTANCEINFOSOLARSYSTEMIDLIST,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoSolarSystemIdList)
  })
_sym_db.RegisterMessage(SUBQuestEnvirmentInstanceInfoSolarSystemIdList)

SUBQuestEnvirmentInstanceInfoMonsterSelectList = _reflection.GeneratedProtocolMessageType('SUBQuestEnvirmentInstanceInfoMonsterSelectList', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTENVIRMENTINSTANCEINFOMONSTERSELECTLIST,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoMonsterSelectList)
  })
_sym_db.RegisterMessage(SUBQuestEnvirmentInstanceInfoMonsterSelectList)

SUBQuestEnvirmentInstanceInfoCustomStringList = _reflection.GeneratedProtocolMessageType('SUBQuestEnvirmentInstanceInfoCustomStringList', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTENVIRMENTINSTANCEINFOCUSTOMSTRINGLIST,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.SUBQuestEnvirmentInstanceInfoCustomStringList)
  })
_sym_db.RegisterMessage(SUBQuestEnvirmentInstanceInfoCustomStringList)

ConfigDataQuestEnvirmentInstanceInfo = _reflection.GeneratedProtocolMessageType('ConfigDataQuestEnvirmentInstanceInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAQUESTENVIRMENTINSTANCEINFO,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.ConfigDataQuestEnvirmentInstanceInfo)
  })
_sym_db.RegisterMessage(ConfigDataQuestEnvirmentInstanceInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataQuestEnvirmentInstanceInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataQuestEnvirmentInstanceInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
