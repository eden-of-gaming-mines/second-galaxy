# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataRareWormholeActiveInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataRareWormholeActiveInfo.proto',
  package='ConfigDataRareWormholeActiveInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n&ConfigDataRareWormholeActiveInfo.proto\x12 ConfigDataRareWormholeActiveInfo\"[\n ConfigDataRareWormholeActiveInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x18\n\x10RefreshCheckTime\x18\x06 \x02(\x05\x12\x11\n\tStartTime\x18\x07 \x02(\t\"Z\n\x05Items\x12Q\n\x05items\x18\x01 \x03(\x0b\x32\x42.ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo')
)




_CONFIGDATARAREWORMHOLEACTIVEINFO = _descriptor.Descriptor(
  name='ConfigDataRareWormholeActiveInfo',
  full_name='ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RefreshCheckTime', full_name='ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo.RefreshCheckTime', index=1,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='StartTime', full_name='ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo.StartTime', index=2,
      number=7, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=76,
  serialized_end=167,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataRareWormholeActiveInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataRareWormholeActiveInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=169,
  serialized_end=259,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATARAREWORMHOLEACTIVEINFO
DESCRIPTOR.message_types_by_name['ConfigDataRareWormholeActiveInfo'] = _CONFIGDATARAREWORMHOLEACTIVEINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataRareWormholeActiveInfo = _reflection.GeneratedProtocolMessageType('ConfigDataRareWormholeActiveInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATARAREWORMHOLEACTIVEINFO,
  '__module__' : 'ConfigDataRareWormholeActiveInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRareWormholeActiveInfo.ConfigDataRareWormholeActiveInfo)
  })
_sym_db.RegisterMessage(ConfigDataRareWormholeActiveInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataRareWormholeActiveInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRareWormholeActiveInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
