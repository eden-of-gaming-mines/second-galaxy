# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataShipTacticalEquipInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataShipTacticalEquipInfo.proto',
  package='ConfigDataShipTacticalEquipInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n%ConfigDataShipTacticalEquipInfo.proto\x12\x1f\x43onfigDataShipTacticalEquipInfo\"+\n\x0cSUBParamInfo\x12\x0c\n\x04Name\x18\x01 \x02(\t\x12\r\n\x05Value\x18\x02 \x02(\x02\"2\n\x15SUBCommonPropertyInfo\x12\n\n\x02Id\x18\x01 \x02(\x05\x12\r\n\x05Value\x18\x02 \x02(\x02\"\xc5\x04\n\x1f\x43onfigDataShipTacticalEquipInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12T\n\x0c\x46unctionType\x18\x04 \x02(\x0e\x32>.ConfigDataShipTacticalEquipInfo.ENUMTacticalEquipFunctionType\x12J\n\x13\x45\x66\x66\x65\x63tDescParamList\x18\x06 \x03(\x0b\x32-.ConfigDataShipTacticalEquipInfo.SUBParamInfo\x12\x0f\n\x07RangMax\x18\x07 \x02(\x02\x12\x12\n\nChargeTime\x18\x08 \x02(\r\x12\x11\n\tBufIdList\x18\t \x03(\x05\x12=\n\x06Params\x18\n \x03(\x0b\x32-.ConfigDataShipTacticalEquipInfo.SUBParamInfo\x12H\n\x10GroupByEquipType\x18\x0b \x02(\x0e\x32..ConfigDataShipTacticalEquipInfo.ENUMEquipType\x12N\n\x0ePropertiesList\x18\x0c \x03(\x0b\x32\x36.ConfigDataShipTacticalEquipInfo.SUBCommonPropertyInfo\x12\x1c\n\x14LocalPropertiesCount\x18\r \x02(\x05\x12\x10\n\x08LaunchCD\x18\x0e \x02(\r\x12\x1c\n\x14IconResPathInStation\x18\x0f \x02(\t\x12\x15\n\rFakeBufInfoId\x18\x14 \x02(\x05\"X\n\x05Items\x12O\n\x05items\x18\x01 \x03(\x0b\x32@.ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo*\xb1\x08\n\x1d\x45NUMTacticalEquipFunctionType\x12:\n6TacticalEquipFunctionType_IncByNeadbyDeadThenAttachBuf\x10\x01\x12\x39\n5TacticalEquipFunctionType_AttachBufBySupperEnergyFull\x10\x02\x12\x41\n=TacticalEquipFunctionType_AttachBufAndInvisibleByArmorPercent\x10\x03\x12;\n7TacticalEquipFunctionType_ProvideFinalexByTargetDitance\x10\x04\x12=\n9TacticalEquipFunctionType_AttachBuff2SelfByAttackCritical\x10\x05\x12\x38\n4TacticalEquipFunctionType_ProvideFinalexBySelfEnergy\x10\x06\x12\x37\n3TacticalEquipFunctionType_ProvideFinalExBySelfSpeed\x10\x07\x12\x35\n1TacticalEquipFunctionType_AddShiledAndBuffByArmor\x10\x08\x12=\n9TacticalEquipFunctionType_RecoverySuperEnergyByKillingHit\x10\t\x12;\n7TacticalEquipFunctionType_AttachBuffByNotBeHitForAWhile\x10\n\x12\x43\n?TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponEquipLaunch\x10\x0b\x12\x38\n4TacticalEquipFunctionType_ProvideFinalexBySelfShield\x10\x0c\x12;\n7TacticalEquipFunctionType_ProvideFinalexBySpeedOverload\x10\r\x12Q\nMTacticalEquipFunctionType_InvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent\x10\x0e\x12\x33\n/TacticalEquipFunctionType_AttachBufByKillingHit\x10\x0f\x12\x32\n.TacticalEquipFunctionType_ReduceCDByKillingHit\x10\x14\x12<\n8TacticalEquipFunctionType_RecoveryShipEnergyByKillingHit\x10\x1e*\xaf\x04\n\rENUMEquipType\x12\x15\n\x11\x45quipType_Booster\x10\x01\x12\x1b\n\x17\x45quipType_BlinkExecutor\x10\x02\x12\x1c\n\x18\x45quipType_ShieldRepairer\x10\x03\x12\x1d\n\x19\x45quipType_ShieldDisturber\x10\x04\x12\x1c\n\x18\x45quipType_RangeDisturber\x10\x05\x12\x1d\n\x19\x45quipType_WeaponDisturber\x10\x06\x12\x1a\n\x16\x45quipType_SpeedReducer\x10\x07\x12\x17\n\x13\x45quipType_EnergyDec\x10\x08\x12 \n\x1c\x45quipType_ExtraEnergyPackage\x10\t\x12\"\n\x1e\x45quipType_RemoteShieldRepairer\x10\n\x12\"\n\x1e\x45quipType_RemoteEnergyRepairer\x10\x0b\x12\x19\n\x15\x45quipType_TeamAtkAsst\x10\x0c\x12\x19\n\x15\x45quipType_TeamDefAsst\x10\r\x12\x1a\n\x16\x45quipType_TeamElecAsst\x10\x0e\x12\x19\n\x15\x45quipType_TeamMovAsst\x10\x0f\x12\x1a\n\x16\x45quipType_BlinkBlocker\x10\x14\x12%\n!EquipType_RemoteAccuracyDisturber\x10\x1e\x12!\n\x1d\x45quipType_TeamSignalDisturber\x10(')
)

_ENUMTACTICALEQUIPFUNCTIONTYPE = _descriptor.EnumDescriptor(
  name='ENUMTacticalEquipFunctionType',
  full_name='ConfigDataShipTacticalEquipInfo.ENUMTacticalEquipFunctionType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_IncByNeadbyDeadThenAttachBuf', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AttachBufBySupperEnergyFull', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AttachBufAndInvisibleByArmorPercent', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ProvideFinalexByTargetDitance', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AttachBuff2SelfByAttackCritical', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ProvideFinalexBySelfEnergy', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ProvideFinalExBySelfSpeed', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AddShiledAndBuffByArmor', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_RecoverySuperEnergyByKillingHit', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AttachBuffByNotBeHitForAWhile', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponEquipLaunch', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ProvideFinalexBySelfShield', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ProvideFinalexBySpeedOverload', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_InvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_AttachBufByKillingHit', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_ReduceCDByKillingHit', index=15, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='TacticalEquipFunctionType_RecoveryShipEnergyByKillingHit', index=16, number=30,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=846,
  serialized_end=1919,
)
_sym_db.RegisterEnumDescriptor(_ENUMTACTICALEQUIPFUNCTIONTYPE)

ENUMTacticalEquipFunctionType = enum_type_wrapper.EnumTypeWrapper(_ENUMTACTICALEQUIPFUNCTIONTYPE)
_ENUMEQUIPTYPE = _descriptor.EnumDescriptor(
  name='ENUMEquipType',
  full_name='ConfigDataShipTacticalEquipInfo.ENUMEquipType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='EquipType_Booster', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_BlinkExecutor', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_ShieldRepairer', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_ShieldDisturber', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_RangeDisturber', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_WeaponDisturber', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_SpeedReducer', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_EnergyDec', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_ExtraEnergyPackage', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_RemoteShieldRepairer', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_RemoteEnergyRepairer', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_TeamAtkAsst', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_TeamDefAsst', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_TeamElecAsst', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_TeamMovAsst', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_BlinkBlocker', index=15, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_RemoteAccuracyDisturber', index=16, number=30,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EquipType_TeamSignalDisturber', index=17, number=40,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1922,
  serialized_end=2481,
)
_sym_db.RegisterEnumDescriptor(_ENUMEQUIPTYPE)

ENUMEquipType = enum_type_wrapper.EnumTypeWrapper(_ENUMEQUIPTYPE)
TacticalEquipFunctionType_IncByNeadbyDeadThenAttachBuf = 1
TacticalEquipFunctionType_AttachBufBySupperEnergyFull = 2
TacticalEquipFunctionType_AttachBufAndInvisibleByArmorPercent = 3
TacticalEquipFunctionType_ProvideFinalexByTargetDitance = 4
TacticalEquipFunctionType_AttachBuff2SelfByAttackCritical = 5
TacticalEquipFunctionType_ProvideFinalexBySelfEnergy = 6
TacticalEquipFunctionType_ProvideFinalExBySelfSpeed = 7
TacticalEquipFunctionType_AddShiledAndBuffByArmor = 8
TacticalEquipFunctionType_RecoverySuperEnergyByKillingHit = 9
TacticalEquipFunctionType_AttachBuffByNotBeHitForAWhile = 10
TacticalEquipFunctionType_IncBuff2AttachBuffByWeaponEquipLaunch = 11
TacticalEquipFunctionType_ProvideFinalexBySelfShield = 12
TacticalEquipFunctionType_ProvideFinalexBySpeedOverload = 13
TacticalEquipFunctionType_InvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent = 14
TacticalEquipFunctionType_AttachBufByKillingHit = 15
TacticalEquipFunctionType_ReduceCDByKillingHit = 20
TacticalEquipFunctionType_RecoveryShipEnergyByKillingHit = 30
EquipType_Booster = 1
EquipType_BlinkExecutor = 2
EquipType_ShieldRepairer = 3
EquipType_ShieldDisturber = 4
EquipType_RangeDisturber = 5
EquipType_WeaponDisturber = 6
EquipType_SpeedReducer = 7
EquipType_EnergyDec = 8
EquipType_ExtraEnergyPackage = 9
EquipType_RemoteShieldRepairer = 10
EquipType_RemoteEnergyRepairer = 11
EquipType_TeamAtkAsst = 12
EquipType_TeamDefAsst = 13
EquipType_TeamElecAsst = 14
EquipType_TeamMovAsst = 15
EquipType_BlinkBlocker = 20
EquipType_RemoteAccuracyDisturber = 30
EquipType_TeamSignalDisturber = 40



_SUBPARAMINFO = _descriptor.Descriptor(
  name='SUBParamInfo',
  full_name='ConfigDataShipTacticalEquipInfo.SUBParamInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataShipTacticalEquipInfo.SUBParamInfo.Name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Value', full_name='ConfigDataShipTacticalEquipInfo.SUBParamInfo.Value', index=1,
      number=2, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=74,
  serialized_end=117,
)


_SUBCOMMONPROPERTYINFO = _descriptor.Descriptor(
  name='SUBCommonPropertyInfo',
  full_name='ConfigDataShipTacticalEquipInfo.SUBCommonPropertyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataShipTacticalEquipInfo.SUBCommonPropertyInfo.Id', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Value', full_name='ConfigDataShipTacticalEquipInfo.SUBCommonPropertyInfo.Value', index=1,
      number=2, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=119,
  serialized_end=169,
)


_CONFIGDATASHIPTACTICALEQUIPINFO = _descriptor.Descriptor(
  name='ConfigDataShipTacticalEquipInfo',
  full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FunctionType', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.FunctionType', index=1,
      number=4, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='EffectDescParamList', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.EffectDescParamList', index=2,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RangMax', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.RangMax', index=3,
      number=7, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ChargeTime', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.ChargeTime', index=4,
      number=8, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BufIdList', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.BufIdList', index=5,
      number=9, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Params', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.Params', index=6,
      number=10, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GroupByEquipType', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.GroupByEquipType', index=7,
      number=11, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PropertiesList', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.PropertiesList', index=8,
      number=12, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LocalPropertiesCount', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.LocalPropertiesCount', index=9,
      number=13, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LaunchCD', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.LaunchCD', index=10,
      number=14, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IconResPathInStation', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.IconResPathInStation', index=11,
      number=15, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FakeBufInfoId', full_name='ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo.FakeBufInfoId', index=12,
      number=20, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=172,
  serialized_end=753,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataShipTacticalEquipInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataShipTacticalEquipInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=755,
  serialized_end=843,
)

_CONFIGDATASHIPTACTICALEQUIPINFO.fields_by_name['FunctionType'].enum_type = _ENUMTACTICALEQUIPFUNCTIONTYPE
_CONFIGDATASHIPTACTICALEQUIPINFO.fields_by_name['EffectDescParamList'].message_type = _SUBPARAMINFO
_CONFIGDATASHIPTACTICALEQUIPINFO.fields_by_name['Params'].message_type = _SUBPARAMINFO
_CONFIGDATASHIPTACTICALEQUIPINFO.fields_by_name['GroupByEquipType'].enum_type = _ENUMEQUIPTYPE
_CONFIGDATASHIPTACTICALEQUIPINFO.fields_by_name['PropertiesList'].message_type = _SUBCOMMONPROPERTYINFO
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATASHIPTACTICALEQUIPINFO
DESCRIPTOR.message_types_by_name['SUBParamInfo'] = _SUBPARAMINFO
DESCRIPTOR.message_types_by_name['SUBCommonPropertyInfo'] = _SUBCOMMONPROPERTYINFO
DESCRIPTOR.message_types_by_name['ConfigDataShipTacticalEquipInfo'] = _CONFIGDATASHIPTACTICALEQUIPINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMTacticalEquipFunctionType'] = _ENUMTACTICALEQUIPFUNCTIONTYPE
DESCRIPTOR.enum_types_by_name['ENUMEquipType'] = _ENUMEQUIPTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBParamInfo = _reflection.GeneratedProtocolMessageType('SUBParamInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBPARAMINFO,
  '__module__' : 'ConfigDataShipTacticalEquipInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataShipTacticalEquipInfo.SUBParamInfo)
  })
_sym_db.RegisterMessage(SUBParamInfo)

SUBCommonPropertyInfo = _reflection.GeneratedProtocolMessageType('SUBCommonPropertyInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBCOMMONPROPERTYINFO,
  '__module__' : 'ConfigDataShipTacticalEquipInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataShipTacticalEquipInfo.SUBCommonPropertyInfo)
  })
_sym_db.RegisterMessage(SUBCommonPropertyInfo)

ConfigDataShipTacticalEquipInfo = _reflection.GeneratedProtocolMessageType('ConfigDataShipTacticalEquipInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATASHIPTACTICALEQUIPINFO,
  '__module__' : 'ConfigDataShipTacticalEquipInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataShipTacticalEquipInfo.ConfigDataShipTacticalEquipInfo)
  })
_sym_db.RegisterMessage(ConfigDataShipTacticalEquipInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataShipTacticalEquipInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataShipTacticalEquipInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
