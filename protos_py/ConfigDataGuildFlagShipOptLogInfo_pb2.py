# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGuildFlagShipOptLogInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGuildFlagShipOptLogInfo.proto',
  package='ConfigDataGuildFlagShipOptLogInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\'ConfigDataGuildFlagShipOptLogInfo.proto\x12!ConfigDataGuildFlagShipOptLogInfo\"\xa6\x02\n!ConfigDataGuildFlagShipOptLogInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12_\n\x17GuildFlagShipOptLogType\x18\x03 \x02(\x0e\x32>.ConfigDataGuildFlagShipOptLogInfo.ENUMGuildFlagShipOptLogType\x12l\n ContentFormatStringParamTypeList\x18\x06 \x03(\x0e\x32\x42.ConfigDataGuildFlagShipOptLogInfo.ENUMCommonFormatStringParamType\x12\x12\n\nNameStrKey\x18\x08 \x02(\t\x12\x12\n\nDescStrKey\x18\t \x02(\t\"\\\n\x05Items\x12S\n\x05items\x18\x01 \x03(\x0b\x32\x44.ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo*\xec\x06\n\x1f\x45NUMCommonFormatStringParamType\x12\x33\n/CommonFormatStringParamType_CurrSolarSystemName\x10\x01\x12.\n*CommonFormatStringParamType_CurrPlayerName\x10\x02\x12(\n$CommonFormatStringParamType_ShipName\x10\x03\x12(\n$CommonFormatStringParamType_ItemName\x10\x04\x12/\n+CommonFormatStringParamType_SolarSystemName\x10\x05\x12/\n+CommonFormatStringParamType_EnvCustomString\x10\x06\x12\x35\n1CommonFormatStringParamType_QuestAcceptStaionName\x10\x07\x12\x38\n4CommonFormatStringParamType_QuestCompleteStationName\x10\x08\x12(\n$CommonFormatStringParamType_InterNum\x10\t\x12(\n$CommonFormatStringParamType_FloatNum\x10\n\x12-\n)CommonFormatStringParamType_GlobalNpcName\x10\x0b\x12.\n*CommonFormatStringParamType_StationNpcName\x10\x0c\x12\x31\n-CommonFormatStringParamType_ServerStringValue\x10\r\x12:\n6CommonFormatStringParamType_QuestAcceptSolarSystemName\x10\x0e\x12<\n8CommonFormatStringParamType_QuestCompleteSolarSystemName\x10\x0f\x12,\n(CommonFormatStringParamType_GuildJobName\x10\x14\x12/\n+CommonFormatStringParamType_MonthlyCardName\x10\x1e*\xe0\x03\n\x1b\x45NUMGuildFlagShipOptLogType\x12&\n\"GuildFlagShipOptLogType_UnpackShip\x10\x01\x12$\n GuildFlagShipOptLogType_PackShip\x10\x02\x12*\n&GuildFlagShipOptLogType_SupplementFuel\x10\x03\x12+\n\'GuildFlagShipOptLogType_CaptainRegister\x10\x04\x12\x34\n0GuildFlagShipOptLogType_CaptainUnregisterForSelf\x10\x05\x12\x35\n1GuildFlagShipOptLogType_CaptainUnregisterForOther\x10\x06\x12\'\n#GuildFlagShipOptLogType_LeaveHangar\x10\x07\x12(\n$GuildFlagShipOptLogType_ReturnHangar\x10\x08\x12-\n)GuildFlagShipOptLogType_FlagShipDestroyed\x10\t\x12+\n\'GuildFlagShipOptLogType_HangarDestroyed\x10\n')
)

_ENUMCOMMONFORMATSTRINGPARAMTYPE = _descriptor.EnumDescriptor(
  name='ENUMCommonFormatStringParamType',
  full_name='ConfigDataGuildFlagShipOptLogInfo.ENUMCommonFormatStringParamType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_CurrSolarSystemName', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_CurrPlayerName', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_ShipName', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_ItemName', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_SolarSystemName', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_EnvCustomString', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_QuestAcceptStaionName', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_QuestCompleteStationName', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_InterNum', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_FloatNum', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_GlobalNpcName', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_StationNpcName', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_ServerStringValue', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_QuestAcceptSolarSystemName', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_QuestCompleteSolarSystemName', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_GuildJobName', index=15, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CommonFormatStringParamType_MonthlyCardName', index=16, number=30,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=470,
  serialized_end=1346,
)
_sym_db.RegisterEnumDescriptor(_ENUMCOMMONFORMATSTRINGPARAMTYPE)

ENUMCommonFormatStringParamType = enum_type_wrapper.EnumTypeWrapper(_ENUMCOMMONFORMATSTRINGPARAMTYPE)
_ENUMGUILDFLAGSHIPOPTLOGTYPE = _descriptor.EnumDescriptor(
  name='ENUMGuildFlagShipOptLogType',
  full_name='ConfigDataGuildFlagShipOptLogInfo.ENUMGuildFlagShipOptLogType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_UnpackShip', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_PackShip', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_SupplementFuel', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_CaptainRegister', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_CaptainUnregisterForSelf', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_CaptainUnregisterForOther', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_LeaveHangar', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_ReturnHangar', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_FlagShipDestroyed', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GuildFlagShipOptLogType_HangarDestroyed', index=9, number=10,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1349,
  serialized_end=1829,
)
_sym_db.RegisterEnumDescriptor(_ENUMGUILDFLAGSHIPOPTLOGTYPE)

ENUMGuildFlagShipOptLogType = enum_type_wrapper.EnumTypeWrapper(_ENUMGUILDFLAGSHIPOPTLOGTYPE)
CommonFormatStringParamType_CurrSolarSystemName = 1
CommonFormatStringParamType_CurrPlayerName = 2
CommonFormatStringParamType_ShipName = 3
CommonFormatStringParamType_ItemName = 4
CommonFormatStringParamType_SolarSystemName = 5
CommonFormatStringParamType_EnvCustomString = 6
CommonFormatStringParamType_QuestAcceptStaionName = 7
CommonFormatStringParamType_QuestCompleteStationName = 8
CommonFormatStringParamType_InterNum = 9
CommonFormatStringParamType_FloatNum = 10
CommonFormatStringParamType_GlobalNpcName = 11
CommonFormatStringParamType_StationNpcName = 12
CommonFormatStringParamType_ServerStringValue = 13
CommonFormatStringParamType_QuestAcceptSolarSystemName = 14
CommonFormatStringParamType_QuestCompleteSolarSystemName = 15
CommonFormatStringParamType_GuildJobName = 20
CommonFormatStringParamType_MonthlyCardName = 30
GuildFlagShipOptLogType_UnpackShip = 1
GuildFlagShipOptLogType_PackShip = 2
GuildFlagShipOptLogType_SupplementFuel = 3
GuildFlagShipOptLogType_CaptainRegister = 4
GuildFlagShipOptLogType_CaptainUnregisterForSelf = 5
GuildFlagShipOptLogType_CaptainUnregisterForOther = 6
GuildFlagShipOptLogType_LeaveHangar = 7
GuildFlagShipOptLogType_ReturnHangar = 8
GuildFlagShipOptLogType_FlagShipDestroyed = 9
GuildFlagShipOptLogType_HangarDestroyed = 10



_CONFIGDATAGUILDFLAGSHIPOPTLOGINFO = _descriptor.Descriptor(
  name='ConfigDataGuildFlagShipOptLogInfo',
  full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GuildFlagShipOptLogType', full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo.GuildFlagShipOptLogType', index=1,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ContentFormatStringParamTypeList', full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo.ContentFormatStringParamTypeList', index=2,
      number=6, type=14, cpp_type=8, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo.NameStrKey', index=3,
      number=8, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DescStrKey', full_name='ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo.DescStrKey', index=4,
      number=9, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=79,
  serialized_end=373,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGuildFlagShipOptLogInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGuildFlagShipOptLogInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=375,
  serialized_end=467,
)

_CONFIGDATAGUILDFLAGSHIPOPTLOGINFO.fields_by_name['GuildFlagShipOptLogType'].enum_type = _ENUMGUILDFLAGSHIPOPTLOGTYPE
_CONFIGDATAGUILDFLAGSHIPOPTLOGINFO.fields_by_name['ContentFormatStringParamTypeList'].enum_type = _ENUMCOMMONFORMATSTRINGPARAMTYPE
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGUILDFLAGSHIPOPTLOGINFO
DESCRIPTOR.message_types_by_name['ConfigDataGuildFlagShipOptLogInfo'] = _CONFIGDATAGUILDFLAGSHIPOPTLOGINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMCommonFormatStringParamType'] = _ENUMCOMMONFORMATSTRINGPARAMTYPE
DESCRIPTOR.enum_types_by_name['ENUMGuildFlagShipOptLogType'] = _ENUMGUILDFLAGSHIPOPTLOGTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGuildFlagShipOptLogInfo = _reflection.GeneratedProtocolMessageType('ConfigDataGuildFlagShipOptLogInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGUILDFLAGSHIPOPTLOGINFO,
  '__module__' : 'ConfigDataGuildFlagShipOptLogInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildFlagShipOptLogInfo.ConfigDataGuildFlagShipOptLogInfo)
  })
_sym_db.RegisterMessage(ConfigDataGuildFlagShipOptLogInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGuildFlagShipOptLogInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildFlagShipOptLogInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
