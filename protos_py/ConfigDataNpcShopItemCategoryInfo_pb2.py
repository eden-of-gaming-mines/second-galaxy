# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataNpcShopItemCategoryInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataNpcShopItemCategoryInfo.proto',
  package='ConfigDataNpcShopItemCategoryInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\'ConfigDataNpcShopItemCategoryInfo.proto\x12!ConfigDataNpcShopItemCategoryInfo\"\xa2\x01\n!ConfigDataNpcShopItemCategoryInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12L\n\x08\x43\x61tegory\x18\x03 \x02(\x0e\x32:.ConfigDataNpcShopItemCategoryInfo.ENUMNpcShopItemCategory\x12\x0f\n\x07IconRes\x18\x05 \x02(\t\x12\x12\n\nNameStrKey\x18\x06 \x02(\t\"\\\n\x05Items\x12S\n\x05items\x18\x01 \x03(\x0b\x32\x44.ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo*\xdd\x04\n\x17\x45NUMNpcShopItemCategory\x12\x1f\n\x1bNpcShopItemCategory_Invalid\x10\x01\x12\x1c\n\x18NpcShopItemCategory_Ship\x10\x02\x12\x1e\n\x1aNpcShopItemCategory_Weapon\x10\x03\x12\x1c\n\x18NpcShopItemCategory_Ammo\x10\x04\x12\x1e\n\x1aNpcShopItemCategory_Device\x10\x05\x12!\n\x1dNpcShopItemCategory_Component\x10\x06\x12 \n\x1cNpcShopItemCategory_CharChip\x10\x07\x12*\n&NpcShopItemCategory_ProductionMaterial\x10\x08\x12%\n!NpcShopItemCategory_ShipBlueprint\x10\t\x12\'\n#NpcShopItemCategory_WeaponBlueprint\x10\n\x12%\n!NpcShopItemCategory_AmmoBlueprint\x10\x0b\x12\'\n#NpcShopItemCategory_DeviceBlueprint\x10\x0c\x12*\n&NpcShopItemCategory_ComponentBlueprint\x10\r\x12&\n\"NpcShopItemCategory_OtherBlueprint\x10\x0e\x12!\n\x1dNpcShopItemCategory_FeatsBook\x10\x0f\x12\x1d\n\x19NpcShopItemCategory_Other\x10\x14')
)

_ENUMNPCSHOPITEMCATEGORY = _descriptor.EnumDescriptor(
  name='ENUMNpcShopItemCategory',
  full_name='ConfigDataNpcShopItemCategoryInfo.ENUMNpcShopItemCategory',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Invalid', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Ship', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Weapon', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Ammo', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Device', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Component', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_CharChip', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_ProductionMaterial', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_ShipBlueprint', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_WeaponBlueprint', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_AmmoBlueprint', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_DeviceBlueprint', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_ComponentBlueprint', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_OtherBlueprint', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_FeatsBook', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpcShopItemCategory_Other', index=15, number=20,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=338,
  serialized_end=943,
)
_sym_db.RegisterEnumDescriptor(_ENUMNPCSHOPITEMCATEGORY)

ENUMNpcShopItemCategory = enum_type_wrapper.EnumTypeWrapper(_ENUMNPCSHOPITEMCATEGORY)
NpcShopItemCategory_Invalid = 1
NpcShopItemCategory_Ship = 2
NpcShopItemCategory_Weapon = 3
NpcShopItemCategory_Ammo = 4
NpcShopItemCategory_Device = 5
NpcShopItemCategory_Component = 6
NpcShopItemCategory_CharChip = 7
NpcShopItemCategory_ProductionMaterial = 8
NpcShopItemCategory_ShipBlueprint = 9
NpcShopItemCategory_WeaponBlueprint = 10
NpcShopItemCategory_AmmoBlueprint = 11
NpcShopItemCategory_DeviceBlueprint = 12
NpcShopItemCategory_ComponentBlueprint = 13
NpcShopItemCategory_OtherBlueprint = 14
NpcShopItemCategory_FeatsBook = 15
NpcShopItemCategory_Other = 20



_CONFIGDATANPCSHOPITEMCATEGORYINFO = _descriptor.Descriptor(
  name='ConfigDataNpcShopItemCategoryInfo',
  full_name='ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Category', full_name='ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo.Category', index=1,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IconRes', full_name='ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo.IconRes', index=2,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo.NameStrKey', index=3,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=79,
  serialized_end=241,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataNpcShopItemCategoryInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataNpcShopItemCategoryInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=243,
  serialized_end=335,
)

_CONFIGDATANPCSHOPITEMCATEGORYINFO.fields_by_name['Category'].enum_type = _ENUMNPCSHOPITEMCATEGORY
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATANPCSHOPITEMCATEGORYINFO
DESCRIPTOR.message_types_by_name['ConfigDataNpcShopItemCategoryInfo'] = _CONFIGDATANPCSHOPITEMCATEGORYINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMNpcShopItemCategory'] = _ENUMNPCSHOPITEMCATEGORY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataNpcShopItemCategoryInfo = _reflection.GeneratedProtocolMessageType('ConfigDataNpcShopItemCategoryInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATANPCSHOPITEMCATEGORYINFO,
  '__module__' : 'ConfigDataNpcShopItemCategoryInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcShopItemCategoryInfo.ConfigDataNpcShopItemCategoryInfo)
  })
_sym_db.RegisterMessage(ConfigDataNpcShopItemCategoryInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataNpcShopItemCategoryInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcShopItemCategoryInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
