# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataMonthlyCardInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataMonthlyCardInfo.proto',
  package='ConfigDataMonthlyCardInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1f\x43onfigDataMonthlyCardInfo.proto\x12\x19\x43onfigDataMonthlyCardInfo\"|\n\x1cSUBMonthlyCardPriviledgeInfo\x12P\n\x0ePriviledgeType\x18\x01 \x02(\x0e\x32\x38.ConfigDataMonthlyCardInfo.ENUMMonthlyCardPriviledgeType\x12\n\n\x02P1\x18\x02 \x02(\x05\"|\n\x12SUBQuestRewardInfo\x12\x42\n\nRewardType\x18\x01 \x02(\x0e\x32..ConfigDataMonthlyCardInfo.ENUMQuestRewardType\x12\n\n\x02P1\x18\x02 \x02(\x05\x12\n\n\x02P2\x18\x03 \x02(\x05\x12\n\n\x02P3\x18\x04 \x02(\x05\"\xa9\x03\n\x19\x43onfigDataMonthlyCardInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\r\n\x05Price\x18\x04 \x02(\x05\x12J\n\x0bPaymentType\x18\x05 \x02(\x0e\x32\x35.ConfigDataMonthlyCardInfo.ENUMMonthlyCardPaymentType\x12\x17\n\x0f\x42onusTotalValue\x18\x06 \x02(\x05\x12H\n\x11OnceRewardContent\x18\x07 \x03(\x0b\x32-.ConfigDataMonthlyCardInfo.SUBQuestRewardInfo\x12I\n\x12\x44\x61ilyRewardContent\x18\x08 \x03(\x0b\x32-.ConfigDataMonthlyCardInfo.SUBQuestRewardInfo\x12O\n\x0ePriviledgeList\x18\t \x03(\x0b\x32\x37.ConfigDataMonthlyCardInfo.SUBMonthlyCardPriviledgeInfo\x12\x12\n\nBgIconPath\x18\n \x02(\t\x12\x12\n\nNameStrKey\x18\x0b \x02(\t\"L\n\x05Items\x12\x43\n\x05items\x18\x01 \x03(\x0b\x32\x34.ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo*\xc6\x02\n\x1d\x45NUMMonthlyCardPriviledgeType\x12%\n!MonthlyCardPriviledgeType_Initial\x10\x00\x12\x38\n4MonthlyCardPriviledgeType_ExtraDailyShipSalvageCount\x10\x01\x12:\n6MonthlyCardPriviledgeType_ExtraDailyWormholeEntryCount\x10\x02\x12\x33\n/MonthlyCardPriviledgeType_ExtraAuctionSlotCount\x10\x03\x12\x30\n,MonthlyCardPriviledgeType_TechSpeedUpPercent\x10\x04\x12!\n\x1dMonthlyCardPriviledgeType_Max\x10\x05*\xa9\x01\n\x1a\x45NUMMonthlyCardPaymentType\x12\"\n\x1eMonthlyCardPaymentType_Initial\x10\x00\x12!\n\x1dMonthlyCardPaymentType_Normal\x10\x01\x12$\n MonthlyCardPaymentType_Subscribe\x10\x02\x12\x1e\n\x1aMonthlyCardPaymentType_Max\x10\x03*\x92\x04\n\x13\x45NUMQuestRewardType\x12\x17\n\x13QuestRewardType_Exp\x10\x01\x12\x1c\n\x18QuestRewardType_Currency\x10\x02\x12\x18\n\x14QuestRewardType_Item\x10\x03\x12\x1c\n\x18QuestRewardType_BindItem\x10\x04\x12!\n\x1dQuestRewardType_FactionCredit\x10\x05\x12\x18\n\x14QuestRewardType_Buff\x10\x06\x12\"\n\x1eQuestRewardType_PlayerLevelExp\x10\x07\x12\x1e\n\x1aQuestRewardType_NpcCaptain\x10\x08\x12\x1d\n\x19QuestRewardType_ScanProbe\x10\t\x12$\n QuestRewardType_GrandFactionItem\x10\n\x12(\n$QuestRewardType_GrandFactionBindItem\x10\x0b\x12#\n\x1fQuestRewardType_GuildTradeMoney\x10\x0c\x12\'\n#QuestRewardType_SolarSystemFlourish\x10\r\x12+\n\'QuestRewardType_SolarSystemGuildMineral\x10\x0e\x12!\n\x1dQuestRewardType_BattlePassExp\x10\x0f')
)

_ENUMMONTHLYCARDPRIVILEDGETYPE = _descriptor.EnumDescriptor(
  name='ENUMMonthlyCardPriviledgeType',
  full_name='ConfigDataMonthlyCardInfo.ENUMMonthlyCardPriviledgeType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_Initial', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_ExtraDailyShipSalvageCount', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_ExtraDailyWormholeEntryCount', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_ExtraAuctionSlotCount', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_TechSpeedUpPercent', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPriviledgeType_Max', index=5, number=5,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=821,
  serialized_end=1147,
)
_sym_db.RegisterEnumDescriptor(_ENUMMONTHLYCARDPRIVILEDGETYPE)

ENUMMonthlyCardPriviledgeType = enum_type_wrapper.EnumTypeWrapper(_ENUMMONTHLYCARDPRIVILEDGETYPE)
_ENUMMONTHLYCARDPAYMENTTYPE = _descriptor.EnumDescriptor(
  name='ENUMMonthlyCardPaymentType',
  full_name='ConfigDataMonthlyCardInfo.ENUMMonthlyCardPaymentType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPaymentType_Initial', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPaymentType_Normal', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPaymentType_Subscribe', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MonthlyCardPaymentType_Max', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1150,
  serialized_end=1319,
)
_sym_db.RegisterEnumDescriptor(_ENUMMONTHLYCARDPAYMENTTYPE)

ENUMMonthlyCardPaymentType = enum_type_wrapper.EnumTypeWrapper(_ENUMMONTHLYCARDPAYMENTTYPE)
_ENUMQUESTREWARDTYPE = _descriptor.EnumDescriptor(
  name='ENUMQuestRewardType',
  full_name='ConfigDataMonthlyCardInfo.ENUMQuestRewardType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_Exp', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_Currency', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_Item', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_BindItem', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_FactionCredit', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_Buff', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_PlayerLevelExp', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_NpcCaptain', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_ScanProbe', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_GrandFactionItem', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_GrandFactionBindItem', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_GuildTradeMoney', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_SolarSystemFlourish', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_SolarSystemGuildMineral', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='QuestRewardType_BattlePassExp', index=14, number=15,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1322,
  serialized_end=1852,
)
_sym_db.RegisterEnumDescriptor(_ENUMQUESTREWARDTYPE)

ENUMQuestRewardType = enum_type_wrapper.EnumTypeWrapper(_ENUMQUESTREWARDTYPE)
MonthlyCardPriviledgeType_Initial = 0
MonthlyCardPriviledgeType_ExtraDailyShipSalvageCount = 1
MonthlyCardPriviledgeType_ExtraDailyWormholeEntryCount = 2
MonthlyCardPriviledgeType_ExtraAuctionSlotCount = 3
MonthlyCardPriviledgeType_TechSpeedUpPercent = 4
MonthlyCardPriviledgeType_Max = 5
MonthlyCardPaymentType_Initial = 0
MonthlyCardPaymentType_Normal = 1
MonthlyCardPaymentType_Subscribe = 2
MonthlyCardPaymentType_Max = 3
QuestRewardType_Exp = 1
QuestRewardType_Currency = 2
QuestRewardType_Item = 3
QuestRewardType_BindItem = 4
QuestRewardType_FactionCredit = 5
QuestRewardType_Buff = 6
QuestRewardType_PlayerLevelExp = 7
QuestRewardType_NpcCaptain = 8
QuestRewardType_ScanProbe = 9
QuestRewardType_GrandFactionItem = 10
QuestRewardType_GrandFactionBindItem = 11
QuestRewardType_GuildTradeMoney = 12
QuestRewardType_SolarSystemFlourish = 13
QuestRewardType_SolarSystemGuildMineral = 14
QuestRewardType_BattlePassExp = 15



_SUBMONTHLYCARDPRIVILEDGEINFO = _descriptor.Descriptor(
  name='SUBMonthlyCardPriviledgeInfo',
  full_name='ConfigDataMonthlyCardInfo.SUBMonthlyCardPriviledgeInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='PriviledgeType', full_name='ConfigDataMonthlyCardInfo.SUBMonthlyCardPriviledgeInfo.PriviledgeType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P1', full_name='ConfigDataMonthlyCardInfo.SUBMonthlyCardPriviledgeInfo.P1', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=62,
  serialized_end=186,
)


_SUBQUESTREWARDINFO = _descriptor.Descriptor(
  name='SUBQuestRewardInfo',
  full_name='ConfigDataMonthlyCardInfo.SUBQuestRewardInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='RewardType', full_name='ConfigDataMonthlyCardInfo.SUBQuestRewardInfo.RewardType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P1', full_name='ConfigDataMonthlyCardInfo.SUBQuestRewardInfo.P1', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P2', full_name='ConfigDataMonthlyCardInfo.SUBQuestRewardInfo.P2', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='P3', full_name='ConfigDataMonthlyCardInfo.SUBQuestRewardInfo.P3', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=188,
  serialized_end=312,
)


_CONFIGDATAMONTHLYCARDINFO = _descriptor.Descriptor(
  name='ConfigDataMonthlyCardInfo',
  full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Price', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.Price', index=1,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PaymentType', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.PaymentType', index=2,
      number=5, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BonusTotalValue', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.BonusTotalValue', index=3,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='OnceRewardContent', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.OnceRewardContent', index=4,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DailyRewardContent', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.DailyRewardContent', index=5,
      number=8, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PriviledgeList', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.PriviledgeList', index=6,
      number=9, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BgIconPath', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.BgIconPath', index=7,
      number=10, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo.NameStrKey', index=8,
      number=11, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=315,
  serialized_end=740,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataMonthlyCardInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataMonthlyCardInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=742,
  serialized_end=818,
)

_SUBMONTHLYCARDPRIVILEDGEINFO.fields_by_name['PriviledgeType'].enum_type = _ENUMMONTHLYCARDPRIVILEDGETYPE
_SUBQUESTREWARDINFO.fields_by_name['RewardType'].enum_type = _ENUMQUESTREWARDTYPE
_CONFIGDATAMONTHLYCARDINFO.fields_by_name['PaymentType'].enum_type = _ENUMMONTHLYCARDPAYMENTTYPE
_CONFIGDATAMONTHLYCARDINFO.fields_by_name['OnceRewardContent'].message_type = _SUBQUESTREWARDINFO
_CONFIGDATAMONTHLYCARDINFO.fields_by_name['DailyRewardContent'].message_type = _SUBQUESTREWARDINFO
_CONFIGDATAMONTHLYCARDINFO.fields_by_name['PriviledgeList'].message_type = _SUBMONTHLYCARDPRIVILEDGEINFO
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAMONTHLYCARDINFO
DESCRIPTOR.message_types_by_name['SUBMonthlyCardPriviledgeInfo'] = _SUBMONTHLYCARDPRIVILEDGEINFO
DESCRIPTOR.message_types_by_name['SUBQuestRewardInfo'] = _SUBQUESTREWARDINFO
DESCRIPTOR.message_types_by_name['ConfigDataMonthlyCardInfo'] = _CONFIGDATAMONTHLYCARDINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMMonthlyCardPriviledgeType'] = _ENUMMONTHLYCARDPRIVILEDGETYPE
DESCRIPTOR.enum_types_by_name['ENUMMonthlyCardPaymentType'] = _ENUMMONTHLYCARDPAYMENTTYPE
DESCRIPTOR.enum_types_by_name['ENUMQuestRewardType'] = _ENUMQUESTREWARDTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBMonthlyCardPriviledgeInfo = _reflection.GeneratedProtocolMessageType('SUBMonthlyCardPriviledgeInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBMONTHLYCARDPRIVILEDGEINFO,
  '__module__' : 'ConfigDataMonthlyCardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataMonthlyCardInfo.SUBMonthlyCardPriviledgeInfo)
  })
_sym_db.RegisterMessage(SUBMonthlyCardPriviledgeInfo)

SUBQuestRewardInfo = _reflection.GeneratedProtocolMessageType('SUBQuestRewardInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBQUESTREWARDINFO,
  '__module__' : 'ConfigDataMonthlyCardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataMonthlyCardInfo.SUBQuestRewardInfo)
  })
_sym_db.RegisterMessage(SUBQuestRewardInfo)

ConfigDataMonthlyCardInfo = _reflection.GeneratedProtocolMessageType('ConfigDataMonthlyCardInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAMONTHLYCARDINFO,
  '__module__' : 'ConfigDataMonthlyCardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataMonthlyCardInfo.ConfigDataMonthlyCardInfo)
  })
_sym_db.RegisterMessage(ConfigDataMonthlyCardInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataMonthlyCardInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataMonthlyCardInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
