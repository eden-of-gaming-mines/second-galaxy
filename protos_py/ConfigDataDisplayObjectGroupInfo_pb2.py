# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataDisplayObjectGroupInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataDisplayObjectGroupInfo.proto',
  package='ConfigDataDisplayObjectGroupInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n&ConfigDataDisplayObjectGroupInfo.proto\x12 ConfigDataDisplayObjectGroupInfo\"F\n ConfigDataDisplayObjectGroupInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x16\n\x0e\x43ountLimitList\x18\x03 \x03(\x05\"Z\n\x05Items\x12Q\n\x05items\x18\x01 \x03(\x0b\x32\x42.ConfigDataDisplayObjectGroupInfo.ConfigDataDisplayObjectGroupInfo')
)




_CONFIGDATADISPLAYOBJECTGROUPINFO = _descriptor.Descriptor(
  name='ConfigDataDisplayObjectGroupInfo',
  full_name='ConfigDataDisplayObjectGroupInfo.ConfigDataDisplayObjectGroupInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataDisplayObjectGroupInfo.ConfigDataDisplayObjectGroupInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CountLimitList', full_name='ConfigDataDisplayObjectGroupInfo.ConfigDataDisplayObjectGroupInfo.CountLimitList', index=1,
      number=3, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=76,
  serialized_end=146,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataDisplayObjectGroupInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataDisplayObjectGroupInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=148,
  serialized_end=238,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATADISPLAYOBJECTGROUPINFO
DESCRIPTOR.message_types_by_name['ConfigDataDisplayObjectGroupInfo'] = _CONFIGDATADISPLAYOBJECTGROUPINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataDisplayObjectGroupInfo = _reflection.GeneratedProtocolMessageType('ConfigDataDisplayObjectGroupInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATADISPLAYOBJECTGROUPINFO,
  '__module__' : 'ConfigDataDisplayObjectGroupInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataDisplayObjectGroupInfo.ConfigDataDisplayObjectGroupInfo)
  })
_sym_db.RegisterMessage(ConfigDataDisplayObjectGroupInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataDisplayObjectGroupInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataDisplayObjectGroupInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
