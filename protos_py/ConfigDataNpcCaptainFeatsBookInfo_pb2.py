# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataNpcCaptainFeatsBookInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataNpcCaptainFeatsBookInfo.proto',
  package='ConfigDataNpcCaptainFeatsBookInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\'ConfigDataNpcCaptainFeatsBookInfo.proto\x12!ConfigDataNpcCaptainFeatsBookInfo\"\xd9\x03\n!ConfigDataNpcCaptainFeatsBookInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x42\n\x04Type\x18\x05 \x02(\x0e\x32\x34.ConfigDataNpcCaptainFeatsBookInfo.ENUMFeatsBookType\x12=\n\x04Rank\x18\x06 \x02(\x0e\x32/.ConfigDataNpcCaptainFeatsBookInfo.ENUMRankType\x12\x43\n\x07SubRank\x18\x07 \x02(\x0e\x32\x32.ConfigDataNpcCaptainFeatsBookInfo.ENUMSubRankType\x12\x0f\n\x07\x46\x65\x61tsId\x18\x08 \x02(\x05\x12\x12\n\nFeatsLevel\x18\t \x02(\x05\x12\x14\n\x0cIsBindOnPick\x18\n \x02(\x08\x12\x11\n\tSalePrice\x18\x0b \x02(\x02\x12\x13\n\x0bIconResPath\x18\x0c \x02(\t\x12U\n\x10ObtainSourceList\x18\r \x03(\x0e\x32;.ConfigDataNpcCaptainFeatsBookInfo.ENUMItemObtainSourceType\x12\x12\n\nNameStrKey\x18\x0e \x02(\t\x12\x12\n\nDescStrKey\x18\x0f \x02(\t\"\\\n\x05Items\x12S\n\x05items\x18\x01 \x03(\x0b\x32\x44.ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo*\xa3\x01\n\x0f\x45NUMSubRankType\x12\x17\n\x13SubRankType_Invalid\x10\x00\x12\x12\n\x0eSubRankType_S1\x10\x01\x12\x12\n\x0eSubRankType_S2\x10\x02\x12\x12\n\x0eSubRankType_S3\x10\x03\x12\x12\n\x0eSubRankType_S4\x10\x04\x12\x12\n\x0eSubRankType_S5\x10\x05\x12\x13\n\x0fSubRankType_Max\x10\x06*\x93\x01\n\x11\x45NUMFeatsBookType\x12\x16\n\x12\x46\x65\x61tsBookType_NONE\x10\x00\x12\x17\n\x13\x46\x65\x61tsBookType_Fight\x10\x01\x12\x19\n\x15\x46\x65\x61tsBookType_Produce\x10\x02\x12\x16\n\x12\x46\x65\x61tsBookType_Tech\x10\x03\x12\x1a\n\x16\x46\x65\x61tsBookType_Delegate\x10\x04*\x9b\x05\n\x18\x45NUMItemObtainSourceType\x12#\n\x1fItemObtainSourceType_Production\x10\x01\x12 \n\x1cItemObtainSourceType_Auction\x10\x02\x12 \n\x1cItemObtainSourceType_NPCShop\x10\x03\x12!\n\x1dItemObtainSourceType_Recharge\x10\x04\x12\x1d\n\x19ItemObtainSourceType_Mall\x10\x05\x12!\n\x1dItemObtainSourceType_Special1\x10\x06\x12!\n\x1dItemObtainSourceType_Special2\x10\x07\x12!\n\x1dItemObtainSourceType_Special3\x10\x08\x12\x1e\n\x1aItemObtainSourceType_Crack\x10\t\x12%\n!ItemObtainSourceType_InfectMisson\x10\n\x12\"\n\x1eItemObtainSourceType_FreeQuest\x10\x0b\x12\'\n#ItemObtainSourceType_DelegateMisson\x10\x0c\x12-\n)ItemObtainSourceType_DelegateMisson_Fight\x10\r\x12,\n(ItemObtainSourceType_FreeSignal_Engineer\x10\x0e\x12-\n)ItemObtainSourceType_FreeSignal_Scientist\x10\x0f\x12-\n)ItemObtainSourceType_BlackMarket_Personal\x10\x14\x12\x1c\n\x18ItemObtainSourceType_Max\x10\x1e*W\n\x0c\x45NUMRankType\x12\x14\n\x10RankType_Invalid\x10\x00\x12\x0f\n\x0bRankType_T1\x10\x01\x12\x0f\n\x0bRankType_T2\x10\x02\x12\x0f\n\x0bRankType_T3\x10\x03')
)

_ENUMSUBRANKTYPE = _descriptor.EnumDescriptor(
  name='ENUMSubRankType',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.ENUMSubRankType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='SubRankType_Invalid', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_S1', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_S2', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_S3', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_S4', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_S5', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SubRankType_Max', index=6, number=6,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=649,
  serialized_end=812,
)
_sym_db.RegisterEnumDescriptor(_ENUMSUBRANKTYPE)

ENUMSubRankType = enum_type_wrapper.EnumTypeWrapper(_ENUMSUBRANKTYPE)
_ENUMFEATSBOOKTYPE = _descriptor.EnumDescriptor(
  name='ENUMFeatsBookType',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.ENUMFeatsBookType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='FeatsBookType_NONE', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FeatsBookType_Fight', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FeatsBookType_Produce', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FeatsBookType_Tech', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FeatsBookType_Delegate', index=4, number=4,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=815,
  serialized_end=962,
)
_sym_db.RegisterEnumDescriptor(_ENUMFEATSBOOKTYPE)

ENUMFeatsBookType = enum_type_wrapper.EnumTypeWrapper(_ENUMFEATSBOOKTYPE)
_ENUMITEMOBTAINSOURCETYPE = _descriptor.EnumDescriptor(
  name='ENUMItemObtainSourceType',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.ENUMItemObtainSourceType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Production', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Auction', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_NPCShop', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Recharge', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Mall', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Special1', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Special2', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Special3', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Crack', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_InfectMisson', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_FreeQuest', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_DelegateMisson', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_DelegateMisson_Fight', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_FreeSignal_Engineer', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_FreeSignal_Scientist', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_BlackMarket_Personal', index=15, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ItemObtainSourceType_Max', index=16, number=30,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=965,
  serialized_end=1632,
)
_sym_db.RegisterEnumDescriptor(_ENUMITEMOBTAINSOURCETYPE)

ENUMItemObtainSourceType = enum_type_wrapper.EnumTypeWrapper(_ENUMITEMOBTAINSOURCETYPE)
_ENUMRANKTYPE = _descriptor.EnumDescriptor(
  name='ENUMRankType',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.ENUMRankType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='RankType_Invalid', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RankType_T1', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RankType_T2', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RankType_T3', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1634,
  serialized_end=1721,
)
_sym_db.RegisterEnumDescriptor(_ENUMRANKTYPE)

ENUMRankType = enum_type_wrapper.EnumTypeWrapper(_ENUMRANKTYPE)
SubRankType_Invalid = 0
SubRankType_S1 = 1
SubRankType_S2 = 2
SubRankType_S3 = 3
SubRankType_S4 = 4
SubRankType_S5 = 5
SubRankType_Max = 6
FeatsBookType_NONE = 0
FeatsBookType_Fight = 1
FeatsBookType_Produce = 2
FeatsBookType_Tech = 3
FeatsBookType_Delegate = 4
ItemObtainSourceType_Production = 1
ItemObtainSourceType_Auction = 2
ItemObtainSourceType_NPCShop = 3
ItemObtainSourceType_Recharge = 4
ItemObtainSourceType_Mall = 5
ItemObtainSourceType_Special1 = 6
ItemObtainSourceType_Special2 = 7
ItemObtainSourceType_Special3 = 8
ItemObtainSourceType_Crack = 9
ItemObtainSourceType_InfectMisson = 10
ItemObtainSourceType_FreeQuest = 11
ItemObtainSourceType_DelegateMisson = 12
ItemObtainSourceType_DelegateMisson_Fight = 13
ItemObtainSourceType_FreeSignal_Engineer = 14
ItemObtainSourceType_FreeSignal_Scientist = 15
ItemObtainSourceType_BlackMarket_Personal = 20
ItemObtainSourceType_Max = 30
RankType_Invalid = 0
RankType_T1 = 1
RankType_T2 = 2
RankType_T3 = 3



_CONFIGDATANPCCAPTAINFEATSBOOKINFO = _descriptor.Descriptor(
  name='ConfigDataNpcCaptainFeatsBookInfo',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Type', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.Type', index=1,
      number=5, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Rank', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.Rank', index=2,
      number=6, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SubRank', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.SubRank', index=3,
      number=7, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FeatsId', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.FeatsId', index=4,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FeatsLevel', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.FeatsLevel', index=5,
      number=9, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IsBindOnPick', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.IsBindOnPick', index=6,
      number=10, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SalePrice', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.SalePrice', index=7,
      number=11, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IconResPath', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.IconResPath', index=8,
      number=12, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ObtainSourceList', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.ObtainSourceList', index=9,
      number=13, type=14, cpp_type=8, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.NameStrKey', index=10,
      number=14, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DescStrKey', full_name='ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo.DescStrKey', index=11,
      number=15, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=79,
  serialized_end=552,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataNpcCaptainFeatsBookInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataNpcCaptainFeatsBookInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=554,
  serialized_end=646,
)

_CONFIGDATANPCCAPTAINFEATSBOOKINFO.fields_by_name['Type'].enum_type = _ENUMFEATSBOOKTYPE
_CONFIGDATANPCCAPTAINFEATSBOOKINFO.fields_by_name['Rank'].enum_type = _ENUMRANKTYPE
_CONFIGDATANPCCAPTAINFEATSBOOKINFO.fields_by_name['SubRank'].enum_type = _ENUMSUBRANKTYPE
_CONFIGDATANPCCAPTAINFEATSBOOKINFO.fields_by_name['ObtainSourceList'].enum_type = _ENUMITEMOBTAINSOURCETYPE
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATANPCCAPTAINFEATSBOOKINFO
DESCRIPTOR.message_types_by_name['ConfigDataNpcCaptainFeatsBookInfo'] = _CONFIGDATANPCCAPTAINFEATSBOOKINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMSubRankType'] = _ENUMSUBRANKTYPE
DESCRIPTOR.enum_types_by_name['ENUMFeatsBookType'] = _ENUMFEATSBOOKTYPE
DESCRIPTOR.enum_types_by_name['ENUMItemObtainSourceType'] = _ENUMITEMOBTAINSOURCETYPE
DESCRIPTOR.enum_types_by_name['ENUMRankType'] = _ENUMRANKTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataNpcCaptainFeatsBookInfo = _reflection.GeneratedProtocolMessageType('ConfigDataNpcCaptainFeatsBookInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATANPCCAPTAINFEATSBOOKINFO,
  '__module__' : 'ConfigDataNpcCaptainFeatsBookInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcCaptainFeatsBookInfo.ConfigDataNpcCaptainFeatsBookInfo)
  })
_sym_db.RegisterMessage(ConfigDataNpcCaptainFeatsBookInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataNpcCaptainFeatsBookInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcCaptainFeatsBookInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
