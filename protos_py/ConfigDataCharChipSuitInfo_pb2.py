# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataCharChipSuitInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataCharChipSuitInfo.proto',
  package='ConfigDataCharChipSuitInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n ConfigDataCharChipSuitInfo.proto\x12\x1a\x43onfigDataCharChipSuitInfo\"O\n\x1a\x43onfigDataCharChipSuitInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x11\n\tBufIdList\x18\x05 \x03(\x05\x12\x12\n\nNameStrKey\x18\x06 \x02(\t\"N\n\x05Items\x12\x45\n\x05items\x18\x01 \x03(\x0b\x32\x36.ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo')
)




_CONFIGDATACHARCHIPSUITINFO = _descriptor.Descriptor(
  name='ConfigDataCharChipSuitInfo',
  full_name='ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BufIdList', full_name='ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo.BufIdList', index=1,
      number=5, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo.NameStrKey', index=2,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=64,
  serialized_end=143,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataCharChipSuitInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataCharChipSuitInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=145,
  serialized_end=223,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATACHARCHIPSUITINFO
DESCRIPTOR.message_types_by_name['ConfigDataCharChipSuitInfo'] = _CONFIGDATACHARCHIPSUITINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataCharChipSuitInfo = _reflection.GeneratedProtocolMessageType('ConfigDataCharChipSuitInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATACHARCHIPSUITINFO,
  '__module__' : 'ConfigDataCharChipSuitInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataCharChipSuitInfo.ConfigDataCharChipSuitInfo)
  })
_sym_db.RegisterMessage(ConfigDataCharChipSuitInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataCharChipSuitInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataCharChipSuitInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
