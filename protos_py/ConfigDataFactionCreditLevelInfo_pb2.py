# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataFactionCreditLevelInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataFactionCreditLevelInfo.proto',
  package='ConfigDataFactionCreditLevelInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n&ConfigDataFactionCreditLevelInfo.proto\x12 ConfigDataFactionCreditLevelInfo\"\xbc\x01\n ConfigDataFactionCreditLevelInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12T\n\x12\x46\x61\x63tionCreditLevel\x18\x04 \x02(\x0e\x32\x38.ConfigDataFactionCreditLevelInfo.ENUMFactionCreditLevel\x12\x10\n\x08MinValue\x18\x05 \x02(\x05\x12\x10\n\x08MaxValue\x18\x06 \x02(\x05\x12\x12\n\nNameStrKey\x18\x07 \x02(\t\"Z\n\x05Items\x12Q\n\x05items\x18\x01 \x03(\x0b\x32\x42.ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo*\xc6\x02\n\x16\x45NUMFactionCreditLevel\x12\x19\n\x15\x46\x61\x63tionCreditLevel_N5\x10\x01\x12\x19\n\x15\x46\x61\x63tionCreditLevel_N4\x10\x02\x12\x19\n\x15\x46\x61\x63tionCreditLevel_N3\x10\x03\x12\x19\n\x15\x46\x61\x63tionCreditLevel_N2\x10\x04\x12\x19\n\x15\x46\x61\x63tionCreditLevel_N1\x10\x05\x12\x1e\n\x1a\x46\x61\x63tionCreditLevel_Neutral\x10\x06\x12\x19\n\x15\x46\x61\x63tionCreditLevel_P1\x10\x07\x12\x19\n\x15\x46\x61\x63tionCreditLevel_P2\x10\x08\x12\x19\n\x15\x46\x61\x63tionCreditLevel_P3\x10\t\x12\x19\n\x15\x46\x61\x63tionCreditLevel_P4\x10\n\x12\x19\n\x15\x46\x61\x63tionCreditLevel_P5\x10\x0b')
)

_ENUMFACTIONCREDITLEVEL = _descriptor.EnumDescriptor(
  name='ENUMFactionCreditLevel',
  full_name='ConfigDataFactionCreditLevelInfo.ENUMFactionCreditLevel',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_N5', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_N4', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_N3', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_N2', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_N1', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_Neutral', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_P1', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_P2', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_P3', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_P4', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FactionCreditLevel_P5', index=10, number=11,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=360,
  serialized_end=686,
)
_sym_db.RegisterEnumDescriptor(_ENUMFACTIONCREDITLEVEL)

ENUMFactionCreditLevel = enum_type_wrapper.EnumTypeWrapper(_ENUMFACTIONCREDITLEVEL)
FactionCreditLevel_N5 = 1
FactionCreditLevel_N4 = 2
FactionCreditLevel_N3 = 3
FactionCreditLevel_N2 = 4
FactionCreditLevel_N1 = 5
FactionCreditLevel_Neutral = 6
FactionCreditLevel_P1 = 7
FactionCreditLevel_P2 = 8
FactionCreditLevel_P3 = 9
FactionCreditLevel_P4 = 10
FactionCreditLevel_P5 = 11



_CONFIGDATAFACTIONCREDITLEVELINFO = _descriptor.Descriptor(
  name='ConfigDataFactionCreditLevelInfo',
  full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FactionCreditLevel', full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo.FactionCreditLevel', index=1,
      number=4, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MinValue', full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo.MinValue', index=2,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MaxValue', full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo.MaxValue', index=3,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo.NameStrKey', index=4,
      number=7, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=77,
  serialized_end=265,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataFactionCreditLevelInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataFactionCreditLevelInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=267,
  serialized_end=357,
)

_CONFIGDATAFACTIONCREDITLEVELINFO.fields_by_name['FactionCreditLevel'].enum_type = _ENUMFACTIONCREDITLEVEL
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAFACTIONCREDITLEVELINFO
DESCRIPTOR.message_types_by_name['ConfigDataFactionCreditLevelInfo'] = _CONFIGDATAFACTIONCREDITLEVELINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMFactionCreditLevel'] = _ENUMFACTIONCREDITLEVEL
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataFactionCreditLevelInfo = _reflection.GeneratedProtocolMessageType('ConfigDataFactionCreditLevelInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAFACTIONCREDITLEVELINFO,
  '__module__' : 'ConfigDataFactionCreditLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataFactionCreditLevelInfo.ConfigDataFactionCreditLevelInfo)
  })
_sym_db.RegisterMessage(ConfigDataFactionCreditLevelInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataFactionCreditLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataFactionCreditLevelInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
