# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGEConfigableConst.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGEConfigableConst.proto',
  package='ConfigDataGEConfigableConst',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n!ConfigDataGEConfigableConst.proto\x12\x1b\x43onfigDataGEConfigableConst\"\x89\x01\n\x1b\x43onfigDataGEConfigableConst\x12\n\n\x02ID\x18\x02 \x02(\x05\x12O\n\x11\x43onfigableConstId\x18\x03 \x02(\x0e\x32\x34.ConfigDataGEConfigableConst.ENUMGEConfigableConstId\x12\r\n\x05Value\x18\x04 \x02(\t\"P\n\x05Items\x12G\n\x05items\x18\x01 \x03(\x0b\x32\x38.ConfigDataGEConfigableConst.ConfigDataGEConfigableConst*\xf8\x07\n\x17\x45NUMGEConfigableConstId\x12\x1a\n\x16GEConfigableConstId_AU\x10\x01\x12-\n)GEConfigableConstId_GravitationalConstant\x10\x02\x12%\n!GEConfigableConstId_MinMoonRadius\x10\x03\x12%\n!GEConfigableConstId_MaxMoonRadius\x10\x04\x12\x33\n/GEConfigableConstId_MinDistanceBetweenMoonOrbit\x10\x05\x12\x41\n=GEConfigableConstId_DifferenceTemperatureBetweenPlanetAndMoon\x10\x06\x12/\n+GEConfigableConstId_StandardMoonOrbitRadius\x10\x07\x12\x33\n/GEConfigableConstId_MoonOrbitRadiusFloatPercent\x10\x08\x12\x35\n1GEConfigableConstId_MinEccentricityOfNormalPlanet\x10\t\x12\x35\n1GEConfigableConstId_MaxEccentricityOfNormalPlanet\x10\n\x12\x36\n2GEConfigableConstId_MinOrbitDistanceBetweenPlanets\x10\x0b\x12>\n:GEConfigableConstId_MinSpecialEccentricityOfOutboardPlanet\x10\x0c\x12>\n:GEConfigableConstId_MaxSpecialEccentricityOfOutboardPlanet\x10\r\x12<\n8GEConfigableConstId_SpecialEccentricityAppearProbability\x10\x0e\x12\x30\n,GEConfigableConstId_PlanetRadiusFloatPercent\x10\x0f\x12.\n*GEConfigableConstId_StarRaidusFloatPercent\x10\x14\x12\x32\n.GEConfigableConstId_MinAsteroidBeltOrbitRadius\x10\x1e\x12/\n+GEConfigableConstId_OrbUnityObjectFixRadius\x10(\x12;\n7GEConfigableConstId_MinOrbitDistanceBetweenPlanetsRatio\x10\x32')
)

_ENUMGECONFIGABLECONSTID = _descriptor.EnumDescriptor(
  name='ENUMGEConfigableConstId',
  full_name='ConfigDataGEConfigableConst.ENUMGEConfigableConstId',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_AU', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_GravitationalConstant', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinMoonRadius', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MaxMoonRadius', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinDistanceBetweenMoonOrbit', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_DifferenceTemperatureBetweenPlanetAndMoon', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_StandardMoonOrbitRadius', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MoonOrbitRadiusFloatPercent', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinEccentricityOfNormalPlanet', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MaxEccentricityOfNormalPlanet', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinOrbitDistanceBetweenPlanets', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinSpecialEccentricityOfOutboardPlanet', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MaxSpecialEccentricityOfOutboardPlanet', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_SpecialEccentricityAppearProbability', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_PlanetRadiusFloatPercent', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_StarRaidusFloatPercent', index=15, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinAsteroidBeltOrbitRadius', index=16, number=30,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_OrbUnityObjectFixRadius', index=17, number=40,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GEConfigableConstId_MinOrbitDistanceBetweenPlanetsRatio', index=18, number=50,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=289,
  serialized_end=1305,
)
_sym_db.RegisterEnumDescriptor(_ENUMGECONFIGABLECONSTID)

ENUMGEConfigableConstId = enum_type_wrapper.EnumTypeWrapper(_ENUMGECONFIGABLECONSTID)
GEConfigableConstId_AU = 1
GEConfigableConstId_GravitationalConstant = 2
GEConfigableConstId_MinMoonRadius = 3
GEConfigableConstId_MaxMoonRadius = 4
GEConfigableConstId_MinDistanceBetweenMoonOrbit = 5
GEConfigableConstId_DifferenceTemperatureBetweenPlanetAndMoon = 6
GEConfigableConstId_StandardMoonOrbitRadius = 7
GEConfigableConstId_MoonOrbitRadiusFloatPercent = 8
GEConfigableConstId_MinEccentricityOfNormalPlanet = 9
GEConfigableConstId_MaxEccentricityOfNormalPlanet = 10
GEConfigableConstId_MinOrbitDistanceBetweenPlanets = 11
GEConfigableConstId_MinSpecialEccentricityOfOutboardPlanet = 12
GEConfigableConstId_MaxSpecialEccentricityOfOutboardPlanet = 13
GEConfigableConstId_SpecialEccentricityAppearProbability = 14
GEConfigableConstId_PlanetRadiusFloatPercent = 15
GEConfigableConstId_StarRaidusFloatPercent = 20
GEConfigableConstId_MinAsteroidBeltOrbitRadius = 30
GEConfigableConstId_OrbUnityObjectFixRadius = 40
GEConfigableConstId_MinOrbitDistanceBetweenPlanetsRatio = 50



_CONFIGDATAGECONFIGABLECONST = _descriptor.Descriptor(
  name='ConfigDataGEConfigableConst',
  full_name='ConfigDataGEConfigableConst.ConfigDataGEConfigableConst',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGEConfigableConst.ConfigDataGEConfigableConst.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ConfigableConstId', full_name='ConfigDataGEConfigableConst.ConfigDataGEConfigableConst.ConfigableConstId', index=1,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Value', full_name='ConfigDataGEConfigableConst.ConfigDataGEConfigableConst.Value', index=2,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=67,
  serialized_end=204,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGEConfigableConst.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGEConfigableConst.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=206,
  serialized_end=286,
)

_CONFIGDATAGECONFIGABLECONST.fields_by_name['ConfigableConstId'].enum_type = _ENUMGECONFIGABLECONSTID
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGECONFIGABLECONST
DESCRIPTOR.message_types_by_name['ConfigDataGEConfigableConst'] = _CONFIGDATAGECONFIGABLECONST
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMGEConfigableConstId'] = _ENUMGECONFIGABLECONSTID
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGEConfigableConst = _reflection.GeneratedProtocolMessageType('ConfigDataGEConfigableConst', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGECONFIGABLECONST,
  '__module__' : 'ConfigDataGEConfigableConst_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGEConfigableConst.ConfigDataGEConfigableConst)
  })
_sym_db.RegisterMessage(ConfigDataGEConfigableConst)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGEConfigableConst_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGEConfigableConst.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
