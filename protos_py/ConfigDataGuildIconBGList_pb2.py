# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGuildIconBGList.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGuildIconBGList.proto',
  package='ConfigDataGuildIconBGList',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1f\x43onfigDataGuildIconBGList.proto\x12\x19\x43onfigDataGuildIconBGList\":\n\x19\x43onfigDataGuildIconBGList\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x11\n\tAssetPath\x18\x03 \x02(\t\"L\n\x05Items\x12\x43\n\x05items\x18\x01 \x03(\x0b\x32\x34.ConfigDataGuildIconBGList.ConfigDataGuildIconBGList')
)




_CONFIGDATAGUILDICONBGLIST = _descriptor.Descriptor(
  name='ConfigDataGuildIconBGList',
  full_name='ConfigDataGuildIconBGList.ConfigDataGuildIconBGList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGuildIconBGList.ConfigDataGuildIconBGList.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='AssetPath', full_name='ConfigDataGuildIconBGList.ConfigDataGuildIconBGList.AssetPath', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=62,
  serialized_end=120,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGuildIconBGList.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGuildIconBGList.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=122,
  serialized_end=198,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGUILDICONBGLIST
DESCRIPTOR.message_types_by_name['ConfigDataGuildIconBGList'] = _CONFIGDATAGUILDICONBGLIST
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGuildIconBGList = _reflection.GeneratedProtocolMessageType('ConfigDataGuildIconBGList', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGUILDICONBGLIST,
  '__module__' : 'ConfigDataGuildIconBGList_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildIconBGList.ConfigDataGuildIconBGList)
  })
_sym_db.RegisterMessage(ConfigDataGuildIconBGList)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGuildIconBGList_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildIconBGList.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
