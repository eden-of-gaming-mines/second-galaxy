# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataPassiveSkillInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataPassiveSkillInfo.proto',
  package='ConfigDataPassiveSkillInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n ConfigDataPassiveSkillInfo.proto\x12\x1a\x43onfigDataPassiveSkillInfo\"\x8a\x02\n\x1a\x43onfigDataPassiveSkillInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x17\n\x0fLevelInfoIdList\x18\x05 \x03(\x05\x12\x0c\n\x04\x46lag\x18\x06 \x02(\x05\x12\x11\n\tSkillType\x18\x07 \x02(\x05\x12\x18\n\x10IconArtResString\x18\x08 \x02(\t\x12\x17\n\x0fSkillModifyType\x18\t \x02(\x05\x12!\n\x19\x44rivingLicenseQuestIdList\x18\n \x03(\x05\x12\r\n\x05Grade\x18\x0b \x02(\x05\x12\x12\n\nNameStrKey\x18\r \x02(\t\x12\x12\n\nDescStrKey\x18\x0e \x02(\t\x12\x19\n\x11GenericNameStrKey\x18\x0f \x02(\t\"N\n\x05Items\x12\x45\n\x05items\x18\x01 \x03(\x0b\x32\x36.ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo')
)




_CONFIGDATAPASSIVESKILLINFO = _descriptor.Descriptor(
  name='ConfigDataPassiveSkillInfo',
  full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelInfoIdList', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.LevelInfoIdList', index=1,
      number=5, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Flag', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.Flag', index=2,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SkillType', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.SkillType', index=3,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IconArtResString', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.IconArtResString', index=4,
      number=8, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SkillModifyType', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.SkillModifyType', index=5,
      number=9, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DrivingLicenseQuestIdList', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.DrivingLicenseQuestIdList', index=6,
      number=10, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Grade', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.Grade', index=7,
      number=11, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NameStrKey', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.NameStrKey', index=8,
      number=13, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DescStrKey', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.DescStrKey', index=9,
      number=14, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GenericNameStrKey', full_name='ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo.GenericNameStrKey', index=10,
      number=15, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=65,
  serialized_end=331,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataPassiveSkillInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataPassiveSkillInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=333,
  serialized_end=411,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAPASSIVESKILLINFO
DESCRIPTOR.message_types_by_name['ConfigDataPassiveSkillInfo'] = _CONFIGDATAPASSIVESKILLINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataPassiveSkillInfo = _reflection.GeneratedProtocolMessageType('ConfigDataPassiveSkillInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAPASSIVESKILLINFO,
  '__module__' : 'ConfigDataPassiveSkillInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPassiveSkillInfo.ConfigDataPassiveSkillInfo)
  })
_sym_db.RegisterMessage(ConfigDataPassiveSkillInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataPassiveSkillInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPassiveSkillInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
