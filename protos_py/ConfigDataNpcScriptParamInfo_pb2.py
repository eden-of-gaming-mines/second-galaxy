# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataNpcScriptParamInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataNpcScriptParamInfo.proto',
  package='ConfigDataNpcScriptParamInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\"ConfigDataNpcScriptParamInfo.proto\x12\x1c\x43onfigDataNpcScriptParamInfo\"*\n\x1c\x43onfigDataNpcScriptParamInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\"R\n\x05Items\x12I\n\x05items\x18\x01 \x03(\x0b\x32:.ConfigDataNpcScriptParamInfo.ConfigDataNpcScriptParamInfo')
)




_CONFIGDATANPCSCRIPTPARAMINFO = _descriptor.Descriptor(
  name='ConfigDataNpcScriptParamInfo',
  full_name='ConfigDataNpcScriptParamInfo.ConfigDataNpcScriptParamInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataNpcScriptParamInfo.ConfigDataNpcScriptParamInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=68,
  serialized_end=110,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataNpcScriptParamInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataNpcScriptParamInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=112,
  serialized_end=194,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATANPCSCRIPTPARAMINFO
DESCRIPTOR.message_types_by_name['ConfigDataNpcScriptParamInfo'] = _CONFIGDATANPCSCRIPTPARAMINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataNpcScriptParamInfo = _reflection.GeneratedProtocolMessageType('ConfigDataNpcScriptParamInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATANPCSCRIPTPARAMINFO,
  '__module__' : 'ConfigDataNpcScriptParamInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcScriptParamInfo.ConfigDataNpcScriptParamInfo)
  })
_sym_db.RegisterMessage(ConfigDataNpcScriptParamInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataNpcScriptParamInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataNpcScriptParamInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
