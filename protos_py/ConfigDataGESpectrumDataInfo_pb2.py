# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGESpectrumDataInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGESpectrumDataInfo.proto',
  package='ConfigDataGESpectrumDataInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\"ConfigDataGESpectrumDataInfo.proto\x12\x1c\x43onfigDataGESpectrumDataInfo\"\xaa\x01\n\x1c\x43onfigDataGESpectrumDataInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x14\n\x0cSpectrumName\x18\x03 \x02(\t\x12\x19\n\x11\x41ppearProbability\x18\x04 \x02(\x05\x12\r\n\x05\x43olor\x18\x05 \x02(\t\x12\x17\n\x0fSubSpectrumList\x18\x06 \x03(\x05\x12\x13\n\x0bResPathList\x18\x07 \x03(\x05\x12\x10\n\x08SunColor\x18\x08 \x03(\x02\"R\n\x05Items\x12I\n\x05items\x18\x01 \x03(\x0b\x32:.ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo')
)




_CONFIGDATAGESPECTRUMDATAINFO = _descriptor.Descriptor(
  name='ConfigDataGESpectrumDataInfo',
  full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SpectrumName', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.SpectrumName', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='AppearProbability', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.AppearProbability', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Color', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.Color', index=3,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SubSpectrumList', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.SubSpectrumList', index=4,
      number=6, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ResPathList', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.ResPathList', index=5,
      number=7, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SunColor', full_name='ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo.SunColor', index=6,
      number=8, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=69,
  serialized_end=239,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGESpectrumDataInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGESpectrumDataInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=241,
  serialized_end=323,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGESPECTRUMDATAINFO
DESCRIPTOR.message_types_by_name['ConfigDataGESpectrumDataInfo'] = _CONFIGDATAGESPECTRUMDATAINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGESpectrumDataInfo = _reflection.GeneratedProtocolMessageType('ConfigDataGESpectrumDataInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGESPECTRUMDATAINFO,
  '__module__' : 'ConfigDataGESpectrumDataInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGESpectrumDataInfo.ConfigDataGESpectrumDataInfo)
  })
_sym_db.RegisterMessage(ConfigDataGESpectrumDataInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGESpectrumDataInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGESpectrumDataInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
